

<html>
<head>
 <title>Stalin Ani Form</title>  
 
 <base href="<?php echo base_url(); ?>">
<script> var baseUrl = "<?php echo base_url()?>";</script>
<link href="<?php echo base_url(); ?>public/css/style.css" rel='stylesheet'>
<link href="public/css/bootstrap.min.css" rel='stylesheet'>
<link href="public/css/plugin/bootstrap-select.min.css" rel='stylesheet'>

<script src="public/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>public/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script> 
</head>


<body>



    <div class="inner-wrapper">

        <h1 class="form-title">Stalin Ani</h1>
	 <?php echo validation_errors();?>
	 
	 <?php
                       if($this->session->flashdata('message')!=''){
                           echo $this->session->flashdata('message');
                        }
                       ?>
	 <?php echo form_open(base_url()."start-campaign?user_id=".$user_id,array("id"=>"campaignform"));?>
	 
		<input type="hidden" name="user_id" value="<?php echo isset($user_id)?$user_id:""?>">
		<input type="hidden" value="<?php echo isset($detail['state_id'])?$detail['state_id']:""?>" name="home_state">
		<input type="hidden" value="<?php echo isset($detail['district_id'])?$detail['district_id']:""?>" name="home_district">
		
		
		
        <div class="question-block-wrap">
                <div class="flex-row form-option-wrap">
                        <div class="flex-col">
                            <p>Q.  Please select your assembly constituency in <b><?php echo isset($detail['district_name'])?ucfirst($detail['district_name']):"N/A" ?></b>,<b><?php echo isset($detail['state_name'])?ucfirst($detail['state_name']):"N/A" ?></b> <span style="color:red">*</span></p>
                        </div>
                </div>           
            <!--Radio-->
                <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="">
							
                              <select class="form-control select-form" name="home_ac" >
                                    <option  value="">--Select--</option>
                                    <?php if(isset($acs) && !empty($acs)){
										
										foreach($acs as $key=>$val){
									?>
										
										<option value="<?php echo $val['id']?>"><?php echo ucfirst($val['ac'])?></option>
										
									<?php } }?>	
                                    
                                </select>
                                
                        </div>
                        
                        <div class="ac-list1">
                        
                        </div>

                </div>
                
                </div>
                <!--Radio Close-->
         </div>
         
         
         


         <div class="question-block-wrap" id ="qus-block2"style="display:none">
                <div class="flex-row form-option-wrap">
                        <div class="flex-col">
                            <p>Q.  I will vote from <b><?php echo isset($detail['district_name'])?ucfirst($detail['district_name']):"N/A" ?></b>,<b><?php echo isset($detail['state_name'])?ucfirst($detail['state_name']):"N/A" ?></b> in 2019 General Election <span style="color:red">*</span></p>
                        </div>
                </div>           
            <!--Radio-->
                <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="listing">
                                <select class="form-control select-form" name="vote-state" onchange="getDistrictList(this.value,'dis-list')">
                                    <option  value="">--Select--</option>
                                    <?php if(isset($states) && !empty($states)){
										
										foreach($states as $key=>$val){
									?>
										
										<option value="<?php echo $val['state_id']?>"><?php echo ucfirst($val['state_name'])?></option>
										
									<?php } }?>	
                                    
                                </select>
                                
                       </div>
                       
                       <div class="dis-list">
						   
					   </div>
					   
					   <div class="ac-list">
						   
					   </div>	
                

                </div>
                
                </div>
                <!--Radio Close-->
         </div>

        
		<div class="question-block-wrap" id ="qus-block3" >
                <div class="flex-row form-option-wrap">
                        <div class="flex-col">
                            <p id="qus-text-3">Q.  The above mentioned constituency is where I'll vote from in GE 2019.<span style="color:red">*</span></p>
                        </div>
                </div>           
            <!--Radio-->
                <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="listing3">
							
								<select data-value="<?php echo isset($detail['state_id'])?$detail['state_id']:""?>" class="form-control select-form" id="question3" name="answer3">
                                    <option value="1" selected>Yes</option>
                                    <option value="2">No</option>
                                </select>
								
                                
                       </div>
                       

                </div>
                
                </div>
                <!--Radio Close-->
         </div>
		 <div class="question-block-wrap" id ="qus-block5"style='display:none' >
               <!-- <div class="flex-row form-option-wrap">
                        <div class="flex-col">
                            <p id="qus-text-3">Q. Please provide information of your voting place/area <span style="color:red">*</span></p>                        </div>
                </div>      -->     
            <!--Radio-->
                <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="listing3">
						
								
                                <select style="display:none;"  id="current-state3" class="form-control select-form" name="current-state" onchange="getDistrictList2(this.value,'dis-list3')">
                                    <option value="">Please select your voting state.</option>
                                    <?php if(isset($states) && !empty($states)){
										
                                            foreach($states as $key=>$val){
                                        ?>

                                                <option value="<?php echo $val['state_id']?>"><?php echo ucfirst($val['state_name'])?></option>

                                        <?php } }?>	
                                    
                                </select>
                                
                       </div>
                       
                       <div class="dis-list3">
						   
					   </div>
					   
					   <div class="ac-list3">
						   
					   </div>	
                

                </div>
                
                </div>
                <!--Radio Close-->
         </div>
   
	
	<div class="question-block-wrap qus-block4" >
                <div class="flex-row form-option-wrap">
                        <div class="flex-col">
                            <p>Q. Please share your current address to receive the <i>Stalin Ani</i> gift hamper.</p>
                        </div>
                </div>           
            <!--Radio-->
                <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="">
							
									
                            <textarea class="form-control" name="address" placeholder="Address"></textarea>
                                
                        </div>
                        
                </div>
                
                </div>
               <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="">
									
                            <input type ="text" class="form-control" name="pincode" placeholder="Pincode" onkeypress="return numeralsOnly(event)">
                                
                        </div>
                        
                </div>
               </div>
            
                <!--Radio Close-->
         </div>
                
                
        <div class="question-block-wrap qus-block4"  id ="qus-block4">
                <div class="flex-row form-option-wrap">
                        <div class="flex-col">
                            <p>Q. Please select your preferred language to receive further communication<span style="color:red">*</span></p>
                        </div>
                </div>           
            <!--Radio-->
                <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="">
							
									
                                <select class="form-control select-form" name="language1">
                                    <option value="">--Preference 1--</option>
										<option value="en">English</option>
                                        <option value="hi">हिन्दी</option>
                                        <option value="ben">বাংলা</option>
                                        <option value="tl">తెలుగు</option>
                                        <option value="mt">मराठी</option>
                                        <option value="tm">தமிழ்</option>
                                        <option value="ur">اردو</option>
                                        <option value="kan">ಕನ್ನಡ</option>
                                        <option value="gj">ગુજરાતી</option>
                                        <option value="od">ଓଡ଼ିଆ</option>
                                        <option value="ma">മലയാളം</option>
                                </select>
                                
                        </div>
                        
                </div>
                
                </div>
            
            
            <div class="flex-row form-option-wrap">
                <div class="flex-col-sm-3">
                        <div class="">
							
									
                                <select class="form-control select-form" name="language2">
                                    <option value="">--Preference 2--</option>
					<option value="en">English</option>
                                        <option value="hi">हिन्दी</option>
                                        <option value="ben">বাংলা</option>
                                        <option value="tl">తెలుగు</option>
                                        <option value="mt">मराठी</option>
                                        <option value="tm">தமிழ்</option>
                                        <option value="ur">اردو</option>
                                        <option value="kan">ಕನ್ನಡ</option>
                                        <option value="gj">ગુજરાતી</option>
                                        <option value="od">ଓଡ଼ିଆ</option>
                                        <option value="ma">മലയാളം</option>
                                </select>
                                
                        </div>
                        
                </div>
                
                </div>
                <!--Radio Close-->
         </div>
	

           <div class="flex-row">
                <div class="flex-col-sm-10 text-center m-t-sm">
                        <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Submit</button>
                        
                    </div>
           </div>
        <?php echo form_close();?>
     </div>




     <style>
      .inner-wrapper {
    background: #fff;
    padding: 15px 15px 20px;
    margin: 15px auto;
    width: 95%;
    border-radius: 5px;
    box-shadow: 0 0 12px #00000014;
}


.question-block-wrap {
    margin: 0 0 38px 0;
}
        [type="checkbox"].filled-in:not(:checked)+label:after {
    height: 20px;
    width: 20px;
    background-color: transparent;
    border: 1px solid #d2d1d1;
    top: -3px;
    z-index: 0;
    border-radius: 3px;
}

[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 31px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 24px;
    font-size: 14px;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-weight: 400;
    color: #363636;
    font-weight: 400;
}

.form-title {
    text-align: center;
    color: #1199d8;
    text-transform: uppercase;
    font-size: 20px;
    margin: 0 0 20px 0;
}


.text-center{
    text-align: center;
}

select.form-control.select-form {
    border: none;
    border-bottom: 1px solid #000;
    margin: 0 0 10px;
}
select {
    -moz-appearance: none;
    -webkit-appearance: menulist;
}

.flex-col-sm-3 {
    width: 100%;
    padding: 0 10px;
}

.form-option-wrap p{
    font-size: 14px;
}

     </style>

</body>
</html>
<script>

$(document).ready(function(){
	
	$("#question1").change(function(){
		
		var selectedValue = $(this).val();
		
		if(selectedValue==1){
			
			$("#qus-block2").css("display","none");
			$(".ac-list1").css("display","block");
			
			var stateId = $(this).data('value');
                        var districtId = $(this).data('district');
			
			//ac-list1
			
			getAcList(stateId,'ac-list1',districtId);
			
			
		}else if(selectedValue==2){
				
				$(".ac-list1").css("display","none");
				$("#qus-block2").css("display","block");
		}else{
		
			$(".ac-list1").css("display","none");
			$("#qus-block2").css("display","none");
			$("#qus-block3").css("display","none");
			$(".qus-block4").css("display","none");	
                       
		}
		
		
	});
	
	$("#question3").change(function(){
		
		var selectedValue = $(this).val();
		
		
		
		if(selectedValue==2){
			
			$("#current-state3,#qus-block5").css("display","block");
			
		} else {
			$("#current-state3,#qus-block5").css("display","none");
		}
		
		$(".qus-block4").css("display","block");
		
		
	});
	
	
	
	
$("#campaignform").validate({
    errorClass: "alert-danger",
    rules: {
       answer1:{required:true
                    },
       answer3:{required:true},
       'cur_ac':{   required: {
                depends: function () {
                    if ($('#question3').find(":selected").val() == '2') {
                        return true;
                    }
                }
            },
	   },
        'home_ac':{required:true},
       'current-state':{
		    required: {
                depends: function () {
                    if ($('#question3').find(":selected").val() == '2') {
                        return true;
                    }
                }
            },
		   },
       'cur_dis':{  required: {
                depends: function () {
                    if ($('#question3').find(":selected").val() == '2') {
                        return true;
                    }
                }
            },},
       'language1':{required:true},
       'language2':{required:true},
       'address':{required:false},
       'pincode':{required:false}
    },
    submitHandler: function (form) {
                    form.submit();
    }
    
});
		
});

function getDistrictList(value, div) {

    //$("body").addClass("loader-wrap");
    //$('.loader-img').show();
    //$('#add_task').attr('disabled', true);

    var opval = "dis";

    $.ajax({
        type: 'GET',
        url: baseUrl + 'form_task/getListOption',
        data: {option: opval, value: value},
        success: function (data) {
            //data = JSON.parse(data);
            //$('.' + divId).css("display", "block");
            $('.' + div).html(data);

            //$("body").removeClass("loader-wrap");
            //$('.loader-img').hide();

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}

function getDistrictList2(value, div) {

    //$("body").addClass("loader-wrap");
    //$('.loader-img').show();
    //$('#add_task').attr('disabled', true);

    var opval = "cur-dis";
    $.ajax({
        type: 'GET',
        url: baseUrl + 'form_task/getListOption',
        data: {option: opval, value: value},
        success: function (data) {
            //data = JSON.parse(data);
            //$('.' + divId).css("display", "block");
            $('.' + div).html(data);

            //$("body").removeClass("loader-wrap");
            //$('.loader-img').hide();

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}

function getAcList(value, div,districtId) {
    //$("body").addClass("loader-wrap");
    //$('.loader-img').show();
    //$('#add_task').attr('disabled', true);
    var opval = "ac";
    
    var request  ={ option: opval, value: value };
    
    // if fetching by state
    if(districtId){
			
		request.type = "test"
                request.districtId = districtId;
                
    }

    $.ajax({
        type: 'GET',
        url: baseUrl + 'form_task/getListOption',
        data:request ,
        success: function (data) {
            //data = JSON.parse(data);
            //$('.' + divId).css("display", "block");
            $('.' + div).html(data);

            //$("body").removeClass("loader-wrap");
            //$('.loader-img').hide();

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}

function getAcList2(value, div) {
	
    //$("body").addClass("loader-wrap");
    //$('.loader-img').show();
    //$('#add_task').attr('disabled', true);

    var opval = "ac2";
    
    var request  ={ option: opval, value: value };
    

    $.ajax({
        type: 'GET',
        url: baseUrl + 'form_task/getListOption',
        data:request ,
        success: function (data) {
            //data = JSON.parse(data);
            //$('.' + divId).css("display", "block");
            $('.' + div).html(data);

            //$("body").removeClass("loader-wrap");
            //$('.loader-img').hide();

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}

//display next or final qus
function displayNextQues(acVal,name){

	if(acVal!="" || acVal!=undefined){
            
            if(name!="home_ac"){
                
                var state = $('select[name=vote-state]').find(":selected").text();
                var dis = $('select[name=vote_dis]').find(":selected").text();

                var text ='Q.  Are you a resident of '+dis+', '+state+'?';

            $("#qus-text-3").html(text);
                
            }
	
            $("#qus-block3").css("display","block");			
			
	}else{
            
            $("#qus-block3").css("display","none");
	}
	
}

function numeralsOnly(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
            ((evt.which) ? evt.which : 0));
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }



</script>
