<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; height: 100%; " >
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="">
                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top">
                                        <img src="<?php echo base_url('public/images/mail/header.png'); ?>"  style="width: 100%;" >
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 15px 15px 15px; background-image: url(<?php echo base_url('public/images/mail/background.PNG'); ?>);background-repeat: no-repeat;background-size: cover;background-attachment: fixed;background-position: center; " >
                                        <p style="font-size: 14px;">வணக்கம் <strong> <?php echo $name; ?>, </strong> </p>
                                        <p style="font-size: 14px;">
                                            ஸ்டாலின் அணி செயலியின் பயன்பாட்டை நன்கறிந்த உங்களை நம் செயலியின் ஓர் சிறப்புப் பயனராக நாங்கள் கருதுகிறோம். இது ஸ்டாலின் அணி செயலியினரான எங்களுக்கும் கழகத்திற்கும் பெருமிதம் கொள்ளும் தருணமாக அமைகிறது. மேலும் தங்களின் முயற்சிகளை அங்கீகரிக்கும் வகையில் தாங்கள் ஸ்டாலின் அணி குழுவில் இணைந்து பணியாற்றிட "பயிற்சிப் பணி" (internship) வாய்ப்பு வழங்கப்படுகிறது. எனவே தாங்கள் பின்வரும் இணைப்பில் குறிப்பிடப்பட்டுள்ள விவரங்களை பதிவிடுமாறு கேட்டுக்கொள்கிறோம். </p>
                                        <p style="font-size: 14px;">
                                            குறிப்பு - விருப்பத்தைப் பதிவிடுவோருடன் மட்டுமே அடுத்தக்கட்ட கலந்துரையாடல் நிகழ்த்தப்படும்.
                                        </p>
                                        <p style="text-align: center;font-size: 14px;">
                                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSe_ewQxzuvI0KoIqXB_d6VUhRVoIy4AqKdpN6NAedzYcvu4Ag/viewform?usp=sf_link"  alt="இங்கே கிளிக் செய்யவும்" title="இங்கே கிளிக் செய்யவும்">
                                             <img src="<?php echo base_url('public/images/mail/button.PNG'); ?>" > </a>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>