<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; background:#eaf2ff; color:#333333;padding:15px 15px 15px 15px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#fbfcff" style="padding:0px 0px 15px 0px;">
                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#323848;text-transform:capitalize;padding: 20px 8px;">
                                        அன்புள்ள <?php echo $name; ?>,
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="color:#323848;padding: 0px 8px;">
                                        <p>
                                            ஸ்டாலின் அணி செயலியில் ஐந்தாம் நிலையை வெற்றிகரமாக கடந்ததற்கு வாழ்த்துக்கள். இதை பாராட்டும் விதமாக தி.மு.க. தலைவர் மு.க.ஸ்டாலின் அவர்களின் ஸ்டாலின் அணி குழுவில் இருந்து உங்களுக்கான பதக்கத்தை இந்த மின்னஞ்சலுடன் இணைத்துள்ளோம்.</p>
                                        <p>
                                            திரு மு.க.ஸ்டாலின் அவர்கள் முதல்வராக பணி தொடங்குவதற்கும் தமிழகத்திற்கான அவரது நோக்கத்தை முன்னெடுத்துச் செல்வதற்கும் நீங்கள் வழங்கும் ஆதரவையும் பங்களிப்பையும் கண்டு மகிழ்ச்சியடைகிறோம். தொடர்ந்து தங்கள் ஆதரவை வழங்குவீர் என நம்புகின்றோம்.
                                        </p>
                                        <p>
                                            தமிழ்நாட்டின் முன்னேற்றத்திற்காக இங்கு நாம் ஒன்றிணைந்துள்ளோம்.
                                        </p>
                                        <br />
                                        <p>
                                            அன்புடன்,<br />ஸ்டாலின் அணி.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>