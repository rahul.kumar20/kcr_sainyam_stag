<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:15px 15px 15px 15px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="" style="padding:0px 0px 15px 0px;">
                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top" style="text-align:center;background:#b6b9bf;">
                                        <img src="public/images/MKS-leading.jpg" alt="logo" style="width:100%;" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="
                                    color: #000;
                                    font-size: 16px;
                                    font-weight: 400;
                                    line-height: 28px;
                                    padding: 27px 0px 17px 0px;">
                                        ஸ்டாலின் அணி குடும்பத்திற்கு உங்களை அன்புடன் வரவேற்கின்றோம்! </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="
                                    color: #000;
                                    font-size: 16px;
                                    font-weight: 400;
                                    line-height: 28px;
                                    padding: 17px 0px 15px 0px;">
                                        தலைவர் மு.க. ஸ்டாலின் அவர்களுடன் உங்களை நேரடியாக இணைத்திட எடுக்கப்பட்ட ஒரு முன்னெடுப்பே இந்தச் செயலி ஆகும். இதன் வழியாக, தமிழகத்தின் முன்னேற்றத்திற்காகக் கழகம் எடுக்கும் அனைத்து முயற்சிகளிலும் நீங்களும் பங்காற்றலாம்.
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="
                                    color: #000;
                                    font-size: 16px;
                                    font-weight: 400;
                                    line-height: 28px;
                                    padding: 15px 0px 20px 0px;">
                                        விடியலின் பாதையில் உங்களின் ஒத்துழைப்பு ஒளியாக மிளிரட்டும். </b>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="" class="signup-detail">
                                <tr>
                                    <td>
                                        <p style=""><span style="font-size: 16px;
                                          font-weight: 500;">அன்புடன் <br> ஸ்டாலின் அணி </span></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>