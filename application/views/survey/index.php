<html>
    <head>
        <title>Campaign 4 India</title> 
    </head>
    <link href="<?php echo base_url(); ?>public/css/style.css" rel='stylesheet'>
    <link href="public/css/bootstrap.min.css" rel='stylesheet'>
    <link href="public/css/plugin/bootstrap-select.min.css" rel='stylesheet'>

    <script src="public/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>public/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>public/js/jquery.validate.min.js"></script>

    <body>
        <div class="inner-wrapper">

            <?php
            if (isset($questions) && !empty($questions)) {
                $step = $step + 1;
                echo form_open(base_url() . "start-survey?code=" . $code . "&step=" . $step, array("id" => "survey-form"));
                ?>


                <h1 class="form-title">Stalin Ani</h1>
            <?php foreach ($questions as $question) {
                $options = $question['options'];
                ?>
                <div class="flex-row form-option-wrap">
                    <div class="flex-col">
                        <input type="hidden" name="step" value="<?php echo isset($step) ? $step : 0 ?>">
                        <input type="hidden" name="qid[]" value="<?php echo isset($question['qid']) ? $question['qid'] : 0 ?>">
                        <input type="hidden" name="parentQusId" value="<?php echo isset($question['parent_qid']) ? $question['parent_qid'] : 0 ?>">
                        <p>Q:- <?php echo isset($question['text']) ? $question['text'] : "" ?></p>
                        <label class="check_error_msg"></label>
                    </div>

                </div>           


                <?php if (isset($question['type']) && $question['type'] == "checkbox") { ?>
                    <div class="flex-row form-option-wrap">
                        <?php
                        foreach ($options as $key => $value) {
                            ?>


                            <div class="flex-col">
                                <div class="">
                                    <input type="hidden" name="desc[]" value="<?php echo isset($value["description"]) ? $value["description"] : "" ?>">
                                    <input  style="display:none;" class="filter-type filled-in" type="radio" name="answer[<?php echo isset($question['qid']) ? $question['qid'] : 0 ?>][]" id="Yes<?php echo $key; ?>" value="<?php echo isset($value['oid']) ? $value['oid'] : ""; ?>" onclick="return getNext('<?php echo $code?>','<?php echo $question['depth']; ?>','<?php echo $value['option_code'];?>','<?php echo $question['qid']; ?>')">
                                    <label for="Yes<?php echo $key; ?>" class="lbl-check"><span></span><?php echo isset($value['description']) ? $value['description'] : "" ?></label>
                                </div>
                            </div>


                    <?php
                        } ?>
                    </div>
                    <?php
                } elseif (isset($question['type']) && $question['type'] == "dropdown") { ?>
                    <div class="flex-row form-option-wrap">
                        <div class="flex-col-sm-10">
                            <div class="">
                                <select required name="answer[]" class="form-control select-form" onchange="getNextDependentQ(this.value,'<?php echo $question['qid']?>','<?php echo $question['depth']; ?>',$(this).find(':selected').data('id'),'<?php echo $code?>')">
                                    <option value="">--Select--</option>

                                <?php foreach ($options as $key => $value) {
                                    ?>
                                        <option value="<?php echo isset($value['oid']) ? $value['oid'] : "" ?>" data-id="<?php echo isset($value['option_code']) ? $value['option_code'] : "" ?>"><?php echo isset($value['description']) ? $value['description'] : "" ?></option>


                                <?php
                                } ?> 
                                </select>
                            </div>
                        </div>
                    </div>
    <?php
} elseif (isset($question['type']) && $question['type'] == "radio") {
    foreach ($options as $key => $value) {
        ?>

                        <div class="flex-row form-option-wrap">
                            <div class="flex-col">
                                <div class="th-checkbox">
                                    <input type="hidden" name="desc[]" value="<?php echo isset($value["description"]) ? $value["description"] : "" ?>">
                                    <input style="display:none;" class="filter-type filled-in" type="radio" name="answer[]" id="Yes<?php echo $key; ?>" value="<?php echo isset($value['oid']) ? $value['oid'] : ""; ?>">
                                    <label for="Yes<?php echo $key; ?>" class="lbl-check"><span></span><?php echo isset($value['description']) ? $value['description'] : "" ?></label>
                                </div>
                            </div>

                        </div>
                    <?php
    } ?>    
<?php
} elseif (isset($question['type']) && $question['type'] == "text") {
    ?>

<div class="flex-row form-option-wrap">
    <div class="flex-col">
<div class="">
                                        
<input class="form-control formrequired" type="text" name="answer[<?php echo isset($question['qid']) ? $question['qid'] : 0 ?>][]" autocomplete="off" onkeyup="return onTextBoxKeyUp('<?php echo $code; ?>',<?php echo $question['depth']; ?>,this.value,'<?php echo $question['qid']; ?>')" required>
                                       
</div>
    </div>

</div>
                                 
<?php
} elseif (isset($question['type']) && $question['type'] == "textarea") {
    ?>
<div class="flex-row form-option-wrap">
<div class="flex-col">
<div class="">
<textarea class="form-control"  name="answer[<?php echo isset($question['qid']) ? $question['qid'] : 0 ?>][]" onkeyup="return onTextBoxKeyUp('<?php echo $code; ?>',<?php echo $question['depth']; ?>,this.value,'<?php echo $question['qid']; ?>')"></textarea>
                                       
</div>
</div>

    </div>
                                 
    <?php
} ?>
          <div class="clearfix"></div>
            <div class="next_question_<?php echo $question['qid'];?>"></div>                
                <div class="flex-row">

                    <div class="flex-col-sm-10 text-center m-t-sm">
                        <?php if (isset($count) && $count > 1) { ?>
                            <!-- <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton">Next </button> -->

                        <?php
                        } elseif (isset($count) && $count == 1) { ?>
                            <button type="submit" style="display:none" class="commn-btn save applyFilterUser" id="filterbutton">Submit</button>
                        <?php
} ?>    
                    </div>

                </div>
                <?php echo form_close(); ?>
                        <?php
            } ?>
                <?php
            } else { ?>
                <h2> No questions found. </h2>

            <?php
            } ?>
        </div>
        <style>
            .inner-wrapper {
                background: #fff;
                padding: 15px 15px 20px;
                margin: 15px auto;
                width: 95%;
                border-radius: 5px;
                box-shadow: 0 0 12px #00000014;
            }
            
            
 .flex-row {
    display: flex;
    margin: 0 -10px;
     margin-bottom: 0px;
    flex-wrap: wrap;
}

 .flex-row .flex-col {
    padding: 0 10px;
    margin: 0 0 14px 0;
}

            [type="checkbox"].filled-in:not(:checked)+label:after {
                height: 20px;
                width: 20px;
                background-color: transparent;
                border: 1px solid #d2d1d1;
                top: -3px;
                z-index: 0;
                border-radius: 3px;
            }

            [type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
                position: relative;
                padding-left: 31px;
                cursor: pointer;
                display: inline-block;
                line-height: 24px;
                font-size: 14px;
                transition: .28s ease;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                font-weight: 400;
                color: #363636;
                font-weight: 400;
            }

            .form-title {
                text-align: center;
                color: #1199d8;
                text-transform: uppercase;
                font-size: 20px;
                margin: 0 0 20px 0;
            }


            .text-center{
                text-align: center;
            }

            select.form-control.select-form {
                border: none;
                border-bottom: 1px solid #000;
                margin: 0 0 10px;
            }
            select {
                -moz-appearance: none;
                -webkit-appearance: menulist;
            }
        </style>

    </body>
</html>


<script>
    $(document).ready(function () {

        // $.validator.addMethod("answer", function (value, elem, param) {
        //     var msg = "";

        //     console.log(elem.nodeName);
        //     if (elem.nodeName == "SELECT") {
        //         msg = "Please select atleast one option from drop down.";
        //         return $(".answer:checkbox:checked").length > 0;

        //     } else if (elem.nodeName == "INPUT") {

        //         msg = "You must select atleast one checkbox.";
        //         return $(".answer:checkbox:checked").length > 0;
        //     }

        // }, "You must select atleast one option.");


        // $.validator.setDefaults({
        //     /*OBSERVATION: note how the ignore option is placed in here*/
        //     ignore: ':not(select:hidden, input:visible, textarea:visible):hidden:not(:checkbox)',

        //     /*...other options omitted to focus on the OP...*/

        //     errorPlacement: function (error, element) {
        //         if (element.hasClass('selectpicker')) {
        //             error.insertAfter(element);
        //         } else if (element.is(":checkbox")) {
        //             // element.siblings('span').hasClass('.check_error_msg').append(error);

        //             error.insertAfter($('.check_error_msg'));
        //         } else {
        //             error.insertAfter(element);
        //         }
        //     }
        // });

        // $("#survey-form").validate({
        //     errorClass: "alert-danger",
        //     rules: {
        //         "answer[][]": {required: true},
        //         "textanswer":{required:true}
        //     },
        //     messages:{
        //         "answer[]":{required:"Please select atleast one option"},
        //         "answer":{required:"Please select atleast one option"},
                
        //     },
        //     submitHandler: function (form) {
        //         form.submit();
        //     }

        // });
        $('#survey-form').validate();

    var user = $('input[name^="answer"]');

    user.filter('select[name$="[answer]"]').each(function() {
        $(this).rules("add", {
            required: true,
            messages: {
                required: "Name is Mandatory"
            }
        });
    });
    });

    function onTextBoxKeyUp(formCode, depth , value,questionId) {

        if(value.length >= 1) {
            var dataString = {code: formCode, depth: depth};
            questionAjax(dataString,questionId);
        }else {
            $('.save').hide();
        }

     }

     function questionAjax(arrayData,id) {
         if(arrayData !='') {
            $.ajax({
                type: "POST",
                url: "Survey/getNextQuestion",
                data:  arrayData,
                success: function(respdata)
                {
                    var Obj = JSON.parse( respdata );

                    if ( Obj.code == 200 ) {
                        if(Obj.removeNext == 2) {
                            $('.next_question_'+id+'').html('')
                        }
                        if(Obj.question.length === 0){
                            $('.save').show();
                        } else{
                            $('.save').hide();
                        }
                        $.each( Obj.question, function ( i, item ) {
                             if(item.type == 'checkbox') {
                                createCheckboxHtml(item,Obj.formCode,id)
                             }
                             if(item.type == 'dropdown') {
                                createDropdownHtml(item,Obj.formCode,id)
                             }
                             if(item.type == 'text') {
                                createTextboxHtml(item,Obj.formCode,id)
                             }
                             if(item.type == 'textarea') {
                                createTextareaboxHtml(item,Obj.formCode,id)
                             }
                             
                        } );
                     }
                } 
            });
         }
       
     }
//create checkbox html
     function createCheckboxHtml(data,formcode,id) {
        var html = '<div class="flex-row form-option-wrap">'
                   +'<div class="flex-col">'
                      +  '<input type="hidden" name="qid[]" value="'+data.qid+'">'
                + '<input type="hidden" name="parentQusId" value="'+data.parent_qid+'">'
                 +       '<p>Q:-'+data.text+'</p>'
                  +      '<label class="check_error_msg"></label>'
                   + '</div>'

                +'</div>';  

                html+= '<div class="flex-row form-option-wrap">';
                $.each( data.options, function ( i, oitem ) {
                      html+=('<div class="flex-col">'
                                 +'<div class="th-checkbox">'
                                  +'<input type="hidden" name="desc[]" value="'+oitem.description+'">'
                                 +'<input  style="display:none;" required class="filter-type filled-in" type="radio" name="answer['+data.qid+'][]" id="Yes'+oitem.oid+'" value="'+oitem.oid+'" onclick=getNext("'+formcode+'","'+data.depth+'","'+oitem.option_code+'","'+data.qid+'")>'
                                +'<label for="Yes'+oitem.oid+'" class="lbl-check"><span></span>'+oitem.description+'</label>'
                                +'</div>'
                            +'</div>');
                });

                html +='</div> <div class="next_question_'+data.qid+'"></div>';
                $('.next_question_'+id+'').html(html)
     }
//for checkbox
     function getNext(code,depth,optioncode,questionID){
        var dataString = {code: code, depth: depth,optionCode:optioncode,questionID:questionID};
            questionAjax(dataString,questionID);
     }
//for drop down
     function createDropdownHtml(data,formcode,id) {
        var html = '<div class="flex-row form-option-wrap">'
                   +'<div class="flex-col">'
                      +  '<input type="hidden" name="qid[]" value="'+data.qid+'">'
                + '<input type="hidden" name="parentQusId" value="'+data.parent_qid+'">'
                 +       '<p>Q:-'+data.text+'</p>'
                  +      '<label class="check_error_msg"></label>'
                   + '</div>'

                +'</div>';  

                html+= '<div class="flex-row form-option-wrap">';
                html+=('<div class="flex-col-sm-10">'
                                 +'<div class="">'
                                  +'<select  required name="answer['+data.qid+'][]" class="form-control select-form" onchange=getNextDependentQ(this.value,"'+data.qid+'","'+data.depth+'",$(this).find(":selected").data("id"),"'+formcode+'")>'
                                  +'<option value="">--Select--</option>');
              if(data.options !== null){
                $.each( data.options, function ( i, oitem ) {
                      html+=('<option value="'+oitem.oid+'" data-id="'+oitem.option_code+'">'+oitem.description+'</option>');  
                });
            }

                html +='</select></div></div>'
                            +'</div></div> <div class="next_question_'+data.qid+'"></div>';
                $('.next_question_'+id+'').html(html);
     }
     //select
    function getNextDependentQ(selectedvalue,questionId,depth,optionCod,formcod) {
        var dataString = {code: formcod, depth: depth,optionCode:optionCod,questionID:questionId};
            questionAjax(dataString,questionId);
    }
//create textbox html
    function createTextboxHtml(data,formcode,id) {
        var html = '<div class="flex-row form-option-wrap">'
                   +'<div class="flex-col">'
                      +  '<input type="hidden" name="qid[]" value="'+data.qid+'">'
                + '<input type="hidden" name="parentQusId" value="'+data.parent_qid+'">'
                 +       '<p>Q:-'+data.text+'</p>'
                  +      '<label class="check_error_msg"></label>'
                   + '</div>'

                +'</div>';  

                html +='<div class="flex-row form-option-wrap"> <div class="flex-col"> <div class="th-checkbox">'
                             +'<input class="form-control formrequired" required type="text" name="answer['+data.qid+'][]" autocomplete="off" onkeyup=onTextBoxKeyUp("'+formcode+'","'+data.depth+'",this.value,"'+data.qid+'")>'
                            +'</div></div> </div><div class="next_question_'+data.qid+'"></div>';
                $('.next_question_'+id+'').html(html);
     }
     //create textbox html
    function createTextareaboxHtml(data,formcode,id) {
        var html = '<div class="flex-row form-option-wrap">'
                   +'<div class="flex-col">'
                      +  '<input type="hidden" name="qid[]" value="'+data.qid+'">'
                + '<input type="hidden" name="parentQusId" value="'+data.parent_qid+'">'
                 +       '<p>Q:-'+data.text+'</p>'
                  +      '<label class="check_error_msg"></label>'
                   + '</div>'

                +'</div>'; 


                html +='<div class="flex-row form-option-wrap"> <div class="flex-col"> <div class="th-checkbox">'
                             +'<textarea  required class="form-control"  name="answer['+data.qid+'][]" autocomplete="off" onkeyup=onTextBoxKeyUp("'+formcode+'","'+data.depth+'",this.value,"'+data.qid+'")></textarea>'
                            +'</div></div> </div><div class="next_question_'+data.qid+'"></div>';
                $('.next_question_'+id+'').html(html);
     }

     //for enter
     $(document).keypress(function(e) {
        if(e.which == 13) {
            var flag = false;
            if ( $('.applyFilterUser').css('display') == 'none' || $('.applyFilterUser').css("visibility") == "hidden"){
                $('#survey-form').validate();
            return false;
              // element is hidden
            }
            // $('.formrequired').filter(function() {
            //     if (this.value != '') {
            //     flag = true;
            //     //no need to iterate further
            //     return false;
            //     }
            // });

            // if (!flag) {
            //     alert('empty');
            //     return false;
            // }
          
        }
});
</script>

<style>
    #survey-form .flex-row .flex-col {
    padding: 0 10px;
    width: 100%;
    margin: 0;
}
input[type="text"], input[type="password"], input[type="email"], select {
    border: 1px solid #807d7d;
}
#survey-form .flex-row.form-option-wrap {
    /* margin-bottom: 10px; */
}
p {
    margin: 0;
}
.next_question_43 {
    margin: 10px 0 0;
}
.next_question_44 .flex-row.form-option-wrap {
    margin: 5px -10px;
}
</style>    