<html>
<head>
 <title>Stalin Ani</title>   
</head>
<base href="<?php echo base_url(); ?>">
<link href="<?php echo base_url(); ?>public/css/style.css" rel='stylesheet'>
<link href="public/css/bootstrap.min.css" rel='stylesheet'>
<link href="public/css/plugin/bootstrap-select.min.css" rel='stylesheet'>

<script src="public/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>public/js/jquery.validate.min.js"></script>


<body>



    <div class="inner-wrapper">

        <h1 class="form-title">Stalin Ani</h1>
        <?php echo form_open(base_url()."survey-form", array("id"=>"mobile-orp-form"));?>
        <div class="flex-row form-option-wrap">
                <div class="flex-col">
                    <p>Q1. Please enter the mobile number to start the survey.</p>
                 </div>
         </div>           
        
        
            <div class="">
                
                <input type="hidden" value="<?php echo isset($formid)?$formid:""?>" name="formid" id="formid">
                <input type="hidden" value="<?php echo isset($type)?$type:""?>" name="type">
                <input type="hidden" value="<?php echo isset($user_id)?$user_id:""?>" name="user_id" id="user_id">
                <input type="hidden" value="<?php echo isset($task_id)?$task_id:""?>" name="task_id" id="task_id">
                <input type="hidden" value="<?php echo isset($news_id)?$news_id:""?>" name="news_id" id="news_id">

                <input type="text"  maxlength ="10" onkeypress="return numeralsOnly(event)" autocomplete="off" placeholder="Phone Number" class="answerbox" name="mobile"> 
                <label class="registered_err" style='color:red'></label>
            </div>
   

           <div class="flex-row">
                <div class="flex-col-sm-10 text-center m-t-sm">
                        <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton">Submit</button>
                        
                </div>
           </div>
        
        <?php echo form_close(); ?>
        
     </div>




     <style>
      .inner-wrapper {
    background: #fff;
    padding: 15px 15px 20px;
    margin: 15px auto;
    width: 600px;
    border-radius: 5px;
    box-shadow: 0 0 12px #00000014;
}

        [type="checkbox"].filled-in:not(:checked)+label:after {
    height: 20px;
    width: 20px;
    background-color: transparent;
    border: 1px solid #d2d1d1;
    top: -3px;
    z-index: 0;
    border-radius: 3px;
}

[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 31px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 24px;
    font-size: 14px;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-weight: 400;
    color: #363636;
    font-weight: 400;
}

.form-title {
    text-align: center;
    color: #1199d8;
    text-transform: uppercase;
    font-size: 20px;
    margin: 0 0 20px 0;
}


.text-center{
    text-align: center;
}

select.form-control.select-form {
    border: none;
    border-bottom: 1px solid #000;
    margin: 0 0 10px;
}
select {
    -moz-appearance: none;
    -webkit-appearance: menulist;
}

@media (max-width : 766px){
       .inner-wrapper {
    width: 95%;
}
}
     </style>

</body>
</html>
<script>

    
    
    function numeralsOnly(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
            ((evt.which) ? evt.which : 0));
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
$(document).ready(function(){
    $.validator.addMethod("uniquePhoneName", function(value, element) {
        var isSuccess = false;

  $.ajax({
      type: "POST",
       url: "check/registered-user",
       async: false,
      data:  {mobile: value, task_id: $("#task_id").val(),user_id:$("#user_id").val(),formid:$("#formid").val()},
   success: function(msg)
   {
      // if the user exists, it returns a string "true"
      if(msg == "false"){
        isSuccess= false;  // already exists
      } else {
        isSuccess = true;      // username is free to use

      }
   }
 });
    return isSuccess;
 }, "Registered mobile number is not allow");
   $("#mobile-orp-form").validate({
    errorClass: "alert-danger",
    rules: {
       mobile:{
           required:true,
           number:true,
           remote: {
            url: "check/survey-user",
            type: "post",
            data: {
              task_id: function() {
                return $("#task_id").val();
              },
              user_id: function() {
                return $("#user_id").val();
              },
               formid: function() {
                return $("#formid").val();
              },
              news_id: function() {
                return $("#news_id").val();
              },
            }
         },
         uniquePhoneName:true,
       
       },
    },
    messages:{
        mobile:{
            remote:'Mobile number already used for survey'
        }
    },
    submitHandler: function (form) {
       // check_mobile();
        form.submit()
    }
    
}); 
    
});

var check_mobile = function () {
    var isSuccess = false;

    return  $.ajax({
        type: "POST",
        url: "survey/checkMobileNumber",
        data:  {mobile: $("input[name=mobile]").val(), task_id: $("#task_id").val(),user_id:$("#user_id").val(),formid:$("#formid").val(),news_id:$("#news_id").val()},
        success: function(msg)
        {
           if(msg == 'false') {
             $('.registered_err').text('Registered mobile number is not allow');

           } else{
                 $('#mobile-orp-form').submit();
           }

        }
        
    });
}

</script>

