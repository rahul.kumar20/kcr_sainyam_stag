<html>
<head>
 <title>Campaign 4 India</title>   
</head>
<base href="<?php echo base_url(); ?>">
<link href="<?php echo base_url(); ?>public/css/style.css" rel='stylesheet'>
<link href="public/css/bootstrap.min.css" rel='stylesheet'>
<link href="public/css/plugin/bootstrap-select.min.css" rel='stylesheet'>

<script src="public/js/jquery.min.js"></script>
<script src="<?php echo base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>public/js/jquery.validate.min.js"></script>


<body>



    <div class="inner-wrapper">

        <h1 class="form-title">Campaign 4 India </h1>
        
        <label id="error">
                    <?php $alertMsg = $this->session->flashdata('message'); ?>
                    <div class="alert alert-danger" <?php echo (!(isset($alertMsg) && !empty($alertMsg)))?"style='display:none'":"" ?> role="alert">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <strong>
                            <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg))?$alertMsg:"" ?></span>
                        </strong>
                        <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg))?$alertMsg:"" ?></span>
                    </div>
       </label>
        
        <?php echo form_open(base_url()."verify-survey?code=".$code,array("id"=>"mobile-orp-form"));?>
        <div class="flex-row form-option-wrap">
                <div class="flex-col">
                    <p>Please enter the 4 digit OTP.</p>
                 </div>
         </div>           
        
        
            <div class="">
                
                <input type="hidden" value="<?php echo isset($code)?$code:""?>" name="code">
                <input type="text"  maxlength ="4" onkeypress="return numeralsOnly(event)" placeholder="OTP" class="answerbox" name="otp"> 
           </div>
   

           <div class="flex-row">
                <div class="flex-col-sm-10 text-center m-t-sm">
                        <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton">Submit</button>
                        
                </div>
           </div>
        
        <?php echo form_close(); ?>
        
     </div>




     <style>
      .inner-wrapper {
    background: #fff;
    padding: 15px 15px 20px;
    margin: 15px auto;
    width: 600px;
    border-radius: 5px;
    box-shadow: 0 0 12px #00000014;
}

        [type="checkbox"].filled-in:not(:checked)+label:after {
    height: 20px;
    width: 20px;
    background-color: transparent;
    border: 1px solid #d2d1d1;
    top: -3px;
    z-index: 0;
    border-radius: 3px;
}

[type="radio"]:not(:checked)+label, [type="radio"]:checked+label {
    position: relative;
    padding-left: 31px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 24px;
    font-size: 14px;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    font-weight: 400;
    color: #363636;
    font-weight: 400;
}

.form-title {
    text-align: center;
    color: #1199d8;
    text-transform: uppercase;
    font-size: 20px;
    margin: 0 0 20px 0;
}


.text-center{
    text-align: center;
}

select.form-control.select-form {
    border: none;
    border-bottom: 1px solid #000;
    margin: 0 0 10px;
}
select {
    -moz-appearance: none;
    -webkit-appearance: menulist;
}

@media (max-width : 766px){
       .inner-wrapper {
    width: 95%;
}
}
     </style>

</body>
</html>
<script>

    
    
    function numeralsOnly(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
            ((evt.which) ? evt.which : 0));
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
$(document).ready(function(){
    
   $("#mobile-orp-form").validate({
    errorClass: "alert-danger",
    rules: {
       otp:{required:true},
    },
    submitHandler: function (form) {
                    form.submit();
    }
    
}); 
    
});

</script>

