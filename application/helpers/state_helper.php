<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (! function_exists('get_state_list')) {

    /**
     * @function get_state_list
     * @description function to fetch state list
     *
     * @return array
     */
    function get_state_list()
    {

        $obj       = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $stateList = $obj->Common->fetch_data('state_list', '*');
        return $stateList;
    }



}

if (! function_exists('get_district_list')) {

    /**
     * @function get_district_list
     * @description function to fetch district list for given state
     *
     * @return array
     */
    function get_district_list($stateId)
    {
        $obj       = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $where = array('where' => array('state_id' =>$stateId));
        $districtList = $obj->Common->fetch_data('district', '*', $where);
       
        return $districtList;
    }
}

if (! function_exists('get_country_list')) {

    /**
     * @function get_country_list
     * @description to fetch country list
     *
     * @return array
     */
    function get_country_list()
    {

        $obj         = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $countryList = $obj->Common->fetch_data('countries', '*');
        return $countryList;
    }

}
if (! function_exists('get_form_list')) {

    /**
     * @function get_form_list
     * @description function to fetch form list
     *
     * @return array
     */
    function get_form_list()
    {

        $obj       = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $stateList = $obj->Common->fetch_data('forms', '*');
        return $stateList;
    }



}

if (! function_exists('get_state_name')) {

    /**
     * @function get_state_name
     * @description function to fetch state name
     *
     * @return array
     */
    function get_state_name($stateId)
    {

        $obj       = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $where = array('where' => array('state_id' =>$stateId));

        $stateDetail = $obj->Common->fetch_data('state_list', 'state_name', $where, true);
        return $stateDetail['state_name'];
    }
}

if (! function_exists('get_district_name')) {

    /**
     * @function get_district_name
     * @description function to fetch district name
     *
     * @return array
     */
    function get_district_name($districtId)
    {

        $obj       = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $where = array('where' => array('district_id' =>$districtId));

        $districtDetail = $obj->Common->fetch_data('district', 'district_name', $where, true);
        return $districtDetail['district_name'];
    }
}

if (! function_exists('get_ac_name')) {

    /**
     * @function get_ac_name
     * @description function to fetch ac name
     *
     * @return array
     */
    function get_ac_name($acId)
    {

        $obj       = & get_instance();
        $obj->load->model('Common_model', 'Common');
        $where = array('where' => array('id' =>$acId));

        $districtDetail = $obj->Common->fetch_data('tbl_ac', 'ac', $where, true);
        return $districtDetail['ac'];
    }
}
