<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}


if (! function_exists("validate_admin_cookie")) {

    /**
     * @function validate_admin_cookie
     * @description To check is cookie set in browser and having valid data to login admin
     *
     * @param string $cookieName admin cookie name
     * @param string $tableName table name
     * @return array | boolean
     */
    function validate_admin_cookie($cookieName, $tableName)
    {
        $CI            = &get_instance();
        $CI->load->model("Utility_model");
        $sessionFields = [
            "admin_id",
            "admin_name",
            "admin_email",
            "admin_profile_pic",
            "admin_profile_thumb",
            "role_id"
        ];

        $dataFields = [
            "admin_id",
            "admin_name",
            "admin_email",
            "admin_profile_pic",
            "admin_profile_thumb",
            "role_id"
        ];
        $cookieCookieData = $CI->Utility_model->validateCookie($cookieName, $tableName, $sessionFields, $dataFields);
        return $cookieCookieData;
    }



}

if (! function_exists("validate_user_cookie")) {

    /**
     * @funciton validate_user_cookie
     * @description To check is cookie set in browser and having valid data to login user
     *
     * @param string $cookieName user cookie name
     * @param string $tableName user table name
     * @return array | boolean
     */
    function validate_user_cookie($cookieName, $tableName)
    {
        $CI            = &get_instance();
        $CI->load->model("Utility_model");
        $sessionFields = [
            "id",
            "name",
            "email"
        ];
        $dataFields    = [
            "user_id",
            "first_name",
            "email"
        ];

        $cookieCookieData = $CI->Utility_model->validateCookie($cookieName, $tableName, $sessionFields, $dataFields, 'registered_date');

        return $cookieCookieData;
    }



}
