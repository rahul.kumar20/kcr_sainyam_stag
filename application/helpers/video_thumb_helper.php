<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH.'composer/vendor/autoload.php';



if (! function_exists("create_video_thumb")) {
    function create_video_thumb($url)
    {
      
        $ffprobe = FFMpeg\FFProbe::create();       
        $duration = $ffprobe
        ->format($url['0']) // extracts file informations
        ->get('duration');
        $ffmpeg = FFMpeg\FFMpeg::create();
        $video = $ffmpeg->open($url['0']);
        $thumbUrl = UPLOAD_THUMB_IMAGE_PATH.time() . '.jpg';
        $finalDuration = $duration < 1 ? 0 : 1;
        // $video
        // ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($duration/3))
        // ->save($thumbUrl);
        $video
        ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($finalDuration))
        ->save($thumbUrl);

        return $thumbUrl;
    }
}
