<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @function sendMessage
 * @description send message to user phone no
 *
 * @param mobile number and message
 * @return boolean
 */
if (!function_exists("sendMessage")) {
    function sendMessage($user_mobile_no, $message,$tid='',$unicode = '')
    {

        $no = (string) $user_mobile_no;
        $no = str_replace("+", "", $no);
        $msg = $message;
        // $msg = urlencode($msg);

        $url = SMS_GATEWAY_URL;
        $postData               = array(
            'accesskey'         => SMS_GATEWAY_ACCESSKEY,
            'from'              => SMS_FROM,
            'to'                => $no,
            'text'              => $msg,
            'url'               => $url,
            'unicode'           => 0
        );
        if($tid){
            $postData['tid']    = $tid;
        }
        if($unicode == 1){
            $postData['unicode']= $unicode;
        }
        $postString = json_encode($postData);
        sendPostRequest($postData);
    }
}

if (!function_exists("sendPromoMessage")) {
    function sendPromoMessage($user_mobile_no, $message)
    {

        $no = (string) $user_mobile_no;
        $no = str_replace("+", "", $no);
        $msg = $message;
        // $msg = urlencode($msg);

        $url = 'https://mobilnxt.in/api/push';
        $postData = array(
            'accesskey' => 'kkMD8QCvSQiLXtF89SqSIvDnxjFbBv',
            'from' => 'STALIN',
            'to' => $no,
            'text' => $msg,
            'url' => $url,
            'unicode' => 1
        );
        $postString = json_encode($postData);
        sendPostRequest($postData);
    }
}

if (!function_exists("sendIVR")) {
    function sendIVR($phone_no, $code)
    {
        $IVRUrl = "http://14.99.70.194:8121/vicidial/non_agent_api.php?source=test&user=manager&pass=manager&function=add_lead&phone_number=$phone_no&phone_code=0&list_id=$code";
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $IVRUrl);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        $res2 = curl_exec($ch2);
        $httpCode2 = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
    }
}
