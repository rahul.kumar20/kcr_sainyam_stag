
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Goa Deserves Better</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; background:#eaf2ff; color:#333333;padding:15px 15px 15px 15px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#fbfcff" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top" style="text-align:center;background:#b6b9bf;">
                                        <a href="https://ondrinaivomvaa.in" style="text-decoration:none;color:#333333;display:inline-block;padding: 10px;" target="_blank">
                                            <img src="<?php echo base_url(); ?>public/images/Vector.png" alt="logo" style="width:150px;" />
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#323848;font-size:20px;font-weight:500;text-transform:capitalize;padding: 30px 0 25px 34px;">
                                        Hi <?php echo $name; ?>,
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="color:#363636;font-size:16px;font-weight:600;text-transform:uppercase;line-height:1.3;padding: 0 0 0 34px;">
                                        
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                    You recently asked to reset your Stalin Ani password.  
<a href="<?php echo $link ?> " style="text-decoration:none; color:#f16725;"><strong> Click here </strong></a> to reset your password. 
                                    </td>
                                </tr>

<!--                                <tr>
                                    <td align="center" valign="top" style="text-align:center;padding-top:20px;padding-bottom:40px;" class="social">
                                        <a href="javascript:void(0)" style="display:inline-block;padding-right:5px;">
                                            <img src="appstore.png" alt="App Store" />
                                        </a>
                                        <a href="javascript:void(0)" style="display:inline-block;">
                                            <img src="googleplay.png" alt="Google Play" />
                                        </a>
                                    </td>
                                </tr>-->

<!--                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    </td>
                                </tr>-->

                            </table>
                        </td>
                        
                    </tr>

                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#fbfcff" style="padding-left:33px;padding-right:33px;" class="signup-detail">
                              

                                <tr>
                                    <td valign="top" style="color: #4c4f59; font-size: 14px; padding-bottom: 5px; line-height: 1.67;">
                                      Incase of any queries, feel free to contact us at <a href="mailto:info@stalinani.in">info@stalinani.in</a>  
                                    </td>
                                </tr>

                                <tr>
                                    <td style="padding:.75pt .75pt .75pt .75pt">
                                       <p style="margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:13.5pt"><span style="    font-size: 12.0pt;
                                          font-weight: 600;">Regards <br> Team Stalin Ani</span></p>
                                    </td>
                                 </tr>

                                <tr>
                                    <td valign="top">
                                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="text-align:center;">

                                         

                                            <tr>
                                                <td align="center" valign="top" style="text-align:center;padding-bottom:20px;" class="social-links">
                                                    <a href="https://www.facebook.com/MKStalin" style="display:inline-block;padding-right:10px;">
                                                        <img src="<?php echo base_url(); ?>public/images/facebook.png" alt="Facebook" />
                                                    </a>
                                                    <a href="https://twitter.com/mkstalin" style="display:inline-block;padding-right:10px;">
                                                        <img src="<?php echo base_url(); ?>public/images/twitter.png" alt="Twitter" />
                                                    </a>
                                                   
                                                    <a href="https://instagram.com/mkstalin" style="display:inline-block;padding-right:10px;">
                                                    <img src="<?php echo base_url(); ?>public/images/instagram.png" alt="Instagram" />
                                                  </a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="center" valign="top" style="text-align:center;font-size:11px;color:#4c4f59;">
                                                    Copyright © 2020 Ondrinaivomvaa. All Rights Reserved
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="padding-top:5px;text-align:center;font-size:11px;color:#4c4f59;">
                                                   <a href="https://ondrinaivomvaa.in" target="_blank">website: ondrinaivomvaa.in</a>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

    </table>

</body>

</html>