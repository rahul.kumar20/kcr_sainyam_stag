<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Notification extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('Notification_model');
    }

    /**
     * @SWG\Get(path="/Notification",
     *   tags={"Notification"},
     *   summary="Get user notification information",
     *   description="Get news added,new task added notification",
     *   operationId="index_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_get() {
        try {
            # load session model
            $this->load->model("Session");

            $params = $this->get();
            $count = isset($params['count']) ? $params['count'] : 0;
            $where['limit']  = PAGE_LIMIT;
            $where['offset'] =  $count ;
            $response_array = array();
            $userId = $GLOBALS['api_user_id'];
            $where['user_id'] = $userId;
            $end_date = date('Y-m-d H:i:s',time());
            $start_date = date('Y-m-d H:i:s', strtotime('-2 day', strtotime($end_date)));
              /**
             * next count
             */
            $this->next_count = $where['offset'] + $where['limit'];
            
            $wherecond = array('where' => array('user_id' => $userId));

            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data('users', 
                    ['gender', 'state','district','registeration_no','college','is_active','user_id'], $wherecond, true);
           
            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
            
                    #fetching user notification from db
                    $notificationdata = $this->Notification_model->fetch_user_notification($where,$userInfo,$start_date,$end_date);
                    
                    $notification  = $notificationdata['notification'];
                    $total_count = $notificationdata['totalcount'];

                    /**
                     * check is data remaining or not
                     */

                    if ($total_count <= $this->next_count) {
                        $this->next_count = '-1';
                    }
               
                    $updateArr          = array();
                    $updateArr['is_notification_received'] = '0';
                    $updateUserCond     = array('where' => array('user_id' => $userId));

                    $userInfo = $this->Common_model->update_single('users',$updateArr, $updateUserCond);
                    
                    if (isset($notification) && ! empty($notification)) {
                       $this->response(array ( 'CODE' => SUCCESS_CODE, 'MESSAGE' => 'success', 'RESULT' => $notification, 'NEXT' => $this->next_count, 'TOTAL' => $total_count));
                   } else {
                       $this->response(array ( "CODE" => NO_DATA_FOUND, 'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'), 'RESULT' => array () ));
                   }

                    
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => $empty_response
                    ];
                }
            }

            #sending Response
            $this->response($response_array);
        }#TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END
    }
    
    
     /**
      * @SWG\Put(path="/read_notification",
      *   tags={"UpdateProfile"},
      *   summary="Update user profile",
      *   description="Update profile of a user",
      *   operationId="index_put",
      *   consumes ={"multipart/form-data"},
      *   produces={"application/json"},
      *   @SWG\Parameter(
      *     name="Authorization",
      *     in="header",
      *     description="",
      *     required=true,
      *     type="string"
      *   ),
      *  @SWG\Parameter(
      *     name="accesstoken",
      *     in="header",
      *     description="Access token received during signup or login",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Response(response=200, description="Success"),
      *   @SWG\Response(response=206, description="Unauthorized request"),
      *   @SWG\Response(response=207, description="Header is missing"),
      *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
      * )
      */
     public function index_put() {
         try {
            # load session model
            $this->load->model("Session");

            $postDataArr = $this->put();
           
            $response_array = array();

            $userId = $GLOBALS['api_user_id'];

           $where = array('where' => array('user_id' => $userId,'is_read' => 0));

           $newdata = array('is_read' => 1);
            
            #start transaction
            $this->db->trans_begin();

            #updating profile details
            $isSuccess = $this->Common_model->update_single('user_notification', $newdata,  $where);

            #If query failed to update
            #Throw Exception
            if (!$isSuccess) {
                $this->db->trans_rollback();
                throw new Exception($this->lang->line('try_again'));
            }

            #if transaction runs successfully
            if (TRUE === $this->db->trans_status()) {

                #Comminting changes
                $this->db->trans_commit(); 
                #setting Response Array
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('success'),
                    'RESULT' => []
                ];
            } else {
                #IF transaction failed
                #rolling back
                $this->db->trans_rollback();

                #setting Response Array
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('try_again'),
                    'RESULT' => []
                ];
            }

            $this->response($response_array);
        }#TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END

     }

    
    

   

}
