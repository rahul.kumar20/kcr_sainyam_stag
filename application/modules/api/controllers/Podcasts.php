<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Podcasts extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index_get()
    {
        try {

            $whereArr = [];
            $table = 'podcast';
            $order = 'id';
            $whereArr['order_by'] = [$order => 'desc'];
            $whereArr['where'] = ['status' => '1', 'notification_status' => '1'];
            $whereArr['limit'] = ['0' => '1'];
            $listData = $this->Common_model->fetch_data($table, array("*"), $whereArr);

            #If Result list is not empty
            if (!empty($listData)) {
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('error'),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }
}
