<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/SNSPush.php';

class Login extends REST_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('email');
        $this->load->helper('sendsms');
    }

    /**
     * @SWG\Post(path="/Login",
     *   tags={"User"},
     *   summary="Login Information",
     *   description="Login Information",
     *   operationId="index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Password",
     *     required=true,
     *     type="string"
     *   ),
     *    @SWG\Parameter(
     *     name="device_id",
     *     in="formData",
     *     description="Unique Device Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_token",
     *     in="formData",
     *     description="Device Token required to send push",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="platform",
     *     in="formData",
     *     description="1: Android and 2: iOS",
     *     type="string"
     *   ),
     *   @SWG\Response(response=101, description="Account Blocked"),
     *   @SWG\Response(response=200, description="Login Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function index_post()
    {
        $postDataArr = $this->post();
        $empty_response = (object) array();

        #Setting Form validation Rules
        $config = array(
            array(
                'field' => 'phone_no',
                'label' => 'Phone number',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'login_type',
                'label' => 'Login Type',
                'rules' => 'trim|required',
            ),
        );

        $this->form_validation->set_rules($config);
        $this->form_validation->set_message('required', 'Please enter the %s');

        $response_array = [];

        #checking Form validation
        if ($this->form_validation->run()) {
            date_default_timezone_set('Asia/Kolkata');
            $phone_no = $postDataArr['phone_no'];
            $login_type = $postDataArr['login_type'];
            //Authenticate user
            $userInfo = $this->check_validation($postDataArr);

            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
                    $flag = 0;
                    #if login type is password check for password otherwise send otp on registered mobile
                    if ($login_type == 'password' && isset($postDataArr['password']) && !empty($postDataArr['password'])) {
                        $saved_password = createPassword($postDataArr['password']);
                        if ($userInfo['password'] == $saved_password) {
                            $flag = 1;
                        }
                    } elseif ($login_type == 'otp') {
                        $flag = 1;
                        # save otp details
                        $otp = generate_otp();
                        $registerArr['otp'] = $otp;
                        $registerArr['otp_sent_time'] = date('Y-m-d H:i:s');
                        $whereArr['where'] = ['user_id' => $userInfo['user_id']];
                        $sessionId = $this->Common_model->update_single('users', $registerArr, $whereArr);

                        #send otp to user
                        //sendMessage(COUNTRYCODE . $userInfo['phone_number'], $otp . $this->lang->line('login_otp_message'),SMS_LOGIN_MSG_TID);
                        $message                = 'Dear user, your OTP for GDB App is '.$otp.'. Use this passcode to validate your login, Thank you. Regards, All India Trinamool Congress';
                        sendMessage(COUNTRYCODE . $userInfo['phone_number'], $message,SMS_LOGIN_MSG_TID);
                    }
                    if ($flag == 1) {

                        #getting access Token
                        $accessToken = create_access_token($userInfo['user_id'], $userInfo['email_id']);

                        $postDataArr['user_id'] = $userInfo['user_id'];

                        $sessionArr = [];
                        #setting Session variable
                        $sessionArr = setSessionVariables($postDataArr, $accessToken);

                        #register device arn
                        if ((isset($postDataArr['device_token']) && !empty($postDataArr['device_token'])) && (isset($postDataArr['platform']) && !empty($postDataArr['platform']))) {
                            //if device token is set generate end point ern
                            $sns = new Snspush();
                            $response = $sns->addDeviceEndPoint($postDataArr['device_token'], $postDataArr['platform']);
                            if ($response["success"]) {
                                if ($response["success"]) {
                                    $sessionArr["end_point_ern"] = $response["result"]["EndpointArn"];
                                }
                            }
                        }

                        $this->load->library('common_function');
                        #If App Support Single Login
                        if (IS_SINGLE_DEVICE_LOGIN) {
                            $where = array('where' => array('user_id' => $userInfo['user_id']));

                            $logData                    = array();
                            $logData['where']           = $where;
                            $logData['updateData']      = $sessionArr;
                            $this->common_function->writeDeviceDetailsLog('update','api :: login >> index_post Before Updating  1',$logData);

                            $this->Common_model->update_single('user_device_details', $sessionArr, $where);

                            $logData                    = array();
                            $logData['fetchData']       = $this->Common_model->fetch_data('user_device_details', array('session_id', 'device_token'), $where, true);
                            $this->common_function->writeDeviceDetailsLog('update','api :: login >> index_post After Updating 1',$logData);

                        } else {
                            #If App Support Multiple Login
                            $whereArr = [];
                            $device_id = isset($postDataArr['device_id']) ? $postDataArr['device_id'] : "";
                            $whereArr['where'] = ['device_id' => $device_id];

                            $isExist = $this->Common_model->fetch_data('user_device_details', array('session_id', 'device_token'), $whereArr, true);
                            /*
                             * If user has logged in previously with same device then update his detail
                             * or insert as a new row
                             */

                            #transaction Start
                            $this->db->trans_begin();

                            if (!empty($isExist)) {
                                #updating user session details if device id is same as previous
                                $logData                    = array();
                                $logData['where']           = $whereArr;
                                $logData['updateData']      = $sessionArr;
                                $logData['fetchData']       = $isExist;
                                $this->common_function->writeDeviceDetailsLog('update','api :: login >> index_post Before Updating',$logData);

                                $isSuccess = $this->Common_model->update_single('user_device_details', $sessionArr, $whereArr);

                                $logData                    = array();
                                $logData['fetchData']       = $this->Common_model->fetch_data('user_device_details','', $whereArr, true);
                                $this->common_function->writeDeviceDetailsLog('update','api :: login >> index_post After Updating ',$logData);
                            } else {
                                #inserting details if device id different
                                $this->common_function->writeDeviceDetailsLog('insert','api :: login >> index_post',$sessionArr);
                                $isSuccess = $this->Common_model->insert_single('user_device_details', $sessionArr);
                            }

                            #if updation/inserting failed
                            if (!$isSuccess) {
                                throw new Exception($this->lang->line('try_again'));
                            }
                        }
                        //logs array
                        $logsInsArray = array(
                            'user_id' => $userInfo['user_id'],
                            'active_date' => datetime(),
                            'device_id' => isset($postDataArr['device_id']) ? $postDataArr['device_id'] : "",
                            'type' => LOGIN,
                            'ip_address' => ip2long($_SERVER['REMOTE_ADDR']),
                        );
                        #updating logs details
                        $isSuccess = $this->Common_model->insert_single('user_session_logs', $logsInsArray);
                        #Checking Transaction status
                        if (true === $this->db->trans_status()) {
                            #commiting trasaction
                            $this->db->trans_commit();
                            unset($userInfo['password']);
                            $userInfo['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];

                            if ($login_type == 'otp') {
                                $code = SUCCESS_CODE;
                                $message = $this->lang->line('otp_sent');
                                $response_array = [
                                    'CODE' => $code,
                                    'MESSAGE' => $message,
                                    'RESULT' => array('otp' => $otp, 'user_id' => $userInfo['user_id'], 'accesstoken' => $userInfo['accesstoken']),
                                ];
                            } else {
                                #setting Response
                                $code = SUCCESS_CODE;
                                $message = $this->lang->line('otp_sent');
                                $response_array = [
                                    'CODE' => $code,
                                    'MESSAGE' => $message,
                                    'RESULT' => (object) $userInfo,
                                ];
                            }
                        } else {
                            $this->db->trans_rollback();

                            #setting response
                            $response_array = [
                                'CODE' => TRY_AGAIN_CODE,
                                'MESSAGE' => $this->lang->line('try_again'),
                                'RESULT' => (object) array(),
                            ];
                        }
                    } else {
                        #setting Response
                        $response_array = [
                            'CODE' => INVALID_CREDENTIALS,
                            'MESSAGE' => $this->lang->line('invalid_credentials'),
                            'RESULT' => $empty_response,
                        ];
                    }
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => $empty_response,
                    ];
                }
            } else { #unable to login/invalid credential
                #setting Response
                $response_array = [
                    'CODE' => INVALID_CREDENTIALS,
                    'MESSAGE' => $this->lang->line('not_registered'),
                    'RESULT' => $empty_response,
                ];
            }
        } else {
            $err = $this->form_validation->error_array();
            $arr = array_values($err);
            $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
        }

        #Sending Response
        echo json_encode($response_array);
        die;
        //$this->response($response_array);
    }

    /**
     * @name check_validation
     * @description This method is used to check the login credentials in LOGIN table
     * @param array
     * @return boolean
     */
    private function check_validation($data)
    {
        $response = null;
        $phone_no = $data['phone_no'];
        $login_where = array('where' => array('phone_number' => $phone_no));

        $userInfo = $this->Common_model->fetch_data(
            'users',
            [
                'user_id', 'registeration_no', 'referal_code', 'full_name', 'email_id', 'facebook_id',
                'twitter_id', 'user_image', 'whatsup_number', 'paytm_number', 'phone_number', 'state', 'district',
                'is_active', 'password', 'is_profile_edited', 'is_number_verified', 'fb_username', 'twitter_username', 'is_bonus_received', 'is_profile_verified',
            ],
            $login_where,
            true
        );
        return $userInfo;
    }
}
