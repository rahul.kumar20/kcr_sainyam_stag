<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class App_Installations extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }


    public function index_post()
    {
        try {
            $postData       = $this->input->post();
                       
            //if referal code and device id exists
            if (isset($postData['referal_code']) && !empty($postData['referal_code']) && isset($postData['device_id']) && !empty($postData['device_id'])) {

                // add installations
                $insertData         = array(
                    'referal_code'  => $postData['referal_code'],
                    'device_id'     => $postData['device_id']
                );
                $insertedId         = $this->Common_model->insert_single('app_installations', $insertData);
                if($insertedId > 0){
                    $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => "Record added successfully.", 'RESULT' => []));
                }else{
                    $this->response(array('CODE' => TRY_AGAIN_CODE, 'MESSAGE' => "Failure while adding record. Please try again.", 'RESULT' => []));
                }
            } else {
                throw new Exception($this->lang->line('try_again'));
            }
            
        } catch (Exception $e) {
            //exception
            $error = $e->getMessage();
            $this->response(array('CODE' => TRY_AGAIN_CODE, 'MESSAGE' => $error, 'RESULT' => []));
        }
    }
}
