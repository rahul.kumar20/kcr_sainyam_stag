<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/SNSPush.php';

class Task extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('User_model');
        $this->load->model('admin/Notification_model');
        $this->load->database();
        $this->load->model("Home_model", "home");
    }

    public function index_get()
    {
        try {
            $params = $this->get();
            $count = isset($params['count']) ? $params['count'] : 0;
            $startDate = date('Y-m-d');
            $currentDate = date('Y-m-d H:i:s');
            // $startDate = (isset($params['start_date']) && !empty($params['start_date']) ? date('Y-m-d', strtotime($params['start_date'])) : '');
            $platform = (isset($params['platform']) && !empty($params['platform']) ? $params['platform'] : '');
            $status = (isset($params['status']) && !empty($params['status']) ? $params['status'] : '');
            $task_id = (isset($params['task_id']) && !empty($params['task_id']) ? $params['task_id'] : '');

            $where['limit'] = PAGE_LIMIT;
            $where['offset'] = $count;
            $where['startDate'] = $startDate;
            /**
             * next count
             */
            $this->next_count = $where['offset'] + $where['limit'];

            $userId = $GLOBALS['api_user_id'];

            $wherecond = array('where' => array('user_id' => $userId));

            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data('users', [
                'user_id', 'is_active',
                'state', 'district', 'ac', 'gender', 'registeration_no', 'college',
                'requested_for_edit_profile', 'is_bonus_received', 'paytm_number', 'upi_address', 'account_number'
            ], $wherecond, true);
            $userInfo['task_id'] = $task_id;

            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
                    # fetch tasks data
                    $all_data = $this->home->fetch_task($where['limit'], $where['offset'], $where['startDate'], $userInfo, $platform, $status);

                    foreach ($all_data['task'] as $taskKey => $taskVal) {
                        $task_img = explode(",", $taskVal['task_image']);
                        $media_type = explode(",", $taskVal['task_media_type']);
                        $media_thumb = explode(",", $taskVal['task_thumb']);

                        $task_media_set = array();
                        foreach ($task_img as $imKey => $imVal) {
                            if (!empty($imVal) && isset($imVal)) {
                                $task_media_set[$imKey]['media_url'] = $task_img[$imKey];
                                $task_media_set[$imKey]['media_type'] = $media_type[$imKey];
                                $task_media_set[$imKey]['media_thumb'] = $media_thumb[$imKey];
                            }
                        }

                        unset($all_data['task'][$taskKey]['task_image']);
                        unset($all_data['task'][$taskKey]['task_media_type']);
                        unset($all_data['task'][$taskKey]['task_thumb']);
                        $all_data['task'][$taskKey]['task_media_set'] = $task_media_set;
                        $all_data['task'][$taskKey]['task_completed_user_count'] = $taskVal['task_completed_user_count'];
                        //$all_data['task'][$taskKey]['task_completed_user_list'] = $this->home->getCompletedTaskUserList($userInfo["user_id"], $taskVal['task_id']);
                        $all_data['task'][$taskKey]['task_completed_user_list'] = array();
                    }
                    $task = $all_data['task'];
                    $total_count = $all_data['totalcount'];

                    /**
                     * check is data remaining or not
                     */
                    if ($total_count <= $this->next_count) {
                        $this->next_count = '-1';
                    }
                    if ($task_id) {
                        $wheretaskCond = array('where' => array('user_id' => $userId, 'task_id' => $task_id, 'is_read' => 0));

                        $newdata = array('is_read' => 1);
                        #read notification
                        $isSuccess = $this->Common_model->update_single('user_notification', $newdata, $wheretaskCond);
                    }

                    //get term and condition detail
                    $where = array('where' => array('status' => ACTIVE));
                    $where['order_by'] = ['created_date' => 'desc'];

                    //if any platform filter is there
                    if (!empty($platform)) {
                        //if status is for task
                        if ($status == COMPLETE) {
                            $taskComplete = 'IFNULL(get_total_completed_task(DATE("' . $startDate . '"),"' . $platform . '",' . $userId . '),0) as  completed_task';
                        } elseif ($status == 3) {
                            //if status for task under review
                            $taskComplete = 'IFNULL(get_total_review_task(DATE("' . $startDate . '"),"' . $platform . '",' . $userId . '),0) as  completed_task';
                        } else {
                            //if no status filter
                            $taskComplete = 'IFNULL(get_total_completed_task(DATE("' . $startDate . '"),"' . $platform . '",' . $userId . '),0) as completed_task';
                        }
                    } else {
                        if ($status == 3) {
                            $taskComplete = 'IFNULL(get_total_review_task(DATE("' . $startDate . '"),"",' . $userId . '),0) as completed_task';
                        } else {
                            $taskComplete = 'IFNULL(get_total_completed_task(DATE("' . $startDate . '"),"",' . $userId . '),0) as completed_task';
                        }
                    }

                    //ter condition info
                    $termConditionInfo = $this->Common_model->fetch_data('ipac_term_condition', [
                        'version',
                        'get_term_condition_accept_status(' . $userId . ') as term_accept_status,
                        getTotalTaskByUser3("' . $currentDate . '",' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","' . $platform . '")
                    as total_task,get_campaign_form_status(' . $userId . ') as campaign_status,get_is_user_added_id(' . $userId . ') as id_proff_added,
                     ' . $taskComplete . ' '
                    ], $where, true);

                    if ($status == 2) {
                        $totalTaskComplete = $total_count;
                        $totalTask = isset($termConditionInfo['total_task']) ? count($termConditionInfo['total_task']) : 0;
                    } else {
                        $totalTaskComplete = isset($termConditionInfo['completed_task']) ? count($termConditionInfo['completed_task']) : 0;
                        $totalTask = isset($termConditionInfo['total_task']) ? count($termConditionInfo['total_task']) : 0;
                    }
                    //check if user added payment detail
                    $paymentDetailAdded = 0;
                    if (
                        !empty($userInfo['paytm_number']) || !empty($userInfo['upi_address'])
                        || !empty($userInfo['account_number'])
                    ) {
                        $paymentDetailAdded = 1;
                    }
                    if (isset($task) && !empty($task)) {
                        $this->response(
                            array(
                                'CODE' => SUCCESS_CODE,
                                'MESSAGE' => 'success', 'RESULT' => $task,
                                'NEXT' => $this->next_count,
                                'TOTAL' => $all_data['totalcount'],
                                'TOTAL_COMPLETE_TASK' => $totalTaskComplete,
                                'IS_PROFILE_EDITED' => $userInfo['requested_for_edit_profile'],
                                'TERM_CONDITION' => $termConditionInfo,
                                'IS_CAMPAIGN_FORM_FILLED' => isset($termConditionInfo['campaign_status']) ? count($termConditionInfo['campaign_status']) : '',
                                'CAMPAIGN_FORM_URL' => base_url() . 'start-campaign?type=2',
                                'PAYMENT_DETAIL_ADDED' => $paymentDetailAdded,
                                'IS_ID_PROOF_ADDED' => isset($termConditionInfo['id_proff_added']) ? count($termConditionInfo['id_proff_added']) : '',
                            )
                        );
                    } else {
                        $this->response(
                            array(
                                "CODE" => NO_DATA_FOUND, 'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'),
                                'RESULT' => array(), 'IS_PROFILE_EDITED' => $userInfo['requested_for_edit_profile'],
                                'TERM_CONDITION' => $termConditionInfo,
                                'TOTAL' => $all_data['totalcount'],
                                'TOTAL_COMPLETE_TASK' => !is_null($totalTaskComplete) ? $totalTaskComplete : 0,
                                'IS_CAMPAIGN_FORM_FILLED' => isset($termConditionInfo['campaign_status']) ? count($termConditionInfo['campaign_status']) : '',
                                'CAMPAIGN_FORM_URL' => base_url() . 'start-campaign?type=2',
                                'PAYMENT_DETAIL_ADDED' => $paymentDetailAdded,
                                'IS_ID_PROOF_ADDED' => isset($termConditionInfo['id_proff_added']) ? count($termConditionInfo['id_proff_added']) : '',
                            )
                        );
                    }

                    #sending Response
                    // $this->response($response_array);
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => [],
                    ];
                }
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function index_post()
    {
        try {
            $postDataArr = $this->post();
            #setting Form  validation Rules
            $unlock_message = '';
            $new_unlock = false;
            $required_fields_arr = array(
                array(
                    'field' => 'task_id',
                    'label' => 'Task Id',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('is_unique', 'The %s is already registered with us');
            $this->form_validation->set_message('required', 'Please enter the %s');

            if ($this->form_validation->run()) {
                $postDataArr = $this->post();

                $userId = $GLOBALS['api_user_id'];
                $taskId = isset($postDataArr['task_id']) ? ($postDataArr['task_id']) : '';
                $setPending = isset($postDataArr['set_pending']) ? ($postDataArr['set_pending']) : '';

                $whereArr['where'] = ['user_id' => $userId];

                #fetching user details from db
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    [
                        'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                        'points_earned', 'total_earning', 'task_completed', 'is_bonus_received',
                    ],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    /*
                     * Check if user is not blocksed
                     */
                    if (ACTIVE == $userInfo['is_active']) {
                        #get the task id based on the action performed by user
                        $task_cond_where = array('where' => array('task_id' => $taskId));
                        //task information
                        $taskInfo = $this->Common_model->fetch_data('ipac_task_master', ['task_id', 'task_status', 'start_date', 'end_date', 'points', 'task_type', 'action'], $task_cond_where, true);
                        //if task data is not empty
                        if ($taskInfo) {
                            $wherecond = array('where' => array('user_id' => $userId, 'task_id' => $taskId));
                            $taskCompleteInfo = $this->Common_model->fetch_data('user_task_master', ['detail_id', 'status'], $wherecond, true);
                            //if task is type whatsapp than complete by admin end only entry is there
                           // $taskInfo['task_type'] == 'twitter' || 
                            if($setPending == 1){
                               $completeStatus = PENDING;
                            } elseif($taskInfo['task_type'] == 'whatsapp' || $taskInfo['task_type'] == 'instagram' || ($taskInfo['task_type'] == 'offline' && ($taskInfo['action'] == 'download media' || $taskInfo['action'] == 'image' || $taskInfo['action'] == 'video')) || ($taskInfo['task_type'] == 'default' && $taskInfo['action'] == 'upload_whatsapp')) {
                                $completeStatus = PENDING;
                            } else {
                                $completeStatus = COMPLETE;
                            }
                            #start transaction
                            $this->db->trans_begin();
                            $date           = new DateTime("now");
                            $taskEndDate    = new DateTime($taskInfo['end_date']);
                            //task not complete
                            if (empty($taskCompleteInfo)) {
                                //insert in user task complete table
                                $taskCompleteInsert = array(
                                    'user_id' => $userId,
                                    'task_id' => $taskId,
                                    'type' => $taskInfo['task_type'],
                                    'status' => $completeStatus,
                                    'remarks' => isset($postDataArr['remarks']) ? $postDataArr['remarks'] : '',
                                    'complete_using_app' => 1,
                                    'complete_using_webhook' => 0,
                                    'added_on' => date('Y-m-d H:i:s'),
                                );
                               //insert in task complete
                                $isSuccess = $this->db->insert('user_task_master', $taskCompleteInsert); #save users details values in DB

                                //task type offline
                                if ($completeStatus == PENDING) {
                                    if (isset($postDataArr['media']) && !empty($postDataArr['media'])) {
                                        $proofImage = explode(",", $postDataArr['media']);

                                        foreach ($proofImage as $key => $val) {
                                            $proofData['user_id'] = $userId;
                                            $proofData['task_id'] = $taskId;
                                            $proofData['media_url'] = $val;
                                            $proofData['created_date'] = date('Y-m-d H:i:s');
                                            $proofId = $this->Common_model->insert_single('offline_task_tracking', $proofData);
                                        }
                                    }

                                    if (isset($postDataArr['lattitude']) && !empty($postDataArr['lattitude'])) {
                                        $proofData['user_id'] = $userId;
                                        $proofData['task_id'] = $taskId;
                                        $proofData['latitude'] = $postDataArr['lattitude'];
                                        $proofData['longitude'] = $postDataArr['longitude'];
                                        $proofData['created_date'] = date('Y-m-d H:i:s');
                                        $proofId = $this->Common_model->insert_single('offline_task_tracking', $proofData);
                                    }
                                }
                                //if task is not whats app
                                $now = now();
                                if ($taskEndDate <= $date) {
                                    // do ur stuff
                                    //updating user value
                                    $updateUserArray = array(
                                        'task_completed' => ($userInfo['task_completed'] + 1),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    );
                                    $whereUserArr['where'] = ['user_id' => $userId];
                                    $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
                                } else {
                                    if ($completeStatus == COMPLETE) {
                                        //earning inset block
                                        $earning_where = array('where' => array('task_id ' => $taskId, 'user_id' => $userId));
                                        $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                                        //earning block
                                        if (empty($earningInfo)) {
                                            # insert user earnings data
                                            $start_date = date('Y-m-01');
                                            $end_date = date('Y-m-t');
                                            //earning table
                                            $earningArr = array(
                                                'user_id' => $userId,
                                                'task_id' => $taskId,
                                                'type' => $taskInfo['task_type'],
                                                'month_start_date' => $start_date,
                                                'month_end_date' => $end_date,
                                                'total_earned_points' => $taskInfo['points'],
                                                'reward_payment_amt' => $taskInfo['points'],
                                                'reward_clear_status' => 0,
                                                'inserted_on' => date('Y-m-d H:i:s'),
                                            );
                                            //earning table insert
                                            // $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                                            $earningSuccess = $this->db->insert('user_earnings', $earningArr);
                                            if ($earningSuccess) {
                                                //updating user value
                                                $updateUserArray = array(
                                                    'task_completed' => ($userInfo['task_completed'] + 1),
                                                    'total_earning' => ($userInfo['total_earning'] + $taskInfo['points']),
                                                    'points_earned' => ($userInfo['points_earned'] + $taskInfo['points']),
                                                    'last_earning_update' => date('Y-m-d H:i:s'),
                                                    'updated_at' => date('Y-m-d H:i:s'),
                                                );
                                                $whereUserArr['where'] = ['user_id' => $userId];
                                                $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);

                                                /** ADDED BY PRADIP :START */
                                                //unlock user levels and Achievements
                                                $newly_unloked_level = $this->unlockUserlevels($userId);

                                                //Removd by nigam
                                                // $newly_unloked_achievement = $this->unlockUserAchievement($userId);
                                                $unlock_message = '';
                                                $new_unlock = false;
                                                // if (count($newly_unloked_level) || count($newly_unloked_achievement)) {
                                                if (count($newly_unloked_level)) {
                                                    $new_unlock = true;
                                                    $unlock_message = 'Congratulations you have unlocked';
                                                    if (count($newly_unloked_level)) {
                                                        $unlock_message .= ' ' . implode(',', $newly_unloked_level);
                                                    }

                                                    //Removd by nigam
                                                    // rtrim(end($newly_unloked_level), ',');
                                                    // if (count($newly_unloked_level) && count($newly_unloked_achievement)) {
                                                    // $unlock_message .= 'AND';
                                                    // }

                                                    // if (count($newly_unloked_achievement)) {
                                                    // $unlock_message .= ' ' . implode(',', $newly_unloked_achievement);
                                                    // }

                                                }
                                                /** ADDED BY PRADIP :END */
                                            }
                                        }
                                        //Insert in wallet log history
                                        $insertWalletLog = array(
                                            'user_id' => $userId,
                                            'point' => $taskInfo['points'],
                                            'title' => $this->lang->line('reward_earned'),
                                            'description' => $taskInfo['action'],
                                            'task_id' => $taskInfo['task_id'],
                                            'type' => $taskInfo['task_type'],
                                            'status' => COMPLETE,
                                            'created_date' => date('Y-m-d H:i:s'),

                                        );
                                        //check if task history alredy exist fo
                                        $where['where'] = ['user_id' => $userId, 'task_id' => $taskInfo['task_id'], 'type' => $taskInfo['task_type']];
                                        $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                                        if (empty($walletLogInfo)) {
                                            #save users details values in DB
                                            $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                                            #if not success
                                            if (!$isSuccess) {
                                                throw new Exception($this->lang->line('try_again'));
                                            }
                                        }
                                    }
                                }
                            } else {
                                $newdata = array(
                                    'complete_using_app' => 1,
                                    'status' => $completeStatus
                                );
                                if ($completeStatus == PENDING) {
                                    if (isset($postDataArr['media']) && !empty($postDataArr['media'])) {
                                        $proofImage = explode(",", $postDataArr['media']);

                                        foreach ($proofImage as $key => $val) {
                                            $proofData['user_id'] = $userId;
                                            $proofData['task_id'] = $taskId;
                                            $proofData['media_url'] = $val;
                                            $proofData['created_date'] = date('Y-m-d H:i:s');
                                            $proofId = $this->Common_model->insert_single('offline_task_tracking', $proofData);
                                        }
                                    }
                                    if (isset($postDataArr['lattitude']) && !empty($postDataArr['lattitude'])) {
                                        $proofData['user_id'] = $userId;
                                        $proofData['task_id'] = $taskId;
                                        $proofData['latitude'] = $postDataArr['lattitude'];
                                        $proofData['longitude'] = $postDataArr['longitude'];
                                        $proofData['created_date'] = date('Y-m-d H:i:s');
                                        $proofId = $this->Common_model->insert_single('offline_task_tracking', $proofData);
                                    }
                                }
                                #updating profile details
                                $isSuccess = $this->Common_model->update_single('user_task_master', $newdata, $wherecond);

                                if($isSuccess){
                                    $now = now();
                                    if ($taskEndDate <= $date) {
                                        $updateUserArray = array(
                                            'task_completed' => ($userInfo['task_completed'] + 1),
                                            'updated_at' => date('Y-m-d H:i:s'),
                                        );
                                        $whereUserArr['where'] = ['user_id' => $userId];
                                        $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
                                    } else {
                                        if ($completeStatus == COMPLETE) {
                                            //earning inset block
                                            $earning_where = array('where' => array('task_id ' => $taskId, 'user_id' => $userId));
                                            $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                                            //earning block
                                            if (empty($earningInfo)) {
                                                # insert user earnings data
                                                $start_date = date('Y-m-01');
                                                $end_date = date('Y-m-t');
                                                //earning table
                                                $earningArr = array(
                                                    'user_id' => $userId,
                                                    'task_id' => $taskId,
                                                    'type' => $taskInfo['task_type'],
                                                    'month_start_date' => $start_date,
                                                    'month_end_date' => $end_date,
                                                    'total_earned_points' => $taskInfo['points'],
                                                    'reward_payment_amt' => $taskInfo['points'],
                                                    'reward_clear_status' => 0,
                                                    'inserted_on' => date('Y-m-d H:i:s'),
                                                );
                                                //earning table insert
                                                $earningSuccess = $this->db->insert('user_earnings', $earningArr);
                                                if ($earningSuccess) {
                                                    //updating user value
                                                    $updateUserArray = array(
                                                        'task_completed' => ($userInfo['task_completed'] + 1),
                                                        'total_earning' => ($userInfo['total_earning'] + $taskInfo['points']),
                                                        'points_earned' => ($userInfo['points_earned'] + $taskInfo['points']),
                                                        'last_earning_update' => date('Y-m-d H:i:s'),
                                                        'updated_at' => date('Y-m-d H:i:s'),
                                                    );
                                                    $whereUserArr['where'] = ['user_id' => $userId];
                                                    $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);

                                                    /** ADDED BY PRADIP :START */
                                                    //unlock user levels and Achievements
                                                    $newly_unloked_level = $this->unlockUserlevels($userId);

                                                    //Removd by nigam
                                                    // $newly_unloked_achievement = $this->unlockUserAchievement($userId);
                                                    $unlock_message = '';
                                                    $new_unlock = false;
                                                    // if (count($newly_unloked_level) || count($newly_unloked_achievement)) {
                                                    if (count($newly_unloked_level)) {
                                                        $new_unlock = true;
                                                        $unlock_message = 'Congratulations you have unlocked';
                                                        if (count($newly_unloked_level)) {
                                                            $unlock_message .= ' ' . implode(',', $newly_unloked_level);
                                                        }

                                                    }
                                                    /** ADDED BY PRADIP :END */
                                                }
                                            }
                                            //Insert in wallet log history
                                            $insertWalletLog = array(
                                                'user_id' => $userId,
                                                'point' => $taskInfo['points'],
                                                'title' => $this->lang->line('reward_earned'),
                                                'description' => $taskInfo['action'],
                                                'task_id' => $taskInfo['task_id'],
                                                'type' => $taskInfo['task_type'],
                                                'status' => COMPLETE,
                                                'created_date' => date('Y-m-d H:i:s'),

                                            );
                                            //check if task history alredy exist fo
                                            $where['where'] = ['user_id' => $userId, 'task_id' => $taskInfo['task_id'], 'type' => $taskInfo['task_type']];
                                            $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                                            if (empty($walletLogInfo)) {
                                                #save users details values in DB
                                                $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                                                #if not success
                                                if (!$isSuccess) {
                                                    throw new Exception($this->lang->line('try_again'));
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            // $newdata = array('complete_using_app' => 1);
                            #If query failed to update
                            #Throw Exception
                            if (!$isSuccess) {
                                $this->db->trans_rollback();
                                throw new Exception($this->lang->line('try_again'));
                            }

                            #if transaction runs successfully
                            if (true === $this->db->trans_status()) {
                                //if user not received bonus
                                if ($userInfo['is_bonus_received'] == 0) {
                                    $this->userBonusUpate($userId);
                                }
                                #Comminting changes
                                $this->db->trans_commit();
                                #setting Response Array
                                $response_array = [
                                    'CODE' => SUCCESS_CODE,
                                    'MESSAGE' => $this->lang->line('success'),
                                    'RESULT' => [],
                                    'new_unlock' => $new_unlock,
                                    'new_unlock_message' => $unlock_message,
                                ];
                            } else {
                                #IF transaction failed
                                #rolling back
                                $this->db->trans_rollback();

                                #setting Response Array
                                $response_array = [
                                    'CODE' => TRY_AGAIN_CODE,
                                    'MESSAGE' => $this->lang->line('try_again'),
                                    'RESULT' => [],
                                ];
                            }
                            $this->response($response_array);
                        } else {
                            $this->response(array("CODE" => NO_DATA_FOUND, 'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'), 'RESULT' => array()));
                        }
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => (object) array(),
                        ];
                    }
                } else {
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function unlockUserlevels($user_id)
    {
        $return_level = array();

        $sql = "SELECT points_earned FROM users WHERE user_id = ?";
        $query = $this->db->query($sql, array($user_id));
        $resultArray = $query->row_array();
        $current_points = $resultArray ['points_earned'];
       
        $new_levels = $this->db->query("SELECT MAX(fk_level_id) as fk_level_id from tbl_user_level where fk_user_id = $user_id");
        $new_levels = $new_levels->row_array();
    	$current_level = $new_levels['fk_level_id'] ? $new_levels['fk_level_id'] : '0';
                                
        if ($current_level != 'null') {
            $level_array = $this->nextlevel($current_points, $current_level);
       		 $difference = $level_array['max_level'] - $current_level;
            if ($difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $difference; $i++) {
                    $fk_level_id = $current_level + $i;
                    $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                    $reward_points = $reward_points->row_array();
                    $free_rewards =  $reward_points['free_reward_points'];
                    if($free_rewards != 0){
                        $params                 = array();
                        $params['points']       = $free_rewards;
                        $params['user_id']      = $user_id;
                        $res                    = $this->User_model->updateUserEarnPoints($params);

                        $walletInsertArr[]      = array(
                            'user_id'           => $user_id,
                            'point'             => $free_rewards,
                            'title'             => 'New Level Rewards',
                            'description'       => 'Rewards earned for completion of levels',
                            'task_id'           => '0',
                            'type'              => 'bonus',
                            'status'            => '1',
                            'created_date'      => date('Y-m-d H:i:s'),
                            'updated_date'      => date('Y-m-d H:i:s')
                        );
                        
                    }
                    
                    $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
                }
               
            }
        }
                    
            if(!empty($walletInsertArr) && count($walletInsertArr) > 0){
               	$this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
           	}
                  
        return $return_level;
    }
    
    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }

    public function unlockUserAchievement($user_id)
    {
        $user_wallet_amount = 0;
        $return_achievement = array();

        $user_wallet = $this->db->query("SELECT SUM(point) as total_erned_points FROM `ipac_user_wallet_history` WHERE user_id = $user_id");
        $user_wallet = $user_wallet->result_array();
        if (isset($user_wallet[0]) && isset($user_wallet[0]['total_erned_points']) && $user_wallet[0]['total_erned_points'] != '') {
            $user_wallet_amount = $user_wallet[0]['total_erned_points'];
        }

        $user_levels = $this->db->query("SELECT fk_level_id from tbl_user_level where fk_user_id = $user_id ");
        $user_levels = $user_levels->result_array();
        if (count($user_levels)) {
            foreach ($user_levels as $level) {
                $level_ids[] = $level['fk_level_id'];
            }
            $sub_query = ' AND ((type = 1 and fk_level_id in (' . implode(',', $level_ids) . ')) OR (type = 2 AND reward_points <= ' . $user_wallet_amount . '))';
        } else {
            $sub_query = " AND (type = 2 AND reward_points <= $user_wallet_amount)";
        }
        $new_achievement = $this->db->query("SELECT pk_achievement_id,name from tbl_achievement where eStatus='active' $sub_query");
        $new_achievement = $new_achievement->result_array();
        if (count($new_achievement)) {
            //unlock the levels
            foreach ($new_achievement as $achievement) {
                //check if already unlocked
                $already_unlocked = $this->db->query("SELECT pk_id from tbl_user_achievement where fk_user_id = ? AND fk_achievement_id=?", array($user_id, $achievement['pk_achievement_id']));
                $already_unlocked = $already_unlocked->result_array();

                if (count($already_unlocked) == 0) {
                    $return_achievement[] = $achievement['name'] . ' ';
                    $this->Common_model->insert_single('tbl_user_achievement', ['fk_achievement_id' => $achievement['pk_achievement_id'], 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
                }
            }
        }
        return $return_achievement;
    }
    /** END:: ADDED BY PRADIP  */

    /**
     * @function userBonusUpdate
     * @description give user bonus 100 on profile
     * completion and when task completed by user is greater than 3
     * @param int $userId
     * @return void
     */
    private function userBonusUpate($userId)
    {
        $userInfo = $this->User_model->userDetail($userId);
        /**
         * Check if user fill all data and task completed by user is greater than 1
         */
        $profileComplete = 0;
        //facebook id
        if (!empty($userInfo['facebook_id'])) {
            $profileComplete++;
        }
        //twitter id
        if (!empty($userInfo['twitter_id'])) {
            $profileComplete++;
        }
        //whatsup
        if (!empty($userInfo['whatsup_number'])) {
            $profileComplete++;
        }
        //upi or paytm numbber or account number
        if (
            !empty($userInfo['upi_address']) || !empty($userInfo['paytm_number'])
            || !empty($userInfo['account_number'])
        ) {
            $profileComplete++;
        }
        $whereUserArr['where'] = ['user_id' => $userId];
        // check if user update in id proff or not
        $user_id_info = $this->Common_model->fetch_data('user_id_proof_details', [
            'proof_id',
        ], $whereUserArr, true);
        //id proof
        if (!empty($user_id_info)) {
            $profileComplete++;
        }
        //if profile completed by user
        if ((($profileComplete == 5) && ($userInfo['is_bonus_received'] == 0) && ($userInfo['totalTaskComplete'] >= 1) && ($userInfo['campaignStatus'] == 0))) {
            //Insert in wallet log history
            $insertWalletLog = array(
                'user_id' => $userId,
                'point' => BONUS_POINT,
                'title' => $this->lang->line('bonus_earned'),
                'description' => $this->lang->line('profile_complete'),
                'task_id' => 0,
                'type' => 'bonus',
                'status' => COMPLETE,
                'created_date' => date('Y-m-d H:i:s'),

            );
            //check if task history alredy exist fo
            $whereWall['where'] = ['user_id' => $userId, 'task_id' => 0, 'type' => 'bonus'];
            $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $whereWall, true);
            if (empty($walletLogInfo)) {
                #save users details values in DB
                $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);

                #if not success
                if (!$isSuccess) {
                    throw new Exception($this->lang->line('try_again'));
                }
            }
            //updating user value
            $updateUserArray = array(
                'total_earning' => ($userInfo['total_earning'] + BONUS_POINT),
                'last_earning_update' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_bonus_received' => BONUS_RECEIVED,
            );
            $whereUserArr['where'] = ['user_id' => $userId];
            //update user data in user table
            $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
            //sending notification
            $this->sendNotification($userId);
        }
    }

    /**
     * @function sendNotification
     * @description send notification on bonus received by user
     * @param int $userID id of user
     * @access private
     * @return void
     */
    private function sendNotification($userId)
    {
        //get user was ern to send notification
        $userDetail = $this->Notification_model->sendNotification($userId);
        if (isset($userDetail) && !empty($userDetail)) {
            //message arry for sending notification
            $message = [
                "default" => $this->lang->line('request_accept_not'),
                "APNS_SANDBOX" => json_encode([
                    "aps" => [
                        "alert" => array(
                            "title" => $this->lang->line('bonus_point_notification'),
                            "body" => $this->lang->line('bonus_point_notification'),
                        ),
                        "sound" => "default",
                        "mutable-content" => 1,
                        "badge" => 1,
                        "data" => array(
                            'notification_id' => '',
                            'news_id' => '',
                            "task_id" => '',
                            "content_type" => "image",
                            "type" => 5,
                        ),
                    ],
                ]),
                "GCM" => json_encode([
                    "data" => [
                        "title" => $this->lang->line('bonus_point_notification'),
                        "message" => $this->lang->line('bonus_point_notification'),
                        "body" => $this->lang->line('bonus_point_notification'),
                        "type" => 5,
                        "image" => '',
                        'news_id' => '',
                        'task_id' => '',
                        'notification_id' => '',

                    ],
                ]),
            ];
            //sns object
            $sns = new Snspush();
            //send message to filtered users or selected users
            $result = $sns->asyncPublish2($userDetail, $message);
            //sending notification array
            $insertNewsNoti = array(
                'user_id' => $userId,
                'news_id' => 0,
                'task_id' => 0,
                'notification_type' => BONUS_NOTIFICATION_TYPE,
                'notification_text' => $this->lang->line('bonus_point_notification'),
                'inserted_on' => date('Y-m-d H:i:s'),
            );
            //insert in user notification table
            $updateId = $this->Common_model->insert_single('user_notification', $insertNewsNoti);
        }
    }

    public function getPeerGroup_post()
    {
        $params = $this->post();
        $count = isset($params['count']) ? $params['count'] : 0;
        $limit = PAGE_LIMIT;
        $offset = (int)$count;

        $required_fields_arr = array(
            array(
                'field' => 'task_id',
                'label' => 'Task Id',
                'rules' => 'trim|required',
            ),
        );
        #Setting Error Messages for rules
        $this->form_validation->set_rules($required_fields_arr);
        $this->form_validation->set_message('required', 'Please enter the %s');

        if ($this->form_validation->run()) {
            $task_id = $params['task_id'];
            $this->next_count = $offset + $limit;

            $userId = $GLOBALS['api_user_id'];
            $wherecond = array('where' => array('user_id' => $userId));

            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data(
                'users',
                ['gender', 'user_id', 'state', 'district', 'registeration_no', 'unread_count', 'college', 'paytm_number', 'upi_address', 'account_number'],
                $wherecond,
                true
            );

            $results = $this->home->getAllCompletedTaskUserList($userInfo["user_id"], $task_id, $offset, $limit);
            $tasks = $results['data'];
            $total_count = $results['count'];

            if ($total_count <= $this->next_count) {
                $this->next_count = '-1';
            }
            $this->response(array(
                'CODE' => SUCCESS_CODE,
                'MESSAGE' => 'success',
                'RESULT' => $tasks,
                'NEXT' => $this->next_count,
                'TOTAL' => $total_count
            ));
        } else {
            $err = $this->form_validation->error_array();
            $arr = array_values($err);
            $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
        }
    }

    public function dailyreward_get()
    {
        $this->db->trans_begin();
        try {
            $dailyrewardupdate = false;
            $userId = $GLOBALS['api_user_id'];
            $whereArr['where'] = ['user_id' => $userId];
            $start_date = date('Y-m-01');
            $end_date = date('Y-m-t');
            $dailyPoints = 50;
            $rewardDay = 1;
            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data(
                'users',
                [
                    'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                    'points_earned', 'total_earning', 'task_completed', 'is_bonus_received', 'last_dailyreward_count'
                ],
                $whereArr,
                true
            );

            if (!empty($userInfo)) {
                /** Check if user is not blocked */
                if (ACTIVE == $userInfo['is_active']) {
                    //earning inset block
                    $earning_where = array(
                        'where' => array('type' => 'dailyreward', 'user_id' => $userId),
                        'limit' => array('1'),
                        'order_by' => array('earning_id ' => 'DESC')
                    );
                    $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id', 'total_earned_points', 'reward_payment_amt', 'inserted_on'], $earning_where, true);
                    //earning block
                    if (!empty($earningInfo)) {
                        $now = time();
                        $your_date = strtotime($earningInfo['inserted_on']);
                        $datediff = $now - $your_date;
                        $days =  round($datediff / (60 * 60 * 24));
                        if ($days == 0) {
                            $dailyrewardupdate = false;
                        } else if ($days == 1) {
                            $dailyrewardupdate = true;
                            if ($userInfo['last_dailyreward_count'] == 20){
                            $dailyrewardupdate = true;
                           
                            } else if ($userInfo['last_dailyreward_count'] == 10 ){
                                $dailyPoints = 150;

                            }else if ($userInfo['last_dailyreward_count'] == 100) {
                                $dailyPoints = 150;

                            }else if ($userInfo['last_dailyreward_count'] == 5) {
                                $dailyPoints = 100;

                            }else if ($userInfo['last_dailyreward_count'] == 50) {
                                $dailyPoints = 100;
                            }
                        } else {
                            $dailyrewardupdate = true;
                        }
                    } else {
                        $dailyrewardupdate = true;
                    }
                    if ($dailyrewardupdate) {
                        $earningArr = array(
                            'user_id' => $userId,
                            'task_id' => 0,
                            'type' => 'dailyreward',
                            'month_start_date' => $start_date,
                            'month_end_date' => $end_date,
                            'total_earned_points' => $dailyPoints,
                            'reward_payment_amt' => $dailyPoints,
                            'reward_clear_status' => 0,
                            'inserted_on' => date('Y-m-d H:i:s'),
                        );
                        $earningSuccess = $this->db->insert('user_earnings', $earningArr);
                        //updating user value
                        $updateUserArray = array(
                            'last_dailyreward_count' => $dailyPoints,
                            'total_earning' => ($userInfo['total_earning'] + $dailyPoints),
                            'points_earned' => ($userInfo['points_earned'] + $dailyPoints),
                            'last_earning_update' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        );
                        $whereUserArr['where'] = ['user_id' => $userId];
                        $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);

                        $newly_unloked_level = $this->unlockUserlevels($userId);

                        $unlock_message = '';
                        $new_unlock = false;

                        if (count($newly_unloked_level)) {
                            $new_unlock = true;
                            $unlock_message = 'Congratulations you have unlocked';
                            if (count($newly_unloked_level)) {
                                $unlock_message .= ' ' . implode(',', $newly_unloked_level);
                            }
                        }

                        //Insert in wallet log history
                        $insertWalletLog = array(
                            'user_id' => $userId,
                            'point' => $dailyPoints,
                            'title' => 'Daily Reward',
                            'description' => 'dailyreward',
                            'task_id' => 0,
                            'type' => 'dailyreward',
                            'status' => COMPLETE,
                            'created_date' => date('Y-m-d H:i:s'),

                        );
                        $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                        $this->db->trans_commit();
                        $rewardDay = $dailyPoints == 150 ? 3 : ($dailyPoints == 100 ? 2 : $rewardDay);
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('success'),
                            'RESULT' => ['rewardDay' => $rewardDay],
                            'new_unlock' => $new_unlock,
                            'new_unlock_message' => $unlock_message,
                        ];
                        $this->response($response_array);
                    } else {
                        $response_array = [
                            'CODE' => 422,
                            'MESSAGE' => $this->lang->line('error'),
                            'RESULT' => []
                        ];
                        $this->response($response_array);
                    }
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => (object) array(),
                    ];
                    $this->response($response_array);
                }
            } else {
                $response_array = [
                    'CODE' => RECORD_NOT_EXISTS,
                    'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                    'RESULT' => (object) array(),
                ];

                $this->response($response_array);
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

//rewards 5,10,20 have been reverted
    // public function dailyreward_get()
    // {
    //     $this->db->trans_begin();
    //     try {
    //         $dailyrewardupdate = false;
    //         $userId = $GLOBALS['api_user_id'];
    //         $whereArr['where'] = ['user_id' => $userId];
    //         $start_date = date('Y-m-01');
    //         $end_date = date('Y-m-t');
    //         $dailyPoints = 5;
    //         $rewardDay = 1;
    //         #fetching user details from db
    //         $userInfo = $this->Common_model->fetch_data(
    //             'users',
    //             [
    //                 'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
    //                 'points_earned', 'total_earning', 'task_completed', 'is_bonus_received', 'last_dailyreward_count'
    //             ],
    //             $whereArr,
    //             true
    //         );

    //         if (!empty($userInfo)) {
    //             /** Check if user is not blocked */
    //             if (ACTIVE == $userInfo['is_active']) {
    //                 //earning inset block
    //                 $earning_where = array(
    //                     'where' => array('type' => 'dailyreward', 'user_id' => $userId),
    //                     'limit' => array('1'),
    //                     'order_by' => array('earning_id ' => 'DESC')
    //                 );
    //                 $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id', 'total_earned_points', 'reward_payment_amt', 'inserted_on'], $earning_where, true);
    //                 //earning block
    //                 if (!empty($earningInfo)) {
    //                     $now = time();
    //                     $your_date = strtotime($earningInfo['inserted_on']);
    //                     $datediff = $now - $your_date;
    //                     $days =  round($datediff / (60 * 60 * 24));
    //                     if ($days == 0) {
    //                         $dailyrewardupdate = false;
    //                     } else if ($days == 1) {
    //                         $dailyrewardupdate = true;
    //                         if ($userInfo['last_dailyreward_count'] != 20) {
    //                             $dailyPoints = $userInfo['last_dailyreward_count'] + $userInfo['last_dailyreward_count'];
    //                         }
    //                     } else {
    //                         $dailyrewardupdate = true;
    //                     }
    //                 } else {
    //                     $dailyrewardupdate = true;
    //                 }
    //                 if ($dailyrewardupdate) {
    //                     $earningArr = array(
    //                         'user_id' => $userId,
    //                         'task_id' => 0,
    //                         'type' => 'dailyreward',
    //                         'month_start_date' => $start_date,
    //                         'month_end_date' => $end_date,
    //                         'total_earned_points' => $dailyPoints,
    //                         'reward_payment_amt' => $dailyPoints,
    //                         'reward_clear_status' => 0,
    //                         'inserted_on' => date('Y-m-d H:i:s'),
    //                     );
    //                     $earningSuccess = $this->db->insert('user_earnings', $earningArr);
    //                     //updating user value
    //                     $updateUserArray = array(
    //                         'last_dailyreward_count' => $dailyPoints,
    //                         'total_earning' => ($userInfo['total_earning'] + $dailyPoints),
    //                         'points_earned' => ($userInfo['points_earned'] + $dailyPoints),
    //                         'last_earning_update' => date('Y-m-d H:i:s'),
    //                         'updated_at' => date('Y-m-d H:i:s'),
    //                     );
    //                     $whereUserArr['where'] = ['user_id' => $userId];
    //                     $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);

    //                     $newly_unloked_level = $this->unlockUserlevels($userId);

    //                     $unlock_message = '';
    //                     $new_unlock = false;

    //                     if (count($newly_unloked_level)) {
    //                         $new_unlock = true;
    //                         $unlock_message = 'Congratulations you have unlocked';
    //                         if (count($newly_unloked_level)) {
    //                             $unlock_message .= ' ' . implode(',', $newly_unloked_level);
    //                         }
    //                     }

    //                     //Insert in wallet log history
    //                     $insertWalletLog = array(
    //                         'user_id' => $userId,
    //                         'point' => $dailyPoints,
    //                         'title' => 'Daily Reward',
    //                         'description' => 'dailyreward',
    //                         'task_id' => 0,
    //                         'type' => 'dailyreward',
    //                         'status' => COMPLETE,
    //                         'created_date' => date('Y-m-d H:i:s'),

    //                     );
    //                     $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
    //                     $this->db->trans_commit();
    //                     $rewardDay = $dailyPoints == 20 ? 3 : ($dailyPoints == 10 ? 2 : $rewardDay);
    //                     $response_array = [
    //                         'CODE' => SUCCESS_CODE,
    //                         'MESSAGE' => $this->lang->line('success'),
    //                         'RESULT' => ['rewardDay' => $rewardDay],
    //                         'new_unlock' => $new_unlock,
    //                         'new_unlock_message' => $unlock_message,
    //                     ];
    //                     $this->response($response_array);
    //                 } else {
    //                     $response_array = [
    //                         'CODE' => 422,
    //                         'MESSAGE' => $this->lang->line('error'),
    //                         'RESULT' => []
    //                     ];
    //                     $this->response($response_array);
    //                 }
    //             } else {
    //                 #if user is blocked
    //                 #setting Response
    //                 $response_array = [
    //                     'CODE' => ACCOUNT_BLOCKED,
    //                     'MESSAGE' => $this->lang->line('account_blocked'),
    //                     'RESULT' => (object) array(),
    //                 ];
    //                 $this->response($response_array);
    //             }
    //         } else {
    //             $response_array = [
    //                 'CODE' => RECORD_NOT_EXISTS,
    //                 'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
    //                 'RESULT' => (object) array(),
    //             ];

    //             $this->response($response_array);
    //         }
    //     } catch (Exception $e) {
    //         $this->db->trans_rollback();
    //         $error = $e->getMessage();

    //         #setting response
    //         $response_array = [
    //             'CODE' => TRY_AGAIN_CODE,
    //             'MESSAGE' => $error,
    //             'RESULT' => [],
    //         ];

    //         #sending Response
    //         $this->response($response_array);
    //     } #CATCH END
    // }
}
