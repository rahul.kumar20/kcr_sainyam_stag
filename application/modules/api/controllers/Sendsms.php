<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Sendsms extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('sendsms');
    }

    public function index_get()
    {
        # Don't send any state or country code for getting the list of country
        try {
            $params = $this->get();
            $phone_no = $params['phone_no'];
            $whereArr = array('where' => ['phone_no' => $phone_no]);
            $table = 'users_newentry';
            $getData = $this->Common_model->fetch_data($table, array("*"), $whereArr, true);

            #If Result list is not empty
            if (!empty($getData)) {
                $username = $getData['phone_no'];
                $password = $getData['password'];
                $message = "ஸ்டாலின் அணி செயலியில் உங்கள் பயணத்தைத் தொடருங்கள்!\n\nusername/பயனரின் குறியீடு  - $username\npassword/கடவுச்சொல் - $password\n\n Link/இணைப்பு - http://bit.ly/Joindmkstalinani";
                sendPromoMessage(
                    COUNTRYCODE . $phone_no,
                    $message
                );
                $updateUserArray = array('is_sms_sent' => 1);
                $this->Common_model->update_single(
                    $table,
                    $updateUserArray,
                    $whereArr
                );
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'SMS Sent',
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => 'No data found'
                ];
            } #else End
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
            ];
            $this->response($response_array);
        }
    }
}
