<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Term_condition extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Common_function');
    }

    /**
     * @SWG\Post(path="/termconditionaccept",
     *   tags={"Term & Condition "},
     *   summary="Term & Condition",
     *   description="Term & Condition accept",
     *   operationId="term_index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="Accesstoken",
     *     in="header",
     *     description="Access token received during signup or login in header",
     *     required=true,
     *     type="string"
     *   ),
     *    @SWG\Parameter(
     *     name="app_version",
     *     in="formData",
     *     description="App version",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="term_condition_version",
     *     in="formData",
     *     description="Term and condition version",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Add Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     *   @SWG\Response(response=201, description="Try again something went wrong"),
     *   @SWG\Response(response=401, description="Access token unthorised"),
     * )
     */

    public function index_post()
    {

        try {
            $postDataArr = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'app_version',
                    'label' => 'App Version',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'term_condition_version',
                    'label' => 'Term And Condition Version',
                    'rules' => 'trim|required',
                ),

            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $userId = $GLOBALS['api_user_id'];
                #setting Default values to array KEYS
                $default = array(
                    "app_version" => "",
                    "term_condition_version" => "",
                    "status" => 1,
                );
                $termArr = defaultValue($postDataArr, $default);

                $termArr["user_id"] = $userId;
                $termArr["created_date"] = date('Y-m-d H:i:s');
                $this->db->trans_begin(); #DB transaction Start
                $where = array('where' => array('user_id' => $userId, 'term_condition_version' => $termArr['term_condition_version']));
                //check if user already accept
                $termAceeptInfo = $this->Common_model->fetch_data('ipac_user_term_condition_log', ['id'], $where);
                //if empty
                if (empty($termAceeptInfo)) {
                    #save users details values in DB
                    $successId = $this->Common_model->insert_single('ipac_user_term_condition_log', $termArr);
                    if (!$successId) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    #Checking transaction status
                    if ($this->db->trans_status()) {
                        #comminting Transaction
                        $this->db->trans_commit();

                        if (!empty($successId)) { #if Start
                        #setting response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('term_accept_success'),
                            'RESULT' => [],
                        ];
                        } #if End
                    } else {
                        $this->db->trans_rollback();

                        #setting response
                        $response_array = [
                            'CODE' => TRY_AGAIN_CODE,
                            'MESSAGE' => $this->lang->line('try_again'),
                            'RESULT' => [],
                        ];
                    }
                } else {
                    #setting response
                    $response_array = [
                        'CODE' => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('term_alresdy_accept_success'),
                        'RESULT' => [],
                    ];
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                #setting response
                $response_array = [
                    'CODE' => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT' => (object) array(),
                ];
            }
            $this->response($response_array);
            #TRY END
        } catch (Exception $e) {
            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => (object) array(),
            ];

            #sending response
            $this->response($response_array);
        } #Catch End
    }
}
