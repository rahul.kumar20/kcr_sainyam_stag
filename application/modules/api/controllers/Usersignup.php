<?php

defined ( 'BASEPATH' ) OR exit ( 'No direct script access allowed' );
require APPPATH . '/libraries/REST_Controller.php';

class Usersignup extends REST_Controller {

    function __construct ()
    {
        parent::__construct ();
        $this->load->helper ( 'security' );
        $this->load->library ( 'Common_function' );

    }

    public function index_post ()
    {
        try {
            $postDataArr    = $this->post ();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array (
                array (
                    'field' => 'full_name',
                    'label' => 'Name',
                    'rules' => 'trim|required'
                ),  
                 array (
                    'field' => 'email_id',
                    'label' => 'Email Id',
                    'rules' => 'trim|required'
                 ),
                 array (
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required'
                ),
                array (
                    'field' => 'state',
                    'label' => 'State',
                    'rules' => 'trim|required'
                ),
                array (
                    'field' => 'district',
                    'label' => 'District',
                    'rules' => 'trim|required'
                ),
                 array (
                    'field' => 'phone_number',
                    'label' => 'Mobile Number',
                    'rules' => 'trim|required'
                )
            );


            #Setting Error Messages for rules
            $this->form_validation->set_rules ( $required_fields_arr );
            $this->form_validation->set_message ( 'required', 'Please enter the %s' );

            #checking if form fields are valid or not
            if ( $this->form_validation->run () )
            {
                $default = array();
                
                $whereArr = [];
               
                # If users enters email for signup
                $whereArr['where'] = [ 'email_id' => $postDataArr['email_id'] ];
                $EmailAlreadyExist = $this->Common_model->fetch_data ( 'users', ['user_id','is_active' ], $whereArr, true );
                
                $whereArr['where'] = [ 'phone_number' => $postDataArr['phone_number'] ];
                $PhoneAlreadyExist = $this->Common_model->fetch_data ( 'users', ['user_id','is_active' ], $whereArr, true );
                
                
                
                #is user Blocked
                if ( ! empty ( $EmailAlreadyExist ) && 0 == $EmailAlreadyExist['is_active'] )
                {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE'    => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line ( 'account_blocked' ),
                        'RESULT'  => ( object ) array ()
                    ];

                    $this->response ( $response_array );
                }else if (! empty ( $EmailAlreadyExist ) )
                {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE'    => EMAIL_ALREADY_EXIST,
                        'MESSAGE' => $this->lang->line ( 'account_exist' ),
                        'RESULT'  => ( object ) array ()
                    ];

                    $this->response ( $response_array );
                }else if (! empty ( $PhoneAlreadyExist ) )
                {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE'    => PHONE_ALREADY_EXISTS,
                        'MESSAGE' => $this->lang->line ( 'phone_exist' ),
                        'RESULT'  => ( object ) array ()
                    ];

                    $this->response ( $response_array );
                }else{
                    
                    $this->db->trans_begin (); #DB transaction Start
                    
                    
                    #save user details
                    $registrationNo = $this->generateRegistrationNo($this->input->post('distict'));


                   //insert user array
                    $insertUserArray = array(
                    'registeration_no' => $registrationNo,
                    'full_name' => $this->input->post('first_name'),
                    'email_id' => $this->input->post('email'),
                    'password' => base64_encode($postData['password']),
                    'facebook_id' => $this->input->post('fbId'),
                    'twitter_id' => $this->input->post('twitterId'),
                    'phone_number' => $this->input->post('phoneno'),
                    'whatsup_number' => $this->input->post('whatsupno'),
                    'paytm_number' => $this->input->post('paytmno'),
                    'state' => $this->input->post('state'),
                    'gender' => $this->input->post('gender'),
                    'district' => $this->input->post('distict'),
                    'user_image' => isset($userImageUrl) && !empty($userImageUrl) ? $userImageUrl : '',
                    'registered_on' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                    );

                    
                    
                   
                    #Generate Public and Private Access Token
                    $accessToken = create_access_token ( '$userId', $user_info['email_id'] );

                    #Access Token for signup
                    $signupArr['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];
                    $signupArr['user_id']     = $userId;
                    
                    $user_info['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];
                     
                  
              
                    #Setting session after Signup
                    $sessionArr        = setSessionVariables ( $postDataArr, $accessToken );
                    
                    /*
                     * Insert Session Data
                     */
                    
                    $whereArr          = [];
                    $device_id         = isset ( $postDataArr['device_id'] ) ? $postDataArr['device_id'] : "";
                    $isExist = array();
                    if($device_id )
                    {
                        $whereArr['where'] = [ 'device_id' => $device_id ];
                         $isExist           = $this->Common_model->fetch_data ( 'user_device_details', array ( 'session_id' ), $whereArr, true );
                    }
                    /*
                     * If user has logged in previously with same device then update his detail
                     * or insert as a new row
                     */
                   
                    if ( ! empty ( $isExist ) )
                    {
                        #updating session table
                        $logData                    = array();
                        $logData['where']           = $whereArr;
                        $logData['updateData']      = $sessionArr;
                        $logData['fetchData']       = $isExist;
                        $this->common_function->writeDeviceDetailsLog('update','api :: user sign up >> index_post Before Updating',$logData);
                        $sessionId = $this->Common_model->update_single ( 'user_device_details', $sessionArr, $whereArr );

                        $logData                    = array();
                        $logData['fetchData']       = $this->Common_model->fetch_data('user_device_details','', $whereArr, true);
                        $this->common_function->writeDeviceDetailsLog('update','api ::  user sign up >> index_post After Updating ',$logData);
                    }
                    else
                    {
                        #inserting session details
                        $this->common_function->writeDeviceDetailsLog('insert','api :: user sign up >> index_post ',$sessionArr);
                        $sessionId = $this->Common_model->insert_single ( 'user_device_details', $sessionArr );
                    }
                    
                    #comminting Transaction
                    $this->db->trans_commit ();

                    if ( ! empty ( $sessionId ) && ! empty ( $userId ) )
                    {
                        #setting response
                        $response_array = [
                            'CODE'    => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line ( 'registration_success' ),
                            'RESULT'  => $user_info
                        ];
                    }
                    
                }
            }#form validation End
            else
            {
                $err = $this->form_validation->error_array ();
                $arr = array_values ( $err );
                $this->response ( array ( 'CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => ( object ) array () ) );
            }
            $this->response ( $response_array );
        }#TRY END
        catch ( Exception $e ) {

            #if transaction failed than rollback
            $this->db->trans_rollback ();
            $error = $e->getMessage ();

            #setting response
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT'  => ( object ) array ()
            ];

            #sending response
            $this->response ( $response_array );
        }#Catch End

    }



    /**
     * @function sendWelcomeMail
     * @description Sending welcome mail to user
     *
     * @param array $mailData user data needed to send mail, user email address and user name
     */
    private function sendWelcomeMail ( $mailData )
    {

        $this->load->helper ( 'url' );
        $data        = [];
        $data['url'] = base_url () . 'request/welcomeMail?email=' . $mailData['email'] . '&name=' . urlencode ( $mailData['name'] );
        sendGetRequest ( $data );

    }



    /**
     * @funciton validate_phone
     * @description Custom validation rules to validate phone number
     *
     * @param  $phone user phone number to validate phone number
     * @return boolean|json
     */
    private function validate_phone ( $phone )
    {

        if ( isset ( $phone ) && ! preg_match ( "/^[0-9]{10}$/", $phone ) && ! empty ( $phone ) )
        {
            #setting Response
            $response_array = [
                'CODE'    => PARAM_REQ,
                'MESSAGE' => $this->lang->line ( 'invalid_phone' ),
                'RESULT'  => []
            ];

            #sending Response
            $this->response ( $response_array );
        }
        else
        {
            return true;
        }

    }



    /*
     * Custom Rule Validate Dob
     * @param: user dob
     */

    /**
     * @function validate_dob
     * @description Custom Rule Validation For Date Of Birth
     *
     * @param date $dob user date of birth to check
     * @return boolean
     */
    private function validate_dob ( $dob )
    {
        if ( ! (isValidDate ( $dob, 'm-d-Y' )) )
        {
            #setting response
            $response_array = [
                'CODE'    => PARAM_REQ,
                'MESSAGE' => $this->lang->line ( 'invalid_dob' ),
                'RESULT'  => []
            ];

            #sending Response
            $this->response ( $response_array );
        }
        else
        {
            return true;
        }

    }



/* Generating user registration number
     * IPAC XXX PTA YYYY (IPAC, PTA are static and XXX, YYYY are variables)
        First 4 digits represents the company (IPAC) name
        XXX represents the district code (number)
        PTA represents as Part Time Associate
        YYYY represents the volunteer serial number (in respective districts)
     */
    private function generateRegistrationNo($districtId)
    {
        
        #generate 3 digit district code of user district
        $districtnum = 3;
        $district_num_padded = sprintf("%03d", $districtId);
        
        # count the number of users in a particular district
        $countUser = $this->News_model->userList($districtId);
        $usercnt = 0;
        if (!empty($countUser)) {
            $usercnt = $countUser->UserCnt;
        }
        $usercnt = $usercnt + 1;
        # generate 4 digit user count for a particular district
        $districtusernum = 4;
        $district_user_num_padded = sprintf("%04d", $usercnt);

        $prefix = 'STLN';
        $sufix = 'PTA';
        $registrationNo = $prefix . $district_num_padded . $sufix . $district_user_num_padded;
        return $registrationNo;
    }



}
