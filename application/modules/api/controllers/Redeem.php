<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Redeem extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->model('Common_model');
        $this->load->database();
    }

    public function index_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'user_level_id',
                    'label' => 'User Level Id',
                    'rules' => 'trim|required',
                ),
            );

            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');
            if ($this->form_validation->run()) {
                $USER_LEVEL_ID = $postDataArr['user_level_id'];
                $USER_ID = $GLOBALS['api_user_id'];
                $wherecond = array('where' => array('user_id' => $USER_ID));
                $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active', 'state', 'district', 'gender', 'registeration_no',
                    'full_name', 'user_image', '0 as rating', 'registered_on', 'paytm_number', 'upi_address', 'account_number'], $wherecond, true);

                if (!empty($userInfo)) {
                    /*
                     * Check if user is not blocksed
                     */
                    if (ACTIVE == $userInfo['is_active']) {
                        $sql = "SELECT l.incentive from tbl_user_level ul left join tbl_levels l on ul.fk_level_id = l.pk_level_id where ul.pk_id = ?";
                        $query = $this->db->query($sql, array($USER_LEVEL_ID));
                        $incentive = $query->result_array();
                        $redeem_msg = "";
                        if (count($incentive)) {
                            $redeem_msg = $incentive[0]['incentive'];
                        }
                        if ($redeem_msg==""){
                            $response_array = [
                                'CODE' => ACCOUNT_BLOCKED,
                                'MESSAGE' => $this->lang->line('account_blocked'),
                                'RESULT' => [],
                            ];
                            echo json_encode($response_array);
                            die;
                        }
                        /*  $connection->update('tbl_user_level', ['incentive_redeem_status' => '1','incentive_text'=>$redeem_msg,'redeem_request_date'=>date('Y-m-d H:i:s')], ['pk_id' => $USER_LEVEL_ID]); */
                        $taskArr = array(
                            'incentive_redeem_status' => '1',
                            'incentive_text' => $redeem_msg,
                            'redeem_request_date' => date('Y-m-d H:i:s'),
                        );
                        $whereArr['where'] = ['pk_id' => $USER_LEVEL_ID];
                        $user_level = $this->Common_model->update_single('tbl_user_level', $taskArr, $whereArr);
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => 'Redeem Request Sent',
                            'valid' => true,
                        ];
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => [],
                        ];
                    }
                }
                echo json_encode($response_array);
                die;
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }

        } #TRY END
         catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CAT

    }

}
