<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Biography extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index_get()
    {

        # Don't send any state or country code for getting the list of country
        try {

            $headers        = $this->input->request_headers();
            $headers        = array_change_key_case($headers);
            $accessToken    = $headers['accesstoken'];
            $accessTokenArr = explode("||", $accessToken);

            if (2 === count($accessTokenArr)) {
                $whereArr               = [];
                $whereArr['where']      = ['public_key' => $accessTokenArr[0], 'private_key' => $accessTokenArr[1]];
                
                #Get platform details from DB
                $deviceInfo             = $this->Common_model->fetch_data('user_device_details', array ( 'user_id','platform'), $whereArr, true);

                $devicePlatform         = 'ios';
                if (!empty($deviceInfo)) {
                    if($deviceInfo['platform']){
                        $devicePlatform = $deviceInfo['platform'];
                    }
                }
                $devicePlatform         = strtolower($devicePlatform);

                $whereArr               = [];
                $whereArr['where']      = ['device_type' => $devicePlatform];
                $table                  = 'ipac_biography';
                $order                  = 'rank';
                $whereArr['order_by']   = ['chapter' => 'asc', $order => 'asc'];
                $listData               = $this->Common_model->fetch_data($table, array("*"), $whereArr);

                #If Result list is not empty
                if (!empty($listData)) {
                    $response_array     = [
                        'CODE'          => SUCCESS_CODE,
                        'MESSAGE'       => $this->lang->line('list_fetched'),
                        'RESULT'        => $listData
                    ];
                } #if End
                else {
                    $response_array     = [
                        'CODE'          => TRY_AGAIN_CODE,
                        'MESSAGE'       => $this->lang->line('list_fetched'),
                        'RESULT'        => []
                    ];
                } #else End

            }else{
                $response_array         = [
                    'CODE'              => PARAM_REQ,
                    'MESSAGE'           => $this->lang->line('try_again'),
                    'RESULT'            => []
                ];
            }                
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array             = [
                'CODE'                  => TRY_AGAIN_CODE,
                'MESSAGE'               => $e->getMessage(),
                'RESULT'                => []
            ];
            $this->response($response_array);
        }
    }
}
