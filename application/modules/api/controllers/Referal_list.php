<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Referal_list extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

      /**
      * @SWG\get(path="/referal_user_list",
      *   tags={"Referal"},
      *   summary="get Referal User List",
      *   description="get Referal User List",
      *   operationId="index_get_referal_history",
      *   consumes ={"multipart/form-data"},
      *   produces={"application/json"},
      *   @SWG\Parameter(
      *     name="Authorization",
      *     in="header",
      *     description="Authorization key",
      *     required=true,
      *     type="string"
      *   ),
      *@SWG\Parameter(
      *     name="Accesstoken",
      *     in="header",
      *     description="Access token got in login",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Parameter(
      *     name="count",
      *     in="query",
     *     description="hit with count to get next page",
     *     type="integer",
     *     required=true,
     *   ),
      *   @SWG\Response(response=200, description="Success"),
      *   @SWG\Response(response=206, description="Unauthorized request"),
      *   @SWG\Response(response=207, description="Header is missing"),
      *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
      * )
      */
     /*
      */
    public function index_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head,CASE_LOWER);
            $getDataArr = $this->input->get();

            $default      = array (
                "count"      => 1,
                "limit"     => QUERY_OFFSET,
            );
            #Setting Default Value
            $defaultValue = defaultValue($getDataArr, $default);
            $userId = $GLOBALS['api_user_id'];
            $userInfo  = $GLOBALS['login_user'];
            $page                = $defaultValue['count'];
            $offset              = ($page - 1) * $defaultValue['limit'];
            $params['limit']     = $defaultValue['limit'];
            $params['offset']    = $offset;
            $params['userId']   = $userId;
            //referal html
            $where['where'] = ['name' => 'Referal', 'status' => ACTIVE];
            $contents = $this->Common_model->fetch_data('page_master', ['content','content_ta'], $where, true);
            
            #Get wallet history log
            $referal_list = $this->get_referal_history($params);
            #total history log count
            $total_count = $referal_list['count'];
            #check is data remaining or not
            if (($total_count > ($page * $params['limit']) )) {
                $page ++;
            } else {
                $page = -1;
            }
            $referal_content = $contents['content'];
            if(isset($head['lang']) && strtolower($head['lang'])=='ta'){
                $referal_content = $contents['content_ta'];
            }
            $userArray = array(
                'referal_code' =>$userInfo['userinfo']['referal_code'],
                'referal_content' =>$referal_content,
                'referal_bonus_point' => REFERAL_BONUS_POINT
            );
            if (!empty($referal_list['data'])) {#if Start
                #setting response
                $response_array = [
                    'CODE'   => SUCCESS_CODE,
                    'MESSAGE'    => 'success',
                    'RESULT' => $referal_list['data'],
                    'USERINFO' => $userArray,
                    'TOTAL_REFERAL' => $total_count,
                    'COUNT'   => $page
                ];
            } #IF END
            else {#ELSE START
                #setting response
                $response_array = [
                    'CODE'   => NO_DATA_FOUND,
                    'MESSAGE'    => 'NO_DATA_FOUND',
                    'RESULT' => [],
                    'USERINFO' => $userArray,
                    'TOTAL_REFERAL' => $total_count,
                    'COUNT'   => '-1',
                ];
            }#Else End
            #sending Response
            $this->response($response_array);
        } #try END
        catch (Exception $e) {#Catch Start
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE'   => TRY_AGAIN_CODE,
                'MESSAGE'    => $error,
                'RESULT' => [],
                'PAGE'   => ''
            ];
            $this->response($response_array);
        }#Catch End
    }

       /**
      * @function get_referal_history
      * @description Fetch all wallet history
      *
      * @param array $params
      * @return type
      */
    private function get_referal_history($params)
    {
        #load wallet history model
        $this->load->model('Referal_model');

        return $this->Referal_model->getReferalUserList($params);
    }
}
