<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Address extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    /**
     * @SWG\Get(path="/address",
     *   tags={"Address"},
     *   summary="Get address information",
     *   description="Get country,state and city list",
     *   operationId="index_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country_code",
     *     in="query",
     *     description="hit with country code to get list of state belongs to it",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state_code",
     *     in="query",
     *     description="hit it with country code to get list of city belongs to it",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_get()
    {
        # Don't send any state or country code for getting the list of country
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);
            $lang = $head['lang'];
            $getDataArr = $this->get();

            #setting Default values to array KEYS
            $default = array(
                "limit" => LIMIT,
                "state_code" => "",
                "district_code" => "",
                "searchlike" => "",
                "page" => 1
            );
            $defaultValue = defaultValue($getDataArr, $default);
            $listData = '';
            $state_code = $defaultValue['state_code'];
            $district_code = $defaultValue['district_code'];
            $searchlike = $defaultValue['searchlike'];
            $whereArr = [];
            $selected_column = array('*');

            if (!empty($state_code) && empty($district_code)) {
                if (!empty($searchlike)) {
                    $whereArr['like'] = array('district_name' => $searchlike);
                }
                $whereArr['where'] = array('state_id' => $state_code);
                $table = 'district';
                $order = 'district_name';
            } else if (!empty($state_code) && !empty($district_code)) {
                if (!empty($searchlike)) {
                    $whereArr['like'] = array('ac' => $searchlike);
                }
                $whereArr['where'] = array('state' => $state_code, 'district' => $district_code);
                $table = 'tbl_ac';
                $order = 'ac';
                $selected_column = $lang == 'ta' ? array('id', 'ac_tn AS ac') : array('id', 'ac');
            }

            $whereArr['order_by'] = [$order => 'asc'];
            $listData = $this->Common_model->fetch_data($table, $selected_column, $whereArr);

            if (!empty($listData)) {
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => EMAIl_SEND_FAILED,
                'MESSAGE' => $e->getMessage(),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }

    public function block_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);
            $lang = $head['lang'];

            $getDataArr = $this->get();
            $default = array(
                "limit" => LIMIT,
                "district_id" => "",
                "searchlike" => "",
                "page" => 1
            );
            $defaultValue = defaultValue($getDataArr, $default);
            $listData = '';
            $district_id = $defaultValue['district_id'];
            $searchlike = $defaultValue['searchlike'];
            $whereArr = [];

            if (!empty($searchlike)) {
                $whereArr['like'] = array('block' => $searchlike);
            }
            $whereArr['where'] = array('district_id' => $district_id);
            $table = 'tbl_block';
            $order = 'block';
            $selected_column = $lang == 'ta' ? array('id', 'block_tn AS block') : array('id', 'block');

            $whereArr['order_by'] = [$order => 'asc'];
            $listData = $this->Common_model->fetch_data($table, $selected_column, $whereArr);

            #If Result list is not empty
            if (!empty($listData)) {
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }

    public function panchayat_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);
            $lang = $head['lang'];

            $getDataArr = $this->get();
            $default = array(
                "limit" => LIMIT,
                "block_id" => "",
                "searchlike" => "",
                "page" => 1
            );
            $defaultValue = defaultValue($getDataArr, $default);
            $listData = '';
            $block_id = $defaultValue['block_id'];
            $searchlike = $defaultValue['searchlike'];
            $whereArr = [];

            if (!empty($block_id)) {
                if (!empty($searchlike)) {
                    $whereArr['like'] = array('ward_panchayat' => $searchlike);
                }
                $whereArr['where'] = array('block_id' => $block_id);
                $table = 'tbl_ward_panchayat';
                $order = 'ward_panchayat';
                $selected_column = $lang == 'ta' ? array('id', 'ward_panchayat_tn AS ward_panchayat') : array('id', 'ward_panchayat');
            }

            $whereArr['order_by'] = [$order => 'asc'];
            $listData = $this->Common_model->fetch_data($table, $selected_column, $whereArr);

            #If Result list is not empty
            if (!empty($listData)) {
                if ($lang != 'ta') {
                    foreach ($listData as $i => $data) {
                        $listData[$i]['ward_panchayat'] = ucwords(strtolower($data['ward_panchayat']));
                    }
                }
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }
}
