<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/SNSPush.php';

class Register_User extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Common_function');
        $this->load->model('User_model');
        $this->load->helper('sendsms');
    }

    public function index_post()
    {
        try {
            // print_r("Hello");exit;
            $postDataArr = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'registeration_no',
                    'label' => 'User Id',
                    'rules' => 'trim|required',
                ),
            );

            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('is_unique', 'The %s is already registered with us');
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $default = array();
                $signupArr = $postDataArr;

                #Check if user registration no already registered
                $whereArr = [];

                # If users enters email for signup
                if (!empty($signupArr['registeration_no'])) {
                    $whereArr['where'] = ['registeration_no' => $signupArr['registeration_no']];
                }

                $user_info = $this->Common_model->fetch_data('users', ['user_id', 'email_id', 'is_active', 'phone_number'], $whereArr, true);
                // print_r($user_info);exit;
                #is user Blocked
                if (!empty($user_info) && 0 == $user_info['is_active']) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                } elseif (empty($user_info)) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('no_user'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                } else {
                    # save otp details
                    date_default_timezone_set('Asia/Kolkata');
                    $otp = generate_otp();
                    $userId = $user_info['user_id'];
                    $signupArr['user_id'] = $userId;
                    $signupArr['otp'] = $otp;
                    $registerArr['registered_on'] = date('Y-m-d H:i:s');
                    $registerArr['otp'] = $otp;
                    $registerArr['otp_sent_time'] = date('Y-m-d H:i:s');
                    $whereArr['where'] = ['user_id' => $userId];
                    $sessionId = $this->Common_model->update_single('users', $registerArr, $whereArr);
                   

                    #send otp to user
                    sendMessage(COUNTRYCODE . $user_info['phone_number'], $otp . $this->lang->line('otp_message'),SMS_SIGNUP_MSG_TID);
                    $message    = 'Dear user, your OTP for GDB App is '.$otp.'. Use this passcode to validate your login, Thank you. Regards, All India Trinamool Congress';
                    sendMessage(COUNTRYCODE . $user_info['phone_number'], $message,SMS_SIGNUP_MSG_TID);
                    #setting response
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line('registration_success'),
                        'RESULT' => (object) $signupArr,
                    ];
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {
            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => (object) array(),
            ];

            #sending response
            $this->response($response_array);
        } #Catch End
    }

    public function Resend_Otp_post()
    {
        date_default_timezone_set('Asia/Kolkata');
        #setting Form  validation Rules
        $required_fields_arr = array(
            array(
                'field' => 'user_id',
                'label' => 'User Id',
                'rules' => 'trim|required',
            ),
        );

        #Setting Error Messages for rules
        $this->form_validation->set_rules($required_fields_arr);
        $this->form_validation->set_message('is_unique', 'The %s is already registered with us');
        $this->form_validation->set_message('required', 'Please enter the %s');

        if ($this->form_validation->run()) {
            $postDataArr = $this->post();
            $userId = isset($postDataArr['user_id']) ? ($postDataArr['user_id']) : '';

            $whereArr['where'] = ['user_id' => $userId];
            $user_info = $this->Common_model->fetch_data('users', ['user_id', 'phone_number','is_number_verified'], $whereArr, true);

            if (count($user_info) > 0) {
                $otp = generate_otp();
                $user_mobileno = $user_info['phone_number'];
                $to = '+91' . $user_mobileno;
                $otpArr['otp'] = $otp;
                $otpArr['otp_sent_time'] = date('Y-m-d H:i:s');
                $sessionId = $this->Common_model->update_single('users', $otpArr, $whereArr);

                #send otp to user
                if($user_info['is_number_verified'] == 1){
                    $message            = $otp . $this->lang->line('login_otp_message');
                    $tid                = SMS_LOGIN_MSG_TID;
                }else{
                    $message            = $otp . $this->lang->line('otp_message');
                    $tid                = SMS_SIGNUP_MSG_TID;
                }
                
                $message                = 'Dear user, your OTP for GDB App is '.$otp.'. Use this passcode to validate your login, Thank you. Regards, All India Trinamool Congress';
                sendMessage(COUNTRYCODE . $user_info['phone_number'], $message,$tid);

                $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => $this->lang->line('otp_sent'), 'RESULT' => array('otp' => $otp)));
            } else {
                $response_array = [
                    'CODE' => RECORD_NOT_EXISTS,
                    'MESSAGE' => $this->lang->line('no_user'),
                    'RESULT' => (object) array(),
                ];

                $this->response($response_array);
            }
        } else {
            $err = $this->form_validation->error_array();
            $arr = array_values($err);
            $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
        }
    }

    public function Otp_Verify_post()
    {

        #setting Form  validation Rules
        $required_fields_arr = array(
            array(
                'field' => 'user_id',
                'label' => 'User Id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'otp',
                'label' => 'OTP',
                'rules' => 'trim|required',
            ),
        );

        #Setting Error Messages for rules
        $this->form_validation->set_rules($required_fields_arr);
        $this->form_validation->set_message('is_unique', 'The %s is already registered with us');
        $this->form_validation->set_message('required', 'Please enter the %s');

        if ($this->form_validation->run()) {
            $postDataArr = $this->post();
            //print_r($postDataArr);exit;
            $userId = isset($postDataArr['user_id']) ? ($postDataArr['user_id']) : '';
            $otp = isset($postDataArr['otp']) ? ($postDataArr['otp']) : '';

            $whereArr['where'] = ['user_id' => $userId];
            if ((string) $otp) {
                $whereArr['where']['otp'] = $otp;
            }

            $user_info = $this->Common_model->fetch_data('users', [
                'user_id', 'registeration_no', 'referal_code', 'full_name', 'email_id', 'facebook_id',
                'twitter_id', 'user_image', 'whatsup_number', 'paytm_number', 'phone_number', 'state', 'district', 'is_active', 'otp', 'otp_sent_time', 'is_profile_edited', 'is_number_verified', 'requested_for_edit_profile', 'is_bonus_received'
            ], $whereArr, true);


            if (!empty($user_info) && count($user_info) > 0) {
                # check wheather otp is expired or not
                date_default_timezone_set('Asia/Kolkata');
                $otp_current_timestamp = date('Y-m-d H:i:s');
                $otp_sent_timestamp = strtotime($user_info['otp_sent_time']);
                // $otp_current_timestamp = strtotime($otp_current_timestamp);
                $minutes = abs($otp_sent_timestamp - time()) / 60;
                // $diff = round(abs($otp_current_timestamp - $otp_sent_timestamp) / 60, 2);

                if ($minutes < OTP_EXPIRATION) {
                    // $otpArr['otp'] = '';
                    // $otpArr['otp_sent_time'] = '';
                    $otpArr['is_number_verified'] = 1;
                    $sessionId = $this->Common_model->update_single('users', $otpArr, $whereArr);

                    #Generate Public and Private Access Token
                    $accessToken = create_access_token($user_info['user_id'], $user_info['email_id']);

                    #Setting session after Signup
                    $sessionArr = setSessionVariables($postDataArr, $accessToken);

                    /*
                     * Insert Session Data
                     */

                    $user_info['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];

                    $whereArr = [];
                    $device_id = isset($postDataArr['device_id']) ? $postDataArr['device_id'] : "";
                    $isExist = array();
                    if ($device_id) {
                        $whereArr['where'] = ['device_id' => $device_id];
                        $isExist = $this->Common_model->fetch_data('user_device_details', array('session_id'), $whereArr, true);
                    }

                    #register device arn
                    if (isset($postDataArr['device_token']) && !empty($postDataArr['device_token'])) {
                        //if device token is set generate end point ern
                        $sns = new Snspush();
                        $response = $sns->addDeviceEndPoint($postDataArr['device_token'], $postDataArr['platform']);
                        if ($response["success"]) {
                            if ($response["success"]) {
                                $sessionArr["end_point_ern"] = $response["result"]["EndpointArn"];
                            }
                        }
                    }
                    if ($user_info['is_number_verified'] == '0') {
                        if (REFERAL_BONUS) {
                            #valid referal check
                            $valid_referal = false;

                            $whereArry['where'] = ['user_id' => $userId];
                            $valid_referal_check = $this->Common_model->fetch_data('ipac_referal_user', ['referal_user_id'], $whereArry, true);
                            if (!empty($valid_referal_check)) {
                                $whereArry['where'] = ['user_id' => $valid_referal_check['referal_user_id'], 'is_active' => ACTIVE];
                                $valid_referal_check = $this->Common_model->fetch_data('users', ['user_id'], $whereArry, true);
                                if (!empty($valid_referal_check)) {
                                    $valid_referal = true;
                                }
                            }

                            if ($valid_referal == true) {
                                $start_date = date('Y-m-01');
                                $end_date = date('Y-m-t');
                                //earning table
                                $earningArr = array(
                                    'user_id' => $valid_referal_check['user_id'],
                                    'task_id' => 0,
                                    'type' => 'referal',
                                    'month_start_date' => $start_date,
                                    'month_end_date' => $end_date,
                                    'total_earned_points' => REFERAL_BONUS_POINT,
                                    'reward_payment_amt' => REFERAL_BONUS_POINT,
                                    'reward_clear_status' => 0,
                                    'inserted_on' => date('Y-m-d H:i:s'),
                                );
                                //earning table insert
                                $earningSuccess = $this->db->insert('user_earnings', $earningArr);
                                if ($earningSuccess) {
                                    $refWhereArr['where'] = ['user_id' => $valid_referal_check['user_id']];

                                    #fetching user details from db
                                    $referedUserInfo = $this->Common_model->fetch_data(
                                        'users',
                                        ['points_earned', 'total_earning',],
                                        $refWhereArr,
                                        true
                                    );
                                    //updating user value
                                    $updateUserArray = array(
                                        'total_earning' => ($referedUserInfo['total_earning'] + REFERAL_BONUS_POINT),
                                        'points_earned' => ($referedUserInfo['points_earned'] + REFERAL_BONUS_POINT),
                                        'last_earning_update' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    );
                                    $whereUserArr['where'] = ['user_id' => $valid_referal_check['user_id']];
                                    $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
                                    $user_id = $valid_referal_check['user_id'];
                                    $this->unlockUserlevels($user_id);
                                }

                                //Insert in wallet log history
                                $insertWalletLog = array(
                                    'user_id' => $valid_referal_check['user_id'],
                                    'point' => REFERAL_BONUS_POINT,
                                    'title' => $this->lang->line('bonus_earned'),
                                    'description' => $this->lang->line('referal_bonus_earned'),
                                    'task_id' => 0,
                                    'type' => 'referal_bonus',
                                    'status' => COMPLETE,
                                    'created_date' => date('Y-m-d H:i:s'),

                                ); 
                                #save users details values in DB
                                $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                            }
                        }
                    }

                    /*
                     * If user has logged in previously with same device then update his detail
                     * or insert as a new row
                     */

                    if (!empty($isExist)) {
                        #updating session table
                        $logData                    = array();
                        $logData['where']           = $whereArr;
                        $logData['updateData']      = $sessionArr;
                        $logData['fetchData']       = $isExist;
                        $this->common_function->writeDeviceDetailsLog('update','api :: register user >> Otp_Verify_post Before Updating',$logData);
                        $sessionId = $this->Common_model->update_single('user_device_details', $sessionArr, $whereArr);

                        $logData                    = array();
                        $logData['fetchData']       = $this->Common_model->fetch_data('user_device_details','', $whereArr, true);
                        $this->common_function->writeDeviceDetailsLog('update','api :: register user >> Otp_Verify_post After Updating ',$logData);
                    } else {
                        #inserting session details
                        $this->common_function->writeDeviceDetailsLog('insert','api :: register user >> Otp_Verify_post ',$sessionArr);
                        $sessionId = $this->Common_model->insert_single('user_device_details', $sessionArr);
                    }

                    //logs array
                    $logsInsArray = array(
                        'user_id' => $userId,
                        'active_date' => datetime(),
                        'device_id' => isset($postDataArr['device_id']) ? $postDataArr['device_id'] : "",
                        'type' => LOGIN,
                        'ip_address' => ip2long($_SERVER['REMOTE_ADDR']),
                    );
                    #updating logs details
                    $isSuccess = $this->Common_model->insert_single('user_session_logs', $logsInsArray);
                    $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => $this->lang->line('otp_verified'), 'RESULT' => (object) $user_info));
                } else {
                    $response_array = [
                        'CODE' => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                }
            } else {
                $response_array = [
                    'CODE' => RECORD_NOT_EXISTS,
                    'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                    'RESULT' => (object) array(),
                ];

                $this->response($response_array);
            }
        } else {
            $err = $this->form_validation->error_array();
            $arr = array_values($err);
            $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
        }
    }

   
    public function unlockUserlevels($user_id)
    {
        
        $return_level = array();
        $sql = "SELECT points_earned FROM users WHERE user_id = ?";
        $query = $this->db->query($sql, array($user_id));
        $resultArray = $query->row_array();
        $user_wallet_amount = $resultArray['points_earned'];
        $current_points = $user_wallet_amount;
        
        $new_levels = $this->db->query("SELECT MAX(fk_level_id) as fk_level_id from tbl_user_level where fk_user_id = $user_id");
        $new_levels = $new_levels->row_array();
        $current_level = $new_levels['fk_level_id'] ? $new_levels['fk_level_id'] : '0';
        
                                
        if ($current_level != 'null') {
            $level_array = $this->nextlevel($current_points, $current_level);
            $difference = $level_array['max_level'] - $current_level;
            if ($difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $difference; $i++) {
                    $fk_level_id = $current_level + $i;
                    $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                    $reward_points = $reward_points->row_array();
                    $free_rewards =  $reward_points['free_reward_points'];
                    if($free_rewards != 0){
                        $params                 = array();
                        $params['points']       = $free_rewards;
                        $params['user_id']      = $user_id;
                        $res                    = $this->User_model->updateUserEarnPoints($params);

                        $walletInsertArr[]      = array(
                            'user_id'           => $user_id,
                            'point'             => $free_rewards,
                            'title'             => 'New Level Rewards',
                            'description'       => 'Rewards earned for completion of levels',
                            'task_id'           => '0',
                            'type'              => 'bonus',
                            'status'            => '1',
                            'created_date'      => date('Y-m-d H:i:s'),
                            'updated_date'      => date('Y-m-d H:i:s')
                        );
                        
                    }
                    
                    $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
                }
               
            }
        }
                    
            if(!empty($walletInsertArr) && count($walletInsertArr) > 0){
               	$this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
           	}
                  
        return $return_level;
    }
    
    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }
}
