<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class JoinDMK extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
        # Don't send any state or country code for getting the list of country
        try {
            $response_array = [
                'CODE' => SUCCESS_CODE,
                'MESSAGE' => $this->lang->line('got_url'),
                'RESULT' => 'https://dmk.in/joindmk'
            ];
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('error'),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }
}
