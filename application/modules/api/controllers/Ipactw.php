<?php

defined ( 'BASEPATH' ) OR exit ( 'No direct script access allowed' );
require APPPATH . '/libraries/REST_Controller.php';

class Ipactw extends REST_Controller {

    function __construct ()
    {
        
        parent::__construct ();
        $this->load->model ( 'Common_model');
        $this->load->helper ( 'email' );
    }

    public function index_post ()
    {
            
            $input = file_get_contents('php://input');
            
            $data = array('twitter_data '=> $input);
            $this->Common_model->insert_single ( 'temp', $data );
            
            $data = json_decode($input);
            $value1 = json_decode(json_encode($data), true);
            
            $event = '';
            foreach($value1  as  $key => $val)
            {
                if ($key == 'favorite_events' || $key == 'tweet_create_events')
                {
                    $event = $key;
                }
                
            }
            $user_id =  $value1[$event][0]['user']['id'];
            $user_id_str =  $value1[$event][0]['user']['id_str'];
            # if user favourite a status
            if ($event == 'favorite_events')
            {
              $tweet_id = $value1[$event][0]['favorited_status']['id'];  
              $tweet_id_str = $value1[$event][0]['favorited_status']['id_str']; 
              $action = 'favourite'; 
            }else if($event == 'tweet_create_events' && $value1[$event][0]['is_quote_status'] == 1)
            {
                # if user retweeted a status
                $tweet_id = $value1[$event][0]['quoted_status_id'];  
                $tweet_id_str = $value1[$event][0]['quoted_status_id_str']; 
                $action = 'retweet';  
                
            }
            
            #get the task id based on the action performed by user
            $task_cond_where = array('where' => array('post_id' => $tweet_id,'action' => $action));


            $taskInfo = $this->Common_model->fetch_data('ipac_task_master', ['task_id', 'task_status','start_date','end_date','points'], $task_cond_where, true);

            if($taskInfo)
            {
               #check the user id according to fb id 
               $user_cond_where = array('where' => array('twitter_id' => $user_id));
               $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active'], $user_cond_where, true);

               if($userInfo)
               {
                    # check wheather points added for this task or not
                    $task_master_where = array('where' => array('task_id ' => $taskInfo['task_id'],'user_id'=>$userInfo['user_id']));
                    $usertaskInfo = $this->Common_model->fetch_data('user_task_master', ['detail_id'], $task_master_where, true);
                    if(!$usertaskInfo)
                    {
                        # insert user task completed data
                        $taskArr = array(
                            'user_id' => $userInfo['user_id'],
                            'task_id' => $taskInfo['task_id'],
                            'status' => 1,
                            'added_on' => date('Y-m-d H:i:s')
                        );
                        $usertaskSuccess = $this->Common_model->insert_single('user_task_master', $taskArr);

                        # insert user earnings data

                        $start_date    = date('Y-m-01') ;
                        $end_date    = date('Y-m-t');

                        $earningArr = array(
                            'user_id' => $userInfo['user_id'],
                            'month_start_date' => $start_date,
                            'month_end_date' => $end_date,
                            'total_earned_points' => $taskInfo['points'],
                            'reward_payment_amt' => $taskInfo['points'],
                            'reward_clear_status' => 0,
                            'inserted_on' => date('Y-m-d H:i:s')
                        );

                        $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                    }

               }



            }     
                  
            
    }
    
    public function  index_get ()
    {  
        if (isset($_GET['crc_token'])) {
            header('Content-Type: application/json');

           $hash = hash_hmac('sha256', $_GET['crc_token'], 'M1pN6KTH6ai8zNjCaEzOkyNlMdoZf816BVLx4Cgthbh7CzqlFI', true);
     
            $response = array(
              'response_token' => 'sha256=' . base64_encode($hash)
            );
            echo json_encode($response);
            
         }
    }

}
?>
