<?php

/*
 * This class file used to Address Boom Managment of USER
 *
 */

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/Rcc_Controller.php';

class Address_book extends Rcc_Controller
{

    public function __construct()
    {
        parent::__construct();
    }



    public function index_get()
    {
        #try Start
        try {
            #Getting Values from GET
            $getDataArr = $this->get();

            #setting Default values to array KEYS
            $default = array(
                "limit" => FRIEND_LIMIT,
                "page"  => 1
            );

            #setting Default value array
            $defaultValue = defaultValue($getDataArr, $default);

            $page   = intval($defaultValue['page']);
            $offset = ($page - 1) * intval($defaultValue['limit']);

            $this->load->model('address');
            $data = $this->address->get_address_book($GLOBALS['api_user_id'], $defaultValue['limit'], $offset);
            if ($data['totalrows'] > (intval($defaultValue['page']) * $defaultValue['limit'])) {
                $page++;
            } else {
                $page = 0;
            }

            #If Result list is not empty
            if (!empty($data['result'])) {
                $response_array = [
                    'CODE'       => SUCCESS_CODE,
                    'MESSAGE'    => $this->lang->line('address_fetched'),
                    'NEXT_PAGE'  => $page,
                    'TOTAL_ROWS' => $data['totalrows'],
                    'RESULT'     => $data['result']
                ];
            } else {
                $response_array = [
                    'CODE'    => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('no_data_found'),
                    'RESULT'  => (object) []
                ];
            }
            $this->response($response_array);
        } #catch Start
        catch (Exception $e) {
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT'  => (object) []
            ];
            $this->response($response_array);
        } #catch Ends

    }



    public function add_address_put()
    {
        #try Start
        try {
            #Getting Values from PUT
            $getDataArr = $this->put();

            #response Array
            $response_array = [];

            #setting Default values to array KEYS
            $default      = array(
                'country_id'    => '',
                'user_id'       => $GLOBALS['api_user_id'],
                'state_id'      => '',
                'city_name'     => '',
                'full_name'     => '',
                'mobile_number' => '',
                'pin_code'      => '',
                'address_line1' => '',
                'address_line2' => '',
                'near_by'       => ''
            );
            $defaultValue = defaultValue($getDataArr, $default);


            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'country_id',
                    'label' => 'Country ID',
                    'rules' => 'trim|required|numeric'
                ), array(
                    'field' => 'state_id',
                    'label' => 'State ID',
                    'rules' => 'trim|required|numeric'
                ), array(
                    'field' => 'city_name',
                    'label' => 'City Name',
                    'rules' => 'trim|required'
                ), array(
                    'field' => 'full_name',
                    'label' => 'Full Name',
                    'rules' => 'trim|required'
                ), array(
                    'field' => 'mobile_number',
                    'label' => 'Mobile Number',
                    'rules' => 'trim|required|numeric'
                ), array(
                    'field' => 'pin_code',
                    'label' => 'Pin Code',
                    'rules' => 'trim|required|numeric|min_length[6]|max_length[6]'
                ), array(
                    'field' => 'address_line1',
                    'label' => 'Address Line One',
                    'rules' => 'trim|required'
                ), array(
                    'field' => 'address_line2',
                    'label' => 'Address Line Two',
                    'rules' => 'trim'
                ), array(
                    'field' => 'near_by',
                    'label' => 'near By',
                    'rules' => 'trim'
                )
            );

            #setting validation data array
            $set_data = [
                'country_id'    => $defaultValue['country_id'],
                'state_id'      => $defaultValue['state_id'],
                'full_name'     => $defaultValue['full_name'],
                'mobile_number' => $defaultValue['mobile_number'],
                'pin_code'      => $defaultValue['pin_code'],
                'address_line1' => $defaultValue['address_line1'],
                'address_line2' => $defaultValue['address_line2'],
                'near_by'       => $defaultValue['near_by'],
                'city_name'     => $defaultValue['city_name']
            ];

            #seting data
            $this->form_validation->set_data($set_data);

            #Setting Error rules
            $this->form_validation->set_rules($required_fields_arr);
            #Setting Error Messages for rules
            $this->form_validation->set_message('required', $this->lang->line('msg_sfx') . '%s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $defaultValue['added_date'] = date('Y-m-d H:i:s');
                $this->db->trans_begin(); #db transaction starts

                $address_id = $this->Common_model->insert_single('address_book', $defaultValue); #insert data in DB
                if ($this->db->trans_status()) {
                    // commit transaction
                    $this->db->trans_commit();
                    #setting Success response array
                    $response_array = [
                        'CODE'    => SUCCESS_CODE,
                        'MESSAGE' => "Address Added Successfully",
                        'RESULT'  => [
                            "address_id" => $address_id
                        ]
                    ];
                } else {
                    #rolling back
                    $this->db->trans_rollback();

                    #setting TRY Again response array
                    $response_array = [
                        'CODE'    => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('try_again'),
                        'RESULT'  => (object) []
                    ];
                }
            } #form validation End
            else {
                $err            = $this->form_validation->error_array();
                $arr            = array_values($err);
                $response_array = [
                    'CODE'    => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT'  => (object) array()
                ];
            }
            $this->response($response_array);
        } #catch Start
        catch (Exception $e) {
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT'  => (object) []
            ];
            $this->response($response_array);
        } #catch Ends

    }



    public function update_address_post()
    {
        #try start
        try {
            #Getting Values from POST
            $getDataArr = $this->post();

            #response Array
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'address_id',
                    'label' => 'Address ID',
                    'rules' => 'trim|required|numeric'
                )
            );

            #Setting Validation rules
            $this->form_validation->set_rules($required_fields_arr);

            #Setting Error Messages for rules
            $this->form_validation->set_message('required', $this->lang->line('msg_sfx') . '%s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                #where condition
                $where                      = [
                    "where" => [
                        'id'      => $getDataArr['address_id'],
                        'user_id' => $GLOBALS['api_user_id']
                    ]
                ];
                $getDataArr['updated_date'] = date('Y-m-d H:i:s');
                $this->db->trans_begin(); #db transaction starts
                unset($getDataArr['address_id']);

                $updated = $this->common_model->update_single('address_book', $getDataArr, $where);

                if ($this->db->trans_status()) {
                    // commit transaction
                    $this->db->trans_commit();
                    #setting Success response array
                    $response_array = [
                        'CODE'    => SUCCESS_CODE,
                        'MESSAGE' => "Address Updated Successfully",
                        'RESULT'  => (object) []
                    ];
                } else {
                    #rolling back
                    $this->db->trans_rollback();

                    #setting TRY Again response array
                    $response_array = [
                        'CODE'    => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('try_again'),
                        'RESULT'  => (object) []
                    ];
                }
            } #form validation End
            else {
                $err            = $this->form_validation->error_array();
                $arr            = array_values($err);
                $response_array = [
                    'CODE'    => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT'  => (object) []
                ];
            }
            $this->response($response_array);
        } #catch Start
        catch (Exception $e) {
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT'  => (object) []
            ];
            $this->response($response_array);
        } #catch Ends

    }



    public function delete_address_delete()
    {
        try {
            #getting data from PUT
            $getDataArr     = $this->delete();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'address_id',
                    'label' => 'Address Id',
                    'rules' => 'trim|required|numeric'
                )
            );

            $set_data = [
                'address_id' => $getDataArr['address_id']
            ];

            $this->form_validation->set_data($set_data);

            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', $this->lang->line('msg_sfx') . '%s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $this->db->trans_begin(); #db transaction starts
                #where condition
                $where = [
                    "where" => [
                        'id'      => $getDataArr['address_id'],
                        'user_id' => $GLOBALS['api_user_id']
                    ]
                ];

                $this->common_model->delete_data('address_book', $where);

                if ($this->db->trans_status()) {
                    // commit transaction
                    $this->db->trans_commit();
                    #setting Success response array
                    $response_array = [
                        'CODE'    => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line("address_fetched"),
                        'RESULT'  => (object) []
                    ];
                } else {
                    #rolling back
                    $this->db->trans_rollback();

                    #setting TRY Again response array
                    $response_array = [
                        'CODE'    => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('try_again'),
                        'RESULT'  => (object) []
                    ];
                }
            } #form validation End
            else {
                $err            = $this->form_validation->error_array();
                $arr            = array_values($err);
                $response_array = [
                    'CODE'    => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT'  => (object) array()
                ];
            }
            $this->response($response_array);
        } #catch Start
        catch (Exception $e) {
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT'  => (object) []
            ];
            $this->response($response_array);
        } #catch Ends

    }



    public function address_detail_post()
    {
        try {
            #getting data from POST
            $getDataArr     = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'address_id',
                    'label' => 'Address Id',
                    'rules' => 'trim|required|numeric'
                )
            );

            $set_data = [
                'address_id' => $getDataArr['address_id']
            ];

            $this->form_validation->set_data($set_data);

            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', $this->lang->line('msg_sfx') . '%s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {

                #loading Address Model
                $this->load->model("address");

                $data = $this->address->address_detail($GLOBALS['api_user_id'], $getDataArr['address_id']);
                if (count($data['result'])) {
                    #setting Success response array
                    $response_array = [
                        'CODE'    => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line("address_fetched"),
                        'RESULT'  => (object) $data['result']
                    ];
                } else {
                    #setting Success response array
                    $response_array = [
                        'CODE'    => NO_DATA_FOUND,
                        'MESSAGE' => $this->lang->line("no_data_found"),
                        'RESULT'  => (object) []
                    ];
                }
            } #form validation End
            else {
                $err            = $this->form_validation->error_array();
                $arr            = array_values($err);
                $response_array = [
                    'CODE'    => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT'  => (object) []
                ];
            }
            $this->response($response_array);
        } #catch Start
        catch (Exception $e) {
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $e->getMessage(),
                'RESULT'  => (object) []
            ];
            $this->response($response_array);
        } #catch Ends

    }
}
