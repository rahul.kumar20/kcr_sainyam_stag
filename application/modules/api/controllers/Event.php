<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Event extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index_post()
    {

        try {      
            $params = $this->post();
            $startDate = $params['start_date'];
            $endDate = $params['end_date'];
            $whereArr = [];
            $table = 'ipac_event';
            $whereArr['where'] = ['eventDate >='=>  $startDate,'eventDate <='=>$endDate];
            $listData = $this->Common_model->fetch_data($table, array("*"), $whereArr);

            #If Result list is not empty
            if (!empty($listData)) {
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => EMAIl_SEND_FAILED,
                'MESSAGE' => $e->getMessage(),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }
}
