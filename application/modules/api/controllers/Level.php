<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Level extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model("Wallet_history_model", "wallet");
        $this->load->database();
    }

    public function index_get()
    {
        try {
            $USER_ID = $GLOBALS['api_user_id'];
            $userInformation = $GLOBALS['login_user'];
            $wherecond = array('where' => array('user_id' => $USER_ID));
            $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active', 'state', 'district', 'gender', 'registeration_no',
                'full_name', 'user_image', '0 as rating', 'registered_on', 'paytm_number', 'upi_address', 'account_number'], $wherecond, true);

            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
                    $userArray = array(
                        'total_point' => $userInformation['userinfo']['total_earning'],
                        'paytm_number' => $userInformation['userinfo']['paytm_number'],
                        'upi_address' => $userInformation['userinfo']['upi_address'],
                        'bank_account' => $userInformation['userinfo']['account_number'],
                        'default_option' => $userInformation['userinfo']['default_payment_option'],
                        'updated_date' => !is_null($userInformation['userinfo']['last_earning_update']) ? $userInformation['userinfo']['last_earning_update'] : '',
                        'redeemable_status' => REDEMMABLE,
                        'updated_expense_date' => ($userInformation['userinfo']['last_expense_date'] != '0000-00-00 00:00:00') ? $userInformation['userinfo']['last_expense_date'] : '',
                    );

                    $data = array();
                    $data1 = array();
                    $user_wallet_amount = 0;
                    $sql = "SELECT points_earned as total_erned_points FROM users WHERE user_id = ?";
                    $query = $this->db->query($sql, array($USER_ID));
                    $user_wallet = $query->result_array();
                    $user_wallet_amount = $user_wallet[0]['total_erned_points'];

                    $sqlLevel = "SELECT CONCAT('Level ', l.pk_level_id) AS level_tag_name,l.pk_level_id, l.name,l.name_tn,l.start_point,l.end_point,l.free_reward_points,l.incentive,l.image,l.description,
                        ul.pk_id as user_level_id,ul.unlock_date,ul.incentive_redeem_status,
                        IF(l.end_point <= ?, 'yes', 'no') AS can_redeem
                        FROM tbl_levels AS l
                        LEFT JOIN tbl_user_level AS ul
                        ON l.pk_level_id = ul.fk_level_id AND ul.fk_user_id = ?
                        where l.estatus = 'active' order by l.pk_level_id ASC";
                    $queryLevel = $this->db->query($sqlLevel, array($user_wallet_amount, $USER_ID));
                    $levels = $queryLevel->result_array();
                    if (count($levels)) {
                        foreach ($levels as $level) {
                            $sqlAchievement = "SELECT * from tbl_achievement where eStatus='active' and type = 1 and fk_level_id = ?";
                            $queryAchievement = $this->db->query($sqlAchievement, array($level['pk_level_id']));
                            $level['achievement'] = $queryAchievement->result_array();
                            $level['is_unlocked'] = false;
                            if ($level['unlock_date'] != null) {
                                $level['is_unlocked'] = true;
                            }
                            $data[] = $level;
                        }
                    }

                    $sqlAchievementListing = "SELECT a.*,
                        ua.unlock_date,
                        l.NAME  AS level_name,
                        ( CASE
                            WHEN a.fk_level_id IS NOT NULL THEN Concat('Level ', a.fk_level_id)
                            ELSE ''
                        END ) AS level_tag_name
                        FROM   tbl_achievement a
                                LEFT JOIN tbl_user_achievement AS ua
                                    ON a.pk_achievement_id = ua.fk_achievement_id
                                        AND ua.fk_user_id = ?
                                LEFT JOIN tbl_levels AS l
                                    ON l.pk_level_id = a.fk_level_id
                        WHERE  a.estatus = 'active'
                        ORDER  BY a.pk_achievement_id ASC  ";
                    $queryAchievementListing = $this->db->query($sqlAchievementListing, array($USER_ID));
                    $achievements = $queryAchievementListing->result_array();
                    if (count($achievements)) {
                        foreach ($achievements as $achievement) {
                            $achievement['is_unlocked'] = false;
                            if ($achievement['unlock_date'] != null) {
                                $achievement['is_unlocked'] = true;
                            }
                            $data1[] = $achievement;
                        }
                    }
                    $walletHistorDetail = $this->wallet->getWalletHistoryDetail($USER_ID);
                    $walletDetail = $walletHistorDetail['data'];
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => 'success',
                        'valid' => true,
                        'total_wallet_amount' => $user_wallet_amount,
                        'USERINFO' => $userArray,
                        'WALLET_DETAIL' => $walletDetail,
                        'levels' => $data,
                        'achievement' => $data1,
                    ];
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => [],
                    ];
                }
            }

            echo json_encode($response_array);
            die;
        } #TRY END
         catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

}
