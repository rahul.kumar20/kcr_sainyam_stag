<?php

defined ( 'BASEPATH' ) OR exit ( 'No direct script access allowed' );
require APPPATH . '/libraries/REST_Controller.php';

class Facebook_Webhook extends REST_Controller {

    function __construct ()
    {
        parent::__construct ();
        $this->load->model ( 'Common_model' );
        $this->load->helper ( 'email' );

    }

    public function index_post ()
    { 
        #$input = json_decode(file_get_contents('php://input'), true);
        $input = file_get_contents('php://input');

        $data = array('fb_data '=> $input);
        $this->Common_model->insert_single ( 'temp', $data );
            
            
//        #decode facebook response    
//        $data = json_decode($input);
//        $postId  = $item = $verb = $userFbId = '';
//        $value1 = json_decode(json_encode($data), true);
//        $changes = $value1['entry'][0]['changes'][0];
//        
//        # get the type of action performed by user i.e like,share
//        if(array_key_exists('item', $changes['value']))
//        {
//            $item = $changes['value']['item'];
//            $verb = $changes['value']['verb'];
//        }
//        #get the id of user who performed the action
//        if(array_key_exists('from', $changes['value']))
//        {
//           $userFbId= $changes['value']['from']['id'];
//        }
//        #get post id on which action is performed
//        if(array_key_exists('post_id', $changes['value']))
//        {
//            $postIdArr = explode("_",$changes['value']['post_id']);
//            $postId = $postIdArr[1];
//        }
//        
//        #get the task id based on the action performed by user
//        $task_cond_where = array('where' => array('post_id' => $postId,'action' => $item));
//        
//        
//        $taskInfo = $this->Common_model->fetch_data('ipac_task_master', ['task_id', 'task_status','start_date','end_date','points'], $task_cond_where, true);
//      
//        if($taskInfo)
//        {
//           #check the user id according to fb id 
//           $user_cond_where = array('where' => array('facebook_id' => $userFbId));
//           $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active'], $user_cond_where, true);
//          
//           if($userInfo)
//           {
//                # check wheather points added for this task or not
//                $task_master_where = array('where' => array('task_id ' => $taskInfo['task_id'],'user_id'=>$userInfo['user_id']));
//                $usertaskInfo = $this->Common_model->fetch_data('user_task_master', ['detail_id'], $task_master_where, true);
//                if(!$usertaskInfo)
//                {
//                    # insert user task completed data
//                    $taskArr = array(
//                        'user_id' => $userInfo['user_id'],
//                        'task_id' => $taskInfo['task_id'],
//                        'status' => 1,
//                        'added_on' => date('Y-m-d H:i:s')
//                    );
//                    $usertaskSuccess = $this->Common_model->insert_single('user_task_master', $taskArr);
//                    
//                    # insert user earnings data
//                    
//                    $start_date    = date('Y-m-01') ;
//                    $end_date    = date('Y-m-t');
//                   
//                    $earningArr = array(
//                        'user_id' => $userInfo['user_id'],
//                        'month_start_date' => $start_date,
//                        'month_end_date' => $end_date,
//                        'total_earned_points' => $taskInfo['points'],
//                        'reward_payment_amt' => $taskInfo['points'],
//                        'reward_clear_status' => 0,
//                        'inserted_on' => date('Y-m-d H:i:s')
//                    );
//                  
//                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
//                }
//               
//           }    
//           
//        }
            
            
            return "Fb Webhook";  
            
    }
    
     public function index_get ()
    {
            $getDataArr     = $this->get ();
            $challenge = $_REQUEST['hub_challenge'];
            $verify_token = $_REQUEST['hub_verify_token'];
            // Set this Verify Token Value on your Facebook App 
            if ($verify_token === '2994583b1183157b95131e9f656129e4') {
               $data = array('fb_data '=>'2994583b1183157b95131e9f656129e4');
               $this->Common_model->insert_single ( 'temp', $data );
              echo $challenge;
            }else{
                echo 'Invalid verification token';
            }
            
//            $updates = json_decode(file_get_contents('php://input'), true);
////            $filename = "fblog.txt";
////            $myfile = fopen($filename, "w");
////            fwrite($myfile,('updates='.print_r($updates,true)));
////            fwrite($myfile,"-----");
////            fwrite($myfile,$updates['entry']['changes']);
////            fclose($myfile);
//            return "Fb Webhook"; 
    }

  

}
?>
