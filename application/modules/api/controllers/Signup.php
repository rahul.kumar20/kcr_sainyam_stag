<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/SNSPush.php';

class Signup extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->helper('sendsms');
        $this->load->library('Common_function');
        $this->load->model('User_model');
        $this->load->model('admin/News_model');
    }

    public function index_post()
    {
        try {
            $postDataArr = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'user_id',
                    'label' => 'User Id',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'proof_type',
                    'label' => 'Proof Type',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'proof_image',
                    'label' => 'Proof Image',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'id_number',
                    'label' => 'Id Number',
                    'rules' => 'trim|required',
                ),
            );

            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $default = array();
                $signupArr = defaultValue($postDataArr, $default);

                #Check if user registration no already registered
                $whereArr = [];

                # If users enters email for signup
                if (!empty($postDataArr['user_id'])) {
                    $whereArr['where'] = ['user_id' => $postDataArr['user_id']];
                }

                $user_info = $this->Common_model->fetch_data('users', [
                    'user_id', 'registeration_no', 'full_name', 'email_id', 'facebook_id',
                    'twitter_id', 'user_image', 'whatsup_number', 'paytm_number', 'phone_number', 'state', 'district', 'ac', 'party_name', 'is_active'
                ], $whereArr, true);

                #is user Blocked
                if (!empty($user_info) && 0 == $user_info['is_active']) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                } elseif (empty($user_info)) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('no_user'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                } else {
                    $this->db->trans_begin(); #DB transaction Start

                    $userId = $user_info['user_id'];

                    $postDataArr['user_id'] = $userId;

                    $user_info['accesstoken'] = '';

                    #save proof details
                    $proofData = array();
                    $proofImage = explode(",", $postDataArr['proof_image']);

                    if (!empty($proofImage)) {
                        foreach ($proofImage as $key => $val) {
                            $proofData['user_id'] = $userId;
                            $proofData['id_number'] = $postDataArr['id_number'];
                            $proofData['proof_type'] = $postDataArr['proof_type'];
                            $proofData['image'] = $val;
                            $proofData['inserted_on'] = date('Y-m-d H:i:s');

                            $proofId = $this->Common_model->insert_single('user_id_proof_details', $proofData);
                        }
                    }

                    #comminting Transaction
                    $this->db->trans_commit();

                    if (!empty($proofId) && !empty($userId)) {
                        #setting response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('registration_success'),
                            'RESULT' => $user_info,
                        ];
                    }
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {
            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => (object) array(),
            ];

            #sending response
            $this->response($response_array);
        } #Catch End
    }

    /**
     * @SWG\Post(path="/Signup",
     *   tags={"User"},
     *   summary="Singup Information",
     *   description="Singup Information",
     *   operationId="index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     description="Users first name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     description="Users last name",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="gender",
     *     in="formData",
     *     description="1: Male, 2: Female",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="Phone Number",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="dob",
     *     in="formData",
     *     description="Date of Birth m/d/Y",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address",
     *     in="formData",
     *     description="Address",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="user_lat",
     *     in="formData",
     *     description="Lattitude",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="user_long",
     *     in="formData",
     *     description="Longitude",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country_id",
     *     in="formData",
     *     description="country Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state_id",
     *     in="formData",
     *     description="State Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city_id",
     *     in="formData",
     *     description="City Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_id",
     *     in="formData",
     *     description="Unique Device Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_token",
     *     in="formData",
     *     description="Device Token required to send push",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="platform",
     *     in="formData",
     *     description="1: Android and 2: iOS",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Signup Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=208, description="Phone number alredy exists"),
     *   @SWG\Response(response=421, description="File Upload Failed"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function registration_post()
    {
        try {
            $postDataArr = $this->post();
            $response_array = [];

            #setting Form validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'full_name',
                    'label' => 'Name',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'email_id',
                    'label' => 'Email',
                    'rules' => 'trim|valid_email',
                ),
                array(
                    'field' => 'phone_number',
                    'label' => 'Phone',
                    'rules' => 'trim|required|numeric',
                ),
                array(
                    'field' => 'whatsup_number',
                    'label' => 'WhatsApp Number',
                    'rules' => 'trim|numeric',
                ),
                array(
                    'field' => 'gender',
                    'label' => 'gender',
                    'rules' => 'trim|required|numeric',
                ),
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'dob',
                    'label' => 'Date of birth',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'state',
                    'label' => 'States',
                    'rules' => 'trim|numeric|required',
                ),
                array(
                    'field' => 'district',
                    'label' => 'District',
                    'rules' => 'trim|numeric|required',
                ),
                array(
                    'field' => 'referal_code',
                    'label' => 'Referal Code',
                    'rules' => 'trim|max_length[25]|min_length[6]',
                )
            );

            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('is_unique', 'The %s is already registered with us');
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {

                #setting Default values to array KEYS
                $default = array(
                    "full_name" => "",
                    "email_id" => "",
                    "phone_number" => "",
                    "whatsup_number" => "",
                    "gender" => "",
                    "password" => "",
                    "dob" => "",
                    "college" => "",
                    "state" => "",
                    "district" => "",
                    'ac' => "",
                    'party_name' => "",
                    "user_image" => "",
                    "is_user_college_student" => "",
                    "referal_code" => "",
                    "app_version" => "",
                );

                $signupArr = defaultValue($postDataArr, $default);

                #checking phone number is valid or not
                if ("" != $signupArr['phone_number']) {
                    $this->validate_phone($signupArr['phone_number']);
                }

                #Checking date of birth is valid or not
                if ("" != $signupArr['dob']) {
                    $this->validate_dob($signupArr['dob']);
                }

                #valid referal check
                $valid_referal = false;
                if (isset($signupArr['referal_code']) && !empty($signupArr['referal_code'])) {
                    $whereArry['where'] = ['referal_code' => $signupArr['referal_code'], 'is_active' => ACTIVE];
                    $valid_referal_check = $this->Common_model->fetch_data('users', ['user_id'], $whereArry, true);
                    #if referal code is invalid
                    if (empty($valid_referal_check)) {
                        #Invalid referal code
                        $response_array = [
                            'CODE' => INVALID_REFERAL_CODE,
                            'MESSAGE' => $this->lang->line('invalid_referal'),
                            'RESULT' => (object) array(),
                        ];
                        echo json_encode($response_array);
                        die;
                    } else {
                        //valid referal code
                        $valid_referal = true;
                    }
                }

                #Check if email if already registered and is it blocked
                $whereArr = [];
                # If users enters email for signup
                /*if (!empty($signupArr['email_id'])) {
                    $whereArr['where'] = ['email_id' => $signupArr['email_id']];
                }*/

                # If users enters phone number along with email id for signup
                /*if (isset($whereArr['where']) && !empty($signupArr['phone_number'])) {
                    $whereArr['or_where'] = ['phone_number' => $signupArr['phone_number']];
                }*/

                if (!empty($signupArr['phone_number'])) {
                    $whereArr['or_where'] = ['phone_number' => $signupArr['phone_number']];
                }

                $user_info = $this->Common_model->fetch_data('users', ['phone_number', 'whatsup_number', 'email_id', 'is_active', 'is_number_verified'], $whereArr, true);

                #is user Blocked
                if (!empty($user_info) && 2 == $user_info['is_active']) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                } elseif (!empty($user_info) && !empty($user_info['phone_number']) && ($user_info['phone_number'] == $signupArr['phone_number'])) {
                    #user is already Registered
                    $response_array = [
                        'CODE' => PHONE_ALREADY_EXISTS,
                        'MESSAGE' => $this->lang->line('phone_exist'),
                        'RESULT' => (object) array(),
                    ];
                    /*} elseif (!empty($user_info) && ($user_info['email_id'] == $signupArr['email_id'])) {
                    #user is already Registered
                    $response_array = [
                        'CODE' => EMAIL_ALREADY_EXIST,
                        'MESSAGE' => $this->lang->line('account_exist'),
                        'RESULT' => (object) array(),
                    ];*/
                } elseif (!empty($user_info) && !empty($user_info['whatsup_number']) && ($user_info['whatsup_number'] == $signupArr['whatsup_number'])) {
                    #user is already Registered
                    $response_array = [
                        'CODE' => PHONE_ALREADY_EXISTS,
                        'MESSAGE' => $this->lang->line('whats_app_already_exist'),
                        'RESULT' => (object) array(),
                    ];
                } else {
                    $signupArr["inclined_party"] = isset($postDataArr['inclined_party']) ? $postDataArr['inclined_party'] : '';
                    $signupArr["password"] = createPassword($postDataArr["password"]);
                    //$signupArr['registeration_no'] = $this->generateRegistrationNo($postDataArr['district']);
                    $signupArr["registered_on"] = date('Y-m-d H:i:s');
                    $referalCode = $this->referalCodeGenerator($postDataArr['full_name']);
                    $signupArr["referal_code"] = $referalCode;
                    $signupArr["is_profile_edited"] = 1;
                    $signupArr["requested_for_edit_profile"] = 0;
                    $signupArr["dob"] = date('Y-m-d', strtotime($signupArr['dob']));
                    $signupArr['dmk_member'] = $postDataArr['dmk_member'];
                    $signupArr['dmk_id'] = $postDataArr['dmk_id'];
                    $signupArr["block"] = isset($postDataArr['block']) ? $postDataArr['block'] : '';
                    $signupArr["ward_panchayat"] = isset($postDataArr['ward_panchayat']) ? $postDataArr['ward_panchayat'] : '';
                    $signupArr["ward_panchayat_other"] = isset($postDataArr['ward_panchayat_other']) ? $postDataArr['ward_panchayat_other'] : '';
                    $signupArr["block_other"] = isset($postDataArr['block_other']) ? $postDataArr['block_other'] : '';
                    $this->db->trans_begin(); #DB transaction Start

                    $userId = $this->Common_model->insert_single('users', $signupArr); #save users details values in DB
                    if (!$userId) {
                        throw new Exception($this->lang->line('try_again'));
                    }
                    // if valid referal code is
                    if ($valid_referal == true) {
                        //referal data insert array
                        $referalInsert = array(
                            "user_id" => $userId,
                            "referal_user_id" => $valid_referal_check['user_id'],
                            "created_date" => date('Y-m-d H:i:s'),
                        );
                        $this->Common_model->insert_single('ipac_referal_user', $referalInsert); #save users details values in DB
                    }

                    $postDataArr['user_id'] = $userId;

                    #Access Token for signup
                    $signupArr['accesstoken'] = '';
                    $signupArr["image"] = isset($signupArr['image']) ? IMAGE_PATH . $signupArr['image'] : "";
                    $signupArr["image_thumb"] = isset($signupArr['image_thumb']) ? THUMB_IMAGE_PATH . $signupArr['image_thumb'] : "";
                    $signupArr['user_id'] = $userId;

                    #Checking transaction status
                    if ($this->db->trans_status()) {
                        #comminting Transaction
                        $this->db->trans_commit();

                        if (!empty($userId)) { #if Start

                            $registrationId         = $this->getUserRegistrationNo($postDataArr['district'],$userId);
                            if($registrationId != ''){
                                $updateArr          = array();
                                $updateArr['registeration_no']  = $registrationId;
                                $updateCond         = array('user_id'=> $userId);
                                $updateRes          = $this->common_model->updateTableData($updateArr,'users',$updateCond);
                                
                                $signupArr['registeration_no'] = $registrationId;
                            }
                            
                            // Insert user level
                            // For reaching Level 1 we have some target points so.
                            //$this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => '1', 'fk_user_id' => $userId, 'unlock_date' => date('Y-m-d H:i:s')]);
                            unset($signupArr['password']);
                            //get term and condition detail
                            $where = array('where' => array('status' => ACTIVE));
                            $where['order_by'] = ['created_date' => 'desc'];
                            //term and conditions
                            $termConditionInfo = $this->Common_model->fetch_data('ipac_term_condition', ['version', 'get_term_condition_accept_status(' . $userId . ') as term_accept_status'], $where, true);
                            if (!empty($termConditionInfo)) {
                                $termsLogArray = array(
                                    'user_id' => $userId,
                                    'term_condition_version' => $termConditionInfo['version'],
                                    'app_version' => $signupArr['app_version'],
                                    'created_date' => date('Y-m-d H:i:s'),
                                );
                                $this->Common_model->insert_single('ipac_user_term_condition_log', $termsLogArray); #save users details values in DB
                            }
                            #send welcome message to user
                            // sendMessage(
                            //     COUNTRYCODE . $postDataArr['phone_number'],
                            //     $this->lang->line('welcome_to_campaign') . $signupArr['registeration_no'] . $this->lang->line('welcome_2'),SMS_WELCOME_MSG_TID
                            // );

                            // $mailData = [];
                            // $mailData['name'] = $postDataArr['full_name'];
                            // $mailData['email'] = $signupArr['email_id'];
                            // $mailData['registration_no'] = $signupArr['registeration_no'];
                            // $mailData['password'] = '';
                            // $mailData['mailerName'] = 'welcome';
                            // $subject = 'Welcome to ' . PROJECT_NAME;
                            // $mailerName = 'welcome';
                            // #sending welcome mail
                            // // $this->sendWelcomeMail( $mailData );
                            // sendmail($subject, $mailData, $mailerName);

                            $mailData                       = [];
                            $mailData['mailerName']         = 'gdb_signup_email';
                            $mailData['email']              = $signupArr['email_id'];
                            $subject                        = 'Welcome aboard! We are proud to have you with us.. ';
                            $mailerName                     = 'gdb_signup_email';
                            #sending welcome mail
                            sendmail($subject, $mailData, $mailerName);

                            /** SENDING IVR */
                            $phone_no = $postDataArr['phone_number'];
                            $IVRCode = '4005';
                            // sendIVR($phone_no, $IVRCode);

                            #setting response
                            $response_array = [
                                'CODE' => SUCCESS_CODE,
                                'MESSAGE' => $this->lang->line('registration_success'),
                                'RESULT' => $signupArr,
                            ];
                        } #if End
                    } else {
                        $this->db->trans_rollback();

                        #setting response
                        $response_array = [
                            'CODE' => TRY_AGAIN_CODE,
                            'MESSAGE' => $this->lang->line('try_again'),
                            'RESULT' => (object) array(),
                        ];
                    }
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            $this->response($response_array);
            #TRY END
        } catch (Exception $e) {
            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => (object) array(),
            ];

            #sending response
            $this->response($response_array);
        } #Catch End
    }

    /**
     * @function sendWelcomeMail
     * @description Sending welcome mail to user
     *
     * @param array $mailData user data needed to send mail, user email address and user name
     */
    private function sendWelcomeMail($mailData)
    {

        $this->load->helper('url');
        $data = [];
        $data['url'] = base_url() . 'request/welcomeMail?email=' . $mailData['email'] . '&name=' . urlencode($mailData['name']);
        sendGetRequest($data);
    }

    /**
     * @funciton validate_phone
     * @description Custom validation rules to validate phone number
     *
     * @param  $phone user phone number to validate phone number
     * @return boolean|json
     */
    private function validate_phone($phone)
    {

        if (isset($phone) && !preg_match("/^[0-9]{10}$/", $phone) && !empty($phone)) {
            #setting Response
            $response_array = [
                'CODE' => PARAM_REQ,
                'MESSAGE' => $this->lang->line('invalid_phone'),
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } else {
            return true;
        }
    }

    /*
     * Custom Rule Validate Dob
     * @param: user dob
     */

    /**
     * @function validate_dob
     * @description Custom Rule Validation For Date Of Birth
     *
     * @param date $dob user date of birth to check
     * @return boolean
     */
    private function validate_dob($dob)
    {
        if (!(isValidDate('m/d/Y', $dob))) {
            #setting response
            $response_array = [
                'CODE' => PARAM_REQ,
                'MESSAGE' => $this->lang->line('invalid_dob'),
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } else {
            return true;
        }
    }

    /**
     * @function insert_signup_data
     * @description add user signup data in login table
     *
     * @param string user ID
     * @return array user signup data
     */
    public function insert_signup_data($userId, $signupArr)
    {

        $login_insert['user_id'] = $userId;
        $login_insert['email'] = $signupArr["email"];
        $login_insert['password'] = $signupArr["password"];
        $login_insert['created_date'] = date('Y-m-d H:i:s');

        return $this->Common_model->insert_single('ipac_login', $login_insert);
    }

    /* Generating user registration number
 * IPAC XXX PTA YYYY (IPAC, PTA are static and XXX, YYYY are variables)
First 4 digits represents the company (IPAC) name
XXX represents the district code (number)
PTA represents as Part Time Associate
YYYY represents the volunteer serial number (in respective districts)
 */
    // private function generateRegistrationNo($districtId)
    // {

    //     #generate 3 digit district code of user district
    //     $districtnum = 3;
    //     $district_num_padded = sprintf("%03d", $districtId);

    //     # count the number of users in a particular district
    //     $countUser = $this->News_model->userList($districtId);
    //     $usercnt = 0;
    //     if (!empty($countUser)) {
    //         $usercnt = $countUser->UserCnt;
    //     }
    //     $usercnt = $usercnt + 1;
    //     # generate 4 digit user count for a particular district
    //     $districtusernum = 4;
    //     $district_user_num_padded = sprintf("%04d", $usercnt);

    //     $prefix = 'STLN';
    //     $sufix = 'PTA';
    //     //$registrationNo = $prefix . $district_num_padded . $sufix . $district_user_num_padded;
    //     $registrationNo = $prefix . $district_num_padded . $sufix . rand(00000, 99999);
    //     return $registrationNo;
    // }

    /*
     * @Function referalCodeGenerator
     * @description referal code generator
     * @param User name and name
     * @Created date 21-08-2018
     */
    private function referalCodeGenerator($name)
    {
        $Username = $this->getReferral(2);
        $uniqecode = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6));
        $referalCode = strtoupper($Username . $uniqecode);
        return $referalCode;
    }

    public function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        }
        // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function getReferral($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }
        return $token;
    }

    public function unlockUserlevels($user_id)
    {
        $user_wallet_amount = 0;
        $return_level = array();
        $sql = "SELECT points_earned FROM users WHERE user_id = ?";
        $query = $this->db->query($sql, array($user_id));
        $resultArray = $query->row_array();
        $user_wallet_amount = $resultArray['points_earned'];
        $current_points = $user_wallet_amount;

        $new_levels = $this->db->query("SELECT MAX(fk_level_id) as fk_level_id from tbl_user_level where fk_user_id = $user_id");
        $new_levels = $new_levels->row_array();
        $current_level = $new_levels['fk_level_id'] ? $new_levels['fk_level_id'] : '0';

        if ($current_level != 'null') {
            $level_array = $this->nextlevel($current_points, $current_level);
            $difference = $level_array['max_level'] - $current_level;
            if ($difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $difference; $i++) {
                    $fk_level_id = $current_level + $i;
                    $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                    $reward_points = $reward_points->row_array();
                    $free_rewards =  $reward_points['free_reward_points'];
                    if ($free_rewards != 0) {
                        $params                 = array();
                        $params['points']       = $free_rewards;
                        $params['user_id']      = $user_id;
                        $res                    = $this->User_model->updateUserEarnPoints($params);

                        $walletInsertArr[]      = array(
                            'user_id'           => $user_id,
                            'point'             => $free_rewards,
                            'title'             => 'New Level Rewards',
                            'description'       => 'Rewards earned for completion of levels',
                            'task_id'           => '0',
                            'type'              => 'bonus',
                            'status'            => '1',
                            'created_date'      => date('Y-m-d H:i:s'),
                            'updated_date'      => date('Y-m-d H:i:s')
                        );
                    }

                    $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
                }
            }
        }

        if (!empty($walletInsertArr) && count($walletInsertArr) > 0) {
            $this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
        }

        return $return_level;
    }

    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }


    /**
     * @SWG\Get(path="/api/getpartylist",
     *   tags={"part listing"},
     *   summary="Get party information",
     *   description="Get party list",
     *   operationId="partylisting_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function partylisting_get()
    {
        # Don't send any state or country code for getting the list of country
        try {

            #Getting Values from GET
            $getDataArr = $this->get();

            #setting Default values to array KEYS
            $default = array(
                "limit" => LIMIT,
                "searchlike" => "",
                "page" => 1
            );
            $defaultValue = defaultValue($getDataArr, $default);

            $listData = '';
            $searchlike = $defaultValue['searchlike'];
            $page = $defaultValue['page'];
            $limit = $defaultValue['limit'];
            $offset = ($page - 1) * $limit;
            $whereArr = [];

            if (!empty($searchlike)) {
                $whereArr['like'] = array('party_name' => $searchlike);
            }

            $table = 'ic_party_name';
            $order = 'party_id';
            $whereArr['order_by'] = [$order => 'asc'];
            $listData = $this->Common_model->fetch_data($table, array("*"), $whereArr);

            #If Result list is not empty
            if (!empty($listData)) {
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData
                ];
            } #if End
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            #sending response
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => EMAIl_SEND_FAILED,
                'MESSAGE' => $e->getMessage(),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }

    function getUserRegistrationNo($districtId,$userId){
        $usersDataCond              = array('where' => array('district' => $districtId,'user_id !=' => $userId));
        $usersDataCond['order_by']  = ['user_id' => 'desc'];
        $usersDataCond['limit'][0]  = 1;
        $usersData                  = $this->Common_model->fetch_data('users','registeration_no',  $usersDataCond);        
        $newRegId                   = "";
        if(!empty($usersData) && $usersData[0]['registeration_no'] != ''){
            $lastRegistrationId     = $usersData[0]['registeration_no'];
            $suffixCode             = str_pad((substr($lastRegistrationId,10,5) + 1), 5, '0', STR_PAD_LEFT); 
            $newRegId               = substr($lastRegistrationId,0,7).substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3). $suffixCode;
        }else{
            $districtDataCond       = array('where' => array('district_id' => $districtId));
            $districtInfo           = $this->Common_model->fetch_data('district','district_display_code,district_name,user_count',  $districtDataCond);
            $districtCodePadded     = str_pad($districtInfo[0]['district_display_code'], 4, '0', STR_PAD_LEFT); 
            $newRegId               = "GDB".$districtCodePadded.substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3).'00001';
        }
        return $newRegId;
    }
}
