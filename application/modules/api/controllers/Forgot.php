<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Forgot extends REST_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('Common_model');
        $this->load->library('Common_function');
        $this->load->helper('sendsms');
    }



    /**
     * @SWG\Post(path="/Forgot",
     *   tags={"User"},
     *   summary="Forgot Password",
     *   description="Forgot Password",
     *   operationId="index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Forgot Email Send Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=211, description="Email Send failed"),
     *   @SWG\Response(response=302, description="Email in not registered"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function index_post()
    {
        try {
            $post_data = $this->post();
            $response_array = [];
            #Setting form validation
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required'
                )
            );
            $this->form_validation->set_rules($config);

            #Checking Form Fields Validation
            if ($this->form_validation->run()) {
                $where = [];
                $where['where'] = array('phone_number' => $post_data['email']);
                $userInfo = $this->Common_model->fetch_data('users', array('full_name as name', 'user_id', 'phone_number', 'email_id', 'is_active', 'is_deleted'), $where, true);

                if (!empty($userInfo) && ACTIVE == $userInfo['is_active']) {
                    #Encrypt the user-email
                    $token = $userInfo['name'] . uniqid();
                    $ciphertext = encryptDecrypt($token);
                    $mailInfoArr = array();
                    $mailInfoArr['subject'] = $this->lang->line('reset_password');
                    $mailInfoArr['mailerName'] = 'reset.php';
                    $mailInfoArr['email'] = $userInfo['email_id'];
                    $mailInfoArr['name'] = $userInfo['name'];
                    $mailInfoArr['link'] = base_url() . 'reset?token=' . $ciphertext;
                    //$mailInfoArr['link']       = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/api/reset?token=' . $ciphertext;
                   
                    #Send Email to user with above mentioned detail
                    $isSuccess = $this->common_function->sendEmailToUser($mailInfoArr);
                    $link = $mailInfoArr['link'] . '&userId=' . $userInfo['user_id'] . '&phoneNo=' . $userInfo['phone_number'];
                    $new_url = $this->get_tiny_url($link);

                    #send link on sms
                    sendMessage(COUNTRYCODE . $userInfo['phone_number'], $this->lang->line('your_reset_password_link') . $new_url);
                    
                    //$isSuccess = true;
                    if (!$isSuccess) {
                        throw new Exception('something went wrong');
                    }

                    #transaction Begin
                    $this->db->trans_begin();
                    $updatearr = array('isreset_link_sent' => 1, 'reset_link_time' => datetime(), 'reset_token' => $ciphertext);
                    $where = array('where' => array('email_id' => $userInfo['email_id']));

                    $isSuccess = $this->Common_model->update_single('users', $updatearr, $where);

                    if (!$isSuccess) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    #Checking transaction Status
                    if (true === $this->db->trans_status()) {
                        #Commiting transation
                        $this->db->trans_commit();

                        #setting response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('email_link_sent'),
                            'RESULT' => []
                        ];
                    }
                } elseif (!empty($userInfo) && $userInfo['is_deleted '] == 1) {
                    #setting response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => []
                    ];
                } else {
                    #setting response
                    $response_array = [
                        'CODE' => INVALID_PHONE_NUMBER,
                        'MESSAGE' => $this->lang->line('not_registered'),
                        'RESULT' => []
                    ];
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);

              #setting response
                $response_array = [
                    'CODE' => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT' => []
                ];
            }
            $this->response($response_array);
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

              #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }



    /**
     * @SWG\Post(path="/Forgot/phone",
     *   tags={"User"},
     *   summary="Forgot Password api with phone number",
     *   description="Forgot Password",
     *   operationId="forget_pass_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="Phone number",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="country_code",
     *     in="formData",
     *     description="Country Code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=102, description="Invalid credentials."),
     *   @SWG\Response(response=106, description="User account blocked."),
     *   @SWG\Response(response=200, description="OTP successfully sent to reset password."),
     *   @SWG\Response(response=201, description="Try again."),
     *   @SWG\Response(response=202, description="No Data Found"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function forget_pass_post()
    {
        try {
            $post_data = $this->post();
            $response_array = [];
            #Setting form validation
            $config = array(
                array(
                    'field' => 'phone',
                    'label' => 'Phone',
                    'rules' => 'trim|required|is_numeric'
                ),
                array(
                    'field' => 'country_code',
                    'label' => 'Country Code',
                    'rules' => 'trim|required'
                )
            );
            $this->form_validation->set_rules($config);

            #Checking Fomr Fields Validation
            if ($this->form_validation->run()) {
                $where = [];
                $where['where'] = array('phone' => $post_data['phone']);
                $userInfo = $this->Common_model->fetch_data('users', array('CONCAT(first_name," ",middle_name) as name', 'email', 'phone', 'status', 'user_id'), $where, true);

                if (!empty($userInfo) && 1 == $userInfo['status']) {
                    #Encrypt the user-email
                    $token = $userInfo['name'] . uniqid();
                    $ciphertext = encryptDecrypt($token);

                    $is_otp_success = $this->send_otp($userInfo, $post_data['country_code']);

                    if (isset($is_otp_success['success']) && !$is_otp_success['success']) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    #transaction Begin
                    $this->db->trans_begin();

                    $isSuccess = $this->save_otp_in_db($userInfo, $is_otp_success);

                    if (!$isSuccess) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    #Checking transaction Status
                    if (true === $this->db->trans_status()) {
                        #Commiting transation
                        $this->db->trans_commit();

                        #setting response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('otp_sent'),
                            'RESULT' => ['otp' => $is_otp_success['otp']]
                        ];
                    }
                } elseif (!empty($userInfo) && 2 == $userInfo['status']) {
                    #setting response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => []
                    ];
                } else {
                    #setting response
                    $response_array = [
                        'CODE' => NO_DATA_FOUND,
                        'MESSAGE' => $this->lang->line('mobile_does_not_exist'),
                        'RESULT' => []
                    ];
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);

                #setting response
                $response_array = [
                    'CODE' => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT' => []
                ];
            }
            $this->response($response_array);
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }



    /**
     * @function send_otp
     * @description send otp to user
     *
     * @param string userinfo
     * @return array with success status
     */
    private function send_otp($userInfo, $country_code)
    {

        $params = [];
        $otp = $this->generate_otp();
        $params['message'] = $this->generate_otp_message($otp);
        $params['phone'] = $country_code . $userInfo['phone'];
        $params['subject'] = OTP_SUBJECT;

        #Send OTP to user with above mentioned detail using SNS
        $this->load->library('SNSPush');
        require_once APPPATH . "libraries/SNSPush.php";
        $lib = new \SNSPush();


        $response['success'] = 1;
        #$response            = $lib->send_msg ( $params );//SNS Function to send SMS
        $response['otp'] = $otp;
        $response['message'] = $params['message'];
        return $response;
    }



    /**
     * @function generate_otp
     * @description Function used to generate random number for OTP
     *
     * @return int Generated OTP
     */
    private function generate_otp()
    {
        if (9 < OTP_LENGTH) {//IF OTP Length is more than 10, it will return 10 digit code
            return mt_rand(1000000000, 9999999999);
        }

        $temp = [];

        //Numbers array having digits from 1 to 9
        $numbers = range(1, 9);

        //Value will shuffled
        shuffle($numbers);

        //Concating OTP digits
        for ($i = 0; $i < OTP_LENGTH; $i++) {
            $temp[] .= $numbers[$i];
        }

        //generated OTP is shuffled
        shuffle($temp);

        //Final OTP
        $otp = implode('', $temp);
        return ($otp);
    }



    /**
     * @function generate_message_otp
     * @description Function to generate OTP message
     *
     * @return String Message Sent to user
     */
    private function generate_otp_message($otp)
    {
        $msg_template = $this->lang->line('otp_message');
        $message = str_replace("#OTP", $otp, $msg_template);
        $message = str_replace("#MIN", OTP_EXPIRATION, $message);
        return ($message);
    }



    /**
     * @function save_otp_in_db
     * @description Save OTP in DB
     *
     * @param type $data
     * @param type $otp_data
     * @return type
     */
    private function save_otp_in_db($data, $otp_data)
    {
        //Expir Unused OTP
        $where = [
            'where' => [
                'phone' => $data['phone'],
                'user_id' => $data['user_id'],
                'status' => 1
            ]
        ];
        $updatearr = ['status' => 0];
        $this->Common_model->update_single('user_otp', $updatearr, $where);
        //End

        $insert_data = [
            'user_id' => $data['user_id'],
            'phone' => $data['phone'],
            'otp' => $otp_data['otp'],
            'message' => $otp_data['message'],
            'send_time' => date('Y-m-d H:i'),
            'expiry_time' => $this->get_expiration_time(),
            'status' => 1
        ];
        return $this->Common_model->insert_single('user_otp', $insert_data);
    }



    /**
     * @function get_expiration_time
     * @description Function to generate OTP time of expiration
     * @return String
     */
    private function get_expiration_time()
    {
        $tenMinsFromNow = (new \DateTime())->add(new \DateInterval('PT' . OTP_EXPIRATION . 'M'));
        return $tenMinsFromNow->format('Y-m-d H:i');
    }



    /**
     * @SWG\Post(path="/Forgot/otp_verification",
     *   tags={"User"},
     *   summary="To verify forgot password user OTP",
     *   description="OTP Verification",
     *   operationId="verify_otp_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="otp",
     *     in="formData",
     *     description="User OTP",
     *     required=true,
     *     type="string"
     *   )
     *   @SWG\Response(response=102, description="Invalid credentials."),
     *   @SWG\Response(response=106, description="User account blocked."),
     *   @SWG\Response(response=200, description="OTP successfully sent to reset password."),
     *   @SWG\Response(response=201, description="Try again."),
     *   @SWG\Response(response=202, description="No Data Found"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function verify_otp_post()
    {
        try {
            $post_data = $this->post();
            $response_array = [];

            #Setting form validation
            $config = array(
                array(
                    'field' => 'otp',
                    'label' => 'OTP',
                    'rules' => 'trim|required|is_numeric|min_length[' . OTP_LENGTH . ']|max_length[' . OTP_LENGTH . ']'
                )
            );
            $this->form_validation->set_rules($config);

            #Checking Fomr Fields Validation
            if ($this->form_validation->run()) {
                $verify_time = date('Y-m-d H:i');
                $where = [];
                $where['where'] = [
                    'otp' => $post_data['otp'],
                    'status' => 1,
                    'send_time<=' => $verify_time,
                    'expiry_time>=' => $verify_time
                ];
                $userInfo = $this->Common_model->fetch_data('user_otp', array('count(user_id) total, user_id, phone, id'), $where, true);

                if (1 === intval($userInfo['total'])) {
                    #transaction Begin
                    $this->db->trans_begin();

                    $isSuccess = $this->verify_otp($userInfo);

                    if (!$isSuccess) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    #Checking transaction Status
                    if (true === $this->db->trans_status()) {
                        #Commiting transation
                        $this->db->trans_commit();

                        #setting response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('otp_verified'),
                            'RESULT' => []
                        ];
                    }
                } else {
                    #setting response
                    $response_array = [
                        'CODE' => INVALID_CREDENTIALS,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => []
                    ];
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);

                #setting response
                $response_array = [
                    'CODE' => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT' => []
                ];
            }
            $this->response($response_array);
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT ' => []
            ];
            $this->response($response_array);
        }
    }



    /**
     * @function verify_otp
     * @description function to update status of OTP from active to Used
     *              1=> Active
     *              2=> Used
     *
     * @param type $data
     * @return type
     */
    private function verify_otp($data)
    {
        //Expir Unused OTP
        $where = [
            'where' => [
                'id' => $data['id'],
                'status' => 1
            ]
        ];
        $updatearr = ['status' => 2, 'verify_time' => date('Y-m-d H:i:s')];
        return $this->Common_model->update_single('user_otp', $updatearr, $where);
        //End
    }



    private function get_tiny_url($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
