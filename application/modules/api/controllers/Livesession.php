<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Livesession extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function index_post()
    {
        try {
            $currentDate = date("Y-m-d");
            $currentDateTime = date("Y-m-d H:i:s");
            $where = array('where' => ['session_date' => $currentDate, 'is_session_start' => 1], 'order_by' => ['id' => 'desc']);
            $getSession = $this->Common_model->fetch_data('live_session', '*', $where, true);

            if (!empty($getSession)) {
                $user_id = $GLOBALS['api_user_id'];
                $session_id = $getSession['id'];
                $data = [
                    'user_id' => $user_id,
                    'session_id' => $session_id,
                    'is_live' => 1,
                    'live_timestamp' => $currentDateTime
                ];
                $whereUser = array('where' => array('user_id' => $user_id, 'session_id' => $session_id));
                $getLiveCount = $this->Common_model->fetch_count('live_session_users', $whereUser);
                if ($getLiveCount == 0) {
                    $this->Common_model->insert_single('live_session_users', $data);
                } else {
                    $this->Common_model->update_single('live_session_users', $data, $whereUser);
                }

                $total_live_users = $getSession['total_live_users'] + 1;
                $gsUpdateData = array('total_live_users' => $total_live_users);
                $gsUpdate = array('where' => array('id' => $getSession['id']));
                $this->Common_model->update_single('live_session', $gsUpdateData, $gsUpdate);

                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $getSession
                ));
            } else {
                $response_array = [
                    'CODE' => NO_DATA_FOUND,
                    'MESSAGE' => 'No Live Session found yet',
                    'RESULT' => (object) array(),
                ];
                $this->response($response_array);
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

    public function leavesession_post()
    {
        try {
            $params = $this->post();
            $currentDateTime = date("Y-m-d H:i:s");
            $required_fields_arr = array(
                array(
                    'field' => 'session_id',
                    'label' => 'Live Session Id',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            if ($this->form_validation->run()) {
                $user_id = $GLOBALS['api_user_id'];
                $session_id = $params['session_id'];
                $data = [
                    'is_live' => 0,
                    'leave_timestamp' => $currentDateTime
                ];
                $whereUser = array('where' => array('user_id' => $user_id, 'session_id' => $session_id));
                $this->Common_model->update_single('live_session_users', $data, $whereUser);

                $where = array('where' => ['id' => $session_id]);
                $getSession = $this->Common_model->fetch_data('live_session', '*', $where, true);
                $total_live_users = $getSession['total_live_users'] > 1 ? $getSession['total_live_users'] - 1 : 1;
                $gsUpdateData = array('total_live_users' => $total_live_users);
                $this->Common_model->update_single('live_session', $gsUpdateData, $where);
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success'
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

    public function addlike_post()
    {
        try {
            $params = $this->post();

            $currentDateTime = date("Y-m-d H:i:s");
            $required_fields_arr = array(
                array(
                    'field' => 'session_id',
                    'label' => 'Live Session Id',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            if ($this->form_validation->run()) {
                $user_id = $GLOBALS['api_user_id'];
                $session_id = $params['session_id'];
                $data = [
                    'user_id' => $user_id,
                    'session_id' => $session_id,
                    'total_likes' => 1,
                    'like_timestamp' => $currentDateTime
                ];
                $whereUser = array('where' => array('user_id' => $user_id, 'session_id' => $session_id));
                $getLiveCount = $this->Common_model->fetch_data('live_session_likes', '*', $whereUser, true);
                if ($getLiveCount == 0) {
                    $this->Common_model->insert_single('live_session_likes', $data);
                } else {
                    $data['total_likes'] = $getLiveCount['total_likes'] + 1;
                    $this->Common_model->update_single('live_session_likes', $data, $whereUser);
                }

                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success'
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

    public function livesession_post()
    {
        try {
            
            $table = 'live_session';
            $whereArr['where'] = ['is_session_start' => '1'];
            $listData = $this->Common_model->fetch_data($table, array("*"), $whereArr);

            #If Result list is not empty
            if (!empty($listData)) {
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('active_live_session'),
                    'RESULT' => $listData
                ];
            } else {
            $params = $this->post();
            $currentDate = date("Y-m-d ");
            $currentTime = date("H:i:s");
            $sessionEndtime = date('H:i:s',strtotime('+2 hour',strtotime($currentTime)));
            $required_fields_arr = array(
                array(
                    'field' => 'session_video_id',
                    'label' => 'Live Session Youtube Id',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            if ($this->form_validation->run()) {
               $session_video_id = $params['session_video_id'];
               $insertlivesessionArray = array(
                'session_name'            => 'MKS Live',
                'session_video_id'        => $session_video_id,
                'session_date'            => $currentDate,
                'session_start_time'      => $currentTime,
                'session_end_time'        => $sessionEndtime,
                'is_session_start'        => 1,
                'total_live_users'        => 0,
            );
            $this->Common_model->insert_single('live_session', $insertlivesessionArray);
               
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success'
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
        }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

}
