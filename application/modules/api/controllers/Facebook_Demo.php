<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require_once APPPATH . 'composer/vendor/facebook/graph-sdk/src/Facebook/autoload.php';


class Facebook_Demo extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('email');
    }

    public function index_post()
    {
    }
    
    public function index_get()
    {
        $fb = new Facebook\Facebook([
           'app_id' => '118100505417819',
           'app_secret' => 'cda9273cc7b35b3a4e6f11ce53c259ff',
           'default_graph_version' => 'v2.12',
        ]);
        $helper = $fb->getCanvasHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
          
        if (! isset($accessToken)) {
            echo 'No OAuth data could be obtained from the signed request. User has not authorized your app yet.';
            exit;
        }
          
          // Logged in
            echo '<h3>Page ID</h3>';
            var_dump($helper->getPageId());
          
            echo '<h3>User is admin of page</h3>';
            var_dump($helper->isAdmin());
          
            echo '<h3>Signed Request</h3>';
            var_dump($helper->getSignedRequest());
          
            echo '<h3>Access Token</h3>';
            var_dump($accessToken->getValue());

            //print_r($helper); die;
    }
}
