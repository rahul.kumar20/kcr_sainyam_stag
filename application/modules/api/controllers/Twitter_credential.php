<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Twitter_credential extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    /**
     * @SWG\Get(path="/twitter",
     *   tags={"Task"},
     *   summary="Get admin twitter credential",
     *   description="Get twitter credential",
     *   operationId="index_get_twitter",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="count",
     *     in="query",
     *     description="hit with count to get next page",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_get()
    {

        try {
            $params = $this->get();
                          
                        $twitterCredential = array(
						'twitter_secret_key' =>TWITTER_SECRET_KEY,
                        'twitter_consumer_key' =>TWITTER_CONSUMER_KEY					
						);
                        $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => 'success', 'RESULT' => $twitterCredential));

                    #sending Response
                    $this->response($response_array);
                
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
               'CODE' => TRY_AGAIN_CODE,
               'MESSAGE' => $error,
               'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END
    }
}
    