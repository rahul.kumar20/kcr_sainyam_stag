<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Wallet_history extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    /**
     * @SWG\get(path="/wallet_logs",
     *   tags={"Wallet"},
     *   summary="get wallet history",
     *   description="get wallet history",
     *   operationId="index_get_wallet_history",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Authorization key",
     *     required=true,
     *     type="string"
     *   ),
     *@SWG\Parameter(
     *     name="Accesstoken",
     *     in="header",
     *     description="Access token got in login",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="count",
     *     in="query",
     *     description="hit with count to get next page",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="status",
     *     in="query",
     *     description="0 for all 1 for earned,3 for redeem",
     *     type="integer",
     *     required=true,
     *   ),
     * @SWG\Parameter(
     *     name="start_date",
     *     in="query",
     *     description="for filter start date Y-m-d",
     *     type="string",
     *     required=false,
     *   ),
     * @SWG\Parameter(
     *     name="end_date",
     *     in="query",
     *     description="end date for filter Y-m-d",
     *     type="string",
     *     required=false,
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
     /*
     */
    public function index_get()
    {
        try {
            $getDataArr = $this->input->get();

            $default = array(
                "count" => 1,
                "start_date" => "",
                "end_date" => "",
                "limit" => QUERY_OFFSET,
                'status' => 0
            );
            #Setting Default Value
            $defaultValue = defaultValue($getDataArr, $default);
            $userId = $GLOBALS['api_user_id'];
            $userInfo = $GLOBALS['login_user'];
            $page = $defaultValue['count'];
            $offset = ($page - 1) * $defaultValue['limit'];
            $params['limit'] = $defaultValue['limit'];
            $params['offset'] = $offset;
            $params['start_date'] = $defaultValue['start_date'];
            $params['end_date'] = $defaultValue['end_date'];
            $params['status'] = $defaultValue['status'];

            $params['userId'] = $userId;

            #Get wallet history log
            $wallet_history = $this->get_wallet_history($params);
            #total history log count
            $total_count = $wallet_history['count'];
            #check is data remaining or not
            if (($total_count > ($page * $params['limit']))) {
                $page++;
            } else {
                $page = -1;
            }
            $userArray = array(
                'total_point' => $userInfo['userinfo']['total_earning'],
                'paytm_number' => $userInfo['userinfo']['paytm_number'],
                'upi_address' => $userInfo['userinfo']['upi_address'],
                'bank_account' => $userInfo['userinfo']['account_number'],
                'default_option' => $userInfo['userinfo']['default_payment_option'],
                'updated_date' => !is_null($userInfo['userinfo']['last_earning_update'])?$userInfo['userinfo']['last_earning_update']:'',
                'redeemable_status' => REDEMMABLE,
                'updated_expense_date' => ($userInfo['userinfo']['last_expense_date']!='0000-00-00 00:00:00')?$userInfo['userinfo']['last_expense_date']:'',
            );
            $walletHistorDetail = $this->Wallet_history_model->getWalletHistoryDetail($userId);
            $walletDetail = $walletHistorDetail['data'];
            if (!empty($wallet_history['data'])) {#if Start
                #setting response
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $wallet_history['data'],
                    'USERINFO' => $userArray,
                    'WALLET_DETAIL' => $walletDetail,
                    'COUNT' => $page
                ];
            } #IF END
            else {#ELSE START
                #setting response
                $response_array = [
                    'CODE' => NO_DATA_FOUND,
                    'MESSAGE' => 'NO_DATA_FOUND',
                    'RESULT' => [],
                    'COUNT' => '-1',
                    'USERINFO' => $userArray,
                    'WALLET_DETAIL' => $walletDetail,
                ];
            }#Else End
            #sending Response
            $this->response($response_array);
            #try END
        } catch (Exception $e) {#Catch Start
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
                'PAGE' => ''
            ];
            $this->response($response_array);
        }#Catch End
    }

    /**
     * @function get_wallet_history
     * @description Fetch all wallet history
     *
     * @param array $params
     * @return type
     */
    private function get_wallet_history($params)
    {
        #load wallet history model
        $this->load->model('Wallet_history_model');

        return $this->Wallet_history_model->getWalletHistory($params);
    }

   /**
     * @SWG\Post(path="/update_payment_method",
     *   tags={"Wallet"},
     *   summary="Change payment method",
     *   description="Change payment method",
     *   operationId="payment_change_index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="payment_method",
     *     in="formData",
     *     description="Payment Method",
     *     required=True,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=101, description="Account Blocked"),
     *   @SWG\Response(response=200, description="Login Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function updatePaymentOption_post()
    {
        try {
            $postDataArr = $this->post();
            $response_array = array();
            //logged user id
            $userId = $GLOBALS['api_user_id'];

          #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'payment_method',
                    'label' => 'Payment method',
                    'rules' => 'trim|required'
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');
            if ($this->form_validation->run()) {
                        //logs array
                $updArray = array(
                    'default_payment_option' => $postDataArr['payment_method'],
                    'updated_at' => datetime(),
                );
                #start transaction
                $this->db->trans_begin();

                $isSuccess = $this->Common_model->update_single(
                    'users',
                    $updArray,
                    array('where' => array('user_id' => $userId))
                );
             

                  #Throw Exception
                if (!$isSuccess) {
                    $this->db->trans_rollback();
                    throw new Exception($this->lang->line('try_again'));
                }

                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();
                    #setting Response Array
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line('payment_method_success'),
                        'RESULT' => []
                    ];
                } else {
                    #IF transaction failed
                    #rolling back
                    $this->db->trans_rollback();

                    #setting Response Array
                    $response_array = [
                        'CODE' => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('try_again'),
                        'RESULT' => []
                    ];
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                 #setting Response Array
                $response_array = [
                    'CODE' => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT' => []
                ];
            }
            $this->response($response_array);
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END
    }

     /**
     * @SWG\get(path="/offline_task",
     *   tags={"Wallet"},
     *   summary="get offline task list",
     *   description="get offline task list",
     *   operationId="index_get_offline_task",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="Authorization key",
     *     required=true,
     *     type="string"
     *   ),
     *@SWG\Parameter(
     *     name="Accesstoken",
     *     in="header",
     *     description="Access token got in login",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="search_key",
     *     in="query",
     *     description="Search key",
     *     type="string",
     *     required=false,
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
     /*
     */
    public function offline_task_get()
    {
        try {
            $getDataArr = $this->input->get();

            $default = array(
                'task_type' => 'offline',
                'search_key' =>'',
            );
            #Setting Default Value
            $defaultValue = defaultValue($getDataArr, $default);
            $userId = $GLOBALS['api_user_id'];
            $userInfo = $GLOBALS['login_user'];

            $params['userId'] = $userId;
            $params['taskType'] = $defaultValue['task_type'];
            $params['searchLike'] = trim($defaultValue['search_key']);
  
            #load wallet history model
              $this->load->model('Wallet_history_model');
            #Get offline task list
            $task_list = $this->Wallet_history_model->getTaskList($params);
            #total history log count
            $total_count = $task_list['count'];
         
            if (!empty($task_list['data'])) {#if Start
                #setting response
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $task_list['data'],
                ];
            } #IF END
            else {#ELSE START
                #setting response
                $response_array = [
                    'CODE' => NO_DATA_FOUND,
                    'MESSAGE' => 'NO_DATA_FOUND',
                    'RESULT' => [],
                ];
            }#Else End
            #sending Response
            $this->response($response_array);
            #try END
        } catch (Exception $e) {#Catch Start
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
                'PAGE' => ''
            ];
            $this->response($response_array);
        }#Catch End
    }
}
