<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . 'libraries/SNSPush.php';

class Profile extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('admin/Notification_model');

        $this->load->model('User_model');
    }

    /**
     * @SWG\Get(path="/Profile",
     *   tags={"Profile"},
     *   summary="Get user profile information",
     *   description="Get name,image,email etc",
     *   operationId="index_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="Accesstoken",
     *     in="header",
     *     description="Access token got in login",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_get()
    {
        try {
            # load session model
            $this->load->model("Session");

            $params = $this->get();
            $response_array = array();
            $userId = $GLOBALS['api_user_id'];
            $userInfo = $this->User_model->userProfileDetail($userId);
            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
                    #fetching proof details from db
                    $where = array('where' => array('user_id' => $userId, 'status' => 0));
                    $where['order_by'] = ['inserted_on' => 'desc'];
                    $proofInfo = $this->Common_model->fetch_data(
                        'user_id_proof_details',
                        ['proof_id', 'id_number', 'proof_type', 'image'],
                        $where
                    );

                    $userInfo['id_proof'] = $proofInfo;
                    $userInfo['full_name']  = ucfirst($userInfo['full_name']) ;

                    #setting Response
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line('profile_fetched'),
                        'RESULT' => (object) $userInfo,
                    ];
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        // 'RESULT' => $empty_response
                    ];
                }
            }

            #sending Response
            $this->response($response_array);
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    /**
     * @SWG\Get(path="/Profile",
     *   tags={"Profile"},
     *   summary="Save user profile information",
     *   description="Save name,image,email etc",
     *   operationId="index_post",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state",
     *     in="formData",
     *     description="User State",
     *     type="string"
     *   ),
     * *   @SWG\Parameter(
     *     name="district",
     *     in="formData",
     *     description="District",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     type="string"
     *   ),
     * *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="Name",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="profileImage",
     *     in="formData",
     *     description="User Profile Image",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="twitterId",
     *     in="formData",
     *     description="Twitter Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="facebookId",
     *     in="formData",
     *     description="Facebook Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="whatsupNo",
     *     in="formData",
     *     description="Whatsup Number",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="PaytmNo",
     *     in="formData",
     *     description="Paytm Number",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_post()
    {
        try {
            # load session model
            $this->load->model("Session");

            $postDataArr = $this->post();
            $response_array = array();
            $userId = $GLOBALS['api_user_id'];
            $whereArr = [];

            /*if (isset($postDataArr['email_id']) && !empty($postDataArr['email_id'])) {
                $whereArr['where'] = ['email_id' => $postDataArr['email_id']];
            }

            if (isset($whereArr['where']) && !empty($postDataArr['whatsup_number'])) {
                $whereArr['or_where'] = ['whatsup_number' => $postDataArr['whatsup_number'], 'phone_number' => $postDataArr['whatsup_number']];
            }*/

            if (!empty($postDataArr['whatsup_number'])) {
                $whereArr['or_where'] = ['whatsup_number' => $postDataArr['whatsup_number'], 'phone_number' => $postDataArr['whatsup_number']];
            }

            #fetching user details from db
            $user_info = $this->Common_model->fetch_data('users', ['user_id', 'email_id','district'], $whereArr, true);
            /*if ((!empty($user_info) && isset($postDataArr['email_id']) && $user_info['email_id'] == $postDataArr['email_id']) && $user_info['user_id'] != $userId) {
                #user is already Registered
                $response_array = [
                    'CODE' => EMAIL_ALREADY_EXIST,
                    'MESSAGE' => $this->lang->line('account_exist'),
                    'RESULT' => [],
                ];
            } else*/ if ((!empty($user_info) && isset($postDataArr['whatsup_number'])) && $user_info['user_id'] != $userId) {
                #user is already Registered
                $response_array = [
                    'CODE' => PHONE_ALREADY_EXISTS,
                    'MESSAGE' => $this->lang->line('whats_app_already_exist'),
                    'RESULT' => (object) array(),
                ];
            } else if (!empty($user_info) && !empty($postDataArr['district']) && ($user_info['district'] != $postDataArr['district']) ) {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('district_cant_change'),
                    'RESULT' => (object) array(),
                ];
            } else {
                $whereArr['where'] = ['user_id' => $userId];
                $user_info = $this->Common_model->fetch_data('users', ['*'], $whereArr, true);

                //updated request array
                $newdata = array(
                    'full_name' => (isset($postDataArr['full_name']) && !empty($postDataArr['full_name'])) ? $postDataArr['full_name'] : $user_info['full_name'],
                    'email_id' => (isset($postDataArr['email_id']) && !empty($postDataArr['email_id'])) ? $postDataArr['email_id'] : $user_info['email_id'],
                    'whatsup_number' => (isset($postDataArr['whatsup_number']) && !empty($postDataArr['whatsup_number'])) ? $postDataArr['whatsup_number'] : $user_info['whatsup_number'],
                    'gender' => (isset($postDataArr['gender']) && !empty($postDataArr['gender'])) ? $postDataArr['gender'] : $user_info['gender'],
                    'district' => (isset($postDataArr['district']) && !empty($postDataArr['district'])) ? $postDataArr['district'] : $user_info['district'],
                    'ac' => (isset($postDataArr['ac']) && !empty($postDataArr['ac'])) ? $postDataArr['ac'] : $user_info['ac'],
                    'user_image' => (isset($postDataArr['user_image']) && !empty($postDataArr['user_image'])) ? $postDataArr['user_image'] : $user_info['user_image'],
                    'inclined_party' => isset($postDataArr['inclined_party']) ? $postDataArr['inclined_party'] : $user_info['inclined_party'],
                    'party_name' => isset($postDataArr['party_name']) ? $postDataArr['party_name'] : $user_info['party_name'],
                    'dmk_id' => isset($postDataArr['dmk_id']) ? $postDataArr['dmk_id'] : $user_info['dmk_id'],
                    'dmk_member' => isset($postDataArr['dmk_member']) ? $postDataArr['dmk_member'] : $user_info['dmk_member'],
                    'block' => isset($postDataArr['block']) ? $postDataArr['block'] : $user_info['block'],
                    'ward_panchayat' => isset($postDataArr['ward_panchayat']) ? $postDataArr['ward_panchayat'] : $user_info['ward_panchayat'],
                    'ward_panchayat_other' => isset($postDataArr['ward_panchayat_other']) ? $postDataArr['ward_panchayat_other'] : $user_info['ward_panchayat_other'],
                    'block_other' => isset($postDataArr['block_other']) ? $postDataArr['block_other'] : $user_info['block_other'],
                    'dob'=> isset($postDataArr['dob']) ? date('Y-m-d', strtotime($postDataArr['dob'])) : $user_info['dob'] 
                );

                $isSuccess = $this->Common_model->update_single('users', $newdata, array('where' => array('user_id' => $userId)));

                #start transaction
                $this->db->trans_begin();

                if (isset($postDataArr['isProofUploaded']) && $postDataArr['isProofUploaded'] == '1') {
                    $where = array('where' => array('user_id' => $userId));

                    #save proof details
                    $proofData = array();
                    if (isset($postDataArr['proof_image'])) {
                        $proofImage = explode(",", $postDataArr['proof_image']);

                        if (!empty($proofImage)) {
                            //update previous id proof
                            $profUpd = array(
                                'status' => ID_PROFF_REQUESTED,
                            );
                            $isSuccess = $this->Common_model->update_single('user_id_proof_details', $profUpd, array('where' => array('user_id' => $userId, 'status' => 0)));
                            foreach ($proofImage as $key => $val) {
                                $proofData['user_id'] = $userId;
                                $proofData['id_number'] = $postDataArr['id_number'];
                                $proofData['proof_type'] = $postDataArr['proof_type'];
                                $proofData['image'] = $val;
                                $proofData['inserted_on'] = date('Y-m-d H:i:s');
                                $proofData['status'] = 0;
                                $proofId = $this->Common_model->insert_single('user_id_proof_details', $proofData);
                            }
                        }
                    }
                }
                #If query failed to update
                #Throw Exception
                if (!$isSuccess) {
                    $this->db->trans_rollback();
                    throw new Exception($this->lang->line('try_again'));
                }

                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();
                    $userInfo = $this->User_model->userProfileDetail($userId);

                    #fetching proof details from db
                    $where = array('where' => array('user_id' => $userId, 'status' => 0));
                    $where['order_by'] = ['inserted_on' => 'desc'];
                    $proofInfo = $this->Common_model->fetch_data('user_id_proof_details', ['proof_id', 'id_number', 'proof_type', 'image'], $where);

                    $userInfo['id_proof'] = $proofInfo;
                    //for bonus block start
                    //if bonus is not receive by user on profile completition
                    // if (isset($userInfo['is_bonus_received']) && ($userInfo['is_bonus_received']) == 0) {
                    //     //update user 100 bonus
                    //     $this->userBonusUpate($userId);
                    // }
                    //bonus block end
                    #setting Response Array
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => 'Your profile has been updated successfully.',
                        'RESULT' => (object) $userInfo,
                    ];
                } else {
                    #IF transaction failed
                    #rolling back
                    $this->db->trans_rollback();
                    #setting Response Array
                    $response_array = [
                        'CODE' => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->db->last_query(),
                        'RESULT' => (object) array(),
                    ];
                }
            }

            $this->response($response_array);
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    /**
     * @SWG\Get(path="/deleteproof",
     *   tags={"deleteproof"},
     *   summary="Delete User Id proof Data",
     *   description="Delete User Id proof Data",
     *   operationId="index_post",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function Delete_Proof_Data_post()
    {

        $userId = $GLOBALS['api_user_id'];
        $where = array('where' => array('user_id' => $userId));
        $newdata = array('status' => 1);

        #start transaction
        $this->db->trans_begin();

        #updating profile details
        $isSuccess = $this->Common_model->update_single(
            'user_id_proof_details',
            $newdata,
            array('where' => array('user_id' => $userId))
        );

        #If query failed to update
        #Throw Exception
        if (!$isSuccess) {
            $this->db->trans_rollback();
            throw new Exception($this->lang->line('try_again'));
        }

        #if transaction runs successfully
        if (true === $this->db->trans_status()) {
            #Comminting changes
            $this->db->trans_commit();
            $newdata['user_id'] = $userId;
            #setting Response Array
            $response_array = [
                'CODE' => SUCCESS_CODE,
                'MESSAGE' => $this->lang->line('proof_deleted'),
                'RESULT' => [],
            ];
        } else {
            #IF transaction failed
            #rolling back
            $this->db->trans_rollback();

            #setting Response Array
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('try_again'),
                'RESULT' => [],
            ];
        }

        $this->response($response_array);
    }

    /**
     * @SWG\Put(path="/update_profile",
     *   tags={"UpdateProfile"},
     *   summary="Update user profile",
     *   description="Update profile of a user",
     *   operationId="index_put",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="accesstoken",
     *     in="header",
     *     description="Access token received during signup or login",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function Update_Profile_put()
    {
        try {
            # load session model
            $this->load->model("Session");

            $postDataArr = $this->put();

            $response_array = array();

            $userId = $GLOBALS['api_user_id'];

            $whereArr = [];

            # If users enters email for update
            if (isset($postDataArr['email']) && !empty($postDataArr['email'])) {
                $whereArr['where'] = ['email_id' => $postDataArr['email']];
            }

            # If users enters only facebook id
            if (!empty($postDataArr['facebookId']) && !isset($whereArr['where'])) {
                $whereArr['where'] = ['facebook_id' => $postDataArr['facebookId']];
            }

            # If users enters only twitter id
            if (!empty($postDataArr['twitterId']) && !isset($whereArr['where'])) {
                $whereArr['where'] = ['twitter_id' => $postDataArr['twitterId']];
            }

            if (empty($whereArr)) {
                $whereArr = array('where' => array('user_id' => $userId));
            }

            #fetching user details from db
            $user_info = $this->Common_model->fetch_data('users', ['user_id', 'user_image', 'twitter_id', 'facebook_id', 'fb_username', 'twitter_username', 'is_active', 'email_id'], $whereArr, true);
            if ((!empty($user_info) && isset($postDataArr['facebookId']) && $user_info['facebook_id'] == $postDataArr['facebookId']) && $user_info['user_id'] != $userId && (ACTIVE == $user_info['is_active'])) {
                #user is already Registered
                $response_array = [
                    'CODE' => FACEBOOKID_ALREADY_EXIST,
                    'MESSAGE' => $this->lang->line('fbid_exist'),
                    'RESULT' => (object) array(),
                ];
            } elseif ((!empty($user_info) && isset($postDataArr['twitterId']) && $user_info['twitter_id'] == $postDataArr['twitterId']) && $user_info['user_id'] != $userId && (ACTIVE == $user_info['is_active'])) {
                #user is already Registered
                $response_array = [
                    'CODE' => TWITTERID_ALREADY_EXIST,
                    'MESSAGE' => $this->lang->line('twitterid_exist'),
                    'RESULT' => (object) array(),
                ];
            } else {
                $whereArry = array('where' => array('user_id' => $userId));
                #fetching user details from db
                $user_info = $this->Common_model->fetch_data('users', [
                    'user_id', 'user_image',
                    'twitter_id', 'facebook_id', 'fb_username', 'twitter_username', 'is_active', 'email_id',
                    'is_bonus_received', 'whatsup_number', 'paytm_number', 'upi_address', 'account_number', 'total_earning',
                ], $whereArry, true);
                //if facebook id and twitter id is already in record than it will approve by admin
                $userUpdateRequest = array();

                if (!empty($user_info['facebook_id']) && isset($postDataArr['facebookId']) && !empty($postDataArr['facebookId'])) {
                    $userUpdateRequest['facebook_id'] = $postDataArr['facebookId'];
                    $userUpdateRequest['facebook_username'] = $postDataArr['fbUsername'];
                    $userUpdateRequest['user_id'] = $userId;
                }
                if (!empty($user_info['twitter_id']) && isset($postDataArr['twitterId']) && !empty($postDataArr['twitterId'])) {
                    $userUpdateRequest['twitter_id'] = $postDataArr['twitterId'];
                    $userUpdateRequest['twitter_username'] = $postDataArr['twitterUsername'];
                    $userUpdateRequest['user_id'] = $userId;
                }

                #start transaction
                $this->db->trans_begin();

                $newdata = array(
                    'user_image' => (isset($postDataArr['profileImage']) && !empty($postDataArr['profileImage'])) ? $postDataArr['profileImage'] : $user_info['user_image'],
                    'twitter_id' => (isset($postDataArr['twitterId']) && !empty($postDataArr['twitterId'])) ? $postDataArr['twitterId'] : $user_info['twitter_id'],
                    'twitter_username' => (isset($postDataArr['twitterUsername']) && !empty($postDataArr['twitterUsername'])) ? $postDataArr['twitterUsername'] : $user_info['twitter_username'],
                    'facebook_id' => (isset($postDataArr['facebookId']) && !empty($postDataArr['facebookId'])) ? $postDataArr['facebookId'] : $user_info['facebook_id'],
                    'fb_username' => (isset($postDataArr['fbUsername']) && !empty($postDataArr['fbUsername'])) ? $postDataArr['fbUsername'] : $user_info['fb_username'],
                    'updated_at' => datetime(),
                );
                
                #updating profile details
                $isSuccess = $this->Common_model->update_single('users', $newdata, array('where' => array('user_id' => $userId)));

                // if (empty($userUpdateRequest)) {
                //     //profile request array
                //     $newdata = array(
                //         'user_image' => (isset($postDataArr['profileImage']) && !empty($postDataArr['profileImage'])) ? $postDataArr['profileImage'] : $user_info['user_image'],
                //         'twitter_id' => (isset($postDataArr['twitterId']) && !empty($postDataArr['twitterId'])) ? $postDataArr['twitterId'] : $user_info['twitter_id'],
                //         'twitter_username' => (isset($postDataArr['twitterUsername']) && !empty($postDataArr['twitterUsername'])) ? $postDataArr['twitterUsername'] : $user_info['twitter_username'],
                //         'facebook_id' => (isset($postDataArr['facebookId']) && !empty($postDataArr['facebookId'])) ? $postDataArr['facebookId'] : $user_info['facebook_id'],
                //         'fb_username' => (isset($postDataArr['fbUsername']) && !empty($postDataArr['fbUsername'])) ? $postDataArr['fbUsername'] : $user_info['fb_username'],
                //         'updated_at' => datetime(),
                //     );
                    
                //     #updating profile details
                //     $isSuccess = $this->Common_model->update_single('users', $newdata, array('where' => array('user_id' => $userId)));
                // } else {
                //     $whereArry = array('where' => array('user_id' => $userId));
                //     $user_request_info = $this->Common_model->fetch_data('user_profile_change_request', ['request_id', 'full_name', 'email_id', 'user_image', 'upi_address', 'account_name', 'account_number', 'ifsc_code'], $whereArry, true);
                //     //if request already exist
                //     if (!empty($user_request_info)) {
                //         //update new request
                //         $isSuccess = $this->Common_model->update_single('user_profile_change_request', $userUpdateRequest, array('where' => array('user_id' => $userId)));
                //     } else {
                //         //else insert user request
                //         $isSuccess = $this->Common_model->insert_single('user_profile_change_request', $userUpdateRequest);
                //     }
                //     $userRequestUpdate = array(
                //         'requested_for_edit_profile' => REQUESTED,
                //     );
                //     #updating profile details
                //     $isSuccess = $this->Common_model->update_single('users', $userRequestUpdate, array('where' => array('user_id' => $userId)));
                // }

                #If query failed to update
                #Throw Exception
                if (!$isSuccess) {
                    $this->db->trans_rollback();
                    throw new Exception($this->lang->line('try_again'));
                }

                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();
                    $userInfo = $this->User_model->userProfileDetail($userId);
                    #fetching proof details from db
                    $where = array('where' => array('user_id' => $userId, 'status' => 0));
                    $where['order_by'] = ['inserted_on' => 'desc'];
                    $proofInfo = $this->Common_model->fetch_data(
                        'user_id_proof_details',
                        ['proof_id', 'id_number', 'proof_type', 'image'],
                        $where
                    );

                    $userInfo['id_proof'] = $proofInfo;
                    //for bonus block start
                    //if bonus is not receive by user on profile completition
                    if (isset($userInfo['is_bonus_received']) && ($userInfo['is_bonus_received']) == 0) {
                        //update user bonus
                        // $this->userBonusUpate($userId);
                    }
                    //bonus block end
                    #setting Response Array
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line('profile_updated'),
                        'RESULT' => (object) $userInfo,
                    ];
                } else {
                    #IF transaction failed
                    #rolling back
                    $this->db->trans_rollback();

                    #setting Response Array
                    $response_array = [
                        'CODE' => TRY_AGAIN_CODE,
                        'MESSAGE' => $this->lang->line('try_again'),
                        'RESULT' => [],
                    ];
                }
            }
            echo json_encode($response_array);
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    /**
     * @function userBonusUpdate
     * @description give user bonus 100 on profile
     * completion and when task completed by user is greater than 3
     * @param int $userId
     * @return void
     */
    private function userBonusUpate($userId)
    {
        $userInfo = $this->User_model->userDetail($userId);
        /**
         * Check if user fill all data and task completed by user is greater than 1
         */
        $profileComplete = 0;
        //facebook id
        if (!empty($userInfo['facebook_id'])) {
            $profileComplete++;
        }
        //twitter id
        if (!empty($userInfo['twitter_id'])) {
            $profileComplete++;
        }
        //whatsup
        if (!empty($userInfo['whatsup_number'])) {
            $profileComplete++;
        }
        //upi or paytm numbber or account number
        if (
            !empty($userInfo['upi_address']) || !empty($userInfo['paytm_number'])
            || !empty($userInfo['account_number'])
        ) {
            $profileComplete++;
        }
        $whereUserArr['where'] = ['user_id' => $userId];
        // check if user update in id proff or not
        $user_id_info = $this->Common_model->fetch_data('user_id_proof_details', [
            'proof_id',
        ], $whereUserArr, true);
        //id proof
        if (!empty($user_id_info)) {
            $profileComplete++;
        }

        //if profile completed by user
        if ((($profileComplete == 5) && ($userInfo['is_bonus_received'] == 0) && ($userInfo['totalTaskComplete'] >= 1) && ($userInfo['campaignStatus'] == 0))) {

            $insertWalletLog = array(
                'user_id' => $userId,
                'point' => BONUS_POINT,
                'title' => $this->lang->line('bonus_earned'),
                'description' => $this->lang->line('profile_complete'),
                'task_id' => 0,
                'type' => 'bonus',
                'status' => COMPLETE,
                'created_date' => date('Y-m-d H:i:s'),

            );
            //check if task history alredy exist fo
            $whereWall['where'] = ['user_id' => $userId, 'task_id' => 0, 'type' => 'bonus'];
            $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $whereWall, true);
            if (empty($walletLogInfo)) {
                #save users details values in DB
                $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);

                #if not success
                if (!$isSuccess) {
                    throw new Exception($this->lang->line('try_again'));
                }
            }
            //updating user value
            $updateUserArray = array(
                'total_earning' => ($userInfo['total_earning'] + BONUS_POINT),
                'last_earning_update' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_bonus_received' => BONUS_RECEIVED,
            );
            $whereUserArr['where'] = ['user_id' => $userId];
            //update user data in user table
            $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
            //sending notification
            $this->sendNotification($userId);
        }
    }

    /**
     * @function sendNotification
     * @description send notification on bonus received by user
     * @param int $userID id of user
     * @access private
     * @return void
     */
    private function sendNotification($userId)
    {
        //get user was ern to send notification
        $userDetail = $this->Notification_model->sendNotification($userId);
        if (isset($userDetail) && !empty($userDetail)) {
            //message arry for sending notification
            $message = [
                "default" => $this->lang->line('request_accept_not'),
                "APNS_SANDBOX" => json_encode([
                    "aps" => [
                        "alert" => array(
                            "title" => 'Bonus Greetings!',
                            "body" => $this->lang->line('bonus_point_notification'),
                        ),
                        "sound" => "default",
                        "mutable-content" => 1,
                        "badge" => 1,
                        "data" => array(
                            'notification_id' => '',
                            'news_id' => '',
                            "task_id" => '',
                            "content_type" => "image",
                            "type" => 5,
                        ),
                    ],
                ]),
                "GCM" => json_encode([
                    "data" => [
                        "title" => 'Bonus Greetings!',
                        "message" => $this->lang->line('bonus_point_notification'),
                        "body" => $this->lang->line('bonus_point_notification'),
                        "type" => 5,
                        "image" => '',
                        'news_id' => '',
                        'task_id' => '',
                        'notification_id' => '',

                    ],
                ]),
            ];
            //sns object
            $sns = new Snspush();
            //send message to filtered users or selected users
            $result = $sns->asyncCouponPublish($message, $userDetail);
            //sending notification array
            $insertNewsNoti = array(
                'user_id' => $userId,
                'news_id' => 0,
                'task_id' => 0,
                'notification_type' => BONUS_NOTIFICATION_TYPE,
                'notification_text' => $this->lang->line('bonus_point_notification'),
                'inserted_on' => date('Y-m-d H:i:s'),
            );
            //insert in user notification table
            $updateId = $this->Common_model->insert_single('user_notification', $insertNewsNoti);
        }
    }
}
