<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class App_Version extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }



    /**
     * @SWG\Get(path="/app_Version",
     *   tags={"Version check"},
     *   summary="Version check",
     *   description="Version",
     *   operationId="versionindex_get",
     *   produces={"application/json"},
     * @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="platform",
     *     in="query",
     *     description="Platform 1 for android and 2 for ios",
     *     required=true,
     *     type="number"
     *   ),
     *
     * @SWG\Response(response=200, description="Success"),
     * @SWG\Response(response=206, description="Unauthorized request"),
     * @SWG\Response(response=207, description="Header is missing"),
     * @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * @SWG\Response(response=201, description="Try again something went wrong"),
     * @SWG\Response(response=202, description="No record found"),
     * )
     */
    public function index_get()
    {

        try {
            $get = $this->input->get();
                       
            //if platform is set
            if (isset($get['platform']) && !empty($get['platform'])) {
                //get version data from database
                $getVersion = $this->Common_model->fetch_data('app_version', '', array('where' => array('platform' => $get['platform']), 'order_by' => array('created_date' => 'desc')), true);
                if (isset($get['version_code']) && !empty($get['version_code'])) {                   
                    if (isset($get['user_id']) && !empty($get['user_id'])) {
                        $userId = $get['user_id'];
                        $wherenewsCond = array('where' => array('user_id' => $userId));
                        $newdata = array('app_version' => $get['version_code']);
                        $this->Common_model->update_single('users', $newdata, $wherenewsCond);
                    }
                }
            } else {
                throw new Exception($this->lang->line('try_again'));
            }
            //if version data is there
            if ($getVersion) {
                //setting response array
                $responseData = $getVersion;
            } else {
                $responseData = [];
            }
            //setting response
            $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => "", 'RESULT' => $responseData));
        } catch (Exception $e) {
            //exception
            $error = $e->getMessage();
            $this->response(array('CODE' => TRY_AGAIN_CODE, 'MESSAGE' => $error, 'RESULT' => []));
        }
    }
}
