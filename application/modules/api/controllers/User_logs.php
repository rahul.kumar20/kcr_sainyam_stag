<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class User_logs extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    /**
     * @SWG\Post(path="/logs",
     *   tags={"User"},
     *   summary="User Active Logs",
     *   description="User Active Logs",
     *   operationId="index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *     name="device_id",
     *     in="formData",
     *     description="Device Id",
     *     required=True,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="deactive_date",
     *     in="formData",
     *     description="Date",
     *     required=False,
     *     type="string"
     *   ),
     *
     *   @SWG\Response(response=101, description="Account Blocked"),
     *   @SWG\Response(response=200, description="Login Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function index_post()
    {
        try {

            $flag   = STORE_SESSION_LOGS;

            if($flag > 0){
                
                # load session model
                $this->load->model("Session");

                $postDataArr = $this->post();
                $response_array = array();
                //logged user id
                $userId = $GLOBALS['api_user_id'];

                #setting Form  validation Rules
                $required_fields_arr = array(
                    array(
                        'field' => 'device_id',
                        'label' => 'Device Id',
                        'rules' => 'trim|required',
                    ),
                    array(
                        'field' => 'deactive_date',
                        'label' => 'Date',
                        'rules' => 'trim',
                    ),

                );
                #Setting Error Messages for rules
                $this->form_validation->set_rules($required_fields_arr);
                $this->form_validation->set_message('required', 'Please enter the %s');
                if ($this->form_validation->run()) {
                    $whereArr = [];

                    //logs array
                    $logsInsArray = array(
                        'user_id' => $userId,
                        'active_date' => datetime(),
                        'device_id' => isset($postDataArr['device_id']) && !empty($postDataArr['device_id']) ? $postDataArr['device_id'] : '',
                        'ip_address' => ip2long($_SERVER['REMOTE_ADDR']),
                    );
                    #start transaction
                    $this->db->trans_begin();
                    # If deactive date is send by app
                    if (!empty($postDataArr['deactive_date'])) {
                        #fetching user details from db
                        $whereArr = array('where' => array('user_id' => $userId, 'device_id' => $postDataArr['device_id']));
                        $whereArr['order_by'] = ['log_id' => 'asc'];
                        $user_device_info = $this->Common_model->fetch_data('user_session_logs', ['log_id'], $whereArr, true);
                        if (!empty($user_device_info)) {
                            $updateArry = array(
                                'deactive_date' => date('Y-m-d H:i:s', strtotime($postDataArr['deactive_date'])),
                            );
                            $isSuccess = $this->Common_model->update_single('user_session_logs', $updateArry, array('where' => array('user_id' => $userId, 'device_id' => $postDataArr['device_id'], 'deactive_date' => '0000-00-00 00:00:00')));
                        }
                    }

                    #updating logs details
                    $isSuccess = $this->Common_model->insert_single('user_session_logs', $logsInsArray);
                    #If query failed to update
                    #Throw Exception
                    if (!$isSuccess) {
                        $this->db->trans_rollback();
                        throw new Exception($this->lang->line('try_again'));
                    }

                    #if transaction runs successfully
                    if (true === $this->db->trans_status()) {
                        #Comminting changes
                        $this->db->trans_commit();
                        #setting Response Array
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('logs_success'),
                            'RESULT' => [],
                        ];
                    } else {
                        #IF transaction failed
                        #rolling back
                        $this->db->trans_rollback();

                        #setting Response Array
                        $response_array = [
                            'CODE' => TRY_AGAIN_CODE,
                            'MESSAGE' => $this->lang->line('try_again'),
                            'RESULT' => [],
                        ];
                    }
                } else {
                    $err = $this->form_validation->error_array();
                    $arr = array_values($err);
                    #setting Response Array
                    $response_array = [
                        'CODE' => PARAM_REQ,
                        'MESSAGE' => $arr[0],
                        'RESULT' => [],
                    ];
                }
            }else{
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'User logs disabled',
                    'RESULT' => [],
                ];
            }


            $this->response($response_array);
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }
}
