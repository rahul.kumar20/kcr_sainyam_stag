<?php

defined ( 'BASEPATH' ) OR exit ( 'No direct script access allowed' );
require APPPATH . '/libraries/REST_Controller.php';

class Earning extends REST_Controller {

    function __construct ()
    {
        parent::__construct ();
        $this->load->model ( 'Common_model' );
        $this->load->helper ( 'email' );

    }

    public function index_post ()
    {
        
        
            $where = array ( 'where' => array ( 'id' => 147 ) );                   
            $isExist  = $this->Common_model->fetch_data ( 'temp', array ('twitter_data' ), $where, true );

            $data = json_decode($isExist['twitter_data']);
            
            $value1 = json_decode(json_encode($data), true);
           
            
            
            $event = '';
            foreach($value1  as  $key => $val)
            {
                if ($key == 'favorite_events' || $key == 'tweet_create_events')
                {
                    $event = $key;
                }
                
            }
           
            $user_id =  $value1[$event][0]['user']['id'];
            $user_id_str =  $value1[$event][0]['user']['id_str'];
            
           
            # if user favourite a status
            if ($event == 'favorite_events')
            {
              $tweet_id = $value1[$event][0]['favorited_status']['id'];  
              $tweet_id_str = $value1[$event][0]['favorited_status']['id_str']; 
            }else if($event == 'tweet_create_events' && $value1[$event][0]['is_quote_status'] == 1)
            {
                # if user retweeted a status
                $tweet_id = $value1[$event][0]['quoted_status_id'];  
                $tweet_id_str = $value1[$event][0]['quoted_status_id_str']; 
               
         $action = 'retweet';       
         #get the task id based on the action performed by user
         $task_cond_where = array('where' => array('post_id' => $tweet_id,'action' => $action));
        
        $taskInfo = $this->Common_model->fetch_data('ipac_task_master', ['task_id', 'task_status','start_date','end_date','points'], $task_cond_where, true);
        
        if($taskInfo)
        {
           #check the user id according to fb id 
           $user_cond_where = array('where' => array('twitter_id' => $user_id));
           $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active'], $user_cond_where, true);
         
           if($userInfo)
           {
                # check wheather points added for this task or not
                $task_master_where = array('where' => array('task_id ' => $taskInfo['task_id'],'user_id'=>$userInfo['user_id']));
                $usertaskInfo = $this->Common_model->fetch_data('user_task_master', ['detail_id'], $task_master_where, true);
                
                if(!$usertaskInfo)
                {
                    # insert user task completed data
                    $taskArr = array(
                        'user_id' => $userInfo['user_id'],
                        'task_id' => $taskInfo['task_id'],
                        'status' => 1,
                        'added_on' => date('Y-m-d H:i:s')
                    );
                    $usertaskSuccess = $this->Common_model->insert_single('user_task_master', $taskArr);
                    
                    # insert user earnings data
                    
                    $start_date    = date('Y-m-01') ;
                    $end_date    = date('Y-m-t');
                   
                    $earningArr = array(
                        'user_id' => $userInfo['user_id'],
                        'month_start_date' => $start_date,
                        'month_end_date' => $end_date,
                        'total_earned_points' => $taskInfo['points'],
                        'reward_payment_amt' => $taskInfo['points'],
                        'reward_clear_status' => 0,
                        'inserted_on' => date('Y-m-d H:i:s')
                    );
                  
                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                }
               
           }
           
           
           
        }     
                
                
            }
            
            echo $user_id.'---';
            echo $user_id_str.'---';
            echo $tweet_id.'---';
            echo $tweet_id_str.'---';
            print_r($value1);
            die; 
        
        
        
        
        
        die;
        
        #facebook webhook
        
        $where = array ( 'where' => array ( 'id' => 134 ) );                   
        $isExist  = $this->Common_model->fetch_data ( 'temp', array ('fb_data' ), $where, true );

        $data = json_decode($isExist['fb_data']);
        $postId  = $item = $verb = $userFbId = '';
        $value1 = json_decode(json_encode($data), true);
        $changes = $value1['entry'][0]['changes'][0];
        
        # get the type of action performed by user i.e like,share
        if(array_key_exists('item', $changes['value']))
        {
            $item = $changes['value']['item'];
            $verb = $changes['value']['verb'];
        }
        #get the id of user who performed the action
        if(array_key_exists('from', $changes['value']))
        {
           $userFbId= $changes['value']['from']['id'];
        }
        #get post id on which action is performed
        if(array_key_exists('post_id', $changes['value']))
        {
            $postIdArr = explode("_",$changes['value']['post_id']);
            $postId = $postIdArr[1];
        }
        
        #get the task id based on the action performed by user
        $task_cond_where = array('where' => array('post_id' => $postId,'action' => $item));
        
        
        $taskInfo = $this->Common_model->fetch_data('ipac_task_master', ['task_id', 'task_status','start_date','end_date','points'], $task_cond_where, true);
      
        if($taskInfo)
        {
           #check the user id according to fb id 
           $user_cond_where = array('where' => array('facebook_id' => $userFbId));
           $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active'], $user_cond_where, true);
          
           if($userInfo)
           {
                # check wheather points added for this task or not
                $task_master_where = array('where' => array('task_id ' => $taskInfo['task_id'],'user_id'=>$userInfo['user_id']));
                $usertaskInfo = $this->Common_model->fetch_data('user_task_master', ['detail_id'], $task_master_where, true);
                if(!$usertaskInfo)
                {
                    # insert user task completed data
                    $taskArr = array(
                        'user_id' => $userInfo['user_id'],
                        'task_id' => $taskInfo['task_id'],
                        'status' => 1,
                        'added_on' => date('Y-m-d H:i:s')
                    );
                    $usertaskSuccess = $this->Common_model->insert_single('user_task_master', $taskArr);
                    
                    # insert user earnings data
                    
                    $start_date    = date('Y-m-01') ;
                    $end_date    = date('Y-m-t');
                   
                    $earningArr = array(
                        'user_id' => $userInfo['user_id'],
                        'month_start_date' => $start_date,
                        'month_end_date' => $end_date,
                        'total_earned_points' => $taskInfo['points'],
                        'reward_payment_amt' => $taskInfo['points'],
                        'reward_clear_status' => 0,
                        'inserted_on' => date('Y-m-d H:i:s')
                    );
                  
                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                }
               
           }
           
           
           
        }
       
    }
    
    
}