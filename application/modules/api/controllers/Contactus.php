<?php
 defined('BASEPATH') or exit('No direct script access allowed');
 require APPPATH.'/libraries/REST_Controller.php';
 require APPPATH . 'libraries/SNSPush.php';

class Contactus extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model('admin/Notification_model');

        $this->load->helper('sendsms');
        $this->load->library('Common_function');
    }



      /**
      * @SWG\get(path="/contactus",
      *   tags={"Contact Us"},
      *   summary="Contact Us category",
      *   description="Contact Us category",
      *   operationId="index_get_contact_category",
      *   consumes ={"multipart/form-data"},
      *   produces={"application/json"},
      *   @SWG\Parameter(
      *     name="Authorization",
      *     in="header",
      *     description="",
      *     required=true,
      *     type="string"
      *   ),
      *  @SWG\Parameter(
      *     name="Accesstoken",
      *     in="header",
      *     description="",
      *     required=true,
      *     type="string"
      *   ),
      *
      *   @SWG\Response(response=200, description="Success"),
      *   @SWG\Response(response=206, description="Unauthorized request"),
      *   @SWG\Response(response=207, description="Header is missing"),
      *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
      * )
      */
     /*
      */
    
    public function index_get()
    {
                
        try {
            $this->load->model("Session");
            $params = $this->get();
            $userId = $GLOBALS['api_user_id'];
            $response_array = array();
            $where = array('where' => array('user_id' => $userId));
            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active'], $where, true);
            
            #fetching admin contact from db
            $adminInfo = $this->Common_model->fetch_data('admin', ['phone'], [], true);
            
            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
                    #fetching contact us category data from db
                    $where = array();
                    $where['order_by'] = ['category_name' => 'desc' ];
                    $category = $this->Common_model->fetch_data('contact_us_category', ['category_id', 'category_name', 'category_name_tn'], $where);
                    $settingsData = $this->Common_model->fetch_data(
                        'ipac_settings',
                        '*',
                        array(),
                        true
                    );
                    $helplineno = '';
                    if (!empty($settingsData)) {
                        $helplineno = $settingsData['help_line_number'];
                    }
                    #setting Response
                    $response_array = [
                        'CODE' => SUCCESS_CODE,
                        'MESSAGE' => $this->lang->line('category_fetched'),
                        'RESULT' =>  $category,
                        'HELPLINENO' => $helplineno
                    ];
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array = [
                        'CODE' => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT' => $empty_response
                    ];
                }
            }

            #sending Response
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE'   => TRY_AGAIN_CODE,
                'MESSAGE'    => $error,
                'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END
    }
    
        /**
     * @SWG\post(path="/contactus",
     *   tags={"Contactus"},
     *   summary="Save contact us data",
     *   description="Save contact content of users",
     *   operationId="index_post",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   )
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_post()
    {
        try {
            $postDataArr = $this->post();
            $empty_response = (object)array();
            #Setting Form validation Rules
            $config = array (
                array (
                    'field' => 'category_id',
                    'label' => 'Contact Category',
                    'rules' => 'trim|required'
                ),
                 array (
                    'field' => 'contact_content',
                    'label' => 'Contact Content',
                    'rules' => 'trim|required'
                ),
            );

            $this->form_validation->set_rules($config);
            $this->form_validation->set_message('required', 'Please enter the %s');

            $response_array = [];
            if ($this->form_validation->run()) {
                 $this->load->model("Session");

                $params = $this->post();
                $userId = $GLOBALS['api_user_id'];
                $response_array = array();
                $where = array('where' => array('user_id' => $userId));
                #fetching user details from db
                $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active','full_name','email_id','phone_number','registeration_no'], $where, true);

                if (!empty($userInfo)) {
                    $ticketId = $this->generateTicketNo($userInfo['registeration_no'], $userInfo['user_id']);
                    /*
                     * Check if user is not blocksed
                     */
                    if (ACTIVE == $userInfo['is_active']) {
                              $contact_category        = $postDataArr['category_id'];
                              $contact_us_content        = $postDataArr['contact_content'];
                        #insert contact us category data from db
                         $newdata = array('contact_category' => $contact_category,
                                          'contact_us_content' => $contact_us_content,
                                          'user_id' => $userId,
                                          'ticket_id'=>$ticketId,
                                          'inserted_on' => datetime());
                         $proofId = $this->Common_model->insert_single('user_contact_us', $newdata);
                         $mailData               = [];
                         $mailData['name']       = $userInfo['full_name'];
                         $mailData['email']      = $userInfo['email_id'];
                         $mailData['registration_no']      = $ticketId;

                         $mailData['mailerName'] = 'Contact us';
                         $subject                = 'Contact us Goa Deserves Better';
                         $mailerName             = 'contactus';
                         #sending welcome mail
                         // $this->sendWelcomeMail( $mailData );
                         sendmail($subject, $mailData, $mailerName);
                        $this->sendNotification($userId, $ticketId);
                        #setting Response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('contact_success'),
                            'RESULT' => []
                        ];
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => $empty_response
                        ];
                    }
                }

                #sending Response
                $this->response($response_array);
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array ( 'CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => ( object ) array () ));
            }

            #Sending Response
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END
    }

     /**
     * @function sendNotification
     * @description send notification on bonus received by user
     * @param int $userID id of user
     * @access private
     * @return void
     */
    private function sendNotification($userId, $tickedId)
    {
        //get user was ern to send notification
        $userDetail = $this->Notification_model->sendNotification($userId);
        if (isset($userDetail) && !empty($userDetail)) {
            //message arry for sending notification
            $message = [
                "default" => $this->lang->line('request_accept_not'),
                "APNS_SANDBOX" => json_encode([
                    "aps" => [
                        "alert" => array(
                            "title" => $this->lang->line('contact_us').$tickedId.$this->lang->line('contact_us_2'),
                            "body" => $this->lang->line('contact_us').$tickedId.$this->lang->line('contact_us_2'),
                        ),
                        "sound" => "default",
                        "mutable-content" => 1,
                        "badge" => 1,
                        "data" => array(
                            'notification_id' => '',
                            'news_id' => '',
                            "task_id" => '',
                            "content_type" => "image",
                            "type" => 6
                        )
                    ]
                ]),
                "GCM" => json_encode([
                    "data" => [
                        "title" => $this->lang->line('contact_us').$tickedId.$this->lang->line('contact_us_2'),
                        "message" => $this->lang->line('contact_us').$tickedId.$this->lang->line('contact_us_2'),
                        "body" => $this->lang->line('contact_us').$tickedId.$this->lang->line('contact_us_2'),
                        "type" => 6,
                        "image" => '',
                        'news_id' => '',
                        'task_id' => '',
                        'notification_id' => '',

                    ]
                ])
            ];
             //sns object
            $sns = new Snspush();
               //send message to filtered users or selected users
            $result = $sns->asyncPublish2($userDetail, $message);
           //sending notification array
            $insertNewsNoti = array(
                'user_id' => $userId,
                'news_id' => 0,
                'task_id' => 0,
                'notification_type' => 'contact_us',
                'notification_text' => $this->lang->line('contact_us').$tickedId.$this->lang->line('contact_us_2'),
                'inserted_on' => date('Y-m-d H:i:s')
            );
            //insert in user notification table
            $updateId = $this->Common_model->insert_single('user_notification', $insertNewsNoti);
        }
    }

    /* Generating user registration number
     * IPAC XXX PTA YYYY (IPAC, PTA are static and XXX, YYYY are variables)
        First 4 digits represents the company (IPAC) name
        XXX represents the district code (number)
        PTA represents as Part Time Associate
        YYYY represents the volunteer serial number (in respective districts)
     */
    private function generateTicketNo($registrationNo, $userId)
    {
        
        #generate 3 digit district code of user district
        $contactInfo = $this->Common_model->fetch_count('user_contact_us', array());
        if (!empty($contactInfo)) {
            $district_num_padded = sprintf("%06d", $contactInfo);
        } else {
            $district_num_padded = '000001';
        }

        $prefix = $registrationNo;
        $sufix = 'CA';
        $ticketCode = $prefix .'_'.$sufix .'_'. $district_num_padded ;
        return $ticketCode;
    }
}
