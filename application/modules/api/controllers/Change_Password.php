<?php
 require APPPATH.'libraries/REST_Controller.php';

 /**
  * @class Change_Password
  * @description This api controller is used to change logged in user password
  *             User Authentication is checked using Hook in Custom_hook
  */
class Change_Password extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }



    /**
      * @SWG\Post(path="/Change_Password",
      *   tags={"User"},
      *   summary="Change Password",
      *   description="Change Password",
      *   operationId="index_post",
      *   consumes ={"multipart/form-data"},
      *   produces={"application/json"},
      *   @SWG\Parameter(
      *     name="Authorization",
      *     in="header",
      *     description="",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Parameter(
      *     name="oldpassword",
      *     in="formData",
      *     description="oldpassword",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Parameter(
      *     name="accesstoken",
      *     in="header",
      *     description="Access Token",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Parameter(
      *     name="password",
      *     in="formData",
      *     description="New password",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Response(response=200, description="Reset Password Success"),
      *   @SWG\Response(response=206, description="Unauthorized request"),
      *   @SWG\Response(response=207, description="Header is missing"),
      *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
      *   @SWG\Response(response=490, description="Old password not matched"),
      *   @SWG\Response(response=491, description="New and Old password are same"),
      *   @SWG\Response(response=501, description="Please try again"),
      * )
      */
    public function index_post()
    {
        try {
            # load session model
            $this->load->model("Session");

            $postDataArr    = $this->post();
            $response_array = array ();

            #setting form validation rule
            $config = array (
                array (
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required|min_length[6]'
                ),
                array (
                    'field' => 'oldpassword',
                    'label' => 'Old Password',
                    'rules' => 'required'
                )
            );
            #setting Rules
            $this->form_validation->set_rules($config);

            #Setting Error Messages for rules
            $this->form_validation->set_message('required', 'Please enter the %s');

            #Checking form validation
            if ($this->form_validation->run()) {
                #getting Access Token from GLobal Array Using Hooks
                $accesstoken = $GLOBALS['login_user']['Accesstoken'];
                $respArr     = $this->Session->getUserInfo($accesstoken, ['u.user_id', 'password', 'is_active']);
                $user_info   = [];
            
                #Response is not success if session expired or invalid access token
                if (SUCCESS_CODE === $respArr['code']) {
                    $user_info = $respArr['userinfo'];

                    #Old password from POST array
                    $oldpassword = createPassword($postDataArr['oldpassword']) ;
                    $savedpassword = $user_info['password'];
                    
                    #new password From POST Array
                    $newpassword =  $postDataArr['password'] ;


                    #match old password and entered current password
                    if (isset($savedpassword) && $savedpassword != $oldpassword) {
                        #setting Response Array
                        $response_array = [
                            'CODE'    => OLD_PASSWORD_MISMATCH,
                            'MESSAGE' => $this->lang->line('old_password_wrong'),
                            'RESULT'  => []
                        ];
                    } #if end
                    elseif ($oldpassword == $newpassword) {#if old password is same as new password
                        #setting Response Array
                        $response_array = [
                            'CODE'    => NEW_PASSWORD_SAME,
                            'MESSAGE' => $this->lang->line('password_exist'),
                            'RESULT'  => []
                        ];
                    } #else IF end
                    else {
                        #start transaction
                        $this->db->trans_begin();

                        $newdata = array ('password' => createPassword($newpassword), 'updated_at' => datetime());

                        #updating password and updated_at time
                        $isSuccess = $this->Common_model->update_single('users', $newdata, array ('where' => array ('user_id' => $user_info['user_id'])));

       
                        #If query failed to update password
                        #Throw Exception
                        if (!$isSuccess) {
                            $this->db->trans_rollback();
                            throw new Exception($this->lang->line('try_again'));
                        }

                        #if transaction runs successfully
                        if (true === $this->db->trans_status()) {
                            #Comminting changes
                            $this->db->trans_commit();

                            #setting Response Array
                            $response_array = [
                                'CODE'    => SUCCESS_CODE,
                                'MESSAGE' => $this->lang->line('password_change_success'),
                                'RESULT'  => []
                            ];
                        } else {#IF transaction failed
                            #rolling back
                            $this->db->trans_rollback();

                            #setting Response Array
                            $response_array = [
                                'CODE'    => TRY_AGAIN_CODE,
                                'MESSAGE' => $this->lang->line('try_again'),
                                'RESULT'  => []
                            ];
                        }
                    }
                     
                    $this->response($response_array);
                } #if END
                else {
                    #setting Response Array
                    $response_array = $respArr;
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);

                #setting Response Array
                $response_array = [
                    'CODE'   => PARAM_REQ,
                    'MESSAGE'    => $arr[0],
                    'RESULT' => []
                ];
            }

            #sending Response
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE'   => TRY_AGAIN_CODE,
                'MESSAGE'    => $error,
                'RESULT' => []
            ];

            #sending Response
            $this->response($response_array);
        }#CATCH END
    }
}
