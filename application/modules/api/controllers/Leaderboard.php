<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Leaderboard extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model("Home_model", "home");
        $this->load->database();
    }

    /**
     * @SWG\Get(path="/leaderboard",
     *   tags={"News"},
     *   summary="Get app dashboard secton",
     *   description="Get User Rank,Rating and Rewards",
     *   operationId="index_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="count",
     *     in="query",
     *     description="hit with count to get next page",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="start_date",
     *     in="query",
     *     description="hit with count to get next page",
     *     type="string",
     *     required=false,
     *   ),
     *   @SWG\Parameter(
     *     name="end_date",
     *     in="query",
     *     description="Start date ",
     *     type="string",
     *     required=false,
     *   ),
     *    @SWG\Parameter(
     *     name="task_platform",
     *     in="query",
     *     description="End date ",
     *     type="string",
     *     required=false,
     *   ),
     *   @SWG\Parameter(
     *     name="flag",
     *     in="query",
     *     description="Task Platform like facebook,twitter",
     *     type="string",
     *     required=false,
     *   ),
     *   @SWG\Parameter(
     *     name="flag",
     *     in="query",
     *     description="flag 1 or 2,3",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_get()
    {
        try {
            $params     = $this->get();
            $userId     = $GLOBALS['api_user_id'];

            $flag                   = isset($params['flag']) ? (int) $params['flag'] : 1;
            $start_date             = isset($params['start_date']) ? $params['start_date'] : '';
            $end_date               = isset($params['end_date']) ? $params['end_date'] : '';
            $count                  = isset($params['count']) ? $params['count'] : 0;
            $leaderscount           = isset($params['leaderscount']) ? $params['leaderscount'] : 0;
            $districtleaderscount   = isset($params['districtleaderscount']) ? $params['districtleaderscount'] : 0;
            $task_platform          = isset($params['task_platform']) && !empty($params['task_platform']) ? $params['task_platform'] : '';
            $state                  = isset($params['state']) && !empty($params['state']) ? $params['state'] : '';

            $where['state']                     = $state;
            $where['leaderlimit']               = PAGE_LIMIT;
            $where['leaderoffset']              = $leaderscount;
            $where['districtleaderoffset']      = $districtleaderscount;
            $where['limit']                     = PAGE_LIMIT;
            $where['offset']                    = $count;
            $where['taskPlatform']              = $task_platform;
            $where['is_profile_edited']         = 1;
            $total_count                        = 0;
            $total_leaders_count                = 0;
            $district_total_leaders_count       = 0;
            $this->next_count                   = $where['offset'] + $where['limit'];
            $this->next_leaders_count           = $where['leaderoffset'] + $where['leaderlimit'];
            $this->next_district_leaders_count  = $where['districtleaderoffset'] + $where['leaderlimit'];

            #fetching user details from db
            $wherecond              = array('where' => array('user_id' => $userId));
            $userInfo               = $this->Common_model->fetch_data('users', [
                'user_id', 'is_active', 'state', 'district', 'gender', 'registeration_no',
                'full_name', 'user_image', '0 as rating', 'registered_on', 'paytm_number', 'upi_address', 'account_number',
                'is_profile_verified'
            ], $wherecond, true);

            //$rewards              = $this->home->getUserRanking($userId, '', '', $task_platform);
            if ($task_platform) {
                $rewards            = $this->home->getUserRanking($userId, '', '', $task_platform);
            } else {
                $rewards            = $this->home->getUserEarnings($userId);
            }
            $userInfo['rewards']    = !empty($rewards) ? $rewards['total_rewards'] : '0';
            $userInfo['rank']       = 0;

            if (!empty($userInfo)) {
                /*
                 * Check if user is not blocksed
                 */
                if (ACTIVE == $userInfo['is_active']) {
                    if ($flag == 1 || $flag == 3) {
                        $leaders_data   = $this->home->fetch_app_leaders($where['leaderlimit'], $where['leaderoffset'], $start_date, $end_date, $where);

                        $leaderArray    = $leaders_data['leaders'];
                        if (count($leaderArray) > 0) {
                            foreach ($leaderArray as $index => $val) {
                                $leaderArray[$index]['ranking'] = $index + 1;
                            }
                        }
                        // if (!empty($leaderArray)) {
                        //     $leaderArray = array_map(function ($element) use ($start_date, $end_date, $userInfo, $where) {
                        //         $element["ranking"] = $this->home->userAppRank($start_date, $end_date, $userInfo, $where, $element['user_id']);
                        //         return $element;
                        //     }, $leaderArray);
                        // }
                    }

                    if ($flag == 1 || $flag == 4) {
                        $district_leaders_data      = array();
                        $districtLeaderArray        = array();
                        $districtWhere              = $where;
                        $districtWhere['district']  = $userInfo['district'];
                        $district_leaders_data      = $this->home->fetch_app_leaders($where['leaderlimit'], $where['districtleaderoffset'], $start_date, $end_date, $districtWhere);
                        if (!empty($district_leaders_data)) {
                            $districtLeaderArray        = $district_leaders_data['leaders'];
                            if (count($districtLeaderArray) > 0) {
                                foreach ($districtLeaderArray as $disIndex => $disVal) {
                                    $districtLeaderArray[$disIndex]['ranking'] = $disIndex + 1;
                                }
                            }
                        }
                    }

                    if ($flag == 1 || $flag == 2) {
                        $all_data = $this->home->fetch_daily_progress($where['limit'], $where['offset'], $userId, $start_date, $end_date, $where, $userInfo);
                    }

                    // Total users in a state
                    $sql = "SELECT COUNT(*) AS total FROM (SELECT user_id,points_earned,state FROM users t where t.is_profile_edited ='1' GROUP BY t.user_id) state_count";
                    $query = $this->db->query($sql, array($userInfo['state']));
                    if ($query->num_rows() > 0) {
                        $total_members_state = $query->result_array();
                        $users_count_state = $total_members_state[0]['total'];
                    } else {
                        $users_count_state = 0;
                    }

                    // Total users in a particular state and particular district
                    $sql = "SELECT COUNT(*) AS total FROM (SELECT user_id,points_earned,state,district FROM users t where t.is_profile_edited ='1' GROUP BY t.user_id) state_count WHERE district=?";
                    $query = $this->db->query($sql, array($userInfo['district']));
                    if ($query->num_rows() > 0) {
                        $total_members_district = $query->result_array();
                        $users_count_district = $total_members_district[0]['total'];
                    } else {
                        $users_count_district = 0;
                    }

                    // state rank of a user
                    // $sql = "SELECT rank FROM (SELECT user_id,points_earned,state,(SELECT COUNT(*)+1 FROM users B WHERE A.points_earned<B.points_earned AND B.state=?) AS rank FROM users A WHERE A.state=? GROUP BY A.user_id ORDER BY A.points_earned DESC) q WHERE q.user_id=?";
                    $sql = "SELECT rank FROM (SELECT user_id, @rownum := @rownum + 1 AS rank FROM users,(SELECT @rownum := 0) r WHERE is_profile_edited ='1' ORDER BY points_earned DESC) as p WHERE user_id = ?";
                    $query = $this->db->query($sql, array($userInfo['user_id']));
                    if ($query->num_rows() > 0) {
                        $state_rank = $query->result_array();
                        $state_rank = $state_rank[0]['rank'];
                    } else {
                        $state_rank = 0;
                    }

                    // district rank of a user
                    // $sql = "SELECT rank FROM (SELECT user_id,points_earned,state,(SELECT COUNT(*)+1  FROM users B WHERE A.points_earned<B.points_earned AND B.state=? AND B.district=?) AS rank FROM users A WHERE A.state=? AND A.district=? GROUP BY A.user_id ORDER BY A.points_earned DESC) q WHERE q.user_id=?";
                    $sql = "SELECT rank FROM (SELECT user_id, @rownum := @rownum + 1 AS rank FROM users,(SELECT @rownum := 0) r WHERE district = ? and is_profile_edited ='1' ORDER BY points_earned DESC) as p WHERE user_id = ?";
                    $query = $this->db->query($sql, array($userInfo['district'], $userInfo['user_id']));
                    if ($query->num_rows() > 0) {
                        $district_rank = $query->result_array();
                        $district_rank = $district_rank[0]['rank'];
                    } else {
                        $district_rank = 0;
                    }

                    $result                             = array();
                    $result['state_rank']               = $state_rank;
                    $result['district_rank']            = $district_rank;
                    $result['state_total']              = $users_count_state;
                    $result['district_total']           = $users_count_district;
                    if ($flag == 3 || $flag == 1) {
                        $total_leaders_count            = $leaders_data['total_leaders'];
                        /**
                         * check is data remaining or not
                         */
                        if ($total_leaders_count > 10 || $total_leaders_count <= $this->next_leaders_count) {
                            $this->next_leaders_count   = '-1';
                        }
                        $result['topleaders']           = $leaderArray;
                    }

                    if ($flag == 4 || $flag == 1) {
                        if (!empty($district_leaders_data)) {
                            $district_total_leaders_count   = $district_leaders_data['total_leaders'];
                        } else {
                            $district_total_leaders_count   = 0;
                        }

                        if ($district_total_leaders_count > 10 || $district_total_leaders_count <= $this->next_district_leaders_count) {
                            $this->next_district_leaders_count = '-1';
                        }
                        $result['districtTopleaders']   = $districtLeaderArray;
                    }

                    if ($flag == 1) {
                        $total_count            = 0;
                        $result['userInfo']     = $userInfo;
                    }
                    if ($flag == 1 || $flag == 2) {
                        $task                   = $all_data['task'];
                        $total_count            = $all_data['total_task'];

                        /**
                         * check is data remaining or not
                         */
                        if ($total_count <= $this->next_count) {
                            $this->next_count   = '-1';
                        }

                        $result['task']         = $task;
                    }

                    //flag = 1, means all data includes daily progress, state top leaders and ditrict top leaders
                    if ($flag == 2) { // display only daily progress data
                        $this->next_leaders_count           = '-1';
                        $total_leaders_count                = 0;

                        $this->next_district_leaders_count  = '-1';
                        $district_total_leaders_count       = 0;
                    } elseif ($flag == 3) { // display only state top leaders data
                        $this->next_count                   = '-1';
                        $total_count                        = 0;

                        $this->next_district_leaders_count  = '-1';
                        $district_total_leaders_count       = 0;
                    } elseif ($flag == 4) { // display only district top leaders data
                        $this->next_count                   = '-1';
                        $total_count                        = 0;

                        $this->next_leaders_count           = '-1';
                        $total_leaders_count                = 0;
                    }

                    //get term and condition detail
                    // $where = array('where' => array('status' => ACTIVE));
                    // $where['order_by'] = ['created_date' => 'desc'];

                    // $termConditionInfo = $this->Common_model->fetch_data(
                    //     'ipac_term_condition',
                    //     ['version', 'get_term_condition_accept_status(' . $userId . ') as term_accept_status,get_is_user_added_id(' . $userId . ') as id_proff_added,
                    //      get_campaign_form_status(' . $userId . ') as campaign_status'],
                    //     $where,
                    //     true
                    // );
                    $termConditionInfo['version']               = '1';
                    $termConditionInfo['term_accept_status']    = '0';
                    $termConditionInfo['id_proff_added']        = '0';
                    $termConditionInfo['campaign_status']       = '1';
                    //check if user added payment detail
                    $paymentDetailAdded             = 0;
                    if (
                        !empty($userInfo['paytm_number']) || !empty($userInfo['upi_address'])
                        || !empty($userInfo['account_number'])
                    ) {
                        $paymentDetailAdded         = 1;
                    }
                    #setting Response
                    $response_array                 = [
                        'CODE'                      => SUCCESS_CODE,
                        'MESSAGE'                   => 'success',
                        'RESULT'                    => $result,
                        'NEXT'                      => $this->next_count,
                        'TOTAL'                     => $total_count,
                        'NEXTLEADERS'               => $this->next_leaders_count,
                        'TOTALLEADERS'              => $total_leaders_count,
                        'NEXTDISTRICTLEADERS'       => $this->next_district_leaders_count,
                        'DISTRICTTOTALLEADERS'      => $district_total_leaders_count,
                        'TERM_CONDITION'            => $termConditionInfo,
                        'IS_CAMPAIGN_FORM_FILLED'   => $termConditionInfo['campaign_status'],
                        'CAMPAIGN_FORM_URL'         => base_url() . 'start-campaign?type=2',
                        'PAYMENT_DETAIL_ADDED'      => $paymentDetailAdded,
                        'IS_ID_PROOF_ADDED'         => $termConditionInfo['id_proff_added'],
                    ];
                } else {
                    #if user is blocked
                    #setting Response
                    $response_array                 = [
                        'CODE'                      => ACCOUNT_BLOCKED,
                        'MESSAGE'                   => $this->lang->line('account_blocked'),
                        'RESULT'                    => [],
                        'TERM_CONDITION'            => $termConditionInfo,
                        'IS_CAMPAIGN_FORM_FILLED'   => $termConditionInfo['campaign_status'],
                        'CAMPAIGN_FORM_URL'         => base_url() . 'start-campaign?type=2',
                    ];
                }
            }

            #sending Response
            // $this->response($response_array);
            echo json_encode($response_array);
            die;
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error              = $e->getMessage();

            #setting response
            $response_array     = [
                'CODE'          => TRY_AGAIN_CODE,
                'MESSAGE'       => $error,
                'RESULT'        => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }
}
