<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Chat extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model("Chat_model", "chat");
    }

    //Function for empty value or key
    public function getErrorMsg($required = array(), $request = array())
    {
        $notExist = true;
        foreach ($required as $value) {
            if (array_key_exists($value, $request)) {
                if ($request[$value] == "") {
                    $data = array(
                        "error_string" => "FAILED",
                        "error_string" => $value . " is empty.",
                        "error_code" => 201,
                    );
                    $this->response($data, 201);
                    exit;
                }
            } else {
                $data = array("error_string" => "FAILED", "error_string" => $value . " key is missing.", "error_code" => 201);
                $this->response($data, 201);
                exit;
            }
        }
        return $notExist;
    }

    public function fetchcategories_post()
    {
        $result = $this->chat->fetchcategories();
        $this->response(array(
            'CODE' => SUCCESS_CODE,
            'MESSAGE' => 'success',
            'RESULT' => $result
        ));
    }

    public function fetchsubcategories_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'category',
                    'label' => 'Category',
                    'rules' => 'trim|required',
                )
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $result = $this->chat->fetchsubcategories($postDataArr);
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $result
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function fetchQA_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'category',
                    'label' => 'Category',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'sub_category',
                    'label' => 'Sub Category',
                    'rules' => 'trim|required',
                )
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $result = $this->chat->fetchqa($postDataArr);
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $result
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function storeChatResponse_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'question_id',
                    'label' => 'question_id',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'answer',
                    'label' => 'answer',
                    'rules' => 'trim|required',
                )
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $userID = $GLOBALS['api_user_id'];
                $postDataArr['user_id'] = $userID;
                $result = $this->chat->storechatresponse($postDataArr);
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => (object) array()
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }
}
