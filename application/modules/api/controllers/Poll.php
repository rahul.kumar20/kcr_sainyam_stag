<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Poll extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model("Home_model", "home");
    }

    public function index_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);
            $params = $this->get();
            $count = isset($params['count']) ? $params['count'] : 0;
            $startDate = (isset($params['start_date']) && !empty($params['start_date']) ? date('Y-m-d', strtotime($params['start_date'])) : '');
            $where['limit'] = PAGE_LIMIT;
            $where['offset'] = (int) $count;
            $where['startDate'] = $startDate;
            $userId = $GLOBALS['api_user_id'];

            $this->next_count = $where['offset'] + $where['limit'];
            $wherecond = array('where' => array('user_id' => $userId));

            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data(
                'users',
                ['gender', 'user_id', 'state', 'district', 'registeration_no', 'unread_count', 'college', 'paytm_number', 'upi_address', 'account_number'],
                $wherecond,
                true
            );
            $userInfo['lang'] = $head['lang'];
            $all_data = $this->home->fetch_poll($where['limit'], $where['offset'], $where['startDate'], $userInfo);

            $pollResult = $all_data['pollResult'];
            $total_count = $all_data['totalPolls'];

            if ($total_count <= $this->next_count) {
                $this->next_count = '-1';
            }

            if (isset($pollResult) && !empty($pollResult)) {
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $pollResult,
                    'NEXT' => $this->next_count,
                    'TOTAL' => $total_count,
                ));
            } else {
                $this->response(array(
                    "CODE" => NO_DATA_FOUND,
                    'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'),
                    'RESULT' => array()
                ));
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function votepoll_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'poll_id',
                    'label' => 'Poll Id',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $postDataArr = $this->post();

                $userId = $GLOBALS['api_user_id'];
                $pollId = isset($postDataArr['poll_id']) ? ($postDataArr['poll_id']) : '';
                $postDataArr['user_id'] = $userId;
                $whereArr['where'] = ['user_id' => $userId];

                #fetching user details from db
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    [
                        'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                        'points_earned', 'total_earning', 'task_completed', 'is_bonus_received',
                    ],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    /*
                     * Check if user is not blocksed
                     */
                    if (ACTIVE == $userInfo['is_active']) {
                        $checkifvoted = $this->home->checkRecordId('ic_poll_answer', 'poll_id', $pollId, 'user_id', $userId);
                        if (true === $checkifvoted) {
                            $response_array = [
                                'CODE' => ALREADY_FOLLOWING,
                                'MESSAGE' => $this->lang->line('You have already taken this poll.')
                            ];
                        } else {
                            $result = $this->home->votePoll($postDataArr);
                            $this->response(array(
                                'CODE' => SUCCESS_CODE,
                                'MESSAGE' => 'success'
                            ));
                        }
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => (object) array(),
                        ];
                    }
                } else {
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => (object) array(),
                    ];

                    $this->response($response_array);
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }
}
