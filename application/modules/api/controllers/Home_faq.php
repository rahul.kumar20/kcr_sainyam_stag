<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Home_faq extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }

    public function index_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);

            $where['where'] = ['status' => ACTIVE];
            if (isset($head['lang']) && strtolower($head['lang']) == 'ta') {
                $contents = $this->Common_model->fetch_data('faq_management_home_screen', ['faq_title_ta as faq_title', 'faq_description_ta as faq_description'], $where, false);
            }else{
                $contents = $this->Common_model->fetch_data('faq_management_home_screen', ['faq_title', 'faq_description'], $where, false);
            }
          
            $response_array = [
                'CODE' => SUCCESS_CODE,
                'MESSAGE' => 'success',
                'RESULT' => $contents
            ];
            $this->response($response_array);
        } #try END
        catch (Exception $e) { #Catch Start
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE'   => TRY_AGAIN_CODE,
                'MESSAGE'    => $error,
                'RESULT' => [],
                'FAQS'   => ''
            ];
            $this->response($response_array);
        } #Catch End
    }
}
