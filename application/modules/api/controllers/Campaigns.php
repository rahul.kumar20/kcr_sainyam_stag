<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Campaigns extends REST_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
    }
    public function index_get()
    {
        try {
           
            $whereArr = [];
            $header = $this->head();
            $accessToken    =  $header['Accesstoken'];
            $keys = explode('||',$accessToken);
            $whereArr['where'] = array(
                    'public_key' => $keys[0],
                    'private_key' => $keys[1],
                );
            $userInfo = $this->Common_model->fetch_data('user_device_details','*',$whereArr,true);
            // print_r($userInfo);
            // die;
            if(empty($userInfo)){
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('error'),
                    'RESULT' => []
                ];
                $this->response($response_array);
            }else{
            $whereUser['where'] = array('user_id' => $userInfo['user_id']);
            $userDetails = $this->common_model->fetch_data('users','*',$whereUser,true);
        
            $table = 'campaigns';
            $order = 'sequence';
            $whereArr1['order_by'] = [$order => 'asc'];
            $whereArr1['where'] = ['status' => '1'];
            $listData = $this->Common_model->fetch_data($table, array("*"), $whereArr1);
                $listData1[] = $listData[0];
                $listData2[] = $listData[1];
            #If Result list is not empty
            if (!empty($listData)) {
                if(empty($userDetails['email_id']) || is_null($userDetails['email_id'])){
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData2,
                ];
                }else{
                $response_array = [
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => $listData1,
                    ];
                }
            }#if end
            else {
                $response_array = [
                    'CODE' => TRY_AGAIN_CODE,
                    'MESSAGE' => $this->lang->line('list_fetched'),
                    'RESULT' => []
                ];
            } #else End
            #sending response
        }
            $this->response($response_array);
        } catch (Exception $e) {
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('error'),
                'RESULT' => []
            ];
            $this->response($response_array);
        }
    }
}