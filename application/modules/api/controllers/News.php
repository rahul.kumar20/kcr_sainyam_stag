<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class News extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->model("News_model", "news");
    }

    /**
     * @SWG\Get(path="/News",
     *   tags={"News"},
     *   summary="Get app news secton",
     *   description="Get news title,discription and images",
     *   operationId="index_get",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="count",
     *     in="query",
     *     description="hit with count to get next page",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Success"),
     *   @SWG\Response(response=201, description="Please try again"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     * )
     */
    public function index_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);

            $params = $this->get();
            $count = isset($params['count']) ? $params['count'] : 0;
            $startDate = (isset($params['start_date']) && !empty($params['start_date']) ? date('Y-m-d', strtotime($params['start_date'])) : '');
            $news_id = (isset($params['news_id']) && !empty($params['news_id']) ? $params['news_id'] : '');
            $section = isset($params['section']) ? $params['section'] : '';
            $type = isset($params['type']) ? $params['type'] : '';
            $where['limit'] = PAGE_LIMIT;
            $where['offset'] = (int)$count;
            $where['startDate'] = $startDate;
            $where['lang'] = $head['lang'];
            $where['type'] = $type;
            $where['news_id'] = $news_id;
            $this->next_count = $where['offset'] + $where['limit'];

            $userId = $GLOBALS['api_user_id'];
            $wherecond = array('where' => array('user_id' => $userId));
            $userInfo = $this->Common_model->fetch_data(
                'users',
                ['gender', 'user_id', 'state', 'district', 'registeration_no', 'unread_count', 'college', 'paytm_number', 'upi_address', 'account_number'],
                $wherecond,
                true
            );

            $img_extension = array('png', 'jpg', 'jpeg', 'gif');
            $video_extension = array('mp4');
            # get priority news
            $priority_data = $this->news->fetch_news($where, $userInfo, HIGH_PRIORITY, $section);
            foreach ($priority_data['news'] as $newsKey => $newsVal) {
                $news_img = explode(",", $newsVal['news_image']);
                $media_thumb = explode(",", $newsVal['media_thumb']);
                $content_media_set = array();
                foreach ($news_img as $imKey => $imVal) {
                    if (!empty($imVal) && isset($imVal)) {
                        $ext = strtolower(pathinfo($news_img[$imKey], PATHINFO_EXTENSION));
                        $media_type = in_array($ext, $img_extension) ? '1' : (in_array($ext, $video_extension) ? '2' : '4');
                        $content_media_set[$imKey]['media_url'] = $news_img[$imKey];
                        $content_media_set[$imKey]['media_type'] = $media_type;
                        $content_media_set[$imKey]['media_thumb'] = isset($media_thumb[$imKey]) ? $media_thumb[$imKey] : '';
                    }
                }
                unset($priority_data['news'][$newsKey]['news_image']);
                unset($priority_data['news'][$newsKey]['media_thumb']);
                $priority_data['news'][$newsKey]['content_media_set'] = $content_media_set;

                $all_data['news'][$newsKey]['clicks_user_list'] = $this->news->getUserClicks($newsVal['news_id']);
                $all_data['news'][$newsKey]['clicks_user_count'] = $newsVal['total_likes'];
                $all_data['news'][$newsKey]['comments_count'] = $newsVal['total_comments'];
            }

            $all_data = $this->news->fetch_news($where, $userInfo, LOW_PRIORITY, $section);
            foreach ($all_data['news'] as $newsKey => $newsVal) {
                $news_img = explode(",", $newsVal['news_image']);
                $media_thumb = explode(",", $newsVal['media_thumb']);
                $content_media_set = array();
                foreach ($news_img as $imKey => $imVal) {
                    if (!empty($imVal) && isset($imVal)) {
                        $ext = strtolower(pathinfo($news_img[$imKey], PATHINFO_EXTENSION));
                        $media_type = in_array($ext, $img_extension) ? '1' : (in_array($ext, $video_extension) ? '2' : '4');
                        $content_media_set[$imKey]['media_url'] = $news_img[$imKey];
                        $content_media_set[$imKey]['media_type'] = $media_type;
                        $content_media_set[$imKey]['media_thumb'] = isset($media_thumb[$imKey]) ? $media_thumb[$imKey] : '';
                    }
                }

                unset($all_data['news'][$newsKey]['news_image']);
                unset($all_data['news'][$newsKey]['media_thumb']);
                $all_data['news'][$newsKey]['content_media_set'] = $content_media_set;

                $all_data['news'][$newsKey]['clicks_user_list'] = $this->news->getUserClicks($newsVal['news_id']);
                $all_data['news'][$newsKey]['clicks_user_count'] = $newsVal['total_likes'];
                $all_data['news'][$newsKey]['comments_count'] = $newsVal['total_comments'];
            }

            $news = $all_data['news'];
            $total_count = $all_data['totalcount'];

            if ($total_count <= $this->next_count) {
                $this->next_count = '-1';
            }

            if ($news_id) {
                $wherenewsCond = array('where' => array('user_id' => $userId, 'news_id' => $news_id, 'is_read' => 0));
                $newdata = array('is_read' => 1);
                #read notification
                $this->Common_model->update_single('user_notification', $newdata, $wherenewsCond);
            }
            //get term and condition detail
            $where = array('where' => array('status' => ACTIVE));
            $where['order_by'] = ['created_date' => 'desc'];

            // $termConditionInfo = $this->Common_model->fetch_data('ipac_term_condition', [
            //     'version',
            //     'get_term_condition_accept_status(' . $userId . ') as term_accept_status, get_is_user_added_id(' . $userId . ') as id_proff_added,
            //     get_campaign_form_status(' . $userId . ') as campaign_status'
            // ], $where, true);
            $termConditionInfo['version'] = '1';
            $termConditionInfo['term_accept_status'] = '0';
            $termConditionInfo['id_proff_added'] = '0';
            $termConditionInfo['campaign_status'] = '1';
            //check if user added payment detail
            $paymentDetailAdded = 0;
            if (!empty($userInfo['paytm_number']) || !empty($userInfo['upi_address']) || !empty($userInfo['account_number'])) {
                $paymentDetailAdded = 1;
            }
            if ((isset($news) && !empty($news)) || (isset($priority_data['news']) && !empty($priority_data['news'])) ) {
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $news,
                    'NEXT' => $this->next_count,
                    'TOTAL' => $total_count,
                    'PRIORITY_NEWS' => $priority_data['news'],
                    'UNREAD_COUNT' => $userInfo['unread_count'],
                    'TERM_CONDITION' => $termConditionInfo,
                    'IS_CAMPAIGN_FORM_FILLED' => $termConditionInfo['campaign_status'],
                    'CAMPAIGN_FORM_URL' => base_url() . 'start-campaign?type=2',
                    'IS_ID_PROOF_ADDED' => $termConditionInfo['id_proff_added'],
                    'PAYMENT_DETAIL_ADDED' => $paymentDetailAdded,
                ));
            } else {
                $this->response(array(
                    "CODE" => NO_DATA_FOUND,
                    'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'),
                    'RESULT' => array(),
                    'TERM_CONDITION' => $termConditionInfo,
                    'IS_CAMPAIGN_FORM_FILLED' => $termConditionInfo['campaign_status'],
                    'CAMPAIGN_FORM_URL' => base_url() . 'start-campaign?type=2',
                    'IS_ID_PROOF_ADDED' => $termConditionInfo['id_proff_added'],
                    'PAYMENT_DETAIL_ADDED' => $paymentDetailAdded,

                ));
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function likenews_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'news_id',
                    'label' => 'News ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'like_type',
                    'label' => 'Type',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $userID = $GLOBALS['api_user_id'];
                $newsID = $postDataArr['news_id'];
                $postDataArr['user_id'] = $userID;

                $checkLikes = $this->news->checkRecordId('ipac_news_likes', 'news_id', $newsID, 'user_id', $userID);
                if (true === $checkLikes) {
                    $this->news->likeNews($postDataArr, 'update');
                } else {
                    $this->news->likeNews($postDataArr, 'insert');
                }
                $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => 'success'));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('somthing_went_wrong'),
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

    public function dislikenews_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'news_id',
                    'label' => 'News ID',
                    'rules' => 'trim|required',
                )
            );

            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $userID = $GLOBALS['api_user_id'];
                $postDataArr['user_id'] = $userID;
                $this->news->likeNews($postDataArr, 'delete');
                $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => 'success'));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('somthing_went_wrong'),
                'RESULT' => [],
            ];

            $this->response($response_array);
        } #CATCH END
    }

    public function likecomment_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'comment_id',
                    'label' => 'Comment ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'like_type',
                    'label' => 'Type',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $userID = $GLOBALS['api_user_id'];
                $commentID = $postDataArr['comment_id'];
                $postDataArr['user_id'] = $userID;

                $checkLikes = $this->news->checkRecordId('ipac_news_comments_reaction', 'comment_id', $commentID, 'user_id', $userID);
                if (true === $checkLikes) {
                    $this->news->likeComment($postDataArr, 'update');
                } else {
                    $this->news->likeComment($postDataArr, 'insert');
                }
                $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => 'success'));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('somthing_went_wrong'),
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

    public function dislikecomment_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'comment_id',
                    'label' => 'Comment ID',
                    'rules' => 'trim|required',
                )
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {

                $userID = $GLOBALS['api_user_id'];
                $postDataArr['user_id'] = $userID;

                $this->news->likeComment($postDataArr, 'delete');
                $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => 'success'));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('somthing_went_wrong'),
                'RESULT' => [],
            ];

            $this->response($response_array);
        } #CATCH END
    }
    
    public function deletecomment_post()
    {
        try {      
            $params = $this->post();
            $id = $params['id'];
            $whereArr = [];
            $table = 'ipac_news_comments';
            $updateUserArray = array('is_deleted' => '1');
            $whereArr['where'] = ['id'=> $id];
            $this->Common_model->update_single($table, $updateUserArray, $whereArr);
            $this->response(array('CODE' => SUCCESS_CODE, 'MESSAGE' => 'success'));
            exit;
        } catch (Exception $e) {
            $this->db->trans_rollback();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('somthing_went_wrong'),
                'RESULT' => [],
            ];
            $this->response($response_array);
        } #CATCH END
    }

    public function sharenews_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'news_id',
                    'label' => 'News ID',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {

                $userID = $GLOBALS['api_user_id'];
                $postDataArr['user_id'] = $userID;
                $postDataArr['submitted_timestamp'] = date('Y-m-d H:i:s');
                $whereArr['where'] = ['user_id' => $userID];

                #fetching user details from db
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    [
                        'full_name', 'gender', 'state', 'district', 'registeration_no', 'user_image', 'is_active'
                    ],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    /*
                     * Check if user is not blocksed
                     */
                    if (ACTIVE == $userInfo['is_active']) {
                        $response['user_id'] = $userID;
                        $response['full_name'] = $userInfo['full_name'];
                        $response['registeration_no'] = $userInfo['registeration_no'];
                        $response['user_image'] = $userInfo['user_image'];
                        $response['package_name'] = isset($postDataArr['package_name']) ? $postDataArr['package_name'] : '';
                        $response['submitted_timestamp'] = $postDataArr['submitted_timestamp'];
                        $shareID = $this->news->shareNews($postDataArr);
                        $this->response(array(
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => 'success',
                            'RESULT' => (object) $response,
                        ));
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => (object) array(),
                        ];
                    }
                } else {
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => (object) array(),
                    ];
                    $this->response($response_array);
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function getPeerGroup_post()
    {
        $params = $this->post();
        $count = isset($params['count']) ? $params['count'] : 0;
        $limit = PAGE_LIMIT;
        $offset = (int)$count;

        $required_fields_arr = array(
            array(
                'field' => 'news_id',
                'label' => 'News Id',
                'rules' => 'trim|required',
            ),
        );

        $this->form_validation->set_rules($required_fields_arr);
        $this->form_validation->set_message('required', 'Please enter the %s');

        if ($this->form_validation->run()) {
            $next_count = $offset + $limit;
            $news_id = $params['news_id'];
            $results = $this->news->getAllUserClicks($news_id, $offset, $limit);
            $tasks = $results['data'];
            $total_count = $results['count'];

            if ($total_count <= $next_count) {
                $next_count = '-1';
            }
            $this->response(array(
                'CODE' => SUCCESS_CODE,
                'MESSAGE' => 'success',
                'RESULT' => $tasks,
                'NEXT' => $next_count,
                'TOTAL' => $total_count
            ));
        } else {
            $err = $this->form_validation->error_array();
            $arr = array_values($err);
            $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
        }
    }

    public function addcomment_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'news_id',
                    'label' => 'News ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'parent_id',
                    'label' => 'Parent ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'user_comment',
                    'label' => 'Comment',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {
                $userID = $GLOBALS['api_user_id'];
                $postDataArr['user_id'] = $userID;
                $postDataArr['submitted_timestamp'] = date('Y-m-d H:i:s');
                $whereArr['where'] = ['user_id' => $userID];

                #fetching user details from db
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    ['full_name', 'gender', 'state', 'district', 'registeration_no', 'user_image', 'is_profile_verified', 'is_active'],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    if (ACTIVE == $userInfo['is_active']) {
                        $response['user_id'] = $userID;
                        $response['full_name'] = $userInfo['full_name'];
                        $response['news_title'] = isset($postDataArr['news_title']) ? $postDataArr['news_title'] : '';
                        $response['registeration_no'] = $userInfo['registeration_no'];
                        $response['user_image'] = $userInfo['user_image'];
                        $response['is_profile_verified'] = $userInfo['is_profile_verified'];
                        $response['user_comment'] = $postDataArr['user_comment'];
                        $response['submitted_timestamp'] = $postDataArr['submitted_timestamp'];
                        $commentID = $this->news->addNewsComment($postDataArr);
                        $response['comment_id'] = $commentID;
                        $response['parent_id'] = isset($postDataArr['parent_id']) ? $postDataArr['parent_id'] : 0;
                        if ($response['parent_id'] != 0) {
                            $this->load->library('common_function');
                            $userErn = $this->news->commentUserErns($userID, $response['parent_id']);
                            if (count($userErn)) {
                                $token                      = array();
                                $androidTokens              = array();
                                $iosTokens                  = array();
                                foreach ($userErn as $user) {
                                    $token[]                =  $user['device_token'];
                                    if(strtolower($user['platform']) == 'android'){
                                        $androidTokens[]    = $user['device_token'];
                                    }else if(strtolower($user['platform']) == 'ios'){
                                        $iosTokens[]        = $user['device_token'];
                                    }
                                }
                                $payload = [
                                    "title" => $userInfo['full_name'] . ' உங்கள் கருத்திற்கு பதிலளித்துள்ளார்',
                                    "message" => $postDataArr['user_comment'],
                                    "body" => $response,
                                    "type" => 12,
                                    "image" => '',
                                    'news_id' => $postDataArr['news_id'],
                                    'task_id' => '',
                                    'notification_id' => ''
                                ];
                                if(count($androidTokens) > 0){
                                    $this->common_function->androidPush($androidTokens, $payload);
                                }
                                if(count($iosTokens) > 0){
                                    $this->common_function->iosPush($iosTokens, $payload);
                                }
                            }
                        }
                        $this->response(array(
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => 'success',
                            'RESULT' => (object) $response,
                        ));
                    } else {
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => (object) array(),
                        ];
                    }
                } else {
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => (object) array(),
                    ];
                    $this->response($response_array);
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function editcomment_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'news_id',
                    'label' => 'News ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'comment_id',
                    'label' => 'Comment ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'parent_id',
                    'label' => 'Parent ID',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'user_comment',
                    'label' => 'Comment',
                    'rules' => 'trim|required',
                ),
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {

                $userID = $GLOBALS['api_user_id'];
                $postDataArr['user_id'] = $userID;
                $postDataArr['submitted_timestamp'] = date('Y-m-d H:i:s');
                $whereArr['where'] = ['user_id' => $userID];

                #fetching user details from db
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    [
                        'full_name', 'gender', 'state', 'district', 'registeration_no', 'user_image', 'is_profile_verified', 'is_active'
                    ],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    if (ACTIVE == $userInfo['is_active']) {
                        $response['comment_id'] = $postDataArr['comment_id'];
                        $response['user_id'] = $userID;
                        $response['full_name'] = $userInfo['full_name'];
                        $response['registeration_no'] = $userInfo['registeration_no'];
                        $response['user_image'] = $userInfo['user_image'];
                        $response['is_profile_verified'] = $userInfo['is_profile_verified'];
                        $response['user_comment'] = $postDataArr['user_comment'];
                        $response['submitted_timestamp'] = $postDataArr['submitted_timestamp'];
                        $response['parent_id'] = isset($postDataArr['parent_id']) ? $postDataArr['parent_id'] : 0;
                        $this->news->editNewsComment($postDataArr);
                        $this->response(array(
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => 'success',
                            'RESULT' => (object) $response,
                        ));
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array = [
                            'CODE' => ACCOUNT_BLOCKED,
                            'MESSAGE' => $this->lang->line('account_blocked'),
                            'RESULT' => (object) array(),
                        ];
                    }
                } else {
                    $response_array = [
                        'CODE' => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('otp_mismatch_expired'),
                        'RESULT' => (object) array(),
                    ];
                    $this->response($response_array);
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function getallcomments_post()
    {
        $params = $this->post();
        $count = isset($params['count']) ? $params['count'] : 0;
        $limit = PAGE_LIMIT;
        $offset = (int)$count;

        $required_fields_arr = array(
            array(
                'field' => 'news_id',
                'label' => 'News Id',
                'rules' => 'trim|required',
            ),
        );
        #Setting Error Messages for rules
        $this->form_validation->set_rules($required_fields_arr);
        $this->form_validation->set_message('required', 'Please enter the %s');

        if ($this->form_validation->run()) {
            $news_id = $params['news_id'];
            $this->next_count = $offset + $limit;

            $userId = $GLOBALS['api_user_id'];
            $results = $this->news->getAllComments($userId, $news_id, $offset, $limit);
            $tasks = $results['data'];
            $total_count = $results['count'];

            if ($total_count <= $this->next_count) {
                $this->next_count = '-1';
            }
            $this->response(array(
                'CODE' => SUCCESS_CODE,
                'MESSAGE' => 'success',
                'RESULT' => $tasks,
                'NEXT' => $this->next_count,
                'TOTAL' => $total_count
            ));
        } else {
            $err = $this->form_validation->error_array();
            $arr = array_values($err);
            $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
        }
    }

    public function bookmarknews_post()
    {
        try {
            $postDataArr = $this->post();
            $required_fields_arr = array(
                array(
                    'field' => 'news_id',
                    'label' => 'News ID',
                    'rules' => 'trim|required',
                ),

            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', '%s key is missing.');

            if ($this->form_validation->run()) {

                $userID = $GLOBALS['api_user_id'];
                $newsID = $postDataArr['news_id'];
                $postDataArr['user_id'] = $userID;
                $checkLikes = $this->news->checkBookmarkRecordId('ipac_news_bookmarks', 'news_id', $newsID, 'user_id', $userID);
                if (true === $checkLikes) {
                    $this->news->bookmarkNews($postDataArr, 'delete');
                } else {
                    $this->news->bookmarkNews($postDataArr, 'insert');
                }
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success'
                ));
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();

            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $this->lang->line('somthing_went_wrong'),
                'RESULT' => [],
            ];

            $this->response($response_array);
        } #CATCH END
    }

    public function bookmarklisting_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);
            $params = $this->get();
            $count = isset($params['count']) ? $params['count'] : 0;
            $startDate = (isset($params['start_date']) && !empty($params['start_date']) ? date('Y-m-d', strtotime($params['start_date'])) : '');
            $where['limit'] = PAGE_LIMIT;
            $where['offset'] = (int)$count;
            $where['startDate'] = $startDate;
            $where['lang'] = $head['lang'];

            $this->next_count = $where['offset'] + $where['limit'];
            $userId = $GLOBALS['api_user_id'];

            $img_extension = array('png', 'jpg', 'jpeg', 'gif');
            $video_extension = array('mp4');
            $all_data = $this->news->fetch_bookmarked_news($where, $userId);
            foreach ($all_data['news'] as $newsKey => $newsVal) {
                $news_img = explode(",", $newsVal['news_image']);
                $media_thumb = explode(",", $newsVal['media_thumb']);
                $content_media_set = array();
                foreach ($news_img as $imKey => $imVal) {
                    if (!empty($imVal) && isset($imVal)) {
                        $ext = strtolower(pathinfo($news_img[$imKey], PATHINFO_EXTENSION));
                        $media_type = in_array($ext, $img_extension) ? '1' : (in_array($ext, $video_extension) ? '2' : '4');
                        $content_media_set[$imKey]['media_url'] = $news_img[$imKey];
                        $content_media_set[$imKey]['media_type'] = $media_type;
                        $content_media_set[$imKey]['media_thumb'] = isset($media_thumb[$imKey]) ? $media_thumb[$imKey] : '';
                    }
                }

                unset($all_data['news'][$newsKey]['news_image']);
                unset($all_data['news'][$newsKey]['media_thumb']);
                $all_data['news'][$newsKey]['content_media_set'] = $content_media_set;

                $all_data['news'][$newsKey]['clicks_user_list'] = $this->news->getUserClicks($newsVal['news_id']);
                $all_data['news'][$newsKey]['clicks_user_count'] = $newsVal['total_likes'];
                $all_data['news'][$newsKey]['comments_count'] = $newsVal['total_comments'];
            }

            $news = $all_data['news'];
            $total_count = $all_data['totalcount'];

            /**
             * check is data remaining or not
             */
            if ($total_count <= $this->next_count) {
                $this->next_count = '-1';
            }

            if (isset($news) && !empty($news)) {
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $news,
                    'NEXT' => $this->next_count,
                    'TOTAL' => $total_count
                ));
            } else {
                $this->response(array(
                    "CODE" => NO_DATA_FOUND,
                    'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'),
                    'RESULT' => array()
                ));
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

    public function archivelisting_get()
    {
        try {
            $head = $this->head();
            $head = array_change_key_case($head, CASE_LOWER);
            $params = $this->get();
            $count = isset($params['count']) ? $params['count'] : 0;
            $startDate = (isset($params['start_date']) && !empty($params['start_date']) ? date('Y-m-d', strtotime($params['start_date'])) : '');
            $where['limit'] = PAGE_LIMIT;
            $where['offset'] = (int)$count;
            $where['startDate'] = $startDate;
            $where['lang'] = $head['lang'];
            /**
             * next count
             */
            $this->next_count = $where['offset'] + $where['limit'];
            $userId = $GLOBALS['api_user_id'];
            $wherecond = array('where' => array('user_id' => $userId));

            #fetching user details from db
            $userInfo = $this->Common_model->fetch_data(
                'users',
                ['gender', 'user_id', 'state', 'district', 'registeration_no', 'unread_count', 'college', 'paytm_number', 'upi_address', 'account_number'],
                $wherecond,
                true
            );

            $img_extension = array('png', 'jpg', 'jpeg', 'gif');
            $video_extension = array('mp4');
            $all_data = $this->news->fetch_archive_news($where, $userInfo);
            foreach ($all_data['news'] as $newsKey => $newsVal) {
                $news_img = explode(",", $newsVal['news_image']);
                $media_thumb = explode(",", $newsVal['media_thumb']);
                $content_media_set = array();
                foreach ($news_img as $imKey => $imVal) {
                    if (!empty($imVal) && isset($imVal)) {
                        $ext = strtolower(pathinfo($news_img[$imKey], PATHINFO_EXTENSION));
                        $media_type = in_array($ext, $img_extension) ? '1' : (in_array($ext, $video_extension) ? '2' : '4');
                        $content_media_set[$imKey]['media_url'] = $news_img[$imKey];
                        $content_media_set[$imKey]['media_type'] = $media_type;
                        $content_media_set[$imKey]['media_thumb'] = isset($media_thumb[$imKey]) ? $media_thumb[$imKey] : '';
                    }
                }

                unset($all_data['news'][$newsKey]['news_image']);
                unset($all_data['news'][$newsKey]['media_thumb']);
                $all_data['news'][$newsKey]['content_media_set'] = $content_media_set;

                $all_data['news'][$newsKey]['clicks_user_list'] = $this->news->getUserClicks($newsVal['news_id']);
                $all_data['news'][$newsKey]['clicks_user_count'] = $newsVal['total_likes'];
                $all_data['news'][$newsKey]['comments_count'] = $newsVal['total_comments'];
            }

            $news = $all_data['news'];
            $total_count = $all_data['totalcount'];

            /**
             * check is data remaining or not
             */
            if ($total_count <= $this->next_count) {
                $this->next_count = '-1';
            }

            if (isset($news) && !empty($news)) {
                $this->response(array(
                    'CODE' => SUCCESS_CODE,
                    'MESSAGE' => 'success',
                    'RESULT' => $news,
                    'NEXT' => $this->next_count,
                    'TOTAL' => $total_count
                ));
            } else {
                $this->response(array(
                    "CODE" => NO_DATA_FOUND,
                    'MESSAGE' => $this->lang->line('NO_RECORD_FOUND'),
                    'RESULT' => array()
                ));
            }
        } #TRY END
        catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }
}
