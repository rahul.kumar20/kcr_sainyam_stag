<?php
 defined('BASEPATH') or exit('No direct script access allowed');
 require APPPATH.'/libraries/REST_Controller.php';
 require APPPATH . 'libraries/SNSPush.php';
 
class Logout extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
    }



    /**
      * @SWG\Put(path="/Logout",
      *   tags={"Login"},
      *   summary="Logout",
      *   description="Logout",
      *   operationId="index_put",
      *   consumes ={"multipart/form-data"},
      *   produces={"application/json"},
      *   @SWG\Parameter(
      *     name="Authorization",
      *     in="header",
      *     description="",
      *     required=true,
      *     type="string"
      *   ),
      *  @SWG\Parameter(
      *     name="accesstoken",
      *     in="header",
      *     description="Access token received during signup or login",
      *     required=true,
      *     type="string"
      *   ),
      *   @SWG\Response(response=200, description="Success"),
      *   @SWG\Response(response=206, description="Unauthorized request"),
      *   @SWG\Response(response=207, description="Header is missing"),
      *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
      * )
      */
    public function index_put()
    {
        try {
            $response_array = [];
             
            $accessToken    = $GLOBALS['login_user']['Accesstoken'];
            $accessTokenArr = explode("||", $accessToken);
            //logged user id
            $userId = $GLOBALS['api_user_id'];
            #if count is equal to 2
            if (2 === count($accessTokenArr)) {
                $whereArr          = [];
                $whereArr['where'] = ['user_id' => $userId,'public_key' => $accessTokenArr[0], 'private_key' => $accessTokenArr[1]];
                
                #Delete Session details from DB
                $deviceInfo  = $this->Common_model->fetch_data('user_device_details', array ( 'session_id','device_id','device_token','end_point_ern' ), $whereArr, true);

                if (!empty($deviceInfo['end_point_ern'])) {
                      //if user device token is subscribed to aws sns for notification
                       $sns = new Snspush();
                       //delete device end point ern from aws sns
                       $response = $sns->deleteDeviceEndPoint($deviceInfo['end_point_ern']);
                }
                $this->load->library('common_function');
                $this->common_function->writeDeviceDetailsLog('delete','api :: logout >> index_put',$whereArr);
                //delete user session
                $isSuccess = $this->Common_model->delete_data('user_device_details', $whereArr);
                //logs of user logout
                if (!empty($deviceInfo['device_id'])) {
                    $whereArr = array('where' => array('user_id' => $userId, 'device_id' => $deviceInfo['device_id'],'type'=> LOGIN));
                  //logs array
                    $logsInsArray = array(
                    'user_id' => $userId,
                    'deactive_date' => datetime(),
                    'type'=> LOGOUT
                    );
                      #updating logs details
                    $isSuccess = $this->Common_model->update_single('user_session_logs', $logsInsArray, $whereArr);
                }
                
                if ($isSuccess) { #if Deleted successfuly
                    #setting Response
                    $response_array = [
                        'CODE'   => SUCCESS_CODE,
                        'MESSAGE'    => $this->lang->line('logout_successful'),
                        'RESULT' => []
                    ];
                } #if END
                else {
                    #setting Response
                    $response_array = [
                        'CODE'   => TRY_AGAIN_CODE,
                        'MESSAGE'    => $this->lang->line('try_again'),
                        'RESULT' => []
                    ];
                }#else END
            } #IF END
            else {#Else IF access token is not in Valid form
                #setting Response
                $response_array = [
                    'CODE'   => PARAM_REQ,
                    'MESSAGE'    => $this->lang->line('try_again'),
                    'RESULT' => []
                ];
            }#else end
            #sending Response
            $this->response($response_array);
        } #try end
        catch (Exception $e) {#catch start
            $error = $e->getMessage();
            list($msg, $code) = explode(" || ", $error);

            #setting response
            $response_array = [
                'CODE'   => $code,
                'MESSAGE'    => $msg,
                'RESULT' => []
            ];

            #sending response
            $this->response($response_array);
        }#catch end
    }
}
