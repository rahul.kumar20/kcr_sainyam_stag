<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class User_uploads extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Common_model');
    }


    public function index_post()
    {
        try {
            $postDataArr                = $this->post();
            #setting Form  validation Rules
            
            $required_fields_arr        = array(
                array(
                    'field'             => 'media_url',
                    'label'             => 'Media',
                    'rules'             => 'required',
                ),
                array(
                    'field'             => 'remarks',
                    'label'             => 'Remarks',
                    'rules'             => 'required',
                )
            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            if ($this->form_validation->run()) {                
                $userId                 = $GLOBALS['api_user_id'];

                $whereArr['where']      = ['user_id' => $userId];
               
                #fetching user details from db
                $userInfo               = $this->Common_model->fetch_data(
                    'users',
                    [
                        'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                        'points_earned', 'total_earning', 'task_completed', 'is_bonus_received',
                    ],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    /*
                     * Check if user is not blocked
                     */
                    if (ACTIVE == $userInfo['is_active']) {

                        $this->db->trans_begin();

                        //insert in user user_uploads table
                        $insertUploads              = array(
                            'user_id'               => $userId,
                            'remarks'               => isset($postDataArr['remarks']) ? $postDataArr['remarks'] : '',
                            'created_date'          => date('Y-m-d H:i:s'),
                        );
                        
                        $isSuccess                  = $this->db->insert('user_uploads', $insertUploads);
                        $userUploadsId              = $this->db->insert_id();
                        

                        if($userUploadsId > 0){
                            $mediaURLArr            = explode(',', $postDataArr['media_url']);
                            
                            if(!empty($mediaURLArr)){
                                $mediaTypeArr       = array();
                                if($postDataArr['media_type'] != ''){
                                    $mediaTypeArr   = explode(',', $postDataArr['media_type']);
                                }

                                foreach ($mediaURLArr as $mKey => $mVal) {
                                    // insert in user_upload_media transaction table
                                    if($mVal != ''){
                                        $insertUploadsMedia[]   = array(
                                            'user_uploads_id'   => $userUploadsId,
                                            'media_url'         => $mVal,
                                            'media_type'        => isset($mediaTypeArr[$mKey]) ? $mediaTypeArr[$mKey] : '1'
                                        );
                                    }
                                }
                               
                                if (is_array($insertUploadsMedia) && count($insertUploadsMedia) > 0) {
                                    $isSuccess                  = $this->db->insert_batch('user_uploads_media', $insertUploadsMedia);
                                }
                            }
                        }
                            
                        #Throw Exception
                        if (!$isSuccess) {
                            $this->db->trans_rollback();
                            throw new Exception($this->lang->line('try_again'));
                        }

                        #if transaction runs successfully
                        if (true === $this->db->trans_status()) {
                            
                            #Comminting changes
                            $this->db->trans_commit();
                            #setting Response Array
                            $response_array         = [
                                'CODE'              => SUCCESS_CODE,
                                'MESSAGE'           => $this->lang->line('success'),
                                'RESULT'            => []
                            ];
                        } else {
                            #IF transaction failed
                            #rolling back
                            $this->db->trans_rollback();

                            #setting Response Array
                            $response_array         = [
                                'CODE'              => TRY_AGAIN_CODE,
                                'MESSAGE'           => $this->lang->line('try_again'),
                                'RESULT'            => [],
                            ];
                        }
                        $this->response($response_array);
                        
                    } else {
                        #if user is blocked
                        #setting Response
                        $response_array             = [
                            'CODE'                  => ACCOUNT_BLOCKED,
                            'MESSAGE'               => $this->lang->line('account_blocked'),
                            'RESULT'                => (object) array(),
                        ];
                    }
                } else {
                    $response_array                 = [
                        'CODE'                      => RECORD_NOT_EXISTS,
                        'MESSAGE'                   => $this->lang->line('user_does_not_exists'),
                        'RESULT'                    => (object) array(),
                    ];

                    $this->response($response_array);
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            #TRY END
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error              = $e->getMessage();

            #setting response
            $response_array     = [
                'CODE'          => TRY_AGAIN_CODE,
                'MESSAGE'       => $error,
                'RESULT'        => [],
            ];

            #sending Response
            $this->response($response_array);
        } #CATCH END
    }

}
