<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class User_campaign_expence extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Common_function');
    }

    /**
     * @SWG\Post(path="/campaign_expence",
     *   tags={"Wallet"},
     *   summary="Wallet",
     *   description="Add expense",
     *   operationId="add_expense_index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *     name="Accesstoken",
     *     in="header",
     *     description="Access token received during signup or login in header",
     *     required=true,
     *     type="string"
     *   ),
     *    @SWG\Parameter(
     *     name="campaign_date",
     *     in="formData",
     *     description="Campaign Date m-d-Y",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="campaign_title",
     *     in="formData",
     *     description="Campaign Title",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="task_id",
     *     in="formData",
     *     description="Task Id",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="expense_file",
     *     in="formData",
     *     description="Expense File in formate [{'media_url':'https://appinventiv-development.s3.amazonaws.com/moso%2FIMG_0001.JPG','amount':'50','particular':'test'}]",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Add Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     *   @SWG\Response(response=201, description="Try again something went wrong"),
     *   @SWG\Response(response=401, description="Access token unthorised"),
     * )
     */

    public function index_post()
    {

        try {
            $postDataArr = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'campaign_date',
                    'label' => 'Campaign Date',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'campaign_title',
                    'label' => 'Campaign Title',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'task_id',
                    'label' => 'Id of task',
                    'rules' => 'trim|numeric',
                ),
                array(
                    'field' => 'expense_file',
                    'label' => 'Expense File',
                    'rules' => 'trim|required',
                ),

            );
            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $userId = $GLOBALS['api_user_id'];
                $where = array('where' => array('user_id' => $userId));

                $userInfo = $this->Common_model->fetch_data('users', ['user_id', 'is_active', 'full_name', 'email_id', 'phone_number', 'registeration_no'], $where, true);
                $whereTask = array('where' => array('task_id' => $postDataArr['task_id']));

                $taskInfo = $this->Common_model->fetch_data('ipac_task_master', ['task_code'], $whereTask, true);
                #setting Default values to array KEYS
                $default = array(
                    "campaign_date" => "",
                    "campaign_title" => "",
                    "task_id" => "",
                );
                $campaignArr = defaultValue($postDataArr, $default);
                $login_insert = [];
                $expenseFile = (isset($postDataArr['expense_file']) &&
                    !empty($postDataArr['expense_file']) ? json_decode($postDataArr['expense_file'], true) : '');
                //validate json object
                if (!$expenseFile) {
                    // setting response
                    $response_array = [
                        'CODE' => PARAM_REQ,
                        'MESSAGE' => 'Invalid data',
                        'RESULT' => [],
                    ];
                } else {
                    # $campaignArr                    = [];
                    $campaignArr["user_id"] = $userId;
                    $campaignArr["expense_code"] = $this->generateExpenseId($taskInfo['task_code'], $userInfo['registeration_no']);

                    $campaignArr['task_id'] = (isset($postDataArr['task_id']) && !empty($postDataArr['task_id']) ? $postDataArr['task_id'] : 0);
                    $campaignArr["created_date"] = date('Y-m-d H:i:s');
                    $campaignArr["campaign_date"] = date('Y-m-d', strtotime($campaignArr['campaign_date']));
                    $this->db->trans_begin(); #DB transaction Start
                    $expenseId = $this->Common_model->insert_single('user_campaign_expences', $campaignArr); #save users details values in DB
                    if (!$expenseId) {
                        throw new Exception($this->lang->line('try_again'));
                    }
                    //expenses file block
                    if (!empty($expenseFile)) {
                        $i = 1;
                        $totalAmount = 0;
                        //foreach array
                        foreach ($expenseFile as $value) {
                            //array foe campaign expense file
                            $insertExpnseArr[$i] = array(
                                'expence_id' => $expenseId,
                                'particular' => $value['particular'],
                                'media_url' => $value['media_url'],
                                'amount' => $value['amount'],
                                'created_date' => date('Y-m-d H:i:s'),
                            );
                            //total amount for campaign
                            $totalAmount = $totalAmount + $value['amount'];
                            $i++;
                        }

                        /*Batch insert in campaign expense file */
                        $isSuccess = $this->Common_model->insert_batch('user_campaign_expences_media', array(), $insertExpnseArr);
                        #if not success
                        if (!$isSuccess) {
                            throw new Exception($this->lang->line('try_again'));
                        }
                        //update campaign expense total amount
                        $whereArr['where'] = ['expence_id' => $expenseId];
                        $updateCampaign = array(
                            'total_amount' => $totalAmount,
                        );
                        $isSuccess = $this->Common_model->update_single('user_campaign_expences', $updateCampaign, $whereArr); #save users details values in DB

                        #if not success
                        if (!$isSuccess) {
                            throw new Exception($this->lang->line('try_again'));
                        }
                        //Insert in wallet log history
                        $insertWalletLog = array(
                            'user_id' => $userId,
                            'point' => $totalAmount,
                            'title' => $this->lang->line('add_expense'),
                            'description' => '',
                            'task_id' => $expenseId,
                            'type' => ADD_EXPENSE,
                            'status' => PENDING,
                            'created_date' => date('Y-m-d H:i:s'),

                        );
                        //check if expense history alredy exist fo
                        $where['where'] = ['user_id' => $userId, 'task_id' => $expenseId, 'type' => ADD_EXPENSE];
                        $expensInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                        if (empty($expensInfo)) {
                            #save users details values in DB
                            $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                            #if not success
                            if (!$isSuccess) {
                                throw new Exception($this->lang->line('try_again'));
                            }
                        }
                    }

                    #Checking transaction status
                    if ($this->db->trans_status()) {
                        #comminting Transaction
                        $this->db->trans_commit();

                        if (!empty($expenseId)) { #if Start
                        #setting response
                        $response_array = [
                            'CODE' => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('expenses_success'),
                            'RESULT' => [],
                        ];
                        } #if End
                    } else {
                        $this->db->trans_rollback();

                        #setting response
                        $response_array = [
                            'CODE' => TRY_AGAIN_CODE,
                            'MESSAGE' => $this->lang->line('try_again'),
                            'RESULT' => (object) array(),
                        ];
                    }
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                #setting response
                $response_array = [
                    'CODE' => PARAM_REQ,
                    'MESSAGE' => $arr[0],
                    'RESULT' => (object) array(),
                ];
            }
            $this->response($response_array);
        } #TRY END
         catch (Exception $e) {
            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE' => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT' => (object) array(),
            ];

            #sending response
            $this->response($response_array);
        } #Catch End
    }

    /**
     * @function validate_dob
     * @description Custom Rule Validation For Date Of Birth
     *
     * @param date $dob user date of birth to check
     * @return boolean
     */
    private function validate_date($dob)
    {
        if (!(isValidDate($dob, 'm-d-Y'))) {
            #setting response
            $response_array = [
                'CODE' => PARAM_REQ,
                'MESSAGE' => $this->lang->line('invalid_dob'),
                'RESULT' => [],
            ];

            #sending Response
            $this->response($response_array);
        } else {
            return true;
        }
    }

/**
 * TASKID+USERID+TIMESTAMP
 */
    public function generateExpenseId($taskId, $userId)
    {
        $expenseCode = $taskId . '_' . $userId . '_' . time();
        return $expenseCode;
    }
}
