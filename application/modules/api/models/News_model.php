<?php

class News_model extends CI_Model
{

    public $totalmsg;
    public function __construct()
    {
        $this->load->database();
    }

    /**
     *
     * @param type $limit
     * @param type $offset
     * @return Array Get news section
     */
    public function fetch_news($where, $userInfo, $priority = '', $section)
    {
        
        $limit =  $where['limit'];
        $offset =  $where['offset'];
        $startDate = $where['startDate'];
        $getLanguage = $where['lang'];
        $lang = ($getLanguage == "ta") ? "_tn" : "";
        $news_id = $where['news_id'];
        $type = $where['type'];

        $district = '';
        if ($userInfo['district']) {
            $district = $userInfo['district'];
        }

        $gender = '';
        if ($userInfo['gender']) {
            $gender = $userInfo['gender'];
        }

        $this->db->select(
            'ipac_news.news_id,
             news_title' . $lang . ' AS news_title,
             news_description' . $lang . ' AS news_description,
             banner_height,
             ipac_news.created_date,
             url,
             home_section,
             news_category,            
             nl.like_type,
             GROUP_CONCAT(DISTINCT media_url) as news_image,
             GROUP_CONCAT(DISTINCT media_thumb) as media_thumb,
             0 AS is_completed,
             CASE WHEN nl.id IS NOT NULL THEN "like" ELSE "dislike" END AS `is_like`,    
             CASE WHEN nb.id IS NOT NULL THEN "yes" ELSE "no" END AS `is_bookmarked`,         
             ipac_news.share_count,
             (SELECT COUNT(*) FROM ipac_news_comments nc WHERE nc.news_id = ipac_news.news_id AND parent_id = 0 AND nc.is_deleted = 0) AS total_comments,
             (SELECT COUNT(*) FROM ipac_news_likes nlikes WHERE nlikes.news_id = ipac_news.news_id) AS total_likes',
            false
        );

        $this->db->from('ipac_news');
        $this->db->join('ipac_news_media', '(ipac_news_media.news_id = ipac_news.news_id)', 'left', false);
        $this->db->join('ipac_news_likes AS nl', 'nl.news_id = ipac_news.news_id AND nl.user_id = ' . $userInfo["user_id"], 'left', false);
        $this->db->join('ipac_news_bookmarks AS nb', 'nb.news_id = ipac_news.news_id AND nb.user_id = ' . $userInfo["user_id"], 'left', false);
        $this->db->group_by('ipac_news.news_id', false);
        $this->db->order_by('ipac_news.news_id DESC');
        if ($section != '') {
            $this->db->where("(home_section='$section')", null, false);
        } else {
            $this->db->where("(home_section= 'gallery' OR home_section='people_speaks' OR home_section='news' )", null, false);
        }

        if ($type != '') {
            $this->db->where("(news_category='$type')", null, false);
        }

        if (isset($startDate) && !empty($startDate)) {
            $this->db->where("DATE(ipac_news.created_date) >= '" . $startDate . "'  ");
        }

        if ($district) {
            $this->db->where("(district = 0 OR district = $district)", null, false);
        }

        if ($gender) {
            $this->db->where("(gender = 0 OR gender = $gender)", null, false);
        }

        if ($priority) {
            $this->db->where("(priority = $priority)", null, false);
        }

        if ($news_id) {
            $this->db->where("(ipac_news.news_id = $news_id)", null, false);
        }

       // $registeration_no = $userInfo['registeration_no'];
        // $this->db->where("(CASE WHEN ipac_news.registeration_no = '0' THEN registeration_no='0' ELSE FIND_IN_SET('" . $registeration_no . "', registeration_no)>0 END)", null, false);
        $this->db->where("(status = 1)", null, false);

        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        $err = $this->db->error();
        $total_news = 0;
        if ($priority == 2) {
            $total = $this->db->query('SELECT count(*) as total_news from ipac_news')->row_array();
        	$total_news = $total['total_news'];
        }
        if (isset($err['code']) && $err['code'] != 0) {
            // echo $err['message'];
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['news'] = $query->result_array();
            $res['totalcount'] =  $total_news;
         	return $res;
        }
    }

    function checkRecordId($tname, $field1, $val1, $field2, $val2)
    {
        $query = $this->db->get_where($tname, array($field1 => $val1, $field2 => $val2), 1, 0);
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    function checkBookmarkRecordId($tname, $field1, $val1, $field2, $val2)
    {
        $query = $this->db->get_where($tname, array($field1 => $val1, $field2 => $val2), 1, 0);
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    //Like News
    public function likeNews($params, $type)
    {
        $data = [
            'user_id' => $params['user_id'],
            'news_id' => $params['news_id'],
            'like_type' => isset($params['like_type']) ? $params['like_type'] : '',
            'submitted_timestamp' => date('Y-m-d H:i:s')
        ];
        if ($type == 'insert') {
            $this->db->insert('ipac_news_likes', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        } else if ($type == 'delete') {
            $this->db->where('user_id', $params['user_id']);
            $this->db->where('news_id', $params['news_id']);
            $update = $this->db->delete('ipac_news_likes');
            if ($update) {
                return true;
            } else {
                return false;
            }
        } else {
            $this->db->where('user_id', $params['user_id']);
            $this->db->where('news_id', $params['news_id']);
            $update = $this->db->update('ipac_news_likes', $data);
            if ($update) {
                return true;
            } else {
                return false;
            }
        }
    }

    //Like Comments
    public function likeComment($params, $type)
    {
        $data = [
            'user_id' => $params['user_id'],
            'comment_id' => $params['comment_id'],
            'like_type' => isset($params['like_type']) ? $params['like_type'] : '',
            'submitted_timestamp' => date('Y-m-d H:i:s')
        ];
        if ($type == 'insert') {
            $this->db->insert('ipac_news_comments_reaction', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        } else if ($type == 'delete') {
            $this->db->where('user_id', $params['user_id']);
            $this->db->where('comment_id', $params['comment_id']);
            $update = $this->db->delete('ipac_news_comments_reaction');
            if ($update) {
                return true;
            } else {
                return false;
            }
        } else {
            $this->db->where('user_id', $params['user_id']);
            $this->db->where('comment_id', $params['comment_id']);
            $update = $this->db->update('ipac_news_comments_reaction', $data);
            if ($update) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function shareNews($params)
    {
        $data = [
            'user_id' => $params['user_id'],
            'news_id' => $params['news_id'],
            'package_name' => isset($params['package_name']) ? $params['package_name'] : '',
            'submitted_timestamp' => $params['submitted_timestamp']
        ];
        $this->db->insert('ipac_news_shares', $data);
        if ($this->db->affected_rows() > 0) {
            $sqlQuery = $this->db->query("SELECT share_count from ipac_news where news_id='" . $params['news_id'] . "'");
            $share_count = $sqlQuery->row_array();
            $sqlQuery = $this->db->query("UPDATE ipac_news SET share_count=  '" . ++$share_count['share_count'] . "' where news_id = '" . $params['news_id'] . "'");
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function getUserClicks($news_id)
    {
        $sql = "SELECT 
        u.user_id, u.full_name, u.registeration_no, u.user_image, nl.like_type
        FROM `ipac_news_likes` `nl`, `users` `u`
        WHERE `nl`.`user_id`=`u`.`user_id` AND `nl`.`news_id` = " . $news_id . "
        ORDER BY `nl`.`submitted_timestamp` DESC
        LIMIT 3";

        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function getAllUserClicks($news_id, $offset, $limit)
    {
        $sql = "SELECT 
        u.user_id, u.full_name, u.registeration_no, u.user_image, nl.like_type
        FROM `ipac_news_likes` `nl`, `users` `u`
        WHERE `nl`.`user_id`=`u`.`user_id` AND `nl`.`news_id` = " . $news_id . "
        ORDER BY `nl`.`submitted_timestamp` DESC LIMIT $offset,$limit";

        $data = $this->db->query($sql);
        $result = $data->result_array();

        $sqlTotal = "SELECT COUNT(nl.id) AS total_count FROM `ipac_news_likes` `nl` WHERE `nl`.`news_id` = " . $news_id;
        $dataTotal = $this->db->query($sqlTotal);
        $resultTotal = $dataTotal->row_array();

        $resArr['data']  = $result;
        $resArr['count'] = $resultTotal['total_count'];
        return $resArr;
    }

    //Add New Comment on News
    public function addNewsComment($params)
    {
        $data = [
            'user_id'               => $params['user_id'],
            'news_id'               => $params['news_id'],
            'parent_id'             => isset($params['parent_id']) ? $params['parent_id'] : 0,
            'sub_parent_id'         => isset($params['sub_parent_id']) ? $params['sub_parent_id'] : 0,
            'user_comment'          => isset($params['user_comment']) ? $params['user_comment'] : '',
            'submitted_timestamp'   => $params['submitted_timestamp']
        ];
        $this->db->insert('ipac_news_comments', $data);
        if ($this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    //Edit Comment on News
    public function editNewsComment($params)
    {
        $data = [
            'user_comment' => isset($params['user_comment']) ? $params['user_comment'] : '',
            'submitted_timestamp' => $params['submitted_timestamp']
        ];
        $this->db->where('id', $params['comment_id']);
        $this->db->where('user_id', $params['user_id']);
        $this->db->where('news_id', $params['news_id']);
        $update = $this->db->update('ipac_news_comments', $data);
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserComments($user_id, $news_id)
    {
        $sqlTotal = "SELECT COUNT(nl.id) AS total_count FROM `ipac_news_comments` `nl` WHERE parent_id = 0 AND nl.is_deleted = 0 AND `nl`.`news_id` = " . $news_id;
        $dataTotal = $this->db->query($sqlTotal);
        $resultTotal = $dataTotal->row_array();
        $resArr['count'] = $resultTotal['total_count'];
        return $resArr;
    }

    //Get all comments by news ID
    public function getAllComments($user_id, $news_id, $offset, $limit)
    {
        $this->db->select(
            "u.user_id, 
            u.full_name,
            u.registeration_no, 
            u.user_image, 
            u.is_profile_verified, 
            nl.id AS comment_id, 
            nl.parent_id, 
            nl.user_comment, 
            nl.submitted_timestamp, 
            case when ncr.id IS NOT NULL then 'like' else 'dislike' end as `is_like`,
            ncr.like_type,
            (SELECT COUNT(*) FROM ipac_news_comments_reaction nlikes WHERE nlikes.comment_id = nl.id) AS total_likes",
            false
        );
        $this->db->from('ipac_news_comments AS nl');
        $this->db->join('users AS u', 'nl.user_id = u.user_id', 'inner', false);
        $this->db->join('ipac_news_comments_reaction AS ncr', 'ncr.comment_id = nl.id AND ncr.user_id =' . $user_id, 'left', false);
        $this->db->order_by('nl.submitted_timestamp DESC');
        $this->db->where("nl.news_id = $news_id", null, false);
        $this->db->where("nl.parent_id = 0", null, false);
        $this->db->where("nl.is_deleted = 0", null, false);
        $this->db->limit($limit, $offset);
        $data = $this->db->get();
        $result = $data->result_array();
        $final_result = array();
        if (count($result) > 0) {
            foreach ($result as $k => $v) {
                $final_result[$k] = $v;
                $final_result[$k]['comments'] = $this->getChildComment($v['comment_id'], $user_id);
            }
        }

        $sqlTotal = "SELECT COUNT(nl.id) AS total_count FROM `ipac_news_comments` `nl` WHERE parent_id = 0 AND nl.is_deleted = 0 AND `nl`.`news_id` = " . $news_id;
        $dataTotal = $this->db->query($sqlTotal);
        $resultTotal = $dataTotal->row_array();
        $resArr['data']  = $final_result;
        $resArr['count'] = $resultTotal['total_count'];
        return $resArr;
    }

    public function getChildComment($comment_id, $user_id)
    {
        $sql = "SELECT u.user_id, u.full_name, u.registeration_no, u.user_image, u.is_profile_verified, nl.id AS comment_id, nl.parent_id, nl.user_comment, nl.submitted_timestamp, case when ncr.id IS NOT NULL then 'like' else 'dislike' end as `is_like`,ncr.like_type
        FROM `ipac_news_comments` `nl`
        INNER JOIN `users` `u` ON `nl`.`user_id`=`u`.`user_id`
        LEFT JOIN `ipac_news_comments_reaction` AS ncr ON ncr.comment_id = nl.id AND ncr.user_id = $user_id
        WHERE `nl`.`user_id`=`u`.`user_id` AND `nl`.`parent_id` = $comment_id AND nl.is_deleted = 0  ORDER BY `nl`.`submitted_timestamp` ASC";

        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    // public function getCommentClicks($comment_id)
    // {
    //     $sqlTotal = "SELECT COUNT(nl.id) AS total_count FROM `ipac_news_comments_reaction` `nl` WHERE `nl`.`comment_id` = " . $comment_id;
    //     $dataTotal = $this->db->query($sqlTotal);
    //     $resultTotal = $dataTotal->row_array();
    //     return $resultTotal['total_count'];
    // }

    //Bookmark News
    public function bookmarkNews($params, $type)
    {
        $data = [
            'user_id' => $params['user_id'],
            'news_id' => $params['news_id'],
            'submitted_timestamp' => date('Y-m-d H:i:s')
        ];
        if ($type == 'insert') {
            $this->db->insert('ipac_news_bookmarks', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        } else if ($type == 'delete') {
            $this->db->where('user_id', $params['user_id']);
            $this->db->where('news_id', $params['news_id']);
            $update = $this->db->delete('ipac_news_bookmarks');
            if ($update) {
                return true;
            } else {
                return false;
            }
        } else {
            $this->db->where('user_id', $params['user_id']);
            $this->db->where('news_id', $params['news_id']);
            $update = $this->db->update('ipac_news_bookmarks', $data);
            if ($update) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     *
     * @param type $limit
     * @param type $offset
     * @return Array Get bookmark news section
     */
    public function fetch_bookmarked_news($where, $userId)
    {
        $limit =  $where['limit'];
        $offset =  $where['offset'];
        $startDate = $where['startDate'];
        $getLanguage = $where['lang'];
        $lang = ($getLanguage == "ta") ? "_tn" : "";

        $this->db->select(
            'ipac_news.news_id,
             news_title' . $lang . ' AS news_title,
             news_description' . $lang . ' AS news_description,
             banner_height,
             ipac_news.created_date,
             url,
             home_section,
             news_category,            
             nl.like_type,
             GROUP_CONCAT(DISTINCT media_url) as news_image,
             GROUP_CONCAT(DISTINCT media_thumb) as media_thumb,
             IF(ipac_news.form_id != 0,get_home_form_competed(' . $userId . ',form_id, ipac_news.news_id),"0") AS is_completed,
             CASE WHEN nl.id IS NOT NULL THEN "like" ELSE "dislike" END AS `is_like`,     
             CASE WHEN nb.id IS NOT NULL THEN "yes" ELSE "no" END AS `is_bookmarked`,        
             ipac_news.share_count,
             (SELECT COUNT(*) FROM ipac_news_comments nc WHERE nc.news_id = ipac_news.news_id AND parent_id = 0 AND nc.is_deleted = 0) AS total_comments,
             (SELECT COUNT(*) FROM ipac_news_likes nlikes WHERE nlikes.news_id = ipac_news.news_id) AS total_likes',
            false
        );

        $this->db->from('ipac_news');
        $this->db->join('ipac_news_media', '(ipac_news_media.news_id = ipac_news.news_id)', 'left', false);
        $this->db->join('ipac_news_likes AS nl', 'nl.news_id = ipac_news.news_id AND nl.user_id = ' . $userId, 'left', false);
        $this->db->join('ipac_news_bookmarks AS nb', 'nb.news_id = ipac_news.news_id', 'inner', false);
        $this->db->where("(nb.user_id = $userId)", null, false);
        $this->db->group_by('ipac_news.news_id', false);
        $this->db->order_by('ipac_news.news_id DESC');

        //filter start
        if (isset($startDate) && !empty($startDate)) {
            $this->db->where("DATE(ipac_news.created_date) >= '" . $startDate . "'  ");
        }

        // $this->db->limit($limit, $offset);
        $query = $this->db->get();
        $err = $this->db->error();
        $this->total = $this->db->query('SELECT count(*) as total_news from ipac_news')->row_array();
        $this->totalnews = $this->total['total_news'];

        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['news'] = $query->result_array();
            $res['totalcount'] = $this->totalnews;
            return $res;
        }
    }

    /**
     *
     * @param type $limit
     * @param type $offset
     * @return Array Get archive news section
     */
    public function fetch_archive_news($where, $userInfo)
    {
        $limit =  $where['limit'];
        $offset =  $where['offset'];
        $startDate = $where['startDate'];
        $getLanguage = $where['lang'];
        $lang = ($getLanguage == "ta") ? "_tn" : "";


        $district = '';
        if ($userInfo['district']) {
            $district = $userInfo['district'];
        }

        $gender = '';
        if ($userInfo['gender']) {
            $gender = $userInfo['gender'];
        }

        $registeration_no = $userInfo['registeration_no'];

        $this->db->select(
            'SQL_CALC_FOUND_ROWS ipac_news.news_id,
             news_title' . $lang . ' AS news_title,
             news_description' . $lang . ' AS news_description,
             banner_height,
             ipac_news.created_date,
             url,
             home_section,
             news_category,            
             nl.like_type,
             GROUP_CONCAT(DISTINCT media_url) as news_image,
             GROUP_CONCAT(DISTINCT media_thumb) as media_thumb,
             IF(ipac_news.form_id != 0,get_home_form_competed(' . $userInfo["user_id"] . ',form_id, ipac_news.news_id),"0") AS is_completed,
             CASE WHEN nl.id IS NOT NULL THEN "like" ELSE "dislike" END AS `is_like`,  
             CASE WHEN nb.id IS NOT NULL THEN "yes" ELSE "no" END AS `is_bookmarked`,            
             ipac_news.share_count,
             (SELECT COUNT(*) FROM ipac_news_comments nc WHERE nc.news_id = ipac_news.news_id AND parent_id = 0 AND nc.is_deleted = 0) AS total_comments,
             (SELECT COUNT(*) FROM ipac_news_likes nlikes WHERE nlikes.news_id = ipac_news.news_id) AS total_likes',
            false
        );

        $this->db->from('ipac_news');
        $this->db->join('ipac_news_media', '(ipac_news_media.news_id = ipac_news.news_id)', 'left', false);
        $this->db->join('ipac_news_likes AS nl', 'nl.news_id = ipac_news.news_id AND nl.user_id = ' . $userInfo["user_id"], 'left', false);
        $this->db->join('ipac_news_bookmarks AS nb', 'nb.news_id = ipac_news.news_id AND nb.user_id = ' . $userInfo["user_id"], 'left', false);
        $this->db->where("(archive = 1)", null, false);
        $this->db->group_by('ipac_news.news_id', false);
        $this->db->order_by('ipac_news.created_date DESC');

        //filter start
        if (isset($startDate) && !empty($startDate)) {
            $this->db->where("DATE(ipac_news.created_date) >= '" . $startDate . "'  ");
        }

        //filter for district
        if ($district) {
            $this->db->where("(district=0 OR district= $district)", null, false);
        }

        //filter for gender
        if ($gender) {
            $this->db->where("(gender=0 OR gender= $gender)", null, false);
        }

        $this->db->where("(CASE WHEN ipac_news.registeration_no = '0' THEN registeration_no='0' ELSE FIND_IN_SET('" . $registeration_no . "', registeration_no)>0 END)", null, false);
        $this->db->where("(status=1)", null, false);


        // $this->db->limit($limit, $offset);
        $query = $this->db->get();
        $err = $this->db->error();
        $this->totalnews = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['news'] = $query->result_array();
            $res['totalcount'] = $this->totalnews;
            return $res;
        }
    }

    public function commentUserErns($userID, $comment_id)
    {
        $this->db->select("ud.device_token,ud.platform", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id', 'inner');
        $this->db->join('ipac_news_comments as nc', 'nc.user_id=u.user_id', 'inner');
        $this->db->where('nc.id=', $comment_id);
        $this->db->where('nc.user_id!=', $userID);
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.device_token!=', '');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}
