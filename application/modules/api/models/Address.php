<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Address extends CI_Model {

    public function __construct ()
    {
        parent::__construct ();

    }



    public function __destruct ()
    {
        #close DB
        $this->db->close ();

    }



    /**
     *
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function get_address_book ( $user_id, $limit, $offset )
    {
        $resArr = [];
        $this->db->select ( ' SQL_CALC_FOUND_ROWS c.name country_name, s.name state_name, b.* ', false )
            ->from ( "address_book as b" )
            ->join ( "state_list as s ", "s.id = b.state_id", "LEFT" )
            ->join ( "country_list as c", "c.id = b.country_id", "LEFT" )
            ->where ( "b.user_id", $user_id )
            ->order_by ( "b.added_date", "DESC" )
            ->limit ( $limit, $offset );
        $query  = $this->db->get ();

        if ( $query->num_rows () )
        {
            $resArr['result']    = $query->result_array ();
            $resArr['totalrows'] = $this->db->query ( 'SELECT FOUND_ROWS() count' )->row ()->count;
        }
        return $resArr;

    }



    /**
     *
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function address_detail ( $user_id, $address_id )
    {
        $resArr = [];
        $this->db->select ( ' c.name country_name, s.name state_name, b.* ', false )
            ->from ( "address_book as b" )
            ->join ( "state_list as s ", "s.id = b.state_id", "LEFT" )
            ->join ( "country_list as c", "c.id = b.country_id", "LEFT" )
            ->where ( "b.user_id", $user_id )
            ->where ( "b.id", $address_id );
        $query  = $this->db->get ();

        if ( $query->num_rows () )
        {
            $resArr['result'] = $query->row_array ();
        }
        return $resArr;

    }



}
