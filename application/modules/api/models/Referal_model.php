<?php

class Referal_model extends CI_Model
{

    public $totalmsg;

    public function __construct()
    {
        $this->load->database();
    }


     /**
     *
     * @function getReferalUserList
     * @description get referal user list history
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getReferalUserList($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 
        u.user_id,
        u.full_name,
        u.email_id,
        u.registeration_no,
        u.user_image,
        ru.created_date,
        u.is_number_verified,
        u.phone_number', false);
        $this->db->from('ipac_referal_user ru');
        $this->db->join('users u', 'ru.user_id=u.user_id', 'inner');
        $this->db->where('ru.referal_user_id', $params['userId']);
        // $this->db->where('u.is_number_verified', '1');
    
        $this->db->order_by('ru.created_date', 'desc');
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['data']  = $query->result_array();
            $resArr['count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['data']  = array();
            $resArr['count'] = 0;
        }
   
        return $resArr;
    }
}
