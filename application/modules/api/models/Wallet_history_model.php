<?php

class Wallet_history_model extends CI_Model
{

    public $totalmsg;

    public function __construct()
    {
        $this->load->database();
    }


     /**
     *
     * @function getWalletHistory
     * @description get wallet log history
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getWalletHistory($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS wh.*', false);
        $this->db->from('ipac_user_wallet_history wh');

        $this->db->where('wh.user_id', $params['userId']);
        //from date
        if (! empty($params['start_date']) && ! empty($params['end_date'])) {
            $this->db->where('DATE(wh.created_date ) >= ', $params['start_date']);
            $this->db->where('DATE(wh.created_date) <= ', $params['end_date']);
        }
        //status filter
        if (isset($params['status']) && !empty($params['status'])) {
              $this->db->where('wh.status', $params['status']);
        }
        $this->db->order_by('wh.created_date', 'desc');
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['data']  = $query->result_array();
            $resArr['count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['data']  = array();
            $resArr['count'] = 0;
        }
   
        return $resArr;
    }

    /**
     *
     * @function getTaskList
     * @description get task list type offline
     * @param type $taskType
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getTaskList($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS tm.task_id,tm.task_title', false);
        $this->db->from('ipac_task_master tm');

        $this->db->where('tm.task_type', $params['taskType']);
       //search key
        if (isset($params['searchLike']) && !empty($params['searchLike'])) {
            $this->db->group_start();
            $this->db->like('tm.task_title', $params['searchLike']);
            $this->db->group_end();
        }
        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['data']  = $query->result_array();
            $resArr['count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['data']  = array();
            $resArr['count'] = 0;
        }
   
        return $resArr;
    }


    public function getWalletHistoryDetail($userID) {
        $this->db->select("IFNULL(Sum(Case When wh.`type` = 'expense' AND wh.`status`=1 Then point Else 0 End),0) expense,
        IFNULL(Sum(Case When wh.`type` != 'expense' AND wh.`type` != 'bonus' Then point Else 0 End),0) reward, IFNULL(Sum(Case When wh.`type` = 'bonus' Then point Else 0 End),0)as bonus");
        $this->db->from('ipac_user_wallet_history wh');

        $this->db->where('wh.user_id', $userID);
        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['data']  = $query->row_array();
        } else {
            $resArr['data']  = array();
        }
   
        return $resArr;
    }

    
}
