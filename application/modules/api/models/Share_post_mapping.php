<?php

class Share_post_mapping extends CI_Model {

    public function __construct() {
        parent::__construct();
        
         #load database 
        $this->load->database();
    }


    # Update post counter 

    public function update_post_counter ( $share_id, $column, $decrese_count = false )
    {

        switch ( $column )
        {
            case 'like':
                $set = ( $decrese_count != UNLIKE) ? 'like_count+1' : 'like_count-1';
                $this->db->set ( 'like_count', $set, FALSE );
                break;
            case 'comment':
                /* Here UNLIKE means delete */
                $set = ( $decrese_count != UNLIKE ) ? 'comment_count+1' : 'comment_count-1';
                $this->db->set ( 'comment_count', $set, FALSE );
                break;
            case 'share':
                /* Here UNLIKE means delete */
                $set = ( $decrese_count != UNLIKE ) ? 'share_count+1' : 'share_count-1';
                $this->db->set ( 'share_count', $set, FALSE );
                break;

            default:
                break;
        }

        $where = [ 'id' => $share_id ];
        
        /* In case of share, do not update update_date  */
        if ( $column != 'share' )
        {
            $this->db->set ( 'update_date', datetime () );
        }
        $this->db->where ( $where );

        return $result = $this->db->update ( 'share_post_mapping' );

    }


}
