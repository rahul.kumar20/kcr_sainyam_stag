<?php

class Home_model extends CI_Model
{

    public $totalmsg;
    public function __construct()
    {
        $this->load->database();
    }

    /**
     *
     * @param type $limit
     * @param type $offset
     * @return Array Get task section
     */
    public function fetch_task($limit, $offset, $startDate, $userInfo, $platform, $status)
    {
        $userId = $userInfo['user_id'];
        $currentDate = date('Y-m-d H:i:s');
        $todayDate = date('Y-m-d');
        $task_id = $userInfo['task_id'];
        // filter data start
        $state = '';
        if ($userInfo['state']) {
            $state = $userInfo['state'];
        }

        $district = '';
        if ($userInfo['district']) {
            $district = $userInfo['district'];
        }

        $ac = '';
        if ($userInfo['ac']) {
            $ac = $userInfo['ac'];
        }

        $gender = '';
        if ($userInfo['gender']) {
            $gender = $userInfo['gender'];
        }

        $college = '';
        if ($userInfo['college']) {
            $college = $userInfo['college'];
        }

        $registeration_no = $userInfo['registeration_no'];

        // filter data end
        $this->db->select(
            'SQL_CALC_FOUND_ROWS  ipac_task_master.*,'
                . 'GROUP_CONCAT(task_url) as task_image,GROUP_CONCAT(task_media_type) as task_media_type,'
                . 'GROUP_CONCAT(task_thumb) as task_thumb,IFNULL(status,"") as status,
                 IFNULL(get_task_status(' . $userId . ',ipac_task_master.task_id),0)  as task_completed,form_steps as total_survay_form,get_total_form_competed(' . $userId . ',form_id,ipac_task_master.task_id) total_survay_form_complete,(SELECT COUNT(user_task_master.detail_id) FROM user_task_master WHERE user_task_master.task_id = ipac_task_master.task_id ) AS
                 task_completed_user_count',
            false
        );

        $this->db->from('ipac_task_master');
        $this->db->join('ipac_task_media', '(ipac_task_master.task_id = ipac_task_media.task_id)', 'left', false);
        $this->db->join('user_task_master', '(ipac_task_master.task_id = user_task_master.task_id)', 'left', false);

        //filter start
        if (isset($startDate) && !empty($startDate)) {
            // $this->db->where("('" . $startDate . "' BETWEEN DATE(ipac_task_master.start_date) AND
            //  DATE(ipac_task_master.end_date) )");
            $this->db->where('ipac_task_master.start_date <= ', $currentDate);
            $this->db->where('ipac_task_master.end_date >= ', $currentDate);
        } else {
            $this->db->where('ipac_task_master.start_date <= ', $currentDate);
            $this->db->where('ipac_task_master.end_date >= ', $currentDate);
        }

        //filter for district
        if ($district) {
            $this->db->where("(district=0 OR district= $district)", null, false);
        }

        //filter for ac
        if ($ac) {
            $this->db->where("(ac=0 OR ac= $ac)", null, false);
        }

        //filter for gender
        if ($gender) {
            $this->db->where("(gender=0 OR gender= $gender)", null, false);
        }

        //filter for $platform ie twitter,facebook
        if ($platform) {
            $this->db->where("(task_type= '$platform')", null, false);
        }

        // filter for task_id
        if ($task_id) {
            $this->db->where("(ipac_task_master.task_id= $task_id)", null, false);
        }
        // filter for task status i.e complete or incomplete or in review

        if ($status) {
            if ((int) $status == 1) {
                $this->db->where("(get_task_status('.$userId.',ipac_task_master.task_id)= $status)", null, false);
                $this->db->where("(user_task_master.user_id= $userId)", null, false);
                $this->db->where("(DATE(user_task_master.added_on)= '" . $startDate . "')", null, false);
            } elseif ((int) $status == 3) {
                $this->db->where("(get_task_status('.$userId.',ipac_task_master.task_id)= 2)", null, false);
                $this->db->where("(user_task_master.user_id= $userId)", null, false);
                $this->db->where("(DATE(user_task_master.added_on)= '" . $startDate . "')", null, false);
            } else {
                $currentDate = date("Y-m-d");
                $this->db->where('ipac_task_master.task_id NOT IN (select task_id from user_task_master where user_task_master.user_id =' . $userId . ' and DATE(added_on)="' . $currentDate . '")', null, false);
                //$this->db->where("(user_task_master.user_id= $userId)", null, false);
            }
        }

        $registrationArr = array('0', $registeration_no);
        $this->db->where("(CASE WHEN ipac_task_master.registeration_no = '0' THEN registeration_no='0' ELSE FIND_IN_SET('" . $registeration_no . "', registeration_no)>0 END)", null, false);

        $this->db->where("(task_status=1)", null, false);
        $this->db->group_by('ipac_task_master.task_id', false);
        $this->db->order_by('ipac_task_master.task_id DESC');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        
        $err = $this->db->error();

        $this->totaltask = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['task'] = $query->result_array();
            $res['totalcount'] = $this->totaltask;
            return $res;
        }
    }

    /**
     * @param type $user_id
     * @return Array Get leader board section details
     */
    public function fetch_daily_progress($limit, $offset, $user_id, $start_date, $end_date, $param, $userInfo)
    {
        //filter by task platform
        if (!empty($param['taskPlatform']) && $param['taskPlatform'] == 'facebook') {
            //facebook platform
            $select = '0 completed_form_task,IFNULL(get_total_completed_task(DATE(selected_date),"facebook",' . $user_id . '),0)
            as completed_fb_task,0 completed_twitter_task,0 completed_whatsapp_task,0 completed_youtube_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"facebook",' . $user_id . '),0) as rewards';
        } elseif (!empty($param['taskPlatform']) && $param['taskPlatform'] == 'twitter') {
            //twitter platform
            $select = '0 completed_form_task,IFNULL(get_total_completed_task(DATE(selected_date),"twitter",' . $user_id . '),0)
            as completed_twitter_task,0 completed_fb_task,0 completed_whatsapp_task,0 completed_youtube_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"twitter",' . $user_id . '),0) as rewards';
        } elseif (!empty($param['taskPlatform']) && $param['taskPlatform'] == 'whatsapp') {
            //whtsapp platform
            $select = '0 completed_form_task,IFNULL(get_total_completed_task(DATE(selected_date),"whatsapp",' . $user_id . '),0)
            as completed_whatsapp_task,0 completed_twitter_task,0 completed_fb_task,0 completed_youtube_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"whatsapp",' . $user_id . '),0) as rewards';
        } elseif (!empty($param['taskPlatform']) && $param['taskPlatform'] == 'offline') {
            //offline platform
            $select = '0 completed_form_task,IFNULL(get_total_completed_task(DATE(selected_date),"offline",' . $user_id . '),0)
            as completed_offline_task,0 completed_whatsapp_task,0 completed_twitter_task,0 completed_fb_task,0 completed_youtube_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"offline",' . $user_id . '),0) as rewards';
        } elseif (!empty($param['taskPlatform']) && $param['taskPlatform'] == 'youtube') {
            //youtube platform
            $select = '0 completed_form_task,IFNULL(get_total_completed_task(DATE(selected_date),"youtube",' . $user_id . '),0)
            as completed_youtube_task,0 completed_offline_task,0 completed_whatsapp_task,0 completed_twitter_task,0 completed_fb_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"youtube",' . $user_id . '),0) as rewards';
        } elseif (!empty($param['taskPlatform']) && $param['taskPlatform'] == 'online') {
            //online platfoem
            $select = 'IFNULL(get_total_completed_task(DATE(selected_date),"online",' . $user_id . '),0)
            as completed_form_task,0 completed_youtube_task,0 completed_offline_task,0 completed_whatsapp_task,0 completed_twitter_task,0 completed_fb_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"online",' . $user_id . '),0) as rewards';
        } else {
            //else if no platform
            $select = 'IFNULL(get_total_completed_task(DATE(selected_date),"facebook",' . $user_id . '),0) as completed_fb_task,
            IFNULL(get_total_completed_task(DATE(selected_date),"twitter",' . $user_id . '),0) as completed_twitter_task,
            IFNULL(get_total_completed_task(DATE(selected_date),"whatsapp",' . $user_id . '),0) as completed_whatsapp_task,
            IFNULL(get_total_completed_task(DATE(selected_date),"offline",' . $user_id . '),0) as completed_offline_task,
            IFNULL(get_total_completed_task(DATE(selected_date),"youtube",' . $user_id . '),0) as completed_youtube_task,
            IFNULL(get_total_completed_task(DATE(selected_date),"online",' . $user_id . '),0) as completed_form_task,
            IFNULL(getRewardPointBytaskType(DATE(selected_date),"",' . $user_id . '),0) as rewards';
        }

        $selectQuery = 'select 
        IFNULL(get_total_completed_task(DATE(selected_date),"",' . $user_id . '),0) as completed_task, /* changed here "" to "online" */
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","",' . $user_id . ') as total_task, /* changed here "" to "online" */
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","facebook",' . $user_id . ') as total_fb_task,
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","twitter",' . $user_id . ') as total_twitter_task,
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","whatsapp",' . $user_id . ') as total_whatsapp_task,
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","offline",' . $user_id . ') as total_offline_task,
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","youtube",' . $user_id . ') as total_youtube_task,
        getTotalTaskByUser(DATE(selected_date),' . $userInfo['state'] . ',' . $userInfo['district'] . ',' . $userInfo['gender'] . ',"' . $userInfo['registeration_no'] . '","online",' . $user_id . ') as total_form_task,/* changed here "" to "online" */
        selected_date as added_on,
        ' . $select . ' from
        all_dates
        where selected_date between "' . $start_date . '" and "' . $end_date . '"
        group by selected_date DESC HAVING added_on >= "' . date("Y-m-d", strtotime($userInfo['registered_on'])) . '" LIMIT ' . $offset . ',' . $limit . ''; /* changed selected_date to added_on */

        //HAVING selected_date >= "'.date("Y-m-d", strtotime($userInfo['registered_on'])).'"
        $data = $this->db->query($selectQuery);

        $err = $this->db->error();
        $this->totaltask = 0;

        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['task'] = $data->result_array();
            $res['total_task'] = $this->totaltask;

            return $res;
        }
    }

    /**
     * @param type $start_date, $end_date
     * @return Array Get top 10 leaders of app
     */
    public function fetch_app_leaders_old($limit, $offset, $start_date, $end_date, $param)
    {

        $this->db->select(
            'SQL_CALC_FOUND_ROWS full_name, users.user_id, user_image, SUM(total_earned_points) as rewards, rating,
            0 as ranking',
            false
        );
        $this->db->from('user_earnings');
        $this->db->join('users', '(users.user_id = user_earnings.user_id)', 'inner', false);

        //task type filter
        if (!empty($param['taskPlatform'])) {
            $this->db->where("(type = '" . $param['taskPlatform'] . "')", null, false);
            // $this->db->join('ipac_task_master it', '(user_earnings.task_id = it.task_id AND it.task_type="' . $param['taskPlatform'] . '")', 'inner', false);
        }

        if (!empty($param['district'])) {
            $this->db->where("(district = '" . $param['district'] . "')", null, false);
        }

        //date filter
        // if ($start_date && $end_date) {
        //     $this->db->where('DATE(user_earnings.inserted_on ) >= ', $start_date);
        //     $this->db->where('DATE(user_earnings.inserted_on ) <= ', $end_date);
        // }
        $this->db->group_by('user_earnings.user_id', false);
        $this->db->order_by('sum(total_earned_points)', 'DESC');
        $this->db->limit(20);
        $query = $this->db->get();
        $err = $this->db->error();
        $this->totalleaders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['leaders'] = $query->result_array();
            $res['total_leaders'] = $this->totalleaders;
            return $res;
        }
    }

    /**
     * @param type $start_date, $end_date
     * @return Array Get top 10 leaders of app
     */
    public function fetch_app_leaders($limit, $offset, $start_date, $end_date, $param)
    {

        $this->db->select(
            ' full_name, users.user_id, user_image, points_earned as rewards, rating,
            0 as ranking',
            false
        );
        $this->db->from('users');

        if (!empty($param['district'])) {
            $this->db->where("(district = '" . $param['district'] . "')", null, false);
        }
        if (!empty($param['is_profile_edited'])) {
            $this->db->where("(is_profile_edited = '" . $param['is_profile_edited'] . "')", null, false);
        }

        $this->db->order_by('points_earned', 'DESC');
        $this->db->limit(20);
        $query = $this->db->get();
        $err = $this->db->error();
        $this->total = $this->db->query('SELECT count(*) as total_users from users')->row_array();
        $this->totalleaders = 0;

        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['leaders'] = $query->result_array();
            $res['total_leaders'] = $this->totalleaders;
            return $res;
        }
    }

    public function userAppRank($start_date, $end_date, $userInfo, $param, $userId)
    {
        $type = $param['taskPlatform'];
        $stateQuery = '1';

        $typeQuery = '1';
        if (!empty($type)) {
            $typeQuery = 't.type="' . $type . '"';
        }
        $sql = "SELECT rank as userRank
            FROM
            (
            SELECT user_id,
                    @r := IF(@c = points_earned, @r, @r+1) rank, @c := points_earned
                FROM
            (
               SELECT t.user_id,SUM(`total_earned_points`) as points_earned FROM `user_earnings` as t JOIN users as u
ON t.user_id = u.user_id WHERE  $stateQuery AND $typeQuery
                GROUP BY t.user_id
                ORDER BY points_earned DESC
            )  AS t
            CROSS JOIN
            (
                SELECT @r := 0,  @c := NULL
            ) AS i
            ) AS q WHERE q.user_id=$userId;";

        $data = $this->db->query($sql);
        $result = $data->row_array();
        $rank = !empty($result['userRank']) ? $result['userRank'] : 0;
        return $rank;
    }


    public function fetch_poll($limit, $offset, $startDate, $userInfo)
    {
        $getLanguage = $userInfo['lang'];
        $currentDate = date('Y-m-d');
        $currentTime = date("H:i:s", strtotime("POLL_HOUR"));
        if ($getLanguage == 'ta') {
            $getLanguage = 'hi';
        }
        $registeration_no = $userInfo['registeration_no'];

        $district = '';
        if ($userInfo['district']) {
            $district = $userInfo['district'];
        }

        $gender = '';
        if ($userInfo['gender']) {
            $gender = $userInfo['gender'];
        }

        $this->db->select("SQL_CALC_FOUND_ROWS *", false);
        $this->db->from('ic_poll');
        $this->db->order_by("created_on", "DESC");

        if (isset($startDate) && !empty($startDate)) {
            $this->db->where("DATE(created_on) >= '" . $startDate . "'  ");
        }

        if ($district) {
            $this->db->where("(poll_district=0 OR poll_district= $district)", null, false);
        }

        if ($gender) {
            $this->db->where("(gender=0 OR gender= $gender)", null, false);
        }
        $this->db->where('valid_till >= ', $currentDate);
        $this->db->where('valid_till_time >= ', $currentTime);
        $this->db->where("(CASE WHEN registeration_no = '0' THEN registeration_no='0' ELSE FIND_IN_SET('" . $registeration_no . "', registeration_no)>0 END)", null, false);
        $this->db->where("(status='active')", null, false);

        $pollQuery = $this->db->get();
        $err = $this->db->error();
        $totalPolls = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            if ($totalPolls > 0) {
                $pollArr = $pollQuery->result_array();
                foreach ($pollArr as $polls) {
                    $optionResult = [];
                    $optionQuery = $this->db->select('option_id,option_name_en,option_name_hi')
                        ->where('poll_id', $polls['poll_id'])
                        ->get('ic_poll_options');
                    if ($optionQuery->num_rows() > 0) {
                        $optionArr = $optionQuery->result_array();
                        foreach ($optionArr as $options) {
                            $countQuery = $this->db->query("SELECT COUNT('*') as result from ic_poll_answer where poll_id='" . $polls['poll_id'] . "' and option_id='" . $options['option_id'] . "'");
                            $countArrs = $countQuery->result_array();
                            $optionResult[] = ['option_id' => $options['option_id'], 'option_name' => (!empty($options['option_name_' . $getLanguage])) ? $options['option_name_' . $getLanguage] : $options['option_name_en'], 'vote_count' => (!empty($countArrs[0]['result'])) ? $countArrs[0]['result'] : '0'];
                        }
                    } else {
                        $optionResult = [];
                    }
                    $isvotedQuery = $this->db->select('poll_id')
                        ->where('poll_id', $polls['poll_id'])
                        ->where('user_id', $userInfo['user_id'])
                        ->get('ic_poll_answer');
                    if ($isvotedQuery->num_rows() > 0) {
                        $isvoted = 'yes';
                    } else {
                        $isvoted = 'no';
                    }
                    if (!empty($polls['poll_question_' . $getLanguage])) {
                        $poll_question = $polls['poll_question_' . $getLanguage];
                        $poll_heading = $polls['poll_heading_' . $getLanguage];
                    } else {
                        $poll_question = (!empty($polls['poll_question_en'])) ? $polls['poll_question_en'] : $polls['poll_question_hi'];
                        $poll_heading = (!empty($polls['poll_heading_en'])) ? $polls['poll_heading_en'] : $polls['poll_heading_hi'];
                    }

                    $pollResult[] = [
                        'type' => 'poll',
                        'poll_id' => $polls['poll_id'],
                        'poll_heading' => (!empty($poll_heading)) ? $poll_heading : '',
                        'poll_question' => (!empty($poll_question)) ? $poll_question : '',
                        'poll_type' => $polls['poll_type'],
                        'created_on' => $polls['created_on'],
                        'isvoted' => $isvoted,
                        'poll_option' => $optionResult,
                    ];
                }
            } else {
                $pollResult = [];
            }
            $res['pollResult'] = $pollResult;
            $res['totalPolls'] = $totalPolls;
            return $res;
        }
    }

    //Poll Voting
    public function votePoll($array)
    {
        $optionidArr = json_decode($array['option_id'], true);
        $optionnameArr = json_decode($array['option_name'], true);
        for ($i = 0; $i < count($optionidArr); $i++) {
            $data = [
                'poll_id' => $array['poll_id'],
                'user_id' => $array['user_id'],
                'option_id' => $optionidArr[$i],
                'option_name' => $optionnameArr[$i],
                'created_on' => date('Y-m-d H:i:s'),
            ];
            //   print_r($data); die;
            $this->db->insert('ic_poll_answer', $data);
        }
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function checkRecordId($tname, $field1, $val1, $field2, $val2)
    {
        $query = $this->db->get_where($tname, array($field1 => $val1, $field2 => $val2), 1, 0);
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    function checkBookmarkRecordId($tname, $field1, $val1, $field2, $val2)
    {
        $query = $this->db->get_where($tname, array($field1 => $val1, $field2 => $val2), 1, 0);
        if ($query->num_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getCompletedTaskUserList($user_id, $task_id)
    {
        $sql = "SELECT 
        u.user_id, u.full_name, u.registeration_no, u.user_image, u.phone_number
        FROM `user_task_master` `utm`, `ipac_referal_user` `ru`, `users` `u`
        WHERE `utm`.`user_id`=`u`.`user_id` 
        AND ((`ru`.`referal_user_id`=`utm`.`user_id` AND `ru`.`user_id` = " . $user_id . ") OR (`ru`.`user_id`=`utm`.`user_id` AND `ru`.`referal_user_id` = " . $user_id . "))
        AND `utm`.`task_id` = " . $task_id . "
        ORDER BY `utm`.`added_on` DESC
        LIMIT 2";

        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function getAllCompletedTaskUserList($user_id, $task_id, $offset, $limit)
    {
        $sql = "SELECT 
        u.user_id, u.full_name, u.registeration_no, u.user_image, u.phone_number, `utm`.`added_on`
        FROM `user_task_master` `utm`, `ipac_referal_user` `ru`, `users` `u`
        WHERE `utm`.`user_id`=`u`.`user_id` 
        AND ((`ru`.`referal_user_id`=`utm`.`user_id` AND `ru`.`user_id` = " . $user_id . ") OR (`ru`.`user_id`=`utm`.`user_id` AND `ru`.`referal_user_id` = " . $user_id . "))
        AND `utm`.`task_id` = " . $task_id . "
        ORDER BY `utm`.`added_on` DESC
        LIMIT $offset,$limit";

        $data = $this->db->query($sql);
        $result = $data->result_array();

        $totalResults = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        $resArr['data']  = $result;
        $resArr['count'] = $totalResults;
        return $resArr;
    }

    public function getUserRanking($userId, $start_date, $end_date, $task_platform)
    {
        $this->db->select('SUM(total_earned_points) AS total_rewards', false);
        $this->db->from('user_earnings');
        $this->db->where("(user_id = $userId)", null, false);
        if (!empty($start_date) && !empty($end_date)) {
            $this->db->where("DATE(inserted_on) >= '" . $start_date . "' AND DATE(inserted_on) <= '" . $end_date . "' ");
        }
        if (!empty($task_platform)) {
            $this->db->where("(type = '" . $task_platform . "')", null, false);
        }
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function getUserEarnings($userId)
    {
        $this->db->select('points_earned AS total_rewards', false);
        $this->db->from('users');
        $this->db->where("(user_id = $userId)", null, false);
        $query  = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
}
