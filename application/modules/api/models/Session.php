<?php

class Session extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        
         #load database
        $this->load->database();
    }




    /**
     * Get Post Owner Info
     * @param array
     * @param array
     * @return array
     */
    public function getPostownerInfo($field, $where)
    {
        $this->db->select($field, false);
        $this->db->from('user_device_details as a');
        $this->db->join('users as u', 'u.user_id=a.user_id', 'left');
        $this->db->join('post as p', 'a.user_id=p.user_id', 'left');
        $this->db->where($where);
        $this->db->order_by('a.session_id', 'DESC');

        $query = $this->db->get();

        return $query->row_array();
    }

    /**
     * Get Shared Post Owner Info
     * @param array
     * @param array
     * @return array
     */
    public function getSharedPostownerInfo($field, $where)
    {
        $this->db->select($field, false);
        $this->db->from('session as a');
        $this->db->join('users as u', 'u.user_id=a.user_id', 'left');
        $this->db->join('share_post_mapping as p', 'a.user_id=p.user_id', 'left');
        $this->db->where($where);
        $this->db->order_by('a.session_id', 'DESC');

        $query = $this->db->get();

        return $query->row_array();
    }

     /**
     * Get current user Info
     * @param string
     * @param array
     * @param array
     * @return array
     */
    public function getCurrentUserInfo($field, $where, $users_ids = null)
    {
        $this->db->select($field, false);
        $this->db->from('user_device_details as a');
        $this->db->join('users as u', 'u.user_id=a.user_id', 'inner');

        if (! empty($users_ids)) {
            $this->db->where('a.device_token IS NOT NULL');
            $this->db->where_in('u.user_id', $users_ids);
            $this->db->order_by('a.session_id', 'DESC');
        } else {
            $this->db->where($where);
        }
        $query = $this->db->get();
        
        if ($query->num_rows()) {
            return ( ! empty($users_ids)) ? $query->result_array() : $query->row_array();
        } else {
            return false;
        }
    }

     /**
     * Get user Info
     * @param string
     * @param array
     * @return array
     */
    public function getUserInfo($accessToken, $field = [ 'u.user_id' ])
    {
        $respArr        = [];
        $accessTokenArr = explode("||", $accessToken);
        if (2 != count($accessTokenArr)) {
            $respArr = array ( 'code' => INVALID_ACCESS_TOKEN, 'msg' => 'Invalid Access Token', 'result' => [] );
        }

        $whereArr = [];
        $whereArr = [ 'public_key' => $accessTokenArr[0], 'private_key' => $accessTokenArr[1], 'login_status' => 1 ];
        $userInfo = $this->getCurrentUserInfo($field, $whereArr);
        
        if (! empty($userInfo)) {
            $respArr = array ( 'code' => SUCCESS_CODE, 'userinfo' => $userInfo );
        } else {
            $respArr = array ( 'code' => ACCESS_TOKEN_EXPIRED, 'msg' => 'Access Token Expired', 'result' => [] );
        }
        return $respArr;
    }
}
