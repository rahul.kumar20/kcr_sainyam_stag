<?php

class Users extends CI_Model {

    public function __construct ()
    {
        parent::__construct ();

        #load database
        $this->load->database ();

    }



    /**
     * send notification
     * @param array
     * @return array
     */
    public function sendNotification ( $params )
    {
        $this->db->select ( 'first_name,u.user_id,s.device_token,s.platform' );
        $this->db->from ( 'users as u' );
        $this->db->join ( 'session as s', 'u.user_id=s.user_id', 'left' );
        if ( ! empty ( $params['platform'] ) && $params['platform'] != 1 )
        {
            $platform = $params['platform'] - 1;
            $this->db->where ( 's.platform', $platform );
        }
        if ( ! empty ( $params['gender'] ) )
        {
            $this->db->where ( 'u.gender', $params['gender'] );
        }

        $this->db->where ( 's.login_status = 1' );
        $query = $this->db->get ();

        return $query->result_array ();

    }



    /**
     * get users list
     * @param array
     * @return array
     */
    public function getUserList ( $params )
    {
        $userlist_type = isset ( $params['userlist_type'] ) ? $params['userlist_type'] : 1;
        if ( $userlist_type == 1 )
        {
            $sql = 'SQL_CALC_FOUND_ROWS CONCAT(first_name," ",last_name) as name,u.user_id,IF(f.status IS NULL,"",f.status) as status';
        }
        else if ( $userlist_type == 2 )
        {
            $sql = 'SQL_CALC_FOUND_ROWS CONCAT(first_name," ",last_name) as name,u.user_id,IF(f.id IS NULL,"",1) as status';
        }
        else if ( $userlist_type == 3 )
        {
            $sql = 'SQL_CALC_FOUND_ROWS CONCAT(first_name," ",last_name) as name,u.user_id,IF(f.id IS NULL,"",1) as status';
        }
        $this->db->select ( $sql, false );
        $this->db->from ( 'users as u' );
        if ( $userlist_type == 1 )
        {
            $this->db->join ( 'friend_request as f', '((f.sender_id=u.user_id AND f.receiver_id=' . $params['user_id'] . ') OR (f.sender_id=' . $params['user_id'] . ' AND f.receiver_id=u.user_id))',
                              'left', false );
        }
        else if ( $userlist_type == 2 )
        {
            $this->db->join ( 'follows as f', '(sender_id=' . $params['user_id'] . ' AND receiver_id = u.user_id)', 'left', false );
        }
        else if ( $userlist_type == 3 )
        {
            $this->db->join ( 'favorite as f', '(f.user_id=' . $params['user_id'] . ' AND favorited_userid = u.user_id)', 'left', false );
        }

        if ( ! empty ( $params['searchlike'] ) )
        {
            $this->db->like ( 'u.first_name', $params['searchlike'] );
        }
        $this->db->limit ( $params['limit'], $params['offset'] );
        $query = $this->db->get ();

        $respArr           = [];
        $respArr['result'] = $query->result_array ();
        $respArr['count']  = $this->db->query ( 'SELECT FOUND_ROWS() count;' )->row ()->count;
        return $respArr;

    }



    /**
     *
     * @param type $limit
     * @param type $offset
     * @param type $params
     * @return Array Getting user details
     */
    public function get_user_data ( $limit, $offset, $params )
    {
        $this->db->select ( 'SQL_CALC_FOUND_ROWS user_id, middle_name,first_name,last_name, image', FALSE )
            ->from ( 'users as u' );
        if ( ! empty ( $params['searchlike'] ) )
        {
            $this->db->like ( 'u.middle_name', $params['searchlike'] );
        }
        $this->db->limit ( $limit, $offset );
        $query          = $this->db->get ();
        $err            = $this->db->error ();
        $this->totalmsg = $this->db->query ( 'SELECT FOUND_ROWS() count;' )->row ()->count;
        if ( isset ( $err['code'] ) && $err['code'] != 0 )
        {
            throw new Exception ( $this->lang->line ( 'somthing_went_wrong' ) );
        }
        else
        {

            return $query->result_array ();
        }

    }



    /**
     * @name           get_users
     * @description    Get users
     * @param          array  $post_data post details array
     * @return         array   Post user name array
     */
    public function get_users ( $user_ids )
    {

        $this->db->select ( "CONCAT(first_name,' ',last_name) as username , user_id , image_thumb as pic", false );
        $this->db->from ( 'users' );
        $this->db->where ( 'status', ACTIVE );
        $this->db->where_in ( 'user_id', $user_ids );
        $this->db->group_by ( 'user_id' );
        $result = $this->db->get ();
        $status = $result->result_array ();
        return $status;

    }



}
