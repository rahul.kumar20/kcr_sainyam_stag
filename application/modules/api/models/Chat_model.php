<?php

class Chat_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function fetchcategories()
    {
        $categories         = array();
        $query              = $this->db->query("SELECT * FROM `ic_chatbot_content` group by category");
        if ($query->num_rows() > 0) {
            $categories     = $query->result_array();
        }
        return $categories;
    }

    public function fetchsubcategories($array)
    {
        $categories         = array();
        $query              = $this->db->query("SELECT * FROM `ic_chatbot_content` WHERE category='" . $array['category'] . "' group by sub_category");
        if ($query->num_rows() > 0) {
            $categories     = $query->result_array();
        }
        return $categories;
    }

    public function fetchqa($array)
    {
        $qa = array();
        $query = $this->db->query("SELECT * FROM `ic_chatbot_content` WHERE category='" . $array['category'] . "' AND sub_category='" . $array['sub_category'] . "'");
        if ($query->num_rows() > 0) {
            $qa = $query->result_array();
        }
        return $qa;
    }

    public function storechatresponse($array)
    {
        $user_id = $array['user_id'];
        $question_id = $array['question_id'];
        $answer = $array['answer'];
        $query_response = $this->db->get_where('ic_chat_response', array('question_id' => $question_id, 'user_id' => $user_id));
        if ($query_response->num_rows() > 0) {
            $data = array(
                'answer' => $answer,
                'answered_datetime' => date('Y-m-d H:i:s')
            );
            $this->db->set($data);
            $this->db->where('user_id', $user_id);
            $this->db->where('question_id', $question_id);
            $this->db->update('ic_chat_response');
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }
        $data = [
            'user_id' => $user_id,
            'question_id' => $question_id,
            'answer' => $answer,
            'answered_datetime' => date('Y-m-d H:i:s'),
        ];
        $this->db->insert('ic_chat_response', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
