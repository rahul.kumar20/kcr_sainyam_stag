<?php

class Notification_model extends CI_Model
{

    public $totalmsg;

    public function __construct()
    {
        $this->load->database();
    }

   
     /**
     * @function fetch_favourite_news
     * @Description fetch favourite news of user
      *
      * @param type $limit
      * @param type $offset
      * @return Array Get news section
      */
    public function fetch_user_notification($params,$userInfo,$start_date, $end_date)
    {  
         // filter data start
        $state = '';
        if ($userInfo['state']) {
            $state = $userInfo['state'];
        }
        
        $district = '';
        if ($userInfo['district']) {
            $district = $userInfo['district'];
        }
        
        $gender = '';
        if ($userInfo['gender']) {
            $gender = $userInfo['gender'];
        }
        
        $college = '';
        if ($userInfo['college']) {
            $college = $userInfo['college'];
        }
        
        $registeration_no  = $userInfo['registeration_no'];
        $userId = $params['user_id'];
        
        // filter data end
        $this->db->select(
            'SQL_CALC_FOUND_ROWS user_notification.*',
            false
        );
        $this->db->from('user_notification');
       
        //date filter
        if ($start_date && $end_date) {
            $this->db->where('DATE(user_notification.inserted_on ) >= ', $start_date);
            $this->db->where('DATE(user_notification.inserted_on ) <= ', $end_date);
        }

         $this->db->where("user_notification.user_id=$userId");
         
        $this->db->order_by('user_notification.inserted_on DESC');

        // $this->db->limit($params['limit'], $params['offset']);
        $query          = $this->db->get();
        
        $err            = $this->db->error();
        $this->totalnews = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['notification']       = $query->result_array();
            $res['totalcount'] = $this->totalnews;
            return $res;
        }
    }
      
    
}
