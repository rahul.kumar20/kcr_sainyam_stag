<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_logs_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name userlist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function userlist($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "ul.active_date",
            "deactive" => "ul.deactive_date",
            "reward_point" => "u.points_earned"
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS ul.*,u.registeration_no,u.full_name,sl.state_name,d.district_name,cl.college_name", false);
        $this->db->from('user_session_logs ul');  
        $this->db->join('users as u', 'ul.user_id=u.user_id' , 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("ul.active_date", "DESC");
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
         //UID filter
        if (! empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
          //state filter
        if (! empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
          //district filter
        if (! empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
           //Gender filter
        if (! empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
            //college filter
        if (! empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(registered_on) >= '" . $startDate . "' AND DATE(registered_on) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
        $res['result'] = $query->result_array();
        $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;

        return $res;
    }
}
