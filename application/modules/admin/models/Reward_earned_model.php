<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reward_earned_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name userlist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function userlist($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "u.user_id",
            "task_completed" => "u.task_completed",
            "reward_point" => "u.points_earned",
            "total_earning" => "u.total_earning"

        ];

        $this->db->select("SQL_CALC_FOUND_ROWS u.*,sl.state_name,d.district_name,cl.college_name", false);
        $this->db->from('users as u');
        $this->db->join('ipac_user_wallet_history as ru', 'u.user_id = ru.user_id', 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("u.registered_on", "DESC");
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
         //UID filter
        if (! empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
          //state filter
        if (! empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
          //district filter
        if (! empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
           //Gender filter
        if (! empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
            //college filter
        if (! empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
            //college filter
        if (! empty($params['paymentMethod'])) {
            $this->db->where('default_payment_option', $params['paymentMethod']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(registered_on) >= '" . $startDate . "' AND DATE(registered_on) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->limit($params['limit'], $params['offset']);
        $this->db->group_by("ru.user_id");
        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
       

        return $res;
    }

    /**
     *
     * @function getWalletHistory
     * @description get wallet log history
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getWalletHistory($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS wh.*,im.task_id,im.task_title,im.task_type,im.start_date,im.end_date,im.action,im.task_code', false);
        $this->db->from('ipac_user_wallet_history wh');
        $this->db->join('ipac_task_master im', 'im.task_id=wh.task_id AND type !="expense" AND type!="bonus"', 'left');

        $this->db->where('wh.user_id', $params['id']);
       //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('title', $params['searchlike']);
            $this->db->group_end();
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('wh.status', $params['status']);
        }
        //tasktype
        if (! empty($params['taskType'])) {
            $this->db->where('wh.type', $params['taskType']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_date) >= '" . $startDate . "' AND DATE(created_date) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->order_by('wh.created_date', 'desc');
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['result']  = $query->result_array();
            $resArr['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['result']  = array();
            $resArr['total'] = 0;
        }
   
        return $resArr;
    }
    
       /**
     *
     * @function getWalletHistory
     * @description get wallet log history
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getPaymentHistory($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS wh.*,u.registeration_no', false);
        $this->db->from('ipac_payment_history wh');
        $this->db->join('users u', 'wh.user_id=u.user_id', 'inner');
        $this->db->where('wh.user_id', $params['id']);
       //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('title', $params['searchlike']);
            $this->db->group_end();
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('wh.status', $params['status']);
        }
        //tasktype
        if (! empty($params['taskType'])) {
            $this->db->where('wh.payment_option', $params['taskType']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(paid_date) >= '" . $startDate . "' AND DATE(paid_date) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->order_by('wh.created_date', 'desc');
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['result']  = $query->result_array();
            $resArr['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['result']  = array();
            $resArr['total'] = 0;
        }
        return $resArr;
    }

      /**
     *
     * @function getWalletHistory
     * @description get wallet log history
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getPaymentHistoryGroup($params)
    {

        $sortMap = [
            "created_date" => "created_date",
            "total_user" => "total_paid_user",
            "total_amount" => "total_amount_paid",

        ];

        $this->db->select('SQL_CALC_FOUND_ROWS id,payment_id,count(id) as total_paid_user,sum(amount_paid) as total_amount_paid,created_date', false);
        $this->db->from('ipac_payment_history wh');
       //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('payment_id', $params['searchlike']);
            $this->db->group_end();
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('wh.status', $params['status']);
        }
      
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(paid_date) >= '" . $startDate . "' AND DATE(paid_date) <= '" . $endDate . "' ");
        }
      //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
        (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
        } else {
            $this->db->order_by('wh.created_date', 'desc');
        }
        $this->db->group_by('payment_id');
        $this->db->limit($params['limit'], $params['offset']);

        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['result']  = $query->result_array();
            $resArr['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['result']  = array();
            $resArr['total'] = 0;
        }
        return $resArr;
    }


     /**
     *
     * @function getWalletHistory
     * @description get wallet log history
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @return boolean | array
     */
    public function getPaymentHistoryDetail($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS wh.*,u.registeration_no', false);
        $this->db->from('ipac_payment_history wh');
        $this->db->join('users u', 'wh.user_id=u.user_id', 'inner');
        $this->db->where('wh.payment_id', $params['id']);
       //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('wh.status', $params['status']);
        }
        //tasktype
        if (! empty($params['taskType'])) {
            $this->db->where('wh.payment_option', $params['taskType']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(paid_date) >= '" . $startDate . "' AND DATE(paid_date) <= '" . $endDate . "' ");
        }
       
        $this->db->order_by('wh.created_date', 'desc');
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        //if data is greater than 0
        if ($query !== false && $query->num_rows() > 0) {
            $resArr['result']  = $query->result_array();
            $resArr['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $resArr['result']  = array();
            $resArr['total'] = 0;
        }
        return $resArr;
    }
}
