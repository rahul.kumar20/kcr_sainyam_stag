<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Poll_model extends CI_Model
{
    function createPollprocess()
    {
        $gender = !empty($this->input->post('gender')) ? $this->input->post('gender') : 0;
        $district = !empty($this->input->post('distict')) ? $this->input->post('distict') : 0;
        $registeration_no = !empty($this->input->post('uid')) ? $this->input->post('uid') : 0;

        $pollData = [
            'poll_heading_en' => $_POST['poll_heading_en'],
            'poll_heading_hi' => $_POST['poll_heading_hi'],
            'poll_question_en' => $_POST['poll_question_en'],
            'poll_question_hi' => $_POST['poll_question_hi'],
            'poll_type' => $_POST['poll_type'],
            'registeration_no' => $registeration_no,
            'poll_district' => $district,
            'gender' => $gender,
            'created_on' => date('Y-m-d H:i:s'),
            'valid_till' => $_POST['valid_till'],
            'valid_till_time' => $_POST['valid_till_time']
        ];

        $this->db->insert('ic_poll', $pollData);
        $pollid = $this->db->insert_id();

        if ($this->db->affected_rows() > 0) {
            if ($_POST['poll_type'] == 'textarea') {
                $data = ['poll_id' => $pollid, 'option_name_en' => 'Answer', 'option_name_hi' => 'பதில்'];
                $this->db->insert('ic_poll_options', $data);
            } elseif ($_POST['poll_type'] == 'linear') {
                $optionArr = [1, 2, 3, 4, 5];
                foreach ($optionArr as $option) {
                    $data = ['poll_id' => $pollid, 'option_name_en' => $option, 'option_name_hi' => $option];
                    $this->db->insert('ic_poll_options', $data);
                }
            } else {
                $labelArrEn = $_POST['poll_option_en'];
                $labelArrHi = $_POST['poll_option_hi'];
                for ($i = 0; $i < count($labelArrEn); $i++) {
                    $data = [
                        'poll_id' => $pollid,
                        'option_name_en' => $labelArrEn[$i],
                        'option_name_hi' => $labelArrHi[$i]
                    ];
                    $this->db->insert('ic_poll_options', $data);
                }
            }
        }
        $this->sendPushMessage($pollid, $pollData);
        header('location: ' . $this->config->item('base_url') . '/admin/poll');
    }

    function sendPushMessage($pollid, $params)
    {
        //sns object
        $sns = new Snspush();
        $this->db->select("ud.end_point_ern as arn_endpoint,ud.user_id", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id');
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.end_point_ern!=', '');

        if (!empty($params['registeration_no'])) {
            $this->db->where("FIND_IN_SET('" . $params["registeration_no"] . "', registeration_no)>0");
        }

        if (!empty($params['poll_district'])) {
            $this->db->where('u.district', $params['poll_district']);
        }

        if (!empty($params['gender'])) {
            $this->db->where('u.gender', $params['gender']);
        }

        $query = $this->db->get();

        $userErn = $query->result_array();
        #Comminting changes

        $message = [
            "default" => $params['poll_heading_en'] . ' ' . $this->lang->line('task_add_notification'),
            "APNS_SANDBOX" => json_encode([
                "aps" => [
                    "alert" => array(
                        "title" => $params['poll_heading_en'] . ' ' . $this->lang->line('task_add_notification'),
                        "body" => $params['poll_heading_en'] . ' ' . $this->lang->line('task_add_notification')
                    ),
                    "sound" => "default",
                    "mutable-content" => 1,
                    "badge" => 1,
                    "data" => array(
                        'notification_id' => '',
                        'poll_id' => $pollid,
                        'news_id' => '',
                        'task_id' => '',
                        "content_type" => "survey",
                        "type" => 2
                    )
                ]
            ]),
            "GCM" => json_encode([
                "data" => [
                    "title" => $params['poll_heading_en'] . ' ' . $this->lang->line('task_add_notification'),
                    "message" => $params['poll_heading_en'] . ' ' . $this->lang->line('task_add_notification'),
                    "body" => $params['poll_heading_en'] . ' ' . $this->lang->line('task_add_notification'),
                    "type" => 2,
                    "image" => '',
                    'poll_id' => $pollid,
                    'news_id' => '',
                    'task_id' => '',
                    'notification_id' => '',

                ]
            ])
        ];
        //send notification
        if (!empty($userErn)) {
            $sendNotifiUser = [];
            $in = 0;
            foreach ($userErn as $users) {
                $insertNewsNoti[$in] = array(
                    'user_id' => $users['user_id'],
                    'news_id' => 0,
                    'task_id' => 0,
                    'poll_id' => $pollid,
                    'notification_type' => 'add_poll',
                    'notification_text' => $params['poll_heading_en'],
                    'inserted_on' => date('Y-m-d H:i:s')
                );
                $in++;
                $sendNotifiUser = $insertNewsNoti;
            }
            $temp = array_unique(array_column($sendNotifiUser, 'user_id'));
            $unique_arr = array_intersect_key($sendNotifiUser, $temp);
            $this->insert_batch('user_notification', array(), $unique_arr, '');

            //send message to filtered users or selected users
            $result = $sns->asyncPublish2($userErn, $message);
        }
    }

    public function insert_batch($table, $defaultArray, $dynamicArray = array(), $updatedTime = false)
    {
        //Check if default array has values
        if (count($dynamicArray) < 1) {
            return false;
        }

        //If updatedTime is true
        if ($updatedTime) {
            $defaultArray['UpdatedTime'] = time();
        }

        //Iterate it
        foreach ($dynamicArray as $val) {
            $updates[] = array_merge($defaultArray, $val);
        }
        return $this->db->insert_batch($table, $updates);
    }

    public function pollsList($params)
    {
        $this->db->select("SQL_CALC_FOUND_ROWS n.*", false);
        $this->db->from('ic_poll as n');

        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('poll_question_en', $params['searchlike']);
            $this->db->group_end();
        }
        $this->db->order_by("n.created_on", "DESC");

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where("FIND_IN_SET('" . $params["uid"] . "', registeration_no)>0");
        }

        //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }

        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_on) >= '" . $startDate . "' AND DATE(created_on) <= '" . $endDate . "' ");
        }

        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();

        if ($query !== false && $query->num_rows() > 0) {
            $result = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
            foreach ($result as $value) {
                $optionQuery = $this->db->select('option_name_en,option_name_hi')
                    ->where('poll_id', $value['poll_id'])
                    ->get('ic_poll_options');
                $optionArr = $optionQuery->result_array();
                $dataArr[] = [
                    'poll_id' => $value['poll_id'],
                    'poll_question' => (!empty($value['poll_question_en'])) ? $value['poll_question_en'] : $value['poll_question_hi'],
                    'status' => (!empty($value['status'])) ? $value['status'] : '',
                    'valid_till' => (!empty($value['valid_till'])) ? $value['valid_till'] : '',
                    'optionArr' => $optionArr
                ];
            }

            $res['result'] = $dataArr;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

    public function total_pollcount()
    {
        $query = $this->db->query("SELECT COUNT(poll_id) as polls from ic_poll");
        $userArrs = $query->result_array();
        $totalcount = $userArrs[0]['polls'];
        return $totalcount;
    }

    function deletePoll($pollid)
    {
        $this->db->where('poll_id', $pollid)
            ->delete('ic_poll');
        $this->db->where('poll_id', $pollid)
            ->delete('ic_poll_answer');
        $this->db->where('poll_id', $pollid)
            ->delete('ic_poll_options');
        if ($this->db->affected_rows() > 0) {
            header('location: ' . $this->config->item('base_url') . 'backoffice/Poll/poll_management');
        } else {
            header('location: ' . $this->config->item('base_url') . 'backoffice/Poll/poll_management');
        }
    }

    function pollDetail($poll_id)
    {
        $query = $this->db->select('*')
            ->where('poll_id', $poll_id)
            ->get('ic_poll');
        $data = $query->result_array();
        if ($data) {
            $quer = $this->db->select('*')
                ->where('poll_id', $poll_id)
                ->get('ic_poll_options');
            $data['options'] = $quer->result_array();
            return $data;
        }
    }

    function pollUser($poll_id)
    {
        $userdata = $this->db->select('ipa.*,iu.unique_id,iu.user_fullname,iu.user_fullname,iu.user_fullname,iu.user_mobile,iu.user_gender,iu.user_district,iu.user_id')
            ->from('ic_poll_answer as ipa')
            ->join('ic_user as iu', 'iu.user_id=ipa.user_id')
            ->where('ipa.poll_id', $poll_id)
            //	 ->limit($limit)
            //	 ->offset($id)
            ->get();
        $pollUser = $userdata->result_array();
        foreach ($pollUser as $value) {
            if (!empty($value['user_district'])) {
                $locationQuery = $this->db->select('d.district_id,d.district_name_en,d.district_name_hi,s.state_id,s.state_name_en,s.state_name_hi')
                    ->from('ic_district as d')
                    ->join('ic_state as s', 'd.state_id=s.state_id')
                    ->where('d.district_id', $value['user_district'])
                    ->get();
                //echo $this->db->last_query(); die;
                $locationArr = $locationQuery->result_array();
            }
            $userlistArr[] = [
                'option_name' => $value['option_name'],
                'user_id' => $value['user_id'],
                'unique_id' => $value['unique_id'],
                'user_fullname' => $value['user_fullname'],
                'user_mobile' => $value['user_mobile'],
                'user_gender' => $value['user_gender'],
                'state_id' => (!empty($locationArr[0]['state_id'])) ? $locationArr[0]['state_id'] : '',
                'state' => (!empty($locationArr[0]['state_name_en'])) ? $locationArr[0]['state_name_en'] : '',
                'district_id' => (!empty($locationArr[0]['district_id'])) ? $locationArr[0]['district_id'] : '',
                'district' => (!empty($locationArr[0]['district_name_en'])) ? $locationArr[0]['district_name_en'] : ''
            ];
        }
        return $userlistArr;
    }

    function total_polluser($poll_id)
    {
        $userdata = $this->db->select('ipa.*,iu.user_fullname')
            ->from('ic_poll_answer as ipa')
            ->join('ic_user as iu', 'iu.user_id=ipa.user_id')
            ->where('ipa.poll_id', $poll_id)
            ->group_by('ipa.user_id')
            ->get();
        $pollUser = $userdata->result_array();
        if (isset($pollUser) && !empty($pollUser)) {
            $total_polluser = count($pollUser);
            return $total_polluser;
        }
    }

    function excelExport($poll_id)
    {
        $userdata = $this->db->select('ip.*,ipa.*,iu.unique_id,iu.user_fullname,iu.user_fullname,iu.user_fullname,iu.user_mobile,iu.user_gender,iu.user_district,iu.user_id')
            ->from('ic_poll as ip')
            ->join('ic_poll_answer as ipa', 'ip.poll_id=ipa.poll_id')
            ->join('ic_user as iu', 'iu.user_id=ipa.user_id')
            ->where('ipa.poll_id', $poll_id)
            //	 ->limit($limit)
            //	 ->offset($id)
            ->get();
        $pollUser = $userdata->result_array();
        //echo "<pre>"; print_r($pollUser); die;
        foreach ($pollUser as $value) {
            if (!empty($value['user_district'])) {
                $locationQuery = $this->db->select('d.district_id,d.district_name_en,d.district_name_hi,s.state_id,s.state_name_en,s.state_name_hi')
                    ->from('ic_district as d')
                    ->join('ic_state as s', 'd.state_id=s.state_id')
                    ->where('d.district_id', $value['user_district'])
                    ->get();
                //echo $this->db->last_query(); die;
                $locationArr = $locationQuery->result_array();
            }
            $userlistArr[] = [
                'poll_question_en' => $value['poll_question_en'],
                'poll_question_hi' => $value['poll_question_hi'],
                'option_name' => $value['option_name'],
                'user_id' => $value['user_id'],
                'unique_id' => $value['unique_id'],
                'user_fullname' => $value['user_fullname'],
                'user_mobile' => $value['user_mobile'],
                'user_gender' => $value['user_gender'],
                'state_id' => (!empty($locationArr[0]['state_id'])) ? $locationArr[0]['state_id'] : '',
                'state' => (!empty($locationArr[0]['state_name_en'])) ? $locationArr[0]['state_name_en'] : '',
                'district_id' => (!empty($locationArr[0]['district_id'])) ? $locationArr[0]['district_id'] : '',
                'district' => (!empty($locationArr[0]['district_name_en'])) ? $locationArr[0]['district_name_en'] : ''
            ];
        }
        return $userlistArr;
    }

    function userlists($postid)
    {
        $query = $this->db->select('poll_state,poll_district,poll_ac,poll_group')
            ->where('poll_id', $postid)
            ->get('ic_poll');
        $result = $query->result_array();
        if (!empty($result[0]['poll_group'])) {
            $group = explode(',', $result[0]['poll_group']);
            foreach ($group as $groupval) {
                $query = $this->db->select('group_members')
                    ->where('group_id', $groupval)
                    ->where('status', 'active')
                    ->get('ic_group');
                $groupArr = $query->result_array();
                $group_members[] = explode(',', $groupArr[0]['group_members']);
            }
            $userArrs = array_unique(call_user_func_array('array_merge', $group_members));
            $userArr = implode(',', $userArrs);
            $userQuery = $this->db->query("SELECT * FROM `ic_user` where user_id IN($userArr)");
        } else {
            $sql = "Select * from ic_user where user_status='active' and profile_completed='true'";
            if (!empty($result[0]['poll_state'])) {
                $sql .= " AND user_state='" . $result[0]['poll_state'] . "'";
            }
            if (!empty($result[0]['poll_district'])) {
                $sql .= " AND user_district='" . $result[0]['poll_district'] . "'";
            }
            if (!empty($result[0]['poll_ac'])) {
                $sql .= " AND ac_name='" . $result[0]['poll_ac'] . "'";
            }
            $userQuery = $this->db->query($sql);
        }
        $userArrRes = $userQuery->result_array();
        foreach ($userArrRes as $value) {
            //echo"<pre>"; print_r($value); die;
            if (!empty($value['user_district'])) {
                $locationQuery = $this->db->select('d.district_name_en,d.district_name_hi,s.state_name_en,s.state_name_hi')
                    ->from('ic_district as d')
                    ->join('ic_state as s', 'd.state_id=s.state_id')
                    ->where('d.district_id', $value['user_district'])
                    ->get();
                //echo $this->db->last_query(); die;
                $locationArr = $locationQuery->result_array();
            }
            $userlistArr[] = [
                'user_id' => $value['user_id'],
                'unique_id' => $value['unique_id'],
                'user_fullname' => $value['user_fullname'],
                'user_mobile' => $value['user_mobile'],
                'user_gender' => $value['user_gender'],
                'state' => (!empty($locationArr[0]['state_name_en'])) ? $locationArr[0]['state_name_en'] : '',
                'district' => (!empty($locationArr[0]['district_name_en'])) ? $locationArr[0]['district_name_en'] : ''
            ];
        }
        return $userlistArr;
    }
}
