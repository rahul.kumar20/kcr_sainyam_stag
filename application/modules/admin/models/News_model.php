<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class News_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name newsList
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function newsList($params)
    {
        $sortMap = [
            "registered" => "n.created_date",
            "priority" => "n.priority",

        ];

        if (!empty($params['EffectivestartDate']) && !empty($params['EffectiveendDate'])) {
            $EffectivestartDate = date('Y-m-d H:i:s', strtotime($params['EffectivestartDate']));
            $EffectiveendDate = date('Y-m-d H:i:s', strtotime($params['EffectiveendDate']));
            $this->db->select('SQL_CALC_FOUND_ROWS n.*,
            (SELECT COUNT(*) FROM ipac_news_comments nc WHERE nc.news_id = n.news_id AND nc.submitted_timestamp
            >= "' . $EffectivestartDate . '" AND nc.submitted_timestamp <= "' . $EffectiveendDate . '") AS comments_count,
            (SELECT COUNT(*) FROM ipac_news_shares ns WHERE ns.news_id = n.news_id AND ns.submitted_timestamp 
            >= "' . $EffectivestartDate . '" AND ns.submitted_timestamp <= "' . $EffectiveendDate . '") AS share_count,
            (SELECT COUNT(*) FROM ipac_news_likes nlikes WHERE nlikes.news_id = n.news_id AND nlikes.submitted_timestamp >= "' . $EffectivestartDate . '" AND nlikes.submitted_timestamp <= "' . $EffectiveendDate . '" ) AS likes_count', false);
            $this->db->from('ipac_news as n');
            
        } else {

            $this->db->select('SQL_CALC_FOUND_ROWS n.*,
        (SELECT COUNT(*) FROM ipac_news_comments nc WHERE nc.news_id = n.news_id) AS comments_count,
        (SELECT COUNT(*) FROM ipac_news_likes nlikes WHERE nlikes.news_id = n.news_id) AS likes_count', false);
            $this->db->from('ipac_news as n');
            
        }
        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('news_title', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) && (isset($params["sortby"]) && !empty($params["sortby"]))) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            //$this->db->order_by("n.priority", "ASC");
            //  $this->db->order_by("n.pin_date", "DESC");
            $this->db->order_by("CASE `priority` 
            WHEN '1' THEN pin_date 
            WHEN '2' THEN created_date
            ELSE 1 END 
        DESC");
        }
        //status filter
        if (!empty($params['status'])) {
            $this->db->where('status', $params['status']);
        } else {
            $this->db->where('status != 3');
        }

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where("FIND_IN_SET('" . $params["uid"] . "', registeration_no)>0");
        }
        //ContentType filter
        if (!empty($params['homeContentType'])) {
            $this->db->where('news_category', $params['homeContentType']);
        }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_date) >= '" . $startDate . "' AND DATE(created_date) <= '" . $endDate . "' ");
        }


        //section filter
        if (!empty($params['section'])) {
            $this->db->where('home_section', $params['section']);
        }
        //task completed filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }

        $this->db->group_by('n.news_id');
        $this->db->limit($params['limit'], $params['offset']);


        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }


        return $res;
    }

    /**
     *
     * @Function userList
     * @Descriptiom get district total user registered
     * @param $districtId id of district
     * @return array of result
     */
    public function userList($districtId)
    {

        $this->db->select("user_count as UserCnt", false);
        $this->db->from('district');
        $this->db->where('district_code' , $districtId);
        $query = $this->db->get();
        //if num or rows greater than 0
        if ($query->num_rows() > 0) {
            $res = $query->row();
        } else {
            $res = array();
        }

        return $res;
    }


    /**
     * @function newsDetail
     * @description get news detail
     *
     * @param type $newsId
     * @return type
     */
    public function newsDetail($newsId)
    {
        //if user id is set
        if (isset($newsId) && !empty($newsId)) {
            $this->db->select("in.*,sl.state_name,d.district_name");
            $this->db->from('ipac_news as in');
            $this->db->join('state_list as sl', 'in.state=sl.state_id', 'left');
            $this->db->join('district as d', 'in.district=d.district_code', 'left');
            $this->db->where_in('news_id', $newsId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    /**
     * @function userErns
     * @description get user ern
     *
     * @param type $newsId
     * @return type
     */
    public function userErns($params)
    {

        $this->db->select("ud.end_point_ern as arn_endpoint,ud.user_id", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id');
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.end_point_ern!=', '');

        //UID filter
        if (!empty($params['registeration_no'])) {
            // $this->db->where('u.registeration_no', $params["registeration_no"]);
            $this->db->where("FIND_IN_SET('" . $params["registeration_no"] . "', registeration_no)>0");
        }
        //state filter
        if (!empty($params['state'])) {
            $this->db->where('u.state', $params['state']);
        }
        //district filter
        if (!empty($params['district'])) {
            $this->db->where('u.district', $params['district']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('u.gender', $params['gender']);
        }
        //Gender filter
        if (!empty($params['college'])) {
            $this->db->where('u.college', $params['college']);
        }
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    /**
     * @function getNewsAllComments
     * @description Used to get the comments of a news
     *
     * @return array
     */
    public function getNewsAllComments($params, $only_comment = 'no')
    {
        $this->db->select(
            "u.user_id, 
            u.full_name,
            u.registeration_no, 
            inc.id AS comment_id, 
            inc.parent_id, 
            inc.user_comment, 
            inc.submitted_timestamp",
            false
        );
        $this->db->from('ipac_news_comments AS inc');
        $this->db->join('users AS u', 'inc.user_id = u.user_id', 'left');
        $this->db->order_by('inc.submitted_timestamp DESC');
        if (strtolower($only_comment) != 'yes') {
            $this->db->where("inc.news_id", $params['id']);
        }
        $this->db->where("inc.parent_id = 0", null, false);
        $this->db->where("inc.is_deleted = 0", null, false);

        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('u.full_name', $params['searchlike']);
            $this->db->group_end();
        }

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('u.registeration_no', $params['uid']);
        }

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(inc.submitted_timestamp) >= '" . $startDate . "' AND DATE(inc.submitted_timestamp) <= '" . $endDate . "' ");
        }

        // If particular comment id is given
        if (!empty($params['comment_id'])) {
            $this->db->where('inc.id', $params['comment_id']);
        }

        $this->db->limit($params['limit'], $params['offset']);
        $data           = $this->db->get();
        $result         = $data->result_array();

        if (strtolower($only_comment) == 'yes') {
            if (count($result) > 0) {
                return $result[0]['user_comment'];
            } else {
                return '';
            }
        }

        $final_result   = array();
        if (count($result) > 0) {
            foreach ($result as $k => $v) {
                $final_result[$k] = $v;
                $final_result[$k]['comments_count'] = $this->getChildCommentCount($v['comment_id']);
            }
        }


        $sqlTotal = "SELECT COUNT(nl.id) AS total_count FROM ipac_news_comments nl left join users AS u on nl.user_id = u.user_id WHERE parent_id = 0 AND nl.is_deleted = 0 AND nl.news_id = '" . $params['id'] . "'";

        //search block
        if (!empty($params['searchlike'])) {
            $sqlTotal .= " AND u.full_name LIKE '%" . $params['searchlike'] . "%'";
        }

        //UID filter
        if (!empty($params['uid'])) {
            $sqlTotal .= " AND u.registeration_no = '" . $params['uid'] . "'";
        }

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $sqlTotal .=  " AND DATE(nl.submitted_timestamp) >= '" . $startDate . "' AND DATE(nl.submitted_timestamp) <= '" . $endDate . "' ";
        }

        $dataTotal          = $this->db->query($sqlTotal);
        $resultTotal        = $dataTotal->row_array();
        $resArr['result']   = $final_result;
        $resArr['total']    = $resultTotal['total_count'];

        return $resArr;
    }

    /**
     * @function getChildCommentCount
     * @description Used to get the child comments count of a comment
     *
     * @return count (integer)
     */
    public function getChildCommentCount($comment_id)
    {
        $sql = "SELECT COUNT(nl.id) AS total_count FROM `ipac_news_comments` `nl` WHERE  `nl`.`parent_id` = $comment_id AND nl.is_deleted = 0  ORDER BY `nl`.`submitted_timestamp` DESC";

        $data       = $this->db->query($sql);
        $result     = $data->row_array();
        return $result['total_count'];
    }

    /**
     * @function getChildComments
     * @description Used to get the child comments of a comment
     *
     * @return array
     */
    public function getChildComments($params)
    {
        $this->db->select(
            "u.user_id, 
            u.full_name,
            u.registeration_no, 
            inc.id AS comment_id, 
            inc.news_id AS news_id, 
            inc.parent_id, 
            inc.user_comment, 
            inc.submitted_timestamp",
            false
        );
        $this->db->from('ipac_news_comments AS inc');
        $this->db->join('users AS u', 'inc.user_id = u.user_id', 'left');
        $this->db->order_by('inc.submitted_timestamp ASC');
        $this->db->where("inc.parent_id", $params['id']);
        $this->db->where("inc.is_deleted = 0", null, false);

        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('u.full_name', $params['searchlike']);
            $this->db->group_end();
        }

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('u.registeration_no', $params['uid']);
        }

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(inc.submitted_timestamp) >= '" . $startDate . "' AND DATE(inc.submitted_timestamp) <= '" . $endDate . "' ");
        }


        $this->db->limit($params['limit'], $params['offset']);
        $data           = $this->db->get();
        $result         = $data->result_array();


        $sqlTotal = "SELECT COUNT(nl.id) AS total_count FROM ipac_news_comments nl left join users AS u on nl.user_id = u.user_id WHERE nl.is_deleted = 0 AND parent_id = '" . $params['id'] . "'";

        //search block
        if (!empty($params['searchlike'])) {
            $sqlTotal .= " AND u.full_name LIKE '%" . $params['searchlike'] . "%'";
        }

        //UID filter
        if (!empty($params['uid'])) {
            $sqlTotal .= " AND u.registeration_no = '" . $params['uid'] . "'";
        }

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $sqlTotal .=  " AND DATE(nl.submitted_timestamp) >= '" . $startDate . "' AND DATE(nl.submitted_timestamp) <= '" . $endDate . "' ";
        }

        $dataTotal          = $this->db->query($sqlTotal);
        $resultTotal        = $dataTotal->row_array();
        $resArr['result']   = $result;
        $resArr['total']    = $resultTotal['total_count'];

        return $resArr;
    }


    public function getAllCommentsdetails($newsCommentId)
    {
        if (isset($newsCommentId) && !empty($newsCommentId)) {
            $this->db->select(
                "u.user_id, 
            u.full_name,
            u.registeration_no, 
            u.phone_number, 
            inc.id AS comment_id,
            (SELECT news_title FROM ipac_news n WHERE n.news_id = inc.news_id) AS news_title,
            inc.news_id AS news_id, 
            inc.parent_id, 
            inc.sub_parent_id,
            inc.user_comment,  
            inc.submitted_timestamp",
                false
            );
            $this->db->from('ipac_news_comments AS inc');
            $this->db->join('users AS u', 'inc.user_id = u.user_id', 'left');
            $this->db->where("inc.id", $newsCommentId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }
    public function commentUserErns($userID, $comment_id)
    {
        $this->db->select("ud.device_token,ud.platform", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id', 'inner');
        $this->db->join('ipac_news_comments as nc', 'nc.user_id=u.user_id', 'inner');
        $this->db->where('nc.id=', $comment_id);
        $this->db->where('nc.user_id!=', $userID);
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.device_token!=', '');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
}
