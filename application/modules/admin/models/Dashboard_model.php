<?php

class Dashboard_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getReportsData()
    {
        $this->db->select('count(user_id) totalUser,getTotalTask() as totalTask,getTotalNews() as totalNews');
        $this->db->where("is_active!=", DELETED);
        $query = $this->db->get("users");
        if ($query != false) {
            $result = $query->row_array();
        } else {
            return false;
        }
        return $result;
    }

    /**
     * @function get_yearly_data
     * @description To fetch number of users' registration from last 10 years
     *
     * @param array $data Required data to fetch details
     * @return array user registration no
     */
    public function get_yearly_data()
    {
        $start_year = date('Y');
        $end_year = $start_year - 10;
        $this->db->select(' year(registered_date) y, count(user_id) total ')
            ->group_by('year(registered_date)')
            ->where('year(registered_date)<=', $start_year, false)
            ->where('year(registered_date)>=', $end_year, false);
        $query = $this->db->get("users");
        $result = $query->result_array();
        if ($query->num_rows()) {
            return $result;
        } else {
            return null;
        }
    }

    /**
     *
     * @param type $year
     */
    public function get_monthly_data($year)
    {
        $this->db->select(' month(registered_date) y, count(user_id) total ')
            ->group_by('month(registered_date)')
            ->where('year(registered_date)', $year, false);
        $query = $this->db->get("users");
        $result = $query->result_array();
        if ($query->num_rows()) {
            return $result;
        } else {
            return null;
        }
    }

    /**
     *
     * @param type $year
     * @param type $month
     */
    public function get_weekly_data($year, $month)
    {
        $this->db->select('count(registered_date) total,concat(DATE_FORMAT(DATE_ADD(registered_date,
         INTERVAL(1-DAYOFWEEK(registered_date)) DAY),"%d,%b"),"-",
          DATE_FORMAT( DATE_ADD(registered_date, INTERVAL(7-DAYOFWEEK(registered_date)) DAY),"%d,%b")) y ')
            ->group_by("week(registered_date)")
            ->where("year(registered_date)", $year)
            ->where("month(registered_date)", $month);

        $query = $this->db->get("users");
        $result = $query->result_array();
        if ($query->num_rows()) {
            return $result;
        } else {
            return null;
        }
    }

    public function getUserCount($where, $data, $method)
    {

        $this->db->reset_query();
        if ($method == 'user') {
            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('users as u');
            $this->db->where($where);
            if (!empty($where['status'])) {
                $this->db->where('is_active', $where['status']);
            } else {
                $this->db->where('is_active != 3');
            }
            if (!empty($data['gender'])) {
                $this->db->where('u.gender', $data['gender']);
            }
            if (!empty($data['state'])) {
                $this->db->where('u.state', $data['state']);
            }
            if (!empty($data['district'])) {
                $this->db->where('u.district', $data['district']);
            }
            if (!empty($data['college'])) {
                $this->db->where('u.college', $data['college']);
            }
            $query = $this->db->get();
            $res['result'] = array_reverse($query->result_array());
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $res;
        } elseif ($method == 'news') {
            $this->db->reset_query();
            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('ipac_news as u');
            $this->db->where($where);

            $this->db->group_by('u.news_id');
            $query = $this->db->get();
//          echo $this->db->last_query();die;
            $res['result'] = array_reverse($query->result_array());
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $res;
        } elseif ($method == 'task') {
            $this->db->reset_query();
            $this->db->select("SQL_CALC_FOUND_ROWS tm.task_id,tm.created_date", false);
            $this->db->from('ipac_task_master as tm');
            $this->db->where($where);

            $query = $this->db->get();
            //          echo $this->db->last_query();die;

            $res['result'] = array_reverse($query->result_array());

            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $res;
        } elseif ($method == 'device_type') {
            $this->db->reset_query();
            $this->db->select("SQL_CALC_FOUND_ROWS s.*", false);
            $this->db->from('session as s');
            $this->db->where($where);
            $this->db->where_in('s.platform', ['1', '2']);
            $query = $this->db->get();
//        echo $this->db->last_query();die;
            $res['result'] = array_reverse($query->result_array());

            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $res;
        }
    }

    public function fetch_app_leaders($param)
    {
        $startDate = $param['startDate2'];
        $endDate = $param['endDate2'];
        if ($param['startDate2'] && $param['endDate2']) {
            $startDate = date('Y-m-d', strtotime($param['startDate2']));
            $endDate = date('Y-m-d', strtotime($param['endDate2']));
        }
        $this->db->select(
            'SQL_CALC_FOUND_ROWS full_name, sl.state_name, d.district_name, registeration_no,registered_on,total_earning,task_completed,user_image,rating, (SELECT COUNT(detail_id) from user_task_master WHERE user_id = users.user_id and status = "1") as task_count  ',
            false
        );
        $this->db->from('users');
        $this->db->join('state_list as sl', 'users.state=sl.state_id', 'inner');
        $this->db->join('district as d', 'users.district=d.district_code', 'inner');
        //state filter
        if (!empty($param['state2'])) {
            $this->db->where('users.state', $param['state2']);
        }
        //state filter
        if (!empty($param['distict2'])) {
            $this->db->where('users.district', $param['distict2']);
        }
        //state filter
        if (!empty($param['college2'])) {
            $this->db->where('users.college', $param['college2']);
        }
        //status filter
        if (!empty($param['status2'])) {
            $this->db->where('users.is_active', $param['status2']);
        }
        //task type filter
        if (!empty($param['taskType'])) {
            $this->db->join('user_task_master', '(user_task_master.user_id = users.user_id)', 'inner', false);
            $this->db->join('ipac_task_master it', '(user_task_master.task_id = it.task_id AND it.task_type="' . $param['taskType'] . '")', 'inner', false);
        }
        //date filter
        if ($param['startDate2'] && $param['endDate2']) {
            $this->db->where('DATE(inserted_on ) >= ', date('Y-m-d', strtotime($param['startDate2'])));
            $this->db->where('DATE(inserted_on ) <= ', date('Y-m-d', strtotime($param['endDate2'])));
        }
        $this->db->group_by('users.user_id', false);
        $this->db->order_by('total_earning', 'DESC');
        $this->db->limit(10);

        $query                  = $this->db->get();
        $err                    = $this->db->error();

        $this->totalleaders     = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['leaders'] = $query->result_array();
            $res['total_leaders'] = $this->totalleaders;
            return $res;
        }
    }

    /**
     * @param type $start_date, $end_date
     * @return Array Get top 10 leaders of app
     */
    public function fetch_app_leaders_old($param)
    {
        $startDate = $param['startDate2'];
        $endDate = $param['endDate2'];
        if ($param['startDate2'] && $param['endDate2']) {
            $startDate = date('Y-m-d', strtotime($param['startDate2']));
            $endDate = date('Y-m-d', strtotime($param['endDate2']));
        }
        // $this->db->select(
        //     'SQL_CALC_FOUND_ROWS full_name, sl.state_name, d.district_name, registeration_no,registered_on,total_earning,task_completed,user_image,SUM(total_earned_points) as rewards,rating,
        //     getUserRankByPTA(user_earnings.user_id,"' . $startDate . '","' . $endDate . '", "' . $param['taskType'] . '","' . $param['state2'] . '","' . $param['distict2'] . '","' . $param['college2'] . '") as ranking,',
        //     false
        // );
        $this->db->select(
            'SQL_CALC_FOUND_ROWS full_name, sl.state_name, d.district_name, registeration_no,registered_on,total_earning,task_completed,user_image,SUM(total_earned_points) as rewards,rating',
            false
        );
        $this->db->from('user_earnings');
        $this->db->join('users', '(users.user_id = user_earnings.user_id)', 'inner', false);
        $this->db->join('state_list as sl', 'users.state=sl.state_id', 'inner');
        $this->db->join('district as d', 'users.district=d.district_code', 'inner');
        // $this->db->join('college_list as cl', 'users.college=cl.college_id', 'left');
        //state filter
        if (!empty($param['state2'])) {
            $this->db->where('users.state', $param['state2']);
        }
        //state filter
        if (!empty($param['distict2'])) {
            $this->db->where('users.district', $param['distict2']);
        }
        //state filter
        if (!empty($param['college2'])) {
            $this->db->where('users.college', $param['college2']);
        }
        //status filter
        if (!empty($param['status2'])) {
            $this->db->where('users.is_active', $param['status2']);
        }
        //task type filter
        if (!empty($param['taskType'])) {
            $this->db->join('ipac_task_master it', '(user_earnings.task_id = it.task_id AND it.task_type="' . $param['taskType'] . '")', 'inner', false);
        }
        //date filter
        if ($param['startDate2'] && $param['endDate2']) {
            $this->db->where('DATE(inserted_on ) >= ', date('Y-m-d', strtotime($param['startDate2'])));
            $this->db->where('DATE(inserted_on ) <= ', date('Y-m-d', strtotime($param['endDate2'])));
        }
        $this->db->group_by('user_earnings.user_id', false);
        $this->db->order_by('sum(total_earned_points)', 'DESC');
        // $this->db->limit($limit, $offset);
        $this->db->limit(10);

        $query = $this->db->get();

        $err = $this->db->error();
        $this->totalleaders = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            $res['leaders'] = $query->result_array();
            $res['total_leaders'] = $this->totalleaders;
            return $res;
        }
    }
}
