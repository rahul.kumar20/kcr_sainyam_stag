<?php

class Contactus_model extends CI_Model
{

    public $finalrole = array();

    public function __construct()
    {
        $this->load->database();
    }



    //--------------------------------------------------------------------------
    /**
     * @name userlist
     * @description Used to filter the users
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function contactuslist($params = [])
    {
        $orderByMap = [
            "added" => "inserted_on",
            "name" => 'full_name'
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS a.*,category_name,full_name", false);
        $this->db->from('user_contact_us a');
        $this->db->join('contact_us_category as con_cat', 'con_cat.category_id=a.contact_category', 'inner');
        $this->db->join('users', 'users.user_id=a.user_id', 'inner');

        /* order by */
        if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) && (isset($params["order"]) && !empty($params["order"]))) {
            $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
        } else { //default Order by on created date desc
            $this->db->order_by("a.inserted_on", "DESC");
        }
        /* order by end */

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(a.inserted_on) >= '" . $startDate . "' AND DATE(a.inserted_on) <= '" . $endDate . "' ");
        }
        /* setting search */
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('category_name', $params['searchlike']);
            $this->db->or_like('full_name', $params['searchlike']);
            $this->db->or_like('contact_us_content', $params['searchlike']);
            $this->db->or_like('ticket_id', $params['searchlike']);

            $this->db->group_end();
        }
        /* setting LImit */
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

    /**
     * @function userDetail
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function contactUsDetail($userId)
    {
        //if user id is set
        if (isset($userId) && !empty($userId)) {
            $this->db->select("
            u.user_id,
            u.registeration_no,
            u.full_name,
            u.phone_number,
            a.*,category_name,
            ", false);
            $this->db->from('user_contact_us a');
            $this->db->join('contact_us_category as con_cat', 'con_cat.category_id=a.contact_category', 'left');
            $this->db->join('users as u', 'u.user_id=a.user_id', 'left');
            $this->db->where_in('a.contact_id', $userId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    /**
     * @name userlist
     * @description Used to filter the users
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function contactuscategorylist($params = [])
    {
        $orderByMap = [
            "added" => "inserted_on",
            "name" => 'full_name'
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS a.*", false);
        $this->db->from('contact_us_category a');


        /* order by */
        if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) && (isset($params["order"]) && !empty($params["order"]))) {
            $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
        } else { //default Order by on created date desc
            $this->db->order_by("a.inserted_on", "DESC");
        }
        /* order by end */

        /* setting LImit */
        $this->db->limit($params['limit'], $params['offset']);
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(a.inserted_on) >= '" . $startDate . "' AND DATE(a.inserted_on) <= '" . $endDate . "' ");
        }
        /* setting search */
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('category_name', $params['searchlike']);
            $this->db->group_end();
        }

        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }
}
