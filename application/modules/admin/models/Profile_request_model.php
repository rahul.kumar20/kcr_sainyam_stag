<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Profile_request_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name requestedUserList
     * @descriptio profile update requested users list
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function requestedUserList($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "pr.requested_date",
            "task_completed" => "u.task_completed",
            "reward_point" => "u.points_earned"
        ];

        $this->db->select(
        "SQL_CALC_FOUND_ROWS 
        u.user_id,
        u.registeration_no,
        u.registered_on,
        u.gender,
        u.is_active,
        pr.requested_date,
        pr.status,
        CASE WHEN pr.full_name !='' THEN pr.full_name ELSE u.full_name END as full_name,
        CASE WHEN pr.email_id !='' THEN pr.email_id ELSE u.email_id END as email_id,
        sl.state_name,
        d.district_name,
        cl.college_name,
        totalRefrralUsed(u.user_id) as total_referral
        ",
            false
        );
        $this->db->from('user_profile_change_request as pr');
        $this->db->join('users as u', 'pr.user_id=u.user_id ', 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');
        $this->db->where('u.requested_for_edit_profile', 1);
        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("pr.requested_date", "DESC");
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
         //UID filter
        if (! empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
          //state filter
        if (! empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
          //district filter
        if (! empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
           //Gender filter
        if (! empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
            //college filter
        if (! empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(pr.requested_date) >= '" . $startDate . "' AND DATE(pr.requested_date) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }


 /**
     * @function userDetail
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function userDetail($userId)
    {
        //if user id is set
        if (isset($userId) && !empty($userId)) {
            $this->db->select("
            u.user_id,
            u.registeration_no,
            u.full_name,
            pr.full_name as new_name,
            pr.email_id as new_email_id,
            pr.user_image as new_image,
            pr.whatsup_number as new_whatsapp_number,
            u.whatsup_number as old_whatsup_number,
            u.email_id,
            u.user_image,
            u.language,
            gender,
            pr.requested_date,
            u.upi_address,
            pr.upi_address as new_upi_address,
            u.account_name,
            pr.account_name as new_account_name,
            u.account_number,
            pr.account_number as new_acount_number,
            u.ifsc_code,
            pr.ifsc_code as new_isfc_code,
            pr.facebook_id,
            pr.facebook_username,
            pr.twitter_id,
            pr.twitter_username,
            u.facebook_id as old_facebook_id,
            u.fb_username as old_fb_username,
            u.twitter_id as old_twitter_id,
            u.twitter_username as old_twitter_username
            ", false);
            $this->db->from('users as u');
            $this->db->join('user_profile_change_request as pr' ,'u.user_id=pr.user_id', 'inner');
            $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
            $this->db->join('district as d', 'u.district=d.district_code', 'left');
            $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

            $this->db->where_in('u.user_id', $userId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }



    /**
     * @name referralUserList
     * @description Used to filter the referral user users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function referralUserList($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "u.user_id",
            "task_completed" => "u.task_completed",
            "reward_point" => "u.points_earned"
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS u.*,sl.state_name,d.district_name,cl.college_name", false);
        $this->db->from('users as u');
        $this->db->join('ipac_referal_user as ru', 'u.user_id = ru.user_id', 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("u.registered_on", "DESC");
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
         //UID filter
        if (! empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
          //state filter
        if (! empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
          //district filter
        if (! empty($params['district'])) {
            $this->db->where('district', $params['distict']);
        }
           //Gender filter
        if (! empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
            //college filter
        if (! empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(registered_on) >= '" . $startDate . "' AND DATE(registered_on) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }

        $this->db->where('ru.referal_user_id', $params['id']);

        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();

        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
        return $res;
    }

    
}
