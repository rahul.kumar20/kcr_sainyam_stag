<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Podcast_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name newsList
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function getpodcastdetails($params)
    {
        $this->db->select("SQL_CALC_FOUND_ROWS a.*", false);
        $this->db->from('podcast a');
       
        /* setting search */
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('title', $params['searchlike']);
            $this->db->group_end();
        }
        /* setting LImit */
        $this->db->limit($params['limit'], $params['offset']);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

}