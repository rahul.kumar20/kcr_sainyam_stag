<?php

class Mks_chat_model extends CI_Model
{

    public $finalrole = array();

    public function __construct()
    {
        $this->load->database();
    }

    public function Mkschatlist($params = [])
    {
        $orderByMap = [
            "added" => "answered_datetime",
            "name" => 'full_name'
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS a.*, full_name, phone_number, d.district_name, i.ac", false);
        $this->db->from('ic_chat_response a');
        $this->db->join('users', 'users.user_id=a.user_id', 'inner');
        $this->db->join('district as d', 'users.district=d.district_code', 'left');
        $this->db->join('tbl_ac as i', 'users.ac=i.id', 'left');

        /* order by */
        if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) && (isset($params["order"]) && !empty($params["order"]))) {
            $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
        } else { //default Order by on created date desc
            $this->db->order_by("a.answered_datetime", "DESC");
        }
        /* order by end */

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(a.answered_datetime) >= '" . $startDate . "' AND DATE(a.answered_datetime) <= '" . $endDate . "' ");
        }
        /* setting search */
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->or_like('full_name', $params['searchlike']);
            $this->db->group_end();
        }
        /* setting LImit */
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

    //--------------------------------------------------------------------------
    /**
     * @name userlist
     * @description Used to filter the users
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */

    /**
     * @function userDetail
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function mks_chatDetail($userId,$Id)
    {
        //if user id is set
        if (isset($userId) && !empty($userId)) {
            $this->db->select("
            u.user_id,
            u.registeration_no,
            u.full_name,
            u.phone_number,
            con_cat.question,
            a.*,
            ", false);
            $this->db->from('ic_chat_response a');
            $this->db->join('ic_chatbot_content as con_cat', 'con_cat.id=a.question_id', 'left');
            $this->db->join('users as u', 'u.user_id=a.user_id', 'left');
            $this->db->where_in('a.question_id', $Id);
            $this->db->where_in('a.user_id', $userId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }
    /**
     * @name userlist
     * @description Used to filter the users
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
}
