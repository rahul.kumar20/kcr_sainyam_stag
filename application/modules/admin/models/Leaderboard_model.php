<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Leaderboard_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name userToplist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function userToplist($params)
    {
        $sortMap = [
            "name" => "full_name",
            "registered" => "u.user_id",
            "task_completed" => "u.task_completed",
            "reward_point" => "rewards"
        ];
        $startDate = $params['startDate'];
        $endDate = $params['endDate'];
        //if start date and end date is set
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
        }
        $this->db->select(
            'SQL_CALC_FOUND_ROWS
             u.user_id,
             u.registeration_no,
             full_name,
             user_image,
             SUM(total_earned_points) as rewards,
             getUserRankByPTA(u.user_id,"'.$startDate.'","'.$endDate.'","","'.$params['state'].'","'.$params['distict'].'","'.$params['college'].'") as ranking,
             rating,
             sl.state_name,
             d.district_name,
             is_active,
             cl.college_name,
             getuserTotalTaskComplted(u.user_id,0) as task_completed,
             registered_on
             ,',
            false
        );
        $this->db->from('user_earnings');
        $this->db->join('users as u', '(u.user_id = user_earnings.user_id)', 'inner', false);
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');
        //task type filter
        if (!empty($params['taskPlatform'])) {
            $this->db->join('ipac_task_master it', '(user_earnings.task_id = it.task_id AND it.task_type="'.$params['taskPlatform'].'")', 'inner', false);
        }
        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) && (isset($params["sortby"]) && !empty($params["sortby"]))) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        }
        //status filter
        if (!empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
         //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
          //state filter
        if (!empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
          //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }

          //college filter
        if (!empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
           //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(inserted_on) >= '" . $startDate . "' AND DATE(inserted_on) <= '" . $endDate . "' ");
        }
        //reward point filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->having('rewards >=', $taskCompleted['0']);
            $this->db->having('rewards <=', $taskCompleted['1']);
        }
        $this->db->group_by('user_earnings.user_id', false);
        $this->db->order_by('rewards', 'Desc');
        $this->db->limit($params['limit'], $params['offset']);

        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
        return $res;
    }

    /**
     * @Function userAppRank
     * @Description Get user rank
     * @param $id id of user
     * @return rank of user
     */
    public function userAppRank($id)
    {

        $sql = "SELECT user_id, points_earned, rank,full_name
            FROM
            (
            SELECT user_id, points_earned,full_name,
                    @r := IF(@c = points_earned, @r, @r+1) rank, @c := points_earned 
                FROM 
            (
                SELECT user_id, points_earned ,full_name 
                FROM users t 
                GROUP BY t.user_id
                ORDER BY points_earned DESC limit 50
            )  AS t 
            CROSS JOIN 
            (
                SELECT @r := 0,  @c := NULL
            ) AS i
            ) AS q WHERE q.user_id=$id";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /**
     * @name userToplist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function userTasklist($params)
    {
        $sortMap = [
            "startdate" => "start_date",
            "registered" => "ut.added_on",
            "completed" => "u.task_completed",
            "point" => "points",
            "enddate" => "end_date",
        ];

        $this->db->select(
            'SQL_CALC_FOUND_ROWS
             it.task_id,
             task_title,
             points,
             task_code,
             start_date,
             end_date,
             task_type,
             action,
             sl.state_name,
             d.district_name,
             is_active,
             cl.college_name,
             task_status,
			 ut.added_on,
			 it.created_date,
             u.full_name,
             ut.status,
             it.registeration_no,
			 getuserTotalTaskComplted(0,it.task_id) as task_completed
             ,',
            false
        );
        $this->db->from('user_task_master as ut');
        $this->db->join('users as u', '(ut.user_id = u.user_id)', 'inner', false);
        $this->db->join('ipac_task_master as it', '(ut.task_id = it.task_id)', 'inner', false);
        $this->db->join('state_list as sl', 'it.state=sl.state_id', 'left');
        $this->db->join('district as d', 'it.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'it.college=cl.college_id', 'left');
        $this->db->where('u.user_id', $params['id']);
        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('task_title', $params['searchlike']);
            $this->db->or_like('task_code', $params['searchlike']);

            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) && (isset($params["sortby"]) && !empty($params["sortby"]))) {
            $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
        } else {
            $this->db->order_by('added_on', 'desc');
        }
        //status filter
        if (!empty($params['status'])) {
            $this->db->where('ut.status', $params['status']);
        }
       
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(ut.added_on) >= '" . $startDate . "' AND DATE(ut.added_on) <= '" . $endDate . "' ");
        }
        //reward point filter
        if (!empty($params['taskCompleted'])) {
            $this->db->where('ut.type', $params['taskCompleted']);
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }
}
