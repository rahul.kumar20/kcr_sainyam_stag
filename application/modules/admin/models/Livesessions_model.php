<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Livesessions_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @name liceSessionslist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function liveSessionslist($params = [])
    {
        $orderByMap = [
            "added" => "session_date",
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS ls.*", false);
        $this->db->from('live_session ls');

        /* order by */
        if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) && (isset($params["order"]) && !empty($params["order"]))) {
            $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
        } else { //default Order by on created date desc
            $this->db->order_by("ls.id", "DESC");
        }
        /* order by end */

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate      = date('Y-m-d', strtotime($params['startDate']));
            $endDate        = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(ls.session_date) >= '" . $startDate . "' AND DATE(ls.session_date) <= '" . $endDate . "' ");
        }

        /* setting search */
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('ls.session_video_id', $params['searchlike']);
            $this->db->group_end();
        }

        /* setting LImit */
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();

        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

}