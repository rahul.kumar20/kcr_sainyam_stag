<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_uploads_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    /**
     * @name getUserUploads
     * @description Used to filter the user uploads list
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function getUserUploads($params)
    {

        $sortMap = [
            "startdate" => "uu.created_date"
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS uu.*,u.registeration_no,u.full_name,sl.state_name,d.district_name", false);
        $this->db->from('user_uploads as uu');
        $this->db->join('users as u', 'uu.user_id=u.user_id' , 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');

        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('u.full_name', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) &&
            (isset($params["sortby"]) && !empty($params["sortby"]))
        ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("uu.created_date", "DESC");
        }

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('u.registeration_no', $params['uid']);
        }
        
        //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
        
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(uu.created_date) >= '" . $startDate . "' AND DATE(uu.created_date) <= '" . $endDate . "' ");
        }
        
        $this->db->limit($params['limit'], $params['offset']);

        $query                  = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result']      = $query->result_array();
            $res['total']       = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result']      = array();
            $res['total']       = 0;
        }

        return $res;
    }


    /**
     * @function userUploadsDetailInfo
     * @description get user uploads info
     *
     * @param type $userUploadsId
     * @return type
     */
    public function userUploadsDetailInfo($userUploadsId)
    {
        //if user id is set
        if (isset($userUploadsId) && !empty($userUploadsId)) {
            $this->db->select("uu.*,u.registeration_no,u.full_name,sl.state_name,d.district_name");
            $this->db->from('user_uploads as uu');
            $this->db->join('users as u', 'uu.user_id=u.user_id' , 'inner');
            $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
            $this->db->join('district as d', 'u.district=d.district_code', 'left');

            $this->db->where_in('user_uploads_id', $userUploadsId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

}
