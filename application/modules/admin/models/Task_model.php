<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Task_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name taskList
     * @description Used to filter the tasks
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function taskList($params)
    {

        $sortMap = [
            "registered" => "tm.created_date",
            "point" => "tm.points",
            "startdate" => "tm.created_date",
            "enddate" => "tm.end_date",
        ];

        //$this->db->select("SQL_CALC_FOUND_ROWS tm.*,d.district_name,getuserTotalTaskComplted(0,tm.task_id) as task_completed", false);
        $this->db->select("SQL_CALC_FOUND_ROWS tm.*,d.district_name", false);
        $this->db->from('ipac_task_master as tm');
        // $this->db->join('state_list as sl', 'tm.state=sl.state_id', 'left');
        $this->db->join('district as d', 'tm.district=d.district_code', 'left');
        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('task_title', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) &&
            (isset($params["sortby"]) && !empty($params["sortby"]))
        ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("tm.created_date", "DESC");
        }
        //status filter
        if (!empty($params['status']) && $params['status'] != DELETED) {
            $this->db->where('task_status', $params['status']);
        } else {
            $this->db->where('task_status != 3');
        }

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
        //state filter
        // if (!empty($params['state'])) {
        //     $this->db->where('state', $params['state']);
        // }
        //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
        //Task Type filter
        if (!empty($params['taskType'])) {
            $this->db->where('task_type', $params['taskType']);
        }

        //Task Type filter
        if (!empty($params['taskAct'])) {
            $this->db->where('action', $params['taskAct']);
        }
        //College filter
        if (!empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_date) >= '" . $startDate . "' AND DATE(created_date) <= '" . $endDate . "' ");
        }
         //Live date filter
         if (!empty($params['LivestartDate']) && !empty($params['LiveendDate'])) {
            $LivestartDate = date('Y-m-d', strtotime($params['LivestartDate']));
            $LiveendDate   = date('Y-m-d', strtotime($params['LiveendDate']));
            $this->db->where("DATE(start_date) >= '" . $LivestartDate . "' AND DATE(start_date) <= '" . $LiveendDate . "' ");
        }
        //task completed filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

    //only for export csv purpose
    public function taskListDownload($params)
    {

        $sortMap = [
            "registered" => "tm.created_date",
            "point" => "tm.points",
            "startdate" => "tm.created_date",
            "enddate" => "tm.end_date",
        ];
        $this->db->select("SQL_CALC_FOUND_ROWS tm.*,getuserTotalTaskComplted(0,tm.task_id) as task_completed", false);
        $this->db->from('ipac_task_master as tm');
        $this->db->limit($params['limit'], $params['offset']);
        
        $query      = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }


    /**
     * @name taskList
     * @description Used to filter the tasks
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function getTotalCount($params)
    {


        $this->db->select("SQL_CALC_FOUND_ROWS 
        SUM(CASE 
             WHEN tm.task_type = 'facebook' THEN 1
             ELSE 0
           END) AS totalFacebookTask,
           SUM(CASE 
             WHEN tm.task_type = 'twitter' THEN 1
             ELSE 0
           END) AS totalTwitterTask,
           SUM(CASE 
             WHEN tm.task_type = 'whatsapp' THEN 1
             ELSE 0
           END) AS totalWhatsappTask,
           SUM(CASE 
           WHEN tm.task_type = 'offline' THEN 1
           ELSE 0
         END) AS totalOfflineTask,
         SUM(CASE 
         WHEN tm.task_type = 'youtube' THEN 1
         ELSE 0
       END) AS totalYoutubeTask,
	   SUM(CASE 
         WHEN tm.task_type = 'online' THEN 1
         ELSE 0
       END) AS totalOnlineTask", false);
        $this->db->from('ipac_task_master as tm');
        //status filter
        if (!empty($params['status']) && $params['status'] != DELETED) {
            $this->db->where('task_status', $params['status']);
        } else {
            $this->db->where('task_status != 3');
        }

        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
        //state filter
        if (!empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
        //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
        //Task Type filter
        if (!empty($params['taskType'])) {
            $this->db->where('task_type', $params['taskType']);
        }
        //College filter
        if (!empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_date) >= '" . $startDate . "' AND DATE(created_date) <= '" . $endDate . "' ");
        }
        //date filter
        if (!empty($params['LivestartDate']) && !empty($params['LiveendDate'])) {
            $LivestartDate = date('Y-m-d', strtotime($params['LivestartDate']));
            $LiveendDate   = date('Y-m-d', strtotime($params['LiveendDate']));
            $this->db->where("DATE(created_date) >= '" . $LivestartDate . "' AND DATE(created_date) <= '" . $LiveendDate . "' ");
        }
        //task completed filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->row_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }
    public function userList($districtId)
    {

        $this->db->select("count(*) as UserCnt", false);
        $this->db->from('users');
        $this->db->where("district = $districtId");
        $this->db->group_by('users.district', false);
        $query         = $this->db->get();
        $res = $query->row();

        return $res;
    }


    /**
     * @function taskDetail
     * @description get task detail
     *
     * @param type $taskId
     * @return type
     */
    public function taskDetail($taskId)
    {
        //if user id is set
        if (isset($taskId) && !empty($taskId)) {
            $this->db->select("tm.*,sl.state_name,d.district_name,cl.college_name,getuserTotalTaskComplted(0,tm.task_id) as task_completed");
            $this->db->from('ipac_task_master as tm');
            $this->db->join('state_list as sl', 'tm.state=sl.state_id', 'left');
            $this->db->join('district as d', 'tm.district=d.district_code', 'left');
            $this->db->join('college_list as cl', 'tm.college=cl.college_id', 'left');

            $this->db->where_in('task_id', $taskId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    /**
     * @name userToplist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function userTaskCompletedList($params)
    {
        $sortMap = [
            "name" => "full_name",
            "registered" => "u.user_id",
            "task_completed" => "u.task_completed",
            "reward_point" => "rewards"
        ];

        $this->db->select(
            'SQL_CALC_FOUND_ROWS
             u.user_id,
             registeration_no,
             full_name,
             user_image,
             ranking,
             rating,
             sl.state_name,
             d.district_name,
             is_active,
             cl.college_name,
             registered_on,
             ut.status',
            false
        );
        $this->db->from('user_task_master as ut');
        $this->db->join('users as u', '(u.user_id = ut.user_id)', 'left', false);
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');
        $this->db->where('ut.task_id', $params['id']);
        //status filter
        if (!empty($params['completeStatus'])) {
            $this->db->where('ut.status', $params['completeStatus']);
        }
        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) && (isset($params["sortby"]) && !empty($params["sortby"]))) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("u.points_earned", "DESC");
        }
        //status filter
        if (!empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
        //state filter
        if (!empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
        //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }

        //college filter
        if (!empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(registered_on) >= '" . $startDate . "' AND DATE(registered_on) <= '" . $endDate . "' ");
        }
        //reward point filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']); 
            $this->db->having('rewards >=', $taskCompleted['0']);
            $this->db->having('rewards <=', $taskCompleted['1']);
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }

    public function getDistrictACs($district_id = '') {
        $this->db->select("*");
        $this->db->from("tbl_ac");
        if($district_id != ''){
            $this->db->where('district',$district_id);
        }
        $this->db->order_by("ac", "asc");
        $query    = $this->db->get();
        $num_rows = $query->num_rows();
        if($num_rows > 0){
            $row = $query->result();
            return $row;
        }
        return false;
    }
}
