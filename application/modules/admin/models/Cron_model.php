<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cron_model extends CI_Model
{
    public function birthdayNotifUserErns()
    {
        $this->db->select("ud.end_point_ern as arn_endpoint, ud.user_id, u.full_name", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id');
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.end_point_ern!=', '');
        $this->db->where("DAYOFMONTH(u.dob)", date('d'));
        $this->db->where('month(u.dob)', date('m'));
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    // public function userlevel($level, $set_limit = '')
    // {
    //     $this->db->select("ul.pk_id,u.full_name,u.email_id,u.registeration_no,u.user_id,u.phone_number", false);
    //     $this->db->from('tbl_user_level as ul');
    //     $this->db->join('users as u', 'ul.fk_user_id = u.user_id');
    //     $this->db->where('u.is_active=', 1);
    //     $this->db->where('ul.fk_level_id=', $level);
    //     $this->db->where('ul.notified=', 0);
    //     // if ($level == 4) {
    //     //     $this->db->where('ul.fk_level_id=', 4);
    //     //     $this->db->where('ul.notified=', 0);
    //     // } else 
    //     // if ($level == 7 || $level == 9 || $level == 4 || $level == 11) {
    //     //    
    //     //  } else {
    //     // //     $this->db->where('ul.fk_level_id>=', 1);
    //     // //     $this->db->where('ul.notified_level_one=', 0);
    //     // // }
    //     if ($set_limit > 0) {
    //         $this->db->limit($set_limit);
    //     } else {
    //         $this->db->limit(50);
    //     }
    //     $query = $this->db->get();
    //     $res = $query->result_array();
    //     return $res;
    // }

    public function userlevel($level, $set_limit = '',$notified_flag = '')
    {
        $this->db->select("ul.pk_id,u.full_name,u.email_id,u.registeration_no,u.user_id,u.phone_number", false);
        $this->db->from('tbl_user_level as ul');
        $this->db->join('users as u', 'ul.fk_user_id = u.user_id');
        $this->db->where('u.is_active=', 1);
        if($notified_flag){
            $this->db->where('ul.fk_level_id=', $level);
            $this->db->where('ul.notified=', $notified_flag);
        }else{
            if ($level == 1) {
                $this->db->where('ul.fk_level_id=', 1);
                $this->db->where('ul.notified_level_one=',0);
            } else {
                $this->db->where('ul.fk_level_id=', $level);
                $this->db->where('ul.notified=', 0);
            }
        }
        if ($set_limit > 0) {
            $this->db->limit($set_limit);
        } else {
            $this->db->limit(100);
        }
        $query = $this->db->get();
        // print_r($this->db->last_query());exit;
        $res = $query->result_array();
        return $res;
    }
    public function getNews()
    {
        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_news');
        $this->db->where('notification_update=', 0);
        $this->db->where('is_send_notif=', 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getscheduledNews()
    {
        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_news');
        $this->db->where('ipac_news.start_date <= ', $currentDate);
        $this->db->where('status=', 2);
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function newsUserErns($params, $news_id)
    {
        $this->db->select('user_id')->from('user_notification')->where('news_id', $news_id);
        $subQuery =  $this->db->get_compiled_select();
        $this->db->select("ud.device_token,ud.user_id", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id', 'inner');
        $this->db->where("ud.user_id NOT IN ($subQuery)", NULL, FALSE);
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.device_token!=', '');

        //UID filter
        if (!empty($params['registeration_no'])) {
            $registeration_no_array = explode(",", $params["registeration_no"]);
            $this->db->where_in('registeration_no', $registeration_no_array);
        }

        //district filter
        if (!empty($params['district'])) {
            $this->db->where('u.district', $params['district']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('u.gender', $params['gender']);
        }

        $this->db->limit(10000);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getTasks()
    {
        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_task_master');
        $this->db->where('ipac_task_master.start_date <= ', $currentDate);
        $this->db->where('notification_update=', 0);
        $this->db->where('is_send_notif=', 1);
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getscheduledTasks()
    {
        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_task_master');
        $this->db->where('ipac_task_master.start_date <= ', $currentDate);
        $this->db->where('ipac_task_master.end_date >= ', $currentDate);
        $this->db->where('is_push_notif_sent=', 0);
        $this->db->where('district=', 0);
        $this->db->where('ac=', 0);
        $this->db->where('is_send_notif=', 1);
        $this->db->where('registeration_no = "0"');
        $query = $this->db->get();
        // print_r($this->db->last_query());
        // die;
        $res = $query->result_array();
        return $res;
    }

    public function getTaskMedia($task_id)
    {
        $this->db->select("task_url,task_thumb,task_media_type", false);
        $this->db->from('ipac_task_media');
        $this->db->where('task_id=', $task_id);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function tasksUserErns($params, $task_id)
    {
        $this->db->select('user_id')->from('user_notification')->where('task_id', $task_id);
        $subQuery =  $this->db->get_compiled_select();

        $this->db->select("ud.device_token,ud.user_id", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id', 'inner');
        $this->db->where("ud.user_id NOT IN ($subQuery)", NULL, FALSE);
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.device_token!=', '');

        //UID filter
        if (!empty($params['registeration_no'])) {
            $registeration_no_array = explode(",", $params["registeration_no"]);
            $this->db->where_in('registeration_no', $registeration_no_array);
        }

        //district filter
        if (!empty($params['district'])) {
            $this->db->where('u.district', $params['district']);
        }

        //AC filter
        if (!empty($params['ac'])) {
            $this->db->where('u.ac', $params['ac']);
        }

        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('u.gender', $params['gender']);
        }

        $this->db->limit(10000);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getGrievanceUser($type = 'grievance')
    {
        // Type = grievance/feedback
        $grienvanceID = $type == 'grievance' ? ['38', '39'] : ['40', '41', '42', '43', '45'];
        $this->db->select("SQL_CALC_FOUND_ROWS grievance.question_id, users.full_name, users.email_id, users.user_id", false);
        $this->db->from('ic_chat_response grievance');
        $this->db->join('users', 'users.user_id = grievance.user_id', 'inner');
        $this->db->where_in('grievance.question_id', $grienvanceID);
        $this->db->where('grievance.is_automail_sent=', 0);
        $this->db->group_by('grievance.user_id');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getNewUsers($day)
    {
        $this->db->select("ud.device_token, ud.user_id, u.full_name,ud.platform", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id');
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.device_token!=', '');
        if ($day == 3) {
            $this->db->where("DATE(u.registered_on)=DATE(NOW() - INTERVAL 3 DAY)");
            $this->db->where('u.daily_notif_count=', '2');
        } else if ($day == 2) {
            $this->db->where("DATE(u.registered_on)=DATE(NOW() - INTERVAL 2 DAY)");
            $this->db->where('u.daily_notif_count=', '1');
        } else {
            $this->db->where("DATE(u.registered_on)=DATE(NOW() - INTERVAL 1 DAY)");
            $this->db->where('u.daily_notif_count=', '0');
        }

        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function updateVerified($table, $updates)
    {
        $ignore = (ENV == 'production') ? '97983 ' : '1';
        $this->db->where_not_in('user_id', $ignore);
        return $this->db->update('users', $updates);
    }

    public function checkreferallist()
    {
        $this->db->select("user_id, referal_code, phone_number, full_name", false);
        $this->db->from('users as u');
        $this->db->like('u.referal_code', '.');
        $this->db->or_like('u.referal_code', '?');
        $this->db->or_like('u.referal_code', '"');
        $this->db->or_like('u.referal_code', '-');
        $this->db->or_like('u.referal_code', '_');
        $this->db->or_like('u.referal_code', ' ');
        $this->db->limit(5000);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        }
        // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function getReferral($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }
        return $token; 
        //test
    }
    public function updaterewardpoints()
    {
        $this->db->select("SQL_CALC_FOUND_ROWS b.fk_user_id, MAX(b.fk_level_id) AS current_level, 
        SUM(c.free_reward_points),(SELECT total_earning + SUM(c.free_reward_points) 
        FROM users WHERE user_id= b.fk_user_id) AS current_points ,a.wallet_status", false);
        $this->db->from('users a');
        $this->db->join('tbl_user_level b', 'a.user_id=b.fk_user_id', 'INNER');
        $this->db->join('tbl_levels c', 'c.pk_level_id = b.fk_level_id', 'INNER');
        $this->db->where('a.wallet_status', 0);
        $this->db->group_by('b.fk_user_id');
        $this->db->limit(200);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }
    public function nextlevel($current_points)
    {
        $resultSet =  $this->db->query("SELECT pk_level_id from tbl_levels where eStatus='active' and $current_points >= start_point AND $current_points <= end_point");
        $res = $resultSet->row_array();
        return $res;
    }

    
    public function districtNotifUsers($districtID)
    {
        $this->db->select("ud.end_point_ern as arn_endpoint, ud.user_id, u.full_name,ud.platform,ud.device_token", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id');
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.end_point_ern!=', '');
        $this->db->where('u.district=', $districtID);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function acNotifUsers($acID)
    {
        $this->db->select("ud.end_point_ern as arn_endpoint, ud.user_id, u.full_name,ud.platform,ud.device_token", false);
        $this->db->from('user_device_details as ud');
        $this->db->join('users as u', 'ud.user_id=u.user_id');
        $this->db->where('ud.login_status=', '1');
        $this->db->where('ud.end_point_ern!=', '');
        $this->db->where('u.ac=', $acID);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getscheduledDistrictWiseTasks()
    {

        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_task_master');
        $this->db->where('ipac_task_master.start_date <= ', $currentDate);
        $this->db->where('ipac_task_master.end_date >= ', $currentDate);
        $this->db->where('is_push_notif_sent=', 0);
        $this->db->where('district!=', 0);
        $this->db->where('ac', 0);
        $this->db->where('is_send_notif=', 1);
        $this->db->where('registeration_no = "0"');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }


    public function getscheduledACWiseTasks()
    {

        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_task_master');
        $this->db->where('ipac_task_master.start_date <= ', $currentDate);
        $this->db->where('ipac_task_master.end_date >= ', $currentDate);
        $this->db->where('is_push_notif_sent=', 0);
        $this->db->where('ac !=', 0);
        $this->db->where('is_send_notif=', 1);
        $this->db->where('registeration_no = "0"');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    public function getReferralUsersTasksList($day_diff = ''){
        
        $this->db->select("iru.referal_user_id,iru.user_id, (SELECT count(detail_id) from user_task_master WHERE user_id = iru.user_id and DATE(added_on) = CURRENT_DATE() - INTERVAL ".$day_diff." DAY ) as completed_task_count, u1.full_name as referal_user, u.full_name as referer_user", false);
        $this->db->from('ipac_referal_user iru');
        $this->db->join('users u','u.user_id = iru.referal_user_id','left');
        $this->db->join('users u1','u1.user_id = iru.user_id','left');
        $this->db->join('user_task_master utm','utm.user_id = iru.user_id','left');
        $this->db->where('DATE(utm.added_on) = CURRENT_DATE() - INTERVAL '.$day_diff.' DAY ');
        $this->db->where('u.user_id NOT IN (select user_id from users WHERE `phone_number` LIKE "12345%") ');
        $this->db->group_by('iru.user_id');
        $this->db->group_by('iru.referal_user_id');
        $query      = $this->db->get();
        $res        = $query->result_array();
        echo $this->db->last_query();
        return $res;
    }

    public function getReferralCreditLogList($day_diff = ''){
        $this->db->select("rcpl.referal_user_id,rcpl.user_id,rcpl.is_credited", false);
        $this->db->from('referal_credit_point_logs rcpl');
        $this->db->where('DATE(rcpl.created_date) = CURRENT_DATE()');
        $query      = $this->db->get();
        $res        = $query->result_array();
        return $res;
    }

    public function getScheduledTasksHavingSpecificUsers()
    {
        $currentDate = date('Y-m-d H:i:s');
        $this->db->select("*", false);
        $this->db->from('ipac_task_master');
        $this->db->where('ipac_task_master.start_date <= ', $currentDate);
        $this->db->where('ipac_task_master.end_date >= ', $currentDate);
        $this->db->where('is_push_notif_sent=', 0);
        $this->db->where('is_send_notif=', 1);
        $this->db->where('registeration_no <> "0"');
        $query = $this->db->get();
        echo $this->db->last_query();
        
        $res = $query->result_array();
        return $res;
    }

    
    public function getUsersByRegisterIds($registrationNos)
    {
        $whereCond      = '';
        if(!empty($registrationNos)){
            $whereCond  = implode('","', explode(',', $registrationNos));
        }
        if($whereCond){
            $this->db->select("ud.end_point_ern as arn_endpoint, ud.user_id, u.full_name,ud.platform,ud.device_token", false);
            $this->db->from('user_device_details as ud');
            $this->db->join('users as u', 'ud.user_id=u.user_id');
            $this->db->where('ud.login_status=', '1');
            $this->db->where('ud.end_point_ern!=', '');
            $this->db->where('u.registeration_no IN ("'. $whereCond.'")');
            $query = $this->db->get();
            echo $this->db->last_query();
            $res = $query->result_array();
            return $res;
        }else{
            return array();
        }
    }
   
}
