<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Offline_task_detail_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }




    /**
     * @function userDetail
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function expenseDetail($params)
    {
        //if user id is set
        if (isset($params) && !empty($params)) {
            $this->db->select("
            u.user_id,
            u.registeration_no,
            u.full_name,
            tm.task_code,
            tm.task_id,
            tm.points,
            tm.task_title,
            tm.task_description,
            tm.task_type,
            tm.action,
            tm.start_date,
            tm.end_date,
            group_concat(tt.media_url separator ',') as media_url,
            group_concat(tt.created_date separator ',') as created_date,
            ", false);
            $this->db->from('ipac_task_master as tm');
            $this->db->join('offline_task_tracking as tt', 'tm.task_id=tt.task_id', 'inner');
            $this->db->join('users as u', 'tt.user_id=u.user_id ', 'inner');
            $this->db->where_in('tt.user_id', $params['id']);
            $this->db->where_in('tt.task_id', $params['task_id']);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    public function userTaskDetail($params)
    {
        //if user id is set
        if (isset($params) && !empty($params)) {
            $this->db->select("remarks", false);
            $this->db->from('user_task_master');
            $this->db->where_in('user_id', $params['id']);
            $this->db->where_in('task_id', $params['task_id']);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }
}
