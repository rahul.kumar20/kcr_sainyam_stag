<?php

class Level_model extends CI_Model
{
    public $finalrole = array();
    public function __construct()
    {
        $this->load->database();
    }

    public function levelListing($limit, $offset, $params)
    {
        try {
            $orderByMap = [
                "added"   => "pk_level_id",
            ];

            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('tbl_levels as u');
            $this->db->limit($limit, $offset);

            if (!empty($params['searchlike'])) { 
                $this->db->group_start();
                $this->db->like('name', $params['searchlike']);
                $this->db->group_end();
            } // IF WHRE CONDITIONS

            /* order by */
            if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) &&
                (isset($params["order"]) && !empty($params["order"]))
            ) {
                $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
            } else { //default Order by on created date desc
                $this->db->order_by("pk_level_id", "ASC");
            }
            /* order by end */

            if (!$query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function achievementListing($limit, $offset, $params)
    {
        try {
            $orderByMap = [
                "added"   => "pk_achievement_id",
            ];

            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('tbl_achievement as u');
            $this->db->limit($limit, $offset);

            if (!empty($params['searchlike'])) { 
                $this->db->group_start();
                $this->db->like('name', $params['searchlike']);
                $this->db->group_end();
            } // IF WHRE CONDITIONS

            /* order by */
            if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) &&
                (isset($params["order"]) && !empty($params["order"]))
            ) {
                $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
            } else { //default Order by on created date desc
                $this->db->order_by("pk_achievement_id", "ASC");
            }
            /* order by end */

            if (!$query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
