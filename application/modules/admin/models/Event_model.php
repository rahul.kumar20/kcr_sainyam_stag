<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Event_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name newsList
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function eventlist($params = [])
    {
        $orderByMap = [
            "added" => "eventDate",
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS a.*", false);
        $this->db->from('ipac_event a');
        /* order by */
        if ((isset($params["field"]) && !empty($params["field"]) && in_array($params["field"], array_keys($orderByMap))) && (isset($params["order"]) && !empty($params["order"]))) {
            $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
        } else { //default Order by on created date desc
            $this->db->order_by("a.eventDate", "DESC");
        }
        /* order by end */

        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(a.eventDate) >= '" . $startDate . "' AND DATE(a.eventDate) <= '" . $endDate . "' ");
        }
        /* setting search */
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('event_title', $params['searchlike']);
            $this->db->group_end();
        }
        /* setting LImit */
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();

        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }

        return $res;
    }
    public function eventDetail($eventId)
    {
        //if user id is set
        if (isset($eventId) && !empty($eventId)) {
            $this->db->select("in.*");
            $this->db->from('ipac_event as in');
            
            $this->db->where_in('event_id', $eventId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

}
