<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Notification_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    /**
     * Fetch all notification listing
     *
     * @Function getNotification
     * @Description get all notification listing
     * @param $params include all filter data
     * @return array
     */
    public function getNotifications($params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS n.*', false);
        $this->db->from('admin_notification as n');
        if (!empty($params['searchlike'])) {
            $this->db->like('n.title', $params['searchlike']);
        }
        if (!empty($params['platform'])) {
            $platform = $params['platform'] + 1;
            $this->db->like('n.platform', $platform);
        }

        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $pushStartDate = date('Y-m-d', strtotime($params['startDate']));
            $pushEndDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_date) >= '" . $pushStartDate . "' AND DATE(created_date) <= '" . $pushEndDate . "' ");
        }
        $this->db->order_by('created_date', 'desc');
        $this->db->limit($params['limit'], $params['offset']);
        $query                   = $this->db->get();
        $respArr                 = [];
        $notiCount               = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        $respArr['totalRows']    = $notiCount;
        $respArr['totalRecords'] = $query->result_array();
        return $respArr;
    }



    /**
     * Fetch all user device token
     *
     * @Function sendNotification
     * @Description fetch all user device token
     * @param $params include filter data
     * @return array
     */
    public function sendNotification($params)
    {
        if (isset($params) && !empty($params)) {
            $this->db->select('full_name,u.user_id,s.device_token,s.platform,s.end_point_ern as arn_endpoint');
            $this->db->from('users as u');
            $this->db->join('user_device_details as s', 'u.user_id=s.user_id', 'inner');
            $this->db->where_in('u.user_id', $params);
            $this->db->where('s.end_point_ern!=', '');
            $this->db->where('s.login_status = 1');
            $query = $this->db->get();
            return $query->result_array();
        } else {
            return false;
        }
    }
}
