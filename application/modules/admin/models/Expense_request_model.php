<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Expense_request_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name requestedUserList
     * @descriptio profile update requested users list
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function requestedUserList($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "uc.campaign_date",
            "task_completed" => "u.task_completed",
            "reward_point" => "u.points_earned"
        ];

        $this->db->select(
            "SQL_CALC_FOUND_ROWS 
        u.user_id,
        u.registeration_no,
        u.registered_on,
        u.full_name,
        u.email_id,
        uc.expence_id,
        uc.campaign_title,
        uc.campaign_date,
        uc.expense_code,
        uc.total_amount,
        uc.status,
        u.gender,
        u.is_active,
        sl.state_name,
        d.district_name,
        cl.college_name,
        GROUP_CONCAT(uci.media_url SEPARATOR ',') as expense_media_url,
        GROUP_CONCAT(uci.particular SEPARATOR ',') as expense_media_particular,
        ",
            false
        );
        $this->db->from('user_campaign_expences as uc');
        $this->db->join('user_campaign_expences_media as uci', 'uc.expence_id=uci.expence_id ', 'left');
        $this->db->join('users as u', 'uc.user_id=u.user_id ', 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');
        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->or_like('uc.campaign_title', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("uc.created_date", "DESC");
        }
        //status filter
        if (! empty($params['status'])) {
            $this->db->where('uc.status', $params['status']);
        } else {
            $this->db->where('u.is_active != 3');
        }
         //UID filter
        if (! empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
          //state filter
        if (! empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
          //district filter
        if (! empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
           //Gender filter
        if (! empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
            //college filter
        if (! empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(uc.campaign_date) >= '" . $startDate . "' AND DATE(uc.campaign_date) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (! empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->group_by('uc.expence_id');
        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
       // echo $this->db->last_query();die;
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total'] = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
        return $res;
    }


 /**
     * @function userDetail
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function expenseDetail($expenseId)
    {
        //if user id is set
        if (isset($expenseId) && !empty($expenseId)) {
            $this->db->select("
            u.user_id,
            u.registeration_no,
            u.full_name,
            uc.expence_id,
            uc.campaign_title,
            uc.campaign_date,
            uc.total_amount,
            uc.status,
            u.email_id,
            u.user_image,
            u.language,
            gender,
            uc.expense_code,
            ", false);
            $this->db->from('user_campaign_expences as uc');
            $this->db->join('users as u', 'uc.user_id=u.user_id ', 'inner');
            $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
            $this->db->join('district as d', 'u.district=d.district_code', 'left');
            $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

            $this->db->where_in('uc.expence_id', $expenseId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }
}
