<?php

class Cms_model extends CI_Model
{

    public $finalrole = array ();

    public function __construct()
    {
        $this->load->database();
    }



    /**
     * @name pagelist
     * @description Used to list out all Page content
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function pagelist($limit, $offset, $params)
    {

        try {
            $orderByMap = [
                "added"   => "created_date",
                "name"    => "name",
                "content" => "content",
            ];

            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('page_master as u');
            $this->db->limit($limit, $offset);

            if (! empty($params['searchlike'])) {//IF WHERE CONDITIONS
                $this->db->group_start();
                $this->db->like('name', $params['searchlike']);
                $this->db->group_end();
            }// IF WHRE CONDITIONS

            /* order by */
            if ((isset($params["field"]) && ! empty($params["field"]) && in_array($params["field"], array_keys($orderByMap)) ) &&
                (isset($params["order"]) && ! empty($params["order"])) ) {
                $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
            } else {//default Order by on created date desc
                $this->db->order_by("created_date", "DESC");
            }
            /* order by end */


            if (! $query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }


    /**
     * @name pagelist
     * @description Used to list out all Page content
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function faqpagelist($limit, $offset, $params)
    {

        try {
            $orderByMap = [
                "added"   => "created_date",
                "faq_title"    => "name",
                "faq_description" => "content",
            ];

            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('faq_management as u');
            $this->db->limit($limit, $offset);

            if (! empty($params['searchlike'])) {//IF WHERE CONDITIONS
                $this->db->group_start();
                $this->db->like('faq_title', $params['searchlike']);
                $this->db->group_end();
            }// IF WHRE CONDITIONS

            /* order by */
            if ((isset($params["field"]) && ! empty($params["field"]) && in_array($params["field"], array_keys($orderByMap)) ) &&
                (isset($params["order"]) && ! empty($params["order"])) ) {
                $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
            } else {//default Order by on created date desc
                $this->db->order_by("created_date", "DESC");
            }
            /* order by end */


            if (! $query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

     /**
     * @name pagelist
     * @description Used to list out all Page content
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function homefaqpagelist($limit, $offset, $params)
    {

        try {
            $orderByMap = [
                "added"   => "created_date",
                "faq_title"    => "name",
                "faq_description" => "content",
            ];

            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('faq_management_home_screen as u');
            $this->db->limit($limit, $offset);

            if (! empty($params['searchlike'])) {//IF WHERE CONDITIONS
                $this->db->group_start();
                $this->db->like('faq_title', $params['searchlike']);
                $this->db->group_end();
            }// IF WHRE CONDITIONS

            /* order by */
            if ((isset($params["field"]) && ! empty($params["field"]) && in_array($params["field"], array_keys($orderByMap)) ) &&
                (isset($params["order"]) && ! empty($params["order"])) ) {
                $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
            } else {//default Order by on created date desc
                $this->db->order_by("created_date", "DESC");
            }
            /* order by end */


            if (! $query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     * @name pagelist
     * @description Used to list out all Page content
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function termConditionList($limit, $offset, $params)
    {

        try {
            $orderByMap = [
                "created_date"   => "created_date",
                "title"    => "name",
                "description" => "content",
            ];

            $this->db->select("SQL_CALC_FOUND_ROWS u.*", false);
            $this->db->from('ipac_term_condition as u');
            $this->db->limit($limit, $offset);

            if (! empty($params['searchlike'])) {//IF WHERE CONDITIONS
                $this->db->group_start();
                $this->db->like('title', $params['searchlike']);
                $this->db->group_end();
            }// IF WHRE CONDITIONS

            /* order by */
            if ((isset($params["field"]) && ! empty($params["field"]) && in_array($params["field"], array_keys($orderByMap)) ) &&
                (isset($params["order"]) && ! empty($params["order"])) ) {
                $this->db->order_by($orderByMap[$params["field"]], $params["order"]);
            } else {//default Order by on created date desc
                $this->db->order_by("created_date", "DESC");
            }
            /* order by end */


            if (! $query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     * @function termConditionDetail
     * @description get term condition detail
     *
     * @param type $termConditionID
     * @return type
     */
    public function termConditionDetail($termConditionID)
    {
        //if user id is set
        if (isset($termConditionID) && !empty($termConditionID)) {
            $this->db->select("tm.*");
            $this->db->from('ipac_term_condition as tm');
         

            $this->db->where_in('id', $termConditionID);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    /**
     * @name pagelist
     * @description Used to list out all Page content
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function termConditionAcceptList($params)
    {
        try {
            
            $this->db->select("SQL_CALC_FOUND_ROWS tcl.*,u.registeration_no,u.full_name", false);
            $this->db->from('ipac_user_term_condition_log as tcl');
            $this->db->join('ipac_term_condition as tc', 'tcl.term_condition_version=tc.version','inner');
            $this->db->join('users as u','u.user_id= tcl.user_id','inner');
            $this->db->limit($params['limit'], $params['offset']);

            if (! empty($params['searchlike'])) {//IF WHERE CONDITIONS
                $this->db->group_start();
                $this->db->like('registeration_no', $params['searchlike']);
                $this->db->group_end();
            }// IF WHRE CONDITIONS

            if (! $query = $this->db->get()) {
                $error = $this->db->error();
                throw new Exception($error['message']);
            }
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

            return $res;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
