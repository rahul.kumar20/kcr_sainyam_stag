<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Form_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


     public function getPcList() {
           
        $this->db->select('*');
        $this->db->from('tbl_pc');

        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {
                echo '<select name="pc_list" required data-live-search="true" class="selectpicker">';
                echo '<option value=" ">Select PC</option>';
                foreach ($data->result_array() as $key => $values) {
                    ?>

                    <option  value="<?php echo $values['iPkPcId']; ?>"><?php echo $values['vPCName']; ?></option>


                    <?php
                }
                echo '</select>';
            } else {
                ?>
                <select required class="selectpicker">
                    <option>No Records</option>
                </select>
                <?php
            }
        }
     }

     public function getAcList($disId,$type) {

           
        $this->db->select('*');
        $this->db->from('tbl_ac');
        
        if(isset($disId) && !empty($disId) && empty($type)){
            
            $this->db->where("district_id",$disId);
            
        }else if (isset($disId) && !empty($disId) && !empty($type)){
            
            $this->db->where("state_id",$disId);
        }
        
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {

                $response =  '<select name="ac_list" required data-live-search="true" onchange="displayNextQues(3)" class="form-control select-form">';
                $response.='<option value=" ">Select AC</option>';
                foreach ($data->result_array() as $key => $values) {
                    

                $response.=  "<option  value=".$values['iPkAcId'].">".$values['vACName']."</option>";
                   
                }
                $response.='</select>';
            } else {
                
                $response='<select required class="selectpicker">
                    <option>No Records</option>
                </select>';
                
            }
            
            return $response;

        }
     }
     
     public function getAcList2($disId) {

           
        $this->db->select('*');
        $this->db->from('tbl_ac');
        
        if(isset($disId) && !empty($disId)){
            
            $this->db->where("district_id",$disId);
            
        }
        
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {

                $response =  '<select name="cur_ac" required data-live-search="true" class="form-control select-form">';
                $response.='<option value="">Select AC</option>';
                foreach ($data->result_array() as $key => $values) {
                    

                $response.=  "<option  value=".$values['iPkAcId'].">".$values['vACName']."</option>";
                   
                }
                $response.='</select>';
            } else {
                
                $response='<select required class="form-control select-form">
                    <option>No Records</option>
                </select>';
                
            }
            
            return $response;

        }
     }

     public function getStateList() {
           
        $this->db->select('*');
        $this->db->from('state_list');

        
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {
                echo '<select name="state_list" required data-live-search="true" class="selectpicker">';
                echo '<option value="">Select State</option>';
                foreach ($data->result_array() as $key => $values) {
                    ?>
                  <option  value="<?php echo $values['state_id']; ?>"><?php echo $values['state_name']; ?></option>
                    <?php
                }
                echo '</select>';
            } else {
                ?>
                <select required class="selectpicker">
                    <option>No Records</option>
                </select>
                <?php
            }
        }
     }


     public function getDistrictList($stateId) {
           
        $this->db->select('*');
        $this->db->from('district');
        
        if(isset($stateId) && !empty($stateId)){
            
            $this->db->where("state_id",$stateId);
        }
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {

                $response = '<select name="vote_dis" required data-live-search="true" class="form-control select-form" onchange=getAcList(this.value,"ac-list","")>';
                $response.='<option value="">Select District</option>';
                foreach ($data->result_array() as $key => $values) {
                   
                 $response.="<option  value=".$values['district_id'].">".$values['district_name']."</option>";

                }
                $response.= "</select>";
            } else {
                
                $response="
                <select required class='form-control'>
                    <option>No Records</option>
                </select>";
            }
        
            
        return $response;    
        }
     }
     
     public function getDistrictList2($stateId) {
           
        $this->db->select('*');
        $this->db->from('district');
        
        if(isset($stateId) && !empty($stateId)){
            
            $this->db->where("state_id",$stateId);
        }
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {

                $response = '<select name="cur_dis" required data-live-search="true" class="form-control select-form" onchange=getAcList2(this.value,"ac-list3")>';
                $response.='<option value="">Select District</option>';
                foreach ($data->result_array() as $key => $values) {
                   
                 $response.="<option  value=".$values['district_id'].">".$values['district_name']."</option>";

                }
                $response.= "</select>";
            } else {
                
                $response="
                <select required class='form-control'>
                    <option>No Records</option>
                </select>";
            }
        
            
        return $response;    
        }
     }

     public function getCollegeList() {
           
        $this->db->select('*');
        $this->db->from('college_list');
        $this->db->limit('1000');

        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {

            if ($data->num_rows() > 0) {

                $response =  '<select name="col_list" required data-live-search="true" class="form-control">';
                $response.= '<option value=" ">Select College</option>';
                foreach ($data->result_array() as $key => $values) {
                    

                $response.="<option  value=".$values['college_id'].">".$values['college_name']."</option>";

                }
                $response."</select>";
            } else {
                
                $response="<select required class='form-control'>
                    <option>No Records</option>
                </select>";
            }
            
            return $response;

        }
     }

     public function fetchPc(){
        $this->db->select('*');
        $this->db->from('tbl_pc');
        $query = $this->db->get();
        $returnArr=array(); 
        if ( FALSE != $query && $query->num_rows () > 0 )
        {
            $returnArr=$query->result_array();
            return $returnArr;
        }else {
            return $returnArr;
        }

     }
	 
	 
 /**
     * @name formList
     * @description Used to filter the form
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function formList($params)
    {

        $sortMap = [
            "registered" => "tm.created_date",
            "point" => "tm.points",
            "startdate" => "tm.created_date",
            "enddate" => "tm.end_date",
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS tm.*", false);
        $this->db->from('forms as tm');
        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('tm.f_title', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("tm.created_date", "DESC");
        }
        //date filter
        if (! empty($params['startDate']) && ! empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(created_date) >= '" . $startDate . "' AND DATE(created_date) <= '" . $endDate . "' ");
        }
      
        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
      
        return $res;
    }

    public function formUserList($formId,$params)
    {

        $sortMap = [
            "registered" => "tm.created_date",
            "point" => "tm.points",
            "startdate" => "tm.created_date",
            "enddate" => "tm.end_date",
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS b.form_id,b.user_id,a.full_name,s.state_name,d.district_name,ac.ac,b.created_at", false);
        $this->db->from('users as a');
        $this->db->join('`user-forms` as b', '(a.user_id = b.user_id)', 'inner', false);
        $this->db->join('state_list as s', 'a.state = s.state_id', 'left');
        $this->db->join('district as d', 'a.district = d.district_id', 'left');
        $this->db->join('tbl_ac as ac', 'a.ac = ac.id', 'left');
        $this->db->where('b.form_id', $formId);
        //search block
        if (! empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('a.full_name', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("a.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("b.created_at", "DESC");
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
      
        return $res;
    }

    public function formSubmitDetails($userId)
    {

        $sortMap = [
            "registered" => "tm.created_date",
            "point" => "tm.points",
            "startdate" => "tm.created_date",
            "enddate" => "tm.end_date",
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS u.`user_id`,a.`user_answer_id`,o.oid,o.description,q.`fid`,q.`qid`,q.`text`,a.`qid`,a.`answer`", false);
        $this->db->from('`user-forms` as u');
        $this->db->join('`answers` as a', 'u.user_id = a.user_answer_id', 'inner');
        $this->db->join('`options` as o', 'a.optionId= o.oid', 'left');
        $this->db->join('`questions` as q', 'a.qid= q.qid AND a.fid=q.fid', 'left');
        $this->db->where('u.user_id', $userId);
       //sort by block
        if ((isset($params["sortfield"]) && ! empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap)) ) &&
            (isset($params["sortby"]) && ! empty($params["sortby"])) ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("a.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("q.qid", "ASC");
        }
       
        $query         = $this->db->get();
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }
      
        return $res;
    }
    
}
