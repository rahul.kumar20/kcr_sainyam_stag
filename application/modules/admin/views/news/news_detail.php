
<?php
$showAction = $permission['action'];
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>

<div class="inner-right-panel">

    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url()?>admin/news">
               Home Content</a></li>
                <li class="breadcrumb-item active">
               Home Content Details</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->

    <div class="fltr-srch-wrap white-wrapper clearfix">
        <div class="white-wrapper">
            <div class="form-item-title clearfix">
                <h3 class="title">Home Content Details</h3>
            </div>

            <div class="form-ele-wrapper clearfix">
                <div class="row">
                    <div class="col-sm-12">
                            <div class="upload-pic-wrapper">
                                
                                <?php if (!empty($news_media)) {
                                    foreach ($news_media as $medVal) { ?>
                               <?php if ($medVal['media_type'] == IMAGE) { ?>
                                    <figure class="show-img">
                                        <a class="example-image-link" href="<?php echo $medVal['media_url']; ?>" data-lightbox="example-set">
                                        <img class="example-image loader" data-lightbox="example-set"  src="<?php echo $medVal['media_url']; ?>"> 
                                        </a>    
                                     </figure>
                               <?php 
                            } else { ?>
                                <figure class="video-img-wrapper">
                                         <video controls class="video-wrap">
                                           <span class="trash-ico"></span>
                                    <source src="<?php echo $medVal['media_url']; ?>" id="video_here">
                                        
                                    </video>
                                    </figure>
                               <?php 
                            }
                        }
                    } ?>


                            </div>
                    </div>
                </div>

                <div class="row">
                   
                    
                    <div class="col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">Title</label>
                            <div class="input-holder">
                               <span class="show-label"><?php echo $news['news_title']; ?></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="admin-label">Description</label>
                            <div class="input-holder">
                                <span class="show-label"><?php echo $news['news_description']; ?> 
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">Status</label>

                            <div class="input-holder">
                                <span class="show-label">
                                    <?php if($news['status'] == 1) {?>
                                    Active 
                                    <?php }else{ ?>
                                    Deactive
                                    <?php } ?>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">Added On</label>

                            <div class="input-holder">
                                <span class="show-label">
                                   <?php echo date("d M Y H:i a", strtotime($news['created_date'])); ?> 
                                </span>
                            </div>

                        </div>
                    </div>

                   <!-- <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">State</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['state_name']) && !empty($news['state_name'])?$news['state_name']:'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div> -->
                   
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">District</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['district_name']) && !empty($news['district_name'])?$news['district_name']:'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div>
                 
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">UID</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['registeration_no']) && !empty($news['registeration_no'])?$news['registeration_no']:'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div>
                   
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">Gender</label>

                            <div class="input-holder">
                                <span class="show-label">
                                  <?php 
                                    if (isset($news['gender']) && !empty($news['gender'])) {
                                        echo ($gender_type[$news['gender']]);
                                    } else {
                                        echo  'Not Available';
                                    }
                                  ?>
                                </span>
                            </div>

                        </div>
                    </div>
                    <!-- <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">College</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['college_name']) && !empty($news['college_name'])?$news['college_name']:'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div> -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">URL</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['url']) && !empty($news['url'])?$news['url']:'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">Type</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['news_category']) && !empty($news['news_category'])?$categoryArr[$news['news_category']]:'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div>
                 <?php if(isset($news['banner_height']) && !empty($news['banner_height']) && $news['news_category']== BANNER){?>
                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group">
                            <label class="admin-label">Banner Height Percentage</label>

                            <div class="input-holder">
                                <span class="show-label">
                                <?php echo (isset($news['banner_height']) && !empty($news['banner_height'])?$news['banner_height'].'%':'Not Available'); ?>
                                </span>
                            </div>

                        </div>
                    </div>
                 <?php }?>
                </div>
            </div>

            <input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
            <input type="hidden" id="districtId" value='<?php echo isset($distict) ? $distict : ''; ?>'>
            <input type="hidden" id="method" value='<?php echo isset($method) ? $method : ''; ?>'>
            <input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . $controller . '/' . $method; ?>'>

            <div class="white-wrapper">

                <?php 
                    if (!empty($this->session->flashdata('alertMsg'))) {
                        if($this->session->flashdata('alertMsg')['type'] == $this->lang->line('success')){
                            ?>
                                <div class="alert alert-success" style="display:block;">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Success!</strong> <?php echo $this->session->flashdata('alertMsg')["text"]; ?>
                                </div>
                            <?php
                        }else{
                            ?>
                                <div class="alert alert-danger" style="display:block;">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Success!</strong> <?php echo $this->session->flashdata('alertMsg')["text"]; ?>
                                </div>
                            <?php
                        }
                    } 
                ?>

                <div class="form-item-title clearfix">
                   <h3 class="title">News Comments</h3>
                </div>

                <div class="fltr-srch-wrap white-wrapper clearfix">
                    <div class="row">
                        <div class="col-lg-2 col-sm-3">
                            <div class="display  col-sm-space">
                               <select class="selectpicker dispLimit">
                                  <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                                  <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                                  <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                                  <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                               </select>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4">
                            <div class="srch-wrap col-sm-space">
                                <button class="srch search-icon" style="cursor:default"></button>
                                <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                                <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by Name" id="searchuser" name="search" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-lg-2 col-sm-2">
                            <?php if (isset($searchlike) && "" != $searchlike) { ?>
                                <div onclick="window.location.href='<?php echo base_url() ?>admin/newsDetail/newsData?data=<?php echo queryStringBuilder("id=" . $news_id); ?>'" id="go_back">Go Back</div>
                            <?php
                            } ?>
                        </div>

                        <div class="col-lg-4 col-sm-4 right-column">
                            <div class="top-opt-wrap text-right">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper2" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="filter-wrap ">
                    <div class="filter_hd clearfix">
                        <div class="pull-left">
                            <h2 class="fltr-heading">Filter</h2>
                        </div>
                        <div class="pull-right">
                            <span class="close flt_cl" data-dismiss="modal">X</span>
                        </div>
                    </div>
                    <div class="inner-filter-wrap">
                        <div class="fltr-field-wrap">
                            <label class="admin-label">UID</label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker filter uid" data-live-search="true" name="uid" id="uid">
                                    <option value="">All</option>
                                    <?php
                                        //if block start
                                        if (isset($uidlist) && !empty($uidlist)) {
                                            //foreach block start
                                            foreach ($uidlist as $value) { ?>
                                                <option value="<?php echo $value['registeration_no']; ?>" <?php if ($uid == $value['registeration_no']) { echo 'selected'; } ?>>
                                                        <?php echo $value['registeration_no']; ?>
                                                </option>
                                                <?php
                                                //fr each block end
                                            }
                                            //if block end 
                                        }
                                    ?>
                               </select>
                            </div>
                        </div>
                        <div class="fltr-field-wrap">
                            <label class="admin-label">Created On</label>
                            <div class="inputfield-wrap">
                                <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                            </div>

                        </div>
                        <div class="fltr-field-wrap">
                            <div class="inputfield-wrap">
                                <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                            </div>
                        </div>

                        <div class="button-wrap text-center">
                            <button type="reset" class="commn-btn cancel" onclick="window.location.href='<?php echo base_url() ?>admin/newsDetail/newsData?data=<?php echo queryStringBuilder("id=" . $news_id); ?>'" id="resetbutton">Reset</button>
                            <button type="submit" class="commn-btn save applyFilter" id="filterbutton" name="filter">Apply</button>
                        </div>
                    </div>
                </div>

                <div class="table-responsive custom-tbl">
                    <!--table div-->
                    <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>User ID</th>
                                <th>Name</th>
                                <th>Comments</th>
                                <th><a href="<?php base_url() ?>admin/newsDetail/newsData?data=<?php echo queryStringBuilder("field=startdate&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_startdate; ?>" > Added Date </a>
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody id="table_tr">
                            <?php
                            //if block start
                            if (isset($newsCommentsList) && !empty($newsCommentsList)) {
                                if ($page > 1) {
                                    $i = (($page * $limit) - $limit) + 1;
                                } else {
                                    $i = 1;
                                }
                                //for each start
                                foreach ($newsCommentsList as $value) {
                                    ?>
                                    <tr id="remove_<?php echo $value['user_id']; ?>">
                                        <td align='left'><span class="serialno"><?php echo $i; ?></span></td>
                                        <td>
                                            <a href="<?php echo base_url() ?>admin/users/detail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>">
                                                <?php echo !empty($value['registeration_no']) ? $value['registeration_no'] : "Not Available"; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url() ?>admin/users/detail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>">
                                                <?php echo !empty($value['full_name']) ? $value['full_name'] : "Not Available"; ?>
                                            </a>
                                        </td>

                                        <td align='left'>
                                            <?php echo !empty($value['user_comment']) ? $value['user_comment'] : "Not Available"; ?>
                                        </td>

                                        <td align='left'>
                                            <?php echo (isset($value['submitted_timestamp']) && !empty($value['submitted_timestamp']) ? mdate(DATE_FORMAT, strtotime($value['submitted_timestamp'])) : 'Not Available') ?>
                                        </td>
                                       
                                        <?php if ($showAction) { ?>
                                            <td class="text-center">
                                                <?php 
                                                    if ($value['comments_count'] > 0) 
                                                    { 
                                                ?>

                                                        <a href="<?php echo base_url() ?>admin/newsDetail/commentThreadList?data=<?php echo queryStringBuilder("id=" . $value['comment_id']); ?>" title="View Thread">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>

                                                <?php
                                                    }
                                                    echo '<span ' . $permission['user_delete'] . '>';
                                                ?>
                                                <a href="javascript:void(0);" class="table_icon" title="Delete"><i class="fa fa-trash" aria-hidden="true" onclick="deleteUser( 'news_comment',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($value['comment_id']); ?>', 'req/change-user-status', 'Do you really want to delete this news comment?' );"></i></a>
                                                <a href="<?php echo base_url() ?>admin/NewsDetail/comment_detail?data=<?php echo queryStringBuilder("id=" . $value['comment_id']); ?>" class="table_icon" title="View"><i class="fa fa-reply" aria-hidden="true"></i></a>
                                                <?php echo "</span>"; ?>
                                            </td>
                                        <?php
                                        } ?>
                                    </tr>

                                    <?php
                                    //for each end
                                    $i++;
                                }
                                 //if block end
                            } else {
                                ?>
                                <tr>
                                    <td colspan="6"><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></td>
                                </tr>
                                <?php 
                            } 
                            ?>
                        </tbody>
                    </table>
                </div>
                
                <div class="pagination_wrap clearfix">
                    <?php echo $link; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Table listing-->

<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>

<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/js/news_detail.js"></script>

<script>
    $('img').load(function(){
       $(this).removeClass('loader');
    });

    $(document).ready(function() {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#startDate').datepicker({
            onRender: function(date) {
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            $('#endDate').val('');
            if (ev.date.valueOf() < checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#endDate')[0].focus();
        }).data('datepicker');
        var checkout = $('#endDate').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


        //on datepicker 2 focus
        $('#datepicker_2').focus(function() {
            if ($('#datepicker_1').val() == '') {
                checkout.hide();
            }
        });
        //prevent typing datepicker's input
        $('#datepicker_2, #datepicker_1').keydown(function(e) {
            e.preventDefault();
            return false;
        });

    });
</script>