<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/news">
                    News Comments</a></li>
            <li class="breadcrumb-item active">
                News Comments Detail</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->

    <!--Filter Section -->
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title"> News Comment Detail</h3>
        </div>
        <?php if (!empty($this->session->flashdata('reply_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('reply_success');
                                            $this->session->unset_userdata('reply_success'); ?>
            </div>
        <?php
        } ?>

        <!-- title and form upper action end-->
        <div class="form-ele-wrapper clearfix">
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($newsCommentsList['full_name']) && !empty($newsCommentsList['full_name']) ? $newsCommentsList['full_name'] : 'N/A'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($newsCommentsList['registeration_no']) && !empty($newsCommentsList['registeration_no']) ? $newsCommentsList['registeration_no'] : 'N/A'; ?>
                            </span>
                        </div>
                    </div>
                </div>
               <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Comment</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($newsCommentsList['user_comment']) && !empty($newsCommentsList['user_comment']) ? $newsCommentsList['user_comment'] : 'N/A'; ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Commented On</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo date("d M Y H:i a", strtotime($newsCommentsList['submitted_timestamp'])); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <form method="POST" id="version_add_form" action="admin/NewsDetail/subcomment_detail?data=<?php echo queryStringBuilder("id=" . $newsCommentsList['comment_id']); ?>">
                <div class="form-group m-t-md">
                    <label class="admin-label">Comment Reply</label>
                    <div class="input-holder">
                        <textarea class="custom-textarea" name="reply" placeholder="Enter Comment Here" id="reply" maxlength="200" required></textarea>
                    </div>
                    <div class="button-wrap text-center">
                        <button type="submit" class="btn btn-primary" id="reply_btn">Reply</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>