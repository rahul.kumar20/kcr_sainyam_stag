<?php
$showAction = $permission['action'];
//pr($newslist);
?>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>
<input type="hidden" id="stateId" value='<?php echo isset($state) ? $state : ''; ?>'>
<input type="hidden" id="districtId" value='<?php echo isset($distict) ? $distict : ''; ?>'>
<input type="hidden" id="collegeId" value='<?php echo isset($college) ? $college : ''; ?>'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home Content</li>
        </ol>
    </div>
    <?php if (!empty($this->session->flashdata('message_success'))) {
    ?>
        <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success');
    $this->session->unset_userdata('message_success'); ?>
        </div>
    <?php
}?>

    <?php if (!empty($this->session->flashdata('message_error'))) {
    ?>
        <div class="alert alert-danger" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
        </div>
    <?php

}?>
    <!--Filter Section -->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by title" id="searchuser" name="search" autocomplete="off">
                </div>

            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) {?>
                    <div class="go_back">Go Back</div>
                <?php

}?>

            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="top-opt-wrap text-right">
                    <ul>
                        <li>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add">
                                <img src="<?php echo base_url() ?>public/images/download.svg">
                            </a>
                        </li>
                        <li>
                            <a href="admin/addNews" title="Add News" id="" class="icon_filter add"><img src="<?php echo base_url() ?>public/images/plus.svg"></a>
                        </li>
                         <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
    <!--Filter Wrapper-->
    <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">

            <div class="fltr-field-wrap">
                <label class="admin-label">Status</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter status" name="status">
                        <option value="1">All</option>
                        <option <?php echo ($status == ACTIVE) ? 'selected' : '' ?> value="1">Active</option>
                        <option <?php echo ($status == BLOCKED) ? 'selected' : '' ?> value="2">Scheduled</option>
                        <option <?php echo ($status == 'INACTIVE') ? 'selected' : '' ?> value="INACTIVE">InActive</option>
                    </select>

                </div>
            </div>

            <div class="fltr-field-wrap">
                <label class="admin-label">Section</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter section" name="section">
                        <option value="">All</option>
                        <option <?php echo ($section == "people_speaks") ? 'selected' : '' ?> value="people_speaks">People Speaks</option>
                        <option <?php echo ($section == "news") ? 'selected' : '' ?> value="news">News</option>
                        <option <?php echo ($section == "gallery") ? 'selected' : '' ?> value="gallery">Gallery</option>
                        <option <?php echo ($section == "stories") ? 'selected' : '' ?> value="stories">Stories</option>
                    </select>
                </div>
            </div>

            <div class="fltr-field-wrap">
                <label class="admin-label">Type</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter categoryType" name="content">
                        <option value="">All</option>
                        <option <?php echo ($homeContentType == GALLERY) ? 'selected' : '' ?> value="1">Gallery</option>
                        <option <?php echo ($homeContentType == ARTICLE) ? 'selected' : '' ?> value="2">Article</option>
                        <option <?php echo ($homeContentType == NEWS) ? 'selected' : '' ?> value="3">News</option>
                        <option <?php echo ($homeContentType == NOTIFICATION) ? 'selected' : '' ?> value="4">Notification</option>
                        <option <?php echo ($homeContentType == BANNER) ? 'selected' : '' ?> value="5">Banner</option>
                        <option <?php echo ($homeContentType == FORM) ? 'selected' : '' ?> value="6">Form</option>
                    </select>
                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">Added Date</label>
                <div class="inputfield-wrap">
                    <input type="text" name="startDate"  data-provide="datepicker" class="form-control startDate start_date" id="startDate" placeholder="From" value="<?php echo isset($startDate) ? $startDate : "" ?>">
                </div>

                

            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input type="text" name="endDate"  data-provide="datepicker"  class="form-control endDate end_date" id="endDate" placeholder="To" value="<?php echo isset($endDate) ? $endDate : "" ?>">
                </div>
            </div>

            <div class="fltr-field-wrap">
                <label class="admin-label">User Engagement</label>
                <div class="inputfield-wrap">
                    <input type="text" name="EffectivestartDate" data-provide="datepicker" class="form-control EffectivestartDate effective_start_date" id="EffectivestartDate" placeholder="From" value="<?php echo isset($EffectivestartDate) ? $EffectivestartDate : "" ?>">
                </div>
            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input type="text" name="EffectiveendDate" data-provide="datepicker"  class="form-control EffectiveendDate effective_end_date" id="EffectiveendDate" placeholder="To" value="<?php echo isset($EffectiveendDate) ? $EffectiveendDate : "" ?>">
                </div>
            </div>


            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Apply</button>
            </div>

        </div>
    </div>

    <!--Table-->
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg');?>
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <div class="white-wrapper">
        <p class="tt-count">Total Content: <?php echo $totalrows ?></p>
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Title</th>
                        <th>
                            Description
                        </th>
                        <th>
                            Add for
                        </th>
                        <th>
                            <a href="<?php base_url()?>admin/news?data=<?php echo queryStringBuilder("field=registered&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_date; ?>">Added On</a>
                        </th>
                        <th>
                            <a href="<?php base_url()?>admin/news?data=<?php echo queryStringBuilder("field=priority&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_priority; ?>">Priority</a>
                        </th>
                        <th>Section</th>
                        <th>Type</th>
                        <th>Likes Count</th>
                        <th>Shares Count</th>
                        <th>comment Count</th>
                        <th>status</th>
                        <?php if ($showAction) {?>
                            <th class="text-center">Action</th>
                        <?php

}?>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
if (isset($newslist) && count($newslist)) {
    if ($page > 1) {
        $i = (($page * $limit) - $limit) + 1;
    } else {
        $i = 1;
    }
    foreach ($newslist as $value) {
        ?>

                            <tr id="remove_<?php echo $value['news_id']; ?>">
                                <td><span class="serialno"><?php echo $i; ?></span></td>
                                <td>

                                    <a href="<?php echo base_url() ?>admin/newsDetail/newsData?data=<?php echo queryStringBuilder("id=" . $value['news_id']); ?>">
                                        <span class="td-text-wrap" title="<?php echo !empty($value['news_title']) ? $value['news_title'] : "Not Available"; ?>
                                     "> <?php echo !empty($value['news_title']) ? $value['news_title'] : "Not Available"; ?>
                                        </span>

                                    </a>
                                </td>

                                <td>
                                    <span class="td-text-wrap" title="<?php echo !empty($value['news_description']) ? strip_tags($value['news_description']) : "Not Available"; ?>
                                "> <?php echo !empty($value['news_description']) ? substr(trim(strip_tags($value['news_description'])), 0, 100) : "Not Available"; ?>
                                    </span>
                                </td>
                                <td>
                                    <?php
if (isset($value['state_name']) && !empty($value['state_name'])) {
            echo $value['state_name'];
        } else if (isset($value['district_name']) && !empty($value['district_name'])) {
            echo $value['district_name'];
        } else if (isset($value['registeration_no']) && !empty($value['registeration_no'])) {
            echo 'Selected Users';
        } else if (isset($value['gender']) && !empty($value['gender'])) {
            echo $gender_type[$value['gender']];
        } else {
            echo 'All(Tamilnadu)';
        }
        ?>
                                </td>
                                <td><?php echo mdate(DATE_FORMAT, strtotime($value['created_date'])); ?></td>
                                <td><?php echo ($value['priority'] == HIGH_PRIORITY) ? 'High Priority' : 'Low Priority'; ?></td>
                                <td><?php echo $sectionArray[$value['home_section']] ?></td>
                                <td><?php echo $cattegoryArray[$value['news_category']] ?></td>
                                <td><?php echo $value['likes_count'] ?></td>
                                <td><?php echo $value['share_count'] ?></td>
                                <td><?php echo $value['comments_count'] ?></td>

                                <td id="status_<?php echo $value['news_id']; ?>"><?php echo ($value['status'] == ACTIVE) ? "Active" : (($value['status'] == 2) ? "Scheduled" : "Inactive"); ?></td>
                                <?php if ($showAction) {?>
                                    <td class="text-center">
                                        <a class="table_icon" title="Edit" href="admin/editNews?data=<?php echo queryStringBuilder("id=" . $value['news_id']); ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                        <?php

            echo '<span ' . $permission['user_delete'] . '>';
            ?>
                                        <a href="javascript:void(0);" class="table_icon" title="Delete"><i class="fa fa-trash" aria-hidden="true" onclick="deleteUser( 'news',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($value['news_id']); ?>', 'req/change-user-status', 'Do you really want to delete this news?' );"></i></a>
                                        <?php echo "</span>"; ?>
                                    </td>
                                <?php
}?>
                            </tr>
                        <?php
$i++;
    }
} else {
    ?>
                        <tr>
                            <td colspan="13" class="text-center">No result found.</td>
                        </tr <?php

}?>
                            </tbody> </table> </div> <div class="pagination_wrap clearfix">
                        <?php echo $link; ?>
        </div>

    </div>
</div>

<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script>
   $(function () {
        var dated = new Date();
        dated.setDate(dated.getDate() - 1);
        $(".start_date").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            // defaultDate: date,
            
        });
        $(".end_date").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            useCurrent: false, //Important! S
        });

        $(".start_date").on("dp.change", function (e) {
            $(".end_date").data("DateTimePicker").minDate(e.date);
        });
        $(".end_date").on("dp.change", function (e) {
            $(".start_date").data("DateTimePicker").maxDate(e.date);
        });
        //effective date
        $(".effective_start_date").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            // defaultDate: date,
            
        });
        $(".effective_end_date").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            useCurrent: false, //Important! S
        });

        $(".effective_start_date").on("dp.change", function (e) {
            $(".effective_end_date").data("DateTimePicker").minDate(e.date);
        });
        $(".effective_end_date").on("dp.change", function (e) {
            $(".effective_start_date").data("DateTimePicker").maxDate(e.date);
        });
    });
</script>


<!-- Modal -->
<div id="mymodal-csv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-alt-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-heading">Upload</h4>
            </div>
            <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit' => 'return doValidateUploadForm();')); ?>

            <div class="modal-body">
                <div class="form-group">
                    <label class="admin-label">Upload File</label>
                    <div class="input-holder">
                        <input type="text" id="upload-btn" class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                        <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                        <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="news_csv_file" id="upload-doc" style="display:none;">

                        <span id="file_error"></span>
                    </div>

                    <span class="download-btn">
                        <a href="<?php echo base_url() ?>public/news_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                    </span>
                </div>
            </div>

            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="commn-btn save" name="csv-upload" value="upload-csv">Upload</button>
                </div>
            </div>

        </div>
        <?php echo form_close(); ?>

    </div>
</div>

<script>
    //$(document).ready(function(){
    //    var state = $("#state").val();
    //    $("#state").val($("#state option:first").val());
    //    $('#state').selectpicker('refresh');
    //    var district = $("#district").val();
    //
    //    if(state && !district)
    //    {
    //        alert("yes");
    //         $("#state").val(state);
    //         $('#state').selectpicker('refresh');
    //    }
    //
    //})

    function doValidateUploadForm() {
        $('#file_error').text('');
        var cnt = 0;

        var allowed_image_type = ['text/csv']

        var landscape_image_data = $('#upload-doc')[0].files

        if (landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length == 0) {

            $('#file_error').text('Please select csv file');
            $('#file_error').css('color', 'red');
            cnt++;
        }
        if (landscape_image_data.length > 0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type) == -1) {

            $('#file_error').text('Please select csv file only');
            $('#file_error').show();
            cnt++;
        }

        if (cnt > 0) {
            return false;
        } else {
            modelHide();
            return true;
        }
    }

    function modelHide() {
        $('#mymodal-csv').modal('hide');
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
    }
</script>