<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/news">Home Content</a></li>
            <li class="breadcrumb-item active">Add Home Content</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'news_add_form')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Home Content</h3>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Content Info</h3>
            </div>
        </div>
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php
        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Title<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title" id="title" value="<?php echo set_value('title'); ?>" placeholder="Enter Title">
                            <?php echo form_error('title', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Title (Tamil)<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title_tn" id="title_tn" value="<?php echo set_value('title'); ?>" placeholder="Enter Title in Tamil" required>
                            <?php echo form_error('title', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Push Notification Title (Optional)</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="notif_title" id="notif_title" placeholder="Enter Push Notification Title">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Push Notification Description</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="notif_desc" id="notif_desc" placeholder="Enter Push Notification Description">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Greeting Message</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="greeting" id="greeting" placeholder="Enter Greeting Message">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <div class="th-checkbox">
                            <input style="display: none;" class="filter-type filled-in" checked="" type="checkbox" name="is_send_notif" id="is_send_notif" value="">
                            <label for="is_send_notif" class="lbl-check"><span></span>Want to send Push Notification?</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm" id="is_event">
                    <div class="form-group">
                        <label class="admin-label">Is Event<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="is_event" id="is_event">
                                <option value="">Select</option>
                                <option value="yes">YES</option>
                                <option value="no">NO</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mapdiv hideinput">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Map<mark class="reject-cross">*</mark></label>
                            <div class="form-field-wrap">
                                <input type="text" placeholder="Search Location" id="location">
                                <label class="alert-danger map_error"></label>
                                <div class="video-wrapper map" id="map_location"></div>
                            </div>
                        </div>
                        <input type='hidden' name="latitude" id='user_lat'>
                        <input type='hidden' name="longitude" id='user_long'>
                    </div>
                    
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Section<mark class="reject-cross">*</mark></label>
                        <select class="filter" id="newsSection" name="section" onchange="changeNewsSection()">
                            <option value="">Select Section</option>
                            <option value="people_speaks">People Speaks</option>
                            <option value="news">News</option>
                            <option value="gallery">Gallery</option>
                            <option value="stories">Stories</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm" id="newsCatDiv" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Type<mark class="reject-cross">*</mark></label>
                        <select class="filter" id="newsCategory" name="type" onchange="changeNewsCategory()">
                            <option value="1">Gallery</option>
                            <option value="2">Article</option>
                            <option value="3" selected>News</option>
                            <option value="4">Notification</option>
                            <option value="5">Banner</option>
                            <option value="6">Form</option>
                            <option value="7">Party News</option>
                            <option value="8">Murasoli</option>
                            <option value="9">Text</option>
                            <option value="10">Video</option>
                            <option value="11">Image</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm" id="onlineHomeForm" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Select Form<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" data-live-search="true" name="onlineform" id="onlineform" onchange="getFormURL2(this.value,'req/form-url')">
                                <option value="">Select Form</option>
                                <?php
                                if (isset($formlist)) {
                                    if (count($formlist) > 0) {
                                        foreach ($formlist as $formkey => $formVal) {
                                            echo '<option value="' . $formVal['fid'] . '">' . ucwords(strtolower($formVal['f_title'])) . '</option>';
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm bannerHeight" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Banner Height Percentage <mark class="reject-cross">*</mark></label>
                        <select class="filter" id="bannerPercentage" name="bannerPercentage">
                            <option value="30">30%</option>
                            <option value="40">40%</option>
                            <option value="50">50%</option>
                            <option value="60">60%</option>
                            <option value="70">70%</option>
                            <option value="80">80%</option>
                            <option value="90">90%</option>
                            <option value="100">100%</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Status<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" id="status" name="status">
                                <option selected value="<?php echo ACTIVE; ?>">Active</option>
                                <option value="<?php echo INACTIVE; ?>">Inactive</option>
                                <option value="2">Schedule</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm live_date_div" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Select Live Date <mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <div class="inputfield-wrap">
                                <input type="text" name="startDate" autocomplete="off" data-provide="datepicker" value="" class="form-control startDate start_date" id="startDate" placeholder="Select Date" value="<?php echo set_value('startDate'); ?>">
                                <?php echo form_error('startDate', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Archive<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="archive">
                                <option value="<?php echo ACTIVE; ?>">Yes</option>
                                <option value="<?php echo INACTIVE; ?>" selected>No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">URL (optional)</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter URL" name="url" id="url" value="<?php echo set_value('url'); ?>">
                            <?php echo form_error('url', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Priority (optional)</label>
                        <select class="selectpicker priorityHome" name="priority" onchange="showPriorityDate(this.value)">
                            <option value="2">No Priority</option>
                            <option value="1">High Priority</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm priorityDate" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Priority End Date (optional)</label>
                        <div class="input-holder">
                            <input type="text" name="priotityEndDate" autocomplete="off" data-provide="datepicker" value="" class="form-control priotityEndDate" id="priotityEndDate" placeholder="Priority End Date" value="<?php echo set_value('priotityEndDate'); ?>">
                            <?php echo form_error('priotityEndDate', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Target PTA</h3>
            </div>
        </div>
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">District (optional)</label>
                        <select class="selectpicker distict" data-live-search="true" name="distict" id="distict">
                            <option value="">Select District</option>
                            <?php
                            if (isset($districtlist) && !empty($districtlist)) {
                                foreach ($districtlist as $districtKey => $districtVal) {
                                    echo '<option value="' . $districtVal['district_id'] . '">' . ucwords(strtolower($districtVal['district_name'])) . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Gender (optional)</label>
                        <select class="selectpicker filter gender" name="gender">
                            <option value="">Select Gender</option>
                            <option value="<?php echo MALE_GENDER ?>">Male</option>
                            <option value="<?php echo FEMALE_GENDER ?>">Female</option>
                            <option value="<?php echo OTHER_GENDER ?>">Other</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">UID (optional)</label>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add commn-btn save">Import</a>
                        <input type='text' name="uid" id="uid" value="">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Other Details</h3>
            </div>
        </div>
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Description<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <textarea class="custom-textarea editor1" name="desc" placeholder="Enter the description" id="desc"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Description (Tamil)<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <textarea class="custom-textarea editor1" name="desc_tn" placeholder="Enter the description in Tamil" id="desc_tn" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm video-gallery" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Upload Video</label>
                        <div class="video-wrapper">
                            <img src="<?php echo base_url() ?>/public/images/video-placeholder.png" class="videoplaceholder video-placeholder">
                            <video controls class="video-wrap">
                                <span class="trash-ico"></span>
                                <source src="" id="video_here">
                            </video>
                        </div>
                        <label class="commn-btn save browse-btn" for="upload-video">Browse</label>
                        <input type="file" name="file[]" class="file_multi_video" accept="video/*" style="display:none;" id="upload-video">
                    </div>
                </div>
                <input type="hidden" value="0" id="imageCnt">
                <input type="hidden" value="0" id="videoCnt">
                <div class="col-sm-6 col-xs-12 m-t-sm image-gallery" style="display:none">
                    <label class="admin-label"><span id="img-label">Upload Image</span></label>
                    <figure class="usr-dtl-pic task-upload-gallery">
                        <img src="public/images/placeholder2.png" id="superadminpic">
                        <span class="sm-loader" style="display:none"><img src="public/images/Gray_circles_rotate.gif"></span>
                        <label class="camera" for="upload-img"><i class="fa fa-camera" aria-hidden="true"></i></label>
                        <input type="file" multiple name="news_image[]" id="upload-img" style="display:none;">
                    </figure>
                    <div class="upload-prod-pic-wrap task-thumbnails">
                        <ul></ul>
                    </div>
                </div>
                <div id="uploadedfile">
                    <input type="file" id="field2" style="display:none" />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm doc-files" style="display:none">
                    <div class="form-group">
                        <label class="label-control">Attachment(PDF only)</label>
                        <input id="attachments" type="file" class="form-control" name="attachment">
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm" id='upload_show' style="display:none">
                    <div class="form-group">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="commn-btn">Upload Image</button>
                        <input type='hidden' value="" name="banner_image" id="banner_image">
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-ele-wrapper clearfix">
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="button" onclick="window.location.href='<?php echo base_url() ?>admin/news'" class="commn-btn cancel">Cancel</button>
                    <button type="submit" class="commn-btn save" id="add_news">Add</button>
                </div>
            </div>
            <!--form ele wrapper end-->
        </div>
        <!--form element wrapper end-->
    </div>
    <!--close form view   -->
    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>
<!--Table listing-->
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/js/news.js"></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAHMFxkH08pIq4lffXwYymVWJ1NFtVbD3c'></script>

<script src="public/js/newmap.js"></script>
<script>
    $("#is_event").change(function() {
    //console.log('hi');
    // $("#mediaType").val("");
    // $("#mediaType").selectpicker("refresh");
    var offlinection = $('#is_event :selected').val(); console.log(offlinection);
    //var offlinection = $(this).attr("value"); 
    if (offlinection != undefined && offlinection == "yes") {
        $(".mapdiv").removeClass("hideinput");
        $(".mapdiv").show();
    } else if (offlinection != undefined && offlinection == "no") {
        $(".mapdiv").addClass("hideinput");
        $(".mapdiv").hide();
    } else {
        $(".mapdiv").show();
        $(".mapdiv").addClass("hideinput");
       
    }
});
</script>
<style>
    .video-wrap {
        display: none;
    }
</style>
<script type="text/javascript">
    function changeNewsSection() {
        $("#newsCatDiv").hide();
        var newsSection = $("#newsSection").val(),
            newsOption = [];

        switch (newsSection) {
            case 'people_speaks':
                newsOption = [{
                    'value': '1',
                    'text': 'Gallery',
                    'selected': false
                }, {
                    'value': '2',
                    'text': 'Article',
                    'selected': false
                }, {
                    'value': '3',
                    'text': 'News',
                    'selected': true
                }, {
                    'value': '5',
                    'text': 'Banner',
                    'selected': false
                }];
                break;
            case 'news':
                newsOption = [{
                    'value': '1',
                    'text': 'Gallery',
                    'selected': false
                }, {
                    'value': '2',
                    'text': 'Article',
                    'selected': false
                }, {
                    'value': '3',
                    'text': 'News',
                    'selected': true
                }, {
                    'value': '5',
                    'text': 'Banner',
                    'selected': false
                }];
                break;
            case 'gallery':
                newsOption = [{
                    'value': '1',
                    'text': 'Gallery',
                    'selected': false
                }];
                break;
            case 'stories':
                newsOption = [{
                    'value': '9',
                    'text': 'Text',
                    'selected': false
                }, {
                    'value': '10',
                    'text': 'Image',
                    'selected': true
                }, {
                    'value': '11',
                    'text': 'Video',
                    'selected': false
                }];
                break;
        }
        if (newsSection != '') {
            $("#newsCatDiv").show();
            $('#newsCategory').empty();
            $.each(newsOption, function(i, item) {
                $('#newsCategory').append($('<option>', {
                    value: item.value,
                    text: item.text,
                    selected: item.selected
                }));
            });
            changeNewsCategory();
        }
    }

    function changeNewsCategory() {
        $('.doc-files, .video-gallery, .image-gallery, .bannerHeight, #onlineHomeForm').hide();
        $('#img-label').html('Upload Image');
        var newsType = $("#newsCategory").val();
        switch (newsType) {
            case '1':
                $(".video-gallery").hide();
                $(".image-gallery").show();
                break;
            case '2':
                $(".video-gallery").show();
                $(".image-gallery").show();
                $("#imageCnt").val(0);
                $(".trash-ico").click();
                break;
            case '3':
                $(".video-gallery").show();
                $(".image-gallery").show();
                break;
            case '4' || '9':
                $(".trash-ico").click();
                $("#imageCnt").val(0);
                break;
            case '5':
                $(".video-gallery").hide();
                $(".image-gallery").show();
                $("#imageCnt").val(0);
                $(".trash-ico").click();
                $(".bannerHeight").show();
                break;
            case '6':
                $("#onlineHomeForm").show();
                $(".trash-ico").click();
                $("#imageCnt").val(0);
                break;
            case '7':
                $(".video-gallery").show();
                $(".image-gallery").show();
                break;
            case '8':
                $(".video-gallery").hide();
                $(".image-gallery").show();
                $('#img-label').html('Upload Thumbnail');
                $('.doc-files').show();
                $("#imageCnt").val(0);
                $(".trash-ico").click();
                break;
            case '10':
                $(".video-gallery").hide();
                $(".image-gallery").show();
                $("#imageCnt").val(0);
                $(".trash-ico").click();
                break;
            case '11':
                $(".video-gallery").show();
                $(".image-gallery").hide();
                $("#imageCnt").val(0);
                $(".trash-ico").click();
                break;
        }
    }

    $("#upload-img").removeAttr('multiple');
    $(document).on("change", ".file_multi_video", function(evt) {
        var fileChooser = document.getElementById('upload-video');
        // var button = document.getElementById(elementID);
        // var results = document.getElementById(erroElement);
        var file = fileChooser.files[0];
        var fileType = file.type;

        var FileSize = file.size / 1024 / 1024; // in MB
        var blob = file; // See step 1 above
        var type = '';
        var imageExt = ['image/png', 'image/gif', 'image/jpeg'];
        var videoExt = ['video/m4v', 'video/avi', 'video/mpg', 'video/mp4', 'video/webm'];
        var fileReader = new FileReader();
        //file type check
        if ($.inArray(fileType, videoExt) == -1) {
            alert(string.video_ext_err);
            $(".videoplaceholder").show();
            $(".video-wrap").hide();
            return false;
        }
        //video size 100 mb
        if (FileSize > 100) {
            alert(string.video_size);
            $(".videoplaceholder").show();
            $(".video-wrap").hide();
            return false;
        }
        fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
            var header = "";
            for (var i = 0; i < arr.length; i++) {
                header += arr[i].toString(16);
            }
            switch (header) {
                case "89504e47":
                    type = "image/png";
                    break;
                case "47494638":
                    type = "image/gif";
                    break;
                case "ffd8ffe0":
                case "ffd8ffe1":
                case "ffd8ffe2":
                case "ffd8ffe3":
                case "ffd8ffe8":
                    type = "image/jpeg";
                    break;
                default:
                    type = "unknown"; // Or you can use the blob.type as fallback
                    break;
            }
            if ($.inArray(type, imageExt) !== -1) {
                alert(string.video_ext_err);
                $(".videoplaceholder").show();
                $(".video-wrap").hide();
                return false;
            }
            // Check the file signature against known types
        };
        fileReader.readAsArrayBuffer(blob);
        var $source = $('#video_here');
        $source[0].src = URL.createObjectURL(this.files[0]);
        $source.parent()[0].load();
    });
    $(".browse-btn").click(function() {
        // alert("hi")
        var a = $("#video_here").attr("src");

        if (a != "null" || a != "unkown") {
            $(".videoplaceholder").hide();
            $(".video-wrap").show();
        }
    });
    // var video = $('#video_here');

    // video.find('source').each(function() {

    //     console.log($(this).attr('src'));

    // })
</script>
<script src="<?php echo base_url() ?>public/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>public/js/cropper.min.js"></script>

<script>
    $(document).ready(function() {
        CKEDITOR.replace('desc');
        CKEDITOR.replace('desc_tn');
        $('#attachments').each(function() {
            $this = $(this);
            $this.on('change', function() {
                var fsize = $this[0].files[0].size,
                    ftype = $this[0].files[0].type,
                    fname = $this[0].files[0].name,
                    fextension = fname.substring(fname.lastIndexOf('.') + 1);
                validExtensions = ["pdf"];

                if ($.inArray(fextension, validExtensions) == -1) {
                    alert("This type of files are not allowed!");
                    this.value = "";
                    return false;
                } else {
                    if (fsize > 3145728) {
                        alert("File size too large! Please upload less than 3MB");
                        this.value = "";
                        return false;
                    }
                    return true;
                }
            });
        });
    });
</script>

<!-- Modal -->
<div id="mymodal-csv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-alt-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-heading">Upload</h4>
            </div>
            <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit' => 'return doValidateUploadForm();')); ?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="admin-label">Upload File</label>
                    <div class="input-holder">
                        <input type="text" id="upload-btn" class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                        <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                        <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="task_csv_file" id="upload-doc" style="display:none;">
                        <span id="file_error"></span>
                    </div>
                    <span class="download-btn">
                        <a href="<?php echo base_url() ?>public/uid_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                    </span>
                </div>
            </div>
            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" class="commn-btn save csvupload" name="csv-upload" value="upload-csv">Upload</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
    function doValidateUploadForm() {
        $('#file_error').text('');
        var cnt = 0;
        var allowed_image_type = ['text/csv']
        var landscape_image_data = $('#upload-doc')[0].files
        if (landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length == 0) {
            $('#file_error').text('Please select csv file');
            $('#file_error').css('color', 'red');
            cnt++;
        }
        if (landscape_image_data.length > 0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type) == -1) {
            $('#file_error').text('Please select csv file only');
            $('#file_error').show();
            cnt++;
        }

        if (cnt > 0) {
            return false;
        } else {
            modelHide();
            return true;
        }
    }

    function modelHide() {
        $('#mymodal-csv').modal('hide');
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
    }
</script>
<!-- portfolio Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body clearfix">
                <div class="ibox float-e-margins">
                    <div class=" rm-brd">
                        <div class="loader_modal" style="display:none"></div>
                        <div class="row">
                            <div class="col-md-6  remove-left">
                                <h4 class="crop-heading"> Image</h4>
                                <div class="image-crop">
                                    <img src="">
                                </div>
                            </div>
                            <div class="col-md-6  remove-right">
                                <h4 class="crop-heading">Preview Image</h4>
                                <div class="img-preview img-preview-sm"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 text-left">
                                <h4>Common method</h4>
                                <p>
                                    You can upload new image to crop container and easily save new cropped image. </p>
                                <div class="modal-bttn-wrap text-center">
                                    <ul>
                                        <li> <label title="Upload image file" for="inputImage" class="commn-btn save">
                                                <input type="file" accept="image/*" name="file" id="inputImage" class="hide">
                                                Upload new image </label> </li>
                                        <li>
                                            <label title="Save Cropped Image" id="save_profile" class="commn-btn save">Save</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var $image = $(".image-crop > img")
        $($image).cropper({
            aspectRatio: 1.618,
            preview: ".img-preview",
            done: function(data) {
                //alert("Image");
                // Output the result data for cropping image.
            }
        });

        var $inputImage = $("#inputImage");
        if (window.FileReader) {
            $inputImage.change(function() {
                var fileReader = new FileReader(),
                    files = this.files,
                    file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function() {
                        $inputImage.val("");
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#download").click(function() {
            // window.open($image.cropper("getDataURL"));

            var byteData = $image.cropper("getDataURL");
            $("#main_preview").attr('src', byteData);
            var byteData_data = byteData.split(';')[1].replace("base64,", "");
            $("#cropped_data").val(byteData_data);
            $('#myModal').modal('hide');

            //                $.post('businesspartner/dashboard/save_portfolio?save_portfolio=yes', {data: byteData}, function(res){
            //                
            //                if(res != ''){
            //
            //                  location.reload(true);
            //
            //                }
            //                else{
            //                  alert('something wrong');
            //                }
            //                });


        });


        $("#save_ads").click(function() {
            var byteData = $image.cropper("getDataURL");
            $("#main_preview").attr('src', byteData);
            var byteData_data = byteData.split(';')[1].replace("base64,", "");
            $("#cropped_data").val(byteData_data);
            $('#myModal').modal('hide');
        });

        $("#crop_store_image").click(function() {
            var byteData = $image.cropper("getDataURL");
            var image_rand_id = localStorage.getItem('image_rand_id');
            console.log('saved =' + image_rand_id);
            $("#main_preview_" + image_rand_id).attr('src', byteData);
            var byteData_data = byteData.split(';')[1].replace("base64,", "");
            $("#cropped_data" + image_rand_id).val(byteData_data);
            $('#storeImageModal').modal('hide');
            $($image).attr('src', '');
            $($image).cropper('destroy');
            // var $image = $(".image-crop > img");
            $($image).cropper({
                aspectRatio: 1.618,
                preview: ".img-preview",
                done: function(data) {
                    //alert("Image");
                    // Output the result data for cropping image.
                }
            });
        });

        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper("rotate", 45);
        });

        $("#rotateRight").click(function() {
            $image.cropper("rotate", -45);
        });

        $("#setDrag").click(function() {
            $image.cropper("setDragMode", "crop");
        });

        $("#validity").keyup(function() {
            if ($(this).val() == '') {
                $("#credit_using").html(0);
            } else {
                $("#credit_using").html($(this).val());
            }
        });
        $("#save_profile").click(function() {
            // window.open($image.cropper("getDataURL"));
            var byteData = $image.cropper("getDataURL");
            byteData = byteData.split(';')[1].replace("base64,", "");
            $('.loader_modal').show();
            $.post(baseUrl + 'admin/AjaxUtil/save_profile_photo', {
                data: byteData,
                csrf_token: csrf_token
            }, function(res) {

                if (res != '') {
                    $('#banner_image').val(res);
                    $('.loader_modal').hide();
                    $('#myModal').modal('hide');
                    $("#superadminpic").attr("src", res);
                    $('.sm-loader').show();
                    hideSmallLoader();
                } else {
                    alert('something wrong');
                }
            });
        });
    });

    $(function() {
        var dated = new Date();
        dated.setDate(dated.getDate() - 1);
        // dated = formatDate(dated)
        $('.priotityEndDate').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            // defaultDate: date,
            minDate: dated
        });
        $(".start_date").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            // defaultDate: date,
            minDate: dated,
        });

        $('#status').change(function() {
            $('.live_date_div').hide();
            $('#startDate').val('');
            if (this.value == '2') {
                $('.live_date_div').show();
            }
        });
    });
</script>

<style>
    .img-preview.img-preview-sm {
        width: 100%;
        max-height: 282px;
        overflow: hidden;
        margin: 0 0 10px 0;
    }

    .crop-heading {
        font-size: 20px;
        font-weight: 500;
        margin: 0 0 10px 0;
    }

    .modal-header {
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
        position: relative;
    }

    .modal-bttn-wrap ul li {
        display: inline-block;
        margin: 11px 11px 11px 0;
    }

    .loader_modal {
        position: absolute;
        top: 33%;
        left: 46%;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #60c6f3;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
    }

    @keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }
</style>