<?php
$showAction = $permission['action'];
?>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">News Comments</li>
        </ol>
    </div>
    <input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
    <input type="hidden" id="method" value='<?php echo isset($method) ? $method : ''; ?>'>

    <input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . $controller . '/' . $method; ?>'>
    <!--Filter Section -->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <?php 
            if (!empty($this->session->flashdata('alertMsg'))) {
                if($this->session->flashdata('alertMsg')['type'] == $this->lang->line('success')){
                    ?>
                        <div class="alert alert-success" style="display:block;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('alertMsg')["text"]; ?>
                        </div>
                    <?php
                }else{
                    ?>
                        <div class="alert alert-danger" style="display:block;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('alertMsg')["text"]; ?>
                        </div>
                    <?php
                }
            } 
        ?>

        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="admin-label">Main News comment</label>
                    <div class="input-holder">
                       <span class="show-label"><?php echo $mainComment; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
   
    <!--Table-->
    <label id="error">
        <div class="alert alert-success" style="display:none" role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"></span>
            </strong>
            <span class="alertText"></span>
        </div>
    </label>
    <div class="white-wrapper">
        
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No </th>
                        <th>UID</th>
                        <th>User Name</th>
                        <th>Comments</th>
                        <th><a href="<?php base_url() ?>admin/newsDetail/commentThreadList?data=<?php echo queryStringBuilder("field=startdate&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_startdate; ?>">Added On</a></th>
                        <th>Action </th>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($newsCommentThreadList) && count($newsCommentThreadList)) {
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($newsCommentThreadList as $value) {
                    ?>
                            <tr>
                                <td><?php echo $i; ?> </td>
                                <td><?php echo $value['registeration_no']; ?> </td>
                                <td><?php echo $value['full_name']; ?> </td>
                                <td><?php 
                                        if(strlen($value['user_comment']) > 50){
                                            echo substr($value['user_comment'], 0,50) . '...';
                                        }else{
                                            echo $value['user_comment'];
                                        }
                                    ?> 
                                </td>
                                <td><?php echo mdate(DATE_FORMAT, strtotime($value['submitted_timestamp'])); ?></td>
                                <?php if ($showAction) { ?>
                                    <td class="text-center">
                                        <?php
                                            echo '<span ' . $permission['user_delete'] . '>';
                                        ?>
                                        <a href="javascript:void(0);" class="table_icon" title="Delete"><i class="fa fa-trash" aria-hidden="true" onclick="deleteUser( 'news_comment',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($value['comment_id']); ?>', 'req/change-user-status', 'Do you really want to delete this news comment?' );"></i></a>
                                        <a href="<?php echo base_url() ?>admin/NewsDetail/subcomment_detail?data=<?php echo queryStringBuilder("id=" . $value['comment_id']); ?>" class="table_icon" title="View"><i class="fa fa-reply" aria-hidden="true"></i></a>
                                         <?php echo "</span>"; ?>
                                    </td>
                                <?php
                                } ?>
                            </tr>
                        <?php
                            $i++;
                        }
                    } else { ?>
                        <td colspan="6"><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></td>
                    <?php
                    } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>
    </div>
</div>

