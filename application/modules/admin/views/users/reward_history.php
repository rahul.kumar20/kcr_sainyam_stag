<?php
$showAction = $permission['action'];
?>
<input type="hidden" id="stateId" value='<?php echo isset($state)?$state:''; ?>'>
   <input type="hidden" id="districtId" value='<?php echo isset($distict)?$distict:''; ?>'>
   <input type="hidden" id="collegeId" value='<?php echo isset($college)?$college:''; ?>'>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
        <li class="breadcrumb-item">Rewards Management</li>
        </ol>
    </div>
    <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
                        <div class="alert alert-success" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                        </div>
        <?php

    } ?>
       
        <?php if (!empty($this->session->flashdata('message_error'))) {
            ?>
                        <div class="alert alert-danger" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
                        </div>
        <?php

    } ?>
    <!--Filter Section -->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by UID" id="searchuser" name="search" autocomplete="off">
                </div>

            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                     <div class="go_back">Go Back</div>
                    <?php
            } ?>

            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="top-opt-wrap text-right">
                    <ul>
                    <li>
                              <a href="javascript:void(0)"data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add">
                              <img src="<?php echo base_url() ?>public/images/download.svg">
                             </a>
                        </li>
                   
                        <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
        <!--Filter Wrapper-->
    <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">

            <div class="fltr-field-wrap">
                <label class="admin-label">Status</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter status" name="status">
                        <option value="">All</option>
                        <option <?php echo ($status == ACTIVE) ? 'selected' : '' ?> value="1">Active</option>
                        <option <?php echo ($status == BLOCKED) ? 'selected' : '' ?> value="2">Blocked</option>
                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">UID</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter uid" data-live-search="true" name="uid" id="uid">
                        <option value="">All</option>
                        <?php 
                        //if block start
                        if (isset($userlist) && !empty($userlist)) {
                               //foreach block start
                            foreach ($userlist as $value) { ?>
                                <option value="<?php echo $value['registeration_no']; ?>" <?php if($uid == $value['registeration_no']) { echo 'selected';}?>><?php echo $value['registeration_no']; ?></option>
                        <?php 
                         //fr each block end
                         }
                        //if block end 
                      }
                    ?>
                    </select>

                </div>
            </div>
            <!-- <div class="fltr-field-wrap">
                <label class="admin-label">State</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter state" id="state" data-live-search="true" name="state" onchange="getDistrictForState('req/getdistrictbystate',this.value,'')">
                        <option value="">All</option>
                                
                        <?php 
                        //if block start
                        if (isset($statelist) && !empty($statelist)) {
                               //foreach block start
                            foreach ($statelist as $stateKey => $stateVal) { ?>
                                <option value="<?php echo $stateVal['state_id']; ?>" <?php if($state ==$stateVal['state_id']) { echo 'selected';}?>><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                        <?php 
                         //fr each block end
                         }
                        //if block end 
                     }
                ?>
                    </select>

                </div>
            </div> -->
            <div class="fltr-field-wrap">
                <label class="admin-label">District</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker distict" data-live-search="true" name="distict" id="distict">
                        <option value="">All</option>
                        <?php 
                        //if block start
                        if (isset($districtlist) && !empty($districtlist)) {
                               //foreach block start
                            foreach ($districtlist as $districtKey => $districtVal) { ?>
                                <option value="<?php echo $districtVal['district_id']; ?>" <?php if($distict ==$districtVal['district_id']) { echo 'selected';}?>><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                        <?php 
                         //fr each block end
                         }
                        //if block end 
                        }
                        ?>
                    </select>
                </div>
            </div>
            <!-- <div class="fltr-field-wrap">
                <label class="admin-label">College</label>
                <div class="commn-select-wrap">
                <select class="selectpicker college" name="college_name" id="collegeDa" >
                     <option value="">All</option>
                                
                     
                    </select>

                </div>
            </div> -->

            <div class="fltr-field-wrap">
                <label class="admin-label">Gender</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter gender" name="gender">
                        <option value="">All</option>
                        <option <?php echo ($gender == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER?>">Male</option>
                        <option <?php echo ($gender == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER?>">Female</option>
                        <option <?php echo ($gender == OTHER_GENDER) ? 'selected' : '' ?> value="<?php echo OTHER_GENDER?>">Other</option>

                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">Registration Date</label>
                <div class="inputfield-wrap">
                    <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                </div>

            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                </div>
            </div>
			 <div class="fltr-field-wrap">
                <label class="admin-label">Task Completed</label>
                <div class="commn-select-wrap">
                     <select class="selectpicker filter taskCompleted" name="task_completed">
                                <option value="">All</option>
                                <option <?php echo ($taskCompleted == '0-10') ? 'selected' : '' ?>  value="0-10">0-10</option>
                                <option <?php echo ($taskCompleted == '10-20') ? 'selected' : '' ?>   value="10-20">10-20</option>
                                <option <?php echo ($taskCompleted == '20-30') ? 'selected' : '' ?>  value="20-30">20-30</option>
                                <option <?php echo ($taskCompleted == '30-100') ? 'selected' : '' ?>   value="30-100">30-100</option>
                     </select>

                </div>
            </div>
            <!-- <div class="fltr-field-wrap">
                <label class="admin-label">Payment Method</label>
                <div class="commn-select-wrap">
                     <select class="selectpicker filter paymentMethod" name="payment_method">
                                <option value="">All</option>
                                <option <?php echo ($paymentMethod == '1') ? 'selected' : '' ?>  value="1">Paytm</option>
                                <option <?php echo ($paymentMethod == '2') ? 'selected' : '' ?>   value="2">UPI Address</option>
                                <option <?php echo ($paymentMethod == '3') ? 'selected' : '' ?>  value="3">Bank Account</option>
                     </select>

                </div>
            </div> -->
            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel" onclick="window.location.href='<?php echo base_url() ?>admin/redards_history'" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyFilterRewardHistory" id="filterbutton" name="filter">Apply</button>
            </div>

        </div>
    </div>

    <!--Table-->
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg'); ?>
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <!-- <a href="admin/payment_history" title="Payment History" class="commn-btn save">Payment History</a> -->

    <div class="white-wrapper big-table">

        <p class="tt-count">Total Users: <?php echo $totalrows ?></p>
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th >S.No</th>
                        <th >UID</th>
                        <th>
                            <a href="<?php base_url() ?>admin/redards_history?data=<?php echo queryStringBuilder("field=name&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_name; ?>">Name</a>
                        </th>
                        <!-- <th  width="180px">
                            <a href="<?php base_url() ?>admin/redards_history?data=<?php echo queryStringBuilder("field=reward_point&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_reward_point; ?>">Total Earning</a>
                        </th> -->
                        <th  width="180px">
                            <a href="<?php base_url() ?>admin/redards_history?data=<?php echo queryStringBuilder("field=task_completed&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_task_completed; ?>">Task Completed</a>
                        </th>
                        <th  width="180px">
                            <a href="<?php base_url() ?>admin/redards_history?data=<?php echo queryStringBuilder("field=reward_point&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_reward_point; ?>">Reward Points</a>
                        </th>
              
                        <th>Last Earning Date</th>
                        <!-- <th>Last Payment Date</th> -->
						<!-- <th>Payment History</th> -->
                        <!-- <th >State</th> -->
                        <th >District</th>
                        <!-- <th  >College</th> -->
                        <th>Gender</th>
                        <th>
                            <a href="<?php base_url() ?>admin/redards_history?data=<?php echo queryStringBuilder("field=registered&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_date; ?>">Registration Date</a>
                        </th>
                     
                        <th >Status</th>
                        <?php if ($showAction) { ?>
                             <th  class="text-center">Action</th>
                            <?php
                    } ?>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($userlist) && count($userlist)) {
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($userlist as $value) {
                            ?>

                             <tr id ="remove_<?php echo $value['user_id']; ?>" >
                                 <td align='left'><span class="serialno"><?php echo $i; ?></span></td>
                                 <td><?php echo !empty($value['registeration_no']) ? $value['registeration_no'] : "Not Available"; ?></td>

                                 <td align='left'>
                                        <?php if ($permission['permissions']['user_detail']) { ?>
                                         <a href="<?php echo base_url() ?>admin/redards_history_detail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>"><?php echo ucfirst($value['full_name']); ?></a>
                                            <?php
                                       } else {
                                        ?>
                                        <?php echo ucfirst($value['full_name']) ; ?>
                                    <?php
                                    } ?>
                                 </td>
                                 <!-- <td><?php echo !empty($value['total_earning']) ? $value['total_earning'] : "0"; ?></td> -->
                                 <td><?php echo !empty($value['task_completed']) ? $value['task_completed'] : "0"; ?></td>
                                 <td><?php echo !empty($value['points_earned']) ? $value['points_earned'] : "0"; ?></td>
                                 <td><?php echo ($value['last_earning_update']!= '0000-00-00 00:00:00' ?mdate(DATE_FORMAT, strtotime($value['last_earning_update'])):'Not available'); ?></td>
                                 <!-- <td><?php echo ($value['last_payment_date']!= '0000-00-00 00:00:00' ?mdate(DATE_FORMAT, strtotime($value['last_payment_date'])):'Not available'); ?></td> -->
                                 <!-- <td><a href="<?php echo base_url() ?>admin/Reward_earned_history/paymentDetail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>" >View</a></td> -->
                                 <!-- <td><?php echo !empty($value['state_name']) ? $value['state_name'] : "Not available"; ?></td> -->
                                 <td><?php echo !empty($value['district_name']) ? $value['district_name'] : "Not available"; ?></td>
                                 <!-- <td><?php echo !empty($value['college_name']) ? $value['college_name'] : "Not available"; ?></td> -->
                                 <td><?php echo !empty($value['gender'])? (($value['gender']== MALE_GENDER) ? 'Male' : "Female"):'Not available'; ?></td>
                                 <td><?php echo mdate(DATE_FORMAT, strtotime($value['registered_on'])); ?></td>
                                 <td id ="status_<?php echo $value['user_id']; ?>"><?php echo ($value['is_active'] == ACTIVE) ? "Active" : "Blocked"; ?></td>

                                <?php if ($showAction) { ?>
                                    <td  class="text-center">
                                        <a href="<?php echo base_url() ?>admin/redards_history_detail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>" class="table_icon" title="View"><i class="fa fa-eye" aria-hidden="true" ></i></a>
                                    </td>
                                <?php
                               } ?>
                             </tr>
                                <?php
                                $i++;
                        }
                    } else {
                        ?>
                         <tr><td colspan="9" class="text-center">No result found.</td></tr
                        <?php
                    } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>

    </div>
</div>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/js/reward.js"></script>

<script>

                                             $( document ).ready( function () {

                                                 var nowTemp = new Date();
                                                 var now = new Date( nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0 );

                                                 var checkin = $( '#startDate' ).datepicker( {
                                                     onRender: function ( date ) {
                                                         return date.valueOf() > now.valueOf() ? 'disabled' : '';
                                                     }
                                                 } ).on( 'changeDate', function ( ev ) {
                                                     $( '#endDate' ).val( '' );
                                                     if ( ev.date.valueOf() < checkout.date.valueOf() ) {
                                                         var newDate = new Date( ev.date )
                                                         newDate.setDate( newDate.getDate() );
                                                         checkout.setValue( newDate );
                                                     }
                                                     checkin.hide();
                                                     $( '#endDate' )[0].focus();
                                                 } ).data( 'datepicker' );
                                                 var checkout = $( '#endDate' ).datepicker( {
                                                     onRender: function ( date ) {
                                                         return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
                                                     }
                                                 } ).on( 'changeDate', function ( ev ) {
                                                     checkout.hide();
                                                 } ).data( 'datepicker' );


                                                 //on datepicker 2 focus
                                                 $( '#datepicker_2' ).focus( function () {
                                                     if ( $( '#datepicker_1' ).val() == '' ) {
                                                         checkout.hide();
                                                     }
                                                 } );
                                                 //prevent typing datepicker's input
                                                 $( '#datepicker_2, #datepicker_1' ).keydown( function ( e ) {
                                                     e.preventDefault();
                                                     return false;
                                                 } );

                                             } );
</script>
<!-- Modal -->
<div id="mymodal-csv" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header modal-alt-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title modal-heading">Upload</h4>
    </div>
    <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit'=>'return doValidateUploadForm();')); ?>

        <div class="modal-body">
        <div class="form-group">
                <label class="admin-label">Upload File</label>
                    <div class="input-holder">
                        <input type="text" id="upload-btn"  class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                            <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                            <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="payment_csv_file" id="upload-doc" style="display:none;">  

                        <span id="file_error"></span>
                    </div>
                    
                <span class="download-btn">
                    <a href="/public/amount_paid_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                </span>
            </div>
        </div>

        <div class="modal-footer">
            <div class="button-wrap">
                    <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="commn-btn save" name="csv-upload" value="upload-csv" id="csv">Upload</button>
            </div>
        </div>

        </div>
    <?php echo form_close(); ?>

  </div>
</div>

<script>

function doValidateUploadForm()
{
    $('#file_error').text('');
    var cnt = 0;

    var allowed_image_type = ['text/csv']

    var landscape_image_data = $('#upload-doc')[0].files

    if(landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length ==0 ) {

        $('#file_error').text('Please select csv file');
        $('#file_error').css('color','red');
        cnt++;
    }
    if(landscape_image_data.length >0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type)==-1) {

        $('#file_error').text('Please select csv file only');
        $('#file_error').show();
        cnt++;
    }

     if (cnt >0){
        return false;
    } else{
        modelHide();
        return true;

    }
}

function modelHide(){
    $('#mymodal-csv').modal('hide');
    $("body").addClass("loader-wrap");
            $('.loader-img').show();
}

</script>