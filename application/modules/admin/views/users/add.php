<?php

?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>

<div class="inner-right-panel">

    <!--breadcrumb wrap-->
<div class="breadcrumb-wrap">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/users">Users</a></li>
        <li class="breadcrumb-item active">Add User</li>
    </ol>
</div>
<!--breadcrumb wrap close-->



    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'user_add_form')); ?>
        <div class="white-wrapper">
            <div class="form-item-title clearfix">
                <h3 class="title">Add User</h3>
            </div>
            
            
                <?php if (!empty($this->session->flashdata('message_success'))) {
                    ?>
                        <div class="alert alert-success" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                        </div>
        <?php
    } ?>
       
        <?php if (!empty($this->session->flashdata('message_error'))) {
            ?>
                        <div class="alert alert-danger" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
                        </div>
        <?php
     } ?>
            
            
            <!-- title and form upper action end-->
            <div class="form-ele-wrapper clearfix">
                <div class="row">
                        <input type = "hidden" value="0" id="imageCnt">
                        <input type = "hidden" value="0" id="videoCnt">
                    <div class="col-sm-12">
                      <figure class="usr-dtl-pic">
                        <a class="example-image-link" id="scandocument" href="assets/images/contract.png" data-lightbox="example-1">
                             <img class="example-image" src="public/images/default.png" id="scancontract-image"></a>   
                              <label class="camera" for="scancontract"><i class="fa fa-camera" aria-hidden="true"></i></label>
                        <input type="file" id="scancontract"  name="user_image" onchange="loadFile(event)" style="display:none;">  
                       </figure>
                            <div class="upload-prod-pic-wrap">
                                <ul>
                                </ul>
                          </div>
                    </div>
                   <input id="password" style="display:none" type="password" name="password" disabled>
                    <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">Name<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                                <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>" autocomplete="off">
                                <?php echo form_error('first_name', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-4 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">Email Id</label>
                            <div class="commn-select-wrap">
                                <input type="text" class="form-control material-control" autocomplete="off" maxlength="255" name="email" id="email" value="<?php echo set_value('email'); ?>" autocomplete="off">
                                <input id="username" style="display:none" type="text" name="email" disabled>

                                <?php echo form_error('email', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                            <span class="error_wrap" id="email_error"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="admin-label">Password<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <input type="password" class="form-control material-control" autocomplete="off" maxlength="50" name="password" id="password" value="<?php echo set_value('password'); ?>" autocomplete="off">
                                <?php echo form_error('password', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                       </div>    
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Facebook Id(optional)</label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off" maxlength="50" name="fbId" id="fbId" value="<?php echo set_value('fbId'); ?>" autocomplete="off">
                                <?php echo form_error('fbId', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="clearfix"></div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="admin-label">Twitter Id(optional)</label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off" maxlength="50" name="twitterId" id="twitterId" value="<?php echo set_value('twitterId'); ?>" autocomplete="off">
                                <?php echo form_error('twitterId', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                       </div>    
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="admin-label">DOB<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <input type="text" readonly class="form-control material-control" autocomplete="off" maxlength="50" name="dob" id="startDate" value="<?php echo set_value('dob'); ?>" autocomplete="off">
                                <?php echo form_error('dob', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                       </div>    
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Phone Number<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off"  name="phoneno" id="phoneno" value="<?php echo set_value('phoneno'); ?>" autocomplete="off" onkeypress="return numeralsOnly(event)" maxlength="10">
                                <?php echo form_error('phoneno', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                     
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Referral Code(optional)</label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off"  name="referalcode" id="referalcode" value="<?php echo set_value('referalcode'); ?>" autocomplete="off" onkeypress="return numeralsOnly(event)" maxlength="10">
                                <?php echo form_error('referalcode', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                          
                     <div class="clearfix"></div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="admin-label">Whatsapp Number<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off"  name="whatsupno" id="whatsupno" value="<?php echo set_value('whatsupno'); ?>" onkeypress="return numeralsOnly(event)" maxlength="10" readonly>
                               
                                 <div class="th-checkbox" style="display:block;">
                                    <input style="display:none;" class="filter-type filled-in" checked type="checkbox" name="filter" id="sameAsPhone" value="">
                                    <label for="sameAsPhone" class="lbl-check"><span></span>Same as mobile number</label>
                                </div>
                            </div>
                            <?php echo form_error('whtsupno', '<label class=" alert-danger">', '</label>'); ?>
                       </div>    
                    </div>
                    <!-- <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Paytm Number(optional)</label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off" name="paytmno" id="paytmno" value="<?php echo set_value('paytmno'); ?>" autocomplete="off" onkeypress="return numeralsOnly(event)" maxlength="10">
                                <?php echo form_error('paytmno', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div> -->
                     
                          <div class="clearfix"></div>
                        <!-- <div class="col-sm-6">
                    <div class="form-group">
                            <label class="admin-label">College Student?<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <select class="selectpicker" name="college_option" id="college_option">
                                     <option value="no">No</option>
                                     <option value="yes">Yes</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="col-sm-6">
                         <div class="form-group">
                            <label class="admin-label" id="stateLabel">Home State<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                            <select class="selectpicker" id="state" data-live-search="true" name="state" onchange="getDistrictForState('req/getdistrictbystate',this.value,'')">
                                <option value="">Select State</option>
                                
                                <?php foreach($state as $stateKey => $stateVal){ ?>
                                      <option value="<?php echo $stateVal['state_id']; ?>" ><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                                <?php } ?>
                                </select>   
                             <?php echo form_error('state', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                       </div>    
                    </div> -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label" id="districtLabel">District<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                                <select class="selectpicker" name="distict" id="distict" data-live-search="true">
                                    <option value="">Select District</option>
                                    <?php foreach($districtlist as $districtKey => $districtVal){ ?>
                                        <option value="<?php echo $districtVal['district_id']; ?>" ><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                                    <?php } ?>    
                                </select>   
                             <?php echo form_error('distict', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-sm-6" id="collegeName" style="display:none">
                        <div class="form-group">
                            <label class="admin-label">College Name<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <select class="selectpicker" name="college_name" data-live-search="true" id="collegeDa" >
                                    
                                    </select>
                               <?php echo form_error('college_name', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Gender<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <select class="selectpicker" name="gender" id="gender">
                                  <option value="">Select Gender</option>
                                  <option  value="<?php echo MALE_GENDER?>">Male</option>
                               <option  value="<?php echo FEMALE_GENDER?>">Female</option>
                               <option  value="<?php echo OTHER_GENDER?>">Other</option>
                                </select>
                             <?php echo form_error('distict', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-sm-6" style="display:none" id="mannualCollege">
                        <div class="form-group">
                            <label class="admin-label">College Name(Mannual)<mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                               <input type="text" class="form-control material-control" autocomplete="off" id="collegeManName" name="collegeManName" id="collegeManName" value="<?php echo set_value('collegeManName'); ?>" autocomplete="off"  maxlength="255">
                                <?php echo form_error('collegeManName', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="form-ele-action-bottom-wrap btns-center clearfix">
                    <div class="button-wrap text-center">
                        <button type="button"  onclick="window.location.href='<?php echo base_url() ?>admin/news'"class="commn-btn cancel">Cancel</button>
                        <button type="submit" class="commn-btn save" id="add_user">Add User</button>
                    </div>
                </div>
                <!--form ele wrapper end-->
            </div>
            <!--form element wrapper end-->

        </div>
        <!--close form view   -->

    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>

<script>
var loadFile = function(event) {
var output = document.getElementById('scancontract-image');
    // var button = document.getElementById(elementID);
    // var results = document.getElementById(erroElement);
    var file = event.target.files[0];
    var fileType = file.type;
    var FileSize = file.size / 1024 / 1024; // in MB
    var blob = file; // See step 1 above
    var type='';
    var imageExt = ['image/png', 'image/jpeg'];
    var fileReader = new FileReader();
   //file type check
    if ($.inArray(fileType, imageExt) == -1) {
        alert(string.video_ext_err);
        return false;
     } 
     fileReader.onloadend = function(e) {
    var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
    var header = "";
    for(var i = 0; i < arr.length; i++) {
     header += arr[i].toString(16);
    }
    //file mime type validation
    switch (header) {
    case "89504e47":
        type = "image/png";
        break;
    case "47494638":
        type = "image/gif";
        break;
    case "ffd8ffe0":
    case "ffd8ffe1":
    case "ffd8ffe2":
    case "ffd8ffe3":
    case "ffd8ffe8":
        type = "image/jpeg";
        break;
    default:
        type = "unknown"; // Or you can use the blob.type as fallback
        break;
}

console.log(type);
//validation for myme type check
if($.inArray(type, imageExt) == -1){
    alert(string.video_ext_err);

    return false;
}
 // Check the file signature against known types

};
fileReader.readAsArrayBuffer(blob);

var scandocument = document.getElementById('scancontract-image');

output.src = URL.createObjectURL(event.target.files[0]);
scandocument.href = URL.createObjectURL(event.target.files[0]);
$("#scandocument").attr("href", scandocument.href)
};

//college student change option 
$('#college_option').change(function() {
   var option = this.value;
   if(option!= undefined && option== 'yes') {
       $('#collegeName').show();
       $('#stateLabel').html('College State <mark class="reject-cross">*</mark>');
       $('#districtLabel').html('College District <mark class="reject-cross">*</mark>');
   } else {
       $('#collegeName').hide();
       $('#stateLabel').html('Home State <mark class="reject-cross">*</mark>');
       $('#districtLabel').html('Home District <mark class="reject-cross">*</mark>');
       $('#collegeDa').val('');
      $( '#collegeDa').selectpicker( 'refresh' );
    }
});

//for whts up number same as  phone number
$('#phoneno').keyup( function(){

    var phone = this.value;
    if($("#sameAsPhone").prop('checked') == true){
        if (phone != undefined || phone != '') {
        $('#whatsupno').val(phone);
    }
  }
   
});
$("#sameAsPhone").click(function(){
    if($("#sameAsPhone").prop('checked') == false){
         $('#whatsupno').val('');
         $('#whatsupno').prop('readonly', false);
    } else {
        $('#whatsupno').prop('readonly', true);
        $('#whatsupno').val($('#phoneno').val());

    }
   })


</script>
<script>

$( document ).ready( function () {

    var nowTemp = new Date();
    var now = new Date( nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0 );

    var checkin = $( '#startDate' ).datepicker( {

        onRender: function ( date ) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        },
    } ).on( 'changeDate', function ( ev ) {
        
    } ).data( 'datepicker' );
    
$('#collegeDa').change( function() {
    var collegeValue = this.value;
    if(collegeValue != undefined && collegeValue == 'others') {
        $('#mannualCollege').show();
    } else {
        $('#collegeManName').val('');
        $('#mannualCollege').hide();
    }
})
} );
</script>