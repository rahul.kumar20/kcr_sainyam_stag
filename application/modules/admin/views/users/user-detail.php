<div class="inner-right-panel">
<?php //pr($profile);?>
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/users">Users</a></li>
            <li class="breadcrumb-item active">User Detail</li>
        </ol>
    </div>

    <!--Filter Section -->
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">User Detail</h3>
        </div>
        <!-- title and form upper action end-->
        <div class="white-wrapper clearfix">
            <div class="row">
                <div class="col-lg-3 col-sm-5 col-xs-12">
                    <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p <?php echo (isset($profile['user_image']) && !empty($profile['user_image'])?'loader':'')?>">
                    <img class="example-image " data-lightbox="example-set"  src="<?php echo (isset($profile['user_image']) && !empty($profile['user_image'])?$profile['user_image']:'public/images/default.png')?>"> 

                        <!-- <div class="profile-pic image-view img-view150" style="background-image:url('<?php echo (isset($profile['user_image']) && !empty($profile['user_image'])?$profile['user_image']:'public/images/default.png')?>');"> -->
                      
                        <!-- </div> -->
                    </div>
                </div>
                <div class="col-lg-9 col-sm-7 col-xs-12">
                    <div class="row">
                        <!--form ele wrapper-->
                        <div class="user-detail-panell">

                            <div class="col-xs-12">
                                <div class="form-group">

                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">UID</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['registeration_no']) && !empty($profile['registeration_no'])? ucfirst($profile['registeration_no']):''); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Name</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['full_name']) && !empty($profile['full_name'])? ucfirst($profile['full_name']):''); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Register Date</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo date("d M Y H:i a", strtotime($profile['registered_on'])); ?></span>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Phone Number</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['phone_number']) && ! empty($profile['phone_number']) ) ? $profile['phone_number'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>-->
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Paytm Number</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['paytm_number']) && ! empty($profile['paytm_number']) ) ? $profile['paytm_number'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <!--<div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Whatsapp Number</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['whatsup_number']) && ! empty($profile['whatsup_number']) ) ? $profile['whatsup_number'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>-->
                            
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Email ID</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['email_id']) && ! empty($profile['email_id']) ) ? $profile['email_id'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Gender</label>
                                    <div class="input-holder">
                                        <span class="text-detail"> <?php echo (isset($profile['gender']) && !empty($profile['gender'])?$GENDER_TYPE[$profile['gender']]:'Not availavle'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">DOB</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['dob']) && ! empty($profile['dob'])  && $profile['dob'] != '0000-00-00') ? date("d M Y", strtotime($profile['dob'])) : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Status</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo $status      = (isset($profile['is_active']) && ! empty($profile['is_active']) ) ? $status_array[$profile['is_active']] : 'Not available'; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Referral Code</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['referal_code']) && ! empty($profile['referal_code']) ) ? $profile['referal_code'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">State</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['state_name']) && ! empty($profile['state_name']) ) ? $profile['state_name'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">District</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['district_name']) && ! empty($profile['district_name']) ) ? $profile['district_name'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <?php if(!empty($profile['college_name'])) {?>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">College</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['college_name']) && ! empty($profile['college_name']) ) ? $profile['college_name'] : "Not available"; ?></span>
                                    </div>
                                </div>
                            </div>
                            <?php }?> -->
                           
                       
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Task Completed</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['task_completed']) && ! empty($profile['task_completed']) ) ? $profile['task_completed'] :0; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Reward Points</label>
                                    <div class="input-holder">
                                        <input type="hidden" name="old_reward_points" id="old_reward_points" value="<?php echo (isset($profile['points_earned']) && ! empty($profile['points_earned']) ) ? $profile['points_earned'] : 0; ?>">
                                        <span class="text-detail" id="reward_points"><?php echo (isset($profile['points_earned']) && ! empty($profile['points_earned']) ) ? $profile['points_earned'] : 0; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Earn Rewards</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['total_earning']) && ! empty($profile['total_earning']) ) ? $profile['total_earning'] :0; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">UPI Address</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['upi_address']) && ! empty($profile['upi_address']) ) ? !empty(decryptString($profile['upi_address']))?decryptString($profile['upi_address']):'Not availavle' :'Not available'; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Account Name</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['account_name']) && ! empty($profile['account_name']) ) ? !empty(decryptString($profile['account_name']))?decryptString($profile['account_name']):'Not availavle' :'Not available'; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Account Number</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['account_number']) && ! empty($profile['account_number']) ) ? !empty(decryptString($profile['account_number']))?decryptString($profile['account_number']):'Not available' :'Not available'; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">IFSC Code</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($profile['ifsc_code']) && ! empty($profile['ifsc_code']) ) ? !empty(decryptString($profile['ifsc_code'])?decryptString($profile['ifsc_code']):'Not availavle') :'Not available'; ?></span>
                                    </div>
                                </div>
                            </div> -->
                            <div class="clearfix"></div>

                           
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="admin-label">Linked Account</label>
                                    <div class="input-holder">
                                        <div class="follow-link">
                                            <ul>
                                            <?php if(!empty($profile['fb_username'])) {?>
                                                 <li> <i class="fa fa-facebook-square" aria-hidden="true"></i> <?php echo $profile['fb_username'];?> </li> 
                                                 <?php }?>
                                                 <?php if(!empty($profile['twitter_username'])) {?>
                                                 <li> <i class="fa fa-twitter-square" aria-hidden="true"></i><span class="sq"><?php echo $profile['twitter_username'];?></span>  </li>
                                                 <?php }?> 
                                                 <!-- <li> <i class="fa fa-whatsapp" aria-hidden="true"></i>  </li>   
                                                  <li> <i class="fa fa-instagram" aria-hidden="true"></i> </li> -->
                                            <ul> 
                                           
                                        </div>   
                                        <?php if(empty($profile['fb_username']) && empty($profile['twitter_username'])) {
                                                echo 'Not available';
                                            } ?> 
                                       
                                    </div>
                                </div>
                            </div>
                           

                        </div>

                        <div class=""> 
                                <div class="col-sm-12">
                                <div class="">
                                <div class="form-group">
                                    <label class="admin-label">ID Image Uploaded</label>
                                    <?php    //id proof  if loop start
                                    if (isset($userIdProof) && !empty($userIdProof)) {
                                            //for each loop start
                                        foreach ($userIdProof as $idProof) {?>
                                    <div class="id-pic-wrapper">
                                        <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p">
                                            <div class="profile-pic image-view img-view150">
                                                    <a  class="example-image-link" href="<?php echo $idProof['image']?>" data-lightbox="example-set"><img class="example-image" data-lightbox="example-set" src="<?php echo $idProof['image']?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                        <?php
                                     //for each loop end
                                        }
                                      // if end
                                    } else {?>
                                    <div class="input-holder">
                                        <span class="text-detail">Not Available</span>
                                    </div>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="clearfix"></div>  
                               </div>    
                        </div>    

                        <!--form ele wrapper end-->

                    </div>
                </div>

            </div>
        </div>
        <!--form element wrapper end-->
    </div>

    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
           <h3 class="title">Add Rewards</h3>
        </div>
        <div class="white-wrapper clearfix">
            <div class="add_rewards_div row">                             
                <div class="col-sm-6 col-xs-6 m-t-sm">
                    <div class="form-group">
                        <div class="input-holder">
                            <label class="admin-label">Title</label>
                            <input type="text" name="rewards_title" id="rewards_title" value="" class="form-control material-control reward-field" />
                        </div>
                        <div id="rewards_title_err" class="alert-danger"></div>
                    </div>
                    <div class="clearfix"></div> 
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm">
                    <div class="form-group">
                        <div class="input-holder">
                            <label class="admin-label">Description</label>
                            <textarea name="rewards_desc" id="rewards_desc" class="form-control material-control reward-field" ></textarea>
                        </div>
                        <div id="rewards_desc_err" class="alert-danger"></div>
                    </div>
                    <div class="clearfix"></div> 
                </div>

                <div class="col-sm-6 col-xs-6 m-t-sm">
                    <div class="form-group">
                        <div class="input-holder">
                            <label class="admin-label">Reward Points</label>
                            <input type="text" name="rewards" id="rewards" value="" class="add-rewards-input reward-field" />
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id?>" class="add-rewards form-control material-control" />
                            <button type="button" class="commn-btn save add-rewards-btn" id="add_reward">Add</button>
                        </div>
                        <div id="rewards_err" class="alert-danger"></div>
                    </div>
                    <div class="clearfix"></div> 
                    <div class="rewards_msg">
                        <div class="message" style="display:none;">
                            <button type="button" class="close_msg" data-dismiss="alert">&times;</button>
                            <span id="ret_message"></span>
                        </div>
                    </div>
                </div> 
            </div> 
        </div>                         
    </div>
    
    <!--form element wrapper end-->
</div>

<script>
    $('img').load(function(){
       $('.form-profile-pic-wrapper').removeClass('loader');
    });


    $(document).ready(function() {

        /*$(".reward-field").keypress(function (e) {
            checkRewardValid();
        });*/

        $("#add_reward").click(function () {
            var checkValid      = checkRewardValid();
            
            var rewards         = $('#rewards').val();
            var user_id         = $('#user_id').val();
            if(checkValid != 1){
                return false;
            }
            if(rewards == "" || typeof rewards == "undefined"){
                return false;
            }
            $.ajax({
                url: baseUrl + "admin/AddReward",
                type: "POST",
                data: {
                    rewards         : rewards,
                    rewards_desc    : $('#rewards_desc').val(),
                    rewards_title   : $('#rewards_title').val(),
                    user_id         : user_id
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if(res.success == 1){
                        $('.message').addClass('message-success');
                        $('.message-success').show();
                        $('.rewards_msg #ret_message').html(res.message);
                        var old_points = $('#old_reward_points').val();
                        var new_points  = (parseInt(old_points) + parseInt(rewards));
                        $('#reward_points').html(new_points);   
                        $('#old_reward_points').val(new_points);   
                        $('#rewards').val('');           
                        $('#rewards_desc').val('');           
                        $('#rewards_title').val('');           
                    }else{
                        $('.message').addClass('message-danger');
                        $('.message-danger').show();
                        $('.rewards_msg #ret_message').html(res.message);
                    }     
                    
                },
            });
        });

        $(".close_msg").click(function () {
            $('.message').removeClass('alert-success');
            $('.message').removeClass('alert-danger');
            $('.message').hide();
        });

    });

    function checkRewardValid(){
        var rewards             = $('#rewards').val();
        var rewards_desc        = $('#rewards_desc').val();
        var rewards_title       = $('#rewards_title').val();
        var is_valid            = 1;
        var intRegex            = /^\d+$/;
        if(rewards == "" || typeof rewards == "undefined"){
            is_valid            = 0;
            $('#rewards_err').html('Please enter rewards');
        }else if(!intRegex.test(rewards)){
            is_valid            = 0;
            $('#rewards_err').html('Please enter only digits');
        }else{
            $('#rewards_err').html('');
        }

        if(rewards_desc == "" || typeof rewards_desc == "undefined"){
            is_valid            = 0;
            $('#rewards_desc_err').html('Please enter description');
        }else{
            $('#rewards_desc_err').html('');
        }

        if(rewards_title == "" || typeof rewards_title == "undefined"){
            is_valid            = 0;
            $('#rewards_title_err').html('Please enter title');
        }else{
            $('#rewards_title_err').html('');
        }

        return is_valid;
    }

</script>