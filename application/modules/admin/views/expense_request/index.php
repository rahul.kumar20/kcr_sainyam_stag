<?php
$showAction = $permission['action'];
//pr($userlist);
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="stateId" value='<?php echo isset($state)?$state:''; ?>'>
   <input type="hidden" id="districtId" value='<?php echo isset($distict)?$distict:''; ?>'>
   <input type="hidden" id="collegeId" value='<?php echo isset($college)?$college:''; ?>'>

<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Expense Request Users</li>
        </ol>
    </div>
    <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
                        <div class="alert alert-success" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                        </div>
        <?php

    } ?>
       
        <?php if (!empty($this->session->flashdata('message_error'))) {
            ?>
                        <div class="alert alert-danger" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
                        </div>
        <?php

    } ?>
    <!--Filter Section -->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by UID, campaign title" id="searchuser" name="search" autocomplete="off">
                </div>

            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                     <div class="go_back">Go Back</div>
                    <?php
            } ?>

            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="top-opt-wrap text-right">
                    <ul>
                    
                   
                        <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
        <!--Filter Wrapper-->
    <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">

            <div class="fltr-field-wrap">
                <label class="admin-label">Status</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter status" name="status">
                        <option value="">All</option>
                        <option <?php echo ($status == REQUESTED) ? 'selected' : '' ?> value="1">Requested</option>
                        <option <?php echo ($status == EXPENSE_APPROVED) ? 'selected' : '' ?> value="2">Approved</option>
                        <option <?php echo ($status == EXPENSE_REJECTED) ? 'selected' : '' ?> value="3">Rejected</option>

                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">UID</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter uid" data-live-search="true" name="uid" id="uid">
                        <option value="">All</option>
                        <?php 
                        //if block start
                        if (isset($userlist) && !empty($userlist)) {
                               //foreach block start
                            foreach ($userlist as $value) { ?>
                                <option value="<?php echo $value['registeration_no']; ?>" <?php if($uid == $value['registeration_no']) { echo 'selected';}?>><?php echo $value['registeration_no']; ?></option>
                        <?php 
                         //fr each block end
                         }
                        //if block end 
                      }
                    ?>
                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">State</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter state" id="state" data-live-search="true" name="state" onchange="getDistrictForState('req/getdistrictbystate',this.value,'')">
                        <option value="">All</option>
                                
                        <?php 
                        //if block start
                        if (isset($statelist) && !empty($statelist)) {
                               //foreach block start
                            foreach ($statelist as $stateKey => $stateVal) { ?>
                                <option value="<?php echo $stateVal['state_id']; ?>" <?php if($state ==$stateVal['state_id']) { echo 'selected';}?>><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                        <?php 
                         //fr each block end
                         }
                        //if block end 
                     }
                ?>
                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">District</label>
                <div class="commn-select-wrap">
                <select class="selectpicker distict" data-live-search="true" name="distict" id="distict" onchange="getcollegeForDistrict('req/getcollegebydistrict',this.value,'')">
                   <option value="">All</option>
                   
                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">College</label>
                <div class="commn-select-wrap">
                <select class="selectpicker college" name="college_name" id="collegeDa" >
                     <option value="">All</option>
                                
                     
                    </select>

                </div>
            </div>

            <div class="fltr-field-wrap">
                <label class="admin-label">Gender</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter gender" name="gender">
                        <option value="">All</option>
                        <option <?php echo ($gender == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER?>">Male</option>
                        <option <?php echo ($gender == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER?>">Female</option>
                        <option <?php echo ($gender == OTHER_GENDER) ? 'selected' : '' ?> value="<?php echo OTHER_GENDER?>">Other</option>

                    </select>

                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">Campaign Date</label>
                <div class="inputfield-wrap">
                    <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                </div>

            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                </div>
            </div>
			
		
            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Apply</button>
            </div>

        </div>
    </div>

    <!--Table-->
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg'); ?>
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <div class="white-wrapper">
        <p class="tt-count">Total Request: <?php echo $totalrows ?></p>
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th >S.No</th>
                        <th>Expense Id</th>
                        <th>Campaign Title</th>

                        <th >UID</th>
                        <th >
                            <a href="<?php base_url() ?>admin/campaign_expense_request?data=<?php echo queryStringBuilder("field=name&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_name; ?>">Name</a>
                        </th>
                        <th>Amount</th>

                        <th>
                            <a href="<?php base_url() ?>admin/campaign_expense_request?data=<?php echo queryStringBuilder("field=registered&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_date; ?>">Campaign Date</a>
                        </th>
                      
                        <th >status</th>
                        <?php if ($showAction) { ?>
                             <th class="text-center">Action</th>
                            <?php
                    } ?>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($userlist) && count($userlist)) {
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($userlist as $value) {
                            ?>

                             <tr id ="remove_<?php echo $value['user_id']; ?>" >
                                 <td align='left'><span class="serialno"><?php echo $i; ?></span></td>
                                 <td><?php echo !empty($value['expense_code']) ? $value['expense_code'] : "Not availavle"; ?></td>

                                 <td>
                                 <a href="<?php echo base_url() ?>admin/expenseDetail?data=<?php echo queryStringBuilder("id=" . $value['expence_id']); ?>">    
                                 <?php echo !empty($value['campaign_title']) ? $value['campaign_title'] : "Not Available"; ?>
                                 </a>
                                </td>

                                 <td><?php echo !empty($value['registeration_no']) ? $value['registeration_no'] : "Not Available"; ?></td>

                                 <td align='left'>
                                        <?php if ($permission['permissions']['user_detail']) { ?>
                                         <a href="<?php echo base_url() ?>admin/Profile_update_request/userDetail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>"><?php echo ucfirst($value['full_name']); ?></a>
                                            <?php
                                       } else {
                                        ?>
                                        <?php echo ucfirst($value['full_name']) ; ?>
                                    <?php
                                    } ?>
                                 </td>
                                 <td><?php echo !empty($value['total_amount']) ? $value['total_amount'] : "0"; ?></td>
                                 <td><?php echo mdate(DATE_FORMAT, strtotime($value['campaign_date'])); ?></td>

                                 <td id ="status_<?php echo $value['user_id']; ?>"><?php echo $expenseStatus[$value['status']]; ?></td>
                                     <td  class="text-center">
                                         <?php if($value['status'] == REQUESTED) {?>
                                            <a href="javascript:void(0);" class="table_icon accept-check" title="Accept"><i class="fa fa-check" title="Accept" aria-hidden="true" onclick="acceptuser( 'expenseaccept',<?php echo PROFILE_ACCEPT; ?>, '<?php echo encryptDecrypt($value['expence_id']); ?>', 'req/accept-user-request', 'Do you want to accept expense request of user?' );"></i></a>
                                            <a href="javascript:void(0);" class="table_icon reject-cross" title="Reject"><i class="fa fa-times" title="Accept" aria-hidden="true" onclick="acceptuser( 'expensereject',<?php echo PROFILE_REJECT; ?>, '<?php echo encryptDecrypt($value['expence_id']); ?>', 'req/accept-user-request', 'Do you want to reject expense  request of user?' );"></i></a>
                                         <?php }else {?>
                                            --
                                         <?php }?>
                                     </td>
                                   
                             </tr>
                                <?php
                                $i++;
                        }
                    } else {
                        ?>
                         <tr><td colspan="11" class="text-center"><?php echo $this->lang->line('NO_RECORD_FOUND');?></td></tr
                        <?php
                    } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>

    </div>
</div>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script>

                                             $( document ).ready( function () {

                                                 var nowTemp = new Date();
                                                 var now = new Date( nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0 );

                                                 var checkin = $( '#startDate' ).datepicker( {
                                                     onRender: function ( date ) {
                                                         return date.valueOf() > now.valueOf() ? 'disabled' : '';
                                                     }
                                                 } ).on( 'changeDate', function ( ev ) {
                                                     $( '#endDate' ).val( '' );
                                                     if ( ev.date.valueOf() < checkout.date.valueOf() ) {
                                                         var newDate = new Date( ev.date )
                                                         newDate.setDate( newDate.getDate() );
                                                         checkout.setValue( newDate );
                                                     }
                                                     checkin.hide();
                                                     $( '#endDate' )[0].focus();
                                                 } ).data( 'datepicker' );
                                                 var checkout = $( '#endDate' ).datepicker( {
                                                     onRender: function ( date ) {
                                                         return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
                                                     }
                                                 } ).on( 'changeDate', function ( ev ) {
                                                     checkout.hide();
                                                 } ).data( 'datepicker' );


                                                 //on datepicker 2 focus
                                                 $( '#datepicker_2' ).focus( function () {
                                                     if ( $( '#datepicker_1' ).val() == '' ) {
                                                         checkout.hide();
                                                     }
                                                 } );
                                                 //prevent typing datepicker's input
                                                 $( '#datepicker_2, #datepicker_1' ).keydown( function ( e ) {
                                                     e.preventDefault();
                                                     return false;
                                                 } );

                                             } );
</script>
 <!-- Modal -->
    <!-- Modal -->
    <div id="myModal-accept" class="modal fade" role="dialog">
        <input type="hidden" id="userid" name="userid" value="">
        <input type="hidden" id="udstatus" name="udstatus" value="">
        <div class="modal-dialog modal-custom">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-alt-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-heading">Accept Request</h4>
                </div>
                <div class="modal-body" >
                    <p class="modal-para">Are you sure want to block this user?</p>
                </div>
                <input type="hidden" id="new_status" name="new_status">
                <input type="hidden" id="new_id" name="new_id">
                <input type="hidden" id="new_url" name="new_url">
                <input type="hidden" id="new_msg" name="new_msg">
                <input type="hidden" id="for" name="for">


                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                        <button type="button" id="action" class="commn-btn save" onclick="changeStatusToBlock($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val())">Accept</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

                                            <script>
                                                /**
 * @name deleteUser
 * @description This method is used to show delete user modal.
 *
 */

function acceptuser( type, status, id, url, msg ) {

$( '#new_status' ).val( status );
$( '#new_id' ).val( id );
$( '#new_url' ).val( url );
$( '.modal-para' ).text( msg );
$( '#for' ).val( type );
if (status == 3) {
    $( '.modal-title' ).text( 'Reject Request' );
    $('#action').text('Reject')
} else {
    $( '.modal-title' ).text( 'Accept Request' );
    $('#action').text('Accept')
}

$( '#myModal-accept' ).modal( 'show' );
}
                                                </script>