<div class="inner-right-panel">
<?php //pr($expenseDetail);?>
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/campaign_expense_request">Expense Request</a></li>
            <li class="breadcrumb-item active">Expense Detail</li>
        </ol>
    </div>

    <!--Filter Section -->
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Expense Detail</h3>
        </div>
        <!-- title and form upper action end-->
        <div class="white-wrapper clearfix">

                <div class="row">
                    <div class="col-sm-6">
                    <div class="row">




<div class="col-sm-4">
    <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p <?php echo (isset($expenseDetail['new_image']) && !empty($expenseDetail['new_image'])?'loader':'')?>">
    <img class="example-image " data-lightbox="example-set"  src="<?php echo (isset($expenseDetail['user_image']) && !empty($expenseDetail['user_image'])?$expenseDetail['user_image']:'public/images/default.png')?>"> 

        <!-- <div class="profile-pic image-view img-view150" style="background-image:url('<?php echo (isset($expenseDetail['user_image']) && !empty($expenseDetail['user_image'])?$expenseDetail['user_image']:'public/images/default.png')?>');"> -->
      
        <!-- </div> -->
    </div>
</div>

<div class="col-sm-8">
                        <div class="form-group">
                            <label class="admin-label">Name</label>
                            <div class="input-holder">
                                <span class="text-detail"><?php echo (isset($expenseDetail['full_name']) && !empty($expenseDetail['full_name'])? ucfirst($expenseDetail['full_name']):'N/A'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                    <label class="admin-label">Expense Code</label>
                    <div class="input-holder">
                        <span class="text-detail"><?php echo (isset($expenseDetail['expense_code']) && !empty($expenseDetail['expense_code'])? ucfirst($expenseDetail['expense_code']):'N/A'); ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="admin-label">Campaign Title</label>
                    <div class="input-holder">
                        <span class="text-detail"><?php echo (isset($expenseDetail['campaign_title']) && !empty($expenseDetail['campaign_title'])? ucfirst($expenseDetail['campaign_title']):'N/A'); ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="admin-label">Total Amount</label>
                    <div class="input-holder">
                        <span class="text-detail"><?php echo (isset($expenseDetail['total_amount']) && !empty($expenseDetail['total_amount'])? ucfirst($expenseDetail['total_amount']):'N/A'); ?></span>
                    </div>
                </div>

              <div class="form-group">
                    <label class="admin-label">Requested Date</label>
                    <div class="input-holder">
                        <span class="text-detail"><?php echo mdate('Y-m-d', strtotime($expenseDetail['campaign_date'])); ?></span>
                    </div>
                </div>

             
</div>


</div>

                    </div>
                    <div class="col-sm-6">
                    <div class="row">
           
                <div class="col-sm-8">

                                 <div class="form-group">
                                    <label class="admin-label">UID</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($expenseDetail['registeration_no']) && !empty($expenseDetail['registeration_no'])? ucfirst($expenseDetail['registeration_no']):'N/A'); ?></span>
                                    </div>
                                </div>

                                  <div class="form-group">
                                    <label class="admin-label">Campaign Date</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($expenseDetail['campaign_date']) && !empty($expenseDetail['campaign_date'])? date('Y-m-d', strtotime($expenseDetail['campaign_date'])):'N/A'); ?></span>
                                    </div>
                                  </div>

                                <div class="form-group">
                                    <label class="admin-label">Email ID</label>
                                    <div class="input-holder">
                                        <span class="text-detail"><?php echo (isset($expenseDetail['email_id']) && !empty($expenseDetail['email_id'])? ucfirst($expenseDetail['email_id']):'N/A'); ?></span>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="admin-label">Expense Input</label>
                                    <?php    //id proof  if loop start

                                    if (isset($expenseMedia) && !empty($expenseMedia)) {
                                            //for each loop start
                                        foreach ($expenseMedia as $idProof) {?>
                                    <div class="id-pic-wrapper">
                                        <?php if(isset( $idProof['media_url']) && !empty( $idProof['media_url'])){
                                              $mediaUrl = explode(',',$idProof['media_url']);
                                              foreach($mediaUrl as $imgVal) {
                                            ?>
                                        <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p">
                                            <div class="profile-pic image-view img-view150">
                                                    <a  class="example-image-link" href="<?php echo $imgVal?>" data-lightbox="example-set"><img class="example-image" data-lightbox="example-set" src="<?php echo $imgVal?>"></a>
                                                   
                                            </div>
                                        </div>
                                              <?php }?>
                                        <?php }?>
                                        <span><strong>Particular:  </strong><?php echo $idProof['particular']?></span><br>
                                        <span><strong>Amount:  </strong><?php echo $idProof['amount']?></span></br>
                                        <span><strong>Date:  </strong><?php echo mdate('Y-m-d', strtotime($idProof['created_date']))?></span>
                                    </div>
                                        <?php
                                     //for each loop end
                                        }
                                      // if end
                                    } else {?>
                                    <div class="input-holder">
                                        <span class="text-detail">Not Available</span>
                                    </div>
                                    <?php }?>
                                </div>


                    </div>

                </div> 
                    </div>
                </div>

                        <!--form ele wrapper end-->
                        <?php if($expenseDetail['status'] == REQUESTED){?>
                        <div class="form-ele-action-bottom-wrap btns-center clearfix">
                                    <div class="button-wrap text-center">            
                                        <a href="javascript:void(0);"  class="table_icon accept-check commn-btn cancel" title="Reject" onclick="acceptuser( 'expensereject',<?php echo PROFILE_REJECT; ?>, '<?php echo encryptDecrypt($expenseDetail['expence_id']); ?>', 'req/accept-user-request', 'Do you want to reject expense request of user?' );"><i class="" aria-hidden="true" >Reject Request</i></a>
                                        <a href="javascript:void(0);" class="table_icon accept-check commn-btn save " title="Accept" onclick="acceptuser( 'expenseaccept',<?php echo PROFILE_ACCEPT; ?>, '<?php echo encryptDecrypt($expenseDetail['expence_id']); ?>', 'req/accept-user-request', 'Do you want to accept expense request of user?' );"><i class="" aria-hidden="true"  >Accept Request</i></a>

                                    </div>
                                 
                          </div>
                        <?php }?>
               
        </div>
        <!--form element wrapper end-->
    </div>

    
    <!--form element wrapper end-->
</div>
<script src="<?php echo base_url() ?>public/js/profile_request.js"></script>


    <!-- Modal -->
    <div id="myModal-accept" class="modal fade" role="dialog">
        <input type="hidden" id="userid" name="userid" value="">
        <input type="hidden" id="udstatus" name="udstatus" value="">
        <div class="modal-dialog modal-custom">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-alt-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-heading">Accept Request</h4>
                </div>
                <div class="modal-body" >
                    <p class="modal-para">Are you sure want to block this user?</p>
                </div>
                <input type="hidden" id="new_status" name="new_status">
                <input type="hidden" id="new_id" name="new_id">
                <input type="hidden" id="new_url" name="new_url">
                <input type="hidden" id="new_msg" name="new_msg">
                <input type="hidden" id="for" name="for">


                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                        <button type="button" id="action" class="commn-btn save" onclick="changeStatusToAccept($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val())">Accept</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
