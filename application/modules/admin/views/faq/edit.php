
<div class="inner-right-panel">

    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/faq">Faq Management</a></li>
            <li class="breadcrumb-item active">Edit FAQ</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->

    <!--Filter Section -->
    <?php echo form_open_multipart( base_url().'admin/faq/edit?data='.queryStringBuilder( "id=".$page_id ), array ('id' => 'cms_add_form') ); ?>
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Edit FAQ</h3>
            <!-- <div class="top-right-panel">
                <a href="http://localhost/shopoholic/admin/mysaveddeals/detail" class="edit-btn"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                <button class="deal-btn" data-toggle="modal" data-target="#delete"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </div> -->
        </div>

        <!-- title and form upper action end-->
        <div class="white-wrapper clearfix">



            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Question Title</label>
                        <div class="input-holder">
                            <!--<input type="password" class="form-control material-control" value="johndoe@gmail.com">-->
                            <input type="text" class="form-control material-control" maxlength="150" name="title" placeholder="Enter the title" value="<?php
                             echo isset( $pages['faq_title'] ) ? $pages['faq_title'] : set_value( 'title' );
                            ?>">
                                   <?php echo form_error( 'title', '<label class="alert-danger">', '</label>' ); ?>
                                <!-- <span class="error_wrap"></span> -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Question Title (Tamil)</label>
                        <div class="input-holder">
                            <!--<input type="password" class="form-control material-control" value="johndoe@gmail.com">-->
                            <input type="text" class="form-control material-control" maxlength="150" name="title_ta" placeholder="Enter the title (tamil)" value="<?php
                             echo isset( $pages['faq_title_ta'] ) ? $pages['faq_title_ta'] : set_value( 'title_ta' );
                            ?>">
                                   <?php echo form_error( 'title_ta', '<label class="alert-danger">', '</label>' ); ?>
                                <!-- <span class="error_wrap"></span> -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Description</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="page_desc" placeholder="Enter the description" id="page_desc"><?php echo isset( $pages['faq_description'] ) ? $pages['faq_description'] : set_value( 'page_desc' ); ?></textarea>
                            <!-- <span class="error_wrap"></span> -->
                            <?php echo form_error( 'page_desc', '<label class="alert-danger">', '</label>' ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Description (Tamil)</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="page_desc_ta" placeholder="Enter the description (Tamil)" id="page_desc_ta"><?php echo isset( $pages['faq_description_ta'] ) ? $pages['faq_description_ta'] : set_value( 'page_desc_ta' ); ?></textarea>
                            <!-- <span class="error_wrap"></span> -->
                            <?php echo form_error( 'page_desc_ta', '<label class="alert-danger">', '</label>' ); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-8">
                    <div class="form-group">
                        <label class="admin-label">Status</label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="status">
                                <option value="">Select</option>
                                <option <?php echo($pages['status'] == ACTIVE) ? "selected='selected'" : ""; ?> value="<?php echo ACTIVE; ?>">Active</option>
                                <option <?php echo($pages['status'] == INACTIVE) ? "selected='selected'" : ""; ?> value="<?php echo INACTIVE; ?>">Inactive</option>
                            </select>
                            <?php echo form_error( 'status', '<label class="alert-danger">', '</label>' ); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="button"  onclick="window.location.href = '<?php echo base_url() ?>admin/Faq'"class="commn-btn cancel">Cancel</button>
                    <button type="submit" class="commn-btn save">Update</button>
                </div>
            </div>
            <!--form ele wrapper end-->
        </div>
        <!--form element wrapper end-->

    </div>
    <!--close form view   -->

    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>
<!--Table listing-->

<script src="<?php echo base_url() ?>public/ckeditor/ckeditor.js"></script>

<script>
                        $( document ).ready( function () {

                            CKEDITOR.replace( 'page_desc' );
                            CKEDITOR.replace( 'page_desc_ta' );

                        } );
</script>
