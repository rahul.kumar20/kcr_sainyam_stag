<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:7px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#000;font-size:15px;font-weight:500;text-transform:capitalize;padding: 30px 0 25px 34px;">
                                        வணக்கம் உடன்பிறப்பே!
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                    ஸ்டாலின் அணி செயலியின் பயன்பாட்டை நன்கறிந்த உங்களை நம் செயலியின் ஓர் சிறப்புப் பயனராக நாங்கள் கருதுகிறோம் . இது ஸ்டாலின் அணி செயலியினரான எங்களுக்கும் கழகத்திற்கும் பெருமிதம் கொள்ளும் தருணமாக அமைகிறது. இதை நினைவுகொள்ளும் வகையிலும் உங்கள் முயற்சிகளை அங்கீகரிக்கும் வகையிலும் உங்களுக்கான பாராட்டுச் சான்றிதழை இந்த மின்னஞ்சலுடன் இணைத்துள்ளோம். இது போன்று நீங்கள் அனைத்து நிலைகளையும் கடந்து வெற்றி வாகை சூட வாழ்த்துகிறோம்.    
                                    </td>
                                </tr>
                               <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        <a href="<?php echo $link; ?>" title="இந்த இணைப்பை அழுத்தி உங்கள் சான்றிதழைப் பெற்றிடுங்கள்.">போன்று நீங்கள் அனைத்து நிலைகளையும் கடந்து வெற்றி வாகை சூட வாழ்த்துகிறோம். </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        ஸ்டாலின் அணி இனி!
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   

                </table>
            </td>
        </tr>

    </table>

</body>

</html>