<!DOCTYPE html>

<head>
    <title>
        Stalin Ani
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        .container {
            max-width: 600px;
            width: 100%;
            background-color: #e5e5e5;
            margin: 0px auto;
        }

        .first-sec {
            background-color: #e5e5e5;
            padding: 15px;
        }

        .first-sec h2 {
            font-size: 1.1rem;
            font-weight: 600;
        }

        .first-sec p {
            font-size: 1.1rem;
            line-height: 1.5rem;
        }

        .second-sec {
            background-color: #d9d9d9;
        }

        .row {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-items: center;
        }

        .column {
            flex-basis: 50%;
            width: 50%;
            justify-content: center;
            align-items: center;
            text-align: center;
            padding: 15px 0px;
        }

        @media (min-width: 200px) and (max-width: 800px) {
            .column {
                width: 100%;
                flex-basis: 100%;
                padding: 15px;
            }

            .app-sec {
                padding-bottom: 40px;
            }
        }

        .app-sec h2 {
            font-size: 1.2rem;
            line-height: 1.8rem;
            margin-bottom: 25px;
            font-weight: 600;
        }

        .btn-app {
            background-color: #cb2128;
            color: #fff !important;
            padding: 10px;
            border-radius: 10px;
            font-size: 1rem;
            text-decoration: none;
            outline: none;
            margin-bottom: 15px;
        }

        .btn-app:hover {
            color: #fff !important;
        }

        #social-sharing {
            BACKGROUND: #262626;
            PADDING: 15px 0px;
            color: #fff;
        }

        .social-icons {
            text-align: center;
            padding: 0;
            margin: 0;
            width: 100%;
            margin: 0px auto;
        }

        .fa {
            padding: 10px;
            font-size: 1.6rem;
            width: 40px;
            height: 40px;
            line-height: 40px;
            text-align: center;
            text-decoration: none;
            margin: 5px 2px;
            border-radius: 50%;
        }

        .fa:hover {
            color: #fff;
            text-decoration: none;
        }

        .fa-facebook {
            background: #3B5998;
            color: white;
        }

        .fa-twitter {
            background: #55ACEE;
            color: white;
        }

        .fa-youtube {
            background: #bb0000;
            color: white;
        }

        .fa-instagram {
            background: #125688;
            color: white;
        }
    </style>
</head>

<body>
    <div class="container">
        <img style="width:100%;height:auto" src="https://www.stalinani.com/newsletter/wp-content/uploads/2021/02/Stalin-Ani-2.png">
        <section class="first-sec">
            <h2>வணக்கம்,</h2>
            <p style="margin-bottom: 15px;">உங்கள் அனைத்து குறைகளுக்குமான தீர்வு உங்கள் கைகளிலேயே உள்ளது. ஸ்டாலின் அணி செயலியில் உங்கள்
                குறைகளை நேரடியாக நம் தலைவர் திரு. மு.க. ஸ்டாலின் அவர்களிடம் பதிவு செய்திடலாம். எளிய
                பயன்பாட்டு முறையைக் கொண்ட நம் செயலி உலகம் போற்றும் நம் தமிழ் மொழியிலும் ஆங்கிலத்திலும்
                உள்ளது. உங்கள் சமூக வலைத்தளங்களை சிறந்த முறையில் பயன்படுத்த ஸ்டாலின் அணி செயலி உங்களுக்கு
                வழி காட்டும். இதுமட்டுமின்றி, நம் செயலியைத் தொடர்ந்து பயன்படுத்தினால் பாராட்டு சான்றிதழ்
                தலைவரின் கூட்டங்களில் சிறப்பு அமர்வு, கழகத் தலைவர்களை சந்திக்கும் வாய்ப்பென்று
                பங்களிப்பிற்கு ஏற்ற அங்கீகாரத்தையும் பெறுவீர்கள்.</p>
            <p style="margin-bottom: 15px;">இந்த மின்னஞ்சலில் நம் செயலிக்குள் உள்நுழையத் தேவையான தகவல்கள் கீழே வழங்கப்பட்டுள்ளது.</p>
            <p style="margin-bottom: 15px;"><b>பயனரின் குறியீடு/Username - <?php echo $username; ?></b></p>
            <p style="margin-bottom: 15px;"><b>கடவுச்சொல்/Password - <?php echo $password; ?></b></p>
            <p><b>இணைப்பு/ Link - <a href="http://bit.ly/Joindmkstalinani" target="_blank">http://bit.ly/Joindmkstalinani</a></b></p>
        </section>
        <section class="second-sec row">
            <div class="column col-6 img-sec">
                <img style="width:70%" src="https://www.stalinani.com/newsletter/wp-content/uploads/2021/02/Group-2.png">
            </div>
            <div class="column col-6 app-sec">
                <h2>தாமதத்தை தகர்த்து! நம் தளத்தில் நுழைந்து!மாற்றத்திற்கு வழிவகுப்போம்!</h2>
                <a class="btn btn-app" href="https://stalinani.com/">செயலியைப் பதிவிறக்குங்கள்</a>
            </div>
        </section>
        <div id="social-sharing" class="social-icons">
            <h5>GET SOCIAL WITH US</h5>
            <div style="margin-top: 5px;" class="three-icons">
                <a href="https://www.facebook.com/OndrinaivomVaa"><img style="width: 35px; height:auto" src="https://www.stalinani.com/newsletter/wp-content/uploads/2021/02/fb.png"></a>
                <a href="https://twitter.com/ondrinaivomvaa?lang=en"><img style="width: 35px; height:auto" src="https://www.stalinani.com/newsletter/wp-content/uploads/2021/02/twitter.png"></a>
                <a href="https://www.youtube.com/channel/UCDKalp-nMiuAY0nGq2T5CGQ"><img style="width: 35px; height:auto" src="https://www.stalinani.com/newsletter/wp-content/uploads/2021/02/youtube.png"></a>
                <a href="https://www.instagram.com/ondrinaivom_vaa/"><img style="width: 35px; height:auto" src="https://www.stalinani.com/newsletter/wp-content/uploads/2021/02/insta.png"></a>
            </div>
        </div>
    </div>
</body>

</html>