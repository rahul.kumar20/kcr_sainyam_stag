<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:7px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#000;font-size:15px;font-weight:500;text-transform:capitalize;padding: 30px 0 25px 34px;">
                                        அன்புள்ள உடன்பிறப்பே,
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        வாழ்த்துக்கள்! ஸ்டாலின் அணி செயலியின் வழி தமிழகத்தின் மாற்றத்திற்கும் மறுமலர்ச்சிக்கும் நீங்கள் அளித்து வரும் பங்களிப்பினால் பல நிலைகளைக் கடந்து "வினையூக்கி" நிலையை அடைந்திருக்கிறீர்கள். 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        இதைக் கொண்டாடும் விதமாக உங்களுக்கான நம் செயலியின் சிறப்புச் சான்றிதழை இந்த மின்னஞ்சலுடன் இணைத்திருக்கிறோம். இது உங்களின் தொடர் முயற்சிக்கான அங்கீகாரம். 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        இதே உற்சாகத்துடன் நமது செயலியில் தொடர்ந்து இணைந்திருங்கள். விரைவில் தலைவரைச் சந்திக்கும் வாய்ப்பைப் பெற வாழ்த்துகிறோம்!
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        <a href="<?php echo $link; ?>" title="இந்த இணைப்பை அழுத்தி உங்கள் சான்றிதழைப் பெற்றிடுங்கள்.">இந்த இணைப்பை அழுத்தி உங்கள் சான்றிதழைப் பெற்றிடுங்கள்.</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        குறிப்பு: எப்போதும் படங்களைப் பதிவிறக்குவது போலவே இச்சான்றிதழையும் பதிவிறக்கிடுங்கள்!
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-left:33px;padding-right:33px;" class="signup-detail">
                                <tr>
                                    <td style="padding:.75pt .75pt .75pt .75pt">
                                        <p style="margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:13.5pt"><span style="    font-size: 12.0pt;
                                          font-weight: 500;">அன்புடன், <br>ஸ்டாலின் அணி </span></p>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

    </table>

</body>

</html>