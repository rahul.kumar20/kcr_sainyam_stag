<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:7px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#000;font-size:15px;font-weight:500;text-transform:capitalize;padding: 30px 0 25px 34px;">
                                        வணக்கம் உடன்பிறப்பே!
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        ஸ்டாலின் அணி செயலியில் இருபத்தி ஒன்றாவது  நிலையான "பரிவாரத்தின் முத்துக்கள்" என்னும் நிலையை வெற்றிகரமாகக் கடந்து உங்களின் தொடர் முயற்சியை பதிவு செய்துள்ளீர்கள். உங்களின் இந்த முயற்சிகளை ஊக்குவிக்கும் வகையில் "ஸ்டாலின் அணியின் சிறந்த தன்னார்வலர்" பாராட்டு சான்றிதழ் உங்களுக்கு வழங்குகிறோம்.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        உங்கள் உழைப்பே எங்களின் அங்கீகாரம். 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        நாளைய விடியல் நமக்கானதே!!
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-left:33px;padding-right:33px;" class="signup-detail">
                                <tr>
                                    <td style="padding:.75pt .75pt .75pt .75pt">
                                        <p style="margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:13.5pt"><span style="    font-size: 12.0pt;
                                          font-weight: 500;">இப்படிக்கு, <br>ஸ்டாலின் அணி </span></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="signup-detail">
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 7px 34px;">
                                        குறிப்பு - கீழே உள்ள இணைப்பைப் பயன்படுத்தி உங்கள் பாராட்டு சான்றிதழை பதிவிறக்கிக்கொள்ளுங்கள்.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0; color: #000;font-size: 14px;
                                    font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 1px 17px 17px 34px;">
                                        <a href="<?php echo $link; ?>" title="இங்கே கிளிக் செய்க">இங்கே கிளிக் செய்க</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

    </table>

</body>

</html>