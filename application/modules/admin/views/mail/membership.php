<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
    <style>
    .fullconent {
        width: 70%;
        margin: 0 auto;
    }

    .img-responsive {
        width: 100%;
        width: 70%;
        margin: 0 auto;
    }

    .content {
        width: 60%;
        text-align: center;
        position: absolute;
        margin: 31% 5% 0 5%;
    }

    .backg {
        position: absolute;
    }

    .first {
        margin-top: 40%;
    }

    .button1 {
        background-color: #ab1919;
        color: white;
        border: 2px;
        border-radius: 10px;
        padding: 1% 2%;
    }

    fieldset {
        background-color: #f1f1f1;
        border-radius: 8px;
        margin: 24% 0 0 0;
        height: 160px;
        border: none;

    }

    .third {
        color: white;
        padding-top: 7%;
    }

    .icon-img {
        border-radius: 100%;
        position: relative;
        top: -65px;
    }

    .fieldtext {
        position: relative;
        top: -82px;
    }

    @media only screen and (min-width: 1200px) {
        body {
            font-size: 1.2rem;
        }

        .button1 {
            font-size: 1.6rem;
        }

    }

    @media only screen and (max-width: 1200px) {

        .button1 {
            font-size: 1.5rem;
        }

        fieldset {
            margin-top: 24%;
            margin-bottom: 0;
            height: 150px;
            padding: 0;
        }

        .icon-img {
            width: 100px;
        }

    }

    @media only screen and (max-width: 1000px) and (min-width: 830px) {
        .content {
            margin-top: 26%;
        }

        .icon-img {
            width: 90px;
            top: -52px;
        }

        fieldset {
            margin-top: 16%;
            margin-bottom: 0;
            height: 150px;
            padding: 0;
        }

        .fieldtext {
            top: -70px;
        }

        .first {
            margin-top: 45%;
        }

        .button1 {
            font-size: 1.3rem;
        }

    }


    @media only screen and (max-width: 830px) and (min-width: 660px) {
        body {
            font-size: .8rem;
        }

        .icon-img {
            width: 70px;
            top: -42px;
        }

        fieldset {
            margin: 13% 0;
            height: 135px;
            padding: 0;
        }

        .fieldtext {
            top: -55px;
        }

        .first {
            margin-top: 36%;
        }

        .button1 {
            font-size: 1rem;
        }

        .third {
            padding-top: 0%;
            margin-top: -20px;
        }

    }

    @media only screen and (max-width: 660px) {
        .img-responsive {
            width: 100%;
        }

        .fullconent {
            width: 100%;
            margin: 0 auto;
        }

        body {
            font-size: .65rem;
            margin: 0;
        }

        .content {
            margin: 55% 15% 0 15%;
            width: 70%
        }

        .icon-img {
            width: 55px;
            top: -42px;
        }

        fieldset {
            margin: 22% 0;
            height: 100px;
        }

        .fieldtext {
            top: -50px;
        }

        .first {
            margin-top: 35%;
        }

        .button1 {
            font-size: .85rem;
        }

        .third {
            margin-top: -40px;
            padding-top: 0%;
        }

    }

    @media only screen and (max-width: 400px) {
        fieldset {
            margin: 22% 0;
            height: 85px;
        }

        body {
            font-size: .55rem;
        }

        .icon-img {
            width: 50px;
            top: -39px;
        }

        .button1 {
            font-size: .8rem;
        }

        .fieldtext {
            top: -46px;
        }

    }
    </style>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <div class="fullconent">
        <img class="img-responsive backg" src="public/images/stalin_ani_mailer_NEW.png" />
        <div class="content">
            <div class="first">
                <fieldset>
                    <img class="icon-img" src="public/images/iconmailer2.jpeg" alt="">
                    <div class="fieldtext">
                        <p>திமுகவில் இணைந்தமைக்கு நன்றி. உங்கள் குடும்ப உறுப்பினர்களையும் கழகத்தில் இணைய பரிந்துரை
                            செய்யுங்கள்
                        </p>
                        <a href="" class="button1" style="text-decoration: none;">இங்கு அழுத்தவும்</a>
                    </div>
                </fieldset>
            </div>
            <div class="second">
                <fieldset>
                    <img class="icon-img" src="public/images/iconmailer1.jpeg" alt="">
                    <div class="fieldtext">
                        <p>திரு. மு.க.ஸ்டாலின் அவருடன் நேரடி தொடர்பில் இருங்கள் பரிசுகளை வெல்லுங்கள். பதிவிறக்கவும்
                            ஸ்டாலின் அணி
                        </p>
                        <a href="" class="button1" style="text-decoration: none;">பதிவிறக்க</a>
                    </div>
                </fieldset>
            </div>
            <div class="third">
                <p>2020 திராவிட முன்னேற்றக் கழகம்</p>
            </div>
        </div>
    </div>

</body>

</html>