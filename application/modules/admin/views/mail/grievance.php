<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:7px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#000;font-size:15px;font-weight:500;text-transform:capitalize;padding: 30px 0 5px 34px;">
                                        Hi <?php echo $name; ?>,
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                        Thank you for reporting your concern. Your issue has been duly noted and we are actively working towards resolving it. Each and everyone is important for me, and I will personally ensure to consider your comments when we are building an agenda for the state. It is through the voices and active participation of people like you that we can take Tamil Nadu to a new dawn.
                                        I request you to continue to keep in touch and continue writing to me if you have any other feedback. Together, we will work to build a prosperous Tamil Nadu.
                                    </td>
                                </tr>

                            </table>
                        </td>

                    </tr>

                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-left:33px;padding-right:33px;" class="signup-detail">

                                <tr>
                                    <td style="padding:.75pt .75pt .75pt .75pt">
                                        <p style="margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:13.5pt"><span style="    font-size: 12.0pt;
                                          font-weight: 500;">Yours sincerely, <br>M.K Stalin </span></p>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

    </table>

</body>

</html>