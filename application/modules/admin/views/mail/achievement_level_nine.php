<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:7px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#000;font-size:15px;font-weight:500;text-transform:capitalize;padding: 30px 0 25px 34px;">
                                        அன்புள்ள உடன்பிறப்பே,
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        வாழ்த்துகள்! நம் தலைவர் தளபதியைச் சந்திப்பதற்கான பயணத்தில் "நான்காம் பாசறை" எனும் நிலையைக் கடந்துவிட்டீர்கள். தமிழகத்தின் மாற்றத்துக்கும் மறுமலர்ச்சிக்கும் உங்களின் தொடர் பங்களிப்பை வழங்கியதற்கு நன்றி. 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                        உங்களின் இந்த முயற்சியை ஊக்குவிக்கும் வகையில் தொகுக்கப்பட்ட தலைவரின் சிறப்புக் காணொளியை உங்களுடன் பகிர்ந்துள்ளோம். இது உங்களின் சிறப்பான பங்களிப்பிர்க்கான அங்கீகாரம். 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                        இதே உற்சாகத்துடன் நமது செயலியில் தொடர்ந்து இணைந்திருங்கள். விரைவில் தலைவரைச் சந்திக்கும் வாய்ப்பைப் பெற வாழ்த்துகிறோம்!
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 10px 34px;">
                                        நான்காம்சங்கப் பாசறை வெகுமதி 
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    line-height: 1.3;
                                    padding: 2px 17px 17px 34px;">                                    
                                        https://drive.google.com/file/d/1c8FV3yG98aulT4oHZ6NoYm6ehydIox1k/view?usp=sharing
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-left:33px;padding-right:33px;" class="signup-detail">
                                <tr>
                                    <td style="padding:.75pt .75pt .75pt .75pt">
                                        <p style="margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:13.5pt"><span style="    font-size: 12.0pt;
                                          font-weight: 500;">அன்புடன், <br>ஸ்டாலின் அணி </span></p>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

    </table>

</body>

</html>