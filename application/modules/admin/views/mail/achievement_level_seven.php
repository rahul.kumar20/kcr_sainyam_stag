<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Stalin Ani</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">
    <table border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:680px; min-width:320px; -webkit-text-size-adjust: 100%; color:#333333;padding:7px;">
        <tr>
            <td>
                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding:0px 0px 15px 0px;">

                    <tr>
                        <td valign="top" width="100%" class="header">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="header-heading user" valign="top" style="color:#000;font-size:15px;font-weight:500;text-transform:capitalize;padding: 30px 0 25px 34px;">
                                        அன்புள்ள உடன்பிறப்பே,
                                    </td>
                                </tr>

                                <tr>
                                    <td class="signup-msg" valign="top" style="padding: 15px 0;color: #000;font-size: 14px;font-weight: 400;text-transform: uppercase;line-height: 1.3;padding: 17px 17px 17px 34px;">
                                        வாழ்த்துகள்! நம் தலைவர் திரு மு.க. ஸ்டாலின் அவர்களைச் சந்திப்பதற்கான பயணத்தில் சிறப்பாக பங்களித்து "இடைச்சங்க பாசறை" எனும் நிலையைக் கடந்துவிட்டீர்கள். தலைவர் ஸ்டாலினைத் தமிழக முதல்வர் ஆக்கவும் நமது கழகத்தின் முற்போக்குக் கருத்துகளை மக்களிடம் முன்னெடுத்துச் செல்லவும் நீங்கள் அளித்து வரும் ஆதரவையும் ஆற்றி வரும் பணிகளையும் கண்டு மகிழ்ச்சியடைகிறோம். </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                        உங்களின் இந்த தொடர் முயற்சியை ஊக்குவிக்கும் வகையில் தலைவரின் வாழ்க்கையை ஆல்பமாகத் தொகுத்து உங்களுடன் பகிர்ந்துள்ளோம். இது உங்களின் சிறந்த பங்களிப்பிர்க்கான அங்கீகாரம்.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="    padding: 15px 0;
                                    color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 17px 34px;">
                                        வரும் நிலைகளையும் வெற்றிகரமாகக் கடந்து, பரிசுகள் பெற்று நம் தலைவரை நீங்கள் சந்திக்கும் நாளையும் நாங்கள் எதிர்பார்த்துக் காத்திருக்கிறோம்! இனி வெல்வோம் ஒன்றாய்! 
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    text-transform: uppercase;
                                    line-height: 1.3;
                                    padding: 17px 17px 10px 34px;">
                                        இடைச்சங்கப் பாசறை வெகுமதி 
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td class="signup-msg" valign="top" style="color: #000;
                                    font-size: 14px;
                                    font-weight: 400;
                                    line-height: 1.3;
                                    padding: 2px 17px 17px 34px;">                                    
                                        https://drive.google.com/file/d/1tnvhM_ci0QLWpg1KS0viQz7jjkEwCdm6/view
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-left:33px;padding-right:33px;" class="signup-detail">
                                <tr>
                                    <td style="padding:.75pt .75pt .75pt .75pt">
                                        <p style="margin-right:0in;margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;line-height:13.5pt"><span style="    font-size: 12.0pt;
                                          font-weight: 500;">அன்புடன், <br>ஸ்டாலின் அணி </span></p>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>

    </table>

</body>

</html>