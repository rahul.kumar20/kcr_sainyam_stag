<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>

<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/podcast">Podcast</a></li>
            <li class="breadcrumb-item active">Add Podcast Content</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'addpodcast')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Podcast Content</h3>
        </div>

        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php
        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="white-wrapper clearfix">
                <div class="row">
                    <div class="col-lg-3 col-sm-5 col-xs-12">
                        <label class="admin-label">Logo<mark class="reject-cross">*</mark></label>
                        <div class="image-view-wrapper img-view150p img-viewbdr-radius4p">
                            <div class="image-view img-view150" id="profilePic" style="background-image:url('public/images/default.png')">
                                <span href="javascript:void(0);" class="upimage-btn"></span>
                                <input type="file" id="upload" name="logo_image" style="display:none;" accept="image/*" name="notificationImage" required>
                                <label class="camera" for="upload"><i class="fa fa-camera" aria-hidden="true"></i></label>
                                <label id="image-error" class="alert-danger"></label>
                            </div>
                        </div>
                    </div>
                    <span class="loder-wrraper-single"></span>
                    <div class="col-lg-9 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Title<mark class="reject-cross">*</mark></label>
                                    <div class="input-holder">
                                        <input type="text" name="title" name="title" id="title" autocomplete="off" maxlength="50" placeholder="Podcast title" value="<?php echo (isset($sess_data['title']) && !empty($sess_data['title'])) ? $sess_data['title'] : ""; ?>" required>
                                        <?php echo form_error('title', '<label class="error">', '</label>'); ?>
                                        <span class="titleErr error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Title (Tamil)<mark class="reject-cross">*</mark></label>
                                    <div class="input-holder">
                                        <input type="text" name="title_tn" name="title_tn" id="title_tn" autocomplete="off" maxlength="50" placeholder="Podcast title(tamil)" value="<?php echo (isset($sess_data['title_tn']) && !empty($sess_data['title_tn'])) ? $sess_data['title_tn'] : ""; ?>" required>
                                        <?php echo form_error('title_tn', '<label class="error">', '</label>'); ?>
                                        <span class="title(tamil)Err error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Select Audio Files<mark class="reject-cross">*</mark></label>
                                    <div class="input-holder">
                                        <input type="file" name="audio" class="form-control" accept="audio/*" id="upload-audio" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Status<mark class="reject-cross">*</mark></label>
                                    <div class="commn-select-wrap">
                                        <select class="selectpicker" id="status"  name="status">
                                            <option selected value="">Select Status</option>
                                            <option value="<?php echo ACTIVE; ?>">Active</option>
                                            <option value="<?php echo INACTIVE; ?>">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Schedule Date <mark class="reject-cross">*</mark></label>
                                    <div class="input-holder">
                                        <div class="inputfield-wrap">
                                            <input type="text" name="scheduleDate" autocomplete="off" data-provide="datepicker" value="" class="form-control scheduleDate schedule_date" id="scheduleDate" placeholder="Select Date" required>
                                            <?php echo form_error('scheduleDate', '<label class=" alert-danger">', '</label>'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Notification Title</label>
                                    <div class="input-holder">
                                        <input type="text" name="noti_title" id="noti_title" autocomplete="off" maxlength="50" placeholder="Notification title" value="<?php echo (isset($sess_data['noti_title']) && !empty($sess_data['noti_title'])) ? $sess_data['noti_title'] : ""; ?>">
                                        <?php echo form_error('noti_title', '<label class="error">', '</label>'); ?>
                                        <span class="titleErr error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="admin-label">Notification Message</label>
                                    <div class="input-holder">
                                        <input type="text" name="noti_message" name="noti_message" id="noti_message" autocomplete="off" maxlength="100" placeholder="Notification Message" value="<?php echo (isset($sess_data['noti_message']) && !empty($sess_data['noti_message'])) ? $sess_data['noti_message'] : ""; ?>">
                                        <?php echo form_error('noti_message', '<label class="error">', '</label>'); ?>
                                        <span class="titleErr error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-ele-action-bottom-wrap btns-center clearfix">
                            <div class="button-wrap">
                                <input type="button" class="commn-btn cancel" value="Cancel" onclick="window.location.href='<?php echo base_url() ?>admin/podcast'">
                                <input type="submit" class="commn-btn save" value="Save">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function() {
        $('#upload').change(function() {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onloadend = function() {
                $('#profilePic').css('background-image', 'url("' + reader.result + '")');
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                console.log('not done');
            }
        });
    })
    $(function() {
        var dated = new Date();
        dated.setDate(dated.getDate() - 1);
        $(".schedule_date").datetimepicker({
            format: "DD-MM-YYYY HH:mm:ss",
            // defaultDate: date,
            minDate: dated,
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#upload-audio').each(function() {
            $this = $(this);
            $this.on('change', function() {
                var fsize = $this[0].files[0].size,
                    ftype = $this[0].files[0].type,
                    fname = $this[0].files[0].name,
                    fextension = fname.substring(fname.lastIndexOf('.') + 1);
                validExtensions = ["mp3", "wav"];

                if ($.inArray(fextension, validExtensions) == -1) {
                    alert("This type of files are not allowed!");
                    this.value = "";
                    return false;
                } else {
                    if (fsize > 15728640) {
                        alert("File size too large! Please upload less than 15MB");
                        this.value = "";
                        return false;
                    }
                    return true;
                }
            });
        });
    });
</script>