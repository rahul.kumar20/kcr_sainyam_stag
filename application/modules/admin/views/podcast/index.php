<?php
$filterArr = $this->input->get();
$filterArr = (object)$filterArr;
$controller = $this->router->fetch_class();
$method = $this->router->fetch_method();
$module = $this->router->fetch_module();
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterArr); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>

<div class="inner-right-panel">

    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">PodCast</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <?php if (!empty($this->session->flashdata('podcast_success'))) {
    ?>
        <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('podcast_success'); 
            $this->session->unset_userdata('podcast_success'); ?>
        </div>
    <?php
    } ?>
    <!--Filter Section -->
    <div class="white-wrapper">
        <div class="fltr-srch-wrap clearfix">
            <div class="row">
                <div class="col-lg-2 col-sm-3">
                    <div class="display col-sm-space">
                        <select class="selectpicker dispLimit">
                            <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                            <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                            <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                            <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <div class="srch-wrap col-sm-space">
                        <button class="srch search-icon" style="cursor:default"></button>
                        <a href="javascript:void(0)"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                        <input autocomplete="off" type="text" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by push title" id="searchuser" name="search">
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2">
                    <?php if (isset($searchlike) && "" != $searchlike) { ?>
                        <div class="go_back">Go Back</div>
                    <?php
                    } ?>

                </div>
                <div class="col-lg-4 col-sm-4">
                    <div class="top-opt-wrap text-right">
                        <ul>
                            <li>
                                <a href="admin/podcast/addpodcast" title="Add Podcast" id="" class="icon_filter add"><img src="<?php echo base_url() ?>public/images/plus.svg"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
    <?php if (!empty($this->session->flashdata('message_success'))) {
    ?>
        <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
        </div>
    <?php

    } ?>

    <?php if (!empty($this->session->flashdata('message_error'))) {
    ?>
        <div class="alert alert-danger" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
        </div>
    <?php

    } ?>


    <div class="row">
        <div class="col-lg-6">Total podcasts: <?php echo $totalrows ?></div>
    </div>
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg'); ?>
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <!--Table-->
    <div class="white-wrapper">
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th width="50px">S.No</th>
                        <th>Title</th>
                        <th>Title TN</th>
                        <th>URL</th>
                        <th width="200px">Schedule Date</th>
                        <th>Notification Title</th>
                        <th>Notification Message</th>
                        <th>Status</th>
                        <?php if ($permission['action']) { ?>
                            <th width="70px" class="text-center">Action</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if (isset($podcastlist['result']) && count($podcastlist['result']) > 0) :
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($podcastlist['result'] as $key => $podcastlist) :
                            ?>

                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td align='left'>
                                <?php echo !empty($podcastlist['title']) ? $podcastlist['title'] : "Not Available"; ?>
                            </td>
                            <td align='left'>
                                <?php echo !empty($podcastlist['title_tn']) ? $podcastlist['title_tn'] : "Not Available"; ?>
                            </td>
                            <td align='left'>
                                <?php echo !empty($podcastlist['url']) ? $podcastlist['url'] : "Not Available"; ?>
                            </td>
                            <td align='left'>
                                <?php echo (isset($podcastlist['schedule_date']) && !empty($podcastlist['schedule_date']) ? mdate(DATE_FORMAT, strtotime($podcastlist['schedule_date'])) : 'Not Available') ?>
                            </td>
                            <td align='left'>
                                <?php echo !empty($podcastlist['notification_title']) ? $podcastlist['notification_title'] : "Not Available"; ?>
                            </td>
                            <td align='left'>
                                <?php echo !empty($podcastlist['notification_message']) ? $podcastlist['notification_message'] : "Not Available"; ?>
                            </td>
                            <td><?php echo ($podcastlist['status'] == ACTIVE) ? "Active" : "Inactive"; ?></td>
                                
                            <?php if ($permission['action']) { ?>
                                <td class="text-center">
                                    <a href="javascript:void(0);" class="table_icon" title="Delete"><i class="fa fa-trash" aria-hidden="true" onclick="deleteUser( 'podcast',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($podcastlist['id']); ?>', 'req/change-user-status', 'Do you really want to delete this podcast?' );"></i></a>
                                    </a>
                                </td>
                            <?php
                            } ?>
                        </tr>
                        <?php
                        endforeach;
                    else :
                        echo '<tr><td colspan="9" class="text-center">No result found.</td></tr>';
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
        <!-- Pagenation and Display data wrap-->
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>
        <!-- Pagination and Display data wrap-->
    </div>
    <!-- table 1 close-->
</div>
</div>

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/js/notification.js"></script>