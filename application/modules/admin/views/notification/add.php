<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style>
    [type="checkbox"]:not(:checked),
    [type="checkbox"]:checked {
        position: absolute;
        left: -9999px;
        opacity: 0;
    }

    [type="checkbox"]+label {
        position: relative;
        padding-left: 30px;
        cursor: pointer;
        display: inline-block;
        height: 25px;
        line-height: 18px;
        font-size: 14px;
        color: #6f6f6f;
        font-weight: 400;
        -webkit-user-select: none;
        -moz-user-select: none;
        -khtml-user-select: none;
        -ms-user-select: none;
    }

    [type="checkbox"].filled-in:not(:checked)+label:before {
        width: 0;
        height: 0;
        border: 3px solid transparent;
        left: 6px;
        top: 10px;
        -webkit-transform: rotateZ(37deg);
        transform: rotateZ(37deg);
        -webkit-transform-origin: 20% 40%;
        transform-origin: 100% 100%;
    }

    [type="checkbox"]+label:before,
    [type="checkbox"]:not(.filled-in)+label:after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 18px;
        height: 18px;
        z-index: 0;
        border: 2px solid #5a5a5a;
        border-radius: 1px;
        margin-top: 2px;
        transition: .2s;
    }

    [type="checkbox"].filled-in:checked+label:before {
        top: 0;
        left: 1px;
        width: 8px;
        height: 13px;
        border-top: 2px solid transparent;
        border-left: 2px solid transparent;
        border-right: 2px solid #f05038;
        border-bottom: 2px solid #f05038;
        -webkit-transform: rotateZ(37deg);
        transform: rotateZ(37deg);
        -webkit-transform-origin: 100% 100%;
        transform-origin: 100% 100%;
    }

    [type="checkbox"].filled-in+label:before,
    [type="checkbox"].filled-in+label:after {
        content: '';
        left: 0;
        position: absolute;
        transition: border .25s, background-color .25s, width .20s .1s, height .20s .1s, top .20s .1s, left .20s .1s;
        z-index: 1;
    }

    [type="checkbox"].filled-in:not(:checked)+label:after {
        height: 20px;
        width: 20px;
        background-color: transparent;
        border: 1px solid #d2d1d1;
        top: 0px;
        z-index: 0;
        border-radius: 3px;
    }

    [type="checkbox"].filled-in:checked+label:after {
        top: 0;
        width: 20px;
        height: 20px;
        border: 1px solid #ebebeb;
        background-color: #ffffff;
        z-index: 0;
        border-radius: 3px;
    }

    [type="radio"]:not(:checked),
    [type="radio"]:checked {
        position: absolute;
        left: -9999px;
        opacity: 0;
    }

    [type="radio"]:not(:checked)+label,
    [type="radio"]:checked+label {
        position: relative;
        padding-left: 25px;
        cursor: pointer;
        display: inline-block;
        height: 25px;
        line-height: 24px;
        font-size: 14px;
        transition: .28s ease;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        font-weight: 400;
        color: #363636;
        font-weight: 400;
    }

    [type="radio"]+label:before,
    [type="radio"]+label:after {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        margin: 4px;
        width: 16px;
        height: 16px;
        z-index: 0;
        transition: .28s ease;
    }

    [type="radio"]:checked+label:before {
        border: 2px solid transparent;
    }

    [type="radio"]:not(:checked)+label:before,
    [type="radio"]:not(:checked)+label:after,
    [type="radio"]:checked+label:before,
    [type="radio"]:checked+label:after,
    [type="radio"]:checked+label:before,
    [type="radio"]:checked+label:after {
        border-radius: 50%;
    }

    [type="radio"]:checked+label:after,
    [type="radio"]:checked+label:before,
    [type="radio"]:checked+label:after {
        border: 2px solid #7da527;
    }

    [type="radio"]:checked+label:after {
        -webkit-transform: scale(1.02);
        transform: scale(1.02);
    }

    [type="radio"]:checked+label:after {
        -webkit-transform: scale(0.5);
        transform: scale(0.5);
    }

    [type="radio"]:checked+label:after,
    [type="radio"]:checked+label:after {
        background-color: #f05038;
    }

    [type="radio"]:not(:checked)+label:before,
    [type="radio"]:not(:checked)+label:after {
        border: 1px solid #c1c1c1;
    }

    [type="radio"]:checked+label:after,
    [type="radio"]:checked+label:before,
    [type="radio"]:checked+label:after {
        border: 1px solid #c1c1c1;
    }

    .th-checkbox {
        display: inline-flex;
        vertical-align: middle;
        margin: -2px 0 -5px 0;
    }
</style>
<?php
if (!empty($this->session->userdata['notification_data'])) {
    $sess_data = $this->session->userdata['notification_data'];
}
?>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/notification">Notifications</a></li>
            <li class="breadcrumb-item active">Add Notification</li>
        </ol>
    </div>
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Notification</h3>
        </div>
        <?php echo form_open_multipart('', array('id' => 'add_not')); ?>
        <div class="white-wrapper clearfix">
            <div class="row">
                <div class="col-lg-3 col-sm-5 col-xs-12">
                    <div class="image-view-wrapper img-view150p img-viewbdr-radius4p">
                        <div class="image-view img-view150" id="profilePic" style="background-image:url('public/images/default.png')">
                            <span href="javascript:void(0);" class="upimage-btn"></span>
                            <input type="file" id="upload" style="display:none;" accept="image/*" name="notificationImage">
                            <label class="camera" for="upload"><i class="fa fa-camera" aria-hidden="true"></i></label>
                            <label id="image-error" class="alert-danger"></label>
                        </div>
                    </div>
                </div>
                <span class="loder-wrraper-single"></span>
                <div class="col-lg-9 col-sm-7 col-xs-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="admin-label">Title<mark class="reject-cross">*</mark></label>
                                <div class="input-holder">
                                    <input type="text" name="title" name="title" id="title" autocomplete="off" maxlength="60" placeholder="Notification title" value="<?php echo (isset($sess_data['notificattitl']) && !empty($sess_data['notificattitl'])) ? $sess_data['notificattitl'] : ""; ?>">
                                    <?php echo form_error('title', '<label class="error">', '</label>'); ?>
                                    <span class="titleErr error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="admin-label">Message<mark class="reject-cross">*</mark></label>
                                <div class="input-holder">
                                    <textarea class="custom-textarea" style="resize:none;" maxlength="100" name="message" id="messagetext"><?php echo (isset($sess_data['notificadesc']) && !empty($sess_data['notificadesc'])) ? $sess_data['notificadesc'] : ""; ?></textarea>
                                    <?php echo form_error('messagetext', '<label class="alert-danger">', '</label>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="admin-label">Link (Optional)</label>
                                <div class="input-holder">
                                    <input class="form-control" type="text" id="link" name="link">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="admin-label">Display Popup Notification? <mark class="reject-cross">*</mark></label>
                                <div class="commn-select-wrap">
                                    <select class="selectpicker" name="show_popup" id="show_popup">
                                        <option value=""></option>
                                        <option value="1">Yes</option>
                                        <option value="0" selected>No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 m-t-sm">
                            <div class="form-group">
                                <p class="userselecterror error"></p>
                                <p class="messgErr error"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-ele-action-bottom-wrap btns-center clearfix">
                        <div class="button-wrap">
                            <input type="button" class="commn-btn cancel" value="Cancel" onclick="window.location.href='<?php echo base_url() ?>admin/notification'">
                            <input type="button" onclick="return checkNotiValidation()" class="commn-btn save" value="Send Now">
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <div class="fltr-srch-wrap white-wrapper clearfix m-t-sm">
                <div class="row">
                    <div class="col-lg-2 col-sm-3">
                        <div class="display  col-sm-space">
                            <select class="selectpicker dispLimit">
                                <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                                <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                                <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                                <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4">
                        <div class="srch-wrap col-sm-space">
                            <button class="srch search-icon" style="cursor:default"></button>
                            <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                            <form method="get">
                                <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by UID,phone number" id="searchuser" name="searchlike" autocomplete="off">
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-2">
                        <?php if (isset($searchlike) && "" != $searchlike) { ?>
                            <div class="go_back">Go Back</div>
                        <?php
                        } ?>
                        <div class="col-lg-4 col-sm-2">
                            <div class="top-opt-wrap text-right">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="public/images/filter.svg"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Filter-->
                <!--Filter Wrapper-->
                <div class="filter-wrap ">
                    <div class="filter_hd clearfix">
                        <div class="pull-left">
                            <h2 class="fltr-heading">Filter</h2>
                        </div>
                        <div class="pull-right">
                            <span class="close flt_cl" data-dismiss="modal">X</span>
                        </div>
                    </div>
                    <div class="inner-filter-wrap">
                        <div class="fltr-field-wrap">
                            <label class="admin-label">Status</label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker filter status" name="status">
                                    <option value="">All</option>
                                    <option <?php echo ($status == ACTIVE) ? 'selected' : '' ?> value="1">Active</option>
                                    <option <?php echo ($status == BLOCKED) ? 'selected' : '' ?> value="2">Blocked</option>
                                </select>
                            </div>
                        </div>

                        <div class="fltr-field-wrap">
                            <label class="admin-label">State</label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker filter state" id="state" data-live-search="true" name="state" onchange="getDistrictForState('req/getdistrictbystate',this.value,'')">
                                    <option value="">All</option>

                                    <?php
                                    //if block start
                                    if (isset($statelist) && !empty($statelist)) {
                                        //foreach block start
                                        foreach ($statelist as $stateKey => $stateVal) { ?>
                                            <option value="<?php echo $stateVal['state_id']; ?>" <?php if ($state == $stateVal['state_id']) {
                                                                                                        echo 'selected';
                                                                                                    } ?>><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                                    <?php
                                            //fr each block end
                                        }
                                        //if block end 
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="fltr-field-wrap">
                            <label class="admin-label">District</label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker distict" data-live-search="true" name="distict" id="distict" onchange="getcollegeForDistrict('req/getcollegebydistrict',this.value,'')">
                                    <option value="">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="fltr-field-wrap">
                            <label class="admin-label">Gender</label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker filter gender" name="gender">
                                    <option value="">All</option>
                                    <option <?php echo ($gender == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER ?>">Male</option>
                                    <option <?php echo ($gender == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER ?>">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="fltr-field-wrap">
                            <label class="admin-label">Registration Date</label>
                            <div class="inputfield-wrap">
                                <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                            </div>
                        </div>
                        <div class="fltr-field-wrap">
                            <div class="inputfield-wrap">
                                <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                            </div>
                        </div>
                        <div class="fltr-field-wrap">
                            <label class="admin-label">Task Completed</label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker filter taskCompleted" name="task_completed">
                                    <option value="">All</option>
                                    <option <?php echo ($taskCompleted == '0-10') ? 'selected' : '' ?> value="0-10">0-10</option>
                                    <option <?php echo ($taskCompleted == '10-20') ? 'selected' : '' ?> value="10-20">10-20</option>
                                    <option <?php echo ($taskCompleted == '20-30') ? 'selected' : '' ?> value="20-30">20-30</option>
                                    <option <?php echo ($taskCompleted == '30-100') ? 'selected' : '' ?> value="30-100">30-100</option>
                                </select>

                            </div>
                        </div>
                        <div class="button-wrap text-center">
                            <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                            <button type="button" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Apply</button>
                        </div>
                    </div>
                </div>
                <!--Filter-->
                <div class="white-wrapper">
                    <div class="table-responsive custom-tbl">
                        <!--table div-->
                        <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="50px">Select</th>
                                    <th width="50px">S.No</th>
                                    <th>User ID</th>
                                    <th>Name</th>
                                    <th>Reg. Date</th>
                                    <th>Task Completed</th>
                                    <th>Reward Points</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($userlist) && !empty($userlist)) {
                                    if ($page > 1) {
                                        $i = (($page * $limit) - $limit) + 1;
                                    } else {
                                        $i = 1;
                                    }
                                    //for each start
                                    foreach ($userlist as $value) {
                                ?>
                                        <tr>
                                            <td>
                                                <div class="th-checkbox">
                                                    <input class="filter-type filled-in" type="checkbox" name="userchecked[]" id="flowers<?php echo isset($value['user_id']) ? $value['user_id'] : "N/A"; ?>" name="check[]" onclick="userListPushNotifi(this.value)" value="<?php echo isset($value['user_id']) ? $value['user_id'] : "N/A"; ?>" <?php if (isset($sess_data[$value['user_id']]) && $sess_data[$value['user_id']] == $value['user_id']) { ?>checked<?php
                                                                                                                                                                                                                                                                                                                                                                                                                                                                } ?>>
                                                    <label for="flowers<?php echo isset($value['user_id']) ? $value['user_id'] : "N/A"; ?>" class="lbl-check"><span></span></label>
                                                </div>
                                            </td>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo (isset($value['registeration_no']) && !empty($value['registeration_no']) ? $value['registeration_no'] : '') ?></td>
                                            <td><?php echo (isset($value['full_name']) && !empty($value['full_name']) ? $value['full_name'] : '') ?></td>
                                            <td><?php echo mdate(DATE_FORMAT, strtotime($value['registered_on'])); ?></td>
                                            <td class="text-center"><?php echo !empty($value['task_completed']) ? $value['task_completed'] : "0"; ?> </td>
                                            <td class="text-center"><?php echo !empty($value['points_earned']) ? $value['points_earned'] : "0"; ?>
                                            <td class="text-center"><?php echo ($value['is_active'] == ACTIVE) ? "Active" : "Blocked"; ?> </td>
                                        </tr>
                                    <?php
                                        //for each end
                                        $i++;
                                    }
                                    //if end
                                } else { ?>
                                    <td colspan="10">No record found</td <?php } ?> </tbody> </table> </div> <!-- Pagenation and Display data wrap-->
                                    <div class="pagination_wrap clearfix">
                                        <?php echo $link; ?>
                                    </div>
                                    <!-- Pagination and Display data wrap-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script>
        $(document).ready(function() {
            $('#upload').change(function() {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onloadend = function() {
                    $('#profilePic').css('background-image', 'url("' + reader.result + '")');
                }
                if (file) {
                    reader.readAsDataURL(file);
                } else {
                    console.log('not done');
                }
            });

            $('#regDate').daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                autoApply: true,
                drops: 'up'
            });
            $('#regDate').val('');
        })

        function userListPushNotifi(userid) {
            //var csrfHash = $('#csrftoken').val();
            var checkval = $("#flowers" + userid).is(':checked') ? 1 : 0;
            $.ajax({
                type: 'GET',
                url: baseUrl + 'admin/notification/checkeduserlist',
                data: {
                    "userid": userid,
                    "checkval": checkval,
                    "notificationtitle": $('#title').val(),
                    "notidesc": $('#messagetext').val(),
                    "sendto": '1'
                },
                async: false,
                success: function(result) {
                    obj = JSON.parse(result);
                    console.log(obj);
                    if (obj.ercode == '200') {
                        //$('#csrftoken').val(obj.token);
                        //window.location.reload();
                    }
                }
            });
        }
    </script>