<link href="<?php echo base_url() ?>public/css/animate.css" rel='stylesheet'>
<div class="account-wrapper">
    <div class="account-inner-wrap animated zoomIn">
        <div class="logo-wrapper">
            <div class="verticle-table">
                <div class="verticle-table-cell">
                    <img src="public/images/logo.png" alt="Logo">
                </div>
            </div>
        </div>
        <div class="registarion-wrapper">
            <!-- Login Form -->
            <?php echo form_open('', array ( 'id' => 'admin_login_form', 'class' => 'login_form form-wrapper login' )); 
                $login_cookie = json_decode(get_cookie('userlogin')); 
            ?>
            <h1 class="login-head">Login</h1>
            <p>Enter your details below to access <br> your account.</p>
            <div class="form-group">
                <div class="input-form has-floating-label">
                    <label class="floating-label">Email Address</label>
                    <input type="text" id = "email_id" class="removespace" maxlength="40"  onfocus="this.removeAttribute( 'readonly' );" readonly name="email" onkeyup="a = this.value;this.value = a.trim()" value="<?php
                    echo isset($email) ? $email : (isset($login_cookie) && ! empty($login_cookie) ? encryptDecrypt($login_cookie->log_id, 'decrypt') : set_value('email'));
                    ?>"  autocomplete="off" />
                </div>
            </div>
            <div class="form-group">
                <div class="input-form has-floating-label">
                    <label class="floating-label">Password</label>
                    <input type="password" class="removespace" maxlength="20"  onfocus="this.removeAttribute( 'readonly' );" readonly name="password" id = "passwd" value="<?php
                    echo isset($password) ? $password : ((isset($login_cookie) && ! empty($login_cookie)) ? encryptDecrypt($login_cookie->log_password, 'decrypt') : set_value('password'));
                    ?>"  autocomplete="off" required />
                    <span class="password-visible ShowHidepssword"><i class="fa fa-eye"></i></span>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-check">
                    <input type="checkbox" name="remember_me" id="remember_me" value="1" <?php echo (isset($login_cookie) && ! empty($login_cookie) ? 'checked' : '') ?>>                
                    <label for="remember_me">Remember Me</label>
                </div>
                <a href="javascript:void(0);" class="other-links" id="showforgot">Forgot Password ?</a>
            </div>
            <div class="text-center button-wrapper">
                <button type="submit" class="custom-btn loading-btn" id="load"> Submit</button>
            </div>
            <?php
            $show_var = '';
            $error_msg = '';

            if (null !== $this->session->flashdata("greetings")) {
                $show_var = 'show';
                $error_msg = $this->session->flashdata("message");
            }
            ?>

            <div class="common-msg error"><span id="error_msgs"></span><span class="close"><img src="public/images/close-white.svg" alt="Close"></span></div>

            <?php echo form_close(); ?>
            <!-- login end -->

            <!-- forgot password -->
            <?php
            $show_forgot = '';
            if (isset($error) && ! empty($error)) {
                $show_forgot = 'showforgot';
            }
            ?>
            <?php echo form_open(base_url() . 'admin/forgot', array ( 'id' => 'admin_forget_pwd_form', 'class' => '' . $show_forgot . ' form-wrapper forgot' )) ?>

            <h1 class="login-head">Forgot Password</h1>
            
            <p>Forgot your password?    Don't Worry. <br> Send us your registered email address and we will send you steps to reset your password.</p>
            
            <div class="form-group">
                <div class="input-form has-floating-label">
                    <label class="floating-label">Email Address</label>
                    <input type="text" class="removespace" maxlength="40" name="email" id="email" value="<?php echo set_value('email'); ?>">
                </div>
            </div>
            <div class="text-center button-wrapper">
                <button type="submit" class="custom-btn loading-btn showemailsuccess-btn" id="load"> Send</button>
                <button type="button" class="custom-btn transbg ml10" id="showlogin">Back</button>
            </div>
            
            <?php echo form_close(); ?>
            <!-- Success Password Start -->
            <div class="form-wrapper sendsuccess">
                <h1 class="login-head">Verification Link Sent</h1>
                <p>A link has been successfully sent to your email address. you can click on the link to reset your password.</p>
                <div class="checkmark-circle">
                    <div class="background"></div>
                    <div class="checkmark draw"></div>
                </div>
                <div class="text-center button-wrapper">

                <button type="button" class="custom-btn loading-btn" id="backToLogin">Back to login</button>
                </div>
            </div>
            <!-- Success password End -->
        </div>
    </div>

<div class="common-msg-server error <?php echo $show_var ;?> "><?php echo $error_msg; ?><span class="close"><img src="public/images/close-white.svg" alt="Close"></span></div>

<?php
    $error_message = $this->session->flashdata("Success");
if (isset($error) && ! empty($error)) {
    ?>
    <!-- show server side error messages -->
    <div class="common-msg-server error"> <?php echo $error; ?><span class="close"><img src="public/images/close-white.svg" alt="Close"></span></div>

        <?php
} elseif ('' !== $error_message && null !== $error_message) {
    ?>
    <div class="common-msg-server error"><?php echo $error_message; ?><span class="close"><img src="public/images/close-white.svg" alt="Close"></span></div>
        <?php
}
?>