<!--main wrapper close-->
</div>
<footer>
    &copy; KCR SAINYAM <?php echo date('Y'); ?>
</footer>
<script src="<?php echo base_url()?>public/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>public/js/bootstrap-select.js"></script>
<script src="<?php echo base_url()?>public/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>public/js/global-msg.js"></script>
<script src="<?php echo base_url()?>public/js/custom.js"></script>
<script src="<?php echo base_url() ?>public/js/wow.min.js"></script>
<script src="<?php echo base_url() ?>public/js/common.js"></script>
<script src="<?php echo base_url() ?>public/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>public/js/script.main.js"></script>
<script src="<?php echo base_url() ?>public/js/lightgallery-all.min.js"></script>
<script src="<?php echo base_url() ?>public/js/dropzone.js"></script>
<script src="<?php echo base_url() ?>public/js/bootstrap-inputmask.min.js"></script>
</body>

</html>

<!--data  Wrap close-->
    <!--Delete  Modal Close-->
    <!-- Modal -->
    <!-- Modal -->
    <div id="myModal-trash" class="modal fade" role="dialog">
        <input type="hidden" id="uid" name="uid" value="">
        <input type="hidden" id="ustatus" name="ustatus" value="">
        <div class="modal-dialog modal-custom">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-alt-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-heading">Delete </h4>
                </div>
                <div class="modal-body">
                    <p class="modal-para">Are you sure want to delete this user?</p>
                </div>

                <input type="hidden" id="new_status" name="new_status">
                <input type="hidden" id="new_id" name="new_id">
                <input type="hidden" id="new_url" name="new_url">
                <input type="hidden" id="new_msg" name="new_msg">
                <input type="hidden" id="for" name="for">
                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                        <button type="button" class="commn-btn save" onclick="changeStatusToDelete($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val())">Delete</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--delete Modal Close-->
    <!--Block  Modal Close-->
    <!-- Modal -->
    <!-- Modal -->
    <div id="myModal-block" class="modal fade" role="dialog">
        <input type="hidden" id="userid" name="userid" value="">
        <input type="hidden" id="udstatus" name="udstatus" value="">
        <div class="modal-dialog modal-custom">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-alt-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modal-heading">BLOCK</h4>
                </div>
                <div class="modal-body" >
                    <p class="modal-para">Are you sure want to block this user?</p>
                </div>
                <input type="hidden" id="new_status" name="new_status">
                <input type="hidden" id="new_id" name="new_id">
                <input type="hidden" id="new_url" name="new_url">
                <input type="hidden" id="new_msg" name="new_msg">
                <input type="hidden" id="for" name="for">


                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                        <button type="button" id="action" class="commn-btn save" onclick="changeStatusToBlock($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val())">Block</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


     <div id="myModal-logout" class="modal fade" role="dialog">
        <input type="hidden" id="uid" name="uid" value="">
        <input type="hidden" id="ustatus" name="ustatus" value="">
        <div class="modal-dialog modal-custom">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header modal-alt-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title-logout modal-heading">Logout</h4>
                </div>
                <div class="modal-body">
                    <p class="modal-para">Are you sure want to logout from admin panel?</p>
                </div>

                <div class="modal-footer">
                    <div class="button-wrap">
                        <button type="button" class="commn-btn cancel" data-dismiss="modal">No</button>
                        <button type="button" onclick="window.location='<?php echo base_url()?>admin/Logout'" class="commn-btn save">Yes</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
    $(document).ready(function(){
        setTimeout(function(){ $('.alert-success').fadeOut(5000); }, 3000);
    });


    $(function(){
            var previewImage = function(input) {
            var fileTypes = ['jpg', 'jpeg', 'png'];
            var extension = input.files[0].name.split('.').pop().toLowerCase(); /*se preia extensia*/
            var isSuccess = fileTypes.indexOf(extension) > -1; /*se verifica extensia*/

            if (isSuccess) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    if($('.upload-prod-pic-wrap ul li').length < 5){
                     $('.upload-prod-pic-wrap ul').append('<li><span class="trash-ico"></span><a href="#"><img src="'+e.target.result+'"></a></li>')
                    }
                };
                reader.readAsDataURL(input.files[0]);
               
            } else {
                alert('Image not accepted');
            }

        };
        $('#upload-img').on('change', function() {
            previewImage(this);
            setTimeout(function() {
                showpreview() 
            }, 300);
        })                              
      });

      function showpreview(){
        var lth = $('.upload-prod-pic-wrap ul li').length;
        if(lth == 0){
            $('#superadminpic').attr({
                    'src': ''
                })
        }
        console.log('dasdas'+lth);  
        $('.upload-prod-pic-wrap ul li').each(function(key,val){
            console.log('key'+key)
            if(key == lth-1){
                $('#superadminpic').attr({
                    'src': $(this).find('img').attr('src')
                })
            }else{
                
            }
        }); 
        if(lth == 5){
            $('.camera').hide()
        }else{
            $('.camera').show()
        }



        $(".upload-prod-pic-wrap ul li .trash-ico").click(function(){
            $(this).parent().remove();  
            // alert("hii"); 
            showpreview();
        });



      }


    </script>
