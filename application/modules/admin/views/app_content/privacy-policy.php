
<div class="content-wrap">
<!-- <h1 class="main-heading">FAIR USE POLICY</h1>
<hr> -->

<?php  echo isset($content) && !empty($content)?$content['content']:'';?>

</div>

<style type="text/css">
body {
    margin: 0;
    background: #fff;
    font-family: calibri;

}
.content-wrap {
    padding:0px;
    color: #363636 !important;
}	

.main-heading {
    text-align: center;
    font-size: 22px;
    padding: 15px 0;
    margin: 0;
    text-transform: uppercase;
}

.sub-heading {
    color: #262626;
    padding: 0 18px;
}
p {
    padding: 15px 0;
    font-size: 14px;
    margin: 0;
    color: #363636;
    line-height: 1.6;

}
ul{
    margin: 0;
    padding: 0 0 0 20px;
}
ul li {
    font-size: 19px;
    padding: 5px 0;
    padding: 15px 0;
    font-size: 14px;
    margin: 0;
    color: #363636;
    line-height: 1.6;
}
hr {
    color: #9a9a9a;
    height: 1px;
    background: #ccc;
    border: 0;
    width: 10%;
}

.header-inner{
	text-align: center;
	padding:5px 5px;}

</style>
