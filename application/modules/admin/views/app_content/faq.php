
<div class="content-wrap">
<!-- <h1 class="main-heading">About the Indian Political Action Committee (I-PAC)</h1>
<hr> -->


<div class="container">
    <?php if (isset($content) && !empty($content)) {
        $i = 1;
        foreach ($content as $value) {
            ?>
      <div class="accordian-block">
       <div class="ac-title" data-in="#tab<?php echo $i; ?>">
          <div class="plus-icon"></div>
          <p><?php echo $i; ?>.<?php echo  empty($lang) || $lang=='en' ? $value['faq_title'] : (empty($value['faq_title_ta'])?$value['faq_title']:$value['faq_title_ta'])?></p>
       </div>
        <div class="accordian-para acc-show" id="tab<?php echo $i; ?>" style="">
        <?php echo  empty($lang) || $lang=='en' ? $value['faq_description'] : (empty($value['faq_description_ta'])?$value['faq_description']:$value['faq_description_ta'])?></p>
        </div>
     </div>
    <?php $i++;
        }
     }     ?> 
 </div>

</div>

<style type="text/css">

body {
    margin: 0;

    font-family: calibri;

}
.main-heading {
    text-align: center;
    margin: 0 0 0 0;
    font-size: 22px;
}
.content-wrap {
    padding:0px;
    border-radius: 15px;
    margin: 26px 9px 0;
    background: #fff;
}

hr {
    color: #9a9a9a;
    height: 1px;
    background: #ccc;
    border: 0;
    width: 10%;
    margin: 18px auto 21px;
}
.accordian-block {
    padding: 2px 0;
    border-bottom: 1px solid #ccc;
    margin: 0 0 0 0;
    border-radius: 0;
    user-select: none;
}

.ac-title {
    position: relative;
    cursor: pointer;
}
.ac-title p {
    font-size: 16px !important;
    font-weight: 500;
    word-wrap: break-word;
    color: #414141;
    margin: 0;
    padding: 10px 50px 10px 15px;
    border-radius: 0;
}
.accordian-para {
    display: none;
    border-top: none;
    margin: 0px 0 0 0;
}
.commn-para-sm {
    font-size: 14px;
    color: #3d3d3d;
    padding: 0px 20px 0px 15px;
    margin: 0 0 15px;
    line-height: 1.6;
}
.ac-title.acc-active p {
    color: #014084 !important;
}
.plus-icon {
    background-image: url(<?php echo base_url().'public/images/';?>faq-plus.svg);
    width: 15px;
    height: 15px;
    position: absolute;
    right: 15px;
    top: 50%;
    transform: translateY(-50%);
    background-size: contain;
    z-index: 99;
    transition: all 0.6s ease;
    -webkit-transition: all 0.6s ease;
    -moz-transition: all 0.6s ease;
    -o-transition: all 0.6s ease;
    background-repeat: no-repeat;
}
.cross-icon {
    /* background-image: url(<?php echo base_url().'public/images/';?>substract.svg); */
    transform: rotate(90deg);
}
ul{
    margin: 0;
    padding: 0 0 0 33px;
}
ul li {
    font-size: 19px;
    padding: 5px 0;
    padding: 15px 0;
    font-size: 14px;
    margin: 0;
    color: #363636;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
     function reset_acc() {
         $('.ac-title').removeClass('acc-active');
         $('.accordian-para').slideUp();
         $('.plus-icon').removeClass('cross-icon');
         }
         $('.ac-title').click(function (e) {
         e.preventDefault();
         if ($(this).hasClass('acc-active'))
         {
         reset_acc();
         }
         else {
         reset_acc();
         var getID = $(this).attr('data-in');
         $(getID).slideDown();
         $(this).addClass('acc-active');
         $(this).find('.plus-icon').addClass('cross-icon');
         }
         });

         //Acordian Close
</script>