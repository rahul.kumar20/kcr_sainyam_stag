
<div class="content-wrap">
    <figure class="img-wrap">
        <img src="<?php echo base_url();?>public/images/logo-home.png" width=133px; height=107px;>
    </figure>
<!-- <h1 class="main-heading">About I-PAC</h1>
<hr> -->
<?php  echo isset($content) && !empty($content)?$content['content']:'';?>
</div>

<style type="text/css">
body {
    margin: 0;
    background: #fff;
    font-family: calibri;

}
.content-wrap {
    padding: 124px 10px 0 10px;
    color: #363636 !important;
    position: relative;
}
.img-wrap {
    width: 133px;
    height: 107px;
    position: absolute;
    border-radius: 5px;
    left: 50%;
    transform: translateX(-50%);
    margin: 0;
    top: 5px;
}
.img-wrap img {
    /* width: 100%;
    height: 120px; */
    object-fit: cover;
}

.main-heading {
    text-align: center;
    font-size: 22px;
    padding: 15px 0;
    margin: 0;
    text-transform: uppercase;
}

.sub-heading {
    color: #262626;
    padding: 0 18px;
}
p {
    padding: 15px 0;
    font-size: 14px;
    margin: 0;
    color: #363636;
    line-height: 1.6;
}
ul{
    margin: 0;
    padding: 0 0 0 20px;
}
ul li {
    font-size: 19px;
    padding: 5px 0;
    padding: 15px 0;
    font-size: 14px;
    margin: 0;
    color: #363636;
}
hr {
    color: #9a9a9a;
    height: 1px;
    background: #ccc;
    border: 0;
    width: 10%;
}

.header-inner{
	text-align: center;
	padding:5px 5px;}

</style>
