<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/livesessions">Live Sessions</a></li>
            <li class="breadcrumb-item active">Add Live Session</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'live_sessions_add_form')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Live Session</h3>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Content Info</h3>
            </div>
        </div>
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php
        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Session Name<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="session_name" id="session_name" value="<?php echo set_value('session_name'); ?>" placeholder="Enter Session Name" required>
                            <?php echo form_error('session_name', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="admin-label">Video ID<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="session_video_id" id="session_video_id" value="<?php echo set_value('session_video_id'); ?>" placeholder="Enter Session Video ID" required>
                            <?php echo form_error('session_video_id', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm" id="newsCatDiv" >
                    <div class="form-group">
                        <label class="admin-label">Session Date<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" name="session_date" autocomplete="off" data-provide="datepicker" value="" class="form-control priotityEndDate" id="session_date" placeholder="Session Date" value="<?php echo set_value('session_date'); ?>" required>
                            <?php echo form_error('session_date', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-ele-wrapper clearfix">
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="submit" class="commn-btn save" id="add_live_sessions">Submit</button>
                </div>
            </div>
            <!--form ele wrapper end-->
        </div>
        <!--form element wrapper end-->
    </div>
    <!--close form view   -->
    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>
<!--Table listing-->
<script>
$("#upload-img").removeAttr('multiple');
</script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>public/js/cropper.min.js"></script>

<!-- Modal -->

<!-- portfolio Modal -->

<script>
   

    $(function() {
        var dated = new Date();
        dated.setDate(dated.getDate() - 1);
        // dated = formatDate(dated)
        $('.priotityEndDate').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            // defaultDate: date,
            minDate: dated
        });
    });
</script>
