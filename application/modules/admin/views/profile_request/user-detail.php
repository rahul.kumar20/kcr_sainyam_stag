<div class="inner-right-panel">
   <?php //pr($profile);?>
   <!--breadcrumb wrap-->
   <div class="breadcrumb-wrap">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/profileUpdateRequest">Profile Request</a></li>
         <li class="breadcrumb-item active">User Detail</li>
      </ol>
   </div>
   <!--Filter Section -->
   <div class="form-item-wrap">
      <div class="form-item-title clearfix">
         <h3 class="title">User Detail</h3>
      </div>
      <!-- title and form upper action end-->
      <div class="white-wrapper clearfix">
         <div class="row">
            <div class="col-sm-6">
            <span class="show-label">Update Request</span>
               <div class="row">
                  <div class="col-sm-4">
                     <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p <?php echo (isset($profile['new_image']) && !empty($profile['new_image'])?'loader':'')?>">
                        <img class="example-image " data-lightbox="example-set"  src="<?php echo (isset($profile['new_image']) && !empty($profile['new_image'])?$profile['new_image']:'public/images/default.png')?>"> 
                     </div>
                  </div>
                  <div class="col-sm-8">
                     <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['registeration_no']) && !empty($profile['registeration_no'])? ucfirst($profile['registeration_no']):'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['new_name']) && !empty($profile['new_name'])? ucfirst($profile['new_name']):'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Email ID</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['new_email_id']) && !empty($profile['new_email_id'])? ucfirst($profile['new_email_id']):'N/A'); ?></span>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Facebok ID</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['facebook_id']) && !empty($profile['facebook_id'])? $profile['facebook_id']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Facebok Username</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['facebook_username']) && !empty($profile['facebook_username'])? $profile['facebook_username']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Twitter ID</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['twitter_id']) && !empty($profile['twitter_id'])? $profile['twitter_id']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Twitter Username</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['twitter_username']) && !empty($profile['twitter_username'])? $profile['twitter_username']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Whatsapp Number</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['new_whatsapp_number']) && !empty($profile['new_whatsapp_number'])? $profile['new_whatsapp_number']:'N/A'); ?></span>
                           </div>
                        </div>
                          <div class="form-group">
                        <label class="admin-label">ID Image Uploaded</label>
                        <?php    //id proof  if loop start
                           if (isset($userIdNewProof) && !empty($userIdNewProof)) {
                                   //for each loop start
                               foreach ($userIdNewProof as $idProof) {?>
                        <div class="id-pic-wrapper">
                           <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p">
                              <div class="profile-pic image-view img-view150">
                                 <a  class="example-image-link" href="<?php echo $idProof['image']?>" data-lightbox="example-set"><img class="example-image" data-lightbox="example-set" src="<?php echo $idProof['image']?>"></a>
                              </div>
                           </div>
                        </div>
                        <?php
                           //for each loop end
                               }
                           // if end
                           } else {?>
                        <div class="input-holder">
                           <span class="text-detail">Not Available</span>
                        </div>
                        <?php }?>
                     </div>

                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-6">
                            <span class="show-label">Previous Data</span>
               <div class="row">
                  <div class="col-sm-4">
                     <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p <?php echo (isset($profile['user_image']) && !empty($profile['user_image'])?'loader':'')?>">
                        <img class="example-image " data-lightbox="example-set"  src="<?php echo (isset($profile['user_image']) && !empty($profile['user_image'])?$profile['user_image']:'public/images/default.png')?>"> 
                        <!-- <div class="profile-pic image-view img-view150" style="background-image:url('<?php echo (isset($profile['user_image']) && !empty($profile['user_image'])?$profile['user_image']:'public/images/default.png')?>');"> -->
                        <!-- </div> -->
                     </div>
                  </div>
                  <div class="col-sm-8">
                  <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['registeration_no']) && !empty($profile['registeration_no'])? ucfirst($profile['registeration_no']):'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['full_name']) && !empty($profile['full_name'])? ucfirst($profile['full_name']):'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Email ID</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['email_id']) && !empty($profile['email_id'])? ucfirst($profile['email_id']):'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Facebook ID</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['old_facebook_id']) && !empty($profile['old_facebook_id'])? $profile['old_facebook_id']:'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Facebook Username</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['old_fb_username']) && !empty($profile['old_fb_username'])? $profile['old_fb_username']:'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Twitter Id</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['old_twitter_id']) && !empty($profile['old_twitter_id'])? $profile['old_twitter_id']:'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Twitter Username</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['old_twitter_username']) && !empty($profile['old_twitter_username'])? $profile['old_twitter_username']:'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                           <label class="admin-label">Whatsapp Number</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['old_whatsup_number']) && !empty($profile['old_whatsup_number'])? $profile['old_whatsup_number']:'N/A'); ?></span>
                           </div>
                        </div>
                     <div class="form-group">
                        <label class="admin-label">ID Image Uploaded</label>
                        <?php    //id proof  if loop start
                           if (isset($userIdProof) && !empty($userIdProof)) {
                                   //for each loop start
                               foreach ($userIdProof as $idProof) {?>
                        <div class="id-pic-wrapper">
                           <div class="form-profile-pic-wrapper image-view-wrapper img-view150p img-viewbdr-radius4p">
                              <div class="profile-pic image-view img-view150">
                                 <a  class="example-image-link" href="<?php echo $idProof['image']?>" data-lightbox="example-set"><img class="example-image" data-lightbox="example-set" src="<?php echo $idProof['image']?>"></a>
                              </div>
                           </div>
                        </div>
                        <?php
                           //for each loop end
                               }
                           // if end
                           } else {?>
                        <div class="input-holder">
                           <span class="text-detail">Not Available</span>
                        </div>
                        <?php }?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <!--form ele wrapper end-->
               <div class="form-ele-action-bottom-wrap btns-center clearfix">
                  <div class="button-wrap text-center">            
                     <a href="javascript:void(0);"  class="table_icon accept-check commn-btn cancel" title="Reject" onclick="acceptuser( 'profileupdatereject',<?php echo PROFILE_REJECT; ?>, '<?php echo encryptDecrypt($profile['user_id']); ?>', 'req/accept-user-request', 'Do you want to reject profile update request of user?' );"><i class="" aria-hidden="true" >Reject Request</i></a>
                     <a href="javascript:void(0);" class="table_icon accept-check commn-btn save" title="Accept"  onclick="acceptuser( 'profileupdateaccept',<?php echo PROFILE_ACCEPT; ?>, '<?php echo encryptDecrypt($profile['user_id']); ?>', 'req/accept-user-request', 'Do you want to accept profile update request of user?' );"><i class="" aria-hidden="true" >Accept Request</i></a>

                    </div>
               </div>
            </div>
         </div>
      </div>
      <!--form element wrapper end-->
   </div>
   <!--form element wrapper end-->
</div>
<script src="<?php echo base_url() ?>public/js/profile_request.js"></script>
<!-- Modal -->
<div id="myModal-accept" class="modal fade" role="dialog">
   <input type="hidden" id="userid" name="userid" value="">
   <input type="hidden" id="udstatus" name="udstatus" value="">
   <div class="modal-dialog modal-custom">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header modal-alt-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title modal-heading">Accept Request</h4>
         </div>
         <div class="modal-body" >
            <p class="modal-para">Are you sure want to block this user?</p>
         </div>
         <input type="hidden" id="new_status" name="new_status">
         <input type="hidden" id="new_id" name="new_id">
         <input type="hidden" id="new_url" name="new_url">
         <input type="hidden" id="new_msg" name="new_msg">
         <input type="hidden" id="for" name="for">
         <div class="modal-footer">
            <div class="button-wrap">
               <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
               <button type="button" id="action" class="commn-btn save" onclick="changeStatusToAccept($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val())">Accept</button>
            </div>
         </div>
      </div>
   </div>
</div>