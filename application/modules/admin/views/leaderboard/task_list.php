<?php
$showAction = $permission['action'];
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>

<div class="inner-right-panel">
   <!--breadcrumb wrap-->
   <div class="breadcrumb-wrap">
      <ol class="breadcrumb">
	  <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/leaderboard">Leaderboard</a></li>
         <li class="breadcrumb-item">Task List</li>
         <li class="breadcrumb-item"><?php echo (isset($userDetail['full_name']) && !empty($userDetail['full_name'])?$userDetail['full_name']:'N/A')?></li>
      </ol>
   </div>
   <input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
  <!--Filter Section -->
  <div class="fltr-srch-wrap white-wrapper clearfix">
  <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
        <div class="alert alert-success" style="display:block;">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
        </div>
        <?php

    } ?>
       
    <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
                    <div class="alert alert-danger" style="display:block;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
                    </div>
    <?php

} ?>
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by task title" id="searchuser" name="search" autocomplete="off">
                </div>

            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                     <div class="go_back">Go Back</div>
                    <?php

                } ?>

            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="top-opt-wrap text-right">
                    <ul>
                  <!--  <li>
                              <a href="javascript:void(0)"data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add">
                              <img src="<?php echo base_url() ?>public/images/download.svg">
                             </a>
                        </li>
                    <li>
                             <a href="admin/task/addTask" title="Add New" id="" class="icon_filter add"><img src="<?php echo base_url() ?>public/images/plus.svg"></a>
                        </li>	-->
                        <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li>
					
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
     <!--Filter Wrapper-->
     <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">

        <div class="fltr-field-wrap">
            <label class="admin-label">Status</label>
            <div class="commn-select-wrap">
                <select class="selectpicker filter status" name="status">
                    <option value="3">All</option>
                    <option <?php echo ($status == COMPLETE) ? 'selected' : '' ?> value="1">Completed</option>
                    <option <?php echo ($status == PENDING) ? 'selected' : '' ?> value="2">Pending</option>
                </select>

            </div>
        </div>
        <div class="fltr-field-wrap">
        <label class="admin-label">Task Type</label>
        <div class="commn-select-wrap">
            <select class="selectpicker filter taskCompleted" name="type">
                <option value="">All</option>
                <option  <?php echo ($taskCompleted == 'facebook') ? 'selected' : '' ?> value="facebook">Facebook</option>
                <option  <?php echo ($taskCompleted == 'twitter') ? 'selected' : '' ?>  value="twitter">Twitter</option>
                <option  <?php echo ($taskCompleted == 'whatsapp') ? 'selected' : '' ?>  value="whatsapp">Whatsapp</option>
                <option  <?php echo ($taskCompleted == 'offline') ? 'selected' : '' ?>  value="offline">Offline</option>
                <option  <?php echo ($taskCompleted == 'youtube') ? 'selected' : '' ?>  value="youtube">Youtube</option>
            </select>

        </div>
    </div>
   

        <div class="fltr-field-wrap">
            <label class="admin-label">Completed On</label>
            <div class="inputfield-wrap">
                <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
            </div>

        </div>
        <div class="fltr-field-wrap">
            <div class="inputfield-wrap">
                <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
            </div>
        </div>
    
    
        <div class="button-wrap text-center">
            <button type="reset" class="commn-btn cancel" onclick="window.location.href='<?php base_url() ?>admin/Leaderboard/taskList?data=<?php echo queryStringBuilder("id=".$userId); ?>'"id="resetbutton">Reset</button>
            <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Apply</button>
        </div>

    </div>
    </div>
   <!--Table-->
   <label id="error">
      <div class="alert alert-success" style="display:none" role="alert">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
         <strong>
         <span class="alertType"></span>
         </strong>
         <span class="alertText"></span>
      </div>
   </label>
   <div class="white-wrapper">
      <p class="tt-count">Total Task Completed By(<?php echo (isset($userDetail['registeration_no']) && !empty($userDetail['registeration_no'])?$userDetail['registeration_no']:'N/A')?>): <?php echo $totalrows ?></p>
      <div class="table-responsive custom-tbl">
         <!--table div-->
         <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
            <thead>
               <tr>
                    <th>S.No </th>
                    <th>Task Id</th>
                    <th>Task</th>
                    <th><a href="<?php base_url() ?>admin/Leaderboard/taskList?data=<?php echo queryStringBuilder("field=point&id=".$userId."&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_points; ?>">Points</a></th>
                    <th><a href="<?php base_url() ?>admin/Leaderboard/taskList?data=<?php echo queryStringBuilder("field=registered&id=".$userId."&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_date; ?>">Completed On</a></th>
                    <th><a href="<?php base_url() ?>admin/Leaderboard/taskList?data=<?php echo queryStringBuilder("field=startdate&id=".$userId."&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_startdate; ?>">Task Live Date</a></th>
                    <th><a href="<?php base_url() ?>admin/Leaderboard/taskList?data=<?php echo queryStringBuilder("field=enddate&id=".$userId."&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_enddate; ?>">Deadline Date</a></th>
                    <th> <a href="<?php base_url() ?>admin/Leaderboard/taskList?data=<?php echo queryStringBuilder("field=completed&id=".$userId."&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_completed; ?>">Completed by</a></th>
                    <th>Task Type</th>
                    <th>Task For</th>
                    <th>Status</th>
               </tr>
            </thead>
            <tbody id="table_tr">
            <?php
            if (isset($tasklist) && count($tasklist)) {
                if ($page > 1) {
                    $i = (($page * $limit) - $limit) + 1;
                } else {
                    $i = 1;
                }
                foreach ($tasklist as $value) {
                    ?>
                <tr>
                    <td><?php echo $i; ?> </td>
                    <td><?php echo !empty($value['task_code']) ? $value['task_code'] : "0"; ?></td>
                    <td><a href="<?php echo base_url() ?>admin/task/taskDetail?data=<?php echo queryStringBuilder("id=" . $value['task_id']); ?>">
                        <span class="td-text-wrap" title="<?php echo !empty($value['task_title']) ? $value['task_title'] : "Not Available"; ?>
                        ">  <?php echo !empty($value['task_title']) ? $value['task_title'] : "Not Available"; ?>
                        </span>
                        
                        </a></td>
                    <td><?php echo !empty($value['points']) ? $value['points'] : "0"; ?></td>
                    <td><?php echo mdate(DATE_FORMAT, strtotime($value['added_on'])); ?></td>
                    <td><?php echo mdate(TASK_DATE_FORMAT, strtotime($value['start_date'])); ?></td>
                    <td><?php echo mdate(TASK_DATE_FORMAT, strtotime($value['end_date'])); ?></td>
                    <td><?php echo !empty($value['task_completed']) ? $value['task_completed'] : "0"; ?></td>
                    <td><?php echo (isset($value['task_type']) && !empty($value['task_type']) ? $value['task_type'].'('.$value['action'].')' : 'Not Available'); ?></td>
                    <td> <?php 
                        if (isset($value['state_name']) && !empty($value['state_name'])) {
                            echo $value['state_name'];
                        } else if (isset($value['district_name']) && !empty($value['district_name'])) {
                            echo $value['district_name'];
                        } else if (isset($value['registeration_no']) && !empty($value['registeration_no'])) {
                            echo $value['registeration_no'];
                        } else if (isset($value['gender']) && !empty($value['gender'])) {
                            echo ($value['gender'] == MALE_GENDER ? 'Male' : 'Female');
                        } else {
                            echo 'All(Tamilnadu)';
                        }
                        ?></td>
                    <td><?php echo ($value['status'] == COMPLETE) ? "Completed" : "Pending"; ?></td>
                   
               </tr>
                        <?php 
                        $i++;
                    }

                } else {?>

                <td colspan="11"><?php echo $this->lang->line('NO_RECORD_FOUND');?></td>
                <?php }?>
            </tbody>
         </table>
      </div>
      <div class="pagination_wrap clearfix">
      <?php echo $link; ?>

    </div>
   </div>
</div>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script>

                                             $( document ).ready( function () {

                                                 var nowTemp = new Date();
                                                 var now = new Date( nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0 );

                                                 var checkin = $( '#startDate' ).datepicker( {
                                                     onRender: function ( date ) {
                                                         return date.valueOf() > now.valueOf() ? 'disabled' : '';
                                                     }
                                                 } ).on( 'changeDate', function ( ev ) {
                                                     $( '#endDate' ).val( '' );
                                                     if ( ev.date.valueOf() < checkout.date.valueOf() ) {
                                                         var newDate = new Date( ev.date )
                                                         newDate.setDate( newDate.getDate() );
                                                         checkout.setValue( newDate );
                                                     }
                                                     checkin.hide();
                                                     $( '#endDate' )[0].focus();
                                                 } ).data( 'datepicker' );
                                                 var checkout = $( '#endDate' ).datepicker( {
                                                     onRender: function ( date ) {
                                                         return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
                                                     }
                                                 } ).on( 'changeDate', function ( ev ) {
                                                     checkout.hide();
                                                 } ).data( 'datepicker' );


                                                 //on datepicker 2 focus
                                                 $( '#datepicker_2' ).focus( function () {
                                                     if ( $( '#datepicker_1' ).val() == '' ) {
                                                         checkout.hide();
                                                     }
                                                 } );
                                                 //prevent typing datepicker's input
                                                 $( '#datepicker_2, #datepicker_1' ).keydown( function ( e ) {
                                                     e.preventDefault();
                                                     return false;
                                                 } );

                                             } );
</script>
<!-- Modal -->
<div id="mymodal-csv" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header modal-alt-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title modal-heading">Upload</h4>
    </div>
    <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit' => 'return doValidateUploadForm();')); ?>

        <div class="modal-body">
        <div class="form-group">
                <label class="admin-label">Upload File</label>
                    <div class="input-holder">
                        <input type="text" id="upload-btn"  class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                            <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                            <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="task_csv_file" id="upload-doc" style="display:none;">  

                        <span id="file_error"></span>
                    </div>
                    
                <span class="download-btn">
                    <a href="/public/task_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                </span>
            </div>
        </div>

        <div class="modal-footer">
            <div class="button-wrap">
                    <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="commn-btn save" name="csv-upload" value="upload-csv">Upload</button>
            </div>
        </div>

        </div>
    <?php echo form_close(); ?>

  </div>
</div>

<script>

function doValidateUploadForm()
{
    $('#file_error').text('');
    var cnt = 0;

    var allowed_image_type = ['text/csv']

    var landscape_image_data = $('#upload-doc')[0].files

    if(landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length ==0 ) {

        $('#file_error').text('Please select csv file');
        $('#file_error').css('color','red');
        cnt++;
    }
    if(landscape_image_data.length >0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type)==-1) {

        $('#file_error').text('Please select csv file only');
        $('#file_error').show();
        cnt++;
    }

     if (cnt >0){
        return false;
    } else{
        return true;
    }
}


</script>