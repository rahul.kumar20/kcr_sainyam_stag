<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/event">Event Management</a></li>
            <li class="breadcrumb-item active">Add Event</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'event_add_form')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Event</h3>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Content Info</h3>
            </div>
        </div>
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php
        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Event Title<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title" id="title" value="<?php echo set_value('title'); ?>" placeholder="Enter Title" required>
                            <?php echo form_error('title', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="admin-label">Event Title (Tamil)<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title_tn" id="title_tn" value="<?php echo set_value('title'); ?>" placeholder="Enter Title in Tamil" required>
                            <?php echo form_error('title', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Location<mark class="reject-cross">*</mark></label>
                        <textarea class="form-control" id="location" name="location" required="" placeholder="Enter Location" aria-required="true"></textarea>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Location (Tamil)<mark class="reject-cross">*</mark></label>
                        <textarea class="form-control" id="location_tn" name="location_tn" required="" placeholder="Enter Location in Tamil" aria-required="true"></textarea>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm" id="newsCatDiv" >
                    <div class="form-group">
                        <label class="admin-label">Event Date</label>
                        <div class="input-holder">
                            <input type="text" name="eventDate" autocomplete="off" data-provide="datepicker" value="" class="form-control priotityEndDate" id="eventDate" placeholder="Event Date" value="<?php echo set_value('eventDate'); ?>">
                            <?php echo form_error('eventDate', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm" id="" >
                    <div class="form-group">
                      <label class="admin-label" for="eventTime">Event Time:</label>
                      <div class="input-holder ">
                      <input type="time" id="eventTime" name="eventTime" class="form-control" style="padding:0; margin: 0">
                      </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm image-gallery" style="">
                    <label class="admin-label"><span id="img-label">Upload Icon</span></label>
                    <figure class="usr-dtl-pic task-upload-gallery">
                        <img src="public/images/placeholder2.png" id="superadminpic">
                        <span class="sm-loader" style="display:none"><img src="public/images/Gray_circles_rotate.gif"></span>
                        <label class="camera" for="upload-img"><i class="fa fa-camera" aria-hidden="true"></i></label>
                        <input type="file" name="event_image" id="upload-img" style="display:none;">
                    </figure>
                    <div class="upload-prod-pic-wrap task-thumbnails">
                        <ul></ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-ele-wrapper clearfix">
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    
                    <button type="submit" class="commn-btn save" id="add_event">Submit</button>
                </div>
            </div>
            <!--form ele wrapper end-->
        </div>
        <!--form element wrapper end-->
    </div>
    <!--close form view   -->
    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>
<!--Table listing-->
<script>
$("#upload-img").removeAttr('multiple');
</script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>public/js/cropper.min.js"></script>

<!-- Modal -->

<!-- portfolio Modal -->

<script>
    $(document).ready(function() {
        var $image = $(".image-crop > img")
        $($image).cropper({
            aspectRatio: 1.618,
            preview: ".img-preview",
            done: function(data) {
                //alert("Image");
                // Output the result data for cropping image.
            }
        });

        var $inputImage = $("#inputImage");
        if (window.FileReader) {
            $inputImage.change(function() {
                var fileReader = new FileReader(),
                    files = this.files,
                    file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function() {
                        $inputImage.val("");
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#download").click(function() {
            // window.open($image.cropper("getDataURL"));

            var byteData = $image.cropper("getDataURL");
            $("#main_preview").attr('src', byteData);
            var byteData_data = byteData.split(';')[1].replace("base64,", "");
            $("#cropped_data").val(byteData_data);
            $('#myModal').modal('hide');

            //                $.post('businesspartner/dashboard/save_portfolio?save_portfolio=yes', {data: byteData}, function(res){
            //                
            //                if(res != ''){
            //
            //                  location.reload(true);
            //
            //                }
            //                else{
            //                  alert('something wrong');
            //                }
            //                });


        });


        $("#save_ads").click(function() {
            var byteData = $image.cropper("getDataURL");
            $("#main_preview").attr('src', byteData);
            var byteData_data = byteData.split(';')[1].replace("base64,", "");
            $("#cropped_data").val(byteData_data);
            $('#myModal').modal('hide');
        });

        $("#crop_store_image").click(function() {
            var byteData = $image.cropper("getDataURL");
            var image_rand_id = localStorage.getItem('image_rand_id');
            console.log('saved =' + image_rand_id);
            $("#main_preview_" + image_rand_id).attr('src', byteData);
            var byteData_data = byteData.split(';')[1].replace("base64,", "");
            $("#cropped_data" + image_rand_id).val(byteData_data);
            $('#storeImageModal').modal('hide');
            $($image).attr('src', '');
            $($image).cropper('destroy');
            // var $image = $(".image-crop > img");
            $($image).cropper({
                aspectRatio: 1.618,
                preview: ".img-preview",
                done: function(data) {
                    //alert("Image");
                    // Output the result data for cropping image.
                }
            });
        });

        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper("rotate", 45);
        });

        $("#rotateRight").click(function() {
            $image.cropper("rotate", -45);
        });

        $("#setDrag").click(function() {
            $image.cropper("setDragMode", "crop");
        });

        $("#validity").keyup(function() {
            if ($(this).val() == '') {
                $("#credit_using").html(0);
            } else {
                $("#credit_using").html($(this).val());
            }
        });
        $("#save_profile").click(function() {
            // window.open($image.cropper("getDataURL"));
            var byteData = $image.cropper("getDataURL");
            byteData = byteData.split(';')[1].replace("base64,", "");
            $('.loader_modal').show();
            $.post(baseUrl + 'admin/AjaxUtil/save_profile_photo', {
                data: byteData,
                csrf_token: csrf_token
            }, function(res) {

                if (res != '') {
                    $('#banner_image').val(res);
                    $('.loader_modal').hide();
                    $('#myModal').modal('hide');
                    $("#superadminpic").attr("src", res);
                    $('.sm-loader').show();
                    hideSmallLoader();
                } else {
                    alert('something wrong');
                }
            });
        });
    });

    $(function() {
        var dated = new Date();
        dated.setDate(dated.getDate() - 1);
        // dated = formatDate(dated)
        $('.priotityEndDate').datetimepicker({
            format: 'DD-MM-YYYY HH:mm:ss',
            // defaultDate: date,
            minDate: dated
        });
    });
</script>
<style>
    .img-preview.img-preview-sm {
        width: 100%;
        max-height: 282px;
        overflow: hidden;
        margin: 0 0 10px 0;
    }

    .crop-heading {
        font-size: 20px;
        font-weight: 500;
        margin: 0 0 10px 0;
    }

    .modal-header {
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
        position: relative;
    }

    .modal-bttn-wrap ul li {
        display: inline-block;
        margin: 11px 11px 11px 0;
    }

    .loader_modal {
        position: absolute;
        top: 33%;
        left: 46%;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #60c6f3;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
    }

    @keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }
</style>