<div class="inner-right-panel">
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/achievement">Achievements</a></li>
            <li class="breadcrumb-item active">Edit Achievement</li>
        </ol>
    </div>
    <?php echo form_open_multipart(base_url() . 'admin/achievement/edit?data=' . queryStringBuilder("id=" . $page_id), array('id' => 'cms_add_form')); ?>
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Edit Content</h3>
        </div>
        <div class="white-wrapper clearfix">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" name="name" value="<?php echo isset($pages['name']) ? $pages['name'] : set_value('name'); ?>">
                            <?php echo form_error('name', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Unlock Type</label>
                        <select class="selectpicker" name="type" id="type">
                            <option <?php echo $pages['type'] == '1' ? 'selected' : '';  ?> value="1">Level</option>
                            <option <?php echo $pages['type'] == '2' ? 'selected' : '';  ?> value="2">Reward Points</option>
                        </select>
                        <?php echo form_error('type', '<label class="alert-danger">', '</label>'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="level_div" class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Select Level</label>
                        <select class="selectpicker" name="fk_level_id" id="fk_level_id">
                            <option value=""></option>
                            <?php foreach ($levelList as $level) {
                                echo '<option ';
                                if ($pages['fk_level_id'] == $level['pk_level_id']) {
                                    echo 'selected ';
                                }
                                echo ' value="' . $level['pk_level_id'] . '">' . $level['name'] . '</option>';
                            } ?>
                        </select>
                        <?php echo form_error('fk_level_id', '<label class="alert-danger">', '</label>'); ?>
                    </div>
                </div>
                <div id="reward_point_div" class="col-sm-6 col-xs-12" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Enter Reward Points</label>
                        <div class="input-holder">
                            <input type="number" class="form-control material-control" name="reward_points" value="<?php echo isset($pages['reward_points']) ? $pages['reward_points'] : set_value('reward_points'); ?>">
                            <?php echo form_error('reward_points', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Description</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="description"><?php echo isset($pages['description']) ? $pages['description'] : set_value('description'); ?></textarea>
                            <?php echo form_error('description', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label class="admin-label">Upload Image</label>
                    <figure class="usr-dtl-pic task-upload-gallery">
                        <a class="example-image-link" id="scandocument" href="assets/images/contract.png" data-lightbox="example-1">
                            <img class="example-image" src="<?php echo !empty($pages['image']) ? $pages['image'] : 'public/images/placeholder2.png'; ?>" id="scancontract-image"></a>
                        <label class="camera" for="scancontract"><i class="fa fa-camera" aria-hidden="true"></i></label>
                        <input type="file" id="scancontract" name="user_image" onchange="loadFile(event)" style="display:none;">
                    </figure>
                </div>
            </div>
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="button" onclick="window.location.href = '<?php echo base_url() ?>admin/achievement'" class="commn-btn cancel">Cancel</button>
                    <button type="submit" class="commn-btn save">Update</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        changeType($('#type').val());
        $('#type').on('change', function() {
            changeType(this.value);
        });

        function changeType(type) {
            if (type == '1') {
                $('#level_div').show();
                $('#reward_point_div').hide();
            } else {
                $('#level_div').hide();
                $('#reward_point_div').show();
            }
        }
    });
    var loadFile = function(event) {
        var output = document.getElementById('scancontract-image');
        var file = event.target.files[0];
        var fileType = file.type;
        var FileSize = file.size / 1024 / 1024; // in MB
        var blob = file; // See step 1 above
        var type = '';
        var imageExt = ['image/png', 'image/jpeg'];
        var fileReader = new FileReader();
        if ($.inArray(fileType, imageExt) == -1) {
            alert(string.video_ext_err);
            return false;
        }
        fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
            var header = "";
            for (var i = 0; i < arr.length; i++) {
                header += arr[i].toString(16);
            }
            //file mime type validation
            switch (header) {
                case "89504e47":
                    type = "image/png";
                    break;
                case "47494638":
                    type = "image/gif";
                    break;
                case "ffd8ffe0":
                case "ffd8ffe1":
                case "ffd8ffe2":
                case "ffd8ffe3":
                case "ffd8ffe8":
                    type = "image/jpeg";
                    break;
                default:
                    type = "unknown"; // Or you can use the blob.type as fallback
                    break;
            }

            console.log(type);
            //validation for myme type check
            if ($.inArray(type, imageExt) == -1) {
                alert(string.video_ext_err);

                return false;
            }
            // Check the file signature against known types

        };
        fileReader.readAsArrayBuffer(blob);

        var scandocument = document.getElementById('scancontract-image');

        output.src = URL.createObjectURL(event.target.files[0]);
        scandocument.href = URL.createObjectURL(event.target.files[0]);
        $("#scandocument").attr("href", scandocument.href)
    };
</script>