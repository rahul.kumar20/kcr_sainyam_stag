<div class="inner-right-panel">
   <?php //pr($profile);?>
   <!--breadcrumb wrap-->
   <div class="breadcrumb-wrap">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/campaign-form">Campaign Form</a></li>
         <li class="breadcrumb-item active">Detail</li>
      </ol>
   </div>
   <!--Filter Section -->
   <div class="form-item-wrap">
      <div class="form-item-title clearfix">
         <h3 class="title">Detail</h3>
      </div>
      <!-- title and form upper action end-->
      <div class="white-wrapper clearfix">
         <div class="row">
            <div class="col-sm-6">
            <span class="show-label">Detail Filled</span>
               <div class="row">
               
                  <div class="col-sm-8">
                     <div class="form-group">
                        <label class="admin-label">Q. Will you vote from <?php echo (isset($profile['district_name']) && !empty($profile['district_name'])?$profile['district_name']:'N/A')?>,<?php echo (isset($profile['state_name']) && !empty($profile['state_name'])?$profile['state_name']:'N/A')?> in 2019 General Elections ?</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['answer_1']) && !empty($profile['answer_1'])? $answer[$profile['answer_1']]:'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Q. Are you a resident of  <?php echo (isset($profile['district_name']) && !empty($profile['district_name'])?$profile['district_name']:'N/A')?>,<?php echo (isset($profile['state_name']) && !empty($profile['state_name'])?$profile['state_name']:'N/A')?>?</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['answer_3']) && !empty($profile['answer_3'])? $answer[$profile['answer_3']]:'N/A'); ?></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="admin-label">Home AC</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($profile['home_ac']) && !empty($profile['home_ac'])? ucfirst(get_ac_name($profile['home_ac'])):'N/A'); ?></span>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Address</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['address']) && !empty($profile['address'])? $profile['address']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Pincode</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['pincode']) && !empty($profile['pincode'])? $profile['pincode']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Q. Which language would you prefer to work on this app ?</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['language1']) && !empty($profile['language1'])? $profile['language1']:'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Q. Where will you vote in 2019 General Election?</label>
                           <div class="input-holder">
                              <span class="text-detail"></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Vote State</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['vote_state']) && !empty($profile['vote_state'])? get_state_name($profile['vote_state']):'N/A'); ?></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="admin-label">Vote District</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['vote_district']) && !empty($profile['vote_district'])? get_district_name($profile['vote_district']):'N/A'); ?></span>
                           </div>
                        </div> 
                        <div class="form-group">
                           <label class="admin-label">Vote AC</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($profile['vote_ac']) && !empty($profile['vote_ac'])? get_ac_name($profile['vote_ac']):'N/A'); ?></span>
                           </div>
                        </div> 
                     </div>
                  </div>
               </div>
            </div>
           
         </div>
       
      </div>
      <!--form element wrapper end-->
   </div>
   <!--form element wrapper end-->
</div>
<script src="<?php echo base_url() ?>public/js/profile_request.js"></script>
<!-- Modal -->
<div id="myModal-accept" class="modal fade" role="dialog">
   <input type="hidden" id="userid" name="userid" value="">
   <input type="hidden" id="udstatus" name="udstatus" value="">
   <div class="modal-dialog modal-custom">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header modal-alt-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title modal-heading">Accept Request</h4>
         </div>
         <div class="modal-body" >
            <p class="modal-para">Are you sure want to block this user?</p>
         </div>
         <input type="hidden" id="new_status" name="new_status">
         <input type="hidden" id="new_id" name="new_id">
         <input type="hidden" id="new_url" name="new_url">
         <input type="hidden" id="new_msg" name="new_msg">
         <input type="hidden" id="for" name="for">
         <div class="modal-footer">
            <div class="button-wrap">
               <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
               <button type="button" id="action" class="commn-btn save" onclick="changeStatusToAccept($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val())">Accept</button>
            </div>
         </div>
      </div>
   </div>
</div>