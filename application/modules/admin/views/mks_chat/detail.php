<?php
?>
<div class="inner-right-panel">

    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/mks_chat">
                    MKS Chat</a></li>
            <li class="breadcrumb-item active">
                MKS Chat Detail</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->

    <!--Filter Section -->
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title"> MKS Chat Detail</h3>
        </div>
        <!-- title and form upper action end-->
        <div class="form-ele-wrapper clearfix">
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($contactDetail['full_name']) && !empty($contactDetail['full_name']) ? $contactDetail['full_name'] : 'N/A'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Mobile no.</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($contactDetail['phone_number']) && !empty($contactDetail['phone_number']) ? $contactDetail['phone_number'] : 'N/A'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($contactDetail['registeration_no']) && !empty($contactDetail['registeration_no']) ? $contactDetail['registeration_no'] : 'N/A';; ?>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Question</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo (isset($contactDetail['question']) && !empty($contactDetail['question']) ? $contactDetail['question'] : 'Not Available'); ?>
                            </span>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Answered On</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo date("d M Y H:i a", strtotime($contactDetail['answered_datetime'])); ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Answer</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo (isset($contactDetail['answer']) && !empty($contactDetail['answer']) ? $contactDetail['answer'] : 'Not Available'); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_open_multipart('', array('id' => 'version_add_form')); ?>
            <div class="form-group m-t-md">
                <label class="admin-label">Send Reply</label>
                <div class="input-holder">
                    <textarea class="custom-textarea" name="reply" placeholder="Enter Comment Here" id="reply" maxlength="200" required></textarea>
                    <input type="hidden" name="token" id="token" value="<?php echo encryptDecrypt($user_id); ?>">
                    <input type="hidden" name="phone_no" id="phone_no" value="<?php echo $contactDetail['phone_number'] ?>">
                </div>
            </div>
            <div class="button-wrap text-center">
                <button type="submit" class="commn-btn reply_btn" name="reply_btn" id="reply_btn">Reply</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!--Table listing-->
<script>
    $('img').load(function() {
        $(this).removeClass('loader');
    });
</script>