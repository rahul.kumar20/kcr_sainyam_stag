<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
   <!--breadcrumb wrap-->
   <div class="breadcrumb-wrap">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/user_uploads">User Uploads</a></li>
         <li class="breadcrumb-item active">User Uploads Detail</li>
      </ol>
   </div>
   <div class="fltr-srch-wrap white-wrapper clearfix">
      <?php if (!empty($this->session->flashdata('message_success'))) {
      ?>
         <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
         </div>
      <?php

      } ?>

      <?php if (!empty($this->session->flashdata('message_error'))) {
      ?>
         <div class="alert alert-danger" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
         </div>
      <?php

      } ?>
      <div class="white-wrapper">
         <div class="form-item-title clearfix">
            <h3 class="title">User Uploads Detail</h3>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="upload-pic-wrapper">

                  <?php if (!empty($uploads_media)) {
                     foreach ($uploads_media as $medVal) { ?>
                        <?php if ($medVal['media_type'] == IMAGE) { ?>
                           <figure class="show-img">
                              <a class="example-image-link" href="<?php echo $medVal['media_url']; ?>" data-lightbox="example-set">
                                 <img class="example-image loader" data-lightbox="example-set" src="<?php echo $medVal['media_url']; ?>">
                              </a>
                           </figure>
                        <?php
                        } else if ($medVal['media_type'] == VIDEO) { ?>
                           <figure class="video-img-wrapper">
                              <video controls class="video-wrap">
                                 <span class="trash-ico"></span>
                                 <source src="<?php echo $medVal['media_url']; ?>" id="video_here">

                              </video>
                           </figure>
                        <?php
                        } else if ($medVal['media_type'] == AUDIO) { ?>
                           <figure class="video-img-wrapper">
                              <audio controls class="video-wrap">
                                 <span class="trash-ico"></span>
                                 <source src="<?php echo $medVal['media_url']; ?>" id="video_here">

                              </audio>
                           </figure>
                  <?php
                        }
                     }
                  } ?>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="row col-sm-12" >
                  <div class="form-group">
                     <label class="admin-label">Remarks</label>
                     <div class="input-holder">
                        <div class="inputfield-wrap">
                           <span class="text-detail"><?php echo (isset($user_uploads['remarks']) && !empty($user_uploads['remarks']) ? $user_uploads['remarks'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($user_uploads['registeration_no']) && !empty($user_uploads['registeration_no']) ? $user_uploads['registeration_no'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">User Name</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($user_uploads['full_name']) && !empty($user_uploads['full_name']) ? $user_uploads['full_name'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">District</label>
                        <div class="input-holder">
                           <div class="inputfield-wrap">
                              <span class="text-detail"><?php echo (isset($user_uploads['district_name']) && !empty($user_uploads['district_name']) ? $user_uploads['district_name'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Added On</label>
                        <div class="input-holder">
                           <div class="inputfield-wrap">
                              <span class="text-detail"><?php echo (isset($user_uploads['created_date']) && !empty($user_uploads['created_date']) ? mdate(TASK_DATE_FORMAT, strtotime($user_uploads['created_date'])) : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">

               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
   <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
</div>
