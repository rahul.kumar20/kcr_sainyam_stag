<?php
$showAction = $permission['action'];
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>

<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">User Uploads</li>
        </ol>
    </div>
    <input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
    <input type="hidden" id="stateId" value='<?php echo isset($state) ? $state : ''; ?>'>
    <input type="hidden" id="districtId" value='<?php echo isset($distict) ? $distict : ''; ?>'>
    <input type="hidden" id="collegeId" value='<?php echo isset($college) ? $college : ''; ?>'>
    <input type="hidden" id="method" value='<?php echo isset($method) ? $method : ''; ?>'>

    <input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
    <!--Filter Section -->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success');
                                            $this->session->unset_userdata('message_success'); ?>
            </div>
        <?php

        } ?>

        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php

        } ?>
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <!-- <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by task title" id="searchuser" name="search" autocomplete="off">
                </div>

            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                    <div class="go_back">Go Back</div>
                <?php

                } ?>

            </div> -->
            <div class="col-lg-4 col-sm-4" style="float: right;">
                <div class="top-opt-wrap text-right">
                    <ul>
                        <!-- <li>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add">
                                <img src="<?php echo base_url() ?>public/images/download.svg">
                            </a>
                        </li>
                        <li>
                            <a href="admin/task/addTask" title="Add New" id="" class="icon_filter add"><img src="<?php echo base_url() ?>public/images/plus.svg"></a>
                        </li> -->
                        <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
    <!--Filter Wrapper-->
    <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">

            <div class="fltr-field-wrap">
                <label class="admin-label">UID</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter uid" data-live-search="true" name="uid" id="uid">
                        <option value="">All</option>
                        <?php
                        //if block start
                        if (isset($uidlist) && !empty($uidlist)) {
                            //foreach block start
                            foreach ($uidlist as $value) { ?>
                                <option value="<?php echo $value['registeration_no']; ?>" <?php if ($uid == $value['registeration_no']) {
                                                                                                echo 'selected';
                                                                                            } ?>><?php echo $value['registeration_no']; ?></option>
                        <?php
                                //fr each block end
                            }
                            //if block end 
                        }
                        ?>
                    </select>

                </div>
            </div>
            
            <div class="fltr-field-wrap">
                <label class="admin-label">District</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker distict" data-live-search="true" name="distict" id="distict">
                        <option value="">All</option>
                        <?php
                        //if block start
                        if (isset($districtlist) && !empty($districtlist)) {
                            //foreach block start
                            foreach ($districtlist as $districtKey => $districtVal) { ?>
                                <option value="<?php echo $districtVal['district_id']; ?>" <?php if ($distict == $districtVal['district_id']) {
                                                                                                echo 'selected';
                                                                                            } ?>><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                        <?php
                                //fr each block end
                            }
                            //if block end 
                        }
                        ?>
                    </select>
                </div>
            </div>
           
            
            <div class="fltr-field-wrap">
                <label class="admin-label">Created On</label>
                <div class="inputfield-wrap">
                    <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                </div>

            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                </div>
            </div>


            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyFilter" id="filterbutton" name="filter">Apply</button>
            </div>

        </div>
    </div>
    <!--Table-->
    <label id="error">
        <div class="alert alert-success" style="display:none" role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"></span>
            </strong>
            <span class="alertText"></span>
        </div>
    </label>
    <div class="white-wrapper">
        <!-- <div class="tt-count"><span><i class="fa fa-facebook"></i> <?php echo $taskTotalData['totalFacebookTask'] ?></span>
            <span> <i class="fa fa-twitter"></i> <?php echo $taskTotalData['totalTwitterTask']  ?></span>
            <span> <i class="fa fa-whatsapp"></i> <?php echo $taskTotalData['totalWhatsappTask']  ?></span>
            <span><i class="fa fa-globe"></i> <?php echo $taskTotalData['totalOfflineTask']  ?> </span>
            <span><i class="fa fa-youtube"></i> <?php echo $taskTotalData['totalYoutubeTask']  ?></span>
            <span><i class="fa fa-wpforms"></i> <?php echo $taskTotalData['totalOnlineTask']  ?></span>

        </div> -->
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No </th>
                        <th>UID</th>
                        <th>User Name</th>
                        <th>District</th>
                        <th>Remarks</th>
                        <th><a href="<?php base_url() ?>admin/user_uploads?data=<?php echo queryStringBuilder("field=startdate&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_startdate; ?>">Added On</a></th>
                        <th>Action </th>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($userUploadsList) && count($userUploadsList)) {
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($userUploadsList as $value) {
                    ?>
                            <tr>
                                <td><?php echo $i; ?> </td>
                                <td><?php echo $value['registeration_no']; ?> </td>
                                <td><?php echo $value['full_name']; ?> </td>
                                <td><?php echo $value['district_name']; ?> </td>
                                <td><?php 
                                        if(strlen($value['remarks']) > 50){
                                            echo substr($value['remarks'], 0,50) . '...';
                                        }else{
                                            echo $value['remarks'];
                                        }
                                    ?> 
                                </td>
                                <td><?php echo mdate(DATE_FORMAT, strtotime($value['created_date'])); ?></td>
                                <td>
                                    <!-- <a href="javascript:void(0);" class="table_icon" title="Delete"><i class="fa fa-trash" aria-hidden="true" onclick="deleteUser( 'task',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($value['user_uploads_id']); ?>', 'req/change-user-status', 'Do you really want to delete this task?' );"></i></a> -->
                                    <a href="<?php echo base_url() ?>admin/user_uploads/userUploadsDetail?data=<?php echo queryStringBuilder("id=" . $value['user_uploads_id']); ?>" class="table_icon" title="View User Uploads Detail"><i class="fa fa-eye" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                        <?php
                            $i++;
                        }
                    } else { ?>

                        <td colspan="11"><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></td>
                    <?php
                    } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>

        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>

<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/js/user_uploads.js"></script>

<script>
    $(document).ready(function() {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#startDate').datepicker({
            onRender: function(date) {
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            $('#endDate').val('');
            if (ev.date.valueOf() < checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#endDate')[0].focus();
        }).data('datepicker');
        var checkout = $('#endDate').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


        //on datepicker 2 focus
        $('#datepicker_2').focus(function() {
            if ($('#datepicker_1').val() == '') {
                checkout.hide();
            }
        });
        //prevent typing datepicker's input
        $('#datepicker_2, #datepicker_1').keydown(function(e) {
            e.preventDefault();
            return false;
        });

    });
</script>
