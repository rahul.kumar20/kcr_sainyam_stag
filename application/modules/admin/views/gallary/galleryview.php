<div class="inner-right-panel">
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="method" value='<?php echo isset($method)?$method:''; ?>'>

<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> Gallery</li>
        </ol>
    </div>
    <div class="white-wrapper">
        <div class="clearfix">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-item-title col-sm-6">
                        <h3 class="title"> Gallery</h3>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="admin/gallary" class="commn-btn save">Add Photos/Videos</a>
                        <div class="top-opt-wrap text-right">
                            <ul>
                                <li>
                                    <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                                </li>                       
                            </ul>
                         </div>
                    </div>
                   
                </div>
            </div>
        </div>
            <div class="filter-wrap ">
                <div class="filter_hd clearfix">
                    <div class="pull-left">
                        <h2 class="fltr-heading">Filter</h2>
                    </div>
                    <div class="pull-right">
                        <span class="close flt_cl" data-dismiss="modal">X</span>
                    </div>
                </div>
                <div class="inner-filter-wrap">

                    <div class="fltr-field-wrap">
                        <label class="admin-label">Type</label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker filter status" name="status">
                                <option value="">All</option>
                                <option <?php echo ($status == IMAGE) ? 'selected' : '' ?> value="1">Image</option>
                                <option <?php echo ($status == VIDEO) ? 'selected' : '' ?> value="2">Video</option>
                            </select>

                        </div>
                    </div>
                    <div class="fltr-field-wrap">
                        <label class="admin-label">Added From</label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker filter type" name="type">
                                <option value="">All</option>
                                <option <?php echo ($type == GALLERY_SECTION) ? 'selected' : '' ?> value="1">Gallery</option>
                                <option <?php echo ($type == HOME_SECTION) ? 'selected' : '' ?> value="2">Home Section</option>
                                <option <?php echo ($type == TASK_SECTION) ? 'selected' : '' ?> value="3">Task Section</option>

                            </select>

                        </div>
                    </div>
                    <div class="button-wrap text-center">
                        <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                        <button type="submit" class="commn-btn save applyFilterGallary" id="filterbutton" name="filter">Apply</button>
                    </div>
               </div>
            </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Images/Videos</h3>
            </div>
        </div>
        <div class="row">
        
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <ul class="gallery-list">
                    <?php if (isset($gallarylist) && !empty($gallarylist)) {
                   //foreach start
                        foreach ($gallarylist as $value) {
                        ?>
                        <?php if ($value['media_type']==IMAGE) {?>
                    <li>
                        <figure class="img-wrap">
                            <a href="javascript:void(0)" onclick="copy(this)" data-src="<?php echo $value['gallary_media_url']?>" class="copy-btn copy-img-url">Copy</a>
                            <img src="<?php echo $value['gallary_media_url']?>" alt="Images" class="loader">
                        </figure>
                    </li>
                        <?php }?>
                        <?php if ($value['media_type']==VIDEO) {?>
                   <li>
                        <div class="video-wrap">
                            <a href="javascript:void(0)" onclick="copy(this)" data-src="<?php echo $value['gallary_media_url']?>" class="copy-btn video-copy-url">Copy</a>
                            <video controls>
                                <source src="<?php echo $value['gallary_media_url']?>" type="video/mp4">
                                <source src="<?php echo $value['gallary_media_url']?>" type="video/ogg">
                            </video>
                        </div>
                    </li>
                        <?php }?>
                        <?php }
                    }?>
                 </ul>
            </div>
            <div class="pagination_wrap clearfix">
        <?php echo $link; ?>

    </div>
        </div>
      <!-- <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Videos</h3>
            </div>
        </div>
       <!-- <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <ul class="gallery-list">
                    <li>
                        <video width="400" controls>
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                            <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                        </video>
                    </li>
                    <li>
                        <video width="400" controls>
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                            <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                        </video>
                    </li>
                    <li>
                        <video width="400" controls>
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                            <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                        </video>
                    </li>
                    <li>
                        <video width="400" controls>
                            <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
                            <source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
                        </video>
                    </li>
                </ul>
            </div>
        </div>!-->
    </div>
</div>
<script src="<?php echo base_url() ?>public/js/gallary.js"></script>

<style>
    .gallery-list li{
        overflow:hidden;
    }
    .gallery-list li img {
        height: 180px;
        object-fit: cover;
        object-position: center;
    }
    .gallery-list li video {
        width: 100%;
        height: 180px;
        object-fit: cover;
    }
    .video-wrap {
        display: inline-block;
        overflow: hidden;
        width: 100%;
        position:relative;
    }
    .img-wrap{
        position:relative;
    }
    .copy-btn{
        background: #60c6f3;
        color: #fff;
        border: solid 2px;
        font-size: 13px;
        font-weight: 400;
        padding: 8px 20px 8px;
        position: relative;
        text-align: center;
        line-height: 1.2;
        border-color: #60c6f3;
        display: inline-block;
        letter-spacing: 0.5px;
        position: absolute;
        right: 0;
        top: 0;
        z-index:99;
    }
    .copy-btn:hover,.copy-btn:link,.copy-btn:visited,.copy-btn:active,.copy-btn:focus{
        color:#ffffff;
        text-decoration:none;
    }
</style>
<script>
    $(document).ready(function(){
        $(".copy-btn").click(function(){
            $(".copy-btn").text("Copy");
            $(this).text("Copied");
        });
    });
    function copy(that){
        var newthat =   that.getAttribute("data-src");
        var inp =document.createElement('input');
        document.body.appendChild(inp)
        inp.value =newthat
        inp.select();
        document.execCommand('copy',false);
        inp.remove();
    }
</script>