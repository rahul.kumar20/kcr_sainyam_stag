<div class="inner-right-panel">
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="admin/gallary/viewGallery">Gallery</a></li>
            <li class="breadcrumb-item active">Add Gallery</li>
        </ol>
    </div>
    <?php echo form_open_multipart('admin/gallary/addGallary', array('id' => 'gallary_add_form', 'onsubmit' => 'return gallaryFormValidate();')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Gallery</h3>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <input type = "hidden" value="0" id="imageCnt">
                <input type = "hidden" value="0" id="videoCnt">
                <div class="row">
                    <div class="col-sm-12 image_upload">
                        <figure class="usr-dtl-pic task-upload-gallery">
                            <img src="public/images/placeholder2.png" id="superadminpic">
                            <label class="camera" for="upload-img"><i class="fa fa-camera" aria-hidden="true"></i></label>
                            <input type="file"  multiple name= "gallary_image[]" id="upload-img" style="display:none;">  
                        </figure>
                        <center><label class="alert-danger img_error"></label></center>
                        <div class="upload-prod-pic-wrap task-thumbnails">
                            <ul></ul>
                        </div>
                    </div>
                    <div id="uploadedfile"> 
                    <input type="file" id="field2" style="display:none"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 m-t-sm video_upload">
                <div class="form-group">
                <label class="admin-label">Upload Video</label>
                <div class="video-wrapper">
                    <img src="public/images/video-placeholder.png" class="videoplaceholder video-placeholder">
                    <video controls="" class="video-wrap">
                        <span class="trash-ico"></span>
                        <source src="" id="video_here">
                    </video>
                </div>
                <label class="commn-btn save browse-btn" for="upload-video">Browse</label>
                <input type="file" name="file[]" class="file_multi_video" accept="video/*" style="display:none;" id="upload-video">   
                </div>
                <label class="alert-danger upload_error"></label>
            </div>
        </div>
        <div class="form-ele-action-bottom-wrap btns-center clearfix">
            <div class="button-wrap text-center">
                <button type="button" class="commn-btn cancel" onclick="window.location.href='admin/gallary/viewGallery'">Cancel</button>
                <button type="submit" class="commn-btn save" id="add_gallary">Add</button>
            </div>
        </div>
    </div>
	<?php echo form_close(); ?>
</div>
<script src="<?php echo base_url() ?>public/js/gallary.js"></script>

<style>
   .video-wrap {
   display: none;
   }
</style>
<script>
    $("#upload-img").removeAttr('multiple');
    $(document).on("change", ".file_multi_video", function(evt) {
    
    var fileChooser = document.getElementById('upload-video');
    // var button = document.getElementById(elementID);
    // var results = document.getElementById(erroElement);
    var file = fileChooser.files[0];
    var fileType = file.type;

    var FileSize = file.size / 1024 / 1024; // in MB
    var blob = file; // See step 1 above
    var type='';
    var imageExt = ['image/png', 'image/gif','image/jpeg'];
    var videoExt = ['video/m4v', 'video/avi','video/mpg','video/mp4', 'video/webm'];
    var fileReader = new FileReader();
   //file type check
    if ($.inArray(fileType, videoExt) == -1) {
        alert(string.video_ext_err);
        $(".videoplaceholder").show();
        $(".video-wrap").hide(); 
        return false;
     } 
    //video size 100 mb
     if(FileSize >100) {
        alert(string.video_size);
        $(".videoplaceholder").show();
        $(".video-wrap").hide(); 
        return false;
     }
    fileReader.onloadend = function(e) {
    var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
    var header = "";
    for(var i = 0; i < arr.length; i++) {
     header += arr[i].toString(16);
    }
    switch (header) {
    case "89504e47":
        type = "image/png";
        break;
    case "47494638":
        type = "image/gif";
        break;
    case "ffd8ffe0":
    case "ffd8ffe1":
    case "ffd8ffe2":
    case "ffd8ffe3":
    case "ffd8ffe8":
        type = "image/jpeg";
        break;
    default:
        type = "unknown"; // Or you can use the blob.type as fallback
        break;
}
if($.inArray(type, imageExt) !== -1){
    alert(string.video_ext_err);
    $(".videoplaceholder").show();
    $(".video-wrap").hide(); 
    return false;
}
  // Check the file signature against known types

};
fileReader.readAsArrayBuffer(blob);
  var $source = $('#video_here');
  $source[0].src = URL.createObjectURL(this.files[0]);


  $source.parent()[0].load();
});


$(".browse-btn").click(function(){
    // alert("hi")
    var a =  $("#video_here").attr("src");

    if( a!="null"|| a!="unkown"){
        $(".videoplaceholder").hide();
        $(".video-wrap").show(); 
    }


});


</script>