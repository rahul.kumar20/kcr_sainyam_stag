<?php
?>
<div class="inner-right-panel">

    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/contactus">
                    Contact Us</a></li>
            <li class="breadcrumb-item active">
                Contact Detail</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->

    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title"> Contact Detail</h3>
        </div>
        <div class="form-ele-wrapper clearfix">
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Ticket Id</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($contactDetail['ticket_id']) && !empty($contactDetail['ticket_id']) ? $contactDetail['ticket_id'] : 'N/A'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($contactDetail['full_name']) && !empty($contactDetail['full_name']) ? $contactDetail['full_name'] : 'N/A'; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="input-holder">
                            <span class="show-label"><?php echo isset($contactDetail['registeration_no']) && !empty($contactDetail['registeration_no']) ? $contactDetail['registeration_no'] : 'N/A';; ?>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Contact On</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo date("d M Y H:i a", strtotime($contactDetail['inserted_on'])); ?>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Category Name</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo (isset($contactDetail['category_name']) && !empty($contactDetail['category_name']) ? $contactDetail['category_name'] : 'Not Available'); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Content</label>
                        <div class="input-holder">
                            <span class="show-label">
                                <?php echo (isset($contactDetail['contact_us_content']) && !empty($contactDetail['contact_us_content']) ? $contactDetail['contact_us_content'] : 'Not Available'); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_open_multipart('', array('id' => 'version_add_form')); ?>
            <div class="form-group m-t-md">
                <label class="admin-label">Send Reply</label>
                <div class="input-holder">
                    <textarea class="custom-textarea" name="reply" placeholder="Enter Comment Here" id="reply" maxlength="200" required></textarea>
                    <input type="hidden" name="token" id="token" value="<?php echo encryptDecrypt($user_id); ?>">
                    <input type="hidden" name="phone_no" id="phone_no" value="<?php echo $contactDetail['phone_number'] ?>">
                </div>
            </div>
            <div class="button-wrap text-center">
                <button type="submit" class="commn-btn reply_btn" name="reply_btn" id="reply_btn">Reply</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script>
    $('img').load(function() {
        $(this).removeClass('loader');
    });
</script>