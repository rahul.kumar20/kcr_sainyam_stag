<?php
$filterArr = $this->input->get();
$filterArr = (object) $filterArr;
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterArr); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<input type="hidden" value="<?php echo $csrfToken; ?>" name="csrf" id="csrf">
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Contact Us</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <?php if (!empty($this->session->flashdata('message_success'))) {
    ?>
        <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success');$this->session->unset_userdata('message_success'); ?>
        </div>
    <?php } ?>
    <!--Filter Section -->
    <div class="fltr-srch-wrap clearfix white-wrapper">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <form action="">
                    <div class="srch-wrap col-sm-space">
                        <button class="srch search-icon" style="cursor:default"></button>
                        <a href="javascript:void(0)"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                        <input type="text" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by User Name, category, ticket_id" id="searchuser" name="search">
                    </div>
                </form>
            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                    <div class="go_back">Go Back</div>
                <?php } ?>

            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="top-opt-wrap text-right">

                    <ul>
                        <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

    <!--Filter Section Close-->
    <!--Filter Wrapper-->
    <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">
            <div class="fltr-field-wrap">
                <label class="admin-label">Added Date</label>
                <div class="inputfield-wrap">
                    <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                </div>
            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                </div>
            </div>
            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyfilter" id="filterbutton" name="filter">Apply</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">Total Content: <?php echo $totalrows ?></div>
    </div>
    <!--Table-->
    <div class="clearfix white-wrapper">
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Ticket Id</th>
                        <th>
                            <a href="<?php base_url() ?>admin/contactus?data=<?php echo queryStringBuilder("field=name&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_name; ?>">User Name</a>
                        </th>
                        <th>
                            Category Name
                        </th>
                        <th>Contact Us Content</th>

                        <th>
                            <a href="<?php base_url() ?>admin/contactus?data=<?php echo queryStringBuilder("field=added&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_date; ?>">Created Date</a>
                        </th>
                        <th>Action</th>
                    </tr>

                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($contactus['result']) && count($contactus['result']) > 0) :
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($contactus['result'] as $key => $value) :
                    ?>

                            <td><?php echo $i++; ?></td>
                            <td><?php echo !empty($value['ticket_id']) ? $value['ticket_id'] : 'Not availavle'; ?></td>
                            <td><?php echo $value['full_name']; ?></td>
                            <td><?php echo $value['category_name']; ?></td>
                            <td>
                                <span class="td-text-wrap" title="<?php echo !empty($value['contact_us_content']) ? $value['contact_us_content'] : "Not Available"; ?>"><?php echo  $value['contact_us_content']; ?></span>
                            </td>
                            <td><?php echo mdate(DATE_FORMAT, strtotime($value['inserted_on'])); ?></td>
                            <td>
                                <a href="<?php echo base_url() ?>admin/contact-detail?data=<?php echo queryStringBuilder("id=" . $value['contact_id']); ?>" class="table_icon" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </td>
                            </tr>
                    <?php
                        endforeach;
                    else :
                        echo '<tr><td colspan="9" class="text-center">No result found.</td></tr>';
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>
    </div>
    <!-- table 1 close-->
</div>
<!--Table listing-->

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script>
    $(document).ready(function() {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#startDate').datepicker({
            onRender: function(date) {
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            $('#endDate').val('');
            if (ev.date.valueOf() < checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#endDate')[0].focus();
        }).data('datepicker');
        var checkout = $('#endDate').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


        //on datepicker 2 focus
        $('#datepicker_2').focus(function() {
            if ($('#datepicker_1').val() == '') {
                checkout.hide();
            }
        });
        //prevent typing datepicker's input
        $('#datepicker_2, #datepicker_1').keydown(function(e) {
            e.preventDefault();
            return false;
        });
    });
</script>