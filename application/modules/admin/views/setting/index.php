<!-- Inner Right Panel Start Here -->
<div class="inner-right-panel">
    <!--Breadcrumb Start Here -->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Settings</li>
        </ol>
    </div>
     <?php
     if ($this->session->flashdata('message') != '') {
         echo $this->session->flashdata('message');
     }
    ?>
    <!--Breadcrumb End Here -->
    <?php echo form_open('', array('id'=>'settings_change_form'));?>
    <!-- Form Section Start Here -->
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Admin Settings</h3>
        </div>
        <div class="white-wrapper change-password clearfix">
            <div class="row">
            <input type='hidden' name="settingId" value="<?php echo (isset($settingsData['id']) && !empty($settingsData['id'])?encryptDecrypt($settingsData['id']):'')?>">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Helpline Number<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" maxlength="10" autocomplete='off' class="form-control material-control" name="helplineNumber" value="<?php echo (isset($settingsData['help_line_number']) && !empty($settingsData['help_line_number'])?$settingsData['help_line_number']:set_value('helplineNumber'));?>">
                            <?php echo isset($error_message)?'<label class="alert-danger">'.$error_message.'</label>':form_error('helplineNumber', '<label class="alert-danger">', '</label>');?>
                            <!-- <span class="error_wrap"></span> -->
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Email<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" maxlength="255" class="form-control material-control" autocomplete='off' name="email" value="<?php echo (isset($settingsData['help_line_mail']) && !empty($settingsData['help_line_mail'])?$settingsData['help_line_mail']:set_value('email'));?>">
                            <?php echo form_error('email', '<label class="alert-danger">', '</label>');?>
                            <!-- <span class="error_wrap"></span> -->
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="row">
             <div class="col-sm-6 col-xs-12">
                    <div class="button-wrap chng-pass text-center">
                        <button type="submit" class="commn-btn save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Form Section End Here -->
</div>
<?php echo form_close();?>

<script src="<?php echo base_url() ?>public/js/settings.js"></script>

<!-- Inner Right Panel End Here -->