<?php
$showAction = $permission['action'];
?>
<input type="hidden" id="stateId" value='<?php echo isset($state)?$state:''; ?>'>
   <input type="hidden" id="districtId" value='<?php echo isset($distict)?$distict:''; ?>'>
   <input type="hidden" id="collegeId" value='<?php echo isset($college)?$college:''; ?>'>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/redards_history">Rewards Management</a></li>

        <li class="breadcrumb-item">Payment History</li>
        </ol>
    </div>
    <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
                        <div class="alert alert-success" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                        </div>
        <?php

    } ?>
       
        <?php if (!empty($this->session->flashdata('message_error'))) {
            ?>
                        <div class="alert alert-danger" style="display:block;">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
                        </div>
        <?php

    } ?>
    <!--Filter Section -->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by payment ID" id="searchuser" name="search" autocomplete="off">
                </div>

            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                     <div class="go_back">Go Back</div>
                    <?php
            } ?>

            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="top-opt-wrap text-right">
                    <ul>
                
                        <li>
                            <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Filter Section Close-->
        <!--Filter Wrapper-->
    <div class="filter-wrap ">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">

            <div class="fltr-field-wrap">
                <label class="admin-label">Date</label>
                <div class="inputfield-wrap">
                    <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                </div>

            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                </div>
            </div>
		
            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel" onclick="window.location.href='<?php echo base_url() ?>admin/payment_history'" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyPaymentHistory" id="filterbutton" name="filter">Apply</button>
            </div>

        </div>
    </div>

    <!--Table-->
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg'); ?>
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <div class="white-wrapper ">
        <p class="tt-count">Total Record: <?php echo $totalrows ?></p>
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th >S.No</th>
                        <th >Payment ID</th>
                        <th>
                            <a href="<?php base_url() ?>admin/payment_history?data=<?php echo queryStringBuilder("field=total_user&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_total_user; ?>">Total User Paid</a>
                        </th>
                        <th >
                            <a href="<?php base_url() ?>admin/payment_history?data=<?php echo queryStringBuilder("field=total_amount&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_total_amount; ?>">Total Amount Paid</a>
                       
                      
                        <th >
                            <a href="<?php base_url() ?>admin/payment_history?data=<?php echo queryStringBuilder("field=created_date&order=" . $order_by . $get_query); ?>" class="sort <?php echo $order_by_date; ?>">Date</a>
                        </th>
                     
                        <?php if ($showAction) { ?>
                             <th  class="text-center">Action</th>
                            <?php
                    } ?>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($paymenthistory) && count($paymenthistory)) {
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($paymenthistory as $value) {
                            ?>

                             <tr id ="remove_<?php echo $value['id']; ?>" >
                                 <td align='left'><span class="serialno"><?php echo $i; ?></span></td>
                                 <td><?php echo !empty($value['payment_id']) ? $value['payment_id'] : "Not Available"; ?></td>

                               
                                 <td><?php echo !empty($value['total_paid_user']) ? $value['total_paid_user'] : "0"; ?></td>
                                 <td><?php echo !empty($value['total_amount_paid']) ? $value['total_amount_paid'] : "0"; ?></td>
</td>
                                 <td><?php echo mdate(DATE_FORMAT, strtotime($value['created_date'])); ?></td>


                                 <?php if ($showAction) { ?>
                                     <td  class="text-center">
                                     <a href="<?php echo base_url() ?>admin/payment_history/detail?data=<?php echo queryStringBuilder("id=" . $value['payment_id']); ?>" class="table_icon" title="View"><i class="fa fa-eye" aria-hidden="true" ></i></a>

                                     </td>
                                    <?php
                               } ?>
                             </tr>
                                <?php
                                $i++;
                        }
                    } else {
                        ?>
                         <tr><td colspan="9" class="text-center">No result found.</td></tr
                        <?php
                    } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>

    </div>
</div>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script src="<?php echo base_url() ?>public/js/payment.js"></script>