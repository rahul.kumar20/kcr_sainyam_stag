<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>

<div class="inner-right-panel">
<input type="hidden" id="stateId" value='<?php echo isset($state) ? $state : ''; ?>'>
   <input type="hidden" id="districtId" value='<?php echo isset($distict) ? $distict : ''; ?>'>
   <input type="hidden" id="collegeId" value='<?php echo isset($college) ? $college : ''; ?>'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<?php //pr($profile);?>
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/payment_history">Payment History</a></li>
            <li class="breadcrumb-item active">Payment History Detail</li>
        </ol>
    </div>

    <!--Filter Section -->
   
</div>
    <div class="white-wrapper">
      <div class="form-item-title clearfix">
         <h3 class="title">Payment History </h3>
      </div>
      <!--Filter Section -->
      <div class="fltr-srch-wrap white-wrapper clearfix">
         <div class="row">
            <div class="col-lg-2 col-sm-3">
               <div class="display  col-sm-space">
                  <select class="selectpicker dispLimit">
                     <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                     <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                     <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                     <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                  </select>
               </div>
            </div>
            <div class="col-lg-4 col-sm-4">
             <!--  <div class="srch-wrap col-sm-space">
                  <button class="srch search-icon" style="cursor:default"></button>
                  <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                  <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by Title" id="searchuser" name="search" autocomplete="off">
               </div>-->
            </div>
            <div class="col-lg-2 col-sm-2">
               <?php if (isset($searchlike) && "" != $searchlike) { ?>
               <div onclick="window.location.href='<?php echo base_url() ?>admin/Reward_earned_history/paymentDetail?data=<?php echo queryStringBuilder("id=" . $payment_id); ?>'">Go Back</div>
               <?php

            } ?>
            </div>
            <div class="col-lg-4 col-sm-4">
               <div class="top-opt-wrap text-right">
                  <ul>
                     <li>
                        <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper2" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                     </li>
                     <li>
                        <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <!--Filter Section Close-->
      <!--Filter Wrapper-->
      <div class="filter-wrap ">
      <div class="filter_hd clearfix">
          <div class="pull-left">
              <h2 class="fltr-heading">Filter</h2>
          </div>
          <div class="pull-right">
              <span class="close flt_cl" data-dismiss="modal">X</span>
          </div>
      </div>
      <div class="inner-filter-wrap">
      

          <div class="fltr-field-wrap">
              <label class="admin-label">Type</label>
              <div class="commn-select-wrap">
                  <select class="selectpicker filter taskType" name="taskType">
                      <option value="">All</option>
                      <option <?php echo ($taskType == '1') ? 'selected' : '' ?> value="1">Paytm</option>
                      <option <?php echo ($taskType == '2') ? 'selected' : '' ?> value="2">UPI Address</option>
                      <option <?php echo ($taskType == '3') ? 'selected' : '' ?> value="3">Bank Account</option>
                  </select>

              </div>
          </div>
          <div class="fltr-field-wrap">
              <label class="admin-label">Paid Date</label>
              <div class="inputfield-wrap">
                  <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
              </div>

          </div>
          <div class="fltr-field-wrap">
              <div class="inputfield-wrap">
                  <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
              </div>
          </div>
         
      
          <div class="button-wrap text-center">
              <button type="reset" class="commn-btn cancel"  onclick="window.location.href='<?php echo base_url() ?>admin/payment_history/detail?data=<?php echo queryStringBuilder("id=" . $payment_id); ?>'" id="resetbutton">Reset</button>
              <button type="submit" class="commn-btn save applyFilterPH" id="filterbutton" name="filter">Apply</button>
          </div>

      </div>
  </div>
      <div class="table-responsive custom-tbl">
         <!--table div-->
         <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
            <thead>
               <tr>
                  <th>S.No</th>
                  <th>Payment ID</th>
                  <th>Transaction ID</th>
                  <th>UID</th>
                  <th>Payment Amount</th>
				  <th>Previous Amount</th>
				  <th>Left Amount</th>
                  <th>Payment Date</th>
                  <th>Payment Option</th>
                  <th>Payment Detail</th>
                  <th>Status</th>
               </tr>
            </thead>
            <tbody id="table_tr">
                <?php 
                   //if block start
                if (isset($walletHistoryList) && !empty($walletHistoryList)) {
                    if ($page > 1) {
                        $i = (($page * $limit) - $limit) + 1;
                    } else {
                        $i = 1;
                    }
                      //for each start
                    foreach ($walletHistoryList as $value) {
                        ?>
               <tr id ="remove_<?php echo $value['user_id']; ?>" >
               <td align='left'><span class="serialno"><?php echo $i; ?></span></td>
               <td>
                   <?php echo !empty($value['payment_id']) ? $value['payment_id'] : "Not Available"; ?>     
               </td>     
               <td>    
                   <?php echo !empty($value['transaction_id']) ? $value['transaction_id'] : "Not Available"; ?>        
               </td>
               <td>
                <?php echo !empty($profile['registeration_no']) ? $profile['registeration_no'] : "Not Available"; ?>
              </td>
			   <td>
                <?php echo !empty($value['amount_paid']) ? $value['amount_paid'] : "Not Available"; ?>
              </td>
			   <td>
                <?php echo !empty($value['user_available_amount']) ? $value['user_available_amount'] : "Not Available"; ?>

              </td>
			   <td>
                   
                <?php echo !empty($value['user_left_amount']) ? $value['user_left_amount'] : "Not Available"; ?>
                   
              </td>
             
              
               <td align='left'>
               <?php echo mdate(DATE_FORMAT, strtotime($value['paid_date'])); ?></td>
               </td>
			    <td>
                   
                <?php echo !empty($value['payment_option']) ? $paymentOption[$value['payment_option']] : "Not Available"; ?>
                   
              </td>
			   <td>
                   
                <?php echo !empty($value['payment_detail']) ? $value['payment_detail'] : "Not Available"; ?>
                   
              </td>
               
               <td align='left'>
                  <?php echo !empty($value['status']) ? $statusPayment[$value['status']] : "Not Available"; ?>

               </td>

              
           </tr>

                <?php
                   //for each end
                $i++;
            }
                  //if block end
        } else {
            ?>
               <tr>
               <td colspan="9"><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></td>

               </tr>
                  <?php 
                } ?>
            </tbody>
         </table>
      </div>
    <!--form element wrapper end-->
</div>
                  </div>
                  <script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>

                  <script src="public/js/reward.js"></script>

<script>
    $('img').load(function(){
       $('.form-profile-pic-wrapper').removeClass('loader');
    });
    </script>