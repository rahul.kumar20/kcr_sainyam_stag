<div class="inner-right-panel">
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/level">Level Management</a></li>
            <li class="breadcrumb-item active">Add Level</li>
        </ol>
    </div>
    <?php echo form_open_multipart('', array('id' => 'cms_add_form')); ?>
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Level</h3>
        </div>
        <div class="white-wrapper clearfix">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Name</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" name="name" value="<?php echo set_value('name'); ?>">
                            <?php echo form_error('name', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Name (Tamil)</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" name="name_tn" value="<?php echo set_value('name_tn'); ?>">
                            <?php echo form_error('name_tn', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Free Reward Credit</label>
                        <div class="input-holder">
                            <input type="number" class="form-control material-control" name="free_reward_points" value="<?php echo set_value('free_reward_points'); ?>">
                            <?php echo form_error('free_reward_points', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Start Reward Point Range</label>
                        <div class="input-holder">
                            <input type="number" class="form-control material-control" name="start_point" value="<?php echo set_value('start_point'); ?>">
                            <?php echo form_error('start_point', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">End Reward Point Range</label>
                        <div class="input-holder">
                            <input type="number" class="form-control material-control" name="end_point" value="<?php echo set_value('end_point'); ?>">
                            <?php echo form_error('end_point', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Incentive</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="incentive"><?php echo set_value('incentive'); ?></textarea>
                            <?php echo form_error('incentive', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Incentive (Tamil)</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="incentive_tn"><?php echo set_value('incentive_tn'); ?></textarea>
                            <?php echo form_error('incentive', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Description</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="description"><?php echo set_value('description'); ?></textarea>
                            <?php echo form_error('description', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="admin-label">Description (Tamil)</label>
                        <div class="input-holder">
                            <textarea class="custom-textarea" name="description_tn"><?php echo set_value('description_tn'); ?></textarea>
                            <?php echo form_error('description', '<label class="alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <label class="admin-label">Upload Image</label>
                    <figure class="usr-dtl-pic task-upload-gallery">
                        <a class="example-image-link" id="scandocument" href="assets/images/contract.png" data-lightbox="example-1">
                            <img class="example-image" src="public/images/placeholder2.png" id="scancontract-image"></a>
                        <label class="camera" for="scancontract"><i class="fa fa-camera" aria-hidden="true"></i></label>
                        <input type="file" id="scancontract" name="user_image" onchange="loadFile(event)" style="display:none;">
                    </figure>
                </div>
            </div>
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="button" onclick="window.location.href = '<?php echo base_url() ?>admin/level'" class="commn-btn cancel">Cancel</button>
                    <button type="submit" class="commn-btn save">Save</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    var loadFile = function(event) {
        var output = document.getElementById('scancontract-image');
        var file = event.target.files[0];
        var fileType = file.type;
        var FileSize = file.size / 1024 / 1024; // in MB
        var blob = file; // See step 1 above
        var type = '';
        var imageExt = ['image/png', 'image/jpeg'];
        var fileReader = new FileReader();
        if ($.inArray(fileType, imageExt) == -1) {
            alert(string.video_ext_err);
            return false;
        }
        fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
            var header = "";
            for (var i = 0; i < arr.length; i++) {
                header += arr[i].toString(16);
            }
            //file mime type validation
            switch (header) {
                case "89504e47":
                    type = "image/png";
                    break;
                case "47494638":
                    type = "image/gif";
                    break;
                case "ffd8ffe0":
                case "ffd8ffe1":
                case "ffd8ffe2":
                case "ffd8ffe3":
                case "ffd8ffe8":
                    type = "image/jpeg";
                    break;
                default:
                    type = "unknown"; // Or you can use the blob.type as fallback
                    break;
            }

            console.log(type);
            //validation for myme type check
            if ($.inArray(type, imageExt) == -1) {
                alert(string.video_ext_err);

                return false;
            }
            // Check the file signature against known types

        };
        fileReader.readAsArrayBuffer(blob);

        var scandocument = document.getElementById('scancontract-image');

        output.src = URL.createObjectURL(event.target.files[0]);
        scandocument.href = URL.createObjectURL(event.target.files[0]);
        $("#scandocument").attr("href", scandocument.href)
    };
</script>