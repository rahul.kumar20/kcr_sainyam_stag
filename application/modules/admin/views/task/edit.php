<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/task">Task Content</a></li>
            <li class="breadcrumb-item active">Task Content</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'task_add_form')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Task  Content</h3>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Content Info</h3>
            </div>
        </div>
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php

        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Task Title<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title" id="title" value="<?php echo (isset($taskDetail['task_title']) && !empty($taskDetail['task_title']) ? $taskDetail['task_title'] : ''); ?>" placeholder="Enter Title">
                            <?php echo form_error('title', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Task Description<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title_desc" id="title_desc" value="<?php echo (isset($taskDetail['task_description']) && !empty($taskDetail['task_description']) ? $taskDetail['task_description'] : ''); ?>" placeholder="Enter Description" required>
                            <?php echo form_error('description', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
               
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">points</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter Points" name="points" id="points" value="<?php echo (isset($taskDetail['points']) && !empty($taskDetail['points']) ? $taskDetail['points'] : ''); ?>">
                            <?php echo form_error('points', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            
                <input type='hidden' value='<?php echo encryptDecrypt($task_id); ?>' name='token'>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                     <div class="form-group"> 
                        <label class="admin-label">Deadline Date</label>
                        <div class="input-holder">
                        <input type="text" name="DeadlineDate" autocomplete="off" data-provide="datepicker" class="form-control DeadlineDate" id="DeadlineDate" placeholder="Deadline Date" value="<?php echo (isset($taskDetail['end_date']) && !empty($taskDetail['end_date']) ? $taskDetail['end_date'] : ''); ?>">
                            <?php echo form_error('DeadlineDate', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Status<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="task_status">
                                <option value="1" <?php if ($taskDetail['task_status'] == ACTIVE) {
                                                                            echo 'selected';
                                                                        } ?>>Active</option>
                                <option value="2" <?php if ($taskDetail['task_status'] == INACTIVE) {
                                                                            echo 'selected';
                                                                        } ?>>InActive</option>
                            </select>
                        </div>
                    </div>
                </div>
                </div>
            <div class="row form-ele-wrapper clearfix">
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="button" onclick="window.location.href='<?php echo base_url() ?>admin/task'" class="commn-btn cancel">Cancel</button>
                    <button type="submit" class="commn-btn save" id="add_task">Update</button>
                </div>
            </div>
            <!--form ele wrapper end-->
        </div>
     </div>
    <!--close form view   -->
    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>
</div>

<script src="<?php echo base_url() ?>public/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/js/cropper.min.js"></script>

<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script>
        $(function() {
            var currentdate = $("#DeadlineDate").value;
            // dated = formatDate(dated)
            $('.DeadlineDate').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss',
                // defaultDate: date,
                minDate: currentdate
            });
        });
</script>