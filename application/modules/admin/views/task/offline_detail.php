<?php //pr($task);
; ?>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
   <div class="fltr-srch-wrap white-wrapper clearfix">
      <!--breadcrumb wrap-->
      <div class="breadcrumb-wrap">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/task">Task</a></li>
            <li class="breadcrumb-item "><a href="<?php echo base_url() ?>admin/task/taskDetail?data=<?php echo queryStringBuilder("id=" . $task_id); ?>">Task Detail</a></li>
            <li class="breadcrumb-item active">User Upload Detail</li>
         </ol>
      </div>
      <?php if (!empty($this->session->flashdata('message_success'))) {
      ?>
         <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
         </div>
      <?php

      } ?>

      <?php if (!empty($this->session->flashdata('message_error'))) {
      ?>
         <div class="alert alert-danger" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
         </div>
      <?php

      } ?>
      <div class="white-wrapper">
         <div class="form-item-title clearfix">
            <h3 class="title">User Upload Detail</h3>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="upload-pic-wrapper">

                  <?php if (!empty($task['media_url'])) {
                     $task_media = explode(',', $task['media_url']);
                     foreach ($task_media as $medVal) { ?>
                        <?php
                        $ext = pathinfo($medVal, PATHINFO_EXTENSION);
                        if ($ext == 'mp4' || $ext == '3gp') { ?>
                           <video width="320" height="240" controls>
                              <source src="<?php echo $medVal; ?>" type="video/mp4">
                              Your browser does not support the video tag.
                           </video>
                        <?php } else { ?>
                           <figure class="show-img">
                              <a class="example-image-link" href="<?php echo $medVal; ?>" data-lightbox="example-set">
                                 <img class="example-image loader" data-lightbox="example-set" src="<?php echo $medVal; ?>">
                              </a>
                           </figure>
                        <?php } ?>
                  <?php
                     }
                  } ?>
               </div>
            </div>
            <div class="col-sm-8">
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task Title</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['task_title']) && !empty($task['task_title']) ? $task['task_title'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task Id</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['task_code']) && !empty($task['task_code']) ? $task['task_code'] : '--') ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task Live Date</label>
                        <div class="input-holder">
                           <div class="inputfield-wrap">
                              <span class="text-detail"><?php echo (isset($task['start_date']) && !empty($task['start_date']) ? mdate(TASK_DATE_FORMAT, strtotime($task['start_date'])) : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Deadline Date</label>
                        <div class="input-holder">
                           <div class="inputfield-wrap">
                              <span class="text-detail"><?php echo (isset($task['end_date']) && !empty($task['end_date']) ? mdate(TASK_DATE_FORMAT, strtotime($task['end_date'])) : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Points</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['points']) && !empty($task['points']) ? $task['points'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Remarks</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task_details['remarks']) && !empty($task_details['remarks']) ? $task_details['remarks'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <!-- <div class="col-sm-6 col-xs-6 m-t-sm">
                  <div class="form-group">
                     <label class="admin-label">PC</label>
                     <div class="commn-select-wrap">
                        <span class="text-detail">Test PC</span>
                     </div>
                  </div>
               </div> -->
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task for(Social Media)</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_type']) && !empty($task['task_type']) ? $task['task_type'] . '(' . $task['action'] . ')' : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Status</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_status']) && !empty($task['task_status']) ? $task_status[$task['task_status']] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Description</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_description']) && !empty($task['task_description']) ? $task['task_description'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">URL</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['post_url']) && !empty($task['post_url']) ? $task['post_url'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <?php if (isset($task['action']) && $task['action'] == 'qrcode') { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Downloard QR Code</label>
                           <div class="input-holder">
                              <a href="<?php echo $task['qr_code_url'] ?>" download>Downloard</a>
                           </div>
                        </div>
                     </div>
                  <?php } ?>

               </div>
            </div>
            <!-- <div class="form-ele-action-bottom-wrap btns-center clearfix">
         <div class="button-wrap text-center">
            <button type="submit" class="commn-btn save" id="add_news" onclick="deleteUser( 'task',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($task['task_id']); ?>', 'req/change-user-status', 'Do you really want to delete this task?' );">Delete Task</button>
         </div>
      </div> -->
         </div>
      </div>
   </div>
</div>