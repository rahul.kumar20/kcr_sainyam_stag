<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>

<div class="inner-right-panel">
    <?php //form open
    echo form_open_multipart('', array('id' => 'task_add_form')); ?>

    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Add Task</h3>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Task Info</h3>
            </div>
        </div>
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-6 col-xs-6 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Task Platform<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="taskType" id="taskType">
                                <option value="">Select Task Type</option>
                                <option value="<?php echo FACEBOOK_TASK; ?>"><?php echo $this->lang->line('facebook') ?></option>
                                <option value="<?php echo TWITTER_TASK ?>"><?php echo $this->lang->line('twitter') ?></option>
                                <option value="<?php echo WHATSAPP ?>"><?php echo $this->lang->line('whatsapp') ?></option>
                                <option value="<?php echo OFFLINE ?>"><?php echo $this->lang->line('offline') ?></option>
                                <option value="<?php echo YOUTUBE_TASK ?>"><?php echo $this->lang->line('youtube') ?></option>
                                <option value="<?php echo ONLINE ?>"><?php echo $this->lang->line('online') ?></option>
                                <option value="7"><?php echo $this->lang->line('default') ?></option>
                                <option value="8"><?php echo $this->lang->line('istagram') ?></option>
                            </select>
                            <?php echo form_error('taskType', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="facebookAction">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="faction" id="actionface" disabled>
                                <option value="">Select Action</option>
                                <option value="1"><?php echo $this->lang->line('like') ?></option>
                                <option value="2"><?php echo $this->lang->line('share') ?></option>
                                <option value="22"><?php echo $this->lang->line('post') ?></option>
                                <option value="4"><?php echo $this->lang->line('follow') ?></option>
                                <!-- <option value="9"><?php echo $this->lang->line('comment') ?></option> -->
                            </select>
                            <?php echo form_error('faction', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="twitterAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="taction" id="actiontwit">
                                <option value="">Select Action</option>
                                <option value="1"><?php echo $this->lang->line('like') ?></option>
                                <option value="9"><?php echo $this->lang->line('comment') ?></option>
                                <option value="5"><?php echo $this->lang->line('tweet'); ?></option>
                                <option value="3"><?php echo $this->lang->line('retweet'); ?></option>
                                <option value="31"><?php echo $this->lang->line('poll_retweet'); ?></option>
                                <option value="4"><?php echo $this->lang->line('follow'); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="whatsappAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker whts" name="whatsappaction" id="actionwhatsapp">
                                <option value="">Select Action</option>
                                <option value="20"><?php echo $this->lang->line('whatsapp_image') ?></option>
                                <option value="15"><?php echo $this->lang->line('whatsapp_video') ?></option>
                                <option value="16"><?php echo $this->lang->line('whatsapp_audio') ?></option>
                                <option value="19"><?php echo $this->lang->line('whatsapp_link') ?></option>
                                <option value="17"><?php echo $this->lang->line('whatsapp_text') ?></option>
                                <option value="18"><?php echo $this->lang->line('whatsapp_gif') ?></option>
                                <option value="6"><?php echo $this->lang->line('change_dp') ?></option>
                                <option value="13"><?php echo $this->lang->line('create_group') ?></option>
                                <option value="21"><?php echo $this->lang->line('whatsapp_status') ?></option>
                                <option value="14"><?php echo $this->lang->line('about_us') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="instagramAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker insta" name="instagramaction" id="actioninstagram">
                                <option value="">Select Action</option>
                                <option value="20"><?php echo $this->lang->line('whatsapp_image') ?></option>
                                <option value="15"><?php echo $this->lang->line('whatsapp_video') ?></option>
                                <option value="18"><?php echo $this->lang->line('whatsapp_gif') ?></option>
                                <option value="6"><?php echo $this->lang->line('change_dp') ?></option>
                                <option value="14"><?php echo $this->lang->line('about_us') ?></option>
                                <option value="1"><?php echo $this->lang->line('like') ?></option>
                                <option value="9"><?php echo $this->lang->line('comment') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="offlineAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="offlineaction" id="actionoffline">
                                <option value="">Select Action</option>
                                <option value="event"><?php echo $this->lang->line('event') ?></option>
                                <option value="8"><?php echo $this->lang->line('multimedia') ?></option>
                                <option value="11"><?php echo $this->lang->line('qrcode') ?></option>
                                <option value="20">Image</option>
                                <option value="15">Video</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="youtubeAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="youtbaction" id="actionyoutube">
                                <option value="">Select Action</option>
                                <option value="1"><?php echo $this->lang->line('like') ?></option>
                                <!-- <option value="9"><?php echo $this->lang->line('comment') ?></option> -->
                                <option value="10"><?php echo $this->lang->line('subscribe') ?></option>
                                <option value="2"><?php echo $this->lang->line('share') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="defaultAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="actiondefault" id="actiondefault">
                                <option value="">Select Action</option>
                                <option value="23"><?php echo $this->lang->line('install_twitter') ?></option>
                                <option value="24"><?php echo $this->lang->line('install_facebook') ?></option>
                                <option value="25"><?php echo $this->lang->line('install_whatsapp') ?></option>
                                <option value="26"><?php echo $this->lang->line('install_youtube') ?></option>
                                <option value="27"><?php echo $this->lang->line('follow_twitter') ?></option>
                                <option value="28"><?php echo $this->lang->line('follow_facebook') ?></option>
                                <option value="29"><?php echo $this->lang->line('subscribe_youtube') ?></option>
                                <option value="30"><?php echo $this->lang->line('upload_whatsapp') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6" id="offlineMediaType" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Media Type<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="mediatype" id="mediaType">
                                <option value="">Select Media Type</option>
                                <option value="<?php echo IMAGE; ?>"><?php echo $this->lang->line('image') ?></option>
                                <option value="<?php echo VIDEO ?>"><?php echo $this->lang->line('video') ?></option>
                                <!-- <option value="<?php echo AUDIO ?>"><?php echo $this->lang->line('audio') ?></option> -->
                            </select>
                            <?php echo form_error('faction', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 m-t-sm" id="onlineAction" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Action<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="onlineaction" id="actiononline">
                                <option value="12"><?php echo $this->lang->line('form') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6" id="onlineActionForm" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Select Form<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="onlineform" id="onlineform" onchange="getFormURL(this.value,'req/form-url')">
                                <option value="">Select Form</option>
                                <?php
                                //if block start
                                if (isset($formlist) && !empty($formlist)) {
                                    //foreach block formlist
                                    foreach ($formlist as $formkey => $formVal) { ?>
                                        <option value="<?php echo $formVal['fid']; ?>"><?php echo ucwords(strtolower($formVal['f_title'])); ?></option>
                                <?php
                                        //fr each block end
                                    }
                                    //if block end
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 numberform" style="display:none;">
                    <label class="admin-label">Number of times form fill<mark class="reject-cross">*</mark></label>
                    <div class="form-field-wrap">
                        <input type="text" name="no_of_time" id="no_of_time" iplaceholder="Enter number of times form fill" maxlength="5" onkeypress="return numeralsOnly(event)">
                    </div>
                </div>
                <div class="youtube_div hideinput">
                    <div class="col-sm-6 hideinput youtube_video_id">
                        <label class="admin-label">Video Id<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="videoId" placeholder="Enter youtube video id v=G0Hx6uN2AJE" >
                        </div>
                    </div>
                    <div class="col-sm-6 hideinput youtube_channel_id">
                        <label class="admin-label">Channel Id<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="channelId" placeholder="Enter youtube channel id like UCh7zogFL029_21h1_eZBXvw" >
                        </div>
                    </div>
                </div>
                <div class="m-t-sm twitter_follow_div hideinput">
                    <div class="col-sm-6">
                        <label class="admin-label">Twitter Username<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="twitterAccountName" placeholder="Enter twitter account name" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="admin-label">Twitter Account ID<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="twiiterAccountID" placeholder="Twitter account ID"  onkeypress="return numeralsOnly(event)">
                        </div>
                    </div>
                </div>
                <div class="m-t-sm facebook_follow_div hideinput">
                    <div class="col-sm-6">
                        <label class="admin-label">Facebook Page Name<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="facebookAccountName" placeholder="Enter facebook page name" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="admin-label">Facebook Page ID<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="facebookAccountID" placeholder="Facebook page ID"  onkeypress="return numeralsOnly(event)">
                        </div>
                    </div>
                </div>
                <div class="mapdiv hideinput">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="admin-label">Map<mark class="reject-cross">*</mark></label>
                            <div class="form-field-wrap">
                                <input type="text" placeholder="Search Location" id="location">
                                <label class="alert-danger map_error"></label>
                                <div class="video-wrapper map" id="map_location"></div>
                            </div>
                        </div>
                        <input type='hidden' name="latitude" id='user_lat'>
                        <input type='hidden' name="longitude" id='user_long'>
                    </div>
                    <div class="col-sm-6">
                        <label class="admin-label">Radius<mark class="reject-cross">*</mark></label>
                        <div class="form-field-wrap">
                            <input type="text" name="radius" placeholder="Enter radius" maxlength="5" onkeypress="return numeralsOnly(event)">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Task Title<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter Task" name="taskName" id="task_name" maxlength="255" value="<?php echo set_value('taskName'); ?>">
                            <?php echo form_error('taskName', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Push Notification Title (Optional)</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="notif_title" id="notif_title" placeholder="Enter Push Notification Title">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Push Notification Description</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="notif_desc" id="notif_desc" placeholder="Enter Push Notification Description">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Greeting Message</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="greeting" id="greeting" placeholder="Enter Greeting Message">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group">
                        <div class="th-checkbox">
                            <input style="display: none;" class="filter-type filled-in" checked="" type="checkbox" name="is_send_notif" id="is_send_notif" value="">
                            <label for="is_send_notif" class="lbl-check"><span></span>Want to send Push Notification?</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 post_url" style="display:none">
                    <div class="form-group">
                        <label class="admin-label "><span id="urltext">URL</span><mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter URL" name="url" id="url" value="<?php echo set_value('url'); ?>">
                            <?php echo form_error('url', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-t-sm">
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Task Live Date<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <div class="inputfield-wrap">
                                <input type="text" name="startDate" autocomplete="off" data-provide="datepicker" value="" class="form-control startDate start_date" id="startDate" placeholder="From" value="<?php echo set_value('startDate'); ?>">
                                <?php echo form_error('startDate', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Deadline Date<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <div class="inputfield-wrap">
                                <input type="text" name="endDate" autocomplete="off" data-provide="datepicker" value="" class="form-control endDate end_date" id="endDate" placeholder="To" value="<?php echo set_value('endDate'); ?>">
                                <?php echo form_error('endDate', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Points<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter Points" name="taskPoints" id="task_points" maxlength="5" onkeypress="return numeralsOnly(event)" value="<?php echo set_value('taskPoints'); ?>">
                            <?php echo form_error('taskPoints', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label class="admin-label">Display Popup Notification? <mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <select class="selectpicker" name="show_popup" id="show_popup">
                                <option value=""></option>
                                <option value="1">Yes</option>
                                <option value="0" selected>No</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Target PTA</h3>
            </div>
        </div>
        <div class="form-section-wrap">
            <div class="row">
                <div class="clearfix"></div>
                <!-- <div class="col-sm-6 col-xs-6 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">State(optional)</label>
                        <select class="selectpicker filter state" id="state" data-live-search="true" name="state"  onchange="getDistrictForState('req/getdistrictbystate',this.value,'')">
                            <option value="">Select State</option>
                            <?php
                            //if block start
                            if (isset($statelist) && !empty($statelist)) {
                                //foreach block start
                                foreach ($statelist as $stateKey => $stateVal) { ?>
                                    <option value="<?php echo $stateVal['state_id']; ?>"><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                            <?php
                                    //fr each block end
                                }
                                //if block end
                            }
                            ?>
                        </select>
                        <?php echo form_error('state', '<label class=" alert-danger">', '</label>'); ?>
                    </div>
                </div> -->
                <div class="col-sm-6 col-xs-4 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">District(optional)</label>
                        <select class="selectpicker filter distict" data-live-search="true" name="distict" id="distict">
                            <option value="">Select District</option>
                            <?php
                            //if block start
                            if (isset($districtlist) && !empty($districtlist)) {
                                //foreach block start
                                foreach ($districtlist as $districtKey => $districtVal) { ?>
                                    <option value="<?php echo $districtVal['district_id']; ?>"><?php echo ucwords(strtolower(ltrim($districtVal['district_name'],"/"))); ?></option>
                            <?php
                                    //fr each block end
                                }
                                //if block end
                            }
                            ?>
                        </select>
                        <?php echo form_error('distict', '<label class=" alert-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-4 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">AC(optional)</label>
                        <select class="selectpicker filter ac" data-live-search="true" name="ac" id="ac">
                            <option value="">Select AC</option>
                        </select>
                        <?php echo form_error('ac', '<label class=" alert-danger">', '</label>'); ?>
                    </div>
                </div>
                <!-- <div class="col-sm-6 col-xs-4 m-t-sm" id="collegeName">
                    <div class="form-group">
                        <label class="admin-label">College Name(optional)</label>
                        <div class="input-holder">
                            <select class="selectpicker" name="college_name" id="collegeDa" data-live-search="true"></select>
                            <?php echo form_error('college_name', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div> -->
                <div class="col-sm-6 col-xs-6 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Gender(optional)</label>
                        <select class="selectpicker filter gender" name="gender">
                            <option value="">Select Gender</option>
                            <option value="<?php echo MALE_GENDER ?>">Male</option>
                            <option value="<?php echo FEMALE_GENDER ?>">Female</option>
                            <option value="<?php echo OTHER_GENDER ?>">Others</option>
                        </select>
                        <?php echo form_error('gender', '<label class=" alert-danger">', '</label>'); ?>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-sm-6 col-xs-4 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">UID(optional)</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add commn-btn save">Import
                            </a>
                            <input type='text' name="uid" id="uid" value="">
                            <?php echo form_error('uid', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">Status<mark class="reject-cross">*</mark></label>
                            <div class="commn-select-wrap">
                                <select class="selectpicker" name="status">
                                    <option value="<?php echo ACTIVE ?>">Active</option>
                                    <!-- <option value="<?php echo INACTIVE ?>">Inactive</option> -->
                                </select>
                                <?php echo form_error('status', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Other Details</h3>
            </div>
        </div>
        <div class="form-section-wrap space-bottom">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Description<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <textarea class="custom-textarea" rows="5" name="taskDescription"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm post_content_div" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Post Content <mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <textarea class="custom-textarea" rows="5" name="post_content"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 m-t-sm video_upload" style="display:none">
                    <div class="form-group">
                        <label class="admin-label">Upload Video</label>
                        <div class="video-wrapper">
                            <img src="<?php echo base_url() ?>public/images/video-placeholder.png" class="videoplaceholder video-placeholder">
                            <video controls="" class="video-wrap">
                                <span class="trash-ico"></span>
                                <source src="" id="video_here">
                            </video>
                        </div>
                        <label class="commn-btn save browse-btn" id="first_video" for="upload-video">Browse</label>
                        <input type="file" name="file[]" class="file_multi_video" accept="video/*" style="display:none;" id="upload-video">
                    </div>
                </div>
                <div class="col-sm-6">
                    <input type="hidden" value="0" id="imageCnt">
                    <input type="hidden" value="0" id="videoCnt">
                    <div class="row">
                        <div class="col-sm-12 image_upload" style="display:none">
                            <label class="admin-label">Upload Image</label>
                            <figure class="usr-dtl-pic task-upload-gallery">
                                <img src="public/images/placeholder2.png" id="superadminpic">
                                <label class="camera" for="upload-img"><i class="fa fa-camera" aria-hidden="true"></i></label>
                                <input type="file" multiple name="news_image[]" id="upload-img" style="display:none;">
                            </figure>
                            <center><label class="alert-danger img_error"></label></center>
                            <div class="upload-prod-pic-wrap task-thumbnails">
                                <ul></ul>
                            </div>
                        </div>
                        <div id="uploadedfile">
                            <input type="file" id="field2" style="display:none" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="whatsappUpload" style="display:none">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="admin-label">Upload Whatsapp Image</label>
                                <label class="commn-btn save browse-btn" for="whatsupload-img">Browse</label>
                                <input type="file" id="whatsupload-img" multiple style="display:none;" name="mul_whatsapp[]">
                                <div class="audio-wrapper"></div>
                            </div>
                            <span id="whatsapp_err" class="alert-danger"></span>
                        </div>
                        <div class="col-sm-5">
                            <div class="button-wrap upload2-prod-pic-wrap">
                                <ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hideinput audio">
                        <div class="form-group">
                            <label class="admin-label">Upload Audio</label>
                            <label class="commn-btn save browse-btn" for="audioinput">Browse</label>
                            <input type="file" id="audioinput" style="display:none" name="mul_audio[]">
                            <div class="audio-wrapper">
                                <audio controls id="sound">
                                    <source src="" type="audio/ogg">
                                    <source src="" type="audio/mpeg">
                                    Your browser does not support the audio element.
                                </audio>
                            </div>
                        </div>
                        <span id="audio_err" class="alert-danger"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-6 m-t-sm ">
                    <div class="form-group">
                        <label class="admin-label">Upload Instruction Video</label>
                        <div class="video-wrapper">
                            <img src="<?php echo base_url(); ?>public/images/video-placeholder.png" class="videoplaceholder video-placeholder">
                            <video controls="" class="video-wrap">
                                <span class="trash-ico"></span>
                                <source src="" id="instruction_video_here">
                            </video>
                        </div>
                        <label class="commn-btn save browse-btn" for="upload-instruction-video" id="ins_vdeo">Browse</label>
                        <input type="file" name="instruction_video_url[]" class="instruction_video_url" accept="video/*" style="display:none;" id="upload-instruction-video">
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Instruction Description</label>
                        <div class="commn-select-wrap">
                            <textarea class="custom-textarea" name="instruction_description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-ele-action-bottom-wrap btns-center clearfix">
            <div class="button-wrap text-center">
                <button type="button" class="commn-btn cancel">Cancel</button>
                <button type="submit" class="commn-btn save" id="add_task">Add Task</button>
            </div>
        </div>
    </div>
    <?php
    //form close
    echo form_close();
    ?>
</div>
<style>
    .video-wrap {
        display: none;
    }

    textarea.custom-textarea {
        width: 100%;
        border: 1px solid #e0dddd;
        font-size: 17px;
        padding: 10px;
    }
</style>
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>

<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="<?php echo base_url() ?>public/js/task.js"></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAHMFxkH08pIq4lffXwYymVWJ1NFtVbD3c'></script>

<script src="public/js/newmap.js"></script>
<script>
    $("#upload-img").removeAttr('multiple');
    $(document).on("change", ".file_multi_video", function(evt) {

        var fileChooser = document.getElementById('upload-video');
        // var button = document.getElementById(elementID);
        // var results = document.getElementById(erroElement);
        var file = fileChooser.files[0];
        var fileType = file.type;

        var FileSize = file.size / 1024 / 1024; // in MB
        var blob = file; // See step 1 above
        var type = '';
        var imageExt = ['image/png', 'image/gif', 'image/jpeg'];
        var videoExt = ['video/m4v', 'video/avi', 'video/mpg', 'video/mp4', 'video/webm'];
        var fileReader = new FileReader();
        //file type check
        if ($.inArray(fileType, videoExt) == -1) {
            alert(string.video_ext_err);
            $(".videoplaceholder").show();
            $(".video-wrap").hide();
            return false;
        }
        //video size 100 mb
        if (FileSize > 100) {
            alert(string.video_size);
            $(".videoplaceholder").show();
            $(".video-wrap").hide();
            return false;
        }
        fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
            var header = "";
            for (var i = 0; i < arr.length; i++) {
                header += arr[i].toString(16);
            }
            switch (header) {
                case "89504e47":
                    type = "image/png";
                    break;
                case "47494638":
                    type = "image/gif";
                    break;
                case "ffd8ffe0":
                case "ffd8ffe1":
                case "ffd8ffe2":
                case "ffd8ffe3":
                case "ffd8ffe8":
                    type = "image/jpeg";
                    break;
                default:
                    type = "unknown"; // Or you can use the blob.type as fallback
                    break;
            }
            if ($.inArray(type, imageExt) !== -1) {
                alert(string.video_ext_err);
                $(".videoplaceholder").show();
                $(".video-wrap").hide();
                return false;
            }
            // Check the file signature against known types

        };
        fileReader.readAsArrayBuffer(blob);
        var $source = $('#video_here');
        $source[0].src = URL.createObjectURL(this.files[0]);


        $source.parent()[0].load();
    });


    $("#upload-img").removeAttr('multiple');
    $(document).on("change", ".instruction_video_url", function(evt) {

        var fileChooser = document.getElementById('upload-instruction-video');
        // var button = document.getElementById(elementID);
        // var results = document.getElementById(erroElement);
        var file = fileChooser.files[0];
        var fileType = file.type;

        var FileSize = file.size / 1024 / 1024; // in MB
        var blob = file; // See step 1 above
        var type = '';
        var imageExt = ['image/png', 'image/gif', 'image/jpeg'];
        var videoExt = ['video/m4v', 'video/avi', 'video/mpg', 'video/mp4', 'video/webm'];
        var fileReader = new FileReader();
        //file type check
        if ($.inArray(fileType, videoExt) == -1) {
            alert(string.video_ext_err);
            $(".videoplaceholder").show();
            $(".video-wrap").hide();
            return false;
        }
        //video size 100 mb
        if (FileSize > 100) {
            alert(string.video_size);
            $(".videoplaceholder").show();
            $(".video-wrap").hide();
            return false;
        }
        fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
            var header = "";
            for (var i = 0; i < arr.length; i++) {
                header += arr[i].toString(16);
            }
            switch (header) {
                case "89504e47":
                    type = "image/png";
                    break;
                case "47494638":
                    type = "image/gif";
                    break;
                case "ffd8ffe0":
                case "ffd8ffe1":
                case "ffd8ffe2":
                case "ffd8ffe3":
                case "ffd8ffe8":
                    type = "image/jpeg";
                    break;
                default:
                    type = "unknown"; // Or you can use the blob.type as fallback
                    break;
            }
            if ($.inArray(type, imageExt) !== -1) {
                alert(string.video_ext_err);
                $(".videoplaceholder").show();
                $(".video-wrap").hide();
                return false;
            }
            // Check the file signature against known types

        };
        fileReader.readAsArrayBuffer(blob);
        var $source = $('#instruction_video_here');
        $source[0].src = URL.createObjectURL(this.files[0]);

        $source.parent()[0].load();
    });
    //for video
    $("#first_video").click(function() {
        var a = $("#video_here").attr("src");

        if (a != "null" && a != "undefined") {
            $(this).siblings(".video-wrapper").find(".videoplaceholder").hide();
            $(this).siblings(".video-wrapper").find(".video-wrap").show();
        }

    });

    //for instruction 
    $("#ins_vdeo").click(function() {
        // alert("hi")
        var b = $("#instruction_video_here").attr("src");
        if (b != "null" || b != "unkown") {
            $(this).siblings(".video-wrapper").find(".videoplaceholder").hide();
            $(this).siblings(".video-wrapper").find(".video-wrap").show();
        }


    });

    //audio
    audioinput.onchange = function(e) {
        var sound = document.getElementById('sound');
        var blnValid = false;
        var _validFilemimetype = [
            "audio/mpeg",
            "audio/mp3",
            "audio/aac",
            "audio/wav"
        ];
        for (var j = 0; j < _validFilemimetype.length; j++) {
            var sCurExtension = _validFilemimetype[j];

            if (this.files[0].type == sCurExtension) {
                blnValid = true;
                break;
            }
        }
        if (blnValid == true) {
            sound.src = URL.createObjectURL(this.files[0]);
            // not really needed in this exact case, but since it is really important in other cases,
            // don't forget to revoke the blobURI when you don't need it
            sound.onend = function(e) {
                URL.revokeObjectURL(this.src);
                $('#audio_err').text('');
            }
        } else {
            $('#audio_err').text(string.invalid_audio)
        }

    }
</script>

</script>
<!-- Modal -->
<div id="mymodal-csv" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-alt-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-heading">Upload</h4>
            </div>
            <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit' => 'return doValidateUserUploadForm();')); ?>

            <div class="modal-body">
                <div class="form-group">
                    <label class="admin-label">Upload File</label>
                    <div class="input-holder">
                        <input type="text" id="upload-btn" class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                        <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                        <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="task_csv_file" id="upload-doc" style="display:none;">

                        <span id="file_error"></span>
                    </div>

                    <span class="download-btn">
                        <a href="<?php echo base_url() ?>public/uid_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                    </span>
                </div>
            </div>

            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" class="commn-btn save csvupload2" name="csv-upload" value="upload-csv">Upload</button>
                </div>
            </div>

        </div>
        <?php echo form_close(); ?>

    </div>
</div>

<script>
    $('.csvupload2').click(function() {
        $('#file_error').text('');
        var cnt = 0;

        var allowed_image_type = ['text/csv','application/vnd.ms-excel']

        var landscape_image_data = $('#upload-doc')[0].files

        if (landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length == 0) {

            $('#file_error').text('Please select csv file');
            $('#file_error').css('color', 'red');
            cnt++;
        }
        if (landscape_image_data.length > 0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type) == -1) {

            $('#file_error').text('Please select csv file only');
            $('#file_error').show();
            cnt++;
        }

        if (cnt > 0) {
            return false;
        } else {
            modelHide();
            uploadCsvAjax();
            return true;
        }
    });

    function modelHide() {
        $('#mymodal-csv').modal('hide');
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
    }

    $(function() {
        var previewImage = function(input) {
            var fileTypes = ['jpg', 'jpeg', 'png'];
            var extension = input.files[0].name.split('.').pop().toLowerCase(); /*se preia extensia*/
            var isSuccess = fileTypes.indexOf(extension) > -1; /*se verifica extensia*/

            if (isSuccess) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    if ($('.upload2-prod-pic-wrap ul li').length < 5) {
                        $('.upload2-prod-pic-wrap ul').append('<li><span class="trash-ico"></span><a href="#"><img src="' + e.target.result + '"></a></li>')
                    }
                };
                reader.readAsDataURL(input.files[0]);

            } else {
                alert('Image not accepted');
            }

        };
        $('#whatsupload-img').on('change', function() {
            previewImage(this);
            setTimeout(function() {
                showpreview()
            }, 300);
        })
    });

    function showpreview() {
        var lth = $('.upload2-prod-pic-wrap ul li').length;
        if (lth == 0) {
            $('#superadminpic').attr({
                'src': ''
            })
        }
        console.log('dasdas' + lth);
        $('.upload2-prod-pic-wrap ul li').each(function(key, val) {
            console.log('key' + key)
            if (key == lth - 1) {
                $('#superadminpic').attr({
                    'src': $(this).find('img').attr('src')
                })
            } else {

            }
        });
        if (lth == 2) {
            $('.camera').hide()
        } else {
            $('.camera').show()
        }

        $(".upload2-prod-pic-wrap ul li .trash-ico").click(function() {
            $(this).parent().remove();
            // alert("hii"); 
            showpreview();
        });
    }
</script>

<style>
    .button-wrap.upload2-prod-pic-wrap ul li {
        position: relative;
    }

    .button-wrap.upload2-prod-pic-wrap ul li a img {
        width: 150px;
        height: 150px;
        object-fit: cover;
    }
</style>