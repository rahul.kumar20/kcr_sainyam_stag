<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
   <!--breadcrumb wrap-->
   <div class="breadcrumb-wrap">
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/task">Task Management</a></li>
         <li class="breadcrumb-item active">Task Detail</li>
      </ol>
   </div>
   <div class="fltr-srch-wrap white-wrapper clearfix">
      <?php if (!empty($this->session->flashdata('message_success'))) {
      ?>
         <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
         </div>
      <?php

      } ?>

      <?php if (!empty($this->session->flashdata('message_error'))) {
      ?>
         <div class="alert alert-danger" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
         </div>
      <?php

      } ?>
      <div class="white-wrapper">
         <div class="form-item-title clearfix">
            <h3 class="title">Task Detail</h3>
         </div>
         <div class="row">
            <div class="col-sm-12">
               <div class="upload-pic-wrapper">

                  <?php if (!empty($task_media)) {
                     foreach ($task_media as $medVal) { ?>
                        <?php if ($medVal['task_media_type'] == IMAGE) { ?>
                           <figure class="show-img">
                              <a class="example-image-link" href="<?php echo $medVal['task_url']; ?>" data-lightbox="example-set">
                                 <img class="example-image loader" data-lightbox="example-set" src="<?php echo $medVal['task_url']; ?>">
                              </a>
                           </figure>
                        <?php
                        } else if ($medVal['task_media_type'] == VIDEO) { ?>
                           <figure class="video-img-wrapper">
                              <video controls class="video-wrap">
                                 <span class="trash-ico"></span>
                                 <source src="<?php echo $medVal['task_url']; ?>" id="video_here">

                              </video>
                           </figure>
                        <?php
                        } else if ($medVal['task_media_type'] == AUDIO) { ?>
                           <figure class="video-img-wrapper">
                              <audio controls class="video-wrap">
                                 <span class="trash-ico"></span>
                                 <source src="<?php echo $medVal['task_url']; ?>" id="video_here">

                              </audio>
                           </figure>
                  <?php
                        }
                     }
                  } ?>


               </div>
            </div>
            <div class="col-sm-8">
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task Title</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['task_title']) && !empty($task['task_title']) ? $task['task_title'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task Id</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['task_code']) && !empty($task['task_code']) ? $task['task_code'] : '--') ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task Live Date</label>
                        <div class="input-holder">
                           <div class="inputfield-wrap">
                              <span class="text-detail"><?php echo (isset($task['start_date']) && !empty($task['start_date']) ? mdate(TASK_DATE_FORMAT, strtotime($task['start_date'])) : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Deadline Date</label>
                        <div class="input-holder">
                           <div class="inputfield-wrap">
                              <span class="text-detail"><?php echo (isset($task['end_date']) && !empty($task['end_date']) ? mdate(TASK_DATE_FORMAT, strtotime($task['end_date'])) : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Points</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['points']) && !empty($task['points']) ? $task['points'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Location</label>
                        <div class="input-holder">
                           <span class="text-detail"><?php echo (isset($task['state_name']) && !empty($task['state_name']) ? $task['state_name'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">

               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Task for(Social Media)</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_type']) && !empty($task['task_type']) ? $task['task_type'] . '(' . $task['action'] . ')' : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Status</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_status']) && !empty($task['task_status']) ? $task_status[$task['task_status']] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">District</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['district_name']) && !empty($task['district_name']) ? $task['district_name'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Gender</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['gender']) && !empty($task['gender']) ? $task_gender[$task['gender']] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">UID</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['registeration_no']) && !empty($task['registeration_no']) ? $task['registeration_no'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">College</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['college_name']) && !empty($task['college_name']) ? $task['college_name'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <?php if (isset($task['action']) && !empty($task['action']) && $task['action'] == 'event') { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Radius</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['radius']) && !empty($task['radius']) ? $task['radius'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Latitude</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['latitude']) && !empty($task['latitude']) ? $task['latitude'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Longitude</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['longitude']) && !empty($task['longitude']) ? $task['longitude'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
                  <?php if (isset($task['action']) && !empty($task['action']) && $task['action'] == 'follow') { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Twitter Account Name</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['twitter_follow_name']) && !empty($task['twitter_follow_name']) ? $task['twitter_follow_name'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Twitter Account ID</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['twitter_follow_id']) && !empty($task['twitter_follow_id']) ? $task['twitter_follow_id'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>

                  <?php } ?>
                  <?php if (isset($task['video_id']) && !empty($task['video_id'])) { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Video ID</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['video_id']) && !empty($task['video_id']) ? $task['video_id'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
                  <?php if (isset($task['channel_id']) && !empty($task['channel_id'])) { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Channel ID</label>
                           <div class="commn-select-wrap">
                              <span class="text-detail"><?php echo (isset($task['channel_id']) && !empty($task['channel_id']) ? $task['channel_id'] : 'Not Available') ?></span>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
               </div>
               <div class="row">
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Description</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_description']) && !empty($task['task_description']) ? $task['task_description'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                  <?php if ($task['task_type'] == 'facebook' || $task['task_type'] == 'twitter' || $task['task_type'] == 'online') { ?>
                     <?php if ($task['action'] != 'follow') { ?>
                        <div class="col-sm-6 col-xs-6 m-t-sm">
                           <div class="form-group">
                              <label class="admin-label">URL</label>
                              <div class="input-holder">
                                 <span class="text-detail"><?php echo (isset($task['post_url']) && !empty($task['post_url']) ? $task['post_url'] : 'Not Available') ?></span>
                              </div>
                           </div>
                        </div>
                     <?php } ?>
                  <?php } ?>
                  <?php if ($task['task_type'] == 'online') { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Number Of Steps</label>
                           <div class="input-holder">
                              <span class="text-detail"><?php echo (isset($task['form_steps']) && !empty($task['form_steps']) ? $task['form_steps'] : '0') ?></span>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
                  <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Instruction Description</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['instruction_description']) && !empty($task['instruction_description']) ? $task['instruction_description'] : 'Not Available') ?></span>
                        </div>
                     </div>
                  </div>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                     <div class="form-group">
                        <label class="admin-label">Completed by</label>
                        <div class="commn-select-wrap">
                           <span class="text-detail"><?php echo (isset($task['task_completed']) && !empty($task['task_completed']) ? $task['task_completed'] : 0) ?></span>
                        </div>
                     </div>
                  </div>
                  <?php if (isset($task['instruction_video_url']) && !empty($task['instruction_video_url'])) { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Instruction Video</label>
                           <div class="commn-select-wrap">

                              <figure class="video-img-wrapper">
                                 <video controls class="video-wrap">
                                    <span class="trash-ico"></span>
                                    <source src="<?php echo $task['instruction_video_url']; ?>" id="video_here">

                                 </video>
                              </figure>

                           </div>
                        </div>
                     </div>
                  <?php } ?>

                  <?php if (isset($task['action']) && $task['action'] == 'qrcode') { ?>
                     <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                           <label class="admin-label">Download QR Code</label>
                           <div class="input-holder">
                              <a href="<?php echo $task['qr_code_url'] ?>" download>Download</a>
                           </div>
                        </div>
                     </div>
                  <?php } ?>

               </div>
            </div>
            <!-- <div class="form-ele-action-bottom-wrap btns-center clearfix">
         <div class="button-wrap text-center">
            <button type="submit" class="commn-btn save" id="add_news" onclick="deleteUser( 'task',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($task['task_id']); ?>', 'req/change-user-status', 'Do you really want to delete this task?' );">Delete Task</button>
         </div>
      </div> -->
         </div>

         <div class="white-wrapper">
            <div class="form-item-title clearfix">
               <h3 class="title">Completed by</h3>
            </div>
            <!--Filter Section -->
            <div class="fltr-srch-wrap white-wrapper clearfix">
               <div class="row">
                  <div class="col-lg-2 col-sm-3">
                     <div class="display  col-sm-space">
                        <select class="selectpicker dispLimit">
                           <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                           <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                           <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                           <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-lg-4 col-sm-4">
                     <div class="srch-wrap col-sm-space">
                        <button class="srch search-icon" style="cursor:default"></button>
                        <a href="javascript:void(0);"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                        <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by UID" id="searchuser" name="search" autocomplete="off">
                     </div>
                  </div>
                  <div class="col-lg-2 col-sm-2">
                     <?php if (isset($searchlike) && "" != $searchlike) { ?>
                        <div onclick="window.location.href='<?php echo base_url() ?>admin/task/taskDetail?data=<?php echo queryStringBuilder("id=" . $task['task_id']); ?>'">Go Back</div>
                     <?php
                     } ?>
                  </div>
                  <div class="col-lg-4 col-sm-4">
                     <div class="top-opt-wrap text-right">
                        <ul>
                           <li>
                              <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper2" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                           </li>
                           <li>
                              <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                           </li>
                           <!-- <?php if (isset($task['task_type']) && ($task['task_type'] != 'facebook' && $task['task_type'] != 'twitter' && $task['task_type'] != 'youtube' && $task['action'] != 'event' && $task['action'] != 'qrcode')) { ?>
                     <li>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add">
                        <img src="<?php echo base_url() ?>public/images/download.svg">
                        </a>
                        </li>
                     
                     <li>
                             <a href="javascript:void(0)" title="Add New"  data-toggle="modal" data-target="#mymodal-user"class="icon_filter add"><img src="<?php echo base_url() ?>public/images/plus.svg"></a>
                        </li>
                        <?php } ?> -->
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!--Filter Section Close-->
            <!--Filter Wrapper-->
            <div class="filter-wrap ">
               <div class="filter_hd clearfix">
                  <div class="pull-left">
                     <h2 class="fltr-heading">Filter</h2>
                  </div>
                  <div class="pull-right">
                     <span class="close flt_cl" data-dismiss="modal">X</span>
                  </div>
               </div>
               <div class="inner-filter-wrap">

                  <div class="fltr-field-wrap">
                     <label class="admin-label">Status</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker filter status" name="status">
                           <option value="">All</option>
                           <option <?php echo ($status == ACTIVE) ? 'selected' : '' ?> value="1">Active</option>
                           <option <?php echo ($status == BLOCKED) ? 'selected' : '' ?> value="2">Blocked</option>
                        </select>

                     </div>
                  </div>
                  <div class="fltr-field-wrap">
                     <label class="admin-label">UID</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker filter uid" data-live-search="true" name="uid" id="uid">
                           <option value="">All</option>
                           <?php
                           //if block start
                           if (isset($userlist) && !empty($userlist)) {
                              //foreach block start
                              foreach ($userlist as $value) { ?>
                                 <option value="<?php echo $value['registeration_no']; ?>" <?php if ($uid == $value['registeration_no']) {
                                                                                                echo 'selected';
                                                                                             } ?>><?php echo $value['registeration_no']; ?></option>
                           <?php
                                 //fr each block end
                              }
                              //if block end 
                           }
                           ?>
                        </select>

                     </div>
                  </div>
                  <div class="fltr-field-wrap">
                     <label class="admin-label">State</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker filter state" id="state" data-live-search="true" name="state" onchange="getDistrictForState('req/getdistrictbystate',this.value,'')">
                           <option value="">All</option>

                           <?php
                           //if block start
                           if (isset($statelist) && !empty($statelist)) {
                              //foreach block start
                              foreach ($statelist as $stateKey => $stateVal) { ?>
                                 <option value="<?php echo $stateVal['state_id']; ?>" <?php if ($state == $stateVal['state_id']) {
                                                                                          echo 'selected';
                                                                                       } ?>><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                           <?php
                                 //fr each block end
                              }
                              //if block end 
                           }
                           ?>
                        </select>

                     </div>
                  </div>
                  <div class="fltr-field-wrap">
                     <label class="admin-label">District</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker distict" data-live-search="true" name="distict" id="distict" onchange="getcollegeForDistrict('req/getcollegebydistrict',this.value,'')">
                           <option value="">All</option>

                        </select>

                     </div>
                  </div>
                  <div class="fltr-field-wrap">
                     <label class="admin-label">College</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker college" name="college_name" id="collegeDa">
                           <option value="">All</option>


                        </select>

                     </div>
                  </div>

                  <div class="fltr-field-wrap">
                     <label class="admin-label">Gender</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker filter gender" name="gender">
                           <option value="">All</option>
                           <option <?php echo ($gender == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER ?>">Male</option>
                           <option <?php echo ($gender == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER ?>">Female</option>
                        </select>

                     </div>
                  </div>
                  <div class="fltr-field-wrap">
                     <label class="admin-label">Registration Date</label>
                     <div class="inputfield-wrap">
                        <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                     </div>

                  </div>
                  <div class="fltr-field-wrap">
                     <div class="inputfield-wrap">
                        <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                     </div>
                  </div>
                  <div class="fltr-field-wrap">
                     <label class="admin-label">Task Completed</label>
                     <div class="commn-select-wrap">
                        <select class="selectpicker filter taskCompleted" name="task_completed">
                           <option value="">All</option>
                           <option <?php echo ($taskCompleted == '0-10') ? 'selected' : '' ?> value="0-10">0-10</option>
                           <option <?php echo ($taskCompleted == '10-20') ? 'selected' : '' ?> value="10-20">10-20</option>
                           <option <?php echo ($taskCompleted == '20-30') ? 'selected' : '' ?> value="20-30">20-30</option>
                           <option <?php echo ($taskCompleted == '30-100') ? 'selected' : '' ?> value="30-100">30-100</option>
                        </select>

                     </div>
                  </div>

                  <div class="button-wrap text-center">
                     <button type="reset" class="commn-btn cancel" onclick="window.location.href='<?php echo base_url() ?>admin/task/taskDetail?data=<?php echo queryStringBuilder("id=" . $task_id); ?>'" id="resetbutton">Reset</button>
                     <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Apply</button>
                  </div>

               </div>
            </div>
            <div class="table-responsive custom-tbl">
               <!--table div-->
               <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                  <thead>
                     <tr>
                        <th>Rank</th>
                        <th>User ID</th>
                        <th>Name</th>
                        <th>Reward Points</th>
                        <th>City</th>
                        <!-- <th>State</th> -->
                        <th>College</th>
                        <th>Status</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody id="table_tr">
                     <?php
                     //if block start
                     if (isset($userlist) && !empty($userlist)) {
                        if ($page > 1) {
                           $i = (($page * $limit) - $limit) + 1;
                        } else {
                           $i = 1;
                        }
                        //for each start
                        foreach ($userlist as $value) {
                     ?>
                           <tr id="remove_<?php echo $value['user_id']; ?>">
                              <td align='left'><span class="serialno"><?php echo $i; ?></span></td>

                              <td>

                                 <a href="<?php echo base_url() ?>admin/users/detail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>">
                                    <?php echo !empty($value['registeration_no']) ? $value['registeration_no'] : "Not Available"; ?>

                                 </a>
                              </td>
                              <td>
                                 <a href="<?php echo base_url() ?>admin/users/detail?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>">
                                    <?php echo !empty($value['full_name']) ? $value['full_name'] : "Not Available"; ?>

                                 </a>
                              </td>

                              <td align='left'>
                                 <?php echo (isset($task['points']) && !empty($task['points']) ? $task['points'] : 'Not Available') ?>
                              </td>


                              <!-- <td align='left'>
                                 <?php echo !empty($value['state_name']) ? $value['state_name'] : "Not Available"; ?>
                              </td> -->
                              <td align='left'>
                                 <?php echo !empty($value['district_name']) ? $value['district_name'] : "Not Available"; ?>
                              </td>
                              <td align='left'>
                                 <?php echo !empty($value['college_name']) ? $value['college_name'] : "Not Available"; ?>
                              </td>

                              <td id="status_<?php echo $value['user_id']; ?>"><?php echo ($value['status'] == ACTIVE) ? "Approved" : "Pending"; ?></td>
                              <td>
                                 <?php if ($value['status'] != ACTIVE) { ?>
                                    <a href="javascript:void(0);" class="table_icon accept-check" title="Accept"><i class="fa fa-check" title="Accept" aria-hidden="true" onclick="acceptuser( 'taskcompleterequest',<?php echo PROFILE_ACCEPT; ?>, '<?php echo encryptDecrypt($value['user_id']); ?>','<?php echo encryptDecrypt($task_id); ?>','req/accept-user-request', 'Do you want to complete task of user?' );"></i></a>
                                 <?php } else { ?>
                                    --
                                 <?php } ?>
                                 <?php if ($task['action'] == 'download media' || $task['action'] == 'set dp' || $task['action'] == 'create group' || $task['action'] == 'about us' || $task['action'] == 'video' || $task['action'] == 'audio' || $task['action'] == 'text' || $task['action'] == 'gif' || $task['action'] == 'link' || $task['action'] == 'image' || $task['action'] == 'change status'  || ($task['task_type'] == 'instagram' && $task['action'] == 'like')  || ($task['task_type'] == 'instagram' && $task['action'] == 'comment')) { ?>
                                    <a href="<?php echo base_url() ?>admin/offlineTaskDetail?data=<?php echo queryStringBuilder("id=" . $value['user_id']) . "&task_id=" . $task_id; ?>" class="table_icon" title="View"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                 <?php } ?>
                              </td>
                           </tr>

                        <?php
                           //for each end
                           $i++;
                        }
                        //if block end
                     } else {
                        ?>
                        <tr>
                           <td colspan="9"><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></td>

                        </tr>
                     <?php } ?>
                  </tbody>
               </table>
               <div class="pagination_wrap clearfix">
                  <?php echo $link; ?>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal -->
      <div id="mymodal-csv" class="modal fade" role="dialog">
         <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header modal-alt-header">
                  <button type="button" class="close" data-dismiss="modal">×</button>
                  <h4 class="modal-title modal-heading">Upload</h4>
               </div>
               <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit' => 'return doValidateUploadForm();')); ?>

               <div class="modal-body">
                  <div class="form-group">
                     <label class="admin-label">Upload File</label>
                     <div class="input-holder">
                        <input type="hidden" name="id" value="<?php echo encryptDecrypt($task_id); ?>">
                        <input type="text" id="upload-btn" class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                        <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                        <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="reward_csv_file" id="upload-doc" style="display:none;">

                        <span id="file_error"></span>
                     </div>

                     <span class="download-btn">
                        <a href="/public/reward_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                     </span>
                  </div>
               </div>

               <div class="modal-footer">
                  <div class="button-wrap">
                     <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                     <button type="submit" class="commn-btn save" name="csv-upload" value="upload-csv">Upload</button>
                  </div>
               </div>

            </div>
            <?php echo form_close(); ?>

         </div>
      </div>
   </div>

   <!-- Modal addd user -->
   <div id="mymodal-user" class="modal fade" role="dialog">
      <div class="modal-dialog">

         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header modal-alt-header">
               <button type="button" class="close" data-dismiss="modal">×</button>
               <h4 class="modal-title modal-heading">Add User</h4>
            </div>
            <?php echo form_open_multipart('', array('id' => 'user_reward', 'onsubmit' => 'return doValidateUserForm();')); ?>

            <div class="modal-body">
               <div class="form-group">
                  <label class="admin-label">Add User</label>
                  <div class="input-holder">
                     <input type="hidden" name="id" value="<?php echo encryptDecrypt($task_id); ?>">
                     <label class="upload-btn">Registration Id</label>
                     <input type="text" class="form-control material-control" autocomplete="off" maxlength="20" name="userRegistrationNumber" id="userRegistrationNumber" value="">

                     <span id="id_error"></span>
                  </div>


               </div>
            </div>

            <div class="modal-footer">
               <div class="button-wrap">
                  <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="commn-btn save" name="user-upload" value="upload-user">Add</button>
               </div>
            </div>

            <?php echo form_close(); ?>

         </div>
      </div>
   </div>
   <script>
      function doValidateUploadForm() {
         $('#file_error').text('');
         var cnt = 0;

         var allowed_image_type = ['text/csv']

         var landscape_image_data = $('#upload-doc')[0].files

         if (landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length == 0) {

            $('#file_error').text('Please select csv file');
            $('#file_error').css('color', 'red');
            cnt++;
         }
         if (landscape_image_data.length > 0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type) == -1) {

            $('#file_error').text('Please select csv file only');
            $('#file_error').show();
            cnt++;
         }

         if (cnt > 0) {
            return false;
         } else {
            modelHide();
            return true;
         }
      }

      function doValidateUserForm() {
         $('#file_error').text('');
         var cnt = 0;

         $('#file_error').text('');

         var userRegistrationNumber = $('#userRegistrationNumber').val();

         if (userRegistrationNumber == '' || typeof userRegistrationNumber == 'undefined' || userRegistrationNumber.length == 0) {

            $('#id_error').text('Please enter user registration Id');
            $('#id_error').css('color', 'red');
            cnt++;
         } else if (userRegistrationNumber.length <= 10) {
            $('#id_error').text('Registration Id must be of 10 digit');
            $('#id_error').css('color', 'red');
            cnt++
         }


         if (cnt > 0) {
            return false;
         } else {
            modelHide2();
            return true;
         }
      }

      function modelHide() {
         $('#mymodal-csv').modal('hide');
         $("body").addClass("loader-wrap");
         $('.loader-img').show();
      }

      function modelHide2() {
         $('#mymodal-user').modal('hide');
         $("body").addClass("loader-wrap");
         $('.loader-img').show();
      }
   </script>
   <script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>

   <script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
   <script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
   <script src="<?php echo base_url() ?>public/js/task.js"></script>

   <!-- Accept Modal -->
   <!-- Modal -->
   <div id="myModal-accept" class="modal fade" role="dialog">
      <input type="hidden" id="userid" name="userid" value="">
      <input type="hidden" id="udstatus" name="udstatus" value="">
      <div class="modal-dialog modal-custom">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header modal-alt-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title modal-heading">Accept Request</h4>
            </div>
            <div class="modal-body">
               <p class="modal-para">Are you sure want to block this user?</p>
            </div>
            <input type="hidden" id="new_status" name="new_status">
            <input type="hidden" id="new_id" name="new_id">
            <input type="hidden" id="t_id" name="t_id">
            <input type="hidden" id="new_url" name="new_url">
            <input type="hidden" id="new_msg" name="new_msg">
            <input type="hidden" id="for" name="for">
            <div class="modal-footer">
               <div class="button-wrap">
                  <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                  <button type="button" id="action" class="commn-btn save" onclick="changeStatusToAccept($('#for').val(),$('#new_status').val(),$('#new_id').val(),$('#new_url').val(),$('#t_id').val())">Accept</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   /**
    * @name deleteUser
    * @description This method is used to show delete user modal.
    *
    */

   function acceptuser(type, status, id, taskid, url, msg) {

      $('#new_status').val(status);
      $('#new_id').val(id);
      $('#new_url').val(url);
      $('#t_id').val(taskid);

      $('.modal-para').text(msg);
      $('#for').val(type);
      if (status == 3) {
         $('.modal-title').text('Reject Request');
         $('#action').text('Reject')
      } else {
         $('.modal-title').text('Accept Request');
         $('#action').text('Accept')
      }

      $('#myModal-accept').modal('show');
   }
</script>