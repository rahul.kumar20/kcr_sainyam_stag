<link href="public/css/dropzone.css" rel='stylesheet'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/profile">Admin Profile</a></li>
            <li class="breadcrumb-item active">Edit Profile</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <?php
     if ( $this->session->flashdata( 'message' ) != '' ) {
         echo $this->session->flashdata( 'message' );
     }
    ?>
    

    

    <!--Filter Section -->
    
    <div class="form-item-wrap">
        <div class="form-item-title clearfix">
            <h3 class="title">Edit Profile</h3>
        </div>
        <!-- title and form upper action end-->

        <div class="white-wrapper clearfix">
 <div class="row">
         <div class="col-lg-3 col-sm-5 custome-col">
             <div class="upload-image" onclick="setImageAfterError()">upload image</div>
                
                <?php echo form_open_multipart( '#', array ('id' => 'dropzone','class'=>'dropzone dz-clickable' ) ); ?>
                     <!-- <form action="#" class="dropzone dz-clickable" id="dropzone"> -->
                        <div class="dz-default dz-message" id="dropfile"><span>Drop files here to upload</span></div>
                    <?php echo form_close(); ?> 
                    <span class="loder-wrraper-single"></span>
                    <p>Max Size : 1024X768</p>
              
            </div>


                <div class="col-lg-5 col-sm-7 form_magn">
                    <div class="row">
            <?php echo form_open_multipart( '', array ('id' => 'editadminprofile1') ); ?>
            <input type="hidden" name="profile_picture" id="profile_picture" value="">
           
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="admin-label">Name</label>
                                <div class="input-holder">
                                    <input type="text" maxlength="100" name="Admin_Name" id="Admin_Name" value="<?php
                                     echo (isset( $editdata['admin_name'] ) && !empty( $editdata['admin_name'] )) ? $editdata['admin_name'] : set_value( 'Admin_Name' );
                                    ?>" placeholder="Enter Name">
                                           <?php echo form_error( 'Admin_Name', '<label class="alert-danger">', '</label>' ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="admin-label">Email ID</label>
                                <div class="input-holder">
                                    <input type="text" readonly maxlength="100" name="email" value="<?php
                                     echo (isset( $editdata['admin_email'] ) && !empty( $editdata['admin_email'] )) ? $editdata['admin_email'] : set_value( 'email' );
                                    ?>" id="email" placeholder="Enter Email">
                                           <?php echo form_error( 'email', '<label class="alert-danger">', '</label>' ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="admin-label">Contact Number</label>
                                <div class="input-holder">
                                    <input type="text" maxlength="100" name="phone_number" value="<?php
                                     echo (isset( $editdata['admin_phone'] ) && !empty( $editdata['admin_phone'] )) ? $editdata['admin_phone'] : set_value( 'phone_number' );
                                    ?>" id="phone_number" data-mask="(999) 999-9999" placeholder="Enter contact number">
                                           <?php echo form_error( 'phone', '<label class="alert-danger">', '</label>' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="button"  onclick="window.location.href = '<?php echo base_url() ?>admin/profile'"class="commn-btn cancel">Cancel</button>
                    <button type="submit" class="commn-btn save">Save</button>
                </div>
            </div>
            <!--form ele wrapper end-->
             <!--close form view   -->
        <?php echo form_close(); ?>
        <!--Filter Section Close-->
                </div>
            </div>
            
        </div>
        <!--form element wrapper end-->
    </div>
   
</div>
<!--Table listing-->
<script>
    $( document ).ready( function () {
        setTimeout( function () {
            $( '.alert-danger' ).fadeOut( 5000 );
        }, 3000 );
        
  
    } );
    
          function setImageAfterError()
        {
            $(".dz-error").remove();
            $(".dropzone").removeClass("dz-started");
            $('#dropfile').click()
        }
        
        
</script>