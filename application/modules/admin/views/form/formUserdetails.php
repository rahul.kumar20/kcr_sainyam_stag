<?php
$filterArr = $this->input->get();
$filterArr = ( object ) $filterArr;
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterArr); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    
    <!--Filter Section -->
    <div class="fltr-srch-wrap clearfix white-wrapper">
        <div class="row">

            <div class="col-lg-4 col-sm-4">
                <form action="">
                    <div class="srch-wrap col-sm-space">
                        <button class="srch search-icon" style="cursor:default"></button>
                        <a href="javascript:void(0)"> <span class="srch-close-icon searchCloseBtn">X</span></a>
                        <input type="text" value="<?php echo (isset($searchlike) && ! empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by form title" id="searchuser" name="search">
                    </div>
                </form>
            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                    <div class="go_back">Go Back</div>
                <?php } ?>

            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-lg-6">Total Content: <?php echo $totalrows ?></div>
    </div>
    <!--Table-->
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg'); ?>
        <div class="alert alert-success" <?php echo ( ! (isset($alertMsg) && ! empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && ! empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && ! empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <div class="clearfix white-wrapper">
        <div class="table-responsive custom-tbl">
            <!--table div-->
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>UserName</th>
                        <th>State</th>
                        <th>District</th>
                        <th>Ac</th>
                        <th>Submitted Date</th>
                        <th>Action</th>
                    </tr>

                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($formUserList)  && !empty($formUserList)) {
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } else {
                            $i = 1;
                        }
                        foreach ($formUserList as $value) {
                           
                    ?>

                        <td align='left'><span class="serialno"><?php echo $i; ?></span></td>
                        <td><?php echo $value['full_name']; ?></td>
						<td><?php echo $value['state_name']; ?></td>
	                    <td><?php echo $value['district_name']; ?></td>
                        <td><?php echo $value['ac']; ?></td>
                        <td><?php echo $value['created_at']; ?></td>
                        <td>
                            <a href="<?php echo base_url() ?>admin/form_list/formSubmitDetails?data=<?php echo queryStringBuilder("id=" . $value['user_id']); ?>" class="table_icon" title="User Form Details"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </td>
                    
                        </tr>
                        <?php
                           //for each end
                           $i++;
                        }
                        //if block end
                     } else {
                        ?>
                        <tr>
                           <td colspan="9"><?php echo $this->lang->line('NO_RECORD_FOUND'); ?></td>

                        </tr>
                     <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>
    </div>
    <!-- table 1 close-->
</div>
<!--Table listing-->

