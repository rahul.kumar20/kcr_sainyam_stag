<style>
    .myloader {
        width: 16%;
        position: absolute;
        margin-top: -29px;
        /*display: none;*/
    }

    img {
        max-width: 100%;
        /* This rule is very important, please do not ignore this! */
    }
</style>
<figure class="banner-wrapper">
    <!-- <img alt="banner" class="banner_img" src="<?php echo base_url(); ?>/public/images/form-banner.png"> -->
</figure>
<div class="inner-right-panel">
    <div class="dynamic-form-wrapper">
        <!--Form Stage-->
        <div class="form-stage-wrap">
            <div class="inner-wrapper">
                <div class="form-title">
                    <input type="text" placeholder="Form Title" value="Untitled form">
                </div>
                <div class="form-description">
                    <textarea placeholder="Form Description"></textarea>
                </div>
            </div>
            <!--Form Stage Close-->
            <!--Form Element-->
            <div class="form-element-col">
                <ul>
                    <li><a href="javascript:void(0)" title="Add"><span class="icon defaultad"></span></a></li>
                    <li><a href="<?php echo base_url() . 'admin/form_task/csvupload' ?>" title="csv upload"><span class="icon defaultad"></span></a></li>
                    <li><a href="javascript:void(0)" title="banner"><span onclick="addCoverImage(308, 100, 'timeline', 1)" class="icon defaultad banner_upload"></span></a></li>
                    <input type="hidden" name="top_left" class="inputhidden" id="inputhidden" value="">
                </ul>
            </div>
            <!--Form Element close-->
        </div>
    </div>
</div>
<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="avatar-form" id="my-cropper" action="<?php echo base_url(); ?>public/admin/crop.php" enctype="multipart/form-data" method="post">
                <div class=" modal-header modal-alt-header" style="border-bottom:none !important; min-height:0px; background:#163f2e !important;">
                    <div class="img_up_hd">
                        <h1>Upload Image</h1>
                    </div>
                    <div class="close_wrapper">
                        <button class="close" data-dismiss="modal" type="button">&times;</button>
                    </div>
                </div>

                <div class="modal-body">
                    <div class="up_img_wrap">
                        <div class="reatiler_box insidegrey_bg">

                            <!-- Upload image and data -->
                            <div class="avatar-upload text-center clearfix">
                                <input class="avatar-src" name="avatar_src" type="hidden">
                                <input class="avatar-data" name="avatar_data" type="hidden">
                                <div class="avatar-chooseimg-wrapper">
                                    <label for="avatarInput">Choose Image</label>
                                    <input class="avatar-input" id="avatarInput" name="avatar_file" type="file" accept="image/x-png, image/png, image/gif, image/jpeg, image/jpg">
                                </div>
                            </div>

                            <!-- Crop and preview -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="avatar-wrapper"></div>
                                </div>
                            </div>

                            <div class="row" style="padding-left: 15px;">
                                <div class="clearfix">
                                    <div class="avatar-btns text-center">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-cropper fa fa-rotate-left" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees"></button>
                                        </div>
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-cropper fa fa-rotate-right" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees"></button>
                                        </div>
                                    </div>
                                    <div class="avatar-zooms text-center">
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-cropper fa fa-plus" data-method="zoom" data-option="0.1" type="button" title="Zoom Out"></button>
                                        </div>
                                        <div class="btn-group">
                                            <button class="btn btn-default btn-cropper fa fa-minus " data-method="zoom" data-option="-0.1" type="button" title="Zoom In"></button>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 text-center change clearfix" style="margin-top:20px">
                                        <button class="custom-btn cancel commn-btn" data-dismiss="modal" type="reset">Cancel</button>
                                        <button class="custom-btn save sv-btn commn-btn" type="submit">Save</button>
                                        <!--<img class="myloader" src="images/loader.svg" style="display: none">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.banner_upload').click(function() {
        $('#avatar-modal').modal('show');
    });

    function addCoverImage(minCropBoxWidth1, minCropBoxHeight1, ratio, target) {
        callme('coverPicInput', minCropBoxWidth1, minCropBoxHeight1, 'adv', 'addshopbtn', 'imageMe1', 'true', '', ratio, target);
    }
</script>