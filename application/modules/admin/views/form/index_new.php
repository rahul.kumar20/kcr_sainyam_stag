


<figure  class="banner-wrapper">
    <img alt="banner" src="<?php echo base_url(); ?>public/images/form-banner.png">
</figure>
<div class="inner-right-panel">
<div class="dynamic-form-wrapper">
    
     <label id="error">
        
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    
    <label id="error">
        
        <div class="alert alert-danger" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <!--Form Stage-->
    <form id="dynamic_form" method="post" enctype="multipart/form-data">
    <div class="form-stage-wrap">
        <div class="inner-wrapper">
         <div class="form-title">
            <input type="text"  class ="form-title" placeholder="Form Title" name="form_title" >
         </div>
         <div class="form-description">
            <textarea class="form-desc" placeholder="Form Description" name="description"></textarea>
         </div>
            
        <div class="form-description messg-verification">
            <!-- <input type="text"  class="steps" placeholder="Mobile number verification" name="steps" readonly> -->
<label class="steps">Mobile number verification</label>
            <div class="switch-btn">
                <label class="switch">
                <input type="checkbox" name="mobileVerification" value="2">
                <span class="slider round"></span>
                </label>
            </div>    
        </div>
                
        <!--
         <div class="question-pc">
            Is PC?
         </div>
         <div class="radio-bttn-wrapper">
            <ul>
               <li>
                  <div class="th-checkbox">
                     <input style="display:none;" class="filter-type filled-in"  type="radio" name="filter" id="Yes" value="">
                     <label for="Yes" class="lbl-check"><span></span>Yes</label>
                  </div>
               </li>
               <li>
                  <div class="th-checkbox">
                     <input style="display:none;" class="filter-type filled-in"  type="radio" name="filter" id="No" value="">
                     <label for="No" class="lbl-check"><span></span>No</label>
                  </div>
               </li>
            </ul>
         </div>
         <div class="drop-down-wrapper">
            <div class="flex-row">
               <div class="flex-col-sm-3">
                  <div class="form-field-wrap">
                      <select class="form-control" onchange="getDistrictList(this.value,'district','district-list')">
                        <option>State</option>
                        
                        <?php //if(isset($states) && !empty($states)){
                            
                            //foreach($states as $key=>$value){
                                
                              //  echo "<option value='".$value['state_id']."'>".$value['state_name']."</option>";
                           // }
                            
                       // }?>
                     </select>
                  </div>
               </div>
                <div class="flex-col-sm-3" style="display:none;" id="district">
                  <div class="form-field-wrap" id="district-list">
                     
                  </div>
               </div>
                <div class="flex-col-sm-3" style="display:none;" id="ac">
                  <div class="form-field-wrap">
                     <select class="form-control" id="ac-list">
                        <option>AC</option>
                     </select>
                  </div>
               </div>
                
                <div class="flex-col-sm-3" style="display:none;" id="college">
                  <div class="form-field-wrap" id="college-list">
                     
                  </div>
               </div>
                
                <div class="flex-col-sm-3">
                  <div class="form-field-wrap">
                     <select class="form-control">
                        <option>Other</option>
                     </select>
                  </div>
               </div>
            </div>

             <div class="flex-row" style="display:none;" id="pc">
               <div class="flex-col-sm-3">
                  <div class="form-field-wrap" id="pc-list">
                  </div>
               </div>
            
            </div>
         </div>-->
            <div class="inner-question-html">   
          <!--section block-->
                <section class="question-block active" id="question-block0">
                  <div class="inner-wrapper">
                     <span class="toggle-btn-wrapper filter-side-wrapper">
                     <span class="dots"></span>
                     <span class="dots"></span>
                     <span class="dots"></span>
                     </span>                
                     <div class="form-option">
                        <div class="form-element-col">
                           <ul>

                                <li onclick="getHtmlByList('innerQusType0','checkbox','0')"><a href="javascript:void(0)"><span class="icon checkbox"></span></a></li>
                                <li onclick="getHtmlByList('innerQusType0','dropdown','0')"><a href="javascript:void(0)" title="Dropdown"><span class="icon dropdown"></span></a></li>
                                <!-- <li onclick="getHtmlByList('innerQusType0','mobile','0')"><a href="javascript:void(0)" title="Date"><span class="icon checkbox"></span></a></li> -->
                                <li onclick="getHtmlByList('innerQusType0','text','0')"><a href="javascript:void(0)" title="Text"><span class="icon text-box"></span></a></li>
                                <li onclick="getHtmlByList('innerQusType0','textarea','0')"><a href="javascript:void(0)" title="Text Area"><span class="icon text-area"></span></a></li>
                          
                           </ul>
                        </div>
                     </div>
                     <div class="form-question">
                        <input type="text" placeholder="Question1" name="question[]" id="question0">
                        <input type="hidden" name="relation[]" id="relation0">
                        <input type="hidden" name="questionType[]" id="questionType0">
                        <input type="hidden" name="optionCode[]" id="optionCode0">
                        <input type="hidden" name="dependency[]" id="dependency0">
                        
                     </div>
                      
                      <!-- innner question html -->
                      
                      <div id="innerQusType0">
                            <div class="flex-row form-option-wrap" id="attribute0">
                            </div>    
                      </div>    
                      
                      
                      <div class="flex-row">
                        <div class="flex-col-sm-10">
                           <div class="action-btn-wrap">

                              <div class="action-wrap">    
                                <span class="action-btn plus-circle"  onclick="makeQusChoice(0)" title="Add"></span>
                                <label class="action-btn export" title="Import"  for="importFile0"></label>
                                <input type="file" required id="importFile0" style="display:none"  onclick="this.value=null;" onchange="importCsv(0,this)">
                                <label class="action-btn import" onclick="exportCsv(0)" title="Export" for="export"></label>
                                <input type="file"  style="display:none">
                                <span class="action-btn delete" onclick=removeQusBlock("question-block0") title="Delete"></span>
                                <!-- <span class="action-btn duplicate" title="Duplicate"></span> -->
                              </div>

                               <div class="option-wrapper" id="choice0">
                                  <ul>
                                      <li data-id="0" data-value="1" onclick="getQusChoice(0,1)"><a href="javascript:void(0)">Dependent</a></li>
                                      <li data-id="0" data-value="2" onclick="getQusChoice(0,2)"><a href="javascript:void(0)">Non dependent</a></li>
                                      <!--<li><a href="javascript:void(0)">Create Nested</a></li>-->
                                  </ul>     
                              </div> 

                          </div>
                        </div>
                     </div>
                  </div>
               </section>
               <!--section block close-->
            </div>
        <div class="flex-row">
         <div class="flex-col-sm-10 text-center">
             <button class="commn-btn" id="survey-btn"> Submit</button>
         </div>
     </div>
      </div>
    
                                
    <!--Form Stage Close-->
    <!--Form Element-->

        <div class="form-element-col">
            <ul>
                <li onclick="addQuestionSection();"><a href="javascript:void(0)" title="Add"><span class="icon defaultad"></span></a></li>
                <!-- <li><a href="javascript:void(0)" title="banner"><span  onclick="addCoverImage(308, 60, 'nofixed', 1)" class="icon defaultad images"></span></a></li> -->
                <input type="hidden" name="top_left" class="inputhidden" id="inputhidden" value="">

            </ul>
        </div>

     <!--Form Element close-->
     
  </div>
    </form>
  </div>
</div>



<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header modal-title">
        <h4 class="modal-title">Select dependency for the question.</h4>
      </div>
        <div id="dependancy">
            
        </div>
        
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<script src="<?php echo base_url() ?>public/admin/js/global-msg.js"></script>
<script src="<?php echo base_url() ?>public/js/survey/dynamicformnew.js"></script>
<script src="<?php echo base_url() ?>public/js/survey/form-validation.js"></script>

<script src="<?php echo base_url() ?>public/js/survey/exportCsv.js"></script>
<script>

    function addCoverImage(minCropBoxWidth1, minCropBoxHeight1, ratio, target) {
        callme('coverPicInput', minCropBoxWidth1, minCropBoxHeight1, 'adv', 'addshopbtn', 'imageMe1', 'true', '', ratio, target);
    }
  //toggle button
  $(function()
    {

        $('input[type=checkbox][name=mobileVerification]').change(function() {
          if($(this).prop("checked") == true){
            //run code
          $(this).val(1)
        }else{
          $(this).val(2)
            //run code
        }
        });
    });
</script>


