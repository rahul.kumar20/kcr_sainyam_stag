<figure class="banner-wrapper">
   <!-- <img alt="banner" src="<?php echo base_url(); ?>public/images/form-banner.png"> -->
</figure>
<div class="inner-right-panel">
   <div class="dynamic-form-wrapper">
      <!--Form Stage-->
      <div class="form-stage-wrap">
         <div class="inner-wrapper">
            <div class="form-title">
               <input type="text" placeholder="Form Title" value="Untitled form">
            </div>
            <div class="form-description">
               <textarea placeholder="Form Description"></textarea>
            </div>
            <!--Cover-pic-->
            <div class="flex-row">
               <div class="flex-col-sm-10">
                  <figure class="cover-pic-wrapper">
                  </figure>
               </div>
            </div>
            <!--Cover-pic close-->
            <div class="question-pc">
               Is PC?
            </div>
            <div class="radio-bttn-wrapper">
               <ul>
                  <li>
                     <div class="th-checkbox">
                        <input style="display:none;" class="filter-type filled-in" type="radio" name="filter" id="Yes" value="">
                        <label for="Yes" class="lbl-check"><span></span>Yes</label>
                     </div>
                  </li>
                  <li>
                     <div class="th-checkbox">
                        <input style="display:none;" class="filter-type filled-in" type="radio" name="filter" id="No" value="">
                        <label for="No" class="lbl-check"><span></span>No</label>
                     </div>
                  </li>
               </ul>
            </div>
            <div class="drop-down-wrapper">
               <div class="flex-row">
                  <div class="flex-col-sm-3">
                     <div class="form-field-wrap">
                        <select class="form-control">
                           <option>Select PC</option>
                        </select>
                     </div>
                  </div>
                  <div class="flex-col-sm-3">
                     <div class="form-field-wrap">
                        <select class="form-control">
                           <option>Select PC</option>
                        </select>
                     </div>
                  </div>
                  <div class="flex-col-sm-3">
                     <div class="form-field-wrap">
                        <select class="form-control">
                           <option>Select PC</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <!--section block-->
            <section class="question-block active">
               <div class="inner-wrapper">
                  <span class="toggle-btn-wrapper filter-side-wrapper">
                     <span class="dots"></span>
                     <span class="dots"></span>
                     <span class="dots"></span>
                  </span>
                  <div class="form-option">
                     <div class="form-element-col">
                        <ul>
                           <li><a href="javascript:void(0)" title="Add"><span class="icon defaultad"></span></a></li>
                           <li><a href="javascript:void(0)"><span class="icon checkbox"></span></a></li>
                           <li><a href="javascript:void(0)" title="Dropdown"><span class="icon dropdown"></span></a></li>
                           <li><a href="javascript:void(0)" title="Map"><span class="icon map"></span></a></li>
                           <li><a href="javascript:void(0)" title="File Upload"><span class="icon fileupload"></span></a></li>
                           <li><a href="javascript:void(0)" title="Date"><span class="icon datepicker-icon"></span></a></li>
                           <li><a href="javascript:void(0)" title="Time"><span class="icon timepicker"></span></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="form-question">
                     <input type="text" placeholder="Question">
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="checkbox" name="filter" id="Yes" value="">
                           <label for="Yes" class="lbl-check"><span></span></label>
                        </div>
                     </div>
                     <div class="flex-col answer-col">
                        <input type="text" class="answerbox" placeholder="Option 1">
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="checkbox" name="filter" id="Yes" value="">
                           <label for="Yes" class="lbl-check"><span></span></label>
                        </div>
                     </div>
                     <div class="flex-col answer-col">
                        <span class="delete-icon" title="Remove"></span>
                        <input type="text" class="answerbox" placeholder="Option 1">
                     </div>
                  </div>
                  <div class="flex-row">
                     <div class="flex-col">
                        <span class="add-other">Add Option </span>
                     </div>
                  </div>
                  <div class="flex-row">
                     <div class="flex-col-sm-10">
                        <div class="action-btn-wrap">
                           <span class="action-btn delete" title="Delete"></span>
                           <span class="action-btn duplicate" title="Duplicate"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!--section block close-->
            <!--section block-->
            <section class="question-block">
               <div class="inner-wrapper">
                  <span class="toggle-btn-wrapper filter-side-wrapper">
                     <span class="dots"></span>
                     <span class="dots"></span>
                     <span class="dots"></span>
                  </span>
                  <div class="form-option">
                     <div class="form-element-col">
                        <ul>
                           <li><a href="javascript:void(0)" title="Add"><span class="icon defaultad"></span></a></li>
                           <li><a href="javascript:void(0)"><span class="icon checkbox"></span></a></li>
                           <li><a href="javascript:void(0)" title="Dropdown"><span class="icon dropdown"></span></a></li>
                           <li><a href="javascript:void(0)" title="Map"><span class="icon map"></span></a></li>
                           <li><a href="javascript:void(0)" title="File Upload"><span class="icon fileupload"></span></a></li>
                           <li><a href="javascript:void(0)" title="Date"><span class="icon datepicker-icon"></span></a></li>
                           <li><a href="javascript:void(0)" title="Time"><span class="icon timepicker"></span></a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="form-question">
                     <input type="text" placeholder="Question">
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="radio" name="filter" id="Yes" value="">
                           <label for="Yes" class="lbl-check"><span></span></label>
                        </div>
                     </div>
                     <div class="flex-col answer-col">
                        <input type="text" class="answerbox" placeholder="Option 1">
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="radio" name="filter" id="Yes" value="">
                           <label for="Yes" class="lbl-check"><span></span></label>
                        </div>
                     </div>
                     <div class="flex-col answer-col">
                        <span class="delete-icon" title="Remove"></span>
                        <input type="text" class="answerbox" placeholder="Option 1">
                     </div>
                  </div>
                  <div class="flex-row">
                     <div class="flex-col">
                        <span class="add-other">Add Option </span>
                     </div>
                  </div>
                  <div class="flex-row">
                     <div class="flex-col-sm-10">
                        <div class="action-btn-wrap">
                           <span class="action-btn delete" title="Delete"></span>
                           <span class="action-btn duplicate" title="Duplicate"></span>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <!--section block close-->
         </div>
         <!--Form Stage Close-->
         <!--Form Element-->
         <div class="form-element-col">
            <ul>
               <li><a href="javascript:void(0)" title="Add"><span class="icon defaultad"></span></a></li>
            </ul>
         </div>
         <!--Form Element close-->
      </div>
   </div>
</div>
<style>
   .drop-down-wrapper {
      margin: 10px 0 15px 0;
   }

   .cover-pic-wrapper {
      width: 100%;
      height: 200px;
      background-color: #f3f3f3;
      margin: 0 0 20px 0;
   }

   .cover-pic-wrapper img {
      width: 100%;
      height: 200px;
      object-fit: cover;
   }
</style>