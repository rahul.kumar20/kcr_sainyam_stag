<figure class="banner-wrapper">
    <img alt="banner" src="<?php echo base_url(); ?>public/images/form-banner.png">
</figure>
<div class="inner-right-panel">
    <div class="dynamic-form-wrapper">
        <!--Form Stage-->
        <div class="form-stage-wrap">
            <div class="inner-wrapper">
                <div class="form-title">
                    <input type="text" placeholder="Form Title" value="Untitled form">
                </div>
                <div class="form-description">
                    <textarea placeholder="Form Description"></textarea>
                </div>
                <div class="inner-question-html">
                    <!--section block-->
                    <section class="question-block active" id="question-block0">
                        <div class="inner-wrapper">
                            <span class="toggle-btn-wrapper filter-side-wrapper">
                                <span class="dots"></span>
                                <span class="dots"></span>
                                <span class="dots"></span>
                            </span>
                            <div class="form-option">
                                <div class="form-element-col">
                                    <ul onclick="getHtmlByList('innerQusType0',this)">
                                        <li onclick="getHtmlByList('innerQusType0','checkbox')"><a href="javascript:void(0)"><span class="icon checkbox"></span></a></li>
                                        <li onclick="getHtmlByList('innerQusType0','dropdown')"><a href="javascript:void(0)" title="Dropdown"><span class="icon dropdown"></span></a></li>
                                        <li onclick="getHtmlByList('innerQusType0','map')"><a href="javascript:void(0)" title="Map"><span class="icon map"></span></a></li>
                                        <li onclick="getHtmlByList('innerQusType0','file')"><a href="javascript:void(0)" title="File Upload"><span class="icon fileupload"></span></a></li>
                                        <li onclick="getHtmlByList('innerQusType0','date')"><a href="javascript:void(0)" title="Date"><span class="icon datepicker-icon"></span></a></li>
                                        <li onclick="getHtmlByList('innerQusType0','time')"><a href="javascript:void(0)" title="Time"><span class="icon timepicker"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-question">
                                <input type="text" placeholder="Question" name="question[0]">
                            </div>
                            <div id="innerQusType0">
                                <div class="flex-row form-option-wrap" id="attribute0">
                                    <div class="flex-col">
                                        <div class="th-checkbox">
                                            <input style="display:none;" class="filter-type filled-in" type="checkbox" name="filter" id="Yes" value="">
                                            <label for="Yes" class="lbl-check"><span></span></label>
                                        </div>
                                    </div>
                                    <div class="flex-col answer-col">
                                        <span class="delete-icon" onclick=removeInnerHtml("attribute0","innerQusType0",'0') title="Remove"></span>
                                        <input type="text" name="option[0][]" class="answerbox" placeholder="Option 1">
                                    </div>
                                </div>
                            </div>
                            <div class="flex-row">
                                <div class="flex-col">
                                    <span class="add-other" id="addother0" onclick="getInnerQusHtml('innerQusType0','0')">Add Option </span>
                                </div>
                            </div>
                            <div class="flex-row">
                                <div class="flex-col-sm-10">
                                    <div class="action-btn-wrap">
                                        <span class="action-btn delete" onclick="removeQusBlock('question-block0')" title="Delete"></span>
                                        <!--<span class="action-btn duplicate" title="Duplicate"></span>!-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--section block close-->
                    <!--section block-->
                    <!--section block close-->
                </div>
            </div>
            <!--Form Stage Close-->
            <!--Form Element-->
            <div class="form-element-col">
                <ul>
                    <li onclick="addQuestionSection();"><a href="javascript:void(0)" title="Add"><span class="icon defaultad"></span></a></li>
                </ul>
            </div>
            <!--Form Element close-->
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>public/js/dynamicform.js"></script>