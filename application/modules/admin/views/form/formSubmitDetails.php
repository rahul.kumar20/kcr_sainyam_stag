<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>
<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/task">Task Content</a></li>
            <li class="breadcrumb-item active">Form Content</li>
        </ol>
    </div>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title section-form-title" style="font-size: 20px;">User Form Content</h3>
        </div>
        <div class="main" id ="maindiv">
        
     </div>
    
</div>
</div>
<script>
    var formSubmitDetails = <?php echo json_encode($formSubmitDetails); ?>
    
    $( document ).ready(function() {
        appaendQuestionsAndAns();
    });

    function appaendQuestionsAndAns() {
        var questionText = ""
        $.each(formSubmitDetails, function(questionIndex, questionOfIobj) {
            questionText += '<div id="questionOf_'+questionIndex+'" style="font-style: italic;"> Question #'+ (questionIndex + 1) +'</div>'
            questionText += '<div class="col-12 form-item-title">'
            questionText += '<div id="questionOf_'+questionIndex+'" style="background-color: lightgray;"> Q: '+ questionOfIobj["text"] +'</div>'
            questionText += '<div id="questionAnsOf_'+ questionIndex +'" style="padding-top: 1%;"> Ans:  '+ questionOfIobj["answer"] +'</div>';
            questionText += '</div>'
        });
        $("#maindiv").html(questionText);
    }
</script>

