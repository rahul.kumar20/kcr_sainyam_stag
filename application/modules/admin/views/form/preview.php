
<link rel="stylesheet" href="<?php echo base_url()?>public/css/bootstrap-datepicker.css" />
<div class="inner-right-panel">
   <div class="dynamic-form-wrapper">
      <!--Form Stage-->
      <div class="form-stage-wrap">
         <div class="inner-wrapper">
            <div class="form-title">
               <h2><?php echo isset($data['form'])? $data['form']:""?></h2>
            </div>
            <div class="form-description">
               <p><?php echo isset($data['discription'])? $data['discription']:""?>  
               </p>
            </div>
            <!--Cover-pic-->
            <div class="flex-row">
               <div class="flex-col-sm-10">
                  <figure class="cover-pic-wrapper">
                  </figure>
               </div>
            </div>
            <!--Cover-pic close-->
            <!--section block-->
            
         <?php if(isset($data['option']) && !empty($data['option'])):
				
				foreach($data['option'] as $key=>$value):
				
         
         ?>   
            <section class="question-block">
               <div class="inner-wrapper">
                  <div class="form-question">
                     <p class="question"><?php echo isset($data['question'][$key])?$data['question'][$key]:"";?></p>
                     <input type="hidden" name="questionType[<?php echo $key;?>]" value="<?php echo $data['questionType'][$key];?>">
                  </div>
                  
					<?php if(isset($value) && !empty($value)):
						
						
						if(isset($data['questionType']) && (strtolower($data['questionType'][$key])=="text" || strtolower($data['questionType'][$key])=="dropdown")){
							
							$required='';
							if($data['req'][$key]==1){
								
								$required= 'required';
							}
							
							$select =  '<div class="flex-row form-option-wrap">
										 <div class="flex-col">
											<div class="select-wrapper">
											   <select class="form-control" '.$required.' name="option['.$key.'][]">
											   <option value="">Select</option>
											   ';
											   
											   foreach($value as $k=>$val){
												   
												$select.= '<option value="'.$val.'">'.$val.'</option>';
												  }
												  
											  $select.=' </select>
											</div>
										 </div>
									  </div>';
									  
							echo $select;		  
				
						}else if(isset($data['questionType']) && strtolower($data['questionType'][$key])=="checkbox"){
							
								foreach($value as $k=>$val):
						
									?> 
									<div class="flex-row form-option-wrap">
									 <div class="flex-col">
										<div class="th-checkbox">
										   <input style="display:none;" class="filter-type filled-in"  type="checkbox" name="option[<?php echo $key; ?>][<?php echo $k; ?>]" id="option[<?php echo $key; ?>][<?php echo $k; ?>]">
										   <label for="option[<?php echo $key; ?>][<?php echo $k; ?>]" class="lbl-check"><span></span> <?php echo isset($val)?$val:"";?></label>
										</div>
									 </div>
								  </div>
                  
                  
					  <?php 
								endforeach;
						
						}else if(trim($data['questionType'][$key])=="date"){
							
							?>
						 
							 <div class="flex-row form-option-wrap">
								<div class="flex-col">
									<div class="form-group">
										<div class="input-group date datepicker1">
											<input type="text" class="form-control" id="datetime<?php echo $key; ?>" name="option[<?php echo $key?>]" placeholder="Date">
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
										</div>
									</div>	

								</div>
						</div>
						 
					<?php
					}else if(isset($data['questionType']) && strtolower($data['questionType'][$key])=="time"){ ?>
						
						<div class="flex-row form-option-wrap">
                        <div class="flex-col">
                        <div class="input-group bootstrap-timepicker">
                                <input id="timepicker1" class="timepicker" type="text" name="option[<?php echo $key;?>]" placeholder="Time">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-time"></i>
                                </span>
                            </div>


                        </div>
                    </div>
							 
					<?php } else if(isset($data['questionType']) && strtolower($data['questionType'][$key])=="file"){ ?>
						
						<div class="flex-row form-option-wrap">
							<div class="flex-col">
								<label class="add-file" for="upload-doc">Add File </label>    
								<input type="file" id="upload-doc" style="display:none">     

									<div class="upload-file-show">
										<span class="img-icon"></span>
										<span class="upload-path">c:/desktop/main.png</span>
										<span class="cross-icon"></span>
									</div>

							</div>
						</div>
					<?php }
						endif;
					?>
	
               </div>
            </section>
            <?php 
            
            endforeach;
            endif; ?>
            
            <div class="button-wrap text-center">
                           <!-- <button type="button" class="commn-btn cancel">Cancel</button> -->
                           <button type="button" id="back_form" class="commn-btn save" id="">Back</button>
                            <button type="submit" class="commn-btn save" id="">Save</button>
                        </div>	
						
            
            
            <!--section block close-->
         </div>
         <!--Form Stage Close-->
         <!--Form Element-->
         <div class="form-element-col">
         </div>
         <!--Form Element close-->
      </div>
   </div>
</div>
<script src="<?php echo base_url() ?>public/js/bootstrap-datetimepicker.js"></script>
<script>

		$(".datepicker1").datetimepicker({
							format: 'DD-MM-YYYY'
						});

$('.timepicker').timepicker();	
		
$('.banner_upload').click(function(){
$('#avatar-modal').modal('show');
});
</script>


