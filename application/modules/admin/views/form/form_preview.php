<figure class="banner-wrapper">
   <img alt="banner" src="<?php echo base_url(); ?>public/images/form-banner.png">
</figure>
<div class="inner-right-panel">
   <div class="dynamic-form-wrapper">
      <!--Form Stage-->
      <div class="form-stage-wrap">
         <div class="inner-wrapper">
            <div class="form-title">
               <h2>Untitled</h2>
            </div>
            <div class="form-description">
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                  Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s,
               </p>
            </div>
            <!--Cover-pic-->
            <div class="flex-row">
               <div class="flex-col-sm-10">
                  <figure class="cover-pic-wrapper">
                  </figure>
               </div>
            </div>
            <!--Cover-pic close-->
            <!--section block-->
            <section class="question-block">
               <div class="inner-wrapper">
                  <div class="form-question">
                     <p class="question">Q1. Lorem Ipsum is simply dummy text of the printing</p>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="checkbox" name="filter" id="Yes" value="">
                           <label for="Yes" class="lbl-check"><span></span> Option 1</label>
                        </div>
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="checkbox" name="filter" id="Yes" value="">
                           <label for="Yes" class="lbl-check"><span></span> Option 2</label>
                        </div>
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="th-checkbox">
                           <input style="display:none;" class="filter-type filled-in" type="radio" name="filter" id="radioyes" value="">
                           <label for="radioyes" class="lbl-check"><span></span> Option 2</label>
                        </div>
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="select-wrapper">
                           <select class="form-control">
                              <option>Test Option</option>
                              <option>Test Option</option>
                              <option>Test Option</option>
                              <option>Test Option</option>
                              <option>Test Option</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <label class="add-file" for="upload-doc">Add File </label>
                        <input type="file" id="upload-doc" style="display:none">
                        <div class="upload-file-show">
                           <span class="img-icon"></span>
                           <span class="upload-path">c:/desktop/main.png</span>
                           <span class="cross-icon"></span>
                        </div>
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="input-group date" id="datetimepicker1">
                           <input type="text" id="datetime1" value="" placeholder="Date">
                           <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="flex-row form-option-wrap">
                     <div class="flex-col">
                        <div class="input-group date" id="datetimepicker1">
                           <input type="text" id="datetime1" value="" placeholder="Date">
                           <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="button-wrap text-center">
                     <!-- <button type="button" class="commn-btn cancel">Cancel</button> -->
                     <button type="button" class="commn-btn save" id="">Edit</button>
                  </div>
               </div>
            </section>
            <!--section block close-->
         </div>
         <!--Form Stage Close-->
         <!--Form Element-->
         <div class="form-element-col">
         </div>
         <!--Form Element close-->
      </div>
   </div>
</div>
<script>
   $('#datetimepicker1').datetimepicker({
      format: 'DD/MM/YYYY'
   });
</script>