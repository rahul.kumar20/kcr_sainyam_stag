<?php
$filterArr = $this->input->get();
$filterArr = (object)$filterArr;
$controller = $this->router->fetch_class();
$method = $this->router->fetch_method();
$module = $this->router->fetch_module();
?>
<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterArr); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>

<div class="inner-right-panel">
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> LiveSession</li>
        </ol>
    </div>
    <div class="white-wrapper">
        <div class="clearfix">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-item-title col-sm-6">
                        <h3 class="title"> LiveSession</h3>
                    </div>
                </div>
            </div>
            <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php

        } ?>
            <div class="row">
                <div class="col-sm-4">
                      <a href="admin/livesession/golivesession" class="commn-btn save">Go Live Session</a>
                </div>
                <div class="col-sm-4">
                      <a href="admin/livesession/endlivesession" class="commn-btn save">End Live Session</a>
                </div>
                <div class="col-sm-4">
                      <a href="admin/cron/sendSessionNotification" class="commn-btn save">Send Live Session Notification</a>
                </div>
            </div>
        </div>
    </div>
</div>
      