<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker.css" rel='stylesheet'>
<link href="<?php echo base_url() ?>public/datatimepicker/css/bootstrap-datetimepicker-standalone.css" rel='stylesheet'>


<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/livesession">Live Session</a></li>
            <li class="breadcrumb-item active">Live Session</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => 'livesession_add_form')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Go Live Session</h3>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xs-12 m-t-sm">
                <h3 class="section-form-title">Live Session Info</h3>
            </div>
        </div>
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php

        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-6 col-xs-6 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">Youtube Id <mark class="reject-cross">*</mark></label>
                            <div class="input-holder">
                                <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter Id" name="youtubeid" id="youtubeid" value="<?php echo set_value('youtubeid'); ?>">
                                <?php echo form_error('youtubeid', '<label class=" alert-danger">', '</label>'); ?>
                            </div>
                        </div>
                    </div>
                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                    <label class="admin-label">Ringer<mark class="reject-cross">*</mark></label>
                    <input type="radio" name="ringer" id ="active" value="<?php echo 'yes'; ?>">
                    <label for="active">Yes</label><br>
                    <input type="radio" id="inactive" name="ringer" value="<?php echo 'no'; ?>">
                    <label for="inactive">No</label><br>
                    </div>
                </div>
               
                <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="duration" class="admin-label">Ringer Duration</label>
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="duration" id="duration" value="<?php echo set_value('duration'); ?>" placeholder="Enter Time duration">
                            <?php echo form_error('duration', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                </div>

                <div class="col-sm-6 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">One time<mark class="reject-cross">*</mark></label>
                        <div class="commn-select-wrap">
                            <input type="radio" name="onetime" id ="yes" value="<?php echo 'yes'; ?>">
                             <label for="yes">Yes</label><br>
                             <input type="radio" id="no" name="onetime" value="<?php echo 'no'; ?>">
                             <label for="no">No</label><br>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Title<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="title" id="title" value="<?php echo set_value('title'); ?>" placeholder="Enter Title">
                            <?php echo form_error('title', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Message</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" maxlength="100" name="message" id="message"  value="<?php echo set_value('message'); ?>" placeholder="Enter Message">
                            <?php echo form_error('message', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
            <div class="row form-ele-wrapper clearfix">
                <div class="form-ele-action-bottom-wrap btns-center clearfix">
                    <div class="button-wrap text-center">
                      <button type="submit" class="commn-btn save" id="go_live">Go Live</button>
                    </div>
                </div>
            <!--form ele wrapper end-->
            </div>
        
   
    <!--close form view   -->
    <?php echo form_close(); ?>
    <!--Filter Section Close-->


<script src="<?php echo base_url() ?>public/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url() ?>public/datatimepicker/js/moment.js"></script>
<script src="<?php echo base_url() ?>public/js/cropper.min.js"></script>

<script src="<?php echo base_url() ?>public/datatimepicker/js/bootstrap-datetimepicker.min.js"></script>
