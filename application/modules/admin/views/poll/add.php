<link href="<?php echo base_url() ?>public/css/cropper.min.css" rel='stylesheet'>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
<style>
    .card-body label {
        display: block;
    }

    a.add_button,
    a.remove_button {
        position: absolute;
        top: 0;
        right: 52px;
        width: 24px;
    }

    .field_wrapper div {
        position: relative;
        display: inline-block;
        width: 100%;
    }

    .field_wrapper input {
        width: 45%;
        float: left;
    }

    #timepicker {
        width: 20%;
        margin: 30px 40%;
        margin-bottom: 0;
        position: absolute;
    }

    .timepicker_wrap {
        top: 60px;
        left: 20%;
        display: none;
        position: absolute;
        background: hsl(0, 0%, 100%);
        border-radius: 5px;
    }

    .hour,
    .min,
    .meridian,
    .sec {
        float: left;
        width: 65px;
        margin: 0 10px;
    }

    .timepicker_wrap .btn {
        background: url('<?php echo base_url() ?>public/images/arrow.png') no-repeat;
        cursor: pointer;
        padding: 18px;
        border: 3px dotted hsl(216, 46%, 27%);
        width: 28%;
        margin: auto;
        border-radius: 5px;
    }

    .timepicker_wrap .prev {
        background-position: 50% -50%;
    }

    .timepicker_wrap .next {
        background-position: 50% 150%;
    }

    .timepicker_wrap .ti_tx {
        margin: 10px -10px;
        width: 100%;
        text-align: center;
    }

    .timepicker_wrap .in_txt {
        border-radius: 10px;
        width: 90%;
        float: none;
        text-align: center;
        padding-bottom: 8px;
        border: 3px solid hsl(216, 46%, 27%);
        font-family: georgia, helvetica, arial;
        font-weight: 700;
        font-size: 1em;
    }

    .timepicker_wrap>.hour>.ti_tx:after {
        content: '';
        font-size: 1em;
        padding-left: 0.2em;
        font-weight: 700;
    }

    .timepicker_wrap>.min>.ti_tx:after {
        content: '';
        font-size: 1em;
        padding-left: 0.2em;
        font-weight: 700;
    }

    .timepicker_wrap>.sec>.ti_tx:after {
        content: '';
        font-size: 1em;
        padding-left: 0.2em;
        font-weight: 700;
    }

    .timepicker_wrap>.meridian>.ti_tx:after {
        content: '';
        font-size: 1em;
        padding-left: 0.2em;
        font-weight: 700;
    }

    .timepicker_wrap {
        left: 0px;
        width: 100%;
        z-index: 123;
        top: 75px;
        background: #f5f5f5;
        padding: 20px 16px;
        max-width: 100%;
        text-align: center;
    }

    .hour,
    .min,
    .meridian,
    .sec {
        float: none;
        width: 65px;
        display: inline-block;
        margin: 0px;
    }

    .timepicker_wrap .ti_tx {
        margin: 10px auto
    }
</style>

<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>admin/poll">Poll Management</a></li>
            <li class="breadcrumb-item active">Create New Poll</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <form action="<?php echo  $this->config->item('base_url'); ?>/admin/poll/createPollprocess" method="post" enctype="multipart/form-data" onSubmit="return validateinput();">
        <div class="white-wrapper">
            <div class="form-item-title clearfix">
                <h3 class="title">Create New Poll</h3>
            </div>
            <!-- title and form upper action end-->
            <div class="form-section-wrap">
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="poll_heading_en" class="admin-label">Poll Heading English: </label>
                            <input id="poll_heading_en" type="text" class="form-control" value="" name="poll_heading_en">
                            <span class="small" id="poll_heading_en-error" style="color:red;"></span>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="poll_heading_hi" class="admin-label">Poll Heading Tamil: </label>
                            <input id="poll_heading_hi" type="text" class="form-control" value="" name="poll_heading_hi">
                            <span class="small" id="poll_heading_hi-error" style="color:red;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="poll_question_en" class="admin-label">(English)Type your poll question here: </label>
                            <input id="poll_question_en" type="text" class="form-control" value="" name="poll_question_en">
                            <span class="small" id="poll_question_en-error" style="color:red;"></span>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="poll_question_hi" class="admin-label">(Tamil)Type your poll question here: </label>
                            <input id="poll_question_hi" type="text" class="form-control" value="" name="poll_question_hi">
                            <span class="small" id="poll_question_hi-error" style="color:red;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="valid_till" class="admin-label">Valid Till Date</label>
                            <input id="datepicker" type="text" class="form-control" value="" name="valid_till">
                            <span class="small" id="datepicker-error" style="color:red;"></span>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="duration" class="admin-label">Valid Till Time</label>
                            <input type="text" id="valid_till_time" class="timepicker form-control inputDisabled" name="valid_till_time" readonly>
                            <span class="small" id="valid_till_time_error" style="color:red;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-12 m-t-sm">
                        <div class="form-group">
                            <label for="poll_type" class="admin-label">Poll Answer Type</label>
                            <select class="selectpicker" id="poll_type" name="poll_type">
                                <option value="textarea" selected>Description(Textarea)</option>
                                <option value="selectlist">Multiple Answer(Select List)</option>
                                <option value="radio">Single Answer(Radio Button)</option>
                                <option value="linear">Linear Type</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row answerslist" style="display: none;">
                    <div class="col-sm-12 col-xs-12 m-t-sm">
                        <div class="form-group field_wrapper">
                            <label for="poll_question" class="active">Add possible answers to your question:</label>
                            <div>
                                <input class="form-control" placeholder="Option Value English" name="poll_option_en[]" type="text">
                                <input class="form-control" placeholder="Option Value Tamil" name="poll_option_hi[]" type="text"><a href="javascript:void(0);" class="add_button" title="Add field"><img src="<?php echo base_url() ?>public/images/add-icon.png" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <h3 class="section-form-title">Target PTA</h3>
                </div>
            </div>
            <div class="form-section-wrap">
                <div class="row">
                    <div class="col-sm-6 col-xs-4 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">District(optional)</label>
                            <select class="selectpicker distict" data-live-search="true" name="distict" id="distict">
                                <option value="">Select District</option>
                                <?php
                                if (isset($districtlist) && !empty($districtlist)) {
                                    foreach ($districtlist as $districtKey => $districtVal) { ?>
                                        <option value="<?php echo $districtVal['district_id']; ?>"><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-4 m-t-sm">
                        <div class="form-group">
                            <label class="admin-label">Gender(optional)</label>
                            <select class="selectpicker filter gender" name="gender">
                                <option value="">Select Gender</option>
                                <option value="<?php echo MALE_GENDER ?>">Male</option>
                                <option value="<?php echo FEMALE_GENDER ?>">Female</option>
                                <option value="<?php echo OTHER_GENDER ?>">Other</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-xs-4">
                        <div class="form-group">
                            <label class="admin-label">UID(optional)</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#mymodal-csv" title="Upload" id="" class="icon_filter add commn-btn save">Import
                            </a>
                            <input type='text' name="uid" id="uid" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-ele-wrapper clearfix">
                <div class="form-ele-action-bottom-wrap btns-center clearfix">
                    <div class="button-wrap text-center">
                        <button type="button" onclick="window.location.href='<?php echo base_url() ?>admin/poll'" class="commn-btn cancel">Cancel</button>
                        <button type="submit" class="commn-btn save">Add</button>
                    </div>
                </div>
                <!--form ele wrapper end-->
            </div>
            <!--form element wrapper end-->
        </div>
        <!--close form view   -->
    </form>
    <!--Filter Section Close-->
</div>
<!--Table listing-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>public/js/pickerjs.js"></script>
<script src="<?php echo base_url() ?>public/js/cropper.min.js"></script>

<!-- Modal -->
<div id="mymodal-csv" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-alt-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title modal-heading">Upload</h4>
            </div>
            <?php echo form_open_multipart('', array('id' => 'news_upload_csv', 'onsubmit' => 'return doValidateUploadForm();')); ?>
            <div class="modal-body">
                <div class="form-group">
                    <label class="admin-label">Upload File</label>
                    <div class="input-holder">
                        <input type="text" id="upload-btn" class="form-control material-control" autocomplete="off" maxlength="50" name="csvid" id="csvid" value="">
                        <label for="upload-doc" class="upload-btn"> Upload CSV</label>
                        <input onchange="CopyCsv(this, 'upload-btn');" type="file" name="task_csv_file" id="upload-doc" style="display:none;">
                        <span id="file_error"></span>
                    </div>
                    <span class="download-btn">
                        <a href="<?php echo base_url() ?>public/uid_csv_template.csv"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download </a>
                    </span>
                </div>
            </div>
            <div class="modal-footer">
                <div class="button-wrap">
                    <button type="button" class="commn-btn cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" class="commn-btn save csvupload" name="csv-upload" value="upload-csv">Upload</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script>
    function doValidateUploadForm() {
        $('#file_error').text('');
        var cnt = 0;
        var allowed_image_type = ['text/csv'];
        var landscape_image_data = $('#upload-doc')[0].files;

        if (landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length == 0) {

            $('#file_error').text('Please select csv file');
            $('#file_error').css('color', 'red');
            cnt++;
        }
        if (landscape_image_data.length > 0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type) == -1) {

            $('#file_error').text('Please select csv file only');
            $('#file_error').show();
            cnt++;
        }

        if (cnt > 0) {
            return false;
        } else {
            modelHide();
            return true;
        }
    }

    function modelHide() {
        $('#mymodal-csv').modal('hide');
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
    }

    $("#download").click(function() {
        var byteData = $image.cropper("getDataURL");
        $("#main_preview").attr('src', byteData);
        var byteData_data = byteData.split(';')[1].replace("base64,", "");
        $("#cropped_data").val(byteData_data);
        $('#myModal').modal('hide');
    });

    $("#datepicker").datepicker({
        format: " yyyy-mm-dd",
        startDate: '-0m'
    });

    $('#poll_type').change(function() {
        if (this.value == 'selectlist') {
            $(".answerslist").css("display", "block");
        } else if (this.value == 'radio') {
            $(".answerslist").css("display", "block");
        } else {
            $(".answerslist").css("display", "none");
        }
    });

    $(document).ready(function() {
        var maxField = 5; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<div><input type="text" class="form-control" id="poll_option" name="poll_option_en[]" placeholder="Option Value" value=""/><input type="text" class="form-control" id="poll_option_hi" name="poll_option_hi[]" placeholder="Option Value" value="" /><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="<?php echo base_url() ?>public/images/remove-icon.png" /></a></div>'; //New input field html 
        var x = 1; //Initial field counter is 1
        $(addButton).click(function() { //Once add button is clicked
            if (x < maxField) { //Check maximum number of input fields
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); // Add field html   
            }
        });
        $(wrapper).on('click', '.remove_button', function(e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
        });
    });

    function validateinput() {
        // alert('sdfdsf');
        var isValid = true;
        if ($("#poll_heading_en").val().trim() === "") {
            isValid = false;
            $("#poll_heading_en-error").text('Please enter Heading in English').show().fadeOut(7500);
        }
        if ($("#poll_heading_hi").val().trim() === "") {
            isValid = false;
            $("#poll_heading_hi-error").text('Please enter Heading in Tamil').show().fadeOut(7500);
        }
        if ($("#poll_question_en").val().trim() === "") {
            isValid = false;
            $("#poll_question_en-error").text('Please enter question in English').show().fadeOut(7500);
        }
        if ($("#poll_question_hi").val().trim() === "") {
            isValid = false;
            $("#poll_question_hi-error").text('Please enter question in Tamil').show().fadeOut(7500);
        }
        if ($("#datepicker").val().trim() === "") {
            isValid = false;
            $("#datepicker-error").text('Please select date').show().fadeOut(7500);
        }
        var optionvalue = $('input[name=poll_type]:checked').val();
        if (optionvalue === 'selectlist' || optionvalue === 'radio') {
            var captionEn = document.getElementsByName('poll_option_en[]');
            for (i = 0; i < captionEn.length; i++) {
                if (captionEn[i].value == "") {
                    alert('Fill all Option in English');
                    isValid = false;
                }
            }
            var captionHi = document.getElementsByName('poll_option_hi[]');
            for (i = 0; i < captionHi.length; i++) {
                if (captionHi[i].value == "") {
                    alert('Fill all Option in Tamil');
                    isValid = false;
                }
            }
        }
        return isValid;
    }
</script>
<style>
    .img-preview.img-preview-sm {
        width: 100%;
        max-height: 282px;
        overflow: hidden;
        margin: 0 0 10px 0;
    }

    .crop-heading {
        font-size: 20px;
        font-weight: 500;
        margin: 0 0 10px 0;
    }

    .modal-header {
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
        position: relative;
    }

    .modal-bttn-wrap ul li {
        display: inline-block;
        margin: 11px 11px 11px 0;
    }

    .loader_modal {
        position: absolute;
        top: 33%;
        left: 46%;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #60c6f3;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
    }

    @keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }
</style>