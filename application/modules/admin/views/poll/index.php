<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<input type="hidden" id="districtId" value='<?php echo isset($distict) ? $distict : ''; ?>'>
<input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
<input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
<div class="inner-right-panel">
    <!--BREADCRUMB START-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Poll Management</li>
        </ol>
    </div>
    <!--BREADCRUMB END-->
    <?php if (!empty($this->session->flashdata('message_success'))) { ?>
        <div class="alert alert-success" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
        </div>
    <?php } ?>
    <?php if (!empty($this->session->flashdata('message_error'))) { ?>
        <div class="alert alert-danger" style="display:block;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_error'); ?>
        </div>
    <?php } ?>

    <!--FILTER SECTION START-->
    <div class="fltr-srch-wrap white-wrapper clearfix">
        <div class="row">
            <div class="col-lg-2 col-sm-3">
                <div class="display  col-sm-space">
                    <select class="selectpicker dispLimit">
                        <option <?php echo ($limit == 10) ? 'Selected' : '' ?> value="10">Display 10</option>
                        <option <?php echo ($limit == 20) ? 'Selected' : '' ?> value="20">Display 20</option>
                        <option <?php echo ($limit == 50) ? 'Selected' : '' ?> value="50">Display 50</option>
                        <option <?php echo ($limit == 100) ? 'Selected' : '' ?> value="100">Display 100</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="srch-wrap col-sm-space">
                    <button class="srch search-icon" style="cursor:default"></button>
                    <a href="javascript:void(0);"><span class="srch-close-icon searchCloseBtn">X</span></a>
                    <input type="text" maxlength="50" value="<?php echo (isset($searchlike) && !empty($searchlike)) ? $searchlike : '' ?>" class="search-box searchlike" placeholder="Search by Poll Question" id="searchuser" name="search" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-2 col-sm-2">
                <?php if (isset($searchlike) && "" != $searchlike) { ?>
                    <div class="go_back">Go Back</div>
                <?php } ?>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="top-opt-wrap text-right">
                    <ul>
                        <li><a href="admin/poll/createPoll" title="Create Poll" class="icon_filter add"><img src="<?php echo base_url() ?>public/images/plus.svg"></a></li>
                        <li><a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--FILTER SECTION END-->
    <!--FILTER WRAPPER START-->
    <div class="filter-wrap">
        <div class="filter_hd clearfix">
            <div class="pull-left">
                <h2 class="fltr-heading">Filter</h2>
            </div>
            <div class="pull-right">
                <span class="close flt_cl" data-dismiss="modal">X</span>
            </div>
        </div>
        <div class="inner-filter-wrap">
            <div class="fltr-field-wrap">
                <label class="admin-label">UID</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter uid" data-live-search="true" name="uid" id="uid">
                        <option value="">All</option>
                        <?php
                        if (isset($uidlist) && !empty($uidlist)) {
                            foreach ($uidlist as $value) { ?>
                                <option value="<?php echo $value['registeration_no']; ?>" <?php if ($uid == $value['registeration_no']) {  echo 'selected'; } ?>><?php echo $value['registeration_no']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">District</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter distict" data-live-search="true" name="distict" id="distict">
                        <option value="">All</option>
                        <?php
                        if (isset($districtlist) && !empty($districtlist)) {
                            foreach ($districtlist as $districtKey => $districtVal) { ?>
                                <option value="<?php echo $districtVal['district_id']; ?>" <?php if ($distict == $districtVal['district_id']) { echo 'selected'; } ?>><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">Gender</label>
                <div class="commn-select-wrap">
                    <select class="selectpicker filter gender" name="gender">
                        <option value="">All</option>
                        <option <?php echo ($gender == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER ?>">Male</option>
                        <option <?php echo ($gender == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER ?>">Female</option>
                        <option <?php echo ($gender == OTHER_GENDER) ? 'selected' : '' ?> value="<?php echo OTHER_GENDER ?>">Other</option>
                    </select>
                </div>
            </div>
            <div class="fltr-field-wrap">
                <label class="admin-label">Added Date</label>
                <div class="inputfield-wrap">
                    <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
                </div>
            </div>
            <div class="fltr-field-wrap">
                <div class="inputfield-wrap">
                    <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
                </div>
            </div>
            <div class="button-wrap text-center">
                <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
                <button type="submit" class="commn-btn save applyFilterUser" id="filterbutton" name="filter">Apply</button>
            </div>
        </div>
    </div>
    <!--FILTER WRAPPER END-->
     <!--TABLE START-->
    <label id="error">
        <?php $alertMsg = $this->session->flashdata('alertMsg'); ?>
        <div class="alert alert-success" <?php echo (!(isset($alertMsg) && !empty($alertMsg))) ? "style='display:none'" : "" ?> role="alert">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <strong>
                <span class="alertType"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['type'] : "" ?></span>
            </strong>
            <span class="alertText"><?php echo (isset($alertMsg) && !empty($alertMsg)) ? $alertMsg['text'] : "" ?></span>
        </div>
    </label>
    <div class="white-wrapper">
        <p class="tt-count">Total Polls: <?php echo $totalrows ?></p>
        <div class="table-responsive custom-tbl">
            <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Poll Question</th>
                        <th>Poll Options</th>
                        <th>Valid Till</th>
                        <th>Action </th>
                    </tr>
                </thead>
                <tbody id="table_tr">
                    <?php
                    if (isset($pollsList) && count($pollsList)) {
                        $i = 1;
                        if ($page > 1) {
                            $i = (($page * $limit) - $limit) + 1;
                        } 
                        foreach ($pollsList as $value) { ?>
                            <tr id="remove_<?php echo $value['poll_id']; ?>">
                                <td><span class="serialno"><?php echo $i; ?></span></td>
                                <td class="text-center"><?php echo $value['poll_question']; ?></td>
                                <td class="text-center">
                                    <ul>
                                        <?php
                                        foreach ($value['optionArr'] as $k=>$option) { ?>
                                            <li>(<?php echo ($k+1); ?>) <?php echo (!empty($option['option_name_en'])) ? $option['option_name_en'] : $option['option_name_hi']; ?></li>
                                        <?php } ?>
                                    </ul>
                                </td>
                                <td class="text-center"><?php echo $value['valid_till']; ?></td>
                                <td>
                                <a href="javascript:void(0);" class="table_icon" title="Delete"><i class="fa fa-trash" aria-hidden="true" onclick="deleteUser( 'poll',<?php echo DELETED; ?>, '<?php echo encryptDecrypt($value['poll_id']); ?>', 'req/change-user-status', 'Do you really want to delete this poll?' );"></i></a>
                                </td>
                            </tr>
                        <?php $i++; }
                    } else { ?>
                        <tr>
                            <td colspan="9" class="text-center">No result found.</td>
                        </tr>
                    <?php  } ?>
                </tbody>
            </table>
        </div>
        <div class="pagination_wrap clearfix">
            <?php echo $link; ?>
        </div>
    </div>
</div>

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script>
    $(document).ready(function() {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#startDate').datepicker({
            onRender: function(date) {
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            $('#endDate').val('');
            if (ev.date.valueOf() < checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#endDate')[0].focus();
        }).data('datepicker');
        var checkout = $('#endDate').datepicker({
            onRender: function(date) {
                return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');


        //on datepicker 2 focus
        $('#datepicker_2').focus(function() {
            if ($('#datepicker_1').val() == '') {
                checkout.hide();
            }
        });
        //prevent typing datepicker's input
        $('#datepicker_2, #datepicker_1').keydown(function(e) {
            e.preventDefault();
            return false;
        });

    });
</script>

<script>
    function doValidateUploadForm() {
        $('#file_error').text('');
        var cnt = 0;

        var allowed_image_type = ['text/csv']

        var landscape_image_data = $('#upload-doc')[0].files

        if (landscape_image_data == 0 || typeof landscape_image_data == 'undefined' || landscape_image_data.length == 0) {

            $('#file_error').text('Please select csv file');
            $('#file_error').css('color', 'red');
            cnt++;
        }
        if (landscape_image_data.length > 0 && typeof landscape_image_data != 'undefined' && allowed_image_type.indexOf(landscape_image_data[0].type) == -1) {

            $('#file_error').text('Please select csv file only');
            $('#file_error').show();
            cnt++;
        }

        if (cnt > 0) {
            return false;
        } else {
            modelHide();
            return true;
        }
    }

    function modelHide() {
        $('#mymodal-csv').modal('hide');
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
    }
</script>