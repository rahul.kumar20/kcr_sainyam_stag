<div class="inner-right-panel">
    <!--breadcrumb wrap-->
    <div class="breadcrumb-wrap">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Custom Pop up</li>
        </ol>
    </div>
    <!--breadcrumb wrap close-->
    <!--Filter Section -->
    <?php echo form_open_multipart('', array('id' => '')); ?>
    <div class="white-wrapper">
        <div class="form-item-title clearfix">
            <h3 class="title">Custom Pop up</h3>
        </div>
        
        <?php if (!empty($this->session->flashdata('message_success'))) {
        ?>
            <div class="alert alert-success" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
            </div>
        <?php
        } ?>
        <?php if (!empty($this->session->flashdata('message_error'))) {
        ?>
            <div class="alert alert-danger" style="display:block;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>error!</strong> <?php echo $this->session->flashdata('message_error'); ?>
            </div>
        <?php
        } ?>
        <!-- title and form upper action end-->
        <div class="form-section-wrap">
            <div class="row">
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">Description<mark class="reject-cross">*</mark></label>
                        <div class="input-holder">
                            <textarea class="custom-textarea editor1" name="desc" maxlength="200" placeholder="Enter the description" id="desc"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 m-t-sm">
                    <div class="form-group">
                        <label class="admin-label">URL</label>
                        <div class="input-holder">
                            <input type="text" class="form-control material-control" autocomplete="off" placeholder="Enter URL" name="url" id="url" value="<?php echo set_value('url'); ?>">
                            <?php echo form_error('url', '<label class=" alert-danger">', '</label>'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="hidden" value="5" id="imageCnt">
                <div class="col-sm-6 col-xs-12 m-t-sm image-gallery">
                    <label class="admin-label"><span id="img-label">Upload Image</span></label>
                    <figure class="usr-dtl-pic task-upload-gallery">
                        <img src="public/images/placeholder2.png" id="superadminpic">
                        <span class="sm-loader" style="display:none"><img src="public/images/Gray_circles_rotate.gif"></span>
                        <label class="camera" for="upload-img"><i class="fa fa-camera" aria-hidden="true"></i></label>
                        <input type="file" name="popup_image" id="upload-img" style="display:none;">
                    </figure>
                    <div class="upload-prod-pic-wrap task-thumbnails">
                        <ul></ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row form-ele-wrapper clearfix">
            <div class="form-ele-action-bottom-wrap btns-center clearfix">
                <div class="button-wrap text-center">
                    <button type="submit" class="commn-btn save" id="add_popup">save</button>
                </div>
            </div>
            <!--form ele wrapper end-->
        </div>
        <!--form element wrapper end-->
    </div>
    <!--close form view   -->
    <?php echo form_close(); ?>
    <!--Filter Section Close-->
</div>