<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>KCR SAINYAM</title>
    <link rel="icon" type="image/png" sizes="32x32" href="public/images/logo.png">
    <!-- Bootstrap Core CSS -->

    <link href="public/css/bootstrap.min.css" rel='stylesheet'>
    <link href="public/css/style.css" rel='stylesheet'>
    <link href="public/css/plugin/bootstrap-select.min.css" rel='stylesheet'>
    <link href="public/css/plugin/jquery.mCustomScrollbar.min.css" rel='stylesheet'>
    <link href="public/css/plugin/lightgallery.min.css" rel='stylesheet'>
    <link href="public/css/lightbox.css" rel='stylesheet'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet'>
    <link href="<?php echo base_url() ?>public/admin/js/cropper/cropper.min.css" rel="stylesheet">
    <!-- Gallery Video js -->
    <link href="https://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
    <script src="public/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>public/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>public/js/global-msg.js"></script>

    <script>
        var projectglobal = {};
        projectglobal.baseurl = "";
        var csrf_token = <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>;
        var baseUrl = '<?php echo base_url(); ?>';
    </script>

</head>
<?php
$this->load->helper('cookie');
$controller   = strtolower($this->router->fetch_class());
$method       = strtolower($this->router->fetch_method());
$sidebarState = isset($adminInfo["sidebar_state"]) ? $adminInfo["sidebar_state"] : "";
$sideBar      = get_cookie('sideBar');
$sideBar      = isset($sideBar) ? $sideBar : "";
//        echo $controller;die('test');
?>

<body class="<?php echo ($sideBar == 'minimized') ? 'body-sm' : '' ?> loader-wrap">

    <div class="loader-img" id="loader"></div>

    <div class="in-data-wrap">
        <div class="responsive-overlay"></div>
        <div class="filter-overlay"></div>
        <!--left panel-->
        <aside class="left-panel-wrapper">
            <div class="closeSidebar768">x</div>
            <div class="left-panel">
                <div class="inner-left-pannel">
                    <div class="user-short-detail">
                        <div class="image-view-wrapper img-view80p img-viewbdr-radius">
                            <a href="admin/profile">
                                <div id="lefft-logo" class="image-view img-view80" style="background:url('<?php
                                                                                                            echo (!empty($admininfo['admin_profile_pic'])) ? IMAGE_PATH . $admininfo['admin_profile_pic'] : DEFAULT_IMAGE
                                                                                                            ?>')"></div>
                            </a>
                        </div>
                        <span class="user-name"><?php echo (!empty($admininfo['admin_name'])) ? ucwords($admininfo['admin_name']) : "Reusable Admin"; ?></span>
                    </div>
                    <div class="left-menu">
                        <ul>
                            <!-- <li>
                                    <a href="admin/dashboard" class="<?php echo ($controller == 'dashboard') ? 'active' : ''; ?>"  >
                                        <span class="left-side-icon"><i class="fa fa-tachometer"></i></span><label class="nav-txt">Dashboard</label>
                                    </a>
                                </li> -->
                            <li>
                                <a href="admin/reports" class="<?php echo ($controller == 'reports') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-line-chart"></i></span><label class="nav-txt">Reports</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/users" class="<?php echo ($controller == 'user' || $controller == "post_management") ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-users"></i></span><label class="nav-txt">Users</label>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>admin/news" class="<?php echo ($controller == 'news') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-address-card"></i></span><label class="nav-txt">Home Content</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/task" class="<?php echo ($controller == 'task') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-tasks" aria-hidden="true"></i></span><label class="nav-txt">Task Management</label>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>admin/event" class="<?php echo ($controller == 'event') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-address-card"></i></span><label class="nav-txt">Event Management</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/form_list" class="<?php echo ($controller == 'form_list' || $controller == 'form_task') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-wpforms"></i></span><label class="nav-txt">Forms</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/version" class="<?php echo ($controller == 'version') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-code-fork" aria-hidden="true"></i></span><label class="nav-txt">Manage Version</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/contactus" class="<?php echo ($controller == 'usercontactus' && $method != 'category' && $method != 'addcategory') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-address-book" aria-hidden="true"></i></span><label class="nav-txt">Contact Us</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/mks_chat" class="<?php echo ($controller == 'mks_chat' && $method != 'category' && $method != 'addcategory') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-address-book" aria-hidden="true"></i></span><label class="nav-txt">MKS Chat</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/cms" class="<?php echo ($controller == 'cms') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span><label class="nav-txt">Content</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/term_condition" class="<?php echo ($controller == 'term_condition') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-question-circle" aria-hidden="true"></i></span><label class="nav-txt">Terms & Conditions</label>
                                </a>
                            </li>



                            <li>
                                <a href="admin/notification" class="<?php echo ($controller == 'notification') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-bell"></i></span><label class="nav-txt">Notification</label>
                                </a>
                            </li>

                            <li>
                                <a href="admin/podcast" class="<?php echo ($controller == 'podcast') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-podcast"></i></span><label class="nav-txt">PodCast</label>
                                </a>
                            </li>

                            <!-- <li>
                                <a href="admin/livesession" class="<?php echo ($controller =='livesession') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="far fa-bell"></i></span><label class="nav-txt">LiveSession</label>
                                </a>
                            </li> -->

                            <li>
                                <a href="admin/livesessions" class="<?php echo ($controller =='livesessions') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-bell"></i></span><label class="nav-txt">Live Sessions</label>
                                </a>
                            </li>

                            <li>
                                <a href="admin/leaderboard" class="<?php echo ($controller == 'leaderboard') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-shield" aria-hidden="true"></i></span><label class="nav-txt">Leaderboard</label>
                                </a>
                            </li>

                            <li>
                                <a href="admin/user_uploads" class="<?php echo ($controller == 'user_uploads') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-history" aria-hidden="true"></i></span><label class="nav-txt">User Uploads</label>
                                </a>
                            </li>

                            <li>
                                <a href="admin/profileUpdateRequest" class="<?php echo ($controller == 'profile_update_request') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-user-secret"></i></span><label class="nav-txt">Profile Request</label>
                                </a>
                            </li>

                            <li>
                                <a href="admin/user_logs" class="<?php echo ($controller == 'user_logs') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-history" aria-hidden="true"></i></span><label class="nav-txt">User Logs</label>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="admin/Campaign_expense_request" class="<?php echo ($controller == 'campaign_expense_request') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa fa-money"></i></span><label class="nav-txt">Expense Request</label>
                                </a>
                            </li> -->
                            <li>
                                <a href="admin/redards_history" class="<?php echo ($controller == 'reward_earned_history') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-trophy"></i></span><label class="nav-txt">Rewards Management</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/level" class="<?php echo ($controller == 'level') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-line-chart"></i></span><label class="nav-txt">Levels</label>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="admin/achievement" class="<?php echo ($controller == 'achievement') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-trophy"></i></span><label class="nav-txt">Achievements</label>
                                </a>
                            </li>-->
                            <li>
                                <a href="admin/Faq" class="<?php echo ($controller == 'faq') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-question-circle"></i></span><label class="nav-txt">FAQ Management</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/gallary/viewGallery" class="<?php echo ($controller == 'gallary') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-picture-o"></i></span><label class="nav-txt">Gallery</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/category" class="<?php echo ($method == 'category' || $method == 'addcategory') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa fa-phone"></i></span><label class="nav-txt">Contact Us Category</label>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="admin/settings" class="<?php echo ($controller == 'settings') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa fa-gear"></i></span><label class="nav-txt">Settings</label>
                                </a>
                            </li> -->
                            <li>
                                <a href="admin/campaign-form" class="<?php echo ($controller == 'campaign_form_detail') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa fa-gear"></i></span><label class="nav-txt">Campaign Form</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/poll" class="<?php echo ($controller == 'poll') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-question"></i></span><label class="nav-txt">Poll Management</label>
                                </a>
                            </li>
                            <li>
                                <a href="admin/Homefaq" class="<?php echo ($controller == 'homefaq') ? 'active' : ''; ?>">
                                    <span class="left-side-icon"><i class="fa fa-question-circle"></i></span><label class="nav-txt">FAQ (Home Screen)</label>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!--left panel-->
        <div class="right-panel">
            <!--Header-->
            <header>
                <!--toggle button-->
                <div class="toggle-btn-wrap" data-state="<?php echo empty($sidebarState) ? "expanded" : "minimized"; ?>">
                    <div class="line-wrap">
                        <span class="line-bar"></span>
                        <span class="line-bar shot-line-br"></span>
                        <span class="line-bar"></span>
                    </div>
                </div>
                <!--toggle button close-->
                <!--nav brand wrap-->
                <div class="nav-brand">
                    <a href="admin/news" class="brand">
                        <img src="public/images/logo.png" title="Admin Logo">
                    </a>
                </div>
                <!--nav brand wrap close-->
                <!--User Setting-->
                <div class="user-setting-wrap">
                    <div class="user-pic-wrap">
                        <ul>
                            <!--  <li>
                                    <a href="javascript:void(0)" class="notification-icon">
                                        <img src="public/images/notification.svg" alt="Icon" title="Notifications">
                                        <span class="no-of-notification has-notificaion">4</span>
                                    </a>
                                </li> -->
                            <li>
                                <a href="admin/profile">
                                    <img src="public/images/setting.svg" alt="Icon" title="Setting" class="setting-icon">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" onclick="logoutUser();" class="logout">
                                    <img src="public/images/logout.svg" alt="Icon" title="Logout">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--User Setting wrap close-->
            </header>