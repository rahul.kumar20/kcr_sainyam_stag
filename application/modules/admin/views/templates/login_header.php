<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?php echo base_url(); ?>">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>KCR SAINYAM</title>
        <link rel="icon" type="image/png" sizes="32x32" href="public/images/logo.png">
        <!-- Bootstrap Core CSS -->

        <link href="public/css/bootstrap.min.css" rel='stylesheet'>
        <link href="public/css/style.css" rel='stylesheet'>
        <link href="public/css/global.css" rel='stylesheet'>
        <link href="public/css/plugin/bootstrap-select.min.css" rel='stylesheet'>
        <link href="public/css/plugin/jquery.mCustomScrollbar.min.css" rel='stylesheet'>
        <link href="public/css/plugin/lightgallery.min.css" rel='stylesheet'>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel='stylesheet'>
        <!-- Gallery Video js -->
        <link href="http://vjs.zencdn.net/6.6.3/video-js.css" rel="stylesheet">
        <script src="public/js/jquery.min.js"></script>

        <script>
            var projectglobal = {};
            projectglobal.baseurl = "";
            var csrf_token = <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>;
            var baseUrl = '';
        </script>

    </head>
    <?php
    $this->load->helper('cookie');
    $controller   = strtolower($this->router->fetch_class());
    $method       = strtolower($this->router->fetch_method());
    $sidebarState = isset($adminInfo["sidebar_state"]) ? $adminInfo["sidebar_state"] : "";
    $sideBar      = get_cookie('sideBar');
    $sideBar      = isset($sideBar) ? $sideBar : "";
//        echo $controller;die('test');
    ?>
    <body >
