<link href="<?php echo base_url() ?>public/css/datepicker.min.css" rel='stylesheet'>
<div class="inner-right-panel">

   <input type="hidden" id="filterVal" value='<?php echo json_encode($filterVal); ?>'>
   <input type="hidden" id="pageUrl" value='<?php echo base_url() . $module . '/' . strtolower($controller) . '/' . $method; ?>'>
   <div id="graph-data" data-value='<?php echo $graph_data ?>'></div>
   <div id="label-data" data-chart-label='<?php echo $label_data ?>'></div>


   <div class="flex-row">
      <div class="dash-col user">
         <p class="total-userscount"> <?php echo (isset($resultData['totalUser']) && !empty($resultData['totalUser']) ? $resultData['totalUser'] : 0) ?></p>
         <p class="total-newusers">Total Users</p>
      </div>
      <div class="dash-col task">
         <p class="total-userscount"> <?php echo (isset($resultData['totalTask']) && !empty($resultData['totalTask']) ? $resultData['totalTask'] : 0) ?></p>
         <p class="total-newusers">Total Tasks</p>
      </div>
      <div class="dash-col news">
         <p class="total-userscount"> <?php echo (isset($resultData['totalNews']) && !empty($resultData['totalNews']) ? $resultData['totalNews'] : 0) ?></p>
         <p class="total-newusers">Total News</p>
      </div>
   </div>


   <!--Top Perfomer filter-->

   <div class="fltr-srch-wrap white-wrapper clearfix m-t-sm">
      <div class="row">
         <div class="col-lg-2 col-sm-3">
            <h2 class="heading"> Total Performer</h2>
         </div>
         <div class="col-lg-4 col-sm-4">
         </div>
         <div class="col-lg-2 col-sm-2">
         </div>
         <div class="col-lg-4 col-sm-4">
            <div class="top-opt-wrap text-right">
               <ul>
                  <!-- <li>
                     <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper3" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                  </li> -->
                  <li>
                     <a href="javascript:void(0)" title="File Export" class="icon_filter exportCsv"><img src="<?php echo base_url() ?>public/images/export-file.svg"> </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!--Filter-->
   <!--Filter Wrapper-->
   <div class="filter-wrap topLeader">
      <div class="filter_hd clearfix">
         <div class="pull-left">
            <h2 class="fltr-heading">Filter</h2>
         </div>
         <div class="pull-right">
            <span class="close flt_cl" data-dismiss="modal">X</span>
         </div>
      </div>
      <div class="inner-filter-wrap">



         <!-- <div class="fltr-field-wrap">
            <label class="admin-label">State</label>
            <div class="commn-select-wrap">
            <select class="selectpicker filter state2" id="state2" data-live-search="true" name="state" onchange="getDistrictForState2('req/getdistrictbystate',this.value,'')">
            <option value="">All</option>

            <?php
            //if block start
            if (isset($statelist) && !empty($statelist)) {
               //foreach block start
               foreach ($statelist as $stateKey => $stateVal) { ?>
                                 <option value="<?php echo $stateVal['state_id']; ?>" <?php if ($state2 == $stateVal['state_id']) {
                                                                                          echo 'selected';
                                                                                       } ?>><?php echo ucwords(strtolower($stateVal['state_name'])); ?></option>
                           <?php
                           //fr each block end
                        }
                        //if block end
                     }
                           ?>
        </select>
            </div>
         </div> -->
         <div class="fltr-field-wrap">
            <label class="admin-label">District</label>
            <div class="commn-select-wrap">
               <select class="selectpicker district2" data-live-search="true" name="distict2" id="distict2">
                  <option value="">All</option>
                  <?php
                  //if block start
                  if (isset($districtlist) && !empty($districtlist)) {
                     //foreach block start
                     foreach ($districtlist as $districtKey => $districtVal) { ?>
                        <option value="<?php echo $districtVal['district_id']; ?>" <?php if ($distict2 == $districtVal['district_id']) {
                                                                                       echo 'selected';
                                                                                    } ?>><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                  <?php
                        //fr each block end
                     }
                     //if block end 
                  }
                  ?>
               </select>
            </div>
         </div>
         <!-- <div class="fltr-field-wrap">
            <label class="admin-label">College</label>
            <div class="commn-select-wrap">
            <select class="selectpicker college2" name="college_name2" id="collegeDa2" >
            <option value="">All</option>


           </select>
            </div>
         </div> -->
         <div class="fltr-field-wrap">
            <label class="admin-label">Gender</label>
            <div class="commn-select-wrap">
               <select class="selectpicker filter gender2" name="gender2">
                  <option value="">All</option>
                  <option <?php echo ($gender2 == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER ?>">Male</option>
                  <option <?php echo ($gender2 == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER ?>">Female</option>
               </select>

            </div>
         </div>
         <div class="fltr-field-wrap">
            <label class="admin-label">Date</label>
            <div class="inputfield-wrap">
               <input readonly type="text" name="startDate2" data-provide="datepicker" value="<?php echo isset($startDate2) ? $startDate2 : "" ?>" class="form-control startDate" id="startDate2" placeholder="From">
            </div>
         </div>
         <div class="fltr-field-wrap">
            <div class="inputfield-wrap">
               <input readonly type="text" name="endDate2" data-provide="datepicker" value="<?php echo isset($endDate2) ? $endDate2 : "" ?>" class="form-control endDate" id="endDate2" placeholder="To">
            </div>
         </div>
         <div class="button-wrap text-center">
            <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
            <button type="submit" class="commn-btn save topLeaders" id="filterbutton" name="filter">Apply</button>
         </div>
      </div>
   </div>
   <!--Filter-->

   <!--Top performer filter close-->





   <div class="clearfix white-wrapper">
      <div class="table-responsive custom-tbl">
         <!--table div-->
         <table id="example" class="list-table table table-striped sortable" cellspacing="0" width="100%">
            <thead>
               <tr>
                  <th>Rank</th>
                  <th>UID</th>
                  <th>Name</th>
                  <th>Total Point</th>
                  <!-- <th>Total Earning</th> -->
                  <th>Total Task Completed</th>
                  <!-- <th>State</th> -->
                  <th>District</th>
                  <!-- <th>College</th> -->
                  <th>Registered Date</th>
               </tr>

            </thead>
            <tbody id="table_tr">
               <?php
               //if block start
               if (isset($topLeaders) && !empty(($topLeaders))) {
                  foreach ($topLeaders as $key => $value) {
               ?>
                     <tr class="text-center">
                        <td><?php echo ($key + 1) ?></td>
                        <!-- <td><?php echo (isset($value['ranking']) && !empty($value['ranking']) ? $value['ranking'] : 'N/A') ?> </td> -->
                        <td><?php echo (isset($value['registeration_no']) && !empty($value['registeration_no']) ? $value['registeration_no'] : 'N/A') ?></td>
                        <td><?php echo (isset($value['full_name']) && !empty($value['full_name']) ? $value['full_name'] : 'N/A') ?></td>
                        <!-- <td><?php echo (isset($value['rewards']) && !empty($value['rewards']) ? $value['rewards'] : '0') ?></td> -->
                        <td><?php echo (isset($value['total_earning']) && !empty($value['total_earning']) ? $value['total_earning'] : '0') ?></td>
                        <!-- <td> <?php echo (isset($value['task_completed']) && !empty($value['task_completed']) ? $value['task_completed'] : '0') ?></td> -->
                        <td> <?php echo (isset($value['task_count']) && !empty($value['task_count']) ? $value['task_count'] : '0') ?></td>
                        <!-- <td> <?php echo (isset($value['state_name']) && !empty($value['state_name']) ? $value['state_name'] : 'N/A') ?></td> -->
                        <td> <?php echo (isset($value['district_name']) && !empty($value['district_name']) ? $value['district_name'] : 'N/A') ?></td>
                        <!-- <td> <?php echo (isset($value['college_name']) && !empty($value['college_name']) ? $value['college_name'] : 'N/A') ?></td> -->

                        <td><?php echo mdate(DATE_FORMAT, strtotime($value['registered_on'])); ?></td>
                     </tr>

                  <?php }
               } else { ?>
                  <td colspan="9"><?php echo $this->lang->line('no_records_found') ?></td>
               <?php } ?>
            </tbody>
         </table>
      </div>

   </div>



   <!-- <div class="fltr-srch-wrap white-wrapper clearfix m-t-sm">
      <div class="row">
         <div class="col-lg-2 col-sm-3">
            <h2 class="heading"> Total <?php echo isset($title) && !empty($title) ? $title . 's' : 'User' ?></h2>
         </div>
         <div class="col-lg-4 col-sm-4">
         </div>
         <div class="col-lg-2 col-sm-2">
         </div>
         <div class="col-lg-4 col-sm-4">
            <div class="top-opt-wrap text-right">
               <ul>
                  <li>
                     <a href="javascript:void(0)" title="Filter" id="filter-side-wrapper" class="icon_filter"><img src="<?php echo base_url() ?>public/images/filter.svg"></a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="filter-wrap ">
      <div class="filter_hd clearfix">
         <div class="pull-left">
            <h2 class="fltr-heading">Filter</h2>
         </div>
         <div class="pull-right">
            <span class="close flt_cl" data-dismiss="modal">X</span>
         </div>
      </div>
      <div class="inner-filter-wrap">
         <div class="fltr-field-wrap">
            <label class="admin-label">Status</label>
            <div class="commn-select-wrap">
               <select class="selectpicker filter status" name="status">
                  <option value="">All</option>
                  <option value="1" <?php if ($status == ACTIVE) {
                                       echo 'selected';
                                    } ?>>Active</option>
                  <option value="2" <?php if ($status == BLOCKED) {
                                       echo 'selected';
                                    } ?>>Blocked</option>
               </select>
            </div>
         </div>
         <div class="fltr-field-wrap">
            <label class="admin-label">Days</label>
            <div class="commn-select-wrap">
               <select class="selectpicker filter days">
                  <option value="day" <?php if ($days == 'days') { ?> selected<?php } ?>>Daily</option>
                  <option value="week" <?php if ($days == 'week') { ?> selected<?php } ?>>Weekly</option>
                  <option value="month" <?php if ($days == 'month') { ?> selected<?php } ?>>Monthly</option>
                  <option value="year" <?php if ($days == 'year') { ?> selected<?php } ?>>Yearly</option>
               </select>
            </div>
         </div>
         <div class="fltr-field-wrap">
            <label class="admin-label">Task Category</label>
            <div class="commn-select-wrap">
               <select class="selectpicker filter task">
                  <option value="">Task Category</option>
                  <option value="facebook" <?php if ($taskType == 'facebook') {
                                                echo 'selected';
                                             } ?>>Facebook</option>
                  <option value="twitter" <?php if ($taskType == 'twitter') {
                                             echo 'selected';
                                          } ?>>Twitter</option>

               </select>
            </div>
         </div>

         <div class="fltr-field-wrap">
            <label class="admin-label">District</label>
            <div class="commn-select-wrap">
               <select class="selectpicker distict" data-live-search="true" name="distict" id="distict">
                  <option value="">All</option>
                  <?php
                  //if block start
                  if (isset($districtlist) && !empty($districtlist)) {
                     //foreach block start
                     foreach ($districtlist as $districtKey => $districtVal) { ?>
                        <option value="<?php echo $districtVal['district_id']; ?>" <?php if ($distict2 == $districtVal['district_id']) {
                                                                                       echo 'selected';
                                                                                    } ?>><?php echo ucwords(strtolower($districtVal['district_name'])); ?></option>
                  <?php
                        //fr each block end
                     }
                     //if block end 
                  }
                  ?>
               </select>
            </div>
         </div>
         
         <div class="fltr-field-wrap">
            <label class="admin-label">Gender</label>
            <div class="commn-select-wrap">
               <select class="selectpicker filter gender" name="gender">
                  <option value="">All</option>
                  <option <?php echo ($gender == MALE_GENDER) ? 'selected' : '' ?> value="<?php echo MALE_GENDER ?>">Male</option>
                  <option <?php echo ($gender == FEMALE_GENDER) ? 'selected' : '' ?> value="<?php echo FEMALE_GENDER ?>">Female</option>
               </select>

            </div>
         </div>
         <div class="fltr-field-wrap">
            <label class="admin-label">Registration Date</label>
            <div class="inputfield-wrap">
               <input readonly type="text" name="startDate" data-provide="datepicker" value="<?php echo isset($startDate) ? $startDate : "" ?>" class="form-control startDate" id="startDate" placeholder="From">
            </div>
         </div>
         <div class="fltr-field-wrap">
            <div class="inputfield-wrap">
               <input readonly type="text" name="endDate" data-provide="datepicker" value="<?php echo isset($endDate) ? $endDate : "" ?>" class="form-control endDate" id="endDate" placeholder="To">
            </div>
         </div>
         <div class="button-wrap text-center">
            <button type="reset" class="commn-btn cancel resetfilter" id="resetbutton">Reset</button>
            <button type="submit" class="commn-btn save applyFilterreport" id="filterbutton" name="filter">Apply</button>
         </div>
      </div>
   </div> -->
   <!--Filter-->
   <!-- <div class="white-wrapper m-t-sm">
      <div class="chart-wrapper">
         <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </div>
   </div> -->
</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="<?php echo base_url() ?>public/js/reports.js"></script>
<script src="<?php echo base_url() ?>public/js/datepicker.min.js"></script>
<script>
   $(document).ready(function() {

      var nowTemp = new Date();
      var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

      var checkin = $('#startDate').datepicker({
         onRender: function(date) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
         }
      }).on('changeDate', function(ev) {
         $('#endDate').val('');
         if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
         }
         checkin.hide();
         $('#endDate')[0].focus();
      }).data('datepicker');
      var checkout = $('#endDate').datepicker({
         onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
         }
      }).on('changeDate', function(ev) {
         checkout.hide();
      }).data('datepicker');


      //on datepicker 2 focus
      $('#datepicker_2').focus(function() {
         if ($('#datepicker_1').val() == '') {
            checkout.hide();
         }
      });
      //prevent typing datepicker's input
      $('#datepicker_2, #datepicker_1').keydown(function(e) {
         e.preventDefault();
         return false;
      });

   });

   $(document).ready(function() {

      var nowTemp = new Date();
      var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

      var checkin = $('#startDate2').datepicker({
         onRender: function(date) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
         }
      }).on('changeDate', function(ev) {
         $('#endDate2').val('');
         if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
         }
         checkin.hide();
         $('#endDate2')[0].focus();
      }).data('datepicker');
      var checkout = $('#endDate2').datepicker({
         onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
         }
      }).on('changeDate', function(ev) {
         checkout.hide();
      }).data('datepicker');


      //on datepicker 2 focus
      $('#datepicker_2').focus(function() {
         if ($('#datepicker_1').val() == '') {
            checkout.hide();
         }
      });
      //prevent typing datepicker's input
      $('#datepicker_2, #datepicker_1').keydown(function(e) {
         e.preventDefault();
         return false;
      });

   });
</script>