<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Gallary extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
         $this->load->model('Gallary_model');

        $this->load->model('Common_model');
         $this->load->library('Common_function');
        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
    }

   


    /**
     * Add news in database
     *
     * @Function addNews
     * @Description add news
     * @Return view page
     */
    public function index()
    {
        try {
            $data['admininfo'] = $this->admininfo;
            $postdata = $this->input->post();
            $this->load->library('Common_function');

            $data["csrfName"] = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("gallary/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function addGallary()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
           
            if (isset($_FILES['gallary_image']['name']) && !empty($_FILES['gallary_image']['name'])) {
                /* Preparing media array for batch insert */
                $files = $_FILES;

                $imgarr = array();
                $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                if (count($_FILES['gallary_image']['name']) == 1) {
                    $datacount = (count($_FILES['gallary_image']['name'])) + 1;
                } else {
                    $datacount = count($_FILES['gallary_image']['name']);
                }
                for ($i = 0; $i < $datacount - 1; $i++) {
                    $filename = $_FILES['gallary_image']['name'][$i];

                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (in_array($ext, $allowed)) {
                        //image check is it is set or not
                        if (!empty($_FILES['gallary_image']['name'][$i])) {
                            //image insert array
                            $imgarr['gallary_image']['name'] = $files['gallary_image']['name'][$i];
                            $imgarr['gallary_image']['type'] = $files['gallary_image']['type'][$i];
                            $imgarr['gallary_image']['tmp_name'] = $files['gallary_image']['tmp_name'][$i];
                            $imgarr['gallary_image']['error'] = $files['gallary_image']['error'][$i];
                            $imgarr['gallary_image']['size'] = $files['gallary_image']['size'][$i];

                            //upload in s3 server
                            $news_image_url = upload_image_s3($imgarr['gallary_image'], '');


                            $type = explode('/', $files['gallary_image']['type'][$i]);
                            $mediatype = IMAGE;
                            //check type
                            if ($type == 'image') {
                                $mediatype = IMAGE;
                            }
                            /*                             * Image thumb block start */
                            $tempName = $_FILES['gallary_image']['tmp_name'][0];
                            $imageName = $files['gallary_image']['name'][0];
                            $uploads = UPLOAD_IMAGE_PATH;
                            $pathComplete = $uploads;
                          
                            $isSuccess = $this->common_function->uploadImg($tempName, $pathComplete, $imageName);
                            $thumb_url = '';

                            if ($isSuccess) {
                                $imgPath = UPLOAD_IMAGE_PATH . $isSuccess;
                                //image thumb create
                                $flag = $this->common_function->thumb_create($isSuccess, $imgPath, UPLOAD_THUMB_IMAGE_PATH);
                                $thumbarr = array();
                                $thumbarr['name'] = $isSuccess;
                                $thumbarr['tmp_name'] = UPLOAD_THUMB_IMAGE_PATH . $isSuccess;

                                $thumb_url = upload_image_s3($thumbarr, '');
                            }
                            /*                             * Image thumb block end */

                            //Upload media in s3 server
                            $insertGallaryImage[$i] = array(
                                'gallary_media_url' => $news_image_url,
                                'media_thumb' => $thumb_url,
                                'media_type' => $mediatype,
                                'inserted_on' => date('Y-m-d H:i:s')
                            );
                        }//end for loop on new images
                    }//end image extension check
                }
            }
            
        
            if ($_FILES && !empty($_FILES['file']['name']) && isset($_FILES['file']['name'])) {
                      /*                         * Video upload block start */
                      $i = 0;
                      //video insert array
                      $vidarr = array();
                if (!empty($_FILES['file']['name'][$i]) && isset($_FILES['file']['name'])) {
                    $allowed = array('m4v', 'avi', 'mpg', 'mp4', 'webm', 'MOV');
                    $filename = $_FILES['file']['name'][$i];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (in_array($ext, $allowed)) {
                        //preparing array for insert
                        $vidarr['file']['name'] = $_FILES['file']['name'][$i];
                        $vidarr['file']['type'] = $_FILES['file']['type'][$i];
                        $vidarr['file']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                        $vidarr['file']['error'] = $_FILES['file']['error'][$i];
                        $vidarr['file']['size'] = $_FILES['file']['size'][$i];

                        $type = explode('/', $vidarr['file']['type'][$i]);

                        $mediatype = VIDEO;
                        //check media type
                        if ($type == 'video') {
                            $mediatype = VIDEO;
                        }
                          //$index = count($insertNewsImage);
                          #30-08-18
                          $index = isset($insertGallaryImage) ? count($insertGallaryImage) : 0;
                          //create image thumb
                          $imageThumb = create_video_thumb($_FILES['file']['tmp_name']);
                          $thumburl = s3_video_thumb_uploader($imageThumb, '');

                          //Upload media in s3 server
                          //insert array
                          $insertGallaryImage[$index] = array(
                            'gallary_media_url' => upload_image_s3($vidarr['file'], ''),
                            'media_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                            'media_type' => $mediatype,
                            'inserted_on' => date('Y-m-d H:i:s')
                        );
                        //unlink video thunb url
                        unlink($imageThumb);
                    }
                }
            }
                  

        //image array check
            if (isset($insertGallaryImage) && !empty($insertGallaryImage)) {
                //insert in image table
                $this->Common_model->insert_batch('ipac_gallary_media', array(), $insertGallaryImage, '');
          
                $this->session->set_flashdata('message_success', $this->lang->line('gallary_success'));
            }
            redirect(base_url() . 'admin/gallary/viewGallery');
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }


    
    public function viewGallery()
    {
        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
              "limit" => 10,
              "page" => 1,
              "startDate" => "",
              "endDate" => "",
              "searchlike" => "",
              "status" => "",
              "uid" => "",
              "export" => "",
              "field" => "",
              "order" => "",
              'taskCompleted' => '',
              "state" => "",
              "distict" => "",
              "gender" => "",
              "college" => "",
              'type' =>"",
              'taskAct' =>""

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

          //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END



            $gallaryList = $this->Gallary_model->getGallaryList($defaultValue);
        
            $totalrows = $gallaryList['total'];
            $data['gallarylist'] = $gallaryList['result'];
            $defaultValue['taskType'] =!empty($defaultValue['taskType'])?$defaultValue['taskType'] :'';
            $data['taskTotalData'] =$totalrows;
            // Manage Pagination
            $pageurl = 'admin/gallary/viewGallery';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_completed"] = $data["order_by_enddate"] = $data["order_by_startdate"] = $data["order_by_points"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "completed":
                            $data["order_by_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "point":
                            $data["order_by_points"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['type'] = $defaultValue['type'];

            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$gallaryList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/gallary/viewGallery?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['permission'] = $GLOBALS['permission'];
         
            load_views("gallary/galleryview", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
