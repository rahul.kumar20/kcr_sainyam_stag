<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Offline_task_detail extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Offline_task_detail_model');

        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
        # import csv file library
        $this->load->library('csvimport');
    }



    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function index()
    {
        try {

            $get = $this->input->get();
            $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/task/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $userId;
            $data['task_id'] = $get['task_id'];

            $params = array(
                'id' => $userId,
                'task_id' => $get['task_id']
            );
            //get user profile data
            $data['task'] = $this->Offline_task_detail_model->expenseDetail($params);
            $data['task_details'] = $this->Offline_task_detail_model->userTaskDetail($params);
            //User ID Proof
            $pageurl = 'admin/offlineTaskDetail';
            $this->load->library('Common_function');




            $data['status_array'] = [1 => 'Active', 2 => 'Blocked', 3 => 'Deleted'];

            //User Subscription END

            if (empty($data['task'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/task/");
                return 0;
            } //IF END

            load_views("task/offline_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
