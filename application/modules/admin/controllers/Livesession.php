<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Livesession extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Livesession_model');
        $this->load->model('Common_model');
        $this->load->library('Common_function');
        $this->load->library('firestore');
        $this->lang->load('common', 'english');
    }

   //index page for the live session
    public function index()
    {
        try {
            $data['admininfo'] = $this->admininfo;
            $postdata = $this->input->post();
            $this->load->library('Common_function');

            $data["csrfName"] = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;
            load_views("livesession/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
// go live session 
//add live session details to the database
    public function golivesession()
    {
    try {
            $data['admininfo'] = $this->admininfo;
            $this->load->library('Common_function');
            $firebase = $this->firestore->init();
		    $collectionReference = $firebase->collection('mks_live_count');
		    $documentReference = $collectionReference->document('counter');
            $snapshot = $documentReference->snapshot();
		    $countRef = $firebase->collection('mks_live_count')->document('counter');
            $postdata = $this->input->post();
            $session_start_time = date('H:i:s');
            $ringer = $this->input->post('ringer');
            $is_one_time = $this->input->post('onetime');
            $duration = $this->input->post('duration');
            $title = $this->input->post('title');
            $message = $this->input->post('message');  
            if (isset($postdata) && !empty($postdata)) {
                if(ENV == 'production'){
                    $countRef->set([
                        'live_count' => 0
                    ], ['merge' => true]);
                    $data = [
                        'comment_duration' => 60,
                        'live_user_duration' => 60,
                        'staging_video_id' => 'test',
                        'video_id' => !empty($this->input->post('youtubeid')) ? $this->input->post('youtubeid') : ''
                    ];
                }else{
                    $countRef->set([
                        'live_count' => 0
                    ], ['merge' => true]);
                    $data = [
                        'comment_duration' => 60,
                        'live_user_duration' => 60,
                        'staging_video_id' => !empty($this->input->post('youtubeid')) ? $this->input->post('youtubeid') : '',
                        'video_id' => ''
                    ];
                }
                $insertlivesessionArray = array(
                    'session_name'            => 'MKS Live',
                    'session_video_id'        => !empty($this->input->post('youtubeid')) ? $this->input->post('youtubeid') : '',
                    'session_date'            => date('Y-m-d'),
                    'session_start_time'      => $session_start_time,
                    'session_end_time'        => date('H:i:s',strtotime('+2 hour',strtotime($session_start_time))),
                    'is_session_start'        => 1,
                    'total_live_users'        => 0,
                );
                
                if($ringer == 'yes')
                {
                $livesession = $this->Common_model->insert_single('live_session', $insertlivesessionArray);
                $firebase->collection('mks_live_session')->document('live_session')->set($data);
                redirect(base_url() . "admin/cron/sendSessionNotification?is_one_time=".$is_one_time."&duration=".$duration."&title=".$title."&message=".$message."");
                }
                else{
                    $livesession = $this->Common_model->insert_single('live_session', $insertlivesessionArray);
                    $firebase->collection('mks_live_session')->document('live_session')->set($data);
                     redirect(base_url() . 'admin/livesession'); 
                }
            }
            load_views("livesession/golive", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    } 
    //ending live session
    public function endlivesession()
    {
    try {
            $data['admininfo'] = $this->admininfo;
            $this->load->library('Common_function');
            
            $where = array('where' => ['is_session_start' => 1]);
            $getlivesession = $this->Common_model->fetch_data('live_session', '*', $where, true);
            if (count($getlivesession) > 0) { 
            $updatelivesessionArray = array('is_session_start' => '0','session_end_time' => date('Y-m-d H:i:s'));
                $this->Common_model->update_single(
                    'live_session',
                    $updatelivesessionArray,
                    ['where' => ['id' => $getlivesession['id']]]
                );
               redirect(base_url() . 'admin/cron/sendSessionEndNotification');
                
            }
            
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    } 
    
}
