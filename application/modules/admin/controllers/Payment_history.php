<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Payment_history extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Reward_earned_model');
        $this->load->model('User_model');

        $this->lang->load('common', 'english');
        $this->load->helper('common');
           # import csv file library
           $this->load->library('csvimport');
    }



    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try {
            $get = $this->input->get();

            # upload csv content

            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv'))
                && (isset($_FILES['payment_csv_file']['name']) && !empty($_FILES['payment_csv_file']['name']))) {
                if ($_FILES['payment_csv_file']['type'] == 'text/csv') {
                    $msg = $this->payment_csv_import($_FILES['payment_csv_file']['tmp_name']);
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }
                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url().'admin/redards_history');
            }
            $this->load->library('common_function');
            $default = array(
                "id" =>'',
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => "",
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "paymentMethod" => "",
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END
            $paymentInfo = $this->Reward_earned_model->getPaymentHistoryGroup($defaultValue);

                        /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadUserListCsv($paymentInfo['result']);
            }//IF 3 END

            $totalrows = $paymentInfo['total'];
            $data['paymenthistory'] = $paymentInfo['result'];

            // Manage Pagination
            $pageurl = 'admin/payment_history';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_total_user"] = $data["order_by_total_amount"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "total_user":
                            $data["order_by_total_user"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "created_date":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "total_amount":
                            $data["order_by_total_amount"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['id'] = $defaultValue['id'];
            $data['paymentMethod'] = $defaultValue['paymentMethod'];

            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$paymentInfo['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = ( string )($defaultValue['page'] - 1);
                redirect(base_url() . "admin/payment_history?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            load_views("payment_history/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

     /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function downloadUserListCsv($userlist)
    {

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('Payment History List')
            ->setSubject('Payment History List')
            ->setDescription('Payment History List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'E') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'Payment ID')
            ->setCellValue("C1", 'Total User Paid')
            ->setCellValue("D1", 'Total Amount Paid')
            ->setCellValue("E1", 'Date');

        // Add some data
        $x = 2;
        $count = 1;
        foreach ($userlist as $res) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['payment_id']) && ($res['payment_id'] != '')) ? $res['payment_id'] : '')
                ->setCellValue("C$x", (isset($res['total_paid_user']) && ($res['total_paid_user'] != '')) ? $res['total_paid_user'] : '0')
                ->setCellValue("D$x", (isset($res['total_amount_paid']) && ($res['total_amount_paid'] != '')) ? $res['total_amount_paid'] : '0')
                ->setCellValue("E$x", mdate(DATE_FORMAT, strtotime($res['created_date'])));
            $x++;
            $count++;
        }



         // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Payment History List');

        // set right to left direction

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="payment_history_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then   the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
  /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function detail()
    {
        try {
            $get = $this->input->get();

            $paymentID = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/payment_history/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['payment_id'] = $paymentID;

            $pageurl = 'admin/payment_history/detail';
            $this->load->library('Common_function');

            $data['searchlike'] = "";
            $params['limit'] = $data['limit'] = 10;
            $data['page'] = 1;
            $data['order'] = "";

            $params['offset'] = ($data['page'] - 1) * $data['limit'];
            $params["user_id"] = $paymentID;


            $data['status_array'] = [1 => 'Earned', 2 => 'Approval Pending', 3 => 'Redeem',4=>'Reject'];

            //User Subscription END

            if (empty($paymentID)) {//IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            }//IF END
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id"=>"",
                'taskType' =>''
            );
            $statusPayment = array(
                '1' =>'Success'
                );
                            $data['statusPayment'] = $statusPayment;
            $data['paymentOption'] =array('1'=>'Paytm','2'=>'UPI','3'=>'Bank Account');

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END


            $paymentHistoryList= $this->Reward_earned_model->getPaymentHistoryDetail($defaultValue);
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadPaymentCsv($paymentHistoryList['result']);
            }//IF 3 END

            $totalrows = $paymentHistoryList['total'];
            $data['walletHistoryList'] = $paymentHistoryList['result'];

            // Manage Pagination
            $pageurl = 'admin/payment_history/detail';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['taskType'] = $defaultValue['taskType'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$paymentHistoryList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/payment_history/detail?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
           //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
           //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("payment_history/detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

     /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function downloadPaymentCsv($userlist)
    {


        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('Payment History User List')
            ->setSubject('Payment History User List')
            ->setDescription('Payment History User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'N') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'Payment ID')
            ->setCellValue("C1", 'Transaction ID')
            ->setCellValue("D1", 'UID')
            ->setCellValue("E1", 'Payment Amount')
            ->setCellValue("F1", 'Previous Amount')
            ->setCellValue("G1", 'Left Amount')
            ->setCellValue("H1", 'Payment Date')
            ->setCellValue("I1", 'Payment Option')
            ->setCellValue("J1", 'Payment Detail')
            ->setCellValue("K1", 'Status');
        // Add some data
        $x = 2;
        $status_array = array('1' => 'Paid');
        $payment_option_array = array('1' => 'Paytm','2'=>'UPI','3'=>'Bank Account');
        $count = 1;
        foreach ($userlist as $res) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['payment_id']) && ($res['payment_id'] != '')) ? $res['payment_id'] : '')
                ->setCellValue("C$x", (isset($res['transaction_id']) && ($res['transaction_id'] != '')) ? $res['transaction_id'] : '')
                ->setCellValue("D$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '')
                ->setCellValue("E$x", (isset($res['amount_paid']) && ($res['amount_paid'] != '')) ? $res['amount_paid'] : '')
                ->setCellValue("F$x", (isset($res['user_available_amount']) && ($res['user_available_amount'] != '')) ? $res['user_available_amount'] : '')
                ->setCellValue("G$x", (isset($res['user_left_amount']) && ($res['user_left_amount'] != '')) ? $res['user_left_amount'] : '')
                ->setCellValue("H$x", mdate(DATE_FORMAT, strtotime($res['paid_date'])))
                ->setCellValue("I$x", (isset($res['payment_option']) && ($res['payment_option'] != '')) ? $payment_option_array[$res['payment_option']] : '')
                ->setCellValue("J$x", (isset($res['payment_detail']) && ($res['payment_detail'] != '')) ? $res['payment_detail'] : '')
                ->setCellValue("K$x", ( !empty($res['status']) ? ( $status_array[$res['status']]) : 'Not Available'));

            $x++;
            $count++;
        }
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Payment History User List');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="payment_history_user_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
