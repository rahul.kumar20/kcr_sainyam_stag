<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
class Event extends MY_Controller
{
    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Common_model');
        $this->load->model('Event_model');
        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
        # import csv file library
        $this->load->library('csvimport');
    }

    //    public function index()
    //    {
    //        echo 'working';exit;
    //    }


    public function index()
    {

        try { //TRY START
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;

            $get = $this->input->get();

            $default      = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "export" => "",
                "order"      => "",
                "startDate" => "",
                "endDate" => "",
            );

            $defaultValue = defaultValue($get, $default);
            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END

            $data['contactus'] = $this->Event_model->eventlist($defaultValue);
            $totalrows         = $data['contactus']['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$data['contactus']['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/contactus?data=" . queryStringBuilder($defaultValue));
            }

            /* Sorting Query */
            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_title"] = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($data['contactus']['result']);
            } //IF 3 END

            /* paggination */
            $pageurl            = 'admin/event';
            $links              = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name();
            $data["csrfToken"]  = $this->security->get_csrf_hash();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class();
            $data['method']     = $this->router->fetch_method();
            $data['module']     = $this->router->fetch_module();
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            load_views("event/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
 /**add event start */
    public function addEvent()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
            $data['admininfo'] = $this->admininfo;
            $postdata = $this->input->post();
            $this->load->library('Common_function');
           
            if (isset($postdata) && !empty($postdata)) {
                $this->db->trans_begin(); #DB transaction Start
                if (isset($_FILES['event_image']['name']) && !empty($_FILES['event_image']['name'])) {
                    /* Preparing media array for batch insert */
                    $files = $_FILES;
                    $imgarr = array();
                    $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                        $filename = $_FILES['event_image']['name'];
                         $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            //image check is it is set or not
                            if (!empty($_FILES['event_image']['name'])) {
                                //image insert array
                                $imgarr['event_image']['name'] = $files['event_image']['name'];
                                $imgarr['event_image']['type'] = $files['event_image']['type'];
                                $imgarr['event_image']['tmp_name'] = $files['event_image']['tmp_name'];
                                $imgarr['event_image']['error'] = $files['event_image']['error'];
                                $imgarr['event_image']['size'] = $files['event_image']['size'];
   
                                //upload in s3 server
                                $event_image_url = upload_image_s3($imgarr['event_image'], '');
                                $type = explode('/', $files['event_image']['type']);
       
                            }
                }
                $insertEventArray = array(
                    'event_title' => $this->input->post('title'),
                    'event_title_tn' => $this->input->post('title_tn'),
                    'event_location' => !empty($this->input->post('location')) ? $this->input->post('location') : '',
                    'event_location_tn' => !empty($this->input->post('location_tn')) ? $this->input->post('location_tn') : '',
                    'event_time' => $this->input->post('eventTime'),
                    'eventDate' => !empty($this->input->post('eventDate')) ? convert_date_time_format($this->input->post('eventDate')) : '',
                    'image_url' => $event_image_url,
                );
                // print_r($insertEventArray);

                //insert event details
                $eventID = $this->Common_model->insert_single('ipac_event', $insertEventArray);
                // print_r($insertEventArray);
                // print_r($event_image_url);
                // print_r($_FILES);
                // print_r($postdata);
                // exit;
                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    $this->db->trans_commit();
                    #setting Response Array
                    $this->session->set_flashdata('message_success', $this->lang->line('event_success'));
                    redirect(base_url() . 'admin/addEvent');
                } else { #IF transaction failed
                    #rolling back
                    $this->db->trans_rollback();
                    #setting Response Array
                }
            }
            $data['admininfo'] = $this->admininfo;
            //load state helper
            load_views("event/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    /**edit event start */
    public function editEvent()
    {
        try {

            $this->load->helper(array('s3', 'video_thumb'));
            $data['admininfo'] = $this->admininfo;
            $post = $this->input->post();
            $this->load->library('Common_function');
            if (isset($post) && !empty($post)) { //PARENT IF START
                //news id

                $eventID = (isset($post['token']) && !empty($post['token'])) ? encryptDecrypt($post['token'], 'decrypt') : '';
                /**News Update block start */
                //news update array
                $eventUpdateArr = [];
                // preparing array for update
                if (isset($_FILES['event_image']['name']) && !empty($_FILES['event_image']['name'])) {
                    /* Preparing media array for batch insert */
                    $files = $_FILES;
                    $imgarr = array();
                    $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                        $filename = $_FILES['event_image']['name'];
                         $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            //image check is it is set or not
                            if (!empty($_FILES['event_image']['name'])) {
                                //image insert array
                                $imgarr['event_image']['name'] = $files['event_image']['name'];
                                $imgarr['event_image']['type'] = $files['event_image']['type'];
                                $imgarr['event_image']['tmp_name'] = $files['event_image']['tmp_name'];
                                $imgarr['event_image']['error'] = $files['event_image']['error'];
                                $imgarr['event_image']['size'] = $files['event_image']['size'];
   
                                //upload in s3 server
                                $event_image_url = upload_image_s3($imgarr['event_image'], '');
                                $type = explode('/', $files['event_image']['type']);
       
                            }
                }
                $eventUpdateArr = array(
                    'event_title' => $this->input->post('title'),
                    'event_title_tn' => $this->input->post('title_tn'),
                    'event_location' => !empty($this->input->post('location')) ? $this->input->post('location') : '',
                    'event_location_tn' => !empty($this->input->post('location_tn')) ? $this->input->post('location_tn') : '',
                    'event_time' => $this->input->post('eventTime'),
                    'eventDate' => !empty($this->input->post('eventDate')) ? convert_date_time_format($this->input->post('eventDate')) : '',
                    'image_url' => $event_image_url,
                );
                //where condition for update event
                $whereArr = [];
                $whereArr['where'] = array('event_id' => $eventID);
                //update news data
                $isSuccess = $this->Common_model->update_single('ipac_event', $eventUpdateArr, $whereArr);
                $this->load->helper(array('s3', 'video_thumb'));
                $this->load->library('common_function');
                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                   
                    $this->db->trans_commit();
                    #setting Response Array
                    $this->session->set_flashdata('message_success', $this->lang->line('event_upd_success'));
                    redirect(base_url() . 'admin/event');
                } else { #IF transaction failed
                    #rolling back
                    
                    $this->db->trans_rollback();

                    #setting Response Array
                }
                /**News Update block end*/
            } else {
                $get = $this->input->get();

                $eventID = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_news'), "/admin/news/");
                $data = array();

                $data['event_id'] = $eventID;
                //get news detail data
                $data['eventDetail'] = $this->Event_model->eventDetail($eventID);
                //get news image media data
                if (empty($data['eventDetail'])) { //IF START
                    //  show404($this->lang->line('no_news'), "/admin/news/");
                    return 0;
                } //IF END
            }
        } catch (Exception $exception) {
        }
        $data['admininfo'] = $this->data['admininfo'];
        load_views("event/edit", $data);
    }
}
