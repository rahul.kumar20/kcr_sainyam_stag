<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';

class Achievement extends MY_Controller
{
    private $admininfo = "";
    private $data = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Level_model');
        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
    }

    public function index()
    {
        try { 
            $this->load->library('Common_function');
            $get = $this->input->get();
            $default = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "order"      => ""
            );

            $defaultValue               = defaultValue($get, $default);
            $defaultValue['searchlike'] = trim($defaultValue['searchlike']);
            $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];

            // Content List fetching
            $respData = $this->Level_model->achievementListing($defaultValue['limit'], $offset, $defaultValue);

            //Manage pagination
            $pageurl            = 'admin/achievement';
            $data["link"]       = $this->common_function->pagination($pageurl, $respData['total'], $defaultValue['limit']);
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['cmsData']    = $respData['result'];
            $data['totalrows']  = $respData['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$respData['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/achievement?data=" . queryStringBuilder($defaultValue));
            }

            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_date"] = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END

            /* CSRF token */
            $data["csrfName"]  = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            $controller = $this->router->fetch_class();
            $method     = $this->router->fetch_method();
            $module     = $this->router->fetch_module();

            $data['pageUrl'] = base_url() . $module . '/' . strtolower($controller) . '/' . $method;

            load_views("achievement/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } //CATCH END
    }

    public function add()
    {
        try { 
            $this->load->helper(array('s3', 'video_thumb'));
            $postedData        = $this->input->post();
            $data['admininfo'] = $this->admininfo;

            if (count($postedData)) { 
                $this->form_validation->set_rules('name', 'Level Name', 'required|trim');
                $this->form_validation->set_rules('type', 'Unlock Type', 'required');
                if($postedData['type'] == '2'){
                    $this->form_validation->set_rules('reward_points', 'Reward Points', 'required|trim');                
                }else{
                    $this->form_validation->set_rules('fk_level_id','Level', 'required|trim');
                }
                
                $this->form_validation->set_rules('description', 'Description', 'required|trim');

                if ($this->form_validation->run()) { 
                    $savedata['name']                    = $postedData['name'];
                    $savedata['type']                    = $postedData['type'];
                    if($postedData['type'] == '2'){
                        $savedata['reward_points']         = $postedData['reward_points'];
                    }else{
                        $savedata['fk_level_id']           = $postedData['fk_level_id'];
                    }
                    $savedata['description']             = $postedData['description'];
                    $userImageUrl = '';
                    if (isset($_FILES['user_image']['name'])) {
                        $files = $_FILES;
                        $imgarr = array();
                        if (!empty($_FILES['user_image']['name'])) {
                            $imgarr['user_image']['name'] = $files['user_image']['name'];
                            $imgarr['user_image']['type'] = $files['user_image']['type'];
                            $imgarr['user_image']['tmp_name'] = $files['user_image']['tmp_name'];
                            $imgarr['user_image']['error'] = $files['user_image']['error'];
                            $imgarr['user_image']['size'] = $files['user_image']['size'];
    
                            $userImageUrl = upload_image_s3($imgarr['user_image'], '');
                        }
                    }
                    $savedata['image']              = $userImageUrl;
                    $res = $this->saveLevelData($savedata);

                    $alertMsg = [];
                    if ($res) {
                        $alertMsg['text'] = 'Achievement has been created successfully.';
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } 

                    redirect('/admin/achievement');
                } 
            }
            else {
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            } 

            $data['levelList'] = $this->Common_model->fetch_data('tbl_levels', array('pk_level_id', 'name'), []);
            load_views("achievement/add", $data);

        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    private function saveLevelData($data, $updateId = false)
    {
        try { 
            $this->db->trans_start(); 
            if ($updateId) { 
                $this->Common_model->update_single('tbl_achievement', $data, ['where' => ['pk_achievement_id' => $updateId]]);
            } else {
                $this->Common_model->insert_single('tbl_achievement', $data);
            } 

            if (true === $this->db->trans_status()) { 
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            } 
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } 
    }

    public function edit()
    {
        try { 
            $this->load->helper(array('s3', 'video_thumb'));
            $get               = $this->input->get();
            $data['admininfo'] = $this->admininfo;            
            $pageId          = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404();
            $data['page_id'] = $pageId;
            $data['pages']   = $this->Common_model->fetch_data('tbl_achievement', '*', ['where' => ['pk_achievement_id' => $pageId]], true);
            if (empty($data['pages']) && array() == $data['pages']) { 
                show404();
            }

            $postedData = $this->input->post();

            if (count($postedData)) { 
                $this->form_validation->set_rules('name', 'Level Name', 'required|trim');
                $this->form_validation->set_rules('type', 'Unlock Type', 'required');
                if($postedData['type'] == '2'){
                    $this->form_validation->set_rules('reward_points', 'Reward Points', 'required|trim');                
                }else{
                    $this->form_validation->set_rules('fk_level_id','Level', 'required|trim');
                }
                
                $this->form_validation->set_rules('description', 'Description', 'required|trim');

                if ($this->form_validation->run()) { 
                    $savedata['name']                    = $postedData['name'];
                    $savedata['type']                    = $postedData['type'];
                    if($postedData['type'] == '2'){
                        $savedata['reward_points']         = $postedData['reward_points'];
                        $savedata['fk_level_id']           =  null;
                    }else{
                        $savedata['fk_level_id']           = $postedData['fk_level_id'];
                        $savedata['reward_points']         = 0;
                    }
                    $savedata['description']             = $postedData['description'];
                   
                    if (isset($_FILES['user_image']['name'])) {
                        $files = $_FILES;
                        $imgarr = array();
                        if (!empty($_FILES['user_image']['name'])) {
                            $imgarr['user_image']['name'] = $files['user_image']['name'];
                            $imgarr['user_image']['type'] = $files['user_image']['type'];
                            $imgarr['user_image']['tmp_name'] = $files['user_image']['tmp_name'];
                            $imgarr['user_image']['error'] = $files['user_image']['error'];
                            $imgarr['user_image']['size'] = $files['user_image']['size'];
    
                            $userImageUrl        = upload_image_s3($imgarr['user_image'], '');
                            $savedata['image']   = $userImageUrl;
                        }
                    }
                   
                    $res = $this->saveLevelData($savedata, $pageId);
                    if ($res) { 
                        $alertMsg['text'] = 'Achievement has been updated successfully.';
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } 
                    redirect('/admin/achievement');
                } 
            } else {             
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            }
            $data['levelList'] = $this->Common_model->fetch_data('tbl_levels', array('pk_level_id', 'name'), []);
            load_views("achievement/edit", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } 
    }
}
