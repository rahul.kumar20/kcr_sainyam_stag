<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Signup extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Common_function');
        $this->load->model('News_model');
    }

    public function index_post()
    {
        try {
            $postDataArr    = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'user_id',
                    'label' => 'User Id',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'proof_type',
                    'label' => 'Proof Type',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'proof_image',
                    'label' => 'Proof Image',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'id_number',
                    'label' => 'Id Number',
                    'rules' => 'trim|required'
                )
            );


            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {
                $default = array();
                $signupArr    = defaultValue($postDataArr, $default);

                #Check if user registration no already registered
                $whereArr = [];

                # If users enters email for signup
                if (!empty($postDataArr['user_id'])) {

                    $whereArr['where'] = ['user_id' => $postDataArr['user_id']];
                }

                $user_info = $this->Common_model->fetch_data('users', [
                    'user_id', 'registeration_no', 'full_name', 'email_id', 'facebook_id',
                    'twitter_id', 'user_image', 'whatsup_number', 'paytm_number', 'phone_number', 'state', 'district', 'is_active'
                ], $whereArr, true);


                #is user Blocked
                if (!empty($user_info) && 0 == $user_info['is_active']) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE'    => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT'  => (object) array()
                    ];

                    $this->response($response_array);
                } else if (empty($user_info)) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE'    => RECORD_NOT_EXISTS,
                        'MESSAGE' => $this->lang->line('no_user'),
                        'RESULT'  => (object) array()
                    ];

                    $this->response($response_array);
                } else {

                    $this->db->trans_begin(); #DB transaction Start

                    $userId = $user_info['user_id'];

                    $postDataArr['user_id'] = $userId;

                    #Generate Public and Private Access Token
                    $accessToken = create_access_token($userId, $user_info['email_id']);

                    #Access Token for signup
                    $signupArr['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];
                    $signupArr['user_id']     = $userId;

                    $user_info['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];

                    #save proof details
                    $proofData = array();
                    $proofImage = explode(",", $postDataArr['proof_image']);

                    if (!empty($proofImage)) {
                        foreach ($proofImage as $key => $val) {
                            $proofData['user_id']     = $userId;
                            $proofData['id_number']     = $postDataArr['id_number'];
                            $proofData['proof_type']     = $postDataArr['proof_type'];
                            $proofData['image']     = $val;
                            $proofData['inserted_on']     = date('Y-m-d H:i:s');

                            $proofId = $this->Common_model->insert_single('user_id_proof_details', $proofData);
                        }
                    }

                    #Setting session after Signup
                    $sessionArr        = setSessionVariables($postDataArr, $accessToken);

                    /*
                     * Insert Session Data
                     */

                    $whereArr          = [];
                    $device_id         = isset($postDataArr['device_id']) ? $postDataArr['device_id'] : "";
                    $isExist = array();
                    if ($device_id) {
                        $whereArr['where'] = ['device_id' => $device_id];
                        $isExist           = $this->Common_model->fetch_data('user_device_details', array('session_id'), $whereArr, true);
                    }
                    /*
                     * If user has logged in previously with same device then update his detail
                     * or insert as a new row
                     */

                    if (!empty($isExist)) {
                        #updating session table
                        $logData                    = array();
                        $logData['where']           = $whereArr;
                        $logData['updateData']      = $sessionArr;
                        $logData['fetchData']       = $isExist;
                        $this->common_function->writeDeviceDetailsLog('update','admin :: signup >> index_post :: Before Updating ',$logData);
                        $sessionId = $this->Common_model->update_single('user_device_details', $sessionArr, $whereArr);

                        $logData                    = array();
                        $logData['fetchData']       = $this->Common_model->fetch_data('user_device_details','', $whereArr, true);
                        $this->common_function->writeDeviceDetailsLog('update','admin :: signup >> index_post :: After Updating ',$logData);
                    } else {
                        #inserting session details
                        $this->common_function->writeDeviceDetailsLog('insert','admin :: signup >> index_post ',$sessionArr);
                        $sessionId = $this->Common_model->insert_single('user_device_details', $sessionArr);
                    }

                    #comminting Transaction
                    $this->db->trans_commit();

                    if (!empty($sessionId) && !empty($userId)) {
                        #setting response
                        $response_array = [
                            'CODE'    => SUCCESS_CODE,
                            'MESSAGE' => $this->lang->line('registration_success'),
                            'RESULT'  => $user_info
                        ];
                    }
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {

            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT'  => (object) array()
            ];

            #sending response
            $this->response($response_array);
        } #Catch End

    }




    /**
     * @SWG\Post(path="/Signup",
     *   tags={"User"},
     *   summary="Singup Information",
     *   description="Singup Information",
     *   operationId="index_post",
     *   consumes ={"multipart/form-data"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="Authorization",
     *     in="header",
     *     description="",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     description="Users first name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     description="Users last name",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="formData",
     *     description="Password",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="gender",
     *     in="formData",
     *     description="1: Male, 2: Female",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     description="Phone Number",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="dob",
     *     in="formData",
     *     description="Date of Birth m/d/Y",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="address",
     *     in="formData",
     *     description="Address",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="user_lat",
     *     in="formData",
     *     description="Lattitude",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="user_long",
     *     in="formData",
     *     description="Longitude",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="country_id",
     *     in="formData",
     *     description="country Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="state_id",
     *     in="formData",
     *     description="State Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="city_id",
     *     in="formData",
     *     description="City Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_id",
     *     in="formData",
     *     description="Unique Device Id",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="device_token",
     *     in="formData",
     *     description="Device Token required to send push",
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="platform",
     *     in="formData",
     *     description="1: Android and 2: iOS",
     *     type="string"
     *   ),
     *   @SWG\Response(response=200, description="Signup Success"),
     *   @SWG\Response(response=206, description="Unauthorized request"),
     *   @SWG\Response(response=207, description="Header is missing"),
     *   @SWG\Response(response=208, description="Phone number alredy exists"),
     *   @SWG\Response(response=421, description="File Upload Failed"),
     *   @SWG\Response(response=418, description="Required Parameter Missing or Invalid"),
     * )
     */
    public function registration_post()
    {
        try {
            $postDataArr    = $this->post();
            $response_array = [];

            #setting Form  validation Rules
            $required_fields_arr = array(
                array(
                    'field' => 'full_name',
                    'label' => 'Name',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'email_id',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email'
                ),
                array(
                    'field' => 'phone_number',
                    'label' => 'Phone',
                    'rules' => 'trim|required|numeric'
                ),
                array(
                    'field' => 'whatsup_number',
                    'label' => 'Whatsup Number',
                    'rules' => 'trim|numeric'
                ),
                array(
                    'field' => 'gender',
                    'label' => 'gender',
                    'rules' => 'trim|required|numeric'
                ),
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'dob',
                    'label' => 'Date of birth',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'college',
                    'label' => 'College',
                    'rules' => 'trim'
                ),
                array(
                    'field' => 'state',
                    'label' => 'States',
                    'rules' => 'trim|numeric'
                ),
                array(
                    'field' => 'district',
                    'label' => 'District',
                    'rules' => 'trim|numeric'
                ),
                array(
                    'field' => 'device_id',
                    'label' => 'Device Id',
                    'rules' => 'trim|required'
                ),
            );


            #Setting Error Messages for rules
            $this->form_validation->set_rules($required_fields_arr);
            $this->form_validation->set_message('is_unique', 'The %s is already registered with us');
            $this->form_validation->set_message('required', 'Please enter the %s');

            #checking if form fields are valid or not
            if ($this->form_validation->run()) {

                #setting Default values to array KEYS
                $default      = array(
                    "full_name" => "",
                    "email_id"   => "",
                    "phone_number"      => "",
                    "whatsup_number"      => "",
                    "gender"         => "",
                    "password"    => "",
                    "dob"         => "",
                    "college"     => "",
                    "state"  => "",
                    "district"    => "",
                );
                $signupArr    = defaultValue($postDataArr, $default);
                $login_insert = [];

                #checking phone number is valid or not
                if ("" != $signupArr['phone_number']) {
                    $this->validate_phone($signupArr['phone_number']);
                }

                #Checking date of birth is valid or not
                if ("" != $signupArr['dob']) {
                    $this->validate_dob($signupArr['dob']);
                }

                #Check if email if already registered and is it blocked
                $whereArr = [];

                # If users enters email for signup
                if (!empty($signupArr['email_id'])) {

                    $whereArr['where'] = ['email_id' => $signupArr['email_id']];
                }

                # If users enters phone number along with email id for signup
                if (isset($whereArr['where']) && !empty($signupArr['phone_number'])) {

                    $whereArr['or_where'] = ['phone_number' => $signupArr['phone_number']];
                }

                # If users enters only phone number for signup
                if (!empty($signupArr['phone_number']) && !isset($whereArr['phone_number'])) {

                    $whereArr['where'] = ['phone_number' => $signupArr['phone_number']];
                }


                $user_info = $this->Common_model->fetch_data('users', ['phone_number', 'email_id', 'is_active'], $whereArr, true);


                #is user Blocked
                if (!empty($user_info) && 2 == $user_info['is_active']) {
                    #user blocked/Setting Response
                    $response_array = [
                        'CODE'    => ACCOUNT_BLOCKED,
                        'MESSAGE' => $this->lang->line('account_blocked'),
                        'RESULT'  => (object) array()
                    ];

                    $this->response($response_array);
                } else if (($user_info['email_id'] == $signupArr['email_id']) && (ACTIVE == $user_info['is_active'])) {

                    #user is already Registered
                    $response_array = [
                        'CODE'    => EMAIL_ALREADY_EXIST,
                        'MESSAGE' => $this->lang->line('account_exist'),
                        'RESULT'  => (object) array()
                    ];
                } else if (!empty($user_info) && !empty($user_info['phone_number']) && ($user_info['phone_number'] == $signupArr['phone_number']) && (ACTIVE == $user_info['is_active'])) {

                    #user is already Registered
                    $response_array = [
                        'CODE'    => PHONE_ALREADY_EXISTS,
                        'MESSAGE' => $this->lang->line('phone_exist'),
                        'RESULT'  => (object) array()
                    ];
                } else {
                    # $signupArr                    = [];
                    $signupArr["password"]        = createPassword($postDataArr["password"]);
                    $signupArr['registeration_no'] = $this->generateRegistrationNo($postDataArr['district']);
                    $signupArr["registered_on"] = date('Y-m-d H:i:s');



                    $this->db->trans_begin(); #DB transaction Start


                    $userId = $this->Common_model->insert_single('users', $signupArr); #save users details values in DB

                    if (!$userId) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    # add signup data in Login table
                    //  $insert_status = $this->insert_signup_data ( $userId, $signupArr ); #save users details values in signup DB

                    /* if ( ! $insert_status )
                    {
                        throw new Exception ( $this->lang->line ( 'try_again' ) );
                    }
*/
                    $postDataArr['user_id'] = $userId;

                    #Generate Public and Private Access Token
                    $accessToken = create_access_token($userId, $signupArr['email']);

                    #Access Token for signup
                    $signupArr['accesstoken'] = $accessToken['public_key'] . '||' . $accessToken['private_key'];
                    $signupArr["image"]       = isset($signupArr['image']) ? IMAGE_PATH . $signupArr['image'] : "";
                    $signupArr["image_thumb"] = isset($signupArr['image_thumb']) ? THUMB_IMAGE_PATH . $signupArr['image_thumb'] : "";
                    $signupArr['user_id']     = $userId;

                    #Setting session after Signup
                    $sessionArr        = setSessionVariables($postDataArr, $accessToken);
                    //print_r($sessionArr);die;
                    /*
                     * Insert Session Data
                     */
                    $whereArr          = [];
                    $device_id         = isset($postDataArr['device_id']) ? $postDataArr['device_id'] : "";
                    $whereArr['where'] = ['device_id' => $device_id];
                    $isExist           = $this->Common_model->fetch_data('user_device_details', array('session_id'), $whereArr, true);

                    /*
                     * If user has logged in previously with same device then update his detail
                     * or insert as a new row
                     */
                    if (!empty($isExist)) {
                        #updating session table
                        $logData                    = array();
                        $logData['where']           = $whereArr;
                        $logData['updateData']      = $sessionArr;
                        $logData['fetchData']       = $isExist;
                        $this->common_function->writeDeviceDetailsLog('update','admin :: signup >> registration_post :: Before Updating',$logData);
                        $sessionId = $this->Common_model->update_single('user_device_details', $sessionArr, $whereArr);

                        $logData                    = array();
                        $logData['fetchData']       = $this->Common_model->fetch_data('user_device_details','', $whereArr, true);
                        $this->common_function->writeDeviceDetailsLog('update','admin :: signup >> registration_post :: After Updating ',$logData);
                    } else {
                        #inserting session details
                        $this->common_function->writeDeviceDetailsLog('insert','admin :: signup >> registration_post ',$sessionArr);
                        $sessionId = $this->Common_model->insert_single('user_device_details', $sessionArr);
                    }

                    #echo $this->db->last_query ();
                    #exit;
                    #Checking transaction status
                    if ($this->db->trans_status()) {

                        #comminting Transaction
                        $this->db->trans_commit();

                        if (!empty($sessionId) && !empty($userId)) { #if Start
                            unset($signupArr['password']);
                            $mailData               = [];
                            $mailData['name']       = $postDataArr['full_name'];
                            $mailData['email']      = $signupArr['email_id'];
                            $mailData['mailerName'] = 'welcome';
                            $subject                = 'Welcome to ' . PROJECT_NAME;
                            $mailerName             = 'welcome';
                            #sending welcome mail
                            // $this->sendWelcomeMail( $mailData );
                            sendmail($subject, $mailData, $mailerName);

                            #setting response
                            $response_array = [
                                'CODE'    => SUCCESS_CODE,
                                'MESSAGE' => $this->lang->line('registration_success'),
                                'RESULT'  => $signupArr
                            ];
                        } #if End
                    } else {
                        $this->db->trans_rollback();

                        #setting response
                        $response_array = [
                            'CODE'    => TRY_AGAIN_CODE,
                            'MESSAGE' => $this->lang->line('try_again'),
                            'RESULT'  => (object) array()
                        ];
                    }
                }
            } #form validation End
            else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);
                $this->response(array('CODE' => PARAM_REQ, 'MESSAGE' => $arr[0], 'RESULT' => (object) array()));
            }
            $this->response($response_array);
        } #TRY END
        catch (Exception $e) {

            #if transaction failed than rollback
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'CODE'    => TRY_AGAIN_CODE,
                'MESSAGE' => $error,
                'RESULT'  => (object) array()
            ];

            #sending response
            $this->response($response_array);
        } #Catch End

    }


    /**
     * @function sendWelcomeMail
     * @description Sending welcome mail to user
     *
     * @param array $mailData user data needed to send mail, user email address and user name
     */
    private function sendWelcomeMail($mailData)
    {

        $this->load->helper('url');
        $data        = [];
        $data['url'] = base_url() . 'request/welcomeMail?email=' . $mailData['email'] . '&name=' . urlencode($mailData['name']);
        sendGetRequest($data);
    }



    /**
     * @funciton validate_phone
     * @description Custom validation rules to validate phone number
     *
     * @param  $phone user phone number to validate phone number
     * @return boolean|json
     */
    private function validate_phone($phone)
    {

        if (isset($phone) && !preg_match("/^[0-9]{10}$/", $phone) && !empty($phone)) {
            #setting Response
            $response_array = [
                'CODE'    => PARAM_REQ,
                'MESSAGE' => $this->lang->line('invalid_phone'),
                'RESULT'  => []
            ];

            #sending Response
            $this->response($response_array);
        } else {
            return true;
        }
    }



    /*
     * Custom Rule Validate Dob
     * @param: user dob
     */

    /**
     * @function validate_dob
     * @description Custom Rule Validation For Date Of Birth
     *
     * @param date $dob user date of birth to check
     * @return boolean
     */
    private function validate_dob($dob)
    {
        if (!(isValidDate($dob, 'm-d-Y'))) {
            #setting response
            $response_array = [
                'CODE'    => PARAM_REQ,
                'MESSAGE' => $this->lang->line('invalid_dob'),
                'RESULT'  => []
            ];

            #sending Response
            $this->response($response_array);
        } else {
            return true;
        }
    }



    /**
     * @function insert_signup_data
     * @description add user signup data in login table
     *
     * @param string user ID
     * @return array user signup data
     */
    function insert_signup_data($userId, $signupArr)
    {

        $login_insert['user_id']      = $userId;
        $login_insert['email']        = $signupArr["email"];
        $login_insert['password']     = $signupArr["password"];
        $login_insert['created_date'] = date('Y-m-d H:i:s');

        return $this->Common_model->insert_single('ipac_login', $login_insert);
    }

    /* Generating user registration number
     * IPAC XXX PTA YYYY (IPAC, PTA are static and XXX, YYYY are variables)
        First 4 digits represents the company (IPAC) name
        XXX represents the district code (number)
        PTA represents as Part Time Associate
        YYYY represents the volunteer serial number (in respective districts)
     */
    private function generateRegistrationNo($districtId)
    {

        #generate 3 digit district code of user district
        $districtnum = 3;
        $district_num_padded = sprintf("%03d", $districtId);

        # count the number of users in a particular district
        $countUser = $this->News_model->userList($districtId);
        $usercnt = 0;
        if (!empty($countUser)) {
            $usercnt = $countUser->UserCnt;
        }
        $usercnt = $usercnt + 1;
        # generate 4 digit user count for a particular district
        $districtusernum = 4;
        $district_user_num_padded = sprintf("%04d", $usercnt);

        $prefix = 'STLN';
        $sufix = 'PTA';
        $registrationNo = $prefix . $district_num_padded . $sufix . $district_user_num_padded;
        return $registrationNo;
    }
}
