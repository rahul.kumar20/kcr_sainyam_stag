<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->lang->load('common', 'english');
        $this->load->model('Dashboard_model');
    }

    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index4()
    {
        $data['resultData'] = $this->Dashboard_model->getReportsData();
        $data['admininfo'] = $this->admininfo;
        load_views("reports/index", $data);
    }
    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index2()
    {
        $data['admininfo'] = $this->admininfo;
        load_views("reports/index2", $data);
    }
    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index3()
    {
        $data['admininfo'] = $this->admininfo;
        load_views("reports/index3", $data);
    }

    /**
     * @function index
     * @description to load and render dashboard
     */
    public function index()
    {
        $get = $this->input->get();
        try {
            $chart = isset($get['chart']) ? trim($get['chart']) : 'user';
            $title = 'User';
            // filter by day/week/month/year for user
            $data['filterBy'] = $filterBy = isset($get['filterBy']) && !empty($get['filterBy']) ? trim($get['filterBy']) : 'day';
            $data['gender'] = $where['gender'] = isset($get['gender']) ? trim($get['gender']) : '';
            $data['status'] = $where['status'] = isset($get['status']) ? trim($get['status']) : '';
            $data['state'] = $where['state'] = isset($get['state']) ? trim($get['state']) : '';
            $data['district'] = $where['district'] = isset($get['district']) ? trim($get['district']) : '';
            $data['college'] = $where['college'] = isset($get['college']) ? trim($get['college']) : '';
            $data['export'] = isset($get['export']) ? trim($get['export']) : '';
            $data['taskType'] = $where['taskType'] = isset($get['taskType']) ? trim($get['taskType']) : '';
            $data['gender2'] = $where['gender2'] = isset($get['gender2']) ? trim($get['gender2']) : '';
            $data['status2'] = $where['status2'] = isset($get['status2']) ? trim($get['status2']) : '';
            $data['state2'] = $where['state2'] = isset($get['state2']) ? trim($get['state2']) : '';
            $data['distict2'] = $where['distict2'] = isset($get['distict2']) ? trim($get['distict2']) : '';
            $data['college2'] = $where['college2'] = isset($get['college2']) ? trim($get['college2']) : '';
            $data['export'] = isset($get['export']) ? trim($get['export']) : '';
            $data['taskType'] = $where['taskType'] = isset($get['taskType']) ? trim($get['taskType']) : '';
            $data['startDate2'] = isset($get['startDate2']) ? trim($get['startDate2']) : '';

            $data['endDate2'] = isset($get['endDate2']) ? trim($get['endDate2']) : '';

            $default = array(
                "chart" => $chart,
                "filterBy" => '',
                "status" => '',
                "state" => '',
                "district" => '',
                'college' => '',
                "startDate" => '',
                "endDate" => '',
                "export" => '',
                "taskType" => '',
                "status2" => '',
                "state2" => '',
                "distict2" => '',
                'college2' => '',
                "startDate2" => '',
                "endDate2" => '',
            );

            $defaultValue = defaultValue($get, $default);
            $topLeaders= $this->Dashboard_model->fetch_app_leaders($defaultValue);
            $data['topLeaders'] = $topLeaders['leaders'];
            // $data['topLeaders'] = [];

            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($topLeaders['leaders']);
            } //IF 3 END
            // date range
            $where['startdate'] = !empty($get['startDate']) ? date('Y-m-d', strtotime($get['startDate'])) : "";
            $where['enddate'] = !empty($get['endDate']) ? date('Y-m-d', strtotime(($get['endDate']))) : "";
            // if (!empty($chart) && $chart == 'user') {
            //     $title = 'User';
            //     if (!empty($where['startdate']) && !empty($where['enddate'])) {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $filterBy = '';
            //         $data['tt_chart'] = $this->getUserDataByDateRange($where, $data, $method = 'user');
            //     } elseif (!empty($where['gender']) && !empty($where['gender'])) {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            //     } elseif (!empty($where['status']) && !empty($where['status'])) {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            //     } elseif (!empty($where['state']) && !empty($where['state'])) {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            //     } elseif (!empty($where['district']) && !empty($where['district'])) {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            //     } elseif (!empty($where['college']) && !empty($where['college'])) {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            //     } else {
            //         $reqdata = array('number_label' => 'Number of users');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            //     }

            // } elseif (!empty($chart) && $chart == 'task') {
            //     $title = 'Task';
            //     if (!empty($where['startdate']) && !empty($where['enddate'])) {
            //         $filterBy = '';
            //         $reqdata = array('number_label' => 'Number of task');
            //         $data['tt_chart'] = $this->getUserDataByDateRange($where, $data, $method = 'task');
            //     } else {
            //         $reqdata = array('number_label' => 'Number of task');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'task');
            //     }
            // } elseif (!empty($chart) && $chart == 'news') {
            //     $title = 'News';
            //     if (!empty($where['startdate']) && !empty($where['enddate'])) {
            //         $filterBy = '';
            //         $reqdata = array('number_label' => 'Number of news');
            //         $data['tt_chart'] = $this->getUserDataByDateRange($where, $data, $method = 'news');
            //     } else {
            //         $reqdata = array('number_label' => 'Number of news');
            //         $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'news');
            //     }
            // } else {
            //     $reqdata = array('number_label' => 'Number of User');
            //     $data['tt_chart'] = $this->getUserDataForChart($data, $where, $method = 'user');
            // }
            // sort array by key

            // krsort($data['tt_chart']);

            // $data["graph_data"] = json_encode($data['tt_chart']);
            $data["graph_data"] = [];

            // $data['label_data'] = json_encode($reqdata);
            $data['label_data'] = [];

            $data['title'] = $title;
            $data['filterVal'] = $defaultValue;
            $data['chart'] = $chart;
            $data['days'] = $defaultValue['filterBy'];
            $data['status'] = $defaultValue['status'];
            $data['state'] = $defaultValue['state'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['taskType'] = $defaultValue['taskType'];

            // $data['number_label'] = $reqdata['number_label'];
            $data['number_label'] = [];
            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            // /* GET Count */
            // $where = [];
            // $dataCount = [];
            // $where['where'] = ['status !=' => DELETED];
            // $dataCount = $this->Common_model->fetch_data('users', array('count(*) as userCount'), $where, true);
            // /**
            //  * @Description calculate game count
            //  */
            // $where = [];
            // $gameCount = [];
            // // $where['group_by'] = ['game_id'];
            // $gamecount = $this->Common_model->fetch_data('user_answers_count', array('count(*) as gameCount'), $where, true);

            // /**
            //  * @Description calculate No of application opened
            //  */
            // $where = [];
            // $appOpenCount = [];
            // $where['where'] = ['login_status' => ACTIVE];
            // $appOpenCount = $this->Common_model->fetch_data('session', array('count(*) as appOpenCount'), $where, true);

            // /**
            //  * @Description calculate No of mobile phone
            //  */
            // $where = [];
            // $mobileCount = [];
            // $where['where'] = ['platform' => MOBILE];
            // $mobileCount = $this->Common_model->fetch_data('session', array('count(*) as mobileCount'), $where, true);

            // /**
            //  * @Description calculate No of Tablet
            //  */
            // $where = [];
            // $tabletCount = [];
            // $where['where'] = ['platform' => TABLET];
            // $tabletCount = $this->Common_model->fetch_data('session', array('count(*) as tabletCount'), $where, true);

            // $data['admininfo'] = $this->data['admininfo'];
            // $data['userCount'] = $dataCount['userCount'];
            // $data['gameCount'] = $gamecount['gameCount'];
            // $data['appOpenCount'] = $appOpenCount['appOpenCount'];
            // $data['mobileCount'] = $mobileCount['mobileCount'];
            // $data['tabletCount'] = $tabletCount['tabletCount'];
            // /* Filter date */
            // $data['startdate'] = (isset($get['start_date']) && !empty($get['start_date'])) ? $get['start_date'] : '';
            // $data['enddate'] = (isset($get['end_date']) && !empty($get['end_date'])) ? $get['end_date'] : '';
            // $default = array(
            //     "chart" => $chart,
            // );

            // $defaultValue = defaultValue($get, $default);
            // $data['filterVal'] = $defaultValue;
            // $data['controller'] = $this->router->fetch_class();
            // $data['method'] = $this->router->fetch_method();
            // $data['module'] = $this->router->fetch_module();
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //District list for goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
            $data['resultData'] = $this->Dashboard_model->getReportsData();
            $data['admininfo'] = $this->admininfo;
            load_views("reports/index", $data);
        } catch (Exception $ex) {
            echo "Error:" . $ex->getTraceAsString();
        }
    }

    /**
     * @method getUserDataForChart
     * @param string $filterBy
     * @return array
     * @date 18-11-2017
     */
    public function getUserDataForChart($data, $data1 = [], $method)
    {
        $tt_chart = array();
        $tt_data = array();
        $method = $method;
        if ($data['filterBy'] == 'day') {
            // filter by day for user chart
            $startDate = date('Y-m-d');
            for ($i = 1; $i <= 6; $i++) {
                $arr['date'] = $startDate;
                if ($method == 'user') {
                    $where['date(registered_on)'] = $startDate;
                    if (isset($data['gender']) && !empty($data['gender'])) {
                        $where['u.gender'] = $data['gender'];
                    } elseif (isset($data['status']) && !empty($data['status'])) {
                        $where['u.is_active'] = $data['status'];
                    }
                    //by state
                    if (isset($data['state']) && !empty($data['state'])) {
                        $where['u.state'] = $data['state'];
                    }
                    //disrict
                    if (isset($data['district']) && !empty($data['district'])) {
                        $where['u.district'] = $data['district'];
                    }
                    //college
                    if (isset($data['college']) && !empty($data['college'])) {
                        $where['u.college'] = $data['college'];
                    }
                } elseif ($method == 'news') {
                    $where['date(created_date)'] = $startDate;
                    //by state
                    if (isset($data['state']) && !empty($data['state'])) {
                        $where['u.state'] = $data['state'];
                    }
                    //disrict
                    if (isset($data['district']) && !empty($data['district'])) {
                        $where['u.district'] = $data['district'];
                    }
                    //college
                    if (isset($data['college']) && !empty($data['college'])) {
                        $where['u.college'] = $data['college'];
                    }
                } elseif ($method == 'task') {
                    $where['date(created_date)'] = $startDate;
                    //by state
                    if (isset($data['state']) && !empty($data['state'])) {
                        $where['state'] = $data['state'];
                    }
                    //disrict
                    if (isset($data['district']) && !empty($data['district'])) {
                        $where['district'] = $data['district'];
                    }
                    //college
                    if (isset($data['college']) && !empty($data['college'])) {
                        $where['college'] = $data['college'];
                    }
                    //college
                    if (isset($data['taskType']) && !empty($data['taskType'])) {
                        $where['task_type'] = $data['taskType'];
                    }
                } else {
                    $where['date(registered_on)'] = $startDate;
                }
                $count = $this->Dashboard_model->getUserCount($where, $data1, $method);
                $arr['count'] = $count['total'];
                $arr['result'] = $count['result'];
                array_push($tt_chart, $arr);
                array_push($tt_data, $arr['result']);

                $startDate = date('Y-m-d', strtotime($startDate . "-1 days"));
            }
            if ($method == 'user') {
                /*
                 * Export to Csv
                 */
                if ($data['export']) { //IF 3 START
                    echo 'hello';
                    die;
                    $this->exportUser($tt_data);
                    return false;
                } //IF 3 END
            }
        } elseif ($data['filterBy'] == 'week') {
            // filter by week for user chart
            $startDate = date('Y-m-d');
            $endDate = date('Y-m-d', strtotime($startDate . "-7 days"));
            for ($i = 1; $i <= 6; $i++) {
                $arr['date'] = $endDate . ' To ' . $startDate;
                if ($method == 'user') {
                    $where['date(registered_on)<='] = $startDate;
                    $where['date(registered_on)>'] = $endDate;
                    if (isset($data['gender']) && !empty($data['gender'])) {
                        $where['u.gender'] = $data['gender'];
                    } elseif (isset($data['status']) && !empty($data['status'])) {
                        $where['u.is_active'] = $data['status'];
                    }
                } elseif ($method == 'task') {
                    $where['date(created_date)<='] = $startDate;
                    $where['date(created_date)>'] = $endDate;
                } elseif ($method == 'news') {
                    $where['date(created_date)<='] = $startDate;
                    $where['date(created_date)>'] = $endDate;
                } else {
                    $where['date(registered_on)<='] = $startDate;
                    $where['date(registered_on)>'] = $endDate;
                }

                $count = $this->Dashboard_model->getUserCount($where, $data1, $method);
                $arr['count'] = $count['total'];
                array_push($tt_chart, $arr);
                $startDate = date('Y-m-d', strtotime($startDate . "-7 days"));
                $endDate = date('Y-m-d', strtotime($endDate . "-7 days"));
            }
        } elseif ($data['filterBy'] == 'month') {
            // filter by month for user chart
            $month = date('m');
            $year = date('Y');
            for ($i = 1; $i <= 12; $i++) {
                $arr['date'] = date('M', strtotime("01-" . $month . "-" . $year)) . " - " . $year;
                if ($method == 'user') {
                    $where['month(registered_on)'] = $month;
                    $where['year(registered_on)'] = $year;
                    if (isset($data['gender']) && !empty($data['gender'])) {
                        $where['u.gender'] = $data['gender'];
                    } elseif (isset($data['status']) && !empty($data['status'])) {
                        $where['u.is_active'] = $data['status'];
                    }
                } elseif ($method == 'task') {
                    $where['month(created_date)'] = $month;
                    $where['year(created_date)'] = $year;
                } elseif ($method == 'news') {
                    $where['month(created_date)'] = $month;
                    $where['year(created_date)'] = $year;
                } else {
                    $where['month(registered_date)'] = $month;
                    $where['year(registered_date)'] = $year;
                }

                $count = $this->Dashboard_model->getUserCount($where, $data1, $method);
                $arr['count'] = $count['total'];
                array_push($tt_chart, $arr);
                $month = $month - 1;
                if ($month == 0) {
                    $year = $year - 1;
                    $month = 12;
                }
            }
        } elseif ($data['filterBy'] == 'year') {
            // filter by year for user chart
            $time = date("Y");
            for ($i = 1; $i <= 6; $i++) {
                $arr['date'] = $time;
                if ($method == 'user') {
                    $where['year(registered_on)'] = $time;
                    if (isset($data['gender']) && !empty($data['gender'])) {
                        $where['u.gender'] = $data['gender'];
                    } elseif (isset($data['status']) && !empty($data['status'])) {
                        $where['u.is_active'] = $data['status'];
                    }
                } elseif ($method == 'task') {
                    $where['year(created_date)'] = $time;
                } elseif ($method == 'news') {
                    $where['year(created_date)'] = $time;
                } else {
                    $where['year(registered_date)'] = $time;
                }
                $count = $this->Dashboard_model->getUserCount($where, $data1, $method);
                $arr['count'] = $count['total'];

                array_push($tt_chart, $arr);
                $time = $time - 1;
            }
        }
        return $tt_chart;
    }

    /**
     * @method getUserDataByDateRange
     * @param array $dataRange
     * @return array
     */
    public function getUserDataByDateRange($dataRange, $data, $method)
    {
        $method = $method;
        $tt_chart = array();

        $y11 = date('Y', strtotime($dataRange['startdate']));
        $y21 = date('Y', strtotime($dataRange['enddate']));
        $m11 = date('m', strtotime($dataRange['startdate']));
        $m21 = date('m', strtotime($dataRange['enddate']));
        $d11 = date('d', strtotime($dataRange['startdate']));
        $d21 = date('d', strtotime($dataRange['enddate']));
//        $start_date = strtotime($dataRange['startdate']); use later
        $end_date = strtotime($dataRange['enddate']);
        $date_start1 = date('Y-m-d', $end_date);
        if ($d21 - $d11 < 8 && $y21 - $y11 == 0 && $m21 - $m11 == 0) {
            for ($i = 0; $i <= $d21 - $d11; $i++) {
                $arr['date'] = $date_start1;
                if ($method == 'user') {
                    $where['date(registered_on)'] = $date_start1;
                } elseif ($method == 'task') {
                    $where['date(created_date)'] = $date_start1;
                } elseif ($method == 'news') {
                    $where['date(created_date)'] = $date_start1;
                } else {
                    $where['date(registered_on)'] = $date_start1;
                }
                $count = $this->Dashboard_model->getUserCount($where, $data, $method);
                $arr['count'] = $count['total'];
                array_push($tt_chart, $arr);
                $date_start1 = date('Y-m-d', strtotime($date_start1 . "-1 days"));
            }
        } elseif ($d21 - $d11 > 7 && $d21 - $d11 <= 31 && $y21 - $y11 == 0 && $m21 - $m11 == 0) {
            for ($i = 0; $i <= $d21 - $d11; $i++) {
                $arr['date'] = $date_start1;
                if ($method == 'user') {
                    $where['date(registered_on)'] = $date_start1;
                } elseif ($method == 'task') {
                    $where['date(created_date)'] = $date_start1;
                } elseif ($method == 'news') {
                    $where['date(created_date)'] = $date_start1;
                } else {
                    $where['date(registered_on)'] = $date_start1;
                }

                $count = $this->Dashboard_model->getUserCount($where, $data, $method);
                $arr['count'] = $count['total'];
                array_push($tt_chart, $arr);
                $date_start1 = date('Y-m-d', strtotime($date_start1 . "-1 days"));
            }
        } elseif ($y21 - $y11 > 0) {
            if ($m21 < $m11) {
                $m21 = $m21 + 12;
            }
            $month = date('m', $end_date);
            $year = date('Y', $end_date);
            for ($i = 0; $i <= $m21 - $m11; $i++) {
                $arr['date'] = date('M', strtotime("01-" . $month . "-" . $year)) . " - " . $year;

                if ($method == 'user') {
                    $where['month(registered_on)'] = $month;
                    $where['year(registered_date)'] = $year;
                } elseif ($method == 'task') {
                    $where['month(created_date)'] = $month;
                    $where['year(created_date)'] = $year;
                } elseif ($method == 'news') {
                    $where['month(created_date)'] = $month;
                    $where['year(created_date)'] = $year;
                } else {
                    $where['month(registered_on)'] = $month;
                    $where['year(registered_on)'] = $year;
                }
                $count = $this->Dashboard_model->getUserCount($where, $data, $method);
                $arr['count'] = $count['total'];
                array_push($tt_chart, $arr);
                $month = $month - 1;
                if ($month == 0) {
                    $year = $year - 1;
                    $month = 12;
                }
            }
        } else {
            $month = date('m', $end_date);
            $year = date('Y', $end_date);
            for ($i = 0; $i <= $m21 - $m11; $i++) {
                $arr['date'] = date('M', strtotime("01-" . $month . "-" . $year)) . " - " . $year;
                if ($method == 'request') {
                    $where['month(registered_on)'] = $month;
                    $where['year(registered_on)'] = $year;
                } elseif ($method == 'task') {
                    $where['month(created_date)'] = $month;
                    $where['year(created_date)'] = $year;
                } elseif ($method == 'news') {
                    $where['month(created_date)'] = $month;
                    $where['year(created_date)'] = $year;
                } else {
                    $where['month(registered_on)'] = $month;
                    $where['year(registered_on)'] = $year;
                }
                $count = $this->Dashboard_model->getUserCount($where, $data, $method);
                $arr['count'] = $count['total'];
                array_push($tt_chart, $arr);
                $month = $month - 1;
                if ($month == 0) {
                    $year = $year - 1;
                    $month = 12;
                }
            }
        }

        return $tt_chart;
    }

    /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function download($userlist)
    {

        //  $subscribers = $this->phpexcel_model->get_users();

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        // $spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);

        // auto fit column to content
        // foreach (range('A', 'J') as $columnID) {
            // $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                // ->setAutoSize(true);
        // }
        foreach (range('A', 'G') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'Rank')
            ->setCellValue("B1", 'UID')
            ->setCellValue("C1", 'Name')
            ->setCellValue("D1", 'Total Point')
            // ->setCellValue("E1", 'Total Earning')
            ->setCellValue("E1", 'Total Task Complete')
            // ->setCellValue("G1", 'State')
            ->setCellValue("F1", 'District')
            // ->setCellValue("I1", 'College')
            ->setCellValue("G1", 'Registration Date');

        // Add some data
        $x = 2;
        $status = array('1' => 'Active', '2' => 'Blocked');
        $count = 1;
        foreach ($userlist as $res) {
            $date = date_create($res['registered_on']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
            //->setCellValue("A$x", (isset($res['ranking']) && ($res['ranking'] != '')) ? $res['ranking'] : '')
            ->setCellValue("A$x", (isset($count) && ($count != '')) ? $count : '')
            ->setCellValue("B$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '')
            ->setCellValue("C$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '')
            // ->setCellValue("D$x", (isset($res['rewards']) && ($res['rewards'] != '')) ? $res['rewards'] : '')
            ->setCellValue("D$x", (isset($res['total_earning']) && ($res['total_earning'] != '')) ? $res['total_earning'] : '')
            ->setCellValue("E$x", (isset($res['task_count']) && ($res['task_count'] != '')) ? $res['task_count'] : '')
            // ->setCellValue("G$x", (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : '')
            ->setCellValue("F$x", (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : '')
            // ->setCellValue("I$x", (isset($res['college_name']) && ($res['college_name'] != '')) ? $res['college_name'] : '')
            ->setCellValue("G$x", $Date . ' ' . $Time);

            $x++;
            $count++;
        }

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

// set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="top_user_list.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
