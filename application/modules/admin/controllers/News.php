<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
class News extends MY_Controller
{
    private $admininfo = "";
    private $data = array();

    public function __construct() 
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('News_model');
        $this->load->model('Common_model');
        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
        # import csv file library
        $this->load->library('csvimport');
    }


    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {
	    try {

            # upload csv content
            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv'))
                && (isset($_FILES['news_csv_file']['name']) && !empty($_FILES['news_csv_file']['name']))
            ) {
                if ($_FILES['news_csv_file']['type'] == 'text/csv') {
                    $msg = $this->news_csv_import($_FILES['news_csv_file']['tmp_name']);
                } else {
                    $msg = 'Invalid file type. Please upload csv file only';
                }

                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . 'admin/news');
            }

            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "EffectivestartDate" => "",
                "EffectiveendDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                'college' => "",
                "homeContentType" => "",
                "section" => ""
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END

            
            $newsList = $this->News_model->newsList($defaultValue);

            $districtdata = array();

            if (!empty($defaultValue['state'])) {
                $whereArr['where'] = ['state_id' => $defaultValue['state']];
                $whereArr['order_by'] = ['district_name' => 'Asc'];
                $districtdata = $this->Common_model->fetch_data('district', 'district_code as district_id,district_name', $whereArr);
            }
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($newsList['result']);
            } //IF 3 END

            $totalrows = $newsList['total'];
            $data['newslist'] = $newsList['result'];
            $data['districtlist'] = $districtdata;

            // Manage Pagination
            $pageurl = 'admin/news';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_priority"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "priority":
                            $data["order_by_priority"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['EffectivestartDate'] = $defaultValue['EffectivestartDate'];
            $data['EffectiveendDate'] = $defaultValue['EffectiveendDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['section'] = $defaultValue['section'];
            $data['homeContentType'] = $defaultValue['homeContentType'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['sectionArray'] = array( 'people_speaks' => 'People Speaks','news' => 'News',  'gallery' => 'Gallery','stories' => 'Stories');
            $data['cattegoryArray'] = array('1' => 'Gallery', '2' => 'Article', '3' => 'News', '4' => 'Notification', '5' => 'Banner', '6' => 'Form', '7' => 'Party News', '8' => 'Murasoli', '9' => 'Text', '10' => 'Image', '11' => 'Video');

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            $data['gender_type'] = unserialize(GENDER_TYPE);

            
            //print_r($data); exit;
            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$newsList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/news?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            $data['permission'] = $GLOBALS['permission'];
            /*//load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //District list for goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);*/
            $data['statelist']      = array();
            $data['districtlist']   = array();
            $data['uidlist']        = array();
            load_views("news/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }


    /**
     *
     *@Function news_csv_import
     *@Description import csv data in to database
     */
    public function news_csv_import($file_path)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->load->helper(array('s3', 'video_thumb'));
        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        $newsArray = array();
        //creating array for news type
        foreach ($file_data as $data) {
            $data = array_values($data);
            $data = array_map('trim', $data);
            if (!array_key_exists($data[0], $newsArray)) {
                $newsArray[$data[0]] = $data;
            } else {
                for ($i = 6; $i < 7; $i++) {
                    if ($data[$i] != '') {
                        if (strpos($newsArray[$data[0]][$i], $data[$i]) === false) {
                            $newsArray[$data[0]][$i] = $newsArray[$data[0]][$i] . "," . $data[$i];
                        }
                    }
                }
            }
        }
        foreach ($newsArray as $row) {
            $date_time = date('Y-m-d H:i:s');
            $row = array_values($row);

            # check for required params
            $news_title = trim($row[1]);
            $news_description = trim($row[2]);
            $news_image_url = trim($row[3]);
            $news_video_url = trim($row[4]);
            $type = trim($row[5]);
            $news_uid = trim($row[6]);
            $banner_height = trim($row[7]);

            if (!$news_title || !$news_description) {
                $not_saved_record = $not_saved_record + 1;
                continue;
            } else {
                # check for the user's registration id

                if ($news_uid) {
                    $registeration_data = $this->Common_model->fetch_data('users', 'registeration_no', array('where_in' => array('registeration_no' => explode(',', $news_uid))), true);

                    if (!$registeration_data) {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                }
                //banner height is greater than 100
                if ($banner_height > 100) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                }

                # save the record to the database
                $ins_data = [
                    'news_title' => $news_title,
                    'news_description' => $news_description,
                    'created_date' => $date_time,
                    'updated_date' => $date_time,
                    'state' => 0,
                    'district' => 0,
                    'registeration_no' => !empty($news_uid) ? $news_uid : 0,
                    'college' => 0,
                    'gender' => 0,
                    'news_category' => $type,
                    'banner_height' => !empty($banner_height) ? $banner_height : 0,

                ];

                $newsId = $this->Common_model->insert_single('ipac_news', $ins_data);

                if ($newsId) {
                    # save the news media files
                    $this->news_media_files($news_image_url, $news_video_url, $newsId, $date_time);
                    $saved_record = $saved_record + 1;
                } else {
                    $not_saved_record = $not_saved_record + 1;
                }
            }
        } // end foreach loop here


        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path


    private function news_media_files($news_image_url, $news_video_url, $newsId, $date_time)
    {
        $images_url = explode(',', $news_image_url);
        # for images

        foreach ($images_url as $old_file_url) {
            $file_url = explode('/', $old_file_url);
            $file_name = array_pop($file_url);

            $file_extenstion = explode('.', $file_name);
            $file_extenstion = array_pop($file_extenstion);
            $file_extenstion = strtolower($file_extenstion);

            if (!in_array($file_extenstion, ['jpg', 'jpeg', 'png'])) {
                continue;
            }

            $file_type = 1;

            // Getting the base name of the file.
            $filename = basename($file_name);

            $temp_folder_path = TMEP_FOLDER_PATH;
            $file_path = $temp_folder_path . $filename;

            $write_status = file_put_contents($file_path, file_get_contents($old_file_url));

            if ($write_status) {
                $s3_main_file_url = upload_image_s3(
                    ['tmp_name' => $file_path, 'name' => $filename],
                    mime_content_type($file_path)
                );

                $news_media_data = [
                    'media_url' => $s3_main_file_url,
                    'media_thumb' => '',
                    'news_id' => $newsId,
                    'media_type' => 1,
                    'created_date' => $date_time,
                ];

                $this->Common_model->insert_single('ipac_news_media', $news_media_data);

                if ($file_path && file_exists($file_path)) {
                    unlink($file_path);
                }
            }
        }


        # for videos
        $video_url = explode(',', $news_video_url);

        # as only one video is allowed
        $new_video_url = $video_url[0];

        $file_extenstion = explode('.', $new_video_url);
        $file_extenstion = array_pop($file_extenstion);
        $file_extenstion = strtolower($file_extenstion);

        if (in_array($file_extenstion, ['mp4', 'mov'])) {
            $file_type = 2;

            $news_media_data = [
                'media_url' => $new_video_url,
                'media_thumb' => '',
                'news_id' => $newsId,
                'media_type' => 2,
                'created_date' => $date_time,
            ];

            $this->Common_model->insert_single('ipac_news_media', $news_media_data);
        }
    }
    /**
     * @function download
     * @description To export news search list
     *
     * @param type $newsData
     */
    public function download($subscribers)
    {
        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('News List')
            ->setSubject('News List')
            ->setDescription('News List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'H') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'Title')
            ->setCellValue("C1", 'Description')
            ->setCellValue("D1", 'Add News For')
            ->setCellValue("E1", 'Added On')
            ->setCellValue("F1", 'Priority')
            ->setCellValue("G1", 'Type')
            ->setCellValue("H1", 'Likes Count')
            ->setCellValue("I1", 'Shares Count')
            ->setCellValue("J1", 'Shares Count')
            ->setCellValue("K1", 'Status');

        // Add some data
        $x = 2;
        $y = 1;
        $cattegoryArray = array('1' => 'Gallery', '2' => 'Article', '3' => 'News', '4' => 'Notification', '5' => 'Banner', '6' => 'Form', '7' => 'Party News', '8' => 'Murasoli');

        $status = array('1' => 'Active', '2' => 'Inactive','0' => 'Inactive','3' => 'Schedule');
        $priority = array('1' => 'High Priority', '2' => 'Low Priority');
        foreach ($subscribers as $res) {
            if (isset($res['state_name']) && !empty($res['state_name'])) {
                $fld_4 = $res['state_name'];
            } elseif (isset($res['district_name']) && !empty($res['district_name'])) {
                $fld_4 = $res['district_name'];
            } elseif (isset($res['registeration_no']) && !empty($res['registeration_no'])) {
                $fld_4 = $res['registeration_no'];
            } elseif (isset($res['gender']) && !empty($res['gender'])) {
                $fld_4 = ($res['gender'] == MALE_GENDER ? 'Male' : 'Female');
            } else {
                $fld_4 = 'All(Tamilnadu)';
            }

            if(array_key_exists($res['news_category'], $cattegoryArray)){
                $category_type          = $cattegoryArray[$res['news_category']];
            }else{
                $category_type          = '';
            }

            $date = date_create($res['created_date']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $y)
                ->setCellValue("B$x", $res['news_title'])
                ->setCellValue("C$x", strip_tags($res['news_description']))
                ->setCellValue("D$x", $fld_4)
                ->setCellValue("E$x", $Date . ' ' . $Time)
                ->setCellValue("F$x", $priority[$res['priority']])
                ->setCellValue("G$x", $category_type)
                ->setCellValue("H$x", $res['likes_count'])
                ->setCellValue("I$x", $res['share_count'])
                ->setCellValue("J$x", $res['comments_count'])
                ->setCellValue("K$x", $status[$res['status']]);
            $x++;
            $y++;
        }



        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('News List');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="news_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }

    /**
     * Add news in database
     *
     * @Function addNews
     * @Description add news
     * @Return view page
     */
    public function addNews()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
            $data['admininfo'] = $this->admininfo;
            $postdata = $this->input->post();
            $this->load->library('Common_function');
            if (isset($postdata) && !empty($postdata)) {
                // setting Form  validation Rules load from config file
                $this->form_validation->set_rules($this->config->item("add_news"));

                //if validation is true
                if ($this->form_validation->run() == true) {
                    $is_send_notif = $this->input->post('is_send_notif');
                    $status = $this->input->post('status');
                    $insertNewsArray = array(
                        'news_title' => $this->input->post('title'),
                        'news_title_tn' => $this->input->post('title_tn'),
                        'news_description' => !empty($this->input->post('desc')) ? $this->input->post('desc') : '',
                        'news_description_tn' => !empty($this->input->post('desc_tn')) ? $this->input->post('desc_tn') : '',
                        'notif_title' => !empty($this->input->post('notif_title')) ? $this->input->post('notif_title') : '',
                        'notif_description' => !empty($this->input->post('notif_desc')) ? $this->input->post('notif_desc') : '',
                        'greeting_message' => !empty($this->input->post('greeting')) ? $this->input->post('greeting') : '',
                        'is_send_notif' => isset($is_send_notif) ? 1 : 0,
                        'status' => $status,
                        'start_date' => !empty($this->input->post('startDate')) ? date('Y-m-d H:i:s', strtotime($this->input->post('startDate'))) : '',
                        'state' => !empty($this->input->post('state')) ? $this->input->post('state') : 0,
                        'district' => !empty($this->input->post('distict')) ? $this->input->post('distict') : 0,
                        'registeration_no' => !empty($this->input->post('uid')) ? $this->input->post('uid') : 0,
                        'college' => !empty($this->input->post('college_name')) ? $this->input->post('college_name') : 0,
                        'gender' => !empty($this->input->post('gender')) ? $this->input->post('gender') : 0,
                        'archive' => !empty($this->input->post('archive')) ? $this->input->post('archive') : 0,
                        'priority' => !empty($this->input->post('priority')) ? $this->input->post('priority') : 0,
                        'url' => !empty($this->input->post('url')) ? $this->input->post('url') : '',
                        'home_section' => !empty($this->input->post('section')) ? $this->input->post('section') : '',
                        'news_category' => !empty($this->input->post('type')) ? $this->input->post('type') : '',
                        'banner_height' => !empty($this->input->post('bannerPercentage')) ? $this->input->post('bannerPercentage') : '',
                        'created_date' => date('Y-m-d H:i:s'),
                        'updated_date' => date('Y-m-d H:i:s'),
                        'form_id' => !empty($this->input->post('onlineform')) ? $this->input->post('onlineform') : 0,
                        'priority_end_date' => !empty($this->input->post('priotityEndDate')) ? convert_date_time_format($this->input->post('priotityEndDate')) : '',
                        'is_event' => $this->input->post('is_event'),
                        'latitude' => !empty($this->input->post('latitude')) ? $this->input->post('latitude') : '0',
                        'longitude' => !empty($this->input->post('longitude')) ? $this->input->post('longitude') : '0',
                              
                    );
                    $this->db->trans_begin(); #DB transaction Start
                    //insert news details
                    $newsId = $this->Common_model->insert_single('ipac_news', $insertNewsArray);
                    
                    /* Check image or video is set or not */
                    if (isset($_FILES['news_image']['name']) && !empty($_FILES['news_image']['name'])) {
                        /* Preparing media array for batch insert */
                        $files = $_FILES;
                        $imgarr = array();
                        $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                        $datacount = count($_FILES['news_image']['name']) == 1 ? (count($_FILES['news_image']['name'])) + 1 : count($_FILES['news_image']['name']);

                        for ($i = 0; $i < $datacount - 1; $i++) {
                            $filename = $_FILES['news_image']['name'][$i];
                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            if (in_array($ext, $allowed)) {
                                //image check is it is set or not
                                if (!empty($_FILES['news_image']['name'][$i])) {
                                    //image insert array
                                    $imgarr['news_image']['name'] = $files['news_image']['name'][$i];
                                    $imgarr['news_image']['type'] = $files['news_image']['type'][$i];
                                    $imgarr['news_image']['tmp_name'] = $files['news_image']['tmp_name'][$i];
                                    $imgarr['news_image']['error'] = $files['news_image']['error'][$i];
                                    $imgarr['news_image']['size'] = $files['news_image']['size'][$i];

                                    //upload in s3 server
                                    $news_image_url = upload_image_s3($imgarr['news_image'], '');

                                    $type = explode('/', $files['news_image']['type'][$i]);
                                    $mediatype = IMAGE;
                                    //check type
                                    if ($type == 'image') {
                                        $mediatype = IMAGE;
                                    }
                                    /**Image thumb block start*/
                                    $tempName = $_FILES['news_image']['tmp_name'][0];
                                    $imageName = $files['news_image']['name'][0];
                                    $uploads = UPLOAD_IMAGE_PATH;
                                    $pathComplete = $uploads;
                                    $isSuccess = $this->common_function->uploadImg($tempName, $pathComplete, $imageName);
                                    //$thumb_url = '';

                                    if ($isSuccess) {
                                        $imgPath = UPLOAD_IMAGE_PATH . $isSuccess;
                                        //image thumb create
                                        $flag = $this->common_function->thumb_create($isSuccess, $imgPath, UPLOAD_THUMB_IMAGE_PATH);

                                        $thumbarr = array();
                                        $thumbarr['name'] = $isSuccess;
                                        $thumbarr['tmp_name'] = UPLOAD_THUMB_IMAGE_PATH . $isSuccess;

                                        $thumb_url = upload_image_s3($thumbarr, '');
                                    }
                                    /**Image thumb block end*/

                                    //Upload media in s3 server
                                    $insertNewsImage[$i] = array(
                                        'media_url' => $news_image_url,
                                        'media_thumb' => $thumb_url,
                                        'media_type' => $mediatype,
                                        'news_id' => $newsId,
                                        'created_date' => date('Y-m-d H:i:s')
                                    );
                                    //array to insert in gallery section
                                    $insertGalleryMedia[$i] = array(
                                        'gallary_media_url' => $news_image_url,
                                        'media_thumb' => $thumb_url,
                                        'media_type' => $mediatype,
                                        'type' => HOME_SECTION,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                } //end for loop on new images
                            } //end image extension check
                        }
                    }

                    /**Check if video is set or not  */
                    if ($_FILES && !empty($_FILES['file']['name']) && isset($_FILES['file']['name'])) {
                        /**Video upload block start */
                        $i = 0;
                        //video insert array
                        $vidarr = array();
                        if (!empty($_FILES['file']['name'][$i]) && isset($_FILES['file']['name'])) {
                            $allowed = array('m4v', 'avi', 'mpg', 'mp4', 'webm', 'MOV');
                            $filename = $_FILES['file']['name'][$i];
                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            if (in_array($ext, $allowed)) {
                                //preparing array for insert
                                $vidarr['file']['name'] = $_FILES['file']['name'][$i];
                                $vidarr['file']['type'] = $_FILES['file']['type'][$i];
                                $vidarr['file']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                                $vidarr['file']['error'] = $_FILES['file']['error'][$i];
                                $vidarr['file']['size'] = $_FILES['file']['size'][$i];

                                $type = explode('/', $vidarr['file']['type'][$i]);

                                $mediatype = VIDEO;
                                //check media type
                                if ($type == 'video') {
                                    $mediatype = VIDEO;
                                }
                                //$index = count($insertNewsImage);
                                #30-08-18
                                $index = isset($insertNewsImage) ? count($insertNewsImage) : 0;
                                //create image thumb
                                $imageThumb = create_video_thumb($_FILES['file']['tmp_name']);
                                $thumburl = s3_video_thumb_uploader($imageThumb, '');

                                //Upload media in s3 server
                                //insert array
                                $insertNewsImage[$index] = array(
                                    'media_url' => upload_image_s3($vidarr['file'], ''),
                                    'media_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                                    'media_type' => $mediatype,
                                    'news_id' => $newsId,
                                    'created_date' => date('Y-m-d H:i:s')
                                );
                                //gallery section array
                                $insertGalleryMedia[$index] = array(
                                    'gallary_media_url' => upload_image_s3($vidarr['file'], ''),
                                    'media_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                                    'media_type' => $mediatype,
                                    'type' => HOME_SECTION,
                                    'inserted_on' => date('Y-m-d H:i:s')
                                );
                                //unlink video thunb url
                                unlink($imageThumb);
                            }
                        }
                    }

                    /* Check PDF is set or not */
                    if (isset($_FILES['attachment']['name']) && !empty($_FILES['attachment']['name'])) {
                        $document_arr = array();
                        $allowed = array('pdf', 'PDF');
                        $filename = $_FILES['attachment']['name'];

                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if (in_array($ext, $allowed)) {
                            if (!empty($_FILES['attachment']['name'])) {
                                $document_arr['attachment']['name'] = $_FILES['attachment']['name'];
                                $document_arr['attachment']['type'] = $_FILES['attachment']['type'];
                                $document_arr['attachment']['tmp_name'] = $_FILES['attachment']['tmp_name'];
                                $document_arr['attachment']['error'] = $_FILES['attachment']['error'];
                                $document_arr['attachment']['size'] = $_FILES['attachment']['size'];

                                //upload in s3 server
                                $document_image_url = upload_image_s3($document_arr['attachment'], '');

                                //Upload media in s3 server
                                $insertDocumentUpload[0] = array(
                                    'media_url' => $document_image_url,
                                    'media_thumb' => '',
                                    'media_type' => FILE,
                                    'news_id' => $newsId,
                                    'created_date' => date('Y-m-d H:i:s')
                                );

                                $this->Common_model->insert_batch('ipac_news_media', array(), $insertDocumentUpload, '');
                            }
                        }
                    }
                    /** END OF PDF upload */

                    /**Check if video is set or not  */
                    if (!empty($postdata['banner_image'])) {
                        //insert array
                        $insertNewsImage[$index] = array(
                            'media_url' => $postdata['banner_image'],
                            'media_thumb' => $postdata['banner_image'],
                            'media_type' => IMAGE,
                            'news_id' => $newsId,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        //unlink video thunb url
                    }

                    //image array check
                    if (isset($insertNewsImage) && !empty($insertNewsImage)) {
                        //insert in image table
                        $this->Common_model->insert_batch('ipac_news_media', array(), $insertNewsImage, '');
                    }
                    //insert in gallery table
                    if (isset($insertGalleryMedia) && !empty($insertGalleryMedia)) {
                        //insert in image table
                        $this->Common_model->insert_batch('ipac_gallary_media', array(), $insertGalleryMedia, '');
                    }
                    #if transaction runs successfully
                    if (true === $this->db->trans_status()) {
                        $this->db->trans_commit();
                        // print_r($newsId);
                        // exit;

                        // if ($postdata['type'] != BANNER && isset($is_send_notif) && $status == 1) {
                        //     $this->load->library('common_function');
                        //     $news_title = !empty($this->input->post('notif_title')) ? $this->input->post('notif_title') : $this->input->post('title') ;
                        //     $news_description = !empty($this->input->post('notif_desc')) ? $this->input->post('notif_desc') : ' ';
                        //     $greeting_message = !empty($this->input->post('greeting')) ? $this->input->post('greeting') : $this->input->post('greeting') ;
                        //     $payload = [
                        //         "title" => $news_title,
                        //         "greeting_message" => $greeting_message,
                        //         "message" => $news_description,
                        //         "body" => $news_title,
                        //         "type" => 2,
                        //         "image" => '',
                        //         'news_id' => $newsId,
                        //         'task_id' => '',
                        //         'notification_id' => ''
                        //     ];
                        //    $topic = ENV == 'production' ? '/topics/prod_news' : '/topics/stag_news';
                        //     $this->common_function->androidTopicPush($topic, $payload);

                        //     $iosTopic = ENV == 'production' ? '/topics/ios_prod_news' : '/topics/ios_stag_news';
                        //     $this->common_function->iOSTopicPush($iosTopic, $payload);
                        // }

                        $this->session->set_flashdata('message_success', $this->lang->line('news_success'));
                        redirect(base_url() . 'admin/news');
                    } else { #IF transaction failed
                        #rolling back
                        $this->db->trans_rollback();
                    }
                }
            } //IF 1 END

            $data["csrfName"] = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            $data['formlist'] = get_form_list();
            //District list for goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);

            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("news/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    /**
     * Update news data in database
     *
     * @Function editNews
     * @Description update news
     * @Return view page
     */
    public function editNews()
    {
        try {
            $post = $this->input->post();
            if (isset($post) && !empty($post)) { //PARENT IF START
                //news id
                $newsId = (isset($post['token']) && !empty($post['token'])) ? encryptDecrypt($post['token'], 'decrypt') : '';
                /**News Update block start */
                //news update array
                $newsUpdateArr = [];
                // preparing array for update
                $newsUpdateArr = array(
                    'news_title' => $this->input->post('title'),
                    'news_title_tn' => $this->input->post('title_tn'),
                    'news_description' => !empty($this->input->post('desc')) ? $this->input->post('desc') : '',
                    'news_description_tn' => !empty($this->input->post('desc_tn')) ? $this->input->post('desc_tn') : '',
                    'status' => $post['status'],
                    'state' => !empty($this->input->post('state')) ? $this->input->post('state') : 0,
                    'district' => !empty($this->input->post('distict')) ? $this->input->post('distict') : 0,
                    'registeration_no' => !empty($this->input->post('uid')) ? $this->input->post('uid') : 0,
                    'college' => !empty($this->input->post('college_name')) ? $this->input->post('college_name') : 0,
                    'gender' => !empty($this->input->post('gender')) ? $this->input->post('gender') : 0,
                    'updated_date' => date('Y-m-d H:i:s'),
                    'archive' => !empty($this->input->post('archive')) ? $this->input->post('archive') : 0,
                    'priority' => !empty($this->input->post('priority')) ? $this->input->post('priority') : 0,
                    'url' => !empty($this->input->post('url')) ? $this->input->post('url') : '',
                    'home_section' => !empty($this->input->post('section')) ? $this->input->post('section') : '',
                    'news_category' => !empty($this->input->post('type')) ? $this->input->post('type') : '',
                    'priority_end_date' => !empty($this->input->post('priotityEndDate')) ? convert_date_time_format($this->input->post('priotityEndDate')) : '',

                );
                if (!empty($this->input->post('priority')) && $this->input->post('priority') == HIGH_PRIORITY) {
                    $newsUpdateArr['pin_date'] = date('Y-m-d H:i:s');
                }

                //where condition for update news
                $whereArr = [];
                $whereArr['where'] = array('news_id' => $newsId);
                //update news data
                $isSuccess = $this->Common_model->update_single('ipac_news', $newsUpdateArr, $whereArr);
                $this->load->helper(array('s3', 'video_thumb'));
                $this->load->library('common_function');

                if (isset($post['deleteImgId']) && !empty($post['deleteImgId'])) {
                    $whereDelArr = [];
                    $whereDelArr['where_in'] = ['media_id' => explode(',', $post['deleteImgId']), 'media_type' => IMAGE];
                    $updateId = $this->Common_model->delete_data('ipac_news_media', $whereDelArr);
                }
                /* Check image or video is set or not */
                if ($_FILES && !empty($_FILES['news_image']['name']) && isset($_FILES['news_image']['name'])) {
                    /* Preparing media array for batch insert */
                    $files = $_FILES;

                    $imgarr = array();
                    $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                    for ($i = 0; $i < count($_FILES['news_image']['name']) - 1; $i++) {
                        $filename = $_FILES['news_image']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if (in_array($ext, $allowed)) {
                            //image check is it is set or not
                            if (!empty($_FILES['news_image']['name'][$i])) {
                                //image insert array
                                $imgarr['news_image']['name'] = $files['news_image']['name'][$i];
                                $imgarr['news_image']['type'] = $files['news_image']['type'][$i];
                                $imgarr['news_image']['tmp_name'] = $files['news_image']['tmp_name'][$i];
                                $imgarr['news_image']['error'] = $files['news_image']['error'][$i];
                                $imgarr['news_image']['size'] = $files['news_image']['size'][$i];

                                //upload in s3 server
                                $news_image_url = upload_image_s3($imgarr['news_image'], '');

                                $type = explode('/', $files['news_image']['type'][$i]);
                                $mediatype = IMAGE;
                                //check type
                                if ($type == 'image') {
                                    $mediatype = IMAGE;
                                }
                                /**Image thumb block start*/
                                $tempName = $_FILES['news_image']['tmp_name'][0];
                                $imageName = $files['news_image']['name'][0];
                                $uploads = UPLOAD_IMAGE_PATH;
                                $pathComplete = $uploads;
                                $isSuccess = $this->common_function->uploadImg($tempName, $pathComplete, $imageName);
                                $thumb_url = '';

                                if ($isSuccess) {
                                    $imgPath = UPLOAD_IMAGE_PATH . $isSuccess;
                                    //image thumb create
                                    $flag = $this->common_function->thumb_create($isSuccess, $imgPath, UPLOAD_THUMB_IMAGE_PATH);
                                    $thumbarr = array();
                                    $thumbarr['name'] = $isSuccess;
                                    $thumbarr['tmp_name'] = UPLOAD_THUMB_IMAGE_PATH . $isSuccess;

                                    $thumb_url = upload_image_s3($thumbarr, '');
                                }
                                /**Image thumb block end*/

                                //Upload media in s3 server
                                $insertNewsImage[$i] = array(
                                    'media_url' => $news_image_url,
                                    'media_thumb' => $thumb_url,
                                    'media_type' => $mediatype,
                                    'news_id' => $newsId,
                                    'created_date' => date('Y-m-d H:i:s')
                                );

                                //array to insert in gallery section
                                $insertGalleryMedia[$i] = array(
                                    'gallary_media_url' => $news_image_url,
                                    'media_thumb' => $thumb_url,
                                    'media_type' => $mediatype,
                                    'type' => HOME_SECTION,
                                    'inserted_on' => date('Y-m-d H:i:s')
                                );
                            }
                        }
                    }
                }
                /**Check if vide is set or not  */
                if ($_FILES && !empty($_FILES['file']['name']) && isset($_FILES['file']['name'])) {
                    /**Video upload block start */
                    $i = 0;
                    //video insert array
                    $vidarr = array();
                    $allowed = array('m4v', 'avi', 'mpg', 'mp4', 'webm');
                    if (!empty($_FILES['file']['name'][$i]) && isset($_FILES['file']['name'])) {
                        $allowed = array('m4v', 'avi', 'mpg', 'mp4', 'webm');
                        $filename = $_FILES['file']['name'][$i];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        if (in_array($ext, $allowed)) {
                            /**Delete previous  */
                            $whereDelArr = [];
                            $whereDelArr['where_in'] = ['news_id' => $newsId, 'media_type' => VIDEO];
                            $updateId = $this->Common_model->delete_data('ipac_news_media', $whereDelArr);
                            //preparing array for insert
                            $vidarr['file']['name'] = $_FILES['file']['name'][$i];
                            $vidarr['file']['type'] = $_FILES['file']['type'][$i];
                            $vidarr['file']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                            $vidarr['file']['error'] = $_FILES['file']['error'][$i];
                            $vidarr['file']['size'] = $_FILES['file']['size'][$i];

                            $type = explode('/', $vidarr['file']['type'][$i]);

                            $mediatype = VIDEO;
                            //check media type
                            if ($type == 'video') {
                                $mediatype = VIDEO;
                            }
                            $index = count($insertNewsImage);
                            //create image thumb
                            $imageThumb = create_video_thumb($_FILES['file']['tmp_name']);
                            //Upload media in s3 server
                            //insert array
                            $insertNewsImage[$index] = array(
                                'media_url' => upload_image_s3($vidarr['file'], ''),
                                'media_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                                'media_type' => $mediatype,
                                'news_id' => $newsId,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                            //gallery section array
                            $insertGalleryMedia[$index] = array(
                                'gallary_media_url' => upload_image_s3($vidarr['file'], ''),
                                'media_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                                'media_type' => $mediatype,
                                'type' => HOME_SECTION,
                                'inserted_on' => date('Y-m-d H:i:s')
                            );
                            //unlink video thunb url
                            unlink($imageThumb);
                        }
                    }
                }
                /**Check if vide is set or not  */
                if (!empty($postdata['banner_image'])) {
                    //insert array
                    $insertNewsImage[$index] = array(
                        'media_url' => $postdata['banner_image'],
                        'media_thumb' => $postdata['banner_image'],
                        'media_type' => IMAGE,
                        'news_id' => $newsId,
                        'created_date' => date('Y-m-d H:i:s')
                    );
                    //unlink video thunb url
                }
                //image array check
                if (isset($insertNewsImage) && !empty($insertNewsImage)) {
                    //insert in image table
                    $this->Common_model->insert_batch('ipac_news_media', array(), $insertNewsImage, '');
                }
                //insert in gallery table
                if (isset($insertGalleryMedia) && !empty($insertGalleryMedia)) {
                    //insert in image table
                    $this->Common_model->insert_batch('ipac_gallary_media', array(), $insertGalleryMedia, '');
                }

                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();
                    #setting Response Array
                    $this->session->set_flashdata('message_success', $this->lang->line('news_upd_success'));
                    redirect(base_url() . 'admin/news');
                } else { #IF transaction failed
                    #rolling back
                    $this->db->trans_rollback();

                    #setting Response Array
                }
                /**News Update block end*/
            } else {
                $get = $this->input->get();
                $newsId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_news'), "/admin/news/");
                $data = array();

                $data['news_id'] = $newsId;

                //get news detail data
                $data['newsDetail'] = $this->News_model->newsDetail($newsId);
                //get news image media data
                $data['newsImage'] = $this->Common_model->fetch_data('ipac_news_media', array(), ['where' => ['news_id' => $newsId, 'media_type' => IMAGE]]);
                //get news image media data
                $data['videoImage'] = $this->Common_model->fetch_data('ipac_news_media', array(), ['where' => ['news_id' => $newsId, 'media_type' => VIDEO]], true);

                if (empty($data['newsDetail'])) { //IF START
                    //  show404($this->lang->line('no_news'), "/admin/news/");
                    return 0;
                } //IF END
            }
        } catch (Exception $exception) {
        }
        //load state helper
        $this->load->helper('state');
        //state list
        $data['statelist'] = get_state_list();
        $data['formlist'] = get_form_list();
        //District list for goa
        $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
        //uid list
        $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
        $data['admininfo'] = $this->data['admininfo'];
        load_views("news/edit", $data);
    }
    /**
     *  get news detail
     *
     * @Function newsDetail
     * @Description get news detail
     * @Return view page
     */
    public function newsDetail()
    {
        try {
            $get = $this->input->get();

            $newsId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/news/");
            //$newsId = 13;
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['news_id'] = $newsId;

            //get user profile data
            $data['news'] = $this->News_model->newsDetail($newsId);
            $data['news_media'] = $this->Common_model->fetch_data('ipac_news_media', array(), ['where' => ['news_id' => $newsId]]);

            $pageurl = 'news/news_detail';
            $this->load->library('Common_function');
            $data['permission'] = $GLOBALS['permission'];
            $data['type'] = array('1' => 'Gallery', '2' => 'Article', '3' => 'News', '4' => 'Notification', '5' => 'Banner', '6' => 'Form', '7' => 'Party News', '8' => 'Murasoli', '9' => 'Text', '10' => 'Image', '11' => 'Video');
            $data['gender_type'] = unserialize(GENDER_TYPE);

            if (empty($data['news'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/news/");
                return 0;
            } //IF END

            load_views("news/news_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
