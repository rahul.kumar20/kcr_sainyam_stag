<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
class Cron extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Cron_model');
        $this->load->model('Common_model');
        $this->lang->load('common', 'english');
        $this->load->helper('sendsms');

        date_default_timezone_set('Asia/Kolkata');
    }

    public function sendBirthdayNotification()
    {
        $sns = new Snspush();
        //get user was ern to send notification
        $userErn = $this->Cron_model->birthdayNotifUserErns();
        if (count($userErn) > 0) {
            //message arry for sending notification
            $message = [
                "default" => 'Happy Birthday',
                "APNS_SANDBOX" => json_encode([
                    "aps" => [
                        "alert" => array(
                            "title" => 'Happy Birthday',
                            "body" => 'Happy Birthday',
                        ),
                        "sound" => "default",
                        "mutable-content" => 1,
                        "badge" => 1,
                        "data" => array(
                            'notification_id' => '',
                            'news_id' => '',
                            "task_id" => '',
                            "content_type" => "image",
                            "type" => 10,
                        ),
                    ],
                ]),
                "GCM" => json_encode([
                    "data" => [
                        "title" => 'Happy Birthday',
                        "message" => 'Happy Birthday',
                        "body" => 'Happy Birthday',
                        "type" => 10,
                        "image" => '',
                        'news_id' => '',
                        'task_id' => '',
                        'notification_id' => ''
                    ],
                ]),
            ];

            foreach ($userErn as $users) {
                $insertNewsNoti = array(
                    'user_id' => $users['user_id'],
                    'news_id' => 0,
                    'task_id' => 0,
                    'notification_type' => 'birthday',
                    'notification_text' => 'Happy Birthday ' . $users['full_name'],
                    'inserted_on' => date('Y-m-d H:i:s'),
                );
                $updateId = $this->Common_model->insert_single('user_notification', $insertNewsNoti);

                if ($updateId > 0) {
                    $this->setNotificationReceived($users['user_id']);
                }
            }
            //send message to filtered users or selected users
            $result = $sns->asyncPublish2($userErn, $message);
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    // public function sendAchievementlevelone()
    // {
    //     $userErn = $this->Cron_model->userlevel(1, $this->input->get('limit'));
    //     if (count($userErn) > 0) {
    //         foreach ($userErn as $users) {
    //             $updateUserArray = array('notified_level_one' => '1');
    //             $this->Common_model->update_single(
    //                 'tbl_user_level',
    //                 $updateUserArray,
    //                 ['where' => ['pk_id' => $users['pk_id']]]
    //             );
    //             if ($users['email_id'] != '') {
    //                 $mailData               = [];
    //                 $mailData['name']       =  $users['full_name'];
    //                 $mailData['email']      =  $users['email_id'];
    //                 $subject                = "உங்களின் முதல் வெற்றிக்கு வாழ்த்துகள்!";
    //                 $template               = 'achievement_level_one';

    //                 sendmail($subject, $mailData, $template);
    //             }
    //         }
    //     }
    //     echo json_encode(array('status' => 'success'));
    //     exit;
    // }

    // public function sendAchievementlevelone()
    // {
    //     $userErn = $this->Cron_model->userlevel(1, $this->input->get('limit'));
    //    if (count($userErn) > 0) {
    //         foreach ($userErn as $users) {
    //             if ($users['email_id'] != '') {
    //                 $mailData               = [];
    //                 $mailData['name']       =  $users['full_name'];
    //                 $mailData['email']      =  $users['email_id'];

    //                 $inputKeys              = $users['registeration_no'] . '@' . $users['user_id'] . '@1';
    //                 $inputKeys              = encrypt_decrypt('encrypt', $inputKeys);
    //                 $mailData['link']       = base_url() . 'levelone_certificate/?key=' . $inputKeys;
    //                 $subject                = "வினையூக்கி நிலையைக் கடந்ததற்கு ஸ்டாலின் அணியின் வாழ்த்துக்கள்!";
    //                 $template               = 'achievement_level_one';
                    
    //                 $sendMail               = sendMail($subject, $mailData, $template);
    //                 if ($sendMail) {
    //                     $updateUserArray    = array(
    //                         'notified'                => '1',
    //                         'notified_date'           => date('Y-m-d H:i:s')
    //                     );
    //                     $this->Common_model->update_single(
    //                         'tbl_user_level',
    //                         $updateUserArray,
    //                         ['where' => ['pk_id' => $users['pk_id']]]
    //                     );
    //                 }
    //             }
    //         }
    //     }
    //     echo json_encode(array('status' => 'success'));
    //     exit;
    // }

    public function sendAchievementMail()
    {
        $userErn = $this->Cron_model->userlevel(4);
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                $updateUserArray = array('notified' => '1');
                $this->Common_model->update_single(
                    'tbl_user_level',
                    $updateUserArray,
                    ['where' => ['pk_id' => $users['pk_id']]]
                );
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $mailData['mailerName'] = 'achievement';
                    $subject                = "ஸ்டாலின் அணி செயலியின் 'மலர்' நிலையை கடந்ததற்கான பரிசு";
                    $mailerName             = 'achievement';
                    $attachment             =  $_SERVER["DOCUMENT_ROOT"] . '/public/images/Badge.png';
                    sendmailwithattachment($subject, $mailData, $mailerName, $attachment);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendgrievanceAutoReply()
    {
        $userErn = $this->Cron_model->getGrievanceUser('grievance');
        if (count($userErn) > 0) {
            $grienvanceID = ['38', '39'];
            foreach ($userErn as $users) {
                $updateUserArray = array('is_automail_sent' => '1');
                $this->Common_model->update_single(
                    'ic_chat_response',
                    $updateUserArray,
                    ['where' => ['user_id' => $users['user_id']], 'where_in' => ['question_id' => $grienvanceID]]
                );
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $subject                = "Message from M.K Stalin's office";
                    $template               = 'grievance';
                    sendmail($subject, $mailData, $template);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendfeedbackAutoReply()
    {
        $userErn = $this->Cron_model->getGrievanceUser('feedback');
        if (count($userErn) > 0) {
            $grienvanceID = ['40', '41', '42', '43', '45'];
            foreach ($userErn as $users) {
                $updateUserArray = array('is_automail_sent' => '1');
                $this->Common_model->update_single(
                    'ic_chat_response',
                    $updateUserArray,
                    ['where' => ['user_id' => $users['user_id']], 'where_in' => ['question_id' => $grienvanceID]]
                );
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $subject                = "Message from M.K Stalin's office";
                    $template               = 'feedback';
                    sendmail($subject, $mailData, $template);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendnewsNotification()
    {
        $total_Records = 0;
        //get user ern to send notification
        $news = $this->Cron_model->getNews();
        if (count($news) > 0) {
            foreach ($news as $n) {
                $news_id = $n['news_id'];
                $news_title = $n['news_title'];
                $news_type = $n['news_category'];
                $param['registeration_no'] = $n['registeration_no'];
                $param['district'] = $n['district'];
                $param['college'] = $n['college'];
                $param['priority'] = $n['priority'];
                $param['gender'] = $n['gender'];

                $homeContentType = unserialize(HOME_CONTENT_TYPE);
                if ($news_type != BANNER) {
                    $userErn = $this->Cron_model->newsUserErns($param, $news_id);

                    //send notification
                    $total_Records = count($userErn);
                    if ($total_Records > 0) {
                        $sendNotifiUser = [];
                        $in = 0;
                        foreach ($userErn as $users) {
                            $insertNewsNoti[$in] = array(
                                'user_id' => $users['user_id'],
                                'news_id' => $news_id,
                                'task_id' => 0,
                                'notification_type' => $homeContentType[$news_type],
                                'notification_text' => $news_title,
                                'inserted_on' => date('Y-m-d H:i:s')
                            );
                            $in++;
                            $sendNotifiUser = $insertNewsNoti;
                        }
                        $temp = array_unique(array_column($sendNotifiUser, 'user_id'));
                        $unique_arr = array_intersect_key($sendNotifiUser, $temp);
                        $insertStatus = $this->Common_model->insert_batch('user_notification', array(), $unique_arr, '');

                        foreach ($unique_arr as $val) {
                            if ($val['user_id'] > 0) {
                                $this->setNotificationReceived($val['user_id']);
                            }
                        }
                    } else {
                        $newsUpdateArr = array('notification_update' => 1);
                        $whereArr = [];
                        $whereArr['where'] = array('news_id' => $news_id);
                        $this->Common_model->update_single('ipac_news', $newsUpdateArr, $whereArr);
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success', 'total_notification_sent' => $total_Records));
        exit;
    }

    public function sendtasksNotification()
    {
        $total_Records = 0;
        $tasks = $this->Cron_model->getTasks();
        if (count($tasks) > 0) {
            foreach ($tasks as $n) {
                $task_id = $n['task_id'];
                $task_title = $n['task_title'];
                $task_type = $n['task_type'];
                $param['registeration_no'] = $n['registeration_no'];
                $param['district'] = $n['district'];
                $param['ac'] = $n['ac'];
                $param['college'] = $n['college'];
                $param['gender'] = $n['gender'];

                $userErn = $this->Cron_model->tasksUserErns($param, $task_id);
                //send notification
                $total_Records = count($userErn);
                if ($total_Records > 0) {
                    $sendNotifiUser = [];
                    $in = 0;
                    foreach ($userErn as $users) {
                        $insertNewsNoti[$in] = array(
                            'user_id' => $users['user_id'],
                            'news_id' => 0,
                            'task_id' => $task_id,
                            'notification_type' => ADD_TASK_NOTIFICATION,
                            'notification_text' => 'New ' . ucfirst($task_type) . ' task added ' . $task_title,
                            'inserted_on' => date('Y-m-d H:i:s')
                        );
                        $in++;
                        $sendNotifiUser = $insertNewsNoti;
                    }
                    $temp = array_unique(array_column($sendNotifiUser, 'user_id'));
                    $unique_arr = array_intersect_key($sendNotifiUser, $temp);
                    $insertStatus = $this->Common_model->insert_batch('user_notification', array(), $unique_arr, '');

                    foreach ($unique_arr as $val) {
                        if ($val['user_id'] > 0) {
                            $this->setNotificationReceived($val['user_id']);
                        }
                    }
                } else {
                    $newsUpdateArr = array('notification_update' => 1);
                    $whereArr['where'] = array('task_id' => $task_id);
                    $this->Common_model->update_single('ipac_task_master', $newsUpdateArr, $whereArr);
                }
            }
        }
        echo json_encode(array('status' => 'success', 'total_notification_sent' => $total_Records));
        exit;
    }

    public function sendScheduledTasksNotification()
    {
        $tasks = $this->Cron_model->getscheduledTasks();
        // print_r($tasks);exit;
        $this->load->library('common_function');
        if (count($tasks) > 0) {
            foreach ($tasks as $n) {
                $task_id = $n['task_id'];
                $task_title = $n['task_title'];
                $task_type = $n['task_type'];
                $greeting_message = $n['greeting_message'];
                $action = $n['action'];
                $show_popup = $n['show_popup'];

                $newsUpdateArr = array('is_push_notif_sent' => 1);
                $whereArr['where'] = array('task_id' => $task_id);
                $this->Common_model->update_single('ipac_task_master', $newsUpdateArr, $whereArr);
                switch ($task_type) {
                    case 'facebook': //Facebook Message
                        $taskTitle = 'Participate in the progress of future generations! - (Facebook Task)';
                        break;
                    case 'twitter': //Twitter Message
                        $taskTitle = 'Participate in the great political change! (Twitter Task)';
                        break;
                    case 'whatsapp': // Whatsapp Message
                        $taskTitle = 'Take part in the journey towards change…! Watch Task)';
                        break;
                    case 'youtube': // Youtube Message
                        $taskTitle = 'One click ... change ... for the first of the people! (YouTube Task)';
                        break;
                    default:
                        $taskTitle = 'New ' . ucfirst($task_type) . ' task added ' . $task_title;
                }
                $taskTitle = !empty($n['notif_title']) ? $n['notif_title'] : $taskTitle;
                $taskdescription = !empty($n['notif_description']) ? $n['notif_description'] : $task_title;

                $insertNewsArray = array(
                    'task_title' => $n['task_title'],
                    'task_description' => $n['task_description'],
                    'post_url' => $n['post_url'],
                    'post_content' => $n['post_content'],
                    'points' => $n['points'],
                    'start_date' => $n['start_date'],
                    'end_date' => $n['end_date'],
                    'state' => $n['state'],
                    'district' => $n['district'],
                    'gender' => $n['gender'],
                    'registeration_no' => $n['registeration_no'],
                    'task_type' => $n['task_type'],
                    'task_status' => $n['task_status'],
                    'college' => $n['college'],
                    'action' => $action,
                    'created_date' => $n['created_date'],
                    'latitude' => $n['latitude'],
                    'longitude' => $n['longitude'],
                    'radius' => $n['radius'],
                    'video_id' => $n['video_id'],
                    'channel_id' => $n['channel_id'],
                    'twitter_follow_id' => $n['twitter_follow_id'],
                    'twitter_follow_name' => $n['twitter_follow_name'],
                    'facebook_follow_id' => $n['facebook_follow_id'],
                    'facebook_follow_name' => $n['facebook_follow_name'],
                    'instruction_video_url' => $n['instruction_video_url'],
                    'instruction_description' => $n['instruction_description'],
                    'form_id' => $n['form_id'],
                    'form_steps' => $n['form_steps'],
                    'whatsapp_image_url1' => $n['whatsapp_image_url1'],
                    'whatsapp_image_url2' => $n['whatsapp_image_url2'],
                    'show_popup' => $n['show_popup'],
                    'qr_code_url' => $n['qr_code_url'],
                    'form_url' => $n['form_url']
                );

                $insertNewsArray['task_media_set'] =  $this->Cron_model->getTaskMedia($task_id);

                $payload = [
                    "title" => $taskTitle,
                    "greeting_message" => $greeting_message,
                    "message" => $taskdescription,
                    "type" => 3,
                    "image" => '',
                    'task_id' => $task_id,
                    'news_id' => '',
                    'notification_id' => '',
                    "show_popup" => $show_popup,
                    "task_details" => $insertNewsArray,
                    "body" => $task_title
                ];
                // print_r($payload);
                // exit;
                $topic = ENV == 'production' ? '/topics/prod_task' : '/topics/stag_task';
                // print_r($topic);
                // exit;
                $this->common_function->androidTopicPush($topic, $payload);

                $iosTopic = ENV == 'production' ? '/topics/ios_prod_task' : '/topics/ios_stag_task';
                $this->common_function->iOSTopicPush($iosTopic, $payload);
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendScheduledTasksNotiForRegisteredUsers()
    {
        $tasks = $this->Cron_model->getScheduledTasksHavingSpecificUsers();
        
        $this->load->library('common_function');
        if (count($tasks) > 0) {
            foreach ($tasks as $n) {
                $userErn = $this->Cron_model->getUsersByRegisterIds($n['registeration_no']);
                
                $task_id            = $n['task_id'];
                $task_title         = $n['task_title'];
                $task_type          = $n['task_type'];
                $greeting_message   = $n['greeting_message'];
                $action             = $n['action'];
                $show_popup         = $n['show_popup'];

                $newsUpdateArr = array('is_push_notif_sent' => 1);
                $whereArr['where'] = array('task_id' => $task_id);
                $this->Common_model->update_single('ipac_task_master', $newsUpdateArr, $whereArr);

                switch ($task_type) {
                    case 'facebook': //Facebook Message
                        $taskTitle = 'எதிர்கால தலைமுறையின் முன்னேற்றத்தில் பங்கெடுங்கள்…! - (பேஸ்புக் டாஸ்க்)';
                        break;
                    case 'twitter': //Twitter Message
                        $taskTitle = 'மாபெரும் அரசியல் மாற்றத்தில் பங்கெடுங்கள்…! (ட்விட்டர் டாஸ்க்)';
                        break;
                    case 'whatsapp': // Whatsapp Message
                        $taskTitle = 'மாற்றத்தை நோக்கிய பயணத்தில் பங்கெடுங்கள்…!வாட்சப் டாஸ்க்)';
                        break;
                    case 'youtube': // Youtube Message
                        $taskTitle = 'ஒரு க்ளிக்...மாற்றம்...மக்களின் முதல்வருக்காக…! (யூடுப் டாஸ்க்)';
                        break;
                    default:
                        $taskTitle = 'New ' . ucfirst($task_type) . ' task added ' . $task_title;
                }
                $taskTitle = !empty($n['notif_title']) ? $n['notif_title'] : $taskTitle;
                $taskdescription = !empty($n['notif_description']) ? $n['notif_description'] : $task_title;

                $insertNewsArray = array(
                    'task_title' => $n['task_title'],
                    'task_description' => $n['task_description'],
                    'post_url' => $n['post_url'],
                    'post_content' => $n['post_content'],
                    'points' => $n['points'],
                    'start_date' => $n['start_date'],
                    'end_date' => $n['end_date'],
                    'state' => $n['state'],
                    'district' => $n['district'],
                    'gender' => $n['gender'],
                    'registeration_no' => $n['registeration_no'],
                    'task_type' => $n['task_type'],
                    'task_status' => $n['task_status'],
                    'college' => $n['college'],
                    'action' => $action,
                    'created_date' => $n['created_date'],
                    'latitude' => $n['latitude'],
                    'longitude' => $n['longitude'],
                    'radius' => $n['radius'],
                    'video_id' => $n['video_id'],
                    'channel_id' => $n['channel_id'],
                    'twitter_follow_id' => $n['twitter_follow_id'],
                    'twitter_follow_name' => $n['twitter_follow_name'],
                    'facebook_follow_id' => $n['facebook_follow_id'],
                    'facebook_follow_name' => $n['facebook_follow_name'],
                    'instruction_video_url' => $n['instruction_video_url'],
                    'instruction_description' => $n['instruction_description'],
                    'form_id' => $n['form_id'],
                    'form_steps' => $n['form_steps'],
                    'whatsapp_image_url1' => $n['whatsapp_image_url1'],
                    'whatsapp_image_url2' => $n['whatsapp_image_url2'],
                    'show_popup' => $n['show_popup'],
                    'qr_code_url' => $n['qr_code_url'],
                    'form_url' => $n['form_url']
                );

                $insertNewsArray['task_media_set'] =  $this->Cron_model->getTaskMedia($task_id);

                $payload = [
                    "title" => $taskTitle,
                    "greeting_message" => $task_title,
                    "message" => $taskdescription,
                    "type" => 3,
                    "image" => '',
                    'task_id' => $task_id,
                    'news_id' => '',
                    'notification_id' => '',
                    "show_popup" => $show_popup,
                    "task_details" => $insertNewsArray,
                    "body" => $task_title
                ];

                //  send notification
                $total_Records = count($userErn);
                if ($total_Records > 0) {
                    $androidTokens = array();
                    $iosTokens = array();
                    foreach ($userErn as $users) {
                        if ($users['platform'] == 'android') {
                            $androidTokens[] = $users['device_token'];
                        }
                        if ($users['platform'] == 'ios') {
                            $iosTokens[] = $users['device_token'];
                        }
                    }

                    if ($androidTokens > 0) {
                        $tokens = array_chunk($androidTokens, 1000);
                        foreach ($tokens as $tok) {
                            if (count($tok) > 0) {
                                $this->common_function->androidPush($tok, $payload);
                            }
                        }
                    }

                    if ($iosTokens > 0) {
                        $tokens = array_chunk($iosTokens, 1000);
                        foreach ($tokens as $tok) {
                            if (count($tok) > 0) {
                                $this->common_function->iosPush($tok, $payload);
                            }
                        }
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    // send District wise notification
    public function sendScheduledDistrictTasksNotification()
    {
        $tasks = $this->Cron_model->getscheduledDistrictWiseTasks();
        $this->load->library('common_function');
        if (count($tasks) > 0) {
            foreach ($tasks as $n) {
                $userErn = $this->Cron_model->districtNotifUsers($n['district']);
                $task_id = $n['task_id'];
                $task_title = $n['task_title'];
                $task_type = $n['task_type'];
                $greeting_message = $n['greeting_message'];
                $action = $n['action'];
                $show_popup = $n['show_popup'];

                $newsUpdateArr = array('is_push_notif_sent' => 1);
                $whereArr['where'] = array('task_id' => $task_id);
                $this->Common_model->update_single('ipac_task_master', $newsUpdateArr, $whereArr);

                switch ($task_type) {
                    case 'facebook': //Facebook Message
                        $taskTitle = 'எதிர்கால தலைமுறையின் முன்னேற்றத்தில் பங்கெடுங்கள்…! - (பேஸ்புக் டாஸ்க்)';
                        break;
                    case 'twitter': //Twitter Message
                        $taskTitle = 'மாபெரும் அரசியல் மாற்றத்தில் பங்கெடுங்கள்…! (ட்விட்டர் டாஸ்க்)';
                        break;
                    case 'whatsapp': // Whatsapp Message
                        $taskTitle = 'மாற்றத்தை நோக்கிய பயணத்தில் பங்கெடுங்கள்…!வாட்சப் டாஸ்க்)';
                        break;
                    case 'youtube': // Youtube Message
                        $taskTitle = 'ஒரு க்ளிக்...மாற்றம்...மக்களின் முதல்வருக்காக…! (யூடுப் டாஸ்க்)';
                        break;
                    default:
                        $taskTitle = 'New ' . ucfirst($task_type) . ' task added ' . $task_title;
                }
                $taskTitle = !empty($n['notif_title']) ? $n['notif_title'] : $taskTitle;
                $taskdescription = !empty($n['notif_description']) ? $n['notif_description'] : $task_title;

                $insertNewsArray = array(
                    'task_title' => $n['task_title'],
                    'task_description' => $n['task_description'],
                    'post_url' => $n['post_url'],
                    'post_content' => $n['post_content'],
                    'points' => $n['points'],
                    'start_date' => $n['start_date'],
                    'end_date' => $n['end_date'],
                    'state' => $n['state'],
                    'district' => $n['district'],
                    'gender' => $n['gender'],
                    'registeration_no' => $n['registeration_no'],
                    'task_type' => $n['task_type'],
                    'task_status' => $n['task_status'],
                    'college' => $n['college'],
                    'action' => $action,
                    'created_date' => $n['created_date'],
                    'latitude' => $n['latitude'],
                    'longitude' => $n['longitude'],
                    'radius' => $n['radius'],
                    'video_id' => $n['video_id'],
                    'channel_id' => $n['channel_id'],
                    'twitter_follow_id' => $n['twitter_follow_id'],
                    'twitter_follow_name' => $n['twitter_follow_name'],
                    'facebook_follow_id' => $n['facebook_follow_id'],
                    'facebook_follow_name' => $n['facebook_follow_name'],
                    'instruction_video_url' => $n['instruction_video_url'],
                    'instruction_description' => $n['instruction_description'],
                    'form_id' => $n['form_id'],
                    'form_steps' => $n['form_steps'],
                    'whatsapp_image_url1' => $n['whatsapp_image_url1'],
                    'whatsapp_image_url2' => $n['whatsapp_image_url2'],
                    'show_popup' => $n['show_popup'],
                    'qr_code_url' => $n['qr_code_url'],
                    'form_url' => $n['form_url']
                );

                $insertNewsArray['task_media_set'] =  $this->Cron_model->getTaskMedia($task_id);

                $payload = [
                    "title" => $taskTitle,
                    "greeting_message" => $task_title,
                    "message" => $taskdescription,
                    "type" => 3,
                    "image" => '',
                    'task_id' => $task_id,
                    'news_id' => '',
                    'notification_id' => '',
                    "show_popup" => $show_popup,
                    "task_details" => $insertNewsArray,
                    "body" => $task_title
                ];

                //  send notification
                $total_Records = count($userErn);
                if ($total_Records > 0) {
                    $androidTokens = array();
                    $iosTokens = array();
                    foreach ($userErn as $users) {
                        if ($users['platform'] == 'android') {
                            $androidTokens[] = $users['device_token'];
                        }
                        if ($users['platform'] == 'ios') {
                            $iosTokens[] = $users['device_token'];
                        }
                    }

                    if ($androidTokens > 0) {
                        $tokens = array_chunk($androidTokens, 1000);
                        foreach ($tokens as $tok) {
                            if (count($tok) > 0) {
                                $this->common_function->androidPush($tok, $payload);
                            }
                        }
                    }

                    if ($iosTokens > 0) {
                        $tokens = array_chunk($iosTokens, 1000);
                        foreach ($tokens as $tok) {
                            if (count($tok) > 0) {
                                $this->common_function->iosPush($tok, $payload);
                            }
                        }
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }
    // end district wise notification here

    // send AC wise notification
    public function sendScheduledACTasksNotification()
    {
        $tasks = $this->Cron_model->getscheduledACWiseTasks();
        $this->load->library('common_function');
        if (count($tasks) > 0) {
            foreach ($tasks as $n) {
                $userErn = $this->Cron_model->acNotifUsers($n['ac']);
                $task_id = $n['task_id'];
                $task_title = $n['task_title'];
                $task_type = $n['task_type'];
                $greeting_message = $n['greeting_message'];
                $action = $n['action'];
                $show_popup = $n['show_popup'];

                $newsUpdateArr = array('is_push_notif_sent' => 1);
                $whereArr['where'] = array('task_id' => $task_id);
                $this->Common_model->update_single('ipac_task_master', $newsUpdateArr, $whereArr);

                switch ($task_type) {
                    case 'facebook': //Facebook Message
                        $taskTitle = 'எதிர்கால தலைமுறையின் முன்னேற்றத்தில் பங்கெடுங்கள்…! - (பேஸ்புக் டாஸ்க்)';
                        break;
                    case 'twitter': //Twitter Message
                        $taskTitle = 'மாபெரும் அரசியல் மாற்றத்தில் பங்கெடுங்கள்…! (ட்விட்டர் டாஸ்க்)';
                        break;
                    case 'whatsapp': // Whatsapp Message
                        $taskTitle = 'மாற்றத்தை நோக்கிய பயணத்தில் பங்கெடுங்கள்…!வாட்சப் டாஸ்க்)';
                        break;
                    case 'youtube': // Youtube Message
                        $taskTitle = 'ஒரு க்ளிக்...மாற்றம்...மக்களின் முதல்வருக்காக…! (யூடுப் டாஸ்க்)';
                        break;
                    default:
                        $taskTitle = 'New ' . ucfirst($task_type) . ' task added ' . $task_title;
                }
                $taskTitle = !empty($n['notif_title']) ? $n['notif_title'] : $taskTitle;
                $taskdescription = !empty($n['notif_description']) ? $n['notif_description'] : $task_title;

                $insertNewsArray = array(
                    'task_title' => $n['task_title'],
                    'task_description' => $n['task_description'],
                    'post_url' => $n['post_url'],
                    'post_content' => $n['post_content'],
                    'points' => $n['points'],
                    'start_date' => $n['start_date'],
                    'end_date' => $n['end_date'],
                    'state' => $n['state'],
                    'district' => $n['district'],
                    'gender' => $n['gender'],
                    'registeration_no' => $n['registeration_no'],
                    'task_type' => $n['task_type'],
                    'task_status' => $n['task_status'],
                    'college' => $n['college'],
                    'action' => $action,
                    'created_date' => $n['created_date'],
                    'latitude' => $n['latitude'],
                    'longitude' => $n['longitude'],
                    'radius' => $n['radius'],
                    'video_id' => $n['video_id'],
                    'channel_id' => $n['channel_id'],
                    'twitter_follow_id' => $n['twitter_follow_id'],
                    'twitter_follow_name' => $n['twitter_follow_name'],
                    'facebook_follow_id' => $n['facebook_follow_id'],
                    'facebook_follow_name' => $n['facebook_follow_name'],
                    'instruction_video_url' => $n['instruction_video_url'],
                    'instruction_description' => $n['instruction_description'],
                    'form_id' => $n['form_id'],
                    'form_steps' => $n['form_steps'],
                    'whatsapp_image_url1' => $n['whatsapp_image_url1'],
                    'whatsapp_image_url2' => $n['whatsapp_image_url2'],
                    'show_popup' => $n['show_popup'],
                    'qr_code_url' => $n['qr_code_url'],
                    'form_url' => $n['form_url']
                );

                $insertNewsArray['task_media_set'] =  $this->Cron_model->getTaskMedia($task_id);

                $payload = [
                    "title" => $taskTitle,
                    "greeting_message" => $task_title,
                    "message" => $taskdescription,
                    "type" => 3,
                    "image" => '',
                    'task_id' => $task_id,
                    'news_id' => '',
                    'notification_id' => '',
                    "show_popup" => $show_popup,
                    "task_details" => $insertNewsArray,
                    "body" => $task_title
                ];

                //  send notification
                $total_Records = count($userErn);
                if ($total_Records > 0) {
                    $androidTokens = array();
                    $iosTokens = array();
                    foreach ($userErn as $users) {
                        if ($users['platform'] == 'android') {
                            $androidTokens[] = $users['device_token'];
                        }
                        if ($users['platform'] == 'ios') {
                            $iosTokens[] = $users['device_token'];
                        }
                    }

                    if ($androidTokens > 0) {
                        $tokens = array_chunk($androidTokens, 1000);
                        foreach ($tokens as $tok) {
                            if (count($tok) > 0) {
                                $this->common_function->androidPush($tok, $payload);
                            }
                        }
                    }

                    if ($iosTokens > 0) {
                        $tokens = array_chunk($iosTokens, 1000);
                        foreach ($tokens as $tok) {
                            if (count($tok) > 0) {
                                $this->common_function->iosPush($tok, $payload);
                            }
                        }
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendScheduledNewsNotification()
    {
        $news = $this->Cron_model->getscheduledNews();
        $this->load->library('common_function');
        if (count($news) > 0) {
            foreach ($news as $n) {
                $news_id = $n['news_id'];
                $news_category = $n['news_category'];
                $is_send_notif = $n['is_send_notif'];
                $greeting_message = $n['greeting_message'];
                $news_title = !empty($n['notif_title']) ? $n['notif_title'] : $n['news_title'] . ' ' . 'See also ..';
                $news_description = !empty($n['notif_description']) ? $n['notif_description'] : ' ';
                $newsUpdateArr = array('status' => 1, 'created_date' => $n['start_date']);
                $whereArr['where'] = array('news_id' => $news_id);
                $this->Common_model->update_single('ipac_news', $newsUpdateArr, $whereArr);
                if ($news_category != BANNER && $is_send_notif == 1) {
                    $payload = [
                        "title" => $news_title,
                        "greeting_message" => $greeting_message,
                        "message" => $news_description,
                        "body" => $news_title,
                        "type" => 2,
                        "image" => '',
                        'news_id' => $news_id,
                        'task_id' => '',
                        'notification_id' => ''
                    ];

                    $topic = ENV == 'production' ? '/topics/prod_news' : '/topics/stag_news';
                    $this->common_function->androidTopicPush($topic, $payload);

                    $iosTopic = ENV == 'production' ? '/topics/ios_prod_news' : '/topics/ios_stag_news';
                    $this->common_function->iOSTopicPush($iosTopic, $payload);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendSessionNotification()
    {
        $url = FCM_URL;
        $serverKey = FCM_SERVER_KEY;
        $currentDate = date("Y-m-d");
        $where = array('where' => ['session_date' => $currentDate, 'is_session_start' => 1], 'order_by' => ['id' => 'desc']);
        $is_one_time = isset($_GET['is_one_time']) ? $_GET['is_one_time'] : ' ';
        $duration = isset($_GET['duration']) ? $_GET['duration'] : ' ';
        $title = isset($_GET['title']) ? $_GET['title'] : 'Goa Deserves Better';
        $message = isset($_GET['message']) ? $_GET['message'] : 'GDB is live';
        $getSession = $this->Common_model->fetch_data('live_session', '*', $where, true);
        $this->load->library('common_function');
        if (!empty($getSession)) {
            $notification = [
                "title" => $title,
                "message" => $message,
                "body" => '',
                "type" => 11,
                "image" => '',
                'news_id' => '',
                'task_id' => '',
                'notification_id' => '',
                'is_one_time' => $is_one_time,
                'duration' => $duration

            ];

            $topic = (ENV == 'production') ? '/topics/live_session' : '/topics/live_session_staging';
            $this->common_function->androidTopicPush($topic, $notification);

            $iosTopic = (ENV == 'production') ? '/topics/ios_live_session' : '/topics/ios_live_session_staging';
            $this->common_function->iOSTopicPush($iosTopic, $notification);

            echo json_encode(array('status' => 'success'));
            exit;
        } else {
            $response_array = [
                'CODE' => RECORD_NOT_EXISTS,
                'MESSAGE' => 'No live session found yet',
                'RESULT' => (object) array(),
            ];
            echo json_encode($response_array);
            exit;
        }
    }

    public function sendDailyNotification()
    {
        $this->load->library('common_function');
        $days = array(1, 2, 3);
        foreach ($days as $day) {
            $users = $this->Cron_model->getNewUsers($day);
            $token = array();
            $data = array();
            if ($day == 3) {
                $title = "தமிழகத்தின் மாற்றத்திற்கு!";
                $msg   = "ஸ்டாலின் அணி செயலியின் வழி பங்காற்றுங்கள்.";
            } else if ($day == 2) {
                $title = "'முடித்தே தீருவோம் என்பது வெற்றிக்கான தொடக்கம்' - கலைஞர்";
                $msg   = "ஸ்டாலின் அணியின் பிரத்யேகச் செய்திகளுடன் உங்கள் நாள் மலரட்டும்";
            } else {
                $title = "காலை வணக்கம்!!";
                $msg   = "உங்கள் நாளை ஸ்டாலின் அணி செயலியுடன் தொடங்குங்கள் !";
            }
            $payload = [
                "title" => $title,
                "body" => $msg,
                "message" => $msg,
                "type" => 1,
                "image" => '',
                "ext_link" => '',
                "url_title" => '',
                "news_id" => '',
                "task_id" => '',
                'notification_id' => 0
            ];
            if (count($users) > 0) {
                /*foreach ($users as $user) {
                    $token[] =  $user['device_token'];
                    $data[] = array('user_id' => $user['user_id'], 'daily_notif_count' => $day);
                }
                $this->db->update_batch('users', $data, 'user_id');
                $this->common_function->androidPush($token, $payload);*/

                $users          = $this->common_function->prepareMultipleAssociativeArr($users, 'platform');
                $androidDevices = $users['android'];
                $iosDevices     = $users['ios'];

                if (count($androidDevices) > 0) {
                    $updateAndroidData          = array();
                    $androidTokens              = array();
                    foreach ($androidDevices as $androidUser) {
                        $androidTokens[]        =  $androidUser['device_token'];
                        $updateAndroidData[]    = array('user_id' => $androidUser['user_id'], 'daily_notif_count' => $day);
                    }
                    $this->db->update_batch('users', $updateAndroidData, 'user_id');
                    $this->common_function->androidPush($androidTokens, $payload);
                }


                if (count($iosDevices) > 0) {
                    $updateiOSData              = array();
                    $iosTokens                  = array();
                    foreach ($iosDevices as $iosUser) {
                        $iosTokens[]            =  $iosUser['device_token'];
                        $updateiOSData[]        = array('user_id' => $iosUser['user_id'], 'daily_notif_count' => $day);
                    }
                    $this->db->update_batch('users', $updateiOSData, 'user_id');
                    $this->common_function->iosPush($iosTokens, $payload);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendnewsNotification_OLD()
    {
        // $token = array();
        // $url = FCM_URL;
        // $serverKey = FCM_SERVER_KEY;
        // $total_Records = 0;

        // //get user ern to send notification
        // $news = $this->Cron_model->getNews();
        // if (count($news) > 0) {
        //     foreach ($news as $n) {
        //         $news_id = $n['news_id'];
        //         $news_title = $n['news_title'];
        //         $news_type = $n['news_category'];
        //         $param['registeration_no'] = $n['registeration_no'];
        //         $param['district'] = $n['district'];
        //         $param['college'] = $n['college'];
        //         $param['priority'] = $n['priority'];
        //         $param['gender'] = $n['gender'];

        //         $homeContentType = unserialize(HOME_CONTENT_TYPE);
        //         if ($news_type != BANNER) {
        //             $userErn = $this->Cron_model->newsUserErns($param, $news_id);

        //             $notification = [
        //                 "title" => $news_title . ' ' . 'மேலும் பார்க்க..',
        //                 "message" => '',
        //                 "body" => $news_title . ' ' . 'மேலும் பார்க்க..',
        //                 "type" => 2,
        //                 "image" => '',
        //                 'news_id' => $news_id,
        //                 'task_id' => '',
        //                 'notification_id' => ''
        //             ];

        //             //send notification
        //             $total_Records = count($userErn);
        //             if ($total_Records > 0) {
        //                 $sendNotifiUser = [];
        //                 $in = 0;
        //                 foreach ($userErn as $users) {
        //                     $token[] = $users['device_token'];
        //                     $insertNewsNoti[$in] = array(
        //                         'user_id' => $users['user_id'],
        //                         'news_id' => $news_id,
        //                         'task_id' => 0,
        //                         'notification_type' => $homeContentType[$news_type],
        //                         'notification_text' => $news_title,
        //                         'inserted_on' => date('Y-m-d H:i:s')
        //                     );
        //                     $in++;
        //                     $sendNotifiUser = $insertNewsNoti;
        //                 }
        //                 $temp = array_unique(array_column($sendNotifiUser, 'user_id'));
        //                 $unique_arr = array_intersect_key($sendNotifiUser, $temp);
        //                 $insertStatus = $this->Common_model->insert_batch('user_notification', array(), $unique_arr, '');
        //                 if ($insertStatus) {
        //                     $tokens = array_chunk($token, 1000);
        //                     foreach ($tokens as $tok) {
        //                         if (count($tok) > 0) {
        //                             //send message to filtered users or selected users
        //                             $arrayToSend = array('registration_ids' => $tok, 'data' => $notification, 'priority' => 'high');
        //                             $json = json_encode($arrayToSend);
        //                             $headers = array();
        //                             $headers[] = 'Content-Type: application/json';
        //                             $headers[] = 'Authorization: key=' . $serverKey;
        //                             $ch = curl_init();
        //                             curl_setopt($ch, CURLOPT_URL, $url);
        //                             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        //                             curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        //                             curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //                             //Send the request
        //                             $response = curl_exec($ch);
        //                             //Close request
        //                             if ($response === FALSE) {
        //                                 die('FCM Send Error: ' . curl_error($ch));
        //                             }
        //                             curl_close($ch);
        //                             print_r($response);
        //                         }
        //                     }
        //                     exit;
        //                 }
        //             } else {
        //                 $newsUpdateArr = array('notification_update' => 1);
        //                 $whereArr = [];
        //                 $whereArr['where'] = array('news_id' => $news_id);
        //                 $this->Common_model->update_single('ipac_news', $newsUpdateArr, $whereArr);
        //             }
        //         }
        //     }
        // }
        // echo json_encode(array('status' => 'success', 'total_notification_sent' => $total_Records));
        // exit;
    }

    public function sendtasksNotification_OLD()
    {
        // $token = array();
        // $url = FCM_URL;
        // $serverKey = FCM_SERVER_KEY;
        // $total_Records = 0;

        // $tasks = $this->Cron_model->getTasks();

        // if (count($tasks) > 0) {
        //     foreach ($tasks as $n) {
        //         $task_id = $n['task_id'];
        //         $task_title = $n['task_title'];
        //         $task_type = $n['task_type'];
        //         $action = $n['action'];
        //         $show_popup = $n['show_popup'];
        //         $param['registeration_no'] = $n['registeration_no'];
        //         $param['district'] = $n['district'];
        //         $param['college'] = $n['college'];
        //         $param['gender'] = $n['gender'];

        //         $userErn = $this->Cron_model->tasksUserErns($param, $task_id);
        //         switch ($task_type) {
        //             case 'facebook': //Facebook Message
        //                 $taskTitle = 'எதிர்கால தலைமுறையின் முன்னேற்றத்தில் பங்கெடுங்கள்…! - (பேஸ்புக் டாஸ்க்)';
        //                 break;
        //             case 'twitter': //Twitter Message
        //                 $taskTitle = 'மாபெரும் அரசியல் மாற்றத்தில் பங்கெடுங்கள்…! (ட்விட்டர் டாஸ்க்)';
        //                 break;
        //             case 'whatsapp': // Whatsapp Message
        //                 $taskTitle = 'மாற்றத்தை நோக்கிய பயணத்தில் பங்கெடுங்கள்…!வாட்சப் டாஸ்க்)';
        //                 break;
        //             case 'youtube': // Youtube Message
        //                 $taskTitle = 'ஒரு க்ளிக்...மாற்றம்...மக்களின் முதல்வருக்காக…! (யூடுப் டாஸ்க்)';
        //                 break;
        //             default:
        //                 $taskTitle = 'New ' . ucfirst($task_type) . ' task added ' . $task_title;
        //         }
        //         $insertNewsArray = array(
        //             'task_title' => $n['task_title'],
        //             'task_description' => $n['task_description'],
        //             'post_url' => $n['post_url'],
        //             'post_content' => $n['post_content'],
        //             'points' => $n['points'],
        //             'start_date' => $n['start_date'],
        //             'end_date' => $n['end_date'],
        //             'state' => $n['state'],
        //             'district' => $n['district'],
        //             'gender' => $n['gender'],
        //             'registeration_no' => $n['registeration_no'],
        //             'task_type' => $n['task_type'],
        //             'task_status' => $n['task_status'],
        //             'college' => $n['college'],
        //             'action' => $action,
        //             'created_date' => $n['created_date'],
        //             'latitude' => $n['latitude'],
        //             'longitude' => $n['longitude'],
        //             'radius' => $n['radius'],
        //             'video_id' => $n['video_id'],
        //             'channel_id' => $n['channel_id'],
        //             'twitter_follow_id' => $n['twitter_follow_id'],
        //             'twitter_follow_name' => $n['twitter_follow_name'],
        //             'facebook_follow_id' => $n['facebook_follow_id'],
        //             'facebook_follow_name' => $n['facebook_follow_name'],
        //             'instruction_video_url' => $n['instruction_video_url'],
        //             'instruction_description' => $n['instruction_description'],
        //             'form_id' => $n['form_id'],
        //             'form_steps' => $n['form_steps'],
        //             'whatsapp_image_url1' => $n['whatsapp_image_url1'],
        //             'whatsapp_image_url2' => $n['whatsapp_image_url2'],
        //             'show_popup' => $n['show_popup'],
        //             'qr_code_url' => $n['qr_code_url'],
        //             'form_url' => $n['form_url']
        //         );

        //         $insertNewsArray['task_media_set'] =  $this->Cron_model->getTaskMedia($task_id);
        //         $notification = [
        //             "title" => $taskTitle,
        //             "message" => $task_title,
        //             "type" => 3,
        //             "image" => '',
        //             'task_id' => $task_id,
        //             'news_id' => '',
        //             'notification_id' => '',
        //             "show_popup" => $show_popup,
        //             "task_details" => $insertNewsArray,
        //             "body" => $task_title
        //         ];
        //         //send notification
        //         $total_Records = count($userErn);
        //         if ($total_Records > 0) {
        //             $sendNotifiUser = [];
        //             $in = 0;
        //             foreach ($userErn as $users) {
        //                 $token[] = $users['device_token'];
        //                 $insertNewsNoti[$in] = array(
        //                     'user_id' => $users['user_id'],
        //                     'news_id' => 0,
        //                     'task_id' => $task_id,
        //                     'notification_type' => ADD_TASK_NOTIFICATION,
        //                     'notification_text' => 'New ' . ucfirst($task_type) . ' task added ' . $task_title,
        //                     'inserted_on' => date('Y-m-d H:i:s')
        //                 );
        //                 $in++;
        //                 $sendNotifiUser = $insertNewsNoti;
        //             }
        //             $temp = array_unique(array_column($sendNotifiUser, 'user_id'));
        //             $unique_arr = array_intersect_key($sendNotifiUser, $temp);
        //             $insertStatus = $this->Common_model->insert_batch('user_notification', array(), $unique_arr, '');
        //             if ($insertStatus) {
        //                 $tokens = array_chunk($token, 1000);
        //                 foreach ($tokens as $tok) {
        //                     if (count($tok) > 0) {
        //                         //send message to filtered users or selected users
        //                         $arrayToSend = array('registration_ids' => $tok, 'data' => $notification, 'priority' => 'high');
        //                         $json = json_encode($arrayToSend);
        //                         $headers = array();
        //                         $headers[] = 'Content-Type: application/json';
        //                         $headers[] = 'Authorization: key=' . $serverKey;
        //                         $ch = curl_init();
        //                         curl_setopt($ch, CURLOPT_URL, $url);
        //                         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        //                         curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        //                         curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //                         //Send the request
        //                         $response = curl_exec($ch);
        //                         //Close request
        //                         if ($response === FALSE) {
        //                             die('FCM Send Error: ' . curl_error($ch));
        //                         }
        //                         curl_close($ch);
        //                         print_r($response);
        //                     }
        //                 }
        //                 exit;
        //             }
        //         } else {
        //             $newsUpdateArr = array('notification_update' => 1);
        //             $whereArr['where'] = array('task_id' => $task_id);
        //             $this->Common_model->update_single('ipac_task_master', $newsUpdateArr, $whereArr);
        //         }
        //     }
        // }
        // echo json_encode(array('status' => 'success', 'total_notification_sent' => $total_Records));
        // exit;
    }

    public function setNotificationReceived($userId = '')
    {
        if ($userId != '') {
            $updateArr          = array();
            $updateArr['is_notification_received'] = '1';
            $updateUserCond     = array('where' => array('user_id' => $userId));

            $userInfo = $this->Common_model->update_single('users', $updateArr, $updateUserCond);
        }
        return true;
    }

    public function sendPhotoAlbumLevelSeven()
    {
        $userErn = $this->Cron_model->userlevel(7);
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {

                if ($users['email_id'] != '') {

                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $subject                = "இடைச்சங்க பாசறை நிலையைக் கடந்ததற்கு ஸ்டாலின் அணியின் வாழ்த்துக்கள்!";
                    $template               = 'achievement_level_seven';

                    $sendMail               = sendMail($subject, $mailData, $template);

                    if ($sendMail) {
                        $updateUserArray = array('notified' => '1');
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                    sleep(5);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendPhotoAlbumLevelNine()
    {
        $userErn = $this->Cron_model->userlevel(9);
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {

                if ($users['email_id'] != '') {

                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $subject                = "நான்காம்சங்கப் பாசறை நிலையைக் கடந்ததற்கு ஸ்டாலின் அணியின் வாழ்த்துக்கள்!";
                    $template               = 'achievement_level_nine';

                    $sendMail               = sendMail($subject, $mailData, $template);

                    if ($sendMail) {
                        $updateUserArray = array('notified' => '1');
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    /*
    *
    * @function addRewardsToUsers
    * @Description used to add rewards to the users mentioned in below sheet
    */
    public function addRewardsToUsers()
    {

        $this->load->library('common_function');
        $fileName               = 'public/live_session_phase7.csv';
        if (file_exists($fileName)) {
            $file               = fopen($fileName, "r");
            $inputUsersData     = array();
            $headerLine         = true;
            while (($column = fgetcsv($file, 100000, ",")) !== FALSE) {
                if ($headerLine) {
                    $headerLine = false;
                } else {
                    $inputUsersData[]       = array(
                        'user_id'           => $column[0],
                        'points'            => $column[1]
                    );
                }
            }

            if (!empty($inputUsersData) && count($inputUsersData) > 0) {
                $associativeUsers       = $this->common_function->prepareAssociativeArr($inputUsersData, 'user_id');
                $getData                = $this->input->get();

                $title                  = '29/3/21 to 4/4/21 (MKS live)';
                if (isset($getData['title']) && $getData['title'] != '') {
                    $title              = $getData['title'];
                }

                $description            = 'Here are your MKS live session special points';
                if (isset($getData['desc']) && $getData['desc'] != '') {
                    $description        = $getData['desc'];
                }


                $created_date           = date('Y-m-d H:i:s');
                if (isset($getData['created_date'])  && $getData['created_date'] != '') {
                    $created_date       = $getData['created_date'];
                }

                $this->db->trans_begin(); #DB transaction Start

                $chunkUsersList         = array_chunk($inputUsersData, 100);


                foreach ($chunkUsersList as $chunkKey => $chunkVal) {
                    $this->load->model('User_model');
                    $userIds            = '';

                    if (!empty($chunkVal) && count($chunkVal) > 0) {
                        $userIds            = array_column($chunkVal, 'user_id');


                        /*Check Users exists or not*/
                        $this->db->select("user_id,is_active");
                        $this->db->from('users');
                        $this->db->where_in('user_id', $userIds);
                        $this->db->order_by("user_id", "desc");
                        $query    = $this->db->get();
                        $num_rows = $query->num_rows();
                        $existedUsers            = array();
                        if ($num_rows > 0) {
                            $existedUsers        = $query->result_array();
                        }


                        if (!empty($existedUsers) && count($existedUsers) > 0) {

                            $userLevelInsertArr     = array();
                            $userWalletInsertArr    = array();
                            $updatePointsArr        = array();

                            foreach ($existedUsers as $euKey => $euVal) {
                                $walletInsertArr        = array();
                                $userId                 = $euVal['user_id'];
                                $params                 = array();
                                $params['points']       = $associativeUsers[$userId]['points'];
                                $params['user_id']      = $userId;
                                echo "<pre>";
                                print_r('userid is =' . $userId);
                                echo "<br/>";
                                print_r('points is =' . $associativeUsers[$userId]['points']);
                                echo "<br/>";
                                $updateRes              = $this->User_model->updateUserEarnPoints($params);

                                echo "  User Points updated query ::  <br/>";
                                echo $this->db->last_query();
                                echo "   <br/> -------------------------   <br/>";

                                $userData               = $this->User_model->userDetail($userId);
                                $current_points         = $userData['points_earned'];
                                $whereArr = ['fk_user_id' => $userId];
                                $user_level             = $this->User_model->getUserLevels($userId);
                                $current_level          = $user_level['fk_level_id'] ? $user_level['fk_level_id'] : '0';
                                print_r('current_level is =' . $current_level);

                                if ($current_level != 'null') {
                                    $level_array = $this->nextlevel($current_points, $current_level);
                                    //print_r($level_array); exit;
                                    $difference = $level_array['max_level'] - $current_level;
                                    if ($difference < 0) {
                                        $level_array['current_points'] = $current_points;
                                        $level_array['max_level'] = $current_level;
                                        return $level_array;
                                    } else {
                                        for ($i = 1; $i <= $difference; $i++) {
                                            $fk_level_id = $current_level + $i;
                                            $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                                            $reward_points = $reward_points->row_array();
                                            $free_rewards =  $reward_points['free_reward_points'];
                                            if ($free_rewards != 0) {
                                                $params                 = array();
                                                $params['points']       = $free_rewards;
                                                $params['user_id']      = $userId;
                                                $res                    = $this->User_model->updateUserEarnPoints($params);
                                                //print_r($res); exit;


                                                $walletInsertArr[]      = array(
                                                    'user_id'           => $userId,
                                                    'point'             => $free_rewards,
                                                    'title'             => 'New Level Rewards',
                                                    'description'       => 'Rewards earned for completion of levels',
                                                    'task_id'           => '0',
                                                    'type'              => 'bonus',
                                                    'status'            => '1',
                                                    'created_date'      => date('Y-m-d H:i:s'),
                                                    'updated_date'      => date('Y-m-d H:i:s')
                                                );
                                            }

                                            $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $userId, 'unlock_date' => date('Y-m-d H:i:s')]);
                                            echo "   <br/> -------------------------   <br/>";
                                            echo "   <br/> -------------------------   <br/>";
                                            print_r('levels added successfully for ' . $userId);
                                        }
                                    }
                                    if ($updateRes) {
                                        $walletInsertArr[]          = array(
                                            'user_id'               => $userId,
                                            'point'                 => $associativeUsers[$userId]['points'],
                                            'title'                 => $title,
                                            'description'           => $description,
                                            'task_id'               => '0',
                                            'type'                  => 'bonus',
                                            'status'                => '1',
                                            'created_date'          => date('Y-m-d H:i:s'),
                                            'updated_date'          => date('Y-m-d H:i:s')
                                        );
                                    }
                                    print_r($walletInsertArr);
                                    if (!empty($walletInsertArr) && count($walletInsertArr) > 0) {
                                        $this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
                                        echo "   <br/> -------------------------   <br/>";
                                        echo "   <br/> -------------------------   <br/>";
                                        print_r('wallet rewards added successfully for ' . $userId);
                                    }
                                    if (true === $this->db->trans_status()) {
                                        #Comminting changes
                                        $this->db->trans_commit();
                                        echo "   <br/> -------------------------   <br/>";
                                        echo "   <br/> -------------------------   <br/>";
                                        echo "Rewards added successfully.";
                                    } else {
                                        echo "Error While adding rewards.Please try again.";
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                echo "Data not found.";
                exit;
            }
        } else {
            echo "File not found.";
            exit;
        }
    }

    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }

    /*
    *
    * @function addReferrals
    * @Description used to add referrals from the below sheet
    */
    public function addReferrals()
    {
        $fileName               = 'public/Final_Referral.csv';
        if (file_exists($fileName)) {
            $file               = fopen($fileName, "r");
            $sheetUsersData     = array();
            while (($column = fgetcsv($file, 100000, ",")) !== FALSE) {
                $sheetUsersData[]       = array(
                    'User_id'           => $column[0],
                    'Referral_User'     => $column[1]
                );
            }

            if (!empty($sheetUsersData) && count($sheetUsersData) > 0) {
                $usersData              = array_shift($sheetUsersData);
            }

            if (!empty($sheetUsersData) && count($sheetUsersData) > 0) {
                $this->db->trans_begin(); #DB transaction Start
                $chunkUsersList         = array_chunk($sheetUsersData, 100);
                foreach ($chunkUsersList as $chunkKey => $chunkVal) {
                    if (!empty($chunkVal) && count($chunkVal) > 0) {
                        $referralInsertArray     = array();

                        foreach ($chunkVal as $euKey => $euVal) {
                            $userId                 = $euVal['User_id'];
                            $referalUser            = $euVal['Referral_User'];
                            $this->db->select("*");
                            $this->db->from('ipac_referal_user');
                            $this->db->where('user_id', $userId);
                            $this->db->where('referal_user_id', $referalUser);
                            $query    = $this->db->get();
                            $num_rows = $query->num_rows();
                            if ($num_rows > 0) {
                                echo " Record already exists <br/>";
                                echo print_r($query->result_array());
                                echo "<br/> --------------------- <br/>";
                            } else {
                                $referralInsertArray[] = array(
                                    'user_id'               => $userId,
                                    'referal_user_id'       => $referalUser,
                                    'created_date'          => '0000-00-00 00:00:00'
                                );
                            }
                        }
                        if (!empty($referralInsertArray) && count($referralInsertArray) > 0) {
                            $this->Common_model->insert_multiple('ipac_referal_user', $referralInsertArray);
                            echo "<br/><br/>";
                            echo " Record inserted <br/>";
                            echo $this->db->last_query();
                            echo "<br/> -------------------------   <br/>";
                        }
                    }
                }
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();
                    echo "   <br/> -------------------------   <br/>";
                    echo "   <br/> -------------------------   <br/>";
                    echo "Records added successfully";
                } else {
                    echo "   <br/> -------------------------   <br/>";
                    echo "   <br/> -------------------------   <br/>";
                    echo "Error While adding records.Please try again.";
                }
                exit;
            } else {
                echo "Data not found.";
                exit;
            }
        } else {
            echo "File not found.";
            exit;
        }
    }

    /*
    *
    * @function changeEmails
    * @Description used to change emails from the below sheet
    */
    public function changeEmails()
    {
        $fileName               = 'public/Stalinani_user_emailid.csv';
        if (file_exists($fileName)) {
            $file               = fopen($fileName, "r");
            $sheetUsersData     = array();
            while (($column = fgetcsv($file, 100000, ",")) !== FALSE) {
                $sheetUsersData[]       = array(
                    'user_id'           => $column[0],
                    'email'             => $column[1]
                );
            }

            if (!empty($sheetUsersData) && count($sheetUsersData) > 0) {
                $usersData              = array_shift($sheetUsersData);
            }

            if (!empty($sheetUsersData) && count($sheetUsersData) > 0) {
                $this->db->trans_begin(); #DB transaction Start
                $chunkUsersList         = array_chunk($sheetUsersData, 100);
                foreach ($chunkUsersList as $chunkKey => $chunkVal) {
                    if (!empty($chunkVal) && count($chunkVal) > 0) {
                        $referralInsertArray     = array();
                        $updateUserArray         = array();

                        foreach ($chunkVal as $euKey => $euVal) {
                            $userId                 = $euVal['user_id'];
                            $email                  = $euVal['email'];
                            $this->db->select("user_id");
                            $this->db->from('users');
                            $this->db->where('user_id', $userId);
                            $query    = $this->db->get();
                            $num_rows = $query->num_rows();
                            if ($num_rows > 0) {
                                // table - users
                                $updateUserArray[]          = array(
                                    'email_id'              => $email,
                                    'user_id'               => $userId
                                );
                            } else {
                                echo "User doesn't exist : " . $userId;
                                echo "<br/> -------------------------<br/>";
                            }
                        }
                        if (!empty($updateUserArray) && count($updateUserArray) > 0) {
                            $this->Common_model->update_multiple('users', $updateUserArray, 'user_id');

                            echo "Emails updated<br>";
                            echo $this->db->last_query();
                            echo "<br/> -------------------------   <br/>";
                        }
                    }
                }
                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();
                    echo "   <br/> -------------------------   <br/>";
                    echo "   <br/> -------------------------   <br/>";
                    echo "Records added successfully";
                } else {
                    echo "   <br/> -------------------------   <br/>";
                    echo "   <br/> -------------------------   <br/>";
                    echo "Error While adding records.Please try again.";
                }
                exit;
            } else {
                echo "Data not found.";
                exit;
            }
        } else {
            echo "File not found.";
            exit;
        }
    }

    public function sendSessionEndNotification()
    {
        $url = FCM_URL;
        $serverKey = FCM_SERVER_KEY;
        $whereStatus['where'] = array('is_session_start' => 1);
        $livesession = array('is_session_start' => '0');
        $this->Common_model->update_single('live_session', $livesession, $whereStatus);
        $notification = [
            "title" => 'GDB Live',
            "message" => 'Thanks for attending. GDB Live is now complete.',
            "body" => '',
            "type" => 14,
            "image" => '',
            'news_id' => '',
            'task_id' => '',
            'notification_id' => ''
        ];

        $this->load->library('common_function');
        $topic = (ENV == 'production') ? '/topics/live_session' : '/topics/live_session_staging';
        $this->common_function->androidTopicPush($topic, $notification);

        $iosTopic = (ENV == 'production') ? '/topics/ios_live_session' : '/topics/ios_live_session_staging';
        $this->common_function->iOSTopicPush($iosTopic, $notification);

        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendPodcastNotification()
    {
        $url = FCM_URL;
        $serverKey = FCM_SERVER_KEY;
        $currentDate = date('Y-m-d H:i:s');
        $where = array('where' => ['schedule_date <=' => $currentDate, 'notification_status' => 0, 'status' => 1], 'order_by' => ['id' => 'desc']);
        $getPodcast = $this->Common_model->fetch_data('podcast', '*', $where, true);

        if (!empty($getPodcast)) {
            $newsUpdateArr = array('notification_status' => 1);
            $whereArr['where'] = array('id' => $getPodcast['id']);
            $this->Common_model->update_single('podcast', $newsUpdateArr, $whereArr);
            $whereStatus['where'] = array('id !=' => $getPodcast['id']);
            $podUpdates = array('status' => '0');
            $this->Common_model->update_single('podcast', $podUpdates, $whereStatus);
            $notifTitle = $getPodcast['notification_title'];
            $notifMessage = $getPodcast['notification_message'];
            $notification = [
                "title" => ($notifTitle != '') ? $notifTitle : 'ஸ்டாலின் அணி வானொலி',
                "message" => ($notifMessage != '') ? $notifMessage : "'ஸ்டாலின் அணி வானொலியில்' சிறப்பு நிகழ்ச்சி துவக்கம்",
                "body" => $getPodcast,
                "type" => 15,
                "image" => '',
                'news_id' => '',
                'task_id' => '',
                'notification_id' => ''
            ];
            $this->load->library('common_function');
            $topic = (ENV == 'production') ? '/topics/prod_radio' : '/topics/stag_radio';
            $this->common_function->androidTopicPush($topic, $notification);

            $iosTopic = (ENV == 'production') ? '/topics/ios_prod_radio' : '/topics/ios_stag_radio';
            $this->common_function->iOSTopicPush($iosTopic, $notification);

            echo json_encode(array('status' => 'success'));
            exit;
        } else {
            $response_array = [
                'CODE' => RECORD_NOT_EXISTS,
                'MESSAGE' => 'No Pod cast found yet',
                'RESULT' => (object) array(),
            ];
            echo json_encode($response_array);
            exit;
        }
    }

    public function sendSessionSimpleNotification()
    {
        $url = FCM_URL;
        $serverKey = FCM_SERVER_KEY;
        $currentDate = date("Y-m-d");
        $where = array('where' => ['session_date' => $currentDate, 'is_session_start' => 1], 'order_by' => ['id' => 'desc']);
        $getSession = $this->Common_model->fetch_data('live_session', '*', $where, true);

        $title = isset($_GET['title']) ? $_GET['title'] : 'GDB Live';
        $greeting_message = isset($_GET['greeting_message']) ? $_GET['greeting_message'] : 'GDB Live';
        $message = isset($_GET['message']) ? $_GET['message'] : 'It’s time to share your ideas!';

        if (!empty($getSession)) {
            $notification = [
                "title" => $title,
                "greeting_message" => $greeting_message,
                "message" => $message,
                "body" => '',
                "type" => 16,
                "image" => '',
                'news_id' => '',
                'task_id' => '',
                'notification_id' => ''
            ];

            $this->load->library('common_function');
            $topic = (ENV == 'production') ? '/topics/live_session' : '/topics/live_session_staging';
            $this->common_function->androidTopicPush($topic, $notification);

            $iosTopic = (ENV == 'production') ? '/topics/ios_live_session' : '/topics/ios_live_session_staging';
            $this->common_function->iOSTopicPush($iosTopic, $notification);

            echo json_encode(array('status' => 'success'));
            exit;
        } else {
            $response_array = [
                'CODE' => RECORD_NOT_EXISTS,
                'MESSAGE' => 'No live session found yet',
                'RESULT' => (object) array(),
            ];
            echo json_encode($response_array);
            exit;
        }
    }

    public function checkreferallist()
    {
        $users = $this->Cron_model->checkreferallist();
        if (count($users) > 0) {
            foreach ($users as $key => $value) {
                $referal_code = $value['referal_code'];
                $newCharacter = $this->Cron_model->getReferral(1);
                $newReferalCode = str_replace(array('?', '"', '.', '_', '-', ' '), $newCharacter, $referal_code);
                // $where = array('where' => ['referal_code' => $newReferalCode]);
                // $result = $this->Common_model->fetch_count('users', $where);
                // if($result == 0){
                $data[] = array('user_id' => $value['user_id'], 'referal_code' =>  $newReferalCode);
                echo "<b>User Id :</b> " . $value['user_id'] . ", <b>Phone Number :</b> " . $value['phone_number'] . ", <b>Full Name :</b> " . $value['full_name'] . ", <b>Old :</b> " . $referal_code . ", <b>New :</b> " . $newReferalCode . "<br>";
                //// }
            }
            $this->db->update_batch('users', $data, 'user_id');
        }

        echo "<br><br>";
        echo "done";
        exit;
    }

    public function setprofileverification()
    {
        $where = array('order_by' => ['points_earned' => 'DESC'], 'limit' => ['500']);
        $users = $this->Common_model->fetch_data('users', 'user_id', $where, false);
        $userUpdateData = array('is_profile_verified' => 0);
        $this->Cron_model->updateVerified('users', $userUpdateData);
        $updateRecords = array();
        foreach ($users as $user) {
            if ($user['user_id'] != '') {
                $updateRecords[]  = array('user_id' => $user['user_id'], 'is_profile_verified' => 1);
            }
        }
        $userInfo = $this->Common_model->update_multiple('users', $updateRecords, 'user_id');
        echo "done";
        exit;
    }

    public function sendPhotoAlbumLevelEleven()
    {
        $userErn = $this->Cron_model->userlevel(1);
        // print_r("Hello");
        //die;
        // print_r($userErn);exit;
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];

                    $inputKeys              = $users['registeration_no'] . '@' . $users['user_id'] . '@11';
                    // print_r($inputKeys);
                    // die;
                    $inputKeys              = encrypt_decrypt('encrypt', $inputKeys);
                    print_r($inputKeys);
                    // die;
                    $mailData['link']       = base_url() . 'generate_certificate/?key=' . $inputKeys;
                    print_r($mailData);
                    die;
                   
                    $subject                = "வினையூக்கி நிலையைக் கடந்ததற்கு ஸ்டாலின் அணியின் வாழ்த்துக்கள்!";
                    $template               = 'achievement_level_eleven';

                    $sendMail               = sendMail($subject, $mailData, $template);

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                    //   sending sms 
                    if ($users['phone_number'] != '') {
                        $new_url = $this->get_tiny_url($mailData['link']);
                        sendMessage(COUNTRYCODE . $users['phone_number'], "வாழ்த்துக்கள் உடன்பிறப்பே! நீங்கள் வினையூக்கி நிலையைக் கடந்துவிட்டீர்கள். கீழுள்ள இணைப்பில் உங்கள் சான்றிதழ் உள்ளது! " . $new_url." - DMK",SMS_LEVEL_11_MSG_TID,1);

                        $updateUserArray    = array(
                            'notified'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                    //end sms sending
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    // public function updaterewardpoints()
    // {
    //     $user_details = $this->Cron_model->updaterewardpoints();
    //     foreach ($user_details as $user) {
    //         $user_id = $user['fk_user_id'];
    //         $current_points = $user['current_points'];
    //         $current_level = $user['current_level'];
    //         if ($current_level != '') {
    //             $level_array = $this->nextlevel($current_points, $current_level);
    //             $difference = $level_array['max_level'] - $current_level;
    //             if ($difference < 0) {
    //                 $level_array['current_points'] = $current_points;
    //                 $level_array['max_level'] = $current_level;
    //                 return $level_array;
    //             } else {
    //                 for ($i = 1; $i <= $difference; $i++) {
    //                     $fk_level_id = $current_level + $i;
    //                     $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
    //                 }
    //             }
    //             $whereArr['where'] = ['user_id' => $user_id];
    //             $userPoints = array(
    //                 'points_earned' => $level_array['current_points'],
    //                 'total_earning' => $level_array['current_points'],
    //                 'wallet_status' => 1
    //             );
    //             $this->Common_model->update_single('users', $userPoints, $whereArr);
    //             $current_points = $this->previousrewards($level_array['current_points'], $user_id);
    //             echo "<pre>";
    //             echo "rewards are added successfully for: $user_id ";

    //         }
    //     }
    //     exit;
    // }
    // function nextlevel($current_points, $current_level)
    // {
    //     $level_array = [];
    //     $new_levels = $this->Cron_model->nextlevel($current_points);
    //     if (!empty($new_levels)) {
    //         $next_levels = $new_levels['pk_level_id'];

    //         if ($next_levels == $current_level) {
    //             $level_array['current_points'] = $current_points;
    //             $level_array['max_level'] = $current_level;
    //             return $level_array;
    //         }
    //         $level_difference =  $next_levels - $current_level;

    //         if ($level_difference < 0) {
    //             $level_array['current_points'] = $current_points;
    //             $level_array['max_level'] = $current_level;
    //             return $level_array;
    //         } else {
    //             for ($i = 1; $i <= $level_difference; $i++) {
    //                 $max_level = $current_level + $i;

    //                 $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
    //                 $free_rewards = $free_rewards->row_array();
    //                 $current_points += $free_rewards['free_reward_points'];
    //                 $level_array['current_points'] = $current_points;
    //                 $level_array['max_level'] = $max_level;
    //             }
    //         }
    //         return self::nextlevel($level_array['current_points'], $level_array['max_level']);
    //     } else {
    //         $level_array['current_points'] = $current_points;
    //         $level_array['max_level'] = $current_level;
    //         return $level_array;
    //     }
    // }
    // function previousrewards($current_points, $user_id)
    // {
    //     $reward_points = $this->db->query("SELECT pk_level_id,free_reward_points from tbl_levels where eStatus='active' and $current_points >= start_point AND ($current_points <= end_point OR $current_points >= end_point)  AND free_reward_points != '0'");
    //     $free_rewards = $reward_points->result_array();
    //     foreach ($free_rewards as $users) {
    //         $walletInsertArr[]      = array(
    //             'user_id'           =>  $user_id,
    //             'point'             =>  $users['free_reward_points'],
    //             'title'             => 'New Level Rewards',
    //             'description'       => 'Rewards earned for completion of levels',
    //             'task_id'           => '0',
    //             'type'              => 'bonus',
    //             'status'            => '1',
    //             'created_date'      => date('Y-m-d H:i:s'),
    //             'updated_date'      => date('Y-m-d H:i:s')
    //         );
    //     }
    //     if (!empty($walletInsertArr) && count($walletInsertArr) > 0) {
    //         $this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
    //     }
    //     return false;
    // }


    private function get_tiny_url($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }


    // public function insertUserstotable()
    // {
    //     $fileName               = 'public/Digital_data.csv';

    //     if (file_exists($fileName)) {
    //         $file               = fopen($fileName, "r");
    //         $sheetUsersData     = array();
    //         $headerLine         = true;
    //         while (($column = fgetcsv($file, 100000, ",")) !== FALSE) {
    //             $sheetUsersData[]       = array(
    //                     'full_name'         => $column[0],
    //                     'phone_number'      => $column[1],
    //                     'password'          => $column[2],
    //                     'gender'            => $column[3],
    //                     'dob'               => $column[4],
    //                     'district'          => $column[5],
    //                     'ac'                => $column[6],
    //                     'email_id'          => $column[7]
    //             );
    //     }

    //         if (!empty($sheetUsersData) && count($sheetUsersData) > 0) {
    //             $usersData              = array_shift($sheetUsersData);
    //         }

    //         if (!empty($sheetUsersData) && count($sheetUsersData) > 0) {
    //           //  $this->db->trans_begin(); #DB transaction Start
    //            foreach ($sheetUsersData as $chunkKey => $chunkVal) {
    //                if (!empty($chunkVal) && count($chunkVal) > 0) {

    //                     $full_name              = $chunkVal['full_name'];
    //                     $phone_number           = $chunkVal['phone_number'];
    //                     $district               = $chunkVal['district'];
    //                     $password               = $chunkVal['password'];
    //                     $gender                 = $chunkVal['gender'];
    //                     $dob                    = $chunkVal['dob'];
    //                     $ac                     = $chunkVal['ac'];
    //                     $email_id               = $chunkVal['email_id'];
    //                     $registration_no        = $this->generateRegistrationNo($district);
    //                     $referalCode            = $this->referalCodeGenerator($full_name);

    //                     //check email exist or not
    //                     $this->db->select("user_id");
    //                     $this->db->from('users');
    //                     $this->db->where('phone_number', $phone_number);
    //                     $query    = $this->db->get();
    //                     $num_rows = $query->num_rows();
    //                     if ($num_rows > 0) {
    //                         echo "phone number existed for the user : " . $full_name;
    //                         echo "<br/> -------------------------<br/>";
    //                         // table - users

    //                     } else {
    //                         $insertUserArray       = array(
    //                             'registeration_no'       => $registration_no,
    //                             'full_name'             => $full_name,
    //                             'email_id'              => $email_id,
    //                             'password'              => createPassword($password),
    //                             'phone_number'          => $phone_number,
    //                             'district'              => $district,
    //                             'gender'                => $gender,
    //                             //'dob'                   => $dob,
    //                             'ac'                    => $ac,
    //                             'referal_code'          => $referalCode,
    //                             'registered_on'         => date('Y-m-d H:i:s')
    //                         );
    //                         if (!empty($insertUserArray) && count($insertUserArray) > 0) {
    //                             $this->Common_model->insert_single('users', $insertUserArray);

    //                             echo "users inserted successfully :". $full_name;
    //                             echo "<br/> -------------------------   <br/>";
    //                         }
    //                     } 
    //                 }
    //             }
    //             // if (true === $this->db->trans_status()) {
    //             //     #Commiting changes
    //             //     $this->db->trans_commit();
    //             //     echo "   <br/> -------------------------   <br/>";
    //             //     echo "   <br/> -------------------------   <br/>";
    //             //     echo "Records added successfully";
    //             // } else {
    //             //     echo "   <br/> -------------------------   <br/>";
    //             //     echo "   <br/> -------------------------   <br/>";
    //             //     echo "Error While adding records.Please try again.";
    //             // }
    //             exit;
    //         } else {
    //             echo "Data not found.";
    //             exit;
    //         }
    //     } else {
    //         echo "File not found.";
    //         exit;
    //     }
    // }

    // function generateRegistrationNo($districtId)
    // {
    //     $this->load->model('admin/News_model');
    //     #generate 3 digit district code of user district
    //     $districtnum = 3;
    //     $district_num_padded = sprintf("%03d", $districtId);

    //     # count the number of users in a particular district
    //     $countUser = $this->News_model->userList($districtId);
    //     $usercnt = 0;
    //     if (!empty($countUser)) {
    //         $usercnt = $countUser->UserCnt;
    //     }
    //     $usercnt = $usercnt + 1;
    //     # generate 4 digit user count for a particular district
    //     $districtusernum = 4;
    //     $district_user_num_padded = sprintf("%04d", $usercnt);

    //     $prefix = 'STLN';
    //     $sufix = 'PTA';
    //     $registrationNo = $prefix . $district_num_padded . $sufix . $district_user_num_padded;
    //     return $registrationNo;
    // }
    // private function referalCodeGenerator($name)
    // {
    //     $Username = $this->Cron_model->getReferral(2);
    //     $uniqecode = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6));
    //     $referalCode = strtoupper($Username . $uniqecode);
    //     return $referalCode;
    // }

    public function sendCredentialsEmail()
    {
        $where = array('where' => ['is_email_sent' => 0], 'limit' => ['100']);
        $user_details = $this->Common_model->fetch_data('users_newentry', '*', $where, false);

        if (count($user_details) > 0) {
            foreach ($user_details as $user) {
                $user_id   = $user['id'];
                $user_name = $user['phone_no'];
                $password  = $user['password'];
                $email  = $user['email'];
                // $email  = 'pradip.nandwana@indianpac.com';

                if ($user_name != '') {
                    $mailData               = [];
                    $mailData['username']   =  $user_name;
                    $mailData['password']   =  $password;
                    $mailData['email']      =  $email;
                    $subject                = "இந்த இரண்டு நிமிடம் உங்களுக்குப் பயனுள்ளதாய் அமையலாம்!";
                    $template               = 'new_user_credentials';
                    sendmail($subject, $mailData, $template);

                    $whereArr['where'] = ['id' => $user_id];
                    $emailsent = array(
                        'is_email_sent' => 1
                    );
                    $this->Common_model->update_single('users_newentry', $emailsent, $whereArr);
                    echo "<pre>";
                    echo "mail sent successfully for: $user_name";
                }
            }
        }
        exit;
    }

    public function addRewardsToRefererUser(){
        $day_diff           = 1;
        $rewardPoints       = 100;

        $usersList          = $this->Cron_model->getReferralUsersTasksList($day_diff);
        echo "<pre>";
        print_r($usersList);//exit;

        if(!empty($usersList)){
            $this->load->library('common_function');
            $this->load->model('User_model');

            $referalList                    = $this->Cron_model->getReferralCreditLogList($day_diff);
            $assocReferalList               = array();
            if(!empty($referalList)){
                $assocReferalList           = $this->common_function->prepareMultipleAssociativeArr($referalList,'referal_user_id','user_id');
            }

            echo "<pre>";
            print_r($assocReferalList);//exit;


            //$this->db->trans_begin(); #DB transaction Start
            foreach ($usersList as $ulKey => $ulVal) {
                $collectedPoints            = '';
                $walletInsertArr            = array();
                $referalCreditInsertArr     = array();
                $refererUserId              = $ulVal['referal_user_id'];
                $registeredUserId           = $ulVal['user_id'];
                if($ulVal['completed_task_count'] >= 3 && empty($assocReferalList[$refererUserId][$registeredUserId])){
                    $collectedPoints        = $rewardPoints + (($ulVal['completed_task_count'] - 3) * 10);
                    //$collectedPoints        = $rewardPoints;
                    $params                 = array();
                    $params['points']       = $collectedPoints;
                    $params['user_id']      = $refererUserId;
                    echo "<pre>";
                    print_r('userid is = ' . $refererUserId);
                    echo "<br/>";
                    print_r('points is = ' . $collectedPoints);
                    echo "<br/>";
                    $updateRes              = $this->User_model->updateUserEarnPoints($params);

                    echo "  User Points updated query ::  <br/>";
                    echo $this->db->last_query();
                    echo "   <br/> -------------------------   <br/>";

                    $userData               = $this->User_model->userDetail($refererUserId);
                    $current_points         = $userData['points_earned'];
                    $whereArr               = ['fk_user_id' => $refererUserId];
                    $user_level             = $this->User_model->getUserLevels($refererUserId);
                    $current_level          = $user_level['fk_level_id'] ? $user_level['fk_level_id'] : '0';
                    print_r('current_level is =' . $current_level);

                    if ($current_level != 'null') {
                        $level_array        = $this->nextlevel($current_points, $current_level);
                        $difference         = $level_array['max_level'] - $current_level;

                        if ($difference < 0) {
                            // nothing to do
                        } else {
                            for ($i = 1; $i <= $difference; $i++) {
                                $fk_level_id                = $current_level + $i;

                                $reward_points              = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                                $reward_points              = $reward_points->row_array();
                                $free_rewards               = $reward_points['free_reward_points'];
                                if ($free_rewards != 0) {
                                    $params                 = array();
                                    $params['points']       = $free_rewards;
                                    $params['user_id']      = $refererUserId;
                                    $res                    = $this->User_model->updateUserEarnPoints($params);

                                    $walletInsertArr[]      = array(
                                        'user_id'           => $refererUserId,
                                        'point'             => $free_rewards,
                                        'title'             => 'New Level Rewards',
                                        'description'       => 'Rewards earned for completion of levels',
                                        'task_id'           => '0',
                                        'type'              => 'bonus',
                                        'status'            => '1',
                                        'created_date'      => date('Y-m-d H:i:s'),
                                        'updated_date'      => date('Y-m-d H:i:s')
                                    );
                                }

                                $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $refererUserId, 'unlock_date' => date('Y-m-d H:i:s')]);
                                echo "   <br/> -------------------------   <br/>";
                                echo "   <br/> -------------------------   <br/>";
                                print_r('levels added successfully for ' . $refererUserId);
                            }
                        }
                        if ($updateRes) {
                            $walletInsertArr[]          = array(
                                'user_id'               => $refererUserId,
                                'point'                 => $collectedPoints,
                                'title'                 => "Star week special",
                                'description'           => "Here are your special points for your hard work!!",
                                //'description'           => "Points earned due to completion of task by ".$ulVal['referal_user'].' on '.date("Y-m-d",strtotime("-".$day_diff." days")),
                                'task_id'               => '0',
                                'type'                  => 'bonus',
                                'status'                => '1',
                                'created_date'          => date('Y-m-d H:i:s'),
                                'updated_date'          => date('Y-m-d H:i:s')
                            );

                            $referalCreditInsertArr[]   = array(
                                'user_id'               => $ulVal['user_id'],
                                'referal_user_id'       => $refererUserId,
                                'is_credited'           => 1,
                                'no_of_tasks_completed' => $ulVal['completed_task_count'],
                                'created_date'          => date('Y-m-d H:i:s')
                            );
                        }
                        print_r($walletInsertArr);//exit;
                        print_r($referalCreditInsertArr);//exit;
                        if (!empty($walletInsertArr) && count($walletInsertArr) > 0) {
                            $this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
                            echo "   <br/> -------------------------   <br/>";
                            echo "   <br/> -------------------------   <br/>";
                            print_r('wallet rewards added successfully for ' . $refererUserId);
                        }

                        if (!empty($referalCreditInsertArr) && count($referalCreditInsertArr) > 0) {
                            $this->Common_model->insert_multiple('referal_credit_point_logs', $referalCreditInsertArr);
                            echo "   <br/> -------------------------   <br/>";
                            echo "   <br/> -------------------------   <br/>";
                            print_r('referal_credit_point_logs added successfully for ' . $refererUserId);
                        }
                        /*if (true === $this->db->trans_status()) {
                            #Comminting changes
                            $this->db->trans_commit();
                            echo "   <br/> -------------------------   <br/>";
                            echo "   <br/> -------------------------   <br/>";
                            echo "Rewards added successfully.";
                        } else {
                            echo "Error While adding rewards.Please try again.";
                        }*/
                    }
                }
            }
        } else{
            echo "   <br/> -------------------------   <br/>";
            echo "   <br/> -------------------------   <br/>";
            echo "Users not found to credit rewards.";
        }          
        
        echo "<br/>-------------------------   <br/>";
        echo "cron completed";         
        exit; 

    }

    public function sendAchievemenLevelTwentyOneOld()
    {
        $userErn = $this->Cron_model->userlevel(21);
        echo $this->db->last_query();
        echo "<pre>";
        print_r($userErn);//exit;
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                $updateUserArray = array('notified' => '1');
                /*$this->Common_model->update_single(
                    'tbl_user_level',
                    $updateUserArray,
                    ['where' => ['pk_id' => $users['pk_id']]]
                );*/
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $mailData['mailerName'] = 'achievement';
                    $subject                = "ஸ்டாலின் அணியுடன் பணியாற்ற உங்களுக்கான வாய்ப்பு!! lvl 21";
                    $mailerName             = 'achievement_twenty_one';
                    $attachments            = array(
                        '0' => $_SERVER["DOCUMENT_ROOT"] . '/public/images/mail/background.PNG',
                        '1' => $_SERVER["DOCUMENT_ROOT"] . '/public/images/Badge.png'
                    );
                    $attachment             =  $_SERVER["DOCUMENT_ROOT"] . '/public/images/mail/header.png';
                    print_r($mailData);
                    print_r($attachments);//exit;
                    sendmailwithattachment($subject, $mailData, $mailerName, $attachment);
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendAchievemenLevelTwentyOne()
    {
        $userErn = $this->Cron_model->userlevel(21);
        echo "<pre>";
        print_r($userErn);
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $mailData['mailerName'] = 'achievement';
                    $subject                = "ஸ்டாலின் அணியுடன் பணியாற்ற உங்களுக்கான வாய்ப்பு!!";
                    $mailerName             = 'achievement_twenty_one';                    
                    print_r($mailData);
                    $sendMail               = sendmail($subject, $mailData, $mailerName);

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendSecondAchievemenForLevelTwentyOne()
    {
        $userErn = $this->Cron_model->userlevel(21,'',1);
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];

                    $inputKeys              = $users['registeration_no'] . '@' . $users['user_id'] . '@21';
                    $inputKeys              = encrypt_decrypt('encrypt', $inputKeys);
                    $mailData['link']       = base_url() . 'generate_certificate/?key=' . $inputKeys;
                   
                    $subject                = "வெற்றி நம் கையில்!";
                    $template               = 'achievement_level_twenty_one_second';

                    $sendMail               = sendMail($subject, $mailData, $template);

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified'      => '2',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                    
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit;
    }

    public function sendLevelTwentyOneForTest()
    {

        $mailData               = [];
        $mailData['name']       = 'Nigam Patro';
        $mailData['email']      = 'epari.patro@indianpac.com';
        //$mailData['mailerName'] = 'achievement';
        $subject                = "இடைச்சங்க பாசறை நிலையைக் கடந்ததற்கு ஸ்டாலின் அணியின் வாழ்த்துக்கள்!";
        $mailerName             = 'achievement_level_seven';                    
        $sendMail               = sendmail($subject, $mailData, $mailerName);

        if ($sendMail) {
            echo "Mail sent successfully";

        }else{
            echo "Error while sending email. Please try again.";
        }
        exit;
    }
    public function livesession_post()
    {
        $url = FCM_URL;
        $serverKey = FCM_SERVER_KEY;
        $where = array('where' => ['is_session_start' => 1]);
        $getSession = $this->Common_model->fetch_data('live_session', '*', $where, true);
        #If Result list is not empty
        if (!empty($getSession)) {
            $response_array = [
                'CODE' => 409,
                'MESSAGE' => 'There is an Activated Live Session',
                'RESULT' => []
            ];
            echo json_encode($response_array);
            exit;
        } else {
        $currentDate = date("Y-m-d ");
        $currentTime = date("H:i:s");
        $sessionEndtime = date('H:i:s',strtotime('+2 hour',strtotime($currentTime)));
        $sessionvideoid = isset($_GET['sessionvideoid']) ? $_GET['sessionvideoid'] : ' ';
        $insertlivesessionArray = array(
            'session_name'            => 'MKS Live',
            'session_video_id'        => $sessionvideoid,
            'session_date'            => $currentDate,
            'session_start_time'      => $currentTime,
            'session_end_time'        => $sessionEndtime,
            'is_session_start'        => 1,
            'total_live_users'        => 0,
        );
        $sucess = $this->Common_model->insert_single('live_session', $insertlivesessionArray);
        $response_sucess_array = [
            'CODE' => SUCCESS_CODE,
            'MESSAGE' => 'success',
            'RESULT' => $insertlivesessionArray
        ];
        echo json_encode($response_sucess_array);
        exit;
        }
    }

    public function sendAchievementlevelone(){
        $userErn = $this->Cron_model->userlevel(1);
       
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    $inputKeys              = $users['registeration_no'] . '@' . $users['user_id'] . '@1';
                   
                    $inputKeys              = encrypt_decrypt('encrypt', $inputKeys);
                    $mailData['link']       = base_url() . 'generate_certificate/?key=' . $inputKeys;


                    $subject                = "Congratulations! You showed up in a big way for the betterment of Goa..";
                    $mailerName             = 'achievement_level_one';                    

                   
                    $sendMail               = sendmail($subject, $mailData, $mailerName);
                    // print_r($sendMail);
                    // die;

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified_level_one'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit; 
    }
    public function sendAchievementleveltwo(){
        $userErn = $this->Cron_model->userlevel(2);
        
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
    
                    $subject                = "Congratulations! Big News and Bigger Thanks...";
                    $mailerName             = 'achievement_level_two';   
                    
                  
                    $sendMail               = sendmail($subject, $mailData, $mailerName);

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit; 
    }
    public function sendAchievementlevelthree(){
        $userErn = $this->Cron_model->userlevel(3);
       
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
                    
                    $subject                = "Congratulations! So pleased to see you accomplish great things...";
                    $mailerName             = 'achievement_level_three';                    
                   
                   
                    $sendMail               = sendmail($subject, $mailData, $mailerName);

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit; 
    }
    public function sendAchievementlevelfour(){
        $userErn = $this->Cron_model->userlevel(4);
        
        if (count($userErn) > 0) {
            foreach ($userErn as $users) {
                if ($users['email_id'] != '') {
                    $mailData               = [];
                    $mailData['name']       =  $users['full_name'];
                    $mailData['email']      =  $users['email_id'];
    
                    $subject                = "Congratulations on your well-deserved success!!";
                    $mailerName             = 'achievement_level_four';   
                    
                   
                    $sendMail               = sendmail($subject, $mailData, $mailerName);

                    if ($sendMail) {
                        $updateUserArray    = array(
                            'notified'      => '1',
                            'notified_date' => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->update_single(
                            'tbl_user_level',
                            $updateUserArray,
                            ['where' => ['pk_id' => $users['pk_id']]]
                        );
                    }
                }
            }
        }
        echo json_encode(array('status' => 'success'));
        exit; 
    }

}
