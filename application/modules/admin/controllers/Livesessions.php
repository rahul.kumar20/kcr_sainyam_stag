<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
class Livesessions extends MY_Controller
{
    private $admininfo  = "";
    private $data       = array();

    public function __construct()
    {
        parent::__construct();
        $this->admininfo            = $this->session->userdata('admininfo');
        $this->data['admininfo']    = $this->admininfo;
        $this->load->model('Common_model');
        $this->load->model('Livesessions_model');
        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
    }

    public function index()
    {
        try 
        { //TRY START
            $this->load->library('Common_function');

            $data['admininfo']  = $this->admininfo;
            $get                = $this->input->get();

            $default            = array(
                "limit"         => 10,
                "page"          => 1,
                "searchlike"    => "",
                "field"         => "",
                "export"        => "",
                "order"         => "",
                "startDate"     => "",
                "endDate"       => "",
            );

            $defaultValue       = defaultValue($get, $default);
            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit']      = EXPORT_LIMIT;
                $defaultValue['offset']     = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset                     = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset']     = $offset;
            } //ELSE 2 END

            $data['contactus'] = $this->Livesessions_model->liveSessionslist($defaultValue);
            $totalrows         = $data['contactus']['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$data['contactus']['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/contactus?data=" . queryStringBuilder($defaultValue));
            }

            /* Sorting Query */
            $getQuery                   = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query']          = "&" . $getQuery;

            $data["order_by_title"]     = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";
            //Default Order by
            $data["order_by"]           = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END
           
            /* paggination */
            $pageurl            = 'admin/livesessions';
            $links              = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name();
            $data["csrfToken"]  = $this->security->get_csrf_hash();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class();
            $data['method']     = $this->router->fetch_method();
            $data['module']     = $this->router->fetch_module();
            $data['startDate']  = $defaultValue['startDate'];
            $data['endDate']    = $defaultValue['endDate'];
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            // echo "<pre>";
            // print_r($data);exit;

            load_views("livesessions/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**add live session start */
    public function addLiveSessions()
    {
        try {

            $data['admininfo']  = $this->admininfo;
            $postData           = $this->input->post();
            
            $this->load->library('Common_function');
           
            if (isset($postData) && !empty($postData)) {
                $this->db->trans_begin(); #DB transaction Start
                
                $insertLiveSessionArray     = array(
                    'session_name'          => $postData['session_name'],
                    'session_video_id'      => $postData['session_video_id'],
                    'session_date'          => !empty($postData['session_date']) ? convert_date_time_format($postData['session_date']) : '',
                    'is_session_start'      => 0,
                    'total_live_users'      => 0
                );

                //insert live session details
                $sessionID                  = $this->Common_model->insert_single('live_session', $insertLiveSessionArray);
                //echo $this->db->last_query();//exit;
                #if transaction runs successfully
                if (true === $this->db->trans_status()) {
                    $this->db->trans_commit();
                    #setting Response Array
                    $this->session->set_flashdata('message_success', 'Live Session added successfully');
                    redirect(base_url() . 'admin/livesessions');
                } else { #IF transaction failed
                    #rolling back
                    $this->db->trans_rollback();
                    #setting Response Array
                }
            }
            $data['admininfo'] = $this->admininfo;
            //load state helper
            load_views("livesessions/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function startSession()
    {
        try {

            $getData                = $this->input->get();
            $data['admininfo']      = $this->admininfo;
            $this->load->library('Common_function');
            
            $where                  = array('where' => ['id' => $getData['id']]);
            $getlivesession         = $this->Common_model->fetch_data('live_session', '*', $where, true);
            if (count($getlivesession) > 0) { 
                $updatelivesessionArray = array('is_session_start' => '1','session_start_time' => date('Y-m-d H:i:s'));
                $this->Common_model->update_single(
                    'live_session',
                    $updatelivesessionArray,
                    ['where' => ['id' => $getData['id']]]
                );
                $this->session->set_flashdata('message_success', 'Session started successfully');
                redirect(base_url() . 'admin/cron/sendSessionNotification');
                
            }
            
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    } 

    public function endSession()
    {
        try {
            
            $getData                = $this->input->get();
            $data['admininfo']      = $this->admininfo;
            $this->load->library('Common_function');
            
            $where                  = array('where' => ['id' => $getData['id']]);
            $getlivesession         = $this->Common_model->fetch_data('live_session', '*', $where, true);
            if (count($getlivesession) > 0) { 
                $updatelivesessionArray = array('is_session_start' => '2','session_end_time' => date('Y-m-d H:i:s'));
                $this->Common_model->update_single(
                    'live_session',
                    $updatelivesessionArray,
                    ['where' => ['id' => $getData['id']]]
                );
                $this->session->set_flashdata('message_success', 'Session ended successfully');
                redirect(base_url() . 'admin/cron/sendSessionEndNotification');
            }
            
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    } 
}
