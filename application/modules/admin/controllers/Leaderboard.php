<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Leaderboard extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Leaderboard_model');

        $this->load->model('Common_model');
        $this->lang->load('common', 'english');

    }


    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "taskPlatform" => ""
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END


            $userList = $this->Leaderboard_model->userToplist($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadLeaderboardList($userList['result']);
            }//IF 3 END

            $totalrows = $userList['total'];
            $data['userlist'] = $userList['result'];

            // Manage Pagination
            $pageurl = 'admin/leaderboard';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['taskType'] = $defaultValue['taskPlatform'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/leaderboard?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
              //state list
            $data['statelist'] = get_state_list();
    //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("leaderboard/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



    /**
     * @function exportNews
     * @description To export news search list
     *
     * @param type $newsData
     */
    private function exportLeaderboard($userData)
    {
        try {
            $fileName = 'userlist' . date('d-m-Y-g-i-h') . '.xlsx';
            $status = array('1' => 'Active', '2' => 'Blocked');

            // The function header by sending raw excel

            header('Content-Disposition: attachment; filename=' . $fileName);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');


            $format = '<table border="1">'
                . '<tr>'
                . '<th width="25%">S.no</th>'
                . '<th>Rank</th>'
                . '<th>UID</th>'
                . '<th>Name</th>'
                . '<th>Rewards Point</th>'
                . '<th>Task Completed</th>'
                . '<th>State</th>'
                . '<th>District</th>'
                . '<th>College</th>'
                . '<th>Status</th>'
                . '</tr>';

            $coun = 1;
            foreach ($userData as $res) {
                $fld_1 = $coun;
                $fld_2 = (isset($res['ranking']) && ($res['ranking'] != '')) ? $res['ranking'] : '';
                $fld_3 = (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '';
                $fld_4 = (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '';
                $fld_5 = (isset($res['rewards']) && ($res['rewards'] != '')) ? $res['rewards'] : '';
                $fld_6 = (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : '0';
                $fld_7 = (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : '';
                $fld_8 = (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : '';
                $fld_9 = (isset($res['college']) && !empty($res['college']) ? $status[$res['college']] : '');
                $fld_10 = (isset($res['is_active']) && !empty($res['is_active']) ? $status[$res['is_active']] : '');

                $format .= '<tr>
                        <td>' . $fld_1 . '</td>
                        <td>' . $fld_2 . '</td>
                        <td>' . $fld_3 . '</td>
                        <td>' . $fld_4 . '</td>
                        <td>' . $fld_5 . '</td>
                        <td>' . $fld_6 . '</td>
                        <td>' . $fld_7 . '</td>
                        <td>' . $fld_8 . '</td>
                        <td>' . $fld_9 . '</td>
                        <td>' . $fld_10 . '</td>
                      </tr>';
                $coun++;
            } //end foreach

            echo $format;
            die;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    public function downloadLeaderboardList($userlist)
    {

    //  $subscribers = $this->phpexcel_model->get_users();

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
              ->setLastModifiedBy('')
              ->setTitle('User List')
              ->setSubject('User List')
              ->setDescription('User List');

        // add style to the header
        $styleArray = array(
              'font' => array(
                      'bold' => true,
              ),
              'alignment' => array(
                      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ),
              'borders' => array(
                      'top' => array(
                              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      ),
              ),
              'fill' => array(
                      'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                      'rotation' => 90,
                      'startcolor' => array(
                              'argb' => 'FFA0A0A0',
                      ),
                      'endcolor' => array(
                              'argb' => 'FFFFFFFF',
                      ),
              ),
        );
        // $spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);


        // auto fit column to content
        // foreach (range('A', 'J') as $columnID) {
            // $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    // ->setAutoSize(true);
        // }
        foreach (range('A', 'H') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue("A1", 'S.no')
              ->setCellValue("B1", 'Ranking')
              ->setCellValue("C1", 'UID')
              ->setCellValue("D1", 'Name')
              ->setCellValue("E1", 'Task Completed')
              ->setCellValue("F1", 'Reward Points')
            //   ->setCellValue("G1", 'State')
              ->setCellValue("G1", 'District')
            //   ->setCellValue("I1", 'College')
              ->setCellValue("H1", 'Status');
 
        // Add some data
        $x= 2;
        $status = array('1' => 'Active', '2' => 'Blocked');
        $count=1;
        foreach ($userlist as $res) {
            $date = date_create($res['registered_on']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A$x", $count)
            ->setCellValue("B$x", (isset($res['ranking']) && ($res['ranking'] != '')) ? $res['ranking'] : '')
            ->setCellValue("C$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '')
            ->setCellValue("D$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '')
            ->setCellValue("E$x", (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : '')
            ->setCellValue("F$x", (isset($res['rewards']) && ($res['rewards'] != '')) ? $res['rewards'] : '')
            // ->setCellValue("G$x", (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : '')
            ->setCellValue("G$x", (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : '')
            // ->setCellValue("I$x", (isset($res['college_name']) && ($res['college_name'] != '')) ? $res['college_name'] : '')
            ->setCellValue("H$x", (isset($res['is_active']) && !empty($res['is_active']) ? $status[$res['is_active']] : ''));
            $x++;
            $count++;
        }



// Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

// set right to left direction
//      $spreadsheet->getActiveSheet()->setRightToLeft(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="leaderboard_list.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
    
    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function taskList()
    {

        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "id"=>'',
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END



            $taskList = $this->Leaderboard_model->userTasklist($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadTaskCompleteUserList($taskList['result']);
            }//IF 3 END

            $totalrows = $taskList['total'];
            $data['tasklist'] = $taskList['result'];

            // Manage Pagination
            $pageurl = 'admin/Leaderboard/taskList';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_completed"] = $data["order_by_enddate"] = $data["order_by_startdate"] = $data["order_by_points"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "completed":
                            $data["order_by_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "point":
                            $data["order_by_points"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['userId'] = $defaultValue['id'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$taskList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/Leaderboard/taskList?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['permission'] = $GLOBALS['permission'];
            //user detail
            $where['where'] = ['user_id' => $defaultValue['id']];

            $data['userDetail'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id','full_name'), $where,true);
            load_views("leaderboard/task_list", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function downloadTaskCompleteUserList($userlist)
    {

    //  $subscribers = $this->phpexcel_model->get_users();

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

// Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
              ->setLastModifiedBy('')
              ->setTitle('User List')
              ->setSubject('User List')
              ->setDescription('User List');

        // add style to the header
        $styleArray = array(
              'font' => array(
                      'bold' => true,
              ),
              'alignment' => array(
                      'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                      'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
              ),
              'borders' => array(
                      'top' => array(
                              'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                      ),
              ),
              'fill' => array(
                      'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                      'rotation' => 90,
                      'startcolor' => array(
                              'argb' => 'FFA0A0A0',
                      ),
                      'endcolor' => array(
                              'argb' => 'FFFFFFFF',
                      ),
              ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'J') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue("A1", 'S.no')
              ->setCellValue("B1", 'UID')
              ->setCellValue("B1", 'Task Id')
              ->setCellValue("C1", 'Task')
              ->setCellValue("D1", 'Points')
              ->setCellValue("E1", 'Completed on')
              ->setCellValue("F1", 'Task Live Date')
              ->setCellValue("G1", 'Deadline Date')
              ->setCellValue("H1", 'Completed By')
              ->setCellValue("I1", 'Task Type')
              ->setCellValue("J1", 'Task For')
              ->setCellValue("K1", 'Status');
 
        // Add some data
        $x= 2;
                  $status = array('1' => 'Completed', '2' => 'Pending');
          $count=1;
        foreach ($userlist as $res) {
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $count)
                    ->setCellValue("B$x", (isset($res['task_code']) && ($res['task_code'] != '')) ? $res['task_code'] : '')
                    ->setCellValue("C$x", (isset($res['task_title']) && ($res['task_title'] != '')) ? $res['task_title'] : '')
                    ->setCellValue("D$x", (isset($res['points']) && ($res['points'] != '')) ? $res['points'] : '')
                    ->setCellValue("E$x", mdate(DATE_FORMAT, strtotime($res['added_on'])))
                    ->setCellValue("F$x", mdate(DATE_FORMAT, strtotime($res['start_date'])))
                    ->setCellValue("G$x", mdate(DATE_FORMAT, strtotime($res['end_date'])))
                    ->setCellValue("H$x", (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : '')
                    ->setCellValue("I$x", (isset($res['task_type']) && ($res['task_type'] != '')) ? $res['task_type'].'('.$res['action'].')' : '')
                    ->setCellValue("J$x", (isset($res['task_type']) && ($res['task_type'] != '')) ? $res['task_type'] : '')
                    ->setCellValue("K$x", (isset($res['status']) && !empty($res['status']) ? $status[$res['status']] : ''));
            $x++;
            $count++;
        }



// Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

// set right to left direction
//      $spreadsheet->getActiveSheet()->setRightToLeft(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="task_completed_user_list.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
