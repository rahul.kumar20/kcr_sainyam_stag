<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AjaxUtil extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("Utility_model");
        $this->load->model("Common_model");
        $this->load->library("session");
        $this->load->library('S3');
        $this->lang->load('common', "english");
        $this->admininfo = $this->session->userdata('admininfo');
        if (! $this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->csrftoken = $this->security->get_csrf_hash();
    }



    /**
     * @function emailExistsAjax
     * @description AJAX Handler for email exists
     */
    public function emailExistsAjax()
    {
        $postData = $this->input->post();
        if (! isset($postData["email"]) || empty($postData["email"])) {
            $errorData = [
                "error"      => true,
                "message"    => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                [ "id" ],
                "users",
                [ "email" => $postData["email"] ]
            )
            ) {
                $errorData = [
                    "error"      => true,
                    "message"    => "Email already in use.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error"      => false,
                    "message"    => "Email available.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    /**
     * @function mobileExistsAjax
     * @description AJAX HANDLER FOR MOBILE NUMBER EXISTS
     */
    public function mobileExistsAjax()
    {
        $postData = $this->input->post();
        if (! isset($postData["mobile_number"]) || empty($postData["mobile_number"])) {
            $errorData = [
                "error"      => true,
                "message"    => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                [ "id" ],
                "users",
                [ "mobile_number" => $postData["mobile_number"] ]
            )
            ) {
                $errorData = [
                    "error"      => true,
                    "message"    => "Mobile number already in use.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error"      => false,
                    "message"    => "Mobile number available.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    /**
     * @function profilePictureUpload
     * @description Profile Picture Upload using Amazon s3 storage
     *
     */
    public function profilePictureUpload()
    {

        $image     = $_FILES['image'];
        $imageSize = getimagesize($image['tmp_name']);

        $validMimeTypes = [ 'image/png', 'image/jpg', 'image/jpeg' ];

        if (! $imageSize || null === $imageSize) {
            $response = [
                "success"    => false,
                "message"    => $this->lang->line("not_an_image"),
                "code"       => NOT_AN_IMAGE,
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($response);
        } else {
        }

        if (! in_array($imageSize['mime'], $validMimeTypes)) {
            $response = [
                "success"    => false,
                "message"    => $this->lang->line("not_an_image"),
                "code"       => NOT_AN_IMAGE,
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($response);
        } else {
        }

        if ($image['size'] > MAX_IMAGE_SIZE) {
            $response = [
                "success"    => false,
                "message"    => $this->lang->line("image_too_big"),
                "code"       => IMAGE_TOO_BIG,
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($response);
        } else {
        }

        $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
        $imageName = "Bonapp_" . time() . "." . $extension;

        $result = $this->Common_model->s3_uplode($imageName, $image['tmp_name']);
        if ($result) {
            $response = [
                "success"    => true,
                "csrf_token" => $this->security->get_csrf_hash(),
                "data"       => $result
            ];
        }
        $this->Utility_model->response($response);
    }



    /**
     * Handles location for google maps
     * https://maps.googleapis.com/maps/api/geocode/json?key=API_KEY&address=appinventiv%20noida
     */
    public function getLocation()
    {
        $postData = $this->input->post();
    }



    /**
     * @function oldpasswordExistsAjax
     * @description AJAX Handler for checking old password
     */
    public function oldpasswordExistsAjax()
    {
        $postData = $this->input->post();
        $id       = encryptDecrypt($postData['userid'], 'decrypt');
        if (( ! isset($postData["oldpassword"]) || empty($postData["oldpassword"]) )) {
            $errorData = [
                "error"      => true,
                "message"    => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];

            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                [ "admin_id" ],
                "admin",
                [ "password" => hash("sha256", base64_decode($postData["oldpassword"])), 'admin_id' => $id ]
            )
            ) {
                $errorData = [
                    "error"      => true,
                    "message"    => "Old password matched.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error"      => false,
                    "message"    => "Old password not matched",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    //check edit mobile number
    public function editmobileExistsAjax()
    {
        $postData = $this->input->post();
        $id       = encryptDecrypt($postData['userid'], 'decrypt');
        if (! isset($postData["mobile_number"]) || empty($postData["mobile_number"])) {
            $errorData = [
                "error"      => true,
                "message"    => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                [ "id" ],
                "users",
                [ "mobile_number" => $postData["mobile_number"], 'id!=' => $id ]
            )
            ) {
                $errorData = [
                    "error"      => true,
                    "message"    => "Mobile number already in use.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error"      => false,
                    "message"    => "Mobile number available.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    /* Check for edit merchant email address */

    /**
     * AJAX Handler for email exists
     */
    public function editemailExistsAjax()
    {
        $postData = $this->input->post();
        $id       = encryptDecrypt($postData['userid'], 'decrypt');

        if (! isset($postData["email"]) || empty($postData["email"])) {
            $errorData = [
                "error"      => true,
                "message"    => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                [ "id" ],
                "users",
                [ "email" => $postData["email"], "id!=" => $id ]
            )
            ) {
                $errorData = [
                    "error"      => true,
                    "message"    => "Email already in use.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error"      => false,
                    "message"    => "Email available.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    /**
     * @function changestatus
     * @description change the status of user to block or unblock
     */
    public function changestatus()
    {
        try {
            $resparr   = array ();
            $userid    = $this->input->post('id');
            $id        = encryptDecrypt($userid, 'decrypt');
            $status    = $this->input->post('is_blocked');
            $table     = 'cs_hf_users';
            $where     = array ( 'where' => array ( 'id' => $id ) );
            $updateArr = array ( 'user_status' => $status );
            $result    = $this->Common_model->update_single('users', $updateArr, $where);
            $csrftoken = $this->security->get_csrf_hash();
            if ($result == true) {
                $resparr = array ( "code" => 200, 'msg' => SUCCESS, "csrf_token" => $csrftoken );
            } else {
                $resparr = array ( "code" => 201, 'msg' => TRY_AGAIN, "csrf_token" => $csrftoken );
            }
            echo json_encode($resparr);
            die;
        } catch (Exception $ex) {
            $resparr = array ( "code" => 201, 'msg' => $ex->getMessage() );
        }
    }



//-----------------------------------------------------------------------------------------
    /**
     * @name getStatesByCountry
     * @description This method is used to get all the states name via country using the get method.
     * @access public
     */
    public function getStatesByCountry()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $req       = $this->input->get();
                $statedata = $this->Common_model->fetch_data('states', 'id,name', [ 'where' => [ 'country_id' => $req['id'] ] ]);
                echo json_encode($statedata);
                exit;
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }



//-----------------------------------------------------------------------------------------
    /**
     * @name getCityByState
     * @description This method is used to get all the cities as per the state using get method.
     * @access public.
     */
    public function getCityByState()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $req      = $this->input->get();
                $citydata = $this->Common_model->fetch_data('cities', 'id,name', [ 'where' => [ 'state_id' => $req['id'] ] ]);
                echo json_encode($citydata);
                exit;
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }



    private function validatePermission($postDataArr)
    {

        $id = encryptDecrypt($postDataArr['id'], 'decrypt');

        if ($postDataArr['new_status'] == 2 || $postDataArr['new_status'] == 1) {
            $permissionType = [ 'blockp' ];
            $key            = 'blockp';
        } else {
            $permissionType = [ 'deletep' ];
            $key            = 'deletep';
        }

        if ($postDataArr['type'] == 'user') {
            $accessPermission = 1;
        } elseif ($postDataArr['type'] == 'version') {
            $accessPermission = 2;
        } else {
            $accessPermission = 3;
        }

        $whereArr          = [];
        $whereArr['where'] = array ( 'admin_id' => $this->admininfo['admin_id'], 'access_permission' => $accessPermission, 'status' => 1 );
        $access_detail     = $this->Common_model->fetch_data('sub_admin', $permissionType, $whereArr, true);
        if (! $access_detail[$key]) {
            $alertMsg['text'] = $this->lang->line('permission_denied');
            $alertMsg['type'] = $this->lang->line('forbidden');
            $resparr          = array ( "code" => 202, 'msg' => $alertMsg, "csrf_token" => $this->csrftoken, 'id' => $id );
            echo json_encode($resparr);
            die;
        } else {
            return true;
        }
    }



//-----------------------------------------------------------------------------------------
    /**
     * @name change-user-status
     * @description This action is used to handle all the block events.
     *
     */
    public function changeUserStatus()
    {
        try {
            $req = $this->input->post();
            #Check for permission on runtime

            if ($this->admininfo['role_id'] != 1) {
                #$this->validatePermission( $req );
            }
            $id = encryptDecrypt($req['id'], 'decrypt');

            #Set alert message according to condition

            $alertMsg         = [];
            $alertMsg['text'] = $this->lang->line('delete_success');
            $alertMsg['type'] = $this->lang->line('success');
            switch ($req['type']) {
                case 'user':
                    $updateArr = [];
                    $updateArr = [ 'is_active' => $req['new_status'] ];
                    $updateId  = $this->Common_model->update_single('users', $updateArr, [ 'where' => [ 'user_id' => $id ] ]);
                    if ($req['new_status'] == DELETED) {
                        $updateArr['field'] = 'email';
                        $updateArr['value'] = 'CONCAT(email,"-","deleted")';
                        $updateId           = $this->Common_model->update_single_withcurrent('users', $updateArr, [ 'where' => [ 'user_id' => $id ] ]);
                    }
                    break;
                case 'cms':
                    $whereArr          = [];
                    $whereArr['where'] = [ 'id' => $id ];
                    $updateId          = $this->Common_model->delete_data('page_master', $whereArr);
                    break;
                case 'version':
                    $whereArr          = [];
                    $whereArr['where'] = [ 'vid' => $id ];
                    $updateId          = $this->Common_model->delete_data('app_version', $whereArr);
                    break;
                case 'notification':
                    $whereArr          = [];
                    $whereArr['where'] = [ 'id' => $id ];
                    $updateId          = $this->Common_model->delete_data('admin_notification', $whereArr);
                    break;
                case 'feedback':
                    $updateId          = $this->Common_model->update_single('student_feedback', [ 'status' => $req['new_status'] ], [ 'where' => [ 'feedback_id' => $userId ] ]);
                    break;
                case 'subscriptions':
                    $updateId          = $this->Common_model->update_single('subscriptions', [ 'status' => $req['new_status'] ], [ 'where' => [ 'subscription_id' => $id ] ]);
                    break;
                case 'post':
                    $updateId          = $this->Common_model->update_single('post', [ 'status' => $req['new_status'] ], [ 'where' => [ 'post_id' => $id ] ]);
                    break;
                case 'user_subscriptions':
                    $updateId          = $this->Common_model->update_single('users_subscriptions', [ 'status' => $req['new_status'] ], [ 'where' => [ 'id' => $id ] ]);
                    break;
                case 'subadmin':
                    $updateArr         = [];
                    $updateArr         = [ 'status' => $req['new_status'] ];
                    $updateId          = $this->Common_model->update_single('admin', $updateArr, [ 'where' => [ 'admin_id' => $id ] ]);
                    $updateId          = $this->Common_model->update_single('sub_admin', $updateArr, [ 'where' => [ 'admin_id' => $id ] ]);
                    if ($req['new_status'] == DELETED) {
                        $updateArr['field'] = 'admin_email';
                        $updateArr['value'] = 'CONCAT(admin_email,"-","deleted")';
                        $updateId           = $this->Common_model->update_single_withcurrent('admin', $updateArr, [ 'where' => [ 'admin_id' => $id ] ]);
                    }
                    break;
            }

            $csrftoken = $this->security->get_csrf_hash();

            if ($updateId) {
                if ($req['new_status'] == 3) {
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                }

                $resparr = array ( "code" => 200, 'msg' => $this->lang->line('success'), "csrf_token" => $csrftoken, 'id' => $id );
            } else {
                $resparr = array ( "code" => 201, 'msg' => $this->lang->line('try_again'), "csrf_token" => $csrftoken, 'id' => $id );
            }
            echo json_encode($resparr);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }



    public function manageSideBar()
    {
        $this->load->helper('cookie');
        $action     = $this->input->post('action');
        set_cookie('sideBar', $action, time() + 3600);
        $csrf_token = $this->security->get_csrf_hash();
        $respArr    = [];
        $respArr    = [ 'code' => 200, 'msg' => 'req success', 'csrf' => $csrf_token ];
        echo json_encode($respArr);
        die;
    }



    public function ajax_post_login()
    {
        $postData = $this->input->post();
        print_r($postData);
        die();
    }



    public function forgot_password()
    {
        try {
            $response_array = [];

            $post_array = $this->input->post();
            $config     = array (
                array (
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email'
                )
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run()) {
                if ($admininfo = $this->get_user_data($post_array)) {
                    $subject                = $this->lang->line('reset_password');
                    $reset_token            = hash('sha256', date("Y-m-d h:i:s"));
                    $timeexpire             = time() + (24 * 60 * 60);
                    $insert['reset_token']  = $reset_token;
                    $insert['timestampexp'] = $timeexpire;
                    $where                  = array ( "where" => array ( 'admin_email' => $admininfo['admin_email'] ) );

                    #transaction Begin
                    $this->db->trans_begin();
                    //update token in DB
                    $update = $this->Common_model->update_single('admin', $insert, $where);

                    //Array to send mail
                    $mailinfoarr               = [];
                    $mailinfoarr['link']       = base_url() . 'admin/reset?token=' . $reset_token;
                    $mailinfoarr['email']      = $admininfo['admin_email'];
                    $mailinfoarr['subject']    = $subject;
                    $mailinfoarr['name']       = $admininfo['admin_name'];
                    $mailinfoarr['mailerName'] = 'forgot';

                    //sending email
                    $this->load->library("Common_function");
                    $isSuccess = $this->common_function->sendEmailToUser($mailinfoarr);


                    if (! $isSuccess) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    if ($isSuccess) {
                        $response_array = [
                            'code'   => SUCCESS_CODE,
                            'msg'    => $this->lang->line('email_link_sent'),
                            'result' => []
                        ];
                    }

                    #Checking transaction Status
                    if (true === $this->db->trans_status()) {
                        #Commiting transation
                        $this->db->trans_commit();

                        #setting response
                        $response_array = [
                            'code'   => SUCCESS_CODE,
                            'msg'    => $this->lang->line('email_link_sent'),
                            'result' => []
                        ];
                    }
                } else {
                    $response_array = [
                        'code'   => SUCCESS_CODE,
                        'msg'    => $this->lang->line('email_not_exists'),
                        'result' => []
                    ];
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);

                #setting response
                $response_array = [
                    'code'   => SUCCESS_CODE,
                    'msg'    => $arr[0],
                    'result' => []
                ];
            }
            echo json_encode($response_array);
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'code'   => SUCCESS_CODE,
                'msg'    => $error,
                'result' => []
            ];
            echo json_encode($response_array);
        }
    }



    private function get_user_data($post_array)
    {
        $response  = false;
        $admininfo = $this->Common_model->fetch_data('admin', 'admin_email, admin_name', array ( 'where' => array ( 'admin_email' => $post_array['email'] ) ), true);
        if (! empty($admininfo) && is_array($admininfo)) {
            $response = $admininfo;
        }
        return $response;
    }

    /**
     *
     * @function uid_import
     * @description uid import
     */
    public function uid_import()
    {
        try {
            $response_array = [];
      # import csv file library
            $this->load->library('csvimport');
            $file_data = $this->csvimport->get_array($_FILES['file']['tmp_name']);
            $csrf_token = $this->security->get_csrf_hash();

            if (!empty($file_data)) {
                foreach ($file_data as $row) {
                    $row = array_values($row);
                    $registrationArray[] = $row[0];
                }
                $commaList = implode(',', $registrationArray);
                $response_array = [
                    'code'   => SUCCESS_CODE,
                    'msg'    => $this->lang->line('import_success'),
                    'result' => $commaList,
                    'csrf_token' => $csrf_token
                ];
            } else {
                #setting response
                $response_array = [
                    'code'   => FAILURE_CODE,
                    'msg'    => $this->lang->line('something_went_Worng'),
                    'result' => [],
                    'csrf_token' => $csrf_token
                ];
            }
            echo json_encode($response_array);
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'code'   => FAILURE_CODE,
                'msg'    => $error,
                'result' => []
            ];
            echo json_encode($response_array);
        }
    }
	
	/**
     * @description: save profile photo
     *
     * @param string cropped image raw data
     *
     * @return string
     */
    public function save_profile_photo()
    {
		 $this->load->helper(array('s3', 'video_thumb'));
        $random = 'profile'.rand(111111111, 999999999999);
        $random_file = "$random.jpg";

        //$_POST[data][1] has the base64 encrypted binary codes.
        //convert the binary to image using file_put_contents
        $img = str_replace(' ', '+', $_POST['data']);
        $data = base64_decode($img);
		
        $savefile = file_put_contents("public/temp/$random_file", $data);
		$imageUrl = base_url().'public/temp/'.$random_file;
        // $savefile = @file_put_contents("'public/web/enquiry/$random.jpg", base64_decode(explode(",", $_POST['data'])[1]));
        //if the file saved properly, print the file name
        if ($savefile) {
           
            echo $imageUrl;
        }
    }
}
