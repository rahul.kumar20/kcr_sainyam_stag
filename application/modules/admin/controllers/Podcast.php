<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Podcast extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('podcast_model');

        $this->load->model('Common_model');
        $this->lang->load('common', 'english');
    }


    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try { //TRY START
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;

            $get = $this->input->get();

            $default      = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "export" => "",
                "field"      => "",
                "order"      => "",
               
            );

            $defaultValue = defaultValue($get, $default);
           //If Request if Excel Export then restrict to 65000 limit
           if ($defaultValue['export']) { //IF 2 START
            $defaultValue['limit'] = EXPORT_LIMIT;
            $defaultValue['offset'] = 0;
        } //IF 2 END
        else { //ELSE 2 START
            $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
            $defaultValue['offset'] = $offset;
        } //ELSE 2 END

            $data['podcastlist'] = $this->podcast_model->getpodcastdetails($defaultValue);
            $totalrows         = $data['podcastlist']['total'];
            // print_r($data);exit;

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$data['podcastlist']['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/podcast?data=" . queryStringBuilder($defaultValue));
            }

            /* Sorting Query */
            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_title"] = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END
           
            /* paggination */
            $pageurl            = 'admin/podcast';
            $links              = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name();
            $data["csrfToken"]  = $this->security->get_csrf_hash();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class();
            $data['method']     = $this->router->fetch_method();
            $data['module']     = $this->router->fetch_module();
           if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            load_views("podcast/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    public function addpodcast()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
            $data['admininfo'] = $this->admininfo;
            $this->load->library('Common_function');
            $postdata = $this->input->post();
             if (isset($postdata) && !empty($postdata)) {
                $document_audio_url = '';
                if (isset($_FILES['audio']['name']) && !empty($_FILES['audio']['name'])) {
                    $audio = array();
                    $allowed = array('mp3', 'wav');
                    $filename = $_FILES['audio']['name'];

                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (in_array($ext, $allowed)) {
                        if (!empty($_FILES['audio']['name'])) {
                            $audio['audio']['name'] = $_FILES['audio']['name'];
                            $audio['audio']['type'] = $_FILES['audio']['type'];
                            $audio['audio']['tmp_name'] = $_FILES['audio']['tmp_name'];
                            $audio['audio']['error'] = $_FILES['audio']['error'];
                            $audio['audio']['size'] = $_FILES['audio']['size'];

                            //upload in s3 server
                            $document_audio_url = upload_image_s3($audio['audio'], '');
                        }
                    }
                }
                $document_image_url = '';
                if (isset($_FILES['logo_image']['name']) && !empty($_FILES['logo_image']['name'])) {
                    $logo_image = array();
                    $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                    $filename = $_FILES['logo_image']['name'];

                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (in_array($ext, $allowed)) {
                        if (!empty($_FILES['logo_image']['name'])) {
                            $logo_image['logo_image']['name'] = $_FILES['logo_image']['name'];
                            $logo_image['logo_image']['type'] = $_FILES['logo_image']['type'];
                            $logo_image['logo_image']['tmp_name'] = $_FILES['logo_image']['tmp_name'];
                            $logo_image['logo_image']['error'] = $_FILES['logo_image']['error'];
                            $logo_image['logo_image']['size'] = $_FILES['logo_image']['size'];

                            //upload in s3 server
                            $document_image_url = upload_image_s3($logo_image['logo_image'], '');
                        }
                    }
                }

                $insertpodcastArray = array(
                    'title'       => !empty($this->input->post('title')) ? $this->input->post('title') : '',
                    'title_tn'    => !empty($this->input->post('title_tn')) ? $this->input->post('title_tn') : '',
                    'logo'        => $document_image_url,
                    'url'         => $document_audio_url,
                    'status'      => !empty($this->input->post('status')) ? $this->input->post('status') : '',
                    'schedule_date'           => !empty($this->input->post('scheduleDate')) ? date('Y-m-d H:i:s', strtotime($this->input->post('scheduleDate'))) : '',
                    'notification_title'      => !empty($this->input->post('noti_title')) ? $this->input->post('noti_title') : '',
                    'notification_message'    => !empty($this->input->post('noti_message')) ? $this->input->post('noti_message') : '',
                  );
               
                $podcast = $this->Common_model->insert_single('podcast', $insertpodcastArray);
                $this->session->set_flashdata('podcast_success', 'Podcast has been created successfully');
                redirect(base_url() . 'admin/podcast');
            }
            load_views("podcast/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
