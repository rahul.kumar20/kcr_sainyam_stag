<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Usercontactus extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->load->model('Contactus_model');
        $this->lang->load('common', "english");
    }



    /**
     * @name index
     * @description This method is used to list all the customers.
     */
    public function index()
    {

        try { //TRY START
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;

            $get = $this->input->get();

            $default      = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "export" => "",
                "order"      => "",
                "startDate" => "",
                "endDate" => "",
            );

            $defaultValue = defaultValue($get, $default);
            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END

            $data['contactus'] = $this->Contactus_model->contactuslist($defaultValue);
            $totalrows         = $data['contactus']['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$data['contactus']['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/contactus?data=" . queryStringBuilder($defaultValue));
            }

            /* Sorting Query */
            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_title"] = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($data['contactus']['result']);
            } //IF 3 END

            /* paggination */
            $pageurl            = 'admin/contactus';
            $links              = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name();
            $data["csrfToken"]  = $this->security->get_csrf_hash();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class();
            $data['method']     = $this->router->fetch_method();
            $data['module']     = $this->router->fetch_module();
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            load_views("contactus/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function detail()
    {
        try {
            $post = $this->input->post();
            $this->form_validation->set_rules($this->config->item("reply_btn"));
            $this->load->helper('sendsms');
            if (isset($post) && !empty($post)) {
                $phone   = $_POST['phone_no'];
                $message = $_POST['reply'];
                sendMessage($phone, $message);
                $this->session->set_flashdata('message_success', "Your Reply Sent Successfully");
                redirect(base_url() . 'admin/contactus');
            } else {
                $get = $this->input->get();
                $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/contactus/");
                $data = array();
                $data['admininfo'] =    $this->admininfo;
                $data['user_id'] = $userId;
                //get user profile data
                $data['contactDetail'] = $this->Contactus_model->contactUsDetail($userId);
                if (empty($data['contactDetail'])) { //IF START
                    show404($this->lang->line('no_user'), "/admin/users/");
                    return 0;
                } //IF END

                load_views("contactus/detail", $data);
            }
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function addCategory()
    {
        try { //TRY START
            $postedData        = $this->input->post();
            $data['admininfo'] = $this->admininfo;

            if (count($postedData)) { //IF 1
                $this->form_validation->set_rules('title', $this->lang->line('title'), 'required|trim');
                $this->form_validation->set_rules('title_tn', $this->lang->line('title_tn'), 'required|trim');
                $this->form_validation->set_rules('status', $this->lang->line('status'), 'required|trim');

                if ($this->form_validation->run()) { //if 2
                    $savedata['category_name']         = $postedData['title'];
                    $savedata['category_name_tn']         = $postedData['title_tn'];
                    $savedata['status']       = $postedData['status'];
                    $savedata['inserted_on'] = DEFAULT_DB_DATE_TIME_FORMAT;

                    // calling to insert data method.
                    $res = $this->saveCmsData($savedata);

                    $alertMsg = [];
                    if ($res) { //IF 3
                        $alertMsg['text'] = $this->lang->line('category_add_success');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } //IF 3 END
                    else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } //ELSE END

                    redirect('/admin/category');
                } //if END
            } //IF 1 END
            else {
                // CSRF token
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            } //ELSE END
            load_views("contactus/add", $data);
            //TRY END
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     * @name saveCmsData
     * @descrition To insert/update CMS page data description.
     *
     * @param type $this->data
     * @return boolean
     */
    private function saveCmsData($data, $updateId = false)
    {

        try { //TRY START
            $this->db->trans_start(); //transaction Start

            if ($updateId) { //IF 1
                $this->Common_model->update_single('contact_us_category', $data, ['where' => ['id' => $updateId]]);
            } //IF 1 END
            else {
                $this->Common_model->insert_single('contact_us_category', $data);
            } //ELSE END


            if (true === $this->db->trans_status()) { //IF 2
                //Commiting Trasaction
                $this->db->trans_commit();
                return true;
            } //IF 2 END
            else {
                //Trasaction Rollback
                $this->db->trans_rollback();
                return false;
            } //ELSE END
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } //CATCH END
    }


    /**
     * @name index
     * @description This method is used to list all the customers.
     */
    public function category()
    {

        try { //TRY START
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;

            $get = $this->input->get();

            $default      = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "order"      => "",
                "startDate" => "",
                "endDate" => "",
                "export" => ""
            );
            $defaultValue = defaultValue($get, $default);


            $data['page']   = $defaultValue['page'];
            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END

            $data['category'] = $this->Contactus_model->contactuscategorylist($defaultValue);
            $totalrows        = $data['category']['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$data['category']['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/category?data=" . queryStringBuilder($defaultValue));
            }


            /* Sorting Query */
            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_title"] = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END

            /* paggination */
            $pageurl            = 'admin/category';
            $links              = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name();
            $data["csrfToken"]  = $this->security->get_csrf_hash();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class();
            $data['method']     = $this->router->fetch_method();
            $data['module']     = $this->router->fetch_module();
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            load_views("contactus/category", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function download($subscribers)
    {
        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('Contact Us List')
            ->setSubject('Contact Us List')
            ->setDescription('Contact Us List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'F') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.No')
            ->setCellValue("B1", 'Ticket Id')
            ->setCellValue("C1", 'User Name')
            ->setCellValue("D1", 'Category Name')
            ->setCellValue("E1", 'Contact Us Content')
            ->setCellValue("F1", 'Created Date');

        // Add some data
        $x = 2;
        $y = 1;

        foreach ($subscribers as $res) {
            $ticket_id = !empty($res['ticket_id']) ? $res['ticket_id'] : 'Not availavle';
            $contact_us_content = !empty($res['contact_us_content']) ? $res['contact_us_content'] : "Not Available";
            $date = date_create($res['inserted_on']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $y)
                ->setCellValue("B$x", $ticket_id)
                ->setCellValue("C$x", $res['full_name'])
                ->setCellValue("D$x", $res['category_name'])
                ->setCellValue("E$x", $contact_us_content)
                ->setCellValue("F$x", $Date . ' ' . $Time);
            $x++;
            $y++;
        }


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('News List');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="news_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
