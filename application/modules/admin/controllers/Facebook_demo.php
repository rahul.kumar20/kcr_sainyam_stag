<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'composer/vendor/facebook/graph-sdk/src/Facebook/autoload.php';


class Facebook_Demo extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->load->helper('email');
    }

    
    public function index()
    {
        $fb = new Facebook\Facebook([
           'app_id' => '118100505417819',
           'app_secret' => 'cda9273cc7b35b3a4e6f11ce53c259ff',
           'default_graph_version' => 'v2.12',
        ]);
       
 

        $helper = $fb->getRedirectLoginHelper();
        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }

        try {
            if (isset($_SESSION['fb_access_token'])) {
                $accessToken = $_SESSION['fb_access_token'];
            } else {
                $accessToken = $helper->getAccessToken();
            }
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(Router::url('/', true) . 'taskaction', $permissions);

            return $this->redirect($loginUrl);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(Router::url('/', true) . 'taskaction', $permissions);

            return $this->redirect($loginUrl);
        }

        if (isset($accessToken)) {
            if (isset($_SESSION['fb_access_token'])) {
                $fb->setDefaultAccessToken($_SESSION['fb_access_token']);
            } else {
                $_SESSION['fb_access_token'] = (string)$accessToken;
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['fb_access_token']);
                $_SESSION['fb_access_token'] = (string)$longLivedAccessToken;
                $fb->setDefaultAccessToken($_SESSION['fb_access_token']);
            }

            // validating the access token
            try {
                $request = $fb->get('/me?fields=id,first_name,last_name,gender,email');
                $graphObject = $request->getGraphObject();
                $fbid = $graphObject->getProperty('id');              // To Get Facebook ID
                $fbfirstname = $graphObject->getProperty('first_name'); // To Get Facebook first name
                $fblastname = $graphObject->getProperty('last_name'); // To Get Facebook last name
                $femail = $graphObject->getProperty('email');    // To Get Facebook email ID

                $user_Data = TableRegistry::get("tbl_facebook_user");
                $tbl_user = $user_Data->find('list', [
                'keyField' => 'id',
                'valueField' => 'email'
                ]);

                $this->set('tbl_user', $tbl_user);
                $this->set('fb_user_email', $femail);
                $this->set('fb_user_id', $fbid);
                $this->set('fb_user_fname', $fbfirstname);
                $this->set('fb_user_lname', $fblastname);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                if ($e->getCode() == 190) {
                    unset($_SESSION['fb_access_token']);
                    $helper = $fb->getRedirectLoginHelper();

                    $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
                    $loginUrl = $helper->getLoginUrl(Router::url('/', true) . 'taskaction', $permissions);
                    return $this->redirect($loginUrl);
                }
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                $this->Flash->success(__('Facebook SDK returned an error: ' . $e->getMessage()));
                return $this->redirect(['controller' => 'TblTaskMaster', 'action' => 'index']);
            }
        } else {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
            $loginUrl = $helper->getLoginUrl($permissions);
            pr($loginUrl);
            return redirect($loginUrl);
        }
    }

    public function checklikes()
    {
        // $connection = ConnectionManager::get('default');
        // Time::setToStringFormat('yyyy-MM-dd HH:mm:ss');

        $fb = new Facebook\Facebook([
            'app_id' => '118100505417819',
            'app_secret' => 'cda9273cc7b35b3a4e6f11ce53c259ff',
            'default_graph_version' => 'v2.12',
        ]);

        $helper = $fb->getRedirectLoginHelper();
        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }

        try {
            if (isset($_SESSION['fb_access_token'])) {
                $accessToken = $_SESSION['fb_access_token'];
            } else {
                $accessToken = $helper->getAccessToken();
            }
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
            $loginUrl = $helper->getLoginUrl($permissions);

            $data = array('status' => false, 'redirect' => true, 'link' => $loginUrl);
            echo json_encode($data);
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(Router::url('/', true) . 'taskaction', $permissions);

            $data = array('status' => false, 'redirect' => true, 'link' => $loginUrl);
            echo json_encode($data);
            exit;
        }

        $accessToken = 'EAABraWqsWFsBAINkuZAqnwxUauZAW7amAxTt5R86XkGmaBd1SU93iMYctq3Ql0doQtV9tPgsovbN69cBMXWHNS3BlyjuuLWncZBnqfh0y3JKRlatN1UdLTZBfKU8GBqNIZAO91GPChDHkCRER8pz9hVP7AXiC8hECdw2pcnZAC2akQQmSZAdXGIfzSMzU39zooKr2VAMZBfsUQZDZD';
        if (isset($accessToken)) {
            if (isset($_SESSION['fb_access_token'])) {
                $fb->setDefaultAccessToken($_SESSION['fb_access_token']);
            } else {
                $_SESSION['fb_access_token'] = (string)$accessToken;
                // OAuth 2.0 client handler
                $oAuth2Client = $fb->getOAuth2Client();
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['fb_access_token']);
                $_SESSION['fb_access_token'] = (string)$longLivedAccessToken;
                $fb->setDefaultAccessToken($_SESSION['fb_access_token']);
            }

            // validating the access token
            try {
                $request = $fb->get('/me?fields=id,first_name,last_name,gender,email,accounts');
                $graphObject = $request->getGraphObject();
                $fbid = $graphObject->getProperty('id');              // To Get Facebook ID
                $_SESSION['fb_id'] = (string)$fbid;
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                if ($e->getCode() == 190) {
                    unset($_SESSION['fb_id']);
                    unset($_SESSION['fb_access_token']);
                    $helper = $fb->getRedirectLoginHelper();

                    $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
                    $loginUrl = $helper->getLoginUrl(Router::url('/', true) . 'taskaction', $permissions);
                    $data = array('status' => false, 'redirect' => true, 'link' => $loginUrl);
                    echo json_encode($data);
                    exit;
                }
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                $data = array('status' => false, 'redirect' => false, 'message' => 'Your token has been expired. Please login again.');
                echo json_encode($data);
                exit;
            }
            if ($request) {
                $accounts = $graphObject->getProperty('accounts');
                if (!empty($accounts)) {
                    foreach ($accounts as $account) {
                        $account = $account->asArray();
                        if ($account['id']) {
                            $posts_request = $fb->get('/' .'143115386374592' .  '/likes?limit=50000', $account['access_token']);
                            $total_posts = array();
                            $posts_response = $posts_request->getGraphEdge();
                            date_default_timezone_set('Asia/Kolkata');
                            $now = date("Y-m-d H:i:s");
                            $dv_facebook_ids = array();
                            $facebook_likes_ids = array();
                            pr($posts_request);

                            if ($fb->next($posts_response)) {
                                $response_array = $posts_response->asArray();
                                $total_posts = array_merge($total_posts, $response_array);
                                while ($posts_response = $fb->next($posts_response)) {
                                    $response_array = $posts_response->asArray();
                                    $total_posts = array_merge($total_posts, $response_array);
                                }
                                if (count($total_posts) > 0) {
                                    $getTasks = $connection->execute("SELECT dv.vfacebook_id FROM tbl_task_assign_details AS task_assign
                                              LEFT JOIN tbl_digital_volunteer_master AS dv ON dv.pk_user_id = task_assign.fk_user_id
                                              WHERE task_assign.fk_task_id= '" . $this->request->data['id'] . "' AND (task_assign.vtaskstatus != '1' OR task_assign.vtaskstatus IS NULL)")->fetchAll('assoc');

                                    foreach ($getTasks as $v) {
                                        if (!empty($v['vfacebook_id'])) {
                                            $dv_facebook_ids[] = $v['vfacebook_id'];
                                        }
                                    }
                                    foreach ($total_posts as $post) {
                                        if (!empty($post['id'])) {
                                            $post_id = $post['id'];
                                            $facebook_likes_ids[] = $post_id;
                                        }
                                    }
                                }
                            } else {
                                $posts_response = $posts_request->getGraphEdge()->asArray();
                                if (count($posts_response) > 0) {
                                    $getTasks = $connection->execute("SELECT dv.vfacebook_id FROM tbl_task_assign_details AS task_assign
                                              LEFT JOIN tbl_digital_volunteer_master AS dv ON dv.pk_user_id = task_assign.fk_user_id
                                              WHERE task_assign.fk_task_id= '" . $this->request->data['id'] . "' AND (task_assign.vtaskstatus != '1' OR task_assign.vtaskstatus IS NULL)")->fetchAll('assoc');

                                    foreach ($getTasks as $v) {
                                        if (!empty($v['vfacebook_id'])) {
                                            $dv_facebook_ids[] = $v['vfacebook_id'];
                                        }
                                    }

                                    foreach ($posts_response as $post) {
                                        if (!empty($post['id'])) {
                                            $post_id = $post['id'];
                                            $facebook_likes_ids[] = $post_id;
                                        }
                                    }
                                }
                            }
                            $FINAL_FB_IDS = array_intersect($dv_facebook_ids, $facebook_likes_ids);
                            if (!empty($FINAL_FB_IDS)) {
                                foreach ($FINAL_FB_IDS as $ID) {
                                    $getTasks = $connection->execute("SELECT task_assign.* FROM tbl_task_assign_details AS task_assign
                                                  LEFT JOIN tbl_digital_volunteer_master AS dv
                                                  ON dv.pk_user_id = task_assign.fk_user_id
                                                  WHERE dv.vfacebook_id = '" . $ID . "' AND task_assign.fk_task_id= '" . $this->request->data['id'] . "' AND (task_assign.vtaskstatus != '1' OR task_assign.vtaskstatus IS NULL)")->fetchAll('assoc');
                                    if (count($getTasks)) {
                                        $select_reward_points = $connection->execute("SELECT vrewardspoints,vtask_deadline,(case when (vtask_deadline > '" . $now . "')
                                                    THEN
                                                          'yes'
                                                     ELSE
                                                          'no'
                                                     END) AS deadline FROM tbl_task_master WHERE pk_task_id = '" . $this->request->data['id'] . "'")->fetch('assoc');
                                        if ($select_reward_points['deadline'] == 'yes') {
                                            $reward_points = $select_reward_points['vrewardspoints'];
                                            $getReward = $connection->execute("SELECT * FROM tbl_user_wallet WHERE fk_user_id = '" . $getTasks[0]['fk_user_id'] . "' AND MONTH(dmonth_start_date) = MONTH(CURRENT_DATE()) AND YEAR(dmonth_start_date) = YEAR(CURRENT_DATE()) ORDER BY pk_earning_id DESC LIMIT 1")->fetchAll('assoc');
                                            if (count($getReward) > 0) {
                                                //UPDATE
                                                $new_rewards = $getReward[0]['total_erned_points'] + $reward_points;
                                                $connection->update('tbl_user_wallet', ['total_erned_points' => $new_rewards], ['pk_earning_id' => $getReward[0]['pk_earning_id'], 'fk_user_id' => $getTasks[0]['fk_user_id']]);
                                            } else {
                                                $month_start_date = date('Y-m-01');
                                                $month_end_date = date('Y-m-t');
                                                $connection->insert('tbl_user_wallet', ['fk_user_id' => $getTasks[0]['fk_user_id'], 'dmonth_start_date' => $month_start_date, 'dmonth_end_date' => $month_end_date, 'total_erned_points' => $reward_points]);
                                            }
                                        }
                                        $connection->update('tbl_task_assign_details', ['vtaskstatus' => 1, 'taskCompleteDate' => Time::now()], ['fk_user_id' => $getTasks[0]['fk_user_id'], 'fk_task_id' => $this->request->data['id']]);
                                    }
                                }
                            }
                        }
                    }
                    $data = array('status' => true, 'redirect' => false, 'message' => 'Updated successfully.');
                    echo json_encode($data);
                    exit;
                } else {
                    $data = array('status' => false, 'redirect' => false, 'message' => 'Page permission is required for selected facebook ID');
                    echo json_encode($data);
                    exit;
                }
            } else {
                try {
                    $POST_ID = $_SESSION['fb_id'] . '_' . $this->request->data['task_id'];
 
                    $posts_request = $fb->get('/' . $POST_ID . '/likes?limit=50000');
                    $total_posts = array();
                    $posts_response = $posts_request->getGraphEdge();
                   
                    date_default_timezone_set('Asia/Kolkata');
                    $now = date("Y-m-d H:i:s");
                    $dv_facebook_ids = array();
                    $facebook_likes_ids = array();
                    if ($fb->next($posts_response)) {
                        $response_array = $posts_response->asArray();
                        $total_posts = array_merge($total_posts, $response_array);
                        while ($posts_response = $fb->next($posts_response)) {
                            $response_array = $posts_response->asArray();
                            $total_posts = array_merge($total_posts, $response_array);
                        }

                        if (count($total_posts) > 0) {
                            $getTasks = $connection->execute("SELECT dv.vfacebook_id FROM tbl_task_assign_details AS task_assign
                                              LEFT JOIN tbl_digital_volunteer_master AS dv ON dv.pk_user_id = task_assign.fk_user_id
                                              WHERE task_assign.fk_task_id= '" . $this->request->data['id'] . "' AND (task_assign.vtaskstatus != '1' OR task_assign.vtaskstatus IS NULL)")->fetchAll('assoc');

                            foreach ($getTasks as $v) {
                                if (!empty($v['vfacebook_id'])) {
                                    $dv_facebook_ids[] = $v['vfacebook_id'];
                                }
                            }

                            foreach ($total_posts as $post) {
                                if (!empty($post['id'])) {
                                    $post_id = $post['id'];
                                    $facebook_likes_ids[] = $post_id;
                                }
                            }
                        }
                    } else {
                        $posts_response = $posts_request->getGraphEdge()->asArray();
                        if (count($posts_response) > 0) {
                            $getTasks = $connection->execute("SELECT dv.vfacebook_id FROM tbl_task_assign_details AS task_assign
                                              LEFT JOIN tbl_digital_volunteer_master AS dv ON dv.pk_user_id = task_assign.fk_user_id
                                              WHERE task_assign.fk_task_id= '" . $this->request->data['id'] . "' AND (task_assign.vtaskstatus != '1' OR task_assign.vtaskstatus IS NULL)")->fetchAll('assoc');

                            foreach ($getTasks as $v) {
                                if (!empty($v['vfacebook_id'])) {
                                    $dv_facebook_ids[] = $v['vfacebook_id'];
                                }
                            }
                            foreach ($posts_response as $post) {
                                if (!empty($post['id'])) {
                                    $post_id = $post['id'];
                                    $facebook_likes_ids[] = $post_id;
                                }
                            }
                        }
                    }
                    
                    $FINAL_FB_IDS = array_intersect($dv_facebook_ids, $facebook_likes_ids);
                    if (!empty($FINAL_FB_IDS)) {
                        foreach ($FINAL_FB_IDS as $ID) {
                            $getTasks = $connection->execute("SELECT task_assign.* FROM tbl_task_assign_details AS task_assign
                                                  LEFT JOIN tbl_digital_volunteer_master AS dv
                                                  ON dv.pk_user_id = task_assign.fk_user_id
                                                  WHERE dv.vfacebook_id = '" . $ID . "' AND task_assign.fk_task_id= '" . $this->request->data['id'] . "' AND (task_assign.vtaskstatus != '1' OR task_assign.vtaskstatus IS NULL)")->fetchAll('assoc');
                            if (count($getTasks)) {
                                $select_reward_points = $connection->execute("SELECT vrewardspoints,vtask_deadline,(case when (vtask_deadline > '" . $now . "')
                                                    THEN
                                                          'yes'
                                                     ELSE
                                                          'no'
                                                     END) AS deadline FROM tbl_task_master WHERE pk_task_id = '" . $this->request->data['id'] . "'")->fetch('assoc');
                                if ($select_reward_points['deadline'] == 'yes') {
                                    $reward_points = $select_reward_points['vrewardspoints'];
                                    $getReward = $connection->execute("SELECT * FROM tbl_user_wallet WHERE fk_user_id = '" . $getTasks[0]['fk_user_id'] . "' AND MONTH(dmonth_start_date) = MONTH(CURRENT_DATE()) AND YEAR(dmonth_start_date) = YEAR(CURRENT_DATE()) ORDER BY pk_earning_id DESC LIMIT 1")->fetchAll('assoc');
                                    if (count($getReward) > 0) {
                                        //UPDATE
                                        $new_rewards = $getReward[0]['total_erned_points'] + $reward_points;
                                        $connection->update('tbl_user_wallet', ['total_erned_points' => $new_rewards], ['pk_earning_id' => $getReward[0]['pk_earning_id'], 'fk_user_id' => $getTasks[0]['fk_user_id']]);
                                    } else {
                                        $month_start_date = date('Y-m-01');
                                        $month_end_date = date('Y-m-t');
                                        $connection->insert('tbl_user_wallet', ['fk_user_id' => $getTasks[0]['fk_user_id'], 'dmonth_start_date' => $month_start_date, 'dmonth_end_date' => $month_end_date, 'total_erned_points' => $reward_points]);
                                    }
                                }
                                $connection->update('tbl_task_assign_details', ['vtaskstatus' => 1, 'taskCompleteDate' => Time::now()], ['fk_user_id' => $getTasks[0]['fk_user_id'], 'fk_task_id' => $this->request->data['id']]);
                            }
                        }
                    }
                    $data = array('status' => true, 'redirect' => false, 'message' => 'Updated successfully.');
                    echo json_encode($data);
                    exit;
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    $data = array('status' => false, 'redirect' => false, 'message' => 'Graph returned an error: ' . $e->getMessage());
                    echo json_encode($data);
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    $data = array('status' => false, 'redirect' => false, 'message' => 'Facebook SDK returned an error: ' . $e->getMessage());
                    echo json_encode($data);
                    exit;
                }
            }
        } else {
            $helper = $fb->getRedirectLoginHelper();
            $permissions = ['email,user_likes,user_posts,publish_actions,manage_pages']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(Router::url('/', true) . 'taskaction', $permissions);
            $data = array('status' => false, 'redirect' => true, 'link' => $loginUrl);
            echo json_encode($data);
            exit;
        }
    }
}
