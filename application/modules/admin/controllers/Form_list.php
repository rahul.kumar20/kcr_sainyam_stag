<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
require_once APPPATH . "/libraries/phpqrcode/qrlib.php";

class Form_list extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();
        $this->load->helper(array('s3', 'video_thumb'));

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Form_model');

        $this->lang->load('common', 'english');
        #import form validation confog file
        $this->load->config("form_validation_admin");
     
    }

    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            

            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                'taskType' => "",
                'taskAct' => ""

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END



            $formList = $this->Form_model->formList($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->download($formList['result']);
            }//IF 3 END

            $totalrows = $formList['total'];
            $data['formlist'] = $formList['result'];
            $defaultValue['taskType'] = !empty($defaultValue['taskType']) ? $defaultValue['taskType'] : '';
            $data['taskTotalData'] = $totalrows;
            // Manage Pagination
            $pageurl = 'admin/form_list';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_completed"] = $data["order_by_enddate"] = $data["order_by_startdate"] = $data["order_by_points"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "completed":
                            $data["order_by_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "point":
                            $data["order_by_points"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['taskType'] = $defaultValue['taskType'];

            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            $data['GENDER_TYPE'] =unserialize(GENDER_TYPE);

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$formList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/form_list?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['permission'] = $GLOBALS['permission'];
              //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("form/form_listing", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function formUserDetails()
    {
        try {
            $get = $this->input->get();
            $formId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/form_list/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['form_id'] = $formId;
            //get user profile data
            $data['limit'] = 10;
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "LivestartDate" => "",
                "LiveendDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id" => "",
                'completeStatus' => COMPLETE
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit']; 
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END


            $formUserList = $this->Form_model->formUserList($formId,$defaultValue);
            # print_r ( $this->common_function );
            

            $totalrows = $formUserList['total'];
            //print_r($totalrows); exit;
            $data['formUserList'] = $formUserList['result'];
            $data['totalrows'] = $totalrows;
            // Manage Pagination
            $pageurl = 'admin/form_list/formUserDetails';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['form_id'] = $defaultValue['id'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

          
            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$formUserList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/form_list/formUserDetails?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            // $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("form/formUserdetails", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function formSubmitDetails()
    {
        try {
            $get = $this->input->get();
            $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/form_list/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $userId;
            //get user profile data
            $this->load->library('common_function');
            
            $formSubmitDetails = $this->Form_model->formSubmitDetails($userId);
            $totalrows = $formSubmitDetails['total'];
            $data['formSubmitDetails'] = $formSubmitDetails['result'];
            $dataResponse['totalrows'] = $totalrows;
            $dataResponse = $data['formSubmitDetails'];
            foreach ( $dataResponse as $key => $value){
               if(!empty($value['description']))
               {
                $dataResponse[$key]['answer']= $value['description'];
               }else{
                $dataResponse[$key]['answer']= $value['answer']; 
               }
            } 
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $dataResponse['formSubmitDetails'] = $dataResponse;
            $data['permission'] = $GLOBALS['permission'];
            load_views("form/formSubmitDetails", $dataResponse);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

}
