<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';

class Ajax_Util extends MX_Controller
{

    public function __construct()
    {
        $this->load->model("Utility_model");
        $this->load->model("Common_model");
        $this->load->model("User_model");
        $this->load->library("session");
        $this->load->library('S3');
        $this->lang->load('common', "english");
        $this->admininfo = $this->session->userdata('admininfo');
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->csrftoken = $this->security->get_csrf_hash();
    }



    /**
     * @function emailExistsAjax
     * @description AJAX Handler for email exists
     */
    public function emailExistsAjax()
    {
        $getData = $this->input->get();
        if (!isset($getData["email"]) || empty($getData["email"])) {
            echo 'false';
            die;
        } else {
            if ($userData = $this->Utility_model->fetchData(
                ["user_id"],
                "users",
                ["email_id" => $getData["email"]]
            )) {
                echo 'false';
                die;
            } else {
                echo 'true';
                die;
            }
        }
    }



    /**
     * @function mobileExistsAjax
     * @description AJAX HANDLER FOR MOBILE NUMBER EXISTS
     */
    public function mobileExistsAjax()
    {
        $getData = $this->input->get();
        if (!isset($getData["phoneno"]) || empty($getData["phoneno"])) {
            echo 'false';
            die;
        } else {
            if ($userData = $this->Utility_model->fetchData(
                ["user_id"],
                "users",
                ["phone_number" => $getData["phoneno"]]
            )) {
                echo 'false';
                die;
            } else {
                echo 'true';
                die;
            }
        }
    }

    /**
     * @function mobileExistsAjax
     * @description AJAX HANDLER FOR MOBILE NUMBER EXISTS
     */
    public function whatsUpNumberExistsAjax()
    {
        $getData = $this->input->get();
        if (!isset($getData["whatsupno"]) || empty($getData["whatsupno"])) {
            echo 'false';
            die;
        } else {
            if ($userData = $this->Utility_model->fetchData(
                ["user_id"],
                "users",
                ["whatsup_number" => $getData["whatsupno"]]
            )) {
                echo 'false';
                die;
            } else {
                echo 'true';
                die;
            }
        }
    }

    /**
     * @function profilePictureUpload
     * @description Profile Picture Upload using Amazon s3 storage
     *
     */
    public function profilePictureUpload()
    {

        $image = $_FILES['image'];
        $imageSize = getimagesize($image['tmp_name']);

        $validMimeTypes = ['image/png', 'image/jpg', 'image/jpeg'];

        if (!$imageSize || null === $imageSize) {
            $response = [
                "success" => false,
                "message" => $this->lang->line("not_an_image"),
                "code" => NOT_AN_IMAGE,
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($response);
        } else {
        }

        if (!in_array($imageSize['mime'], $validMimeTypes)) {
            $response = [
                "success" => false,
                "message" => $this->lang->line("not_an_image"),
                "code" => NOT_AN_IMAGE,
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($response);
        } else {
        }

        if ($image['size'] > MAX_IMAGE_SIZE) {
            $response = [
                "success" => false,
                "message" => $this->lang->line("image_too_big"),
                "code" => IMAGE_TOO_BIG,
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($response);
        } else {
        }

        $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
        $imageName = "RCC_" . time() . "." . $extension;

        $result = $this->Common_model->s3_uplode($imageName, $image['tmp_name']);
        if ($result) {
            $response = [
                "success" => true,
                "csrf_token" => $this->security->get_csrf_hash(),
                "data" => $result
            ];
        }
        $this->Utility_model->response($response);
    }



    /**
     * Handles location for google maps
     * https://maps.googleapis.com/maps/api/geocode/json?key=API_KEY&address=appinventiv%20noida
     */
    public function getLocation()
    {
        $postData = $this->input->post();
    }



    /**
     * @function oldpasswordExistsAjax
     * @description AJAX Handler for checking old password
     */
    public function oldpasswordExistsAjax()
    {
        $postData = $this->input->post();
        $id = encryptDecrypt($postData['userid'], 'decrypt');
        if ((!isset($postData["oldpassword"]) || empty($postData["oldpassword"]))) {
            $errorData = [
                "error" => true,
                "message" => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];

            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                ["admin_id"],
                "admin",
                ["password" => hash("sha256", base64_decode($postData["oldpassword"])), 'admin_id' => $id]
            )) {
                $errorData = [
                    "error" => true,
                    "message" => "Old password matched.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error" => false,
                    "message" => "Old password not matched",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    //check edit mobile number
    public function editmobileExistsAjax()
    {
        $postData = $this->input->post();
        $id = encryptDecrypt($postData['userid'], 'decrypt');
        if (!isset($postData["mobile_number"]) || empty($postData["mobile_number"])) {
            $errorData = [
                "error" => true,
                "message" => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                ["id"],
                "users",
                ["mobile_number" => $postData["mobile_number"], 'id!=' => $id]
            )) {
                $errorData = [
                    "error" => true,
                    "message" => "Mobile number already in use.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error" => false,
                    "message" => "Mobile number available.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    /* Check for edit merchant email address */

    /**
     * AJAX Handler for email exists
     */
    public function editemailExistsAjax()
    {
        $postData = $this->input->post();
        $id = encryptDecrypt($postData['userid'], 'decrypt');

        if (!isset($postData["email"]) || empty($postData["email"])) {
            $errorData = [
                "error" => true,
                "message" => "fields are not set",
                "csrf_token" => $this->security->get_csrf_hash()
            ];
            $this->Utility_model->response($errorData);
        } else {
            if ($userData = $this->Utility_model->fetchData(
                ["id"],
                "users",
                ["email" => $postData["email"], "id!=" => $id]
            )) {
                $errorData = [
                    "error" => true,
                    "message" => "Email already in use.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            } else {
                $errorData = [
                    "error" => false,
                    "message" => "Email available.",
                    "csrf_token" => $this->security->get_csrf_hash()
                ];
                $this->Utility_model->response($errorData);
            }
        }
    }



    /**
     * @function changestatus
     * @description change the status of user to block or unblock
     */
    public function changestatus()
    {
        try {
            $resparr = array();
            $userid = $this->input->post('id');
            $id = encryptDecrypt($userid, 'decrypt');
            $status = $this->input->post('is_blocked');
            $table = 'cs_hf_users';
            $where = array('where' => array('id' => $id));
            $updateArr = array('user_status' => $status);
            $result = $this->Common_model->update_single('users', $updateArr, $where);
            $csrftoken = $this->security->get_csrf_hash();
            if ($result == true) {
                $resparr = array("code" => 200, 'msg' => SUCCESS, "csrf_token" => $csrftoken);
            } else {
                $resparr = array("code" => 201, 'msg' => TRY_AGAIN, "csrf_token" => $csrftoken);
            }
            echo json_encode($resparr);
            die;
        } catch (Exception $ex) {
            $resparr = array("code" => 201, 'msg' => $ex->getMessage());
        }
    }



    //-----------------------------------------------------------------------------------------
    /**
     * @name getStatesByCountry
     * @description This method is used to get all the states name via country using the get method.
     * @access public
     */
    public function getStatesByCountry()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $req = $this->input->get();
                $statedata = $this->Common_model->fetch_data('states', 'id,name', ['where' => ['country_id' => $req['id']]]);
                echo json_encode($statedata);
                exit;
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }



    //-----------------------------------------------------------------------------------------
    /**
     * @name getCityByState
     * @description This method is used to get all the cities as per the state using get method.
     * @access public.
     */
    public function getCityByState()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $req = $this->input->get();

                $citydata = $this->Common_model->fetch_data('cities', 'id,name', ['where' => ['state_id' => $req['id']]]);
                echo json_encode($citydata);
                exit;
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }

    /**
     * @name getDistrictByState
     * @description This method is used to get all the district name via state using the get method.
     * @access public
     */
    public function getDistrictByState()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $csrftoken = $this->security->get_csrf_hash();
                $req = $this->input->post();
                if (isset($req['id']) && !empty($req['id'])) {
                    $statedata = $this->Common_model->fetch_data('district', 'district_code as district_id,district_name', ['where' => ['state_id' => $req['id']]]);
                    $resparr = array("code" => 200, "csrf_token" => $csrftoken, "value" => $statedata);
                    echo json_encode($resparr);
                    exit;
                } else {
                    $resparr = array();
                    $resparr = array("code" => 200, "csrf_token" => $csrftoken, 'value' => []);
                    echo json_encode($resparr);
                    exit;
                }
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }
    /**
     * @name getcollegeByDistrict
     * @description This method is used to get all the college name via district using the get method.
     * @access public
     */
    public function getcollegeByDistrict()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $csrftoken = $this->security->get_csrf_hash();

                $req = $this->input->post();
                $whereArr = [];
                $whereArr['where'] = ['district_id' => $req['id']];
                $whereArr['order_by'] = ['college_name' => 'ASC'];
                $collegedata = $this->Common_model->fetch_data('college_list', 'college_id,college_name', $whereArr);


                $resparr = array("code" => 200, "csrf_token" => $csrftoken, "value" => $collegedata);
                echo json_encode($resparr);
                exit;
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }
    private function validatePermission($postDataArr)
    {

        $id = encryptDecrypt($postDataArr['id'], 'decrypt');

        if ($postDataArr['new_status'] == 2 || $postDataArr['new_status'] == 1) {
            $permissionType = ['blockp'];
            $key = 'blockp';
        } else {
            $permissionType = ['deletep'];
            $key = 'deletep';
        }

        if ($postDataArr['type'] == 'user') {
            $accessPermission = 1;
        } elseif ($postDataArr['type'] == 'version') {
            $accessPermission = 2;
        } else {
            $accessPermission = 3;
        }

        $whereArr = [];
        $whereArr['where'] = array('admin_id' => $this->admininfo['admin_id'], 'access_permission' => $accessPermission, 'status' => 1);
        $access_detail = $this->Common_model->fetch_data('sub_admin', $permissionType, $whereArr, true);
        if (!$access_detail[$key]) {
            $alertMsg['text'] = $this->lang->line('permission_denied');
            $alertMsg['type'] = $this->lang->line('forbidden');
            $resparr = array("code" => 202, 'msg' => $alertMsg, "csrf_token" => $this->csrftoken, 'id' => $id);
            echo json_encode($resparr);
            die;
        } else {
            return true;
        }
    }



    //-----------------------------------------------------------------------------------------
    /**
     * @name change-user-status
     * @description This action is used to handle all the block events.
     *
     */
    public function changeUserStatus()
    {
        try {
            $req = $this->input->post();

            #Check for permission on runtime

            if ($this->admininfo['role_id'] != 1) {
                #$this->validatePermission( $req );
            }
            $id = encryptDecrypt($req['id'], 'decrypt');

            #Set alert message according to condition

            $alertMsg = [];
            $alertMsg['text'] = $this->lang->line('delete_success');
            $alertMsg['type'] = $this->lang->line('success');
            switch ($req['type']) {
                case 'user':
                    $updateArr = [];
                    if ($req['new_status'] != DELETED) {
                        $alertMsg['text'] = $this->lang->line('block_success');
                        if ($req['new_status'] == ACTIVE) {
                            $alertMsg['text'] = $this->lang->line('unblocked_success');
                        }
                        $updateArr = ['is_active' => $req['new_status']];
                        $updateId = $this->Common_model->update_single('users', $updateArr, ['where' => ['user_id' => $id]]);
                    } else {
                        $whereArr = [];
                        $whereArr['where'] = ['user_id' => $id];
                        $updateId = $this->Common_model->delete_data('users', $whereArr);
                    }


                    break;
                case 'news_comment':
                    $updateArr          = [];
                    $updateArr          = ['is_deleted' => '1'];
                    $where['or_where']  = array(
                        'id'            => $id,
                        'parent_id'     => $id,
                    );
                    $updateId           = $this->Common_model->update_single('ipac_news_comments', $updateArr, $where);
                    break;
                case 'news':
                    $whereArr = [];
                    $whereArr['where'] = ['news_id' => $id];
                    $updateId = $this->Common_model->delete_data('ipac_news', $whereArr);
                    break;
                case 'cms':
                    $whereArr = [];
                    $whereArr['where'] = ['id' => $id];
                    $updateId = $this->Common_model->delete_data('page_master', $whereArr);
                    break;
                case 'contactcategory':
                    $whereArr = [];
                    $whereArr['where'] = ['category_id' => $id];
                    $updateId = $this->Common_model->delete_data('contact_us_category', $whereArr);
                    break;
                case 'term':
                    $whereArr = [];
                    $whereArr['where'] = ['id' => $id];
                    $updateId = $this->Common_model->delete_data('ipac_term_condition', $whereArr);
                    break;
                case 'event':
                    $whereArr = [];
                    $whereArr['where'] = ['event_id' => $id];
                    $updateId = $this->Common_model->delete_data('ipac_event', $whereArr);
                    break;                        
                case 'faq':
                    $whereArr = [];
                    $whereArr['where'] = ['id' => $id];
                    $updateId = $this->Common_model->delete_data('faq_management', $whereArr);
                    break;
                case 'homefaq':
                    $whereArr = [];
                    $whereArr['where'] = ['id' => $id];
                    $updateId = $this->Common_model->delete_data('faq_management_home_screen', $whereArr);
                    break;
                case 'poll':
                    $whereArr = [];
                    $whereArr['where'] = ['poll_id ' => $id];
                    $updateId = $this->Common_model->delete_data('ic_poll', $whereArr);
                    $this->Common_model->delete_data('ic_poll_answer', $whereArr);
                    $this->Common_model->delete_data('ic_poll_options', $whereArr);
                    break;
                case 'level':
                    $whereArr = [];
                    $whereArr['where'] = ['pk_level_id' => $id];
                    $updateId = $this->Common_model->delete_data('tbl_levels', $whereArr);
                    break;
                case 'achievement':
                    $whereArr = [];
                    $whereArr['where'] = ['pk_achievement_id' => $id];
                    $updateId = $this->Common_model->delete_data('tbl_achievement', $whereArr);
                    break;
                case 'version':
                    $whereArr = [];
                    $whereArr['where'] = ['vid' => $id];
                    $updateId = $this->Common_model->delete_data('app_version', $whereArr);
                    break;
                case 'form':
                    $whereArr = [];
                    $whereArr['where'] = ['fid' => $id];
                    $updateId = $this->Common_model->delete_data('forms', $whereArr);
                    break;
                case 'notification':
                    $whereArr = [];
                    $whereArr['where'] = ['id' => $id];
                    $updateId = $this->Common_model->delete_data('admin_notification', $whereArr);
                    break;
                case 'task':
                    $whereArr = [];
                    $whereArr['where'] = ['task_id' => $id];
                    $updateId = $this->Common_model->delete_data('ipac_task_master', $whereArr);
                    break;
                case 'userlog':
                    $whereArr = [];
                    $whereArr['where'] = ['log_id' => $id];
                    $updateId = $this->Common_model->delete_data('user_session_logs', $whereArr);
                    break;
                case 'profileupdateaccept':
                    $this->load->model("Notification_model");

                    $updateArr = [];
                    $updateArr = ['requested_for_edit_profile' => PROFILE_EDITED_ZERO, 'is_profile_edited' => PROFILE_EDITED_ZERO];
                    $updateId = $this->Common_model->update_single('users', $updateArr, ['where' => ['user_id' => $id]]);
                    $userDetail = $this->Notification_model->sendNotification($id);
                    if (isset($userDetail) && !empty($userDetail)) {
                        $message = [
                            "default" => $this->lang->line('request_accept_not'),
                            "APNS_SANDBOX" => json_encode([
                                "aps" => [
                                    "alert" => array(
                                        "title" => $this->lang->line('request_accept_not'),
                                        "body" => $this->lang->line('request_accept_not')
                                    ),
                                    "sound" => "default",
                                    "mutable-content" => 1,
                                    "badge" => 1,
                                    "data" => array(
                                        'notification_id' => '',
                                        'news_id' => '',
                                        "task_id" => '',
                                        "content_type" => "image",
                                        "type" => PROFILE_ACCEPT_NOTIFICATION
                                    )
                                ]
                            ]),
                            "GCM" => json_encode([
                                "data" => [
                                    "title" => $this->lang->line('request_accept_not'),
                                    "message" => $this->lang->line('request_accept_not'),
                                    "body" => $this->lang->line('request_accept_not'),
                                    "type" => PROFILE_ACCEPT_NOTIFICATION,
                                    "image" => '',
                                    'news_id' => '',
                                    'task_id' => '',
                                    'notification_id' => '',

                                ]
                            ])
                        ];
                        //sns object
                        $sns = new Snspush();
                        //send message to filtered users or selected users
                        $result = $sns->asyncPublish2($userDetail, $message);
                    }
                    $insertNewsNoti = array(
                        'user_id' => $id,
                        'news_id' => 0,
                        'task_id' => 0,
                        'notification_type' => 'profile_request',
                        'notification_text' => $this->lang->line('request_accept_not'),
                        'inserted_on' => date('Y-m-d H:i:s')
                    );
                    $updateId = $this->Common_model->insert_single('user_notification', $insertNewsNoti);
                    $alertMsg['text'] = $this->lang->line('request_accept');
                    $alertMsg['type'] = $this->lang->line('success');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    break;
                case 'profileupdatereject':
                    $updateArr = [];
                    $updateArr = ['requested_for_edit_profile' => PROFILE_REJECT_BY_ADMIN, 'is_profile_edited' => PROFILE_NOT_EDITED];
                    $updateId = $this->Common_model->update_single('users', $updateArr, ['where' => ['user_id' => $id]]);
                    $alertMsg['text'] = $this->lang->line('request_reject');
                    $alertMsg['type'] = $this->lang->line('success');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    break;
            }

            $csrftoken = $this->security->get_csrf_hash();

            if ($updateId) {
                if ($req['new_status'] == DELETED) {
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                } else {
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                }

                $resparr = array("code" => 200, 'msg' => $this->lang->line('success'), "csrf_token" => $csrftoken, 'id' => $id);
            } else {
                $resparr = array("code" => 201, 'msg' => $this->lang->line('try_again'), "csrf_token" => $csrftoken, 'id' => $id);
            }
            echo json_encode($resparr);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }



    public function manageSideBar()
    {
        $this->load->helper('cookie');
        $action = $this->input->post('action');
        set_cookie('sideBar', $action, time() + 3600);
        $csrf_token = $this->security->get_csrf_hash();
        $respArr = [];
        $respArr = ['code' => 200, 'msg' => 'req success', 'csrf' => $csrf_token];
        echo json_encode($respArr);
        die;
    }



    public function ajax_post_login()
    {
        $postData = $this->input->post();
        print_r($postData);
        die();
    }



    public function forgot_password()
    {
        try {
            $response_array = [];

            $post_array = $this->input->post();
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email'
                )
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run()) {
                if ($admininfo = $this->get_user_data($post_array)) {
                    $subject = $this->lang->line('reset_password');
                    $reset_token = hash('sha256', date("Y-m-d h:i:s"));
                    $timeexpire = time() + (24 * 60 * 60);
                    $insert['reset_token'] = $reset_token;
                    $insert['timestampexp'] = $timeexpire;
                    $where = array("where" => array('admin_email' => $admininfo['admin_email']));

                    #transaction Begin
                    $this->db->trans_begin();
                    //update token in DB
                    $update = $this->Common_model->update_single('admin', $insert, $where);

                    //Array to send mail
                    $mailinfoarr = [];
                    $mailinfoarr['link'] = base_url() . 'admin/reset?token=' . $reset_token;
                    $mailinfoarr['email'] = $admininfo['admin_email'];
                    $mailinfoarr['subject'] = $subject;
                    $mailinfoarr['name'] = $admininfo['admin_name'];
                    $mailinfoarr['mailerName'] = 'forgot';

                    //sending email
                    $this->load->library("Common_function");
                    $isSuccess = $this->common_function->sendEmailToUser($mailinfoarr);

                    if (!$isSuccess) {
                        throw new Exception($this->lang->line('try_again'));
                    }

                    if ($isSuccess) {
                        $response_array = [
                            'code' => SUCCESS_CODE,
                            'msg' => $this->lang->line('email_link_sent'),
                            'result' => []
                        ];
                    }

                    #Checking transaction Status
                    if (true === $this->db->trans_status()) {
                        #Commiting transation
                        $this->db->trans_commit();

                        #setting response
                        $response_array = [
                            'code' => SUCCESS_CODE,
                            'msg' => $this->lang->line('email_link_sent'),
                            'result' => []
                        ];
                    }
                } else {
                    $response_array = [
                        'code' => 201,
                        'msg' => $this->lang->line('sorry_ivalid_email'),
                        'result' => []
                    ];
                }
            } else {
                $err = $this->form_validation->error_array();
                $arr = array_values($err);

                #setting response
                $response_array = [
                    'code' => SUCCESS_CODE,
                    'msg' => $arr[0],
                    'result' => []
                ];
            }
            echo json_encode($response_array);
        } catch (Exception $e) {
            $this->db->trans_rollback();
            $error = $e->getMessage();

            #setting response
            $response_array = [
                'code' => SUCCESS_CODE,
                'msg' => $error,
                'result' => []
            ];
            echo json_encode($response_array);
        }
    }



    private function get_user_data($post_array)
    {
        $response = false;
        $admininfo = $this->Common_model->fetch_data('admin', 'admin_email, admin_name', array('where' => array('admin_email' => $post_array['email'])), true);
        if (!empty($admininfo) && is_array($admininfo)) {
            $response = $admininfo;
        }
        return $response;
    }

    /**
     * @name check_category
     * @description This action is used to check if category exists in database.
     *
     */
    public function check_category()
    {
        $get = $this->input->get();
        $response = false;
        $category = (isset($get['category_name']) && !empty($get['category_name'])) ? strtolower(trim($get['category_name'])) : "";

        $category_id = (isset($get['category']) && !empty($get['category'])) ? strtolower(trim($get['category'])) : "";

        $where['where'] = ['category' => $category, 'status' => ACTIVE];

        if (!empty($category_id)) {
            $where['where_not_in']['id'] = $category_id;
        }

        $check_category = $this->Common_model->fetch_data('category', 'id', $where, true);

        #setting response
        $response_array = ['code' => TRY_AGAIN_CODE];

        if (!empty($check_category) && is_array($check_category)) {
            $response_array = ['code' => SUCCESS_CODE];
        }
        echo json_encode($response_array);
    }

    /**
     * @name acceptUserRequest
     * @description This action is used to handle all the request events.
     *
     */
    public function acceptUserRequest()
    {
        try {
            $req = $this->input->post();

            #Check for permission on runtime

            if ($this->admininfo['role_id'] != 1) {
                #$this->validatePermission( $req );
            }
            $id = encryptDecrypt($req['id'], 'decrypt');
            //$updateId = 0;
            #Set alert message according to condition
            $alertMsg = [];
            $alertMsg['text'] = $this->lang->line('delete_success');
            $alertMsg['type'] = $this->lang->line('success');
            #start transaction
            //$this->db->trans_begin();
            switch ($req['type']) {
                case 'profileupdateaccept':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $whereArr = array('where' => array('user_id' => $id));
                    $user_change_info = $this->Common_model->fetch_data(
                        'user_profile_change_request',
                        [
                            'user_id', 'full_name', 'email_id', 'user_image', 'status',
                            'upi_address', 'account_name', 'account_number', 'ifsc_code', 'facebook_id',
                            'facebook_username', 'twitter_id', 'twitter_username', 'whatsup_number'
                        ],
                        $whereArr,
                        true
                    );
                    $user_info = $this->Common_model->fetch_data(
                        'users',
                        [
                            'user_id', 'full_name', 'email_id', 'user_image', 'upi_address',
                            'account_name', 'account_number', 'ifsc_code', 'twitter_id', 'twitter_username', 'facebook_id', 'fb_username', 'whatsup_number'
                        ],
                        $whereArr,
                        true
                    );
                    if (!empty($user_info)) {
                        $updateArr = [];
                        $updateArr = array(
                            'requested_for_edit_profile' => PROFILE_EDITED_ZERO,
                            'is_profile_edited' => PROFILE_EDITED_ZERO,
                            'full_name' => !empty($user_change_info['full_name']) ? $user_change_info['full_name'] : $user_info['full_name'],
                            'email_id' => !empty($user_change_info['email_id']) ? $user_change_info['email_id'] : $user_info['email_id'],
                            'user_image' => !empty($user_change_info['user_image']) ? $user_change_info['user_image'] : $user_info['user_image'],
                            'updated_at' => datetime(),
                            'twitter_id' => !empty($user_change_info['twitter_id']) ? $user_change_info['twitter_id'] : $user_info['twitter_id'],
                            'twitter_username' => !empty($user_change_info['twitter_username']) ? $user_change_info['twitter_username'] : $user_info['twitter_username'],
                            'fb_username' => !empty($user_change_info['facebook_username']) ? $user_change_info['facebook_username'] : $user_info['fb_username'],
                            'facebook_id' => !empty($user_change_info['facebook_id']) ? $user_change_info['facebook_id'] : $user_info['facebook_id'],
                            'whatsup_number' => !empty($user_change_info['whatsup_number']) ? $user_change_info['whatsup_number'] : $user_info['whatsup_number'],
                        );
                        $updateId = $this->Common_model->update_single('users', $updateArr, ['where' => ['user_id' => $id]]);
                        $userDetail = $this->Notification_model->sendNotification($id);
                        if (isset($userDetail) && !empty($userDetail)) {
                            $message = [
                                "default" => $this->lang->line('request_accept_not'),
                                "APNS_SANDBOX" => json_encode([
                                    "aps" => [
                                        "alert" => array(
                                            "title" => $this->lang->line('request_accept_not'),
                                            "body" => $this->lang->line('request_accept_not')
                                        ),
                                        "sound" => "default",
                                        "mutable-content" => 1,
                                        "badge" => 1,
                                        "data" => array(
                                            'notification_id' => '',
                                            'news_id' => '',
                                            "task_id" => '',
                                            "content_type" => "image",
                                            "type" => PROFILE_ACCEPT_NOTIFICATION
                                        )
                                    ]
                                ]),
                                "GCM" => json_encode([
                                    "data" => [
                                        "title" => $this->lang->line('request_accept_not'),
                                        "message" => $this->lang->line('request_accept_not'),
                                        "body" => $this->lang->line('request_accept_not'),
                                        "type" => PROFILE_ACCEPT_NOTIFICATION,
                                        "image" => '',
                                        'news_id' => '',
                                        'task_id' => '',
                                        'notification_id' => '',

                                    ]
                                ])
                            ];
                            //sns object
                            $sns = new Snspush();
                            //send message to filtered users or selected users
                            $result = $sns->asyncPublish2($userDetail, $message);
                        }
                        //send message to filtered users or selected users
                        $insertNewsNoti = array(
                            'user_id' => $id,
                            'news_id' => 0,
                            'task_id' => 0,
                            'notification_type' => 'profile_request',
                            'notification_text' => $this->lang->line('request_accept_not'),
                            'inserted_on' => date('Y-m-d H:i:s')
                        );
                        $updateId = $this->Common_model->insert_single('user_notification', $insertNewsNoti);
                    }
                    if ($updateId) {
                        $whereArray = [];
                        $whereArray['where'] = ['user_id' => $id];
                        $updateId = $this->Common_model->delete_data('user_profile_change_request', $whereArray);
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }

                    break;
                case 'profileupdatereject':
                    $updateArr = [];
                    $updateArr = ['requested_for_edit_profile' => PROFILE_REJECT_BY_ADMIN, 'is_profile_edited' => PROFILE_NOT_EDITED];
                    $updateId = $this->Common_model->update_single('users', $updateArr, ['where' => ['user_id' => $id]]);
                    $whereArray = [];
                    $whereArray['where'] = ['user_id' => $id];
                    $updateId = $this->Common_model->delete_data('user_profile_change_request', $whereArray);
                    $alertMsg['text'] = $this->lang->line('request_reject');
                    $alertMsg['type'] = $this->lang->line('success');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    break;
                case 'expenseaccept':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $whereArr = array('where' => array('expence_id' => $id));
                    $expense_info = $this->Common_model->fetch_data(
                        'user_campaign_expences',
                        ['expence_id', 'user_id', 'task_id', 'total_amount'],
                        $whereArr,
                        true
                    );

                    if (!empty($expense_info)) {
                        $whereArray = array('where' => array('user_id' => $expense_info['user_id']));
                        $user_info = $this->Common_model->fetch_data(
                            'users',
                            ['user_id', 'total_earning'],
                            $whereArray,
                            true
                        );
                        if (!empty($user_info)) {
                            //update user total earning
                            $updateUserArr = array(
                                'total_earning' => ($user_info['total_earning'] + $expense_info['total_amount']),
                                'updated_at' => datetime(),
                                'last_expense_date' => datetime(),
                            );
                            //update user table
                            $updateId = $this->Common_model->update_single('users', $updateUserArr, ['where' => ['user_id' => $expense_info['user_id']]]);
                            //update wallet expense history
                            //update user total earning
                            $updateWalletArr = array(
                                'status' => COMPLETE,
                                'updated_date' => datetime(),
                            );
                            //update user table
                            $updateId = $this->Common_model->update_single('ipac_user_wallet_history', $updateWalletArr, ['where' => ['task_id' => $expense_info['expence_id']], 'type' => 'expense', 'status' => PENDING]);
                        }
                        $updateArr = [];
                    }
                    if ($updateId) {
                        $whereArray = [];
                        $whereArray['where'] = ['expence_id' => $id];
                        $updExpenseStatus = array(
                            'status' => EXPENSE_APPROVED,
                            'updated_date' => datetime()
                        );
                        $updateId = $this->Common_model->update_single('user_campaign_expences', $updExpenseStatus, $whereArray);

                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }

                    break;
                case 'taskcompleterequest':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $task_id = encryptDecrypt($req['tid'], 'decrypt');
                    $whereArr = array('where' => array('task_id' => $task_id));
                    $task_info = $this->Common_model->fetch_data(
                        'ipac_task_master',
                        ['task_id', 'task_type', 'action', 'points'],
                        $whereArr,
                        true
                    );
                    if (!empty($task_info)) {
                        $whereArray = array('where' => array('user_id' => $id, 'task_id' => $task_info['task_id'],'status' => PENDING));
                        $user_task_complete = $this->Common_model->fetch_data(
                            'user_task_master',
                            ['detail_id'],
                            $whereArray,
                            true
                        );
                        if (!empty($user_task_complete)) {
                            //update user total earning
                            $updateUserArr = array(
                                'status' => COMPLETE,
                            );
                            //update user table
                            $updateId = $this->Common_model->update_single('user_task_master', $updateUserArr, ['where' => ['user_id' => $id, 'task_id' => $task_info['task_id']]]);
                            //update wallet expense history
                            //update user total earning

                            if ($updateId) {
                               $whereArray = array('where' => array('user_id' => $id));
                                    $user_info = $this->Common_model->fetch_data(
                                        'users',
                                        ['user_id', 'total_earning', 'points_earned', 'task_completed'],
                                        $whereArray,
                                        true
                                    );
                                    if (!empty($user_info)) {
                                        //update user total earning
                                        $updateUserArr = array(
                                            'total_earning' => ($user_info['total_earning'] + $task_info['points']),
                                            'points_earned' => ($user_info['total_earning'] + $task_info['points']),
                                            'updated_at' => datetime(),
                                            'last_earning_update' => datetime(),
                                            'task_completed' => ($user_info['task_completed'] + 1),
                                        );
                                        //update user table
                                        $updateId = $this->Common_model->update_single('users', $updateUserArr, ['where' => ['user_id' => $id]]);
                                        //unlock user levels
                                        $this->unlockUserlevels($id);
                                       
                                    }
                                    
                                //Insert in wallet log history
                                $insertWalletLog = array(
                                    'user_id' => $id,
                                    'point' => $task_info['points'],
                                    'title' => $this->lang->line('reward_earned'),
                                    'description' => $task_info['action'],
                                    'task_id' => $task_info['task_id'],
                                    'type' => $task_info['task_type'],
                                    'status' => COMPLETE,
                                    'created_date' => date('Y-m-d H:i:s'),

                                );
                                //check if task history alredy exist fo
                                $where['where'] = ['user_id' => $id, 'task_id' => $task_info['task_id'], 'type' => $task_info['task_type']];
                                $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                                if (empty($walletLogInfo)) {
                                    #save users details values in DB
                                    $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                                }
                            }
                        }
                        $updateArr = [];
                    }
                    if ($updateId) {
                        $whereArray = [];
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }

                    break;
                case 'taskcompleteallrequest':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $task_id = encryptDecrypt($req['tid'], 'decrypt');
                    $whereArr = array('where' => array('task_id' => $task_id));
                    $task_info = $this->Common_model->fetch_data(
                        'ipac_task_master',
                        ['task_id', 'task_type', 'action', 'points'],
                        $whereArr,
                        true
                    );
                    if (!empty($task_info)) {
                        $whereArray = array('where' => array('task_id' => $task_info['task_id'],'status' => PENDING));
                        $user_task_complete = $this->Common_model->fetch_data(
                            'user_task_master',
                            ['user_id', 'detail_id'],
                            $whereArray,
                            false
                        );
                        if (count($user_task_complete) > 0) {
                            foreach ($user_task_complete as $task_user) {
                                $id = $task_user['user_id'];
                                //update user total earning
                                $updateUserArr = array(
                                    'status' => COMPLETE,
                                );
                                //update user table
                                $updateId = $this->Common_model->update_single('user_task_master', $updateUserArr, ['where' => ['user_id' => $id, 'task_id' => $task_info['task_id']]]);
                                //update wallet expense history
                                //update user total earning

                                if ($updateId) {
                                    $whereArray = array('where' => array('user_id' => $id));
                                        $user_info = $this->Common_model->fetch_data(
                                            'users',
                                            ['user_id', 'total_earning', 'points_earned', 'task_completed'],
                                            $whereArray,
                                            true
                                        );
                                        if (!empty($user_info)) {
                                            //update user total earning
                                            $updateUserArr = array(
                                                'total_earning' => ($user_info['total_earning'] + $task_info['points']),
                                                'points_earned' => ($user_info['total_earning'] + $task_info['points']),
                                                'updated_at' => datetime(),
                                                'last_earning_update' => datetime(),
                                                'task_completed' => ($user_info['task_completed'] + 1),
                                            );
                                            //update user table
                                            $updateId = $this->Common_model->update_single('users', $updateUserArr, ['where' => ['user_id' => $id]]);
                                            //unlock user levels
                                            $this->unlockUserlevels($id);
                                        }
                                       
                                    //Insert in wallet log history
                                    $insertWalletLog = array(
                                        'user_id' => $id,
                                        'point' => $task_info['points'],
                                        'title' => $this->lang->line('reward_earned'),
                                        'description' => $task_info['action'],
                                        'task_id' => $task_info['task_id'],
                                        'type' => $task_info['task_type'],
                                        'status' => COMPLETE,
                                        'created_date' => date('Y-m-d H:i:s'),

                                    );
                                    //check if task history alredy exist fo
                                    $where['where'] = ['user_id' => $id, 'task_id' => $task_info['task_id'], 'type' => $task_info['task_type']];
                                    $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                                    if (empty($walletLogInfo)) {
                                        #save users details values in DB
                                        $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                                    }
                                }
                            }
                        }
                        $updateArr = [];
                        $whereArray = [];
                    }
                    $alertMsg['text'] = $this->lang->line('request_accept');
                    $alertMsg['type'] = $this->lang->line('success');
                    $this->session->set_flashdata('alertMsg', $alertMsg);
                    // if ($updateId) {
                    //     $whereArray = [];
                    //     $alertMsg['text'] = $this->lang->line('request_accept');
                    //     $alertMsg['type'] = $this->lang->line('success');
                    //     $this->session->set_flashdata('alertMsg', $alertMsg);
                    // } else {
                    //     $alertMsg['text'] = $this->lang->line('request_accept');
                    //     $alertMsg['type'] = $this->lang->line('success');
                    //     $this->session->set_flashdata('alertMsg', $alertMsg);
                    // }

                    break;
                case 'taskrejectrequest':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $task_id = encryptDecrypt($req['tid'], 'decrypt');
                    $whereArr = array('where' => array('task_id' => $task_id));
                    $task_info = $this->Common_model->fetch_data(
                        'ipac_task_master',
                        ['task_id', 'task_type', 'action', 'points'],
                        $whereArr,
                        true
                    );
                    if (!empty($task_info)) {
                        $whereArray = array('where' => array('user_id' => $id, 'task_id' => $task_info['task_id']));
                        $user_task_complete = $this->Common_model->fetch_data(
                            'user_task_master',
                            ['detail_id'],
                            $whereArray,
                            true
                        );
                        if (!empty($user_task_complete)) {
                            $updateId = $this->Common_model->delete_data('user_task_master', $whereArray);
                        }
                    }
                    if ($updateId) {
                        $whereArray = [];
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }

                    break;
                case 'taskrejectallrequest':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $task_id = encryptDecrypt($req['tid'], 'decrypt');
                    $whereArr = array('where' => array('task_id' => $task_id));
                    $task_info = $this->Common_model->fetch_data(
                        'ipac_task_master',
                        ['task_id', 'task_type', 'action', 'points'],
                        $whereArr,
                        true
                    );
                    if (!empty($task_info)) {
                        $whereArray = array('where' => array('task_id' => $task_info['task_id'], 'status' => PENDING));
                        $user_task_complete = $this->Common_model->fetch_data(
                            'user_task_master',
                            ['detail_id'],
                            $whereArray,
                            true
                        );
                        if (!empty($user_task_complete)) {
                            $updateId = $this->Common_model->delete_data('user_task_master', $whereArray);
                        }
                    }
                    if ($updateId) {
                        $whereArray = [];
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }

                    break;
                case 'expensereject':
                    $this->load->model("Notification_model");
                    #fetching user details from db
                    $whereArr = array('where' => array('expence_id' => $id));
                    $expense_info = $this->Common_model->fetch_data(
                        'user_campaign_expences',
                        ['expence_id', 'user_id', 'task_id', 'total_amount'],
                        $whereArr,
                        true
                    );

                    if (!empty($expense_info)) {
                        $whereArray = array('where' => array('user_id' => $expense_info['user_id']));
                        $user_info = $this->Common_model->fetch_data(
                            'users',
                            ['user_id', 'total_earning'],
                            $whereArray,
                            true
                        );
                        if (!empty($user_info)) {
                            //update wallet expense history
                            $updateWalletArr = array(
                                'status' => REJECTED,
                                'updated_date' => datetime(),
                            );
                            //update user table
                            $updateId = $this->Common_model->update_single('ipac_user_wallet_history', $updateWalletArr, ['where' => ['task_id' => $expense_info['expence_id']], 'type' => 'expense', 'status' => PENDING]);
                        }
                        $updateArr = [];
                    }
                    if ($updateId) {
                        $whereArray = [];
                        $whereArray['where'] = ['expence_id' => $id];
                        $updExpenseStatus = array(
                            'status' => EXPENSE_REJECTED,
                            'updated_date' => datetime()
                        );
                        $updateId = $this->Common_model->update_single('user_campaign_expences', $updExpenseStatus, $whereArray);
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('request_accept');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }
            }

            $csrftoken = $this->security->get_csrf_hash();
            #if transaction runs successfully
            if (true === $this->db->trans_status()) {
                #Comminting changes
                //$this->db->trans_commit();


                #setting Response Array
                $resparr = array("code" => 200, 'msg' => $this->lang->line('success'), "csrf_token" => $csrftoken, 'id' => $id);
            } else {
                #IF transaction failed
                #rolling back
                $this->db->trans_rollback();

                #setting Response Array
                $resparr = array("code" => 201, 'msg' => $this->lang->line('try_again'), "csrf_token" => $csrftoken, 'id' => $id);
            }
            echo json_encode($resparr);
            exit;
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }

    /**
     * @name getCityByState
     * @description This method is used to get all the cities as per the state using get method.
     * @access public.
     */
    public function getFormUrl()
    {
        try {
            if ($this->input->is_ajax_request()) {
                $req = $this->input->post();

                $citydata = $this->Common_model->fetch_data('forms', 'form_url', ['where' => ['fid' => $req['id']]], true);
                $csrftoken = $this->security->get_csrf_hash();

                $resparr = array("code" => 200, 'msg' => $this->lang->line('success'), "csrf_token" => $csrftoken, 'url' => $citydata['form_url']);
                echo json_encode($resparr);
            }
        } catch (Exception $e) {
            echo json_encode($e->getTraceAsString());
        }
    }

    public function unlockUserlevels($user_id)
    {
        $return_level = array();

        $sql = "SELECT points_earned FROM users WHERE user_id = ?";
        $query = $this->db->query($sql, array($user_id));
        $resultArray = $query->row_array();
        $current_points = $resultArray ['points_earned'];
       
        $new_levels = $this->db->query("SELECT MAX(fk_level_id) as fk_level_id from tbl_user_level where fk_user_id = $user_id");
        $new_levels = $new_levels->row_array();
    	$current_level = $new_levels['fk_level_id'] ? $new_levels['fk_level_id'] : '0';
                                
        if ($current_level != 'null') {
            $level_array = $this->nextlevel($current_points, $current_level);
       		 $difference = $level_array['max_level'] - $current_level;
            if ($difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $difference; $i++) {
                    $fk_level_id = $current_level + $i;
                    $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                    $reward_points = $reward_points->row_array();
                    $free_rewards =  $reward_points['free_reward_points'];
                    if($free_rewards != 0){
                        $params                 = array();
                        $params['points']       = $free_rewards;
                        $params['user_id']      = $user_id;
                        $res                    = $this->User_model->updateUserEarnPoints($params);

                        $walletInsertArr[]      = array(
                            'user_id'           => $user_id,
                            'point'             => $free_rewards,
                            'title'             => 'New Level Rewards',
                            'description'       => 'Rewards earned for completion of levels',
                            'task_id'           => '0',
                            'type'              => 'bonus',
                            'status'            => '1',
                            'created_date'      => date('Y-m-d H:i:s'),
                            'updated_date'      => date('Y-m-d H:i:s')
                        );
                        
                    }
                    
                    $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
                }
               
            }
        }
                    
            if(!empty($walletInsertArr) && count($walletInsertArr) > 0){
               	$this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
           	}
                  
        return $return_level;
    }
    
    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }
}
