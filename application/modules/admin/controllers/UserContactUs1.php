<?php

defined ( 'BASEPATH' ) OR exit ( 'No direct script access allowed' );

class Usercontactus extends MY_Controller {

    function __construct ()
    {
        parent::__construct ();
        $this->admininfo = $this->session->userdata ( 'admininfo' );
        $this->load->model ( 'Contactus_model' );
        $this->lang->load ( 'common', "english" );

    }



    /**
     * @name index
     * @description This method is used to list all the customers.
     */
    public function index ()
    {

        try {//TRY START
            $this->load->library ( 'Common_function' );

            $data['admininfo'] = $this->admininfo;

            $get = $this->input->get ();
            
            $default      = array (
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "order"      => ""
            );
            $defaultValue = defaultValue ( $get, $default );


            $data['page']   = $defaultValue['page'];
            $data['offset'] = ($defaultValue['page'] - 1) * $defaultValue['limit'];

            $data['contactus'] = $this->Contactus_model->contactuslist ( '', $data['offset'], $defaultValue['limit'], $defaultValue );
            $totalrows        = $data['contactus']['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if ( ! $data['contactus']['result'] && $defaultValue['page'] > 1 )
            {
                $defaultValue['page'] = ( string ) ($defaultValue['page'] - 1);
                redirect ( base_url () . "admin/contactus?data=" . queryStringBuilder ( $defaultValue ) );
            }


            /* Sorting Query */
            $getQuery          = http_build_query ( array_filter ( [ "limit" => $defaultValue['limit'], "page" => $defaultValue['page'] ] ) );
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_title"] = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if ( ! empty ( $defaultValue['order'] ) )
            {//IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if ( ! empty ( $defaultValue["field"] ) )
                {
                    switch ( trim ( $defaultValue["field"] ) )
                    {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            }//IF 1 END

            /* paggination */
            $pageurl            = 'admin/contactus';
            $links              = $this->common_function->pagination ( $pageurl, $totalrows, $defaultValue['limit'] );
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name ();
            $data["csrfToken"]  = $this->security->get_csrf_hash ();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class ();
            $data['method']     = $this->router->fetch_method ();
            $data['module']     = $this->router->fetch_module ();

            if ( ! $GLOBALS['permission'] )
            {
                setDefaultPermission ();
            }

            $data['permission'] = $GLOBALS['permission'];

            load_views ( "contactus/index", $data );
        }//TRY END
        catch ( Exception $exception ) {
            showException ( $exception->getMessage () );
            exit;
        }

    }





}
