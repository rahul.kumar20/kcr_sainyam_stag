<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_model');
        $this->lang->load('common', "english");
        $this->data      = [];
        $this->admininfo = [];
        $this->admininfo = $this->session->userdata('admininfo');

        $this->data['admininfo'] = $this->admininfo;
    }



  


    /**
     * @function admin_change_password
     * @description This method is used to change admin password.
     */
    public function index()
    {
        try {
            $postdata          = $this->input->post();
            $data['admininfo'] = $this->admininfo;
           
            //IF post array is set and not null
            if (isset($postdata) && ! empty($postdata)) {
                //setting form rules
                $this->form_validation->set_rules('helplineNumber', $this->lang->line('help_line_number_req'), 'trim|required');
                $this->form_validation->set_rules('email','trim|required|valid_email');

                //Checking Form validation
                if ($this->form_validation->run()) {
					
					$settingArray = array(
					    'help_line_number'=> $this->input->post('helplineNumber'),
						'help_line_mail' =>$this->input->post('email'),
						'created_date'=>date('Y-m-d H:i;s')
					);
                    if(isset($postdata['settingId']) && !empty($postdata['settingId'])) {
						  $settingId = encryptDecrypt($postdata['settingId'],'decrypt');
						  $where     = array ( "where" => array ( 'id' => $settingId ) );
						$isSuccess = $this->Common_model->update_single("ipac_settings", $settingArray);
					} else {
						$isSuccess = $this->Common_model->insert_single("ipac_settings", $settingArray);
					}					
					
					    $this->session->set_flashdata(
                                    'message',
                                    $this->lang->line('success_prefix') . $this->lang->line('setting_update_Success') . $this->lang->line('success_suffix')
                                );
                                redirect(base_url() . "admin/settings");
                   
                }//ELSE 2
            }//IF end
			  $data['settingsData'] = $this->Common_model->fetch_data(
                             'ipac_settings',
                             '*',
                             array(),
                             true
                         );
            load_views("setting/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



   
}
