<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';

class Notification extends MY_Controller
{

    /**
     * private variable to hold logged in admin Information
     * @var type
     */
    private $admininfo = array();

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('common', "english");
        $this->load->model('User_model');
        $this->lang->load('common', 'english');
        $this->load->helper('common');
        $this->admininfo = $this->session->userdata('admininfo');
        # import csv file library
        $this->load->library('csvimport');
    }

    /**
     * @function index
     * @description To load list of all notification
     */
    public function index()
    {
        $this->session->unset_userdata('notification_data');
        if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv'))
            && (isset($_FILES['task_csv_file']['name']) && !empty($_FILES['task_csv_file']['name']))
        ) {
            if ($_FILES['task_csv_file']['type'] == 'text/csv') {
                $msg = $this->notification_csv_import($_FILES['task_csv_file']['tmp_name']);
            } else {
                $msg = $this->lang->line('invalid_csv_file');
            }

            $this->session->set_flashdata('message_success', $msg);
            redirect(base_url() . 'admin/notification');
        }
        //TRY START
        try {
            $default = array(
                "searchlike" => "",
                "limit"      => 10,
                "page"       => 1,
                "platform"   => "",
                "startDate"  => "",
                "endDate"    => ""
            );

            $defaultValue              = defaultValue($this->input->get(), $default);
            $defaultValue['admininfo'] = $this->admininfo;
            $this->load->library('Common_function');

            #$getDataArr = $this->input->get();


            $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];

            $defaultValue['offset'] = $offset;
            $this->load->model("Notification_model");
            $notiDetail             = $this->Notification_model->getNotifications($defaultValue);

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$notiDetail['totalRecords'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/notification?data=" . queryStringBuilder($defaultValue));
            }

            $pageurl                   = 'admin/notification';
            $defaultValue['notiList']  = $defaultValue['totalrows'] = $notiDetail['totalRows'];
            $defaultValue['links']     = $this->common_function->pagination($pageurl, $defaultValue['totalrows'], $defaultValue['limit']);
            $defaultValue['notiList']  = $notiDetail['totalRecords'];
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $defaultValue['permission'] = $GLOBALS['permission'];

            load_views("notification/index", $defaultValue);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } //CATCH END
    }


    /**
     * @function add
     * @description To add new notification in DB
     */
    public function add()
    {
        try { //TRY END
            // If logged user is sub admin check for his permission
            $data['admininfo'] = $this->admininfo;
            $postDataArr       = $this->input->post();

            if (!empty($_FILES) && '' != $_FILES['notificationImage']['name']) {
                $this->load->helper(array('s3'));

                $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                $filename = $_FILES['notificationImage']['name'];
                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                if (in_array($ext, $allowed)) {
                    //upload in s3 server
                    $image_url = upload_image_s3($_FILES['notificationImage'], '');
                    $postDataArr['image'] = $image_url;
                }
            }

            if (!empty($postDataArr)) { //IF 2 START
                $this->sendNotification($postDataArr);
            } //IF 2 END

            $get = $this->input->get();
            // pr($get);
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => "",
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END


            $userInfo = $this->User_model->userlist($defaultValue);

            $totalrows = $userInfo['total'];
            $data['userlist'] = $userInfo['result'];

            // Manage Pagination
            $pageurl = 'admin/notification/add';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userInfo['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/notification/add?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            load_views("notification/add", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } //CATCH END
    }



    /**
     * Function used to send notification to device
     *
     * @param array $dataArr all required data to send notification
     * @param bollean $isResend to check is it resend or first time send
     * @return '' NA
     */
    private function sendNotification($dataArr, $isResend = false)
    {
        try {
            $userList = '';
            $sesuserlist = $this->session->userdata('notification_data');
            //if user list is not blank
            if (!empty($sesuserlist)) {
                $userlist = array();
                foreach ($sesuserlist as $key => $data) {
                    if (is_int($key)) {
                        $userlist[] =  $data;
                    }
                }
                $userList = $userlist;
            }

            $default = array(
                "platform" => "",
                "gender"   => "",
                "regDate"  => "",
                "title"    => "",
                "message"  => "",
                "link"     => "",
                "image"    => ""
            );

            $defaultValue = defaultValue($dataArr, $default);
            $notification_id = uniqid();
            $type = $dataArr['show_popup'] == '0' ? ADMIN_NOTIFICATION : 13;
            $payload = [
                "title" => $dataArr["title"],
                "body" => $dataArr["message"],
                "message" => $dataArr["message"],
                "type" => $type,
                "image" => isset($dataArr["image"]) ? $dataArr["image"] : '',
                "ext_link" => $dataArr["link"],
                "url_title" => '',
                "news_id" => '',
                "task_id" => '',
                'notification_id' => $notification_id
            ];
            $this->load->library('common_function');
            if (!empty($userList)) {
                $this->load->model("Notification_model");
                $notificationsList = $this->Notification_model->sendNotification($userList);
                if (!empty($notificationsList)) {
                    $totalCounts = count($notificationsList);
                    /*$token = array();
                    foreach ($notificationsList as $user) {
                        $token[] =  $user['device_token'];
                    }

                    $this->common_function->androidPush($token, $payload);*/

                    $users          = $this->common_function->prepareMultipleAssociativeArr($notificationsList,'platform');
                    $androidDevices = $users['android'];
                    $iosDevices     = $users['ios'];

                    if(count($androidDevices) > 0){
                        $androidTokens              = array();
                        foreach ($androidDevices as $androidUser) {
                            $androidTokens[]        =  $androidUser['device_token'];
                        }
                        $this->common_function->androidPush($androidTokens, $payload);
                    }


                    if(count($iosDevices) > 0){
                        $iosTokens                  = array();
                        foreach ($iosDevices as $iosUser) {
                            $iosTokens[]            =  $iosUser['device_token'];
                        }
                        $this->common_function->iosPush($iosTokens, $payload);
                    }
                } else {
                    throw new Exception($this->lang->line('try_again'));
                }
            } else {
                $totalCounts = 0;
                $topic = ENV == 'production' ? '/topics/prod_custom' : '/topics/stag_custom';
                $this->common_function->androidTopicPush($topic, $payload);

                $iosTopic = ENV == 'production' ? '/topics/ios_prod_custom' : '/topics/ios_stag_custom';
                $this->common_function->iOSTopicPush($iosTopic, $payload);
            }
            $pushInfoArr = [
                "platform"     => 0,
                "title"        => $defaultValue['title'],
                "message"      => $defaultValue['message'],
                "image"        => $defaultValue['image'],
                "link"        => $defaultValue['link'],
                "total_sents"  => $totalCounts,
                "created_date" => date('Y-m-d H:i:s')
            ];
            $isSuccess = $this->saveNotificationData($pushInfoArr, false);
            $alertMsg = [];
            if ($isSuccess) {
                $this->session->unset_userdata('notification_data');
                $alertMsg['text'] = $this->lang->line('notification_added');
                $alertMsg['type'] = $this->lang->line('success');
                $this->session->set_flashdata('alertMsg', $alertMsg);
            } else {
                $alertMsg['text'] = $this->lang->line('try_again');
                $alertMsg['type'] = $this->lang->line('error');
                $this->session->set_flashdata('alertMsg', $alertMsg);
            }

            if ($isResend) {
                echo json_encode(array('code' => 200, 'msg' => 'Success'));
                die;
            } else {
                redirect('/admin/notification');
            }
        } catch (Exception $exception) {
            $errorMessage = $exception->getMessage();
            if (!empty($errorMessage)) {
                $alertMsg['text'] = $errorMessage;
                $alertMsg['type'] = $this->lang->line('error');
                $this->session->set_flashdata('alertMsg', $alertMsg);
                redirect('/admin/notification');
            }
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     *  import csv data into database
     *
     * @Function task_csv_import
     * @Description  import csv data into database
     * @Return void
     */
    private function notification_csv_import($file_path)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->load->helper(array('s3', 'video_thumb'));

        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        $notificationArray = array();
        //creating array for notification

        //foreach block start
        foreach ($file_data as $data) {
            $data = array_values($data);

            $data = array_map('trim', $data);
            if (!array_key_exists($data[0], $notificationArray)) {
                $notificationArray[$data[0]] = $data;
            } else {
                for ($i = 3; $i < 4; $i++) {
                    if ($data[$i] != '') {
                        if (strpos($notificationArray[$data[0]][$i], $data[$i]) === false) {
                            $notificationArray[$data[0]][$i] = $notificationArray[$data[0]][$i] . "," . $data[$i];
                        }
                    }
                }
            }
        }
        //foreach block end

        //if not empty task
        if (!empty($notificationArray)) {
            //foreach row start
            foreach ($notificationArray as $row) {
                //current date
                $date_time = date('Y-m-d H:i:s');

                $row = array_values($row);
                #post parameters
                $notification_title = trim($row[1]);
                $notification_description = trim($row[2]);
                $registration_id = trim($row[3]);
                $image_url = trim($row[4]);

                //required field validation
                if (!$notification_title || !$notification_description) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                } else {
                    # save the record to the database
                    $ins_data = [
                        'title' => $notification_title,
                        'message' => $notification_description,
                        'image' => $image_url,
                        'created_date' => $date_time,
                    ];
                    # check for the user's registration id to
                    if ($registration_id) {
                        //get user earns to send notifications
                        $registeration_data = $this->Common_model->fetch_data('users', 'user_id', array('where_in' => array('registeration_no' => explode(',', $registration_id))));
                        $userIds = implode(",", array_column($registeration_data, "user_id"));
                        $notificationsList = $this->sendNotificationCsv(explode(',', $userIds), $ins_data);
                        if (!$notificationsList) {
                            $not_saved_record = $not_saved_record + 1;
                            continue;
                        }
                    }


                    $issuccess = $this->Common_model->insert_single('admin_notification', $ins_data);
                    echo $this->db->last_query();
                    die;
                    if ($issuccess) {
                        $saved_record = $saved_record + 1;
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                    }
                }
            } // end foreach loop here
        }



        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path



    /**
     * @function saveNotificationData
     * @description to save/update version information in DB
     *
     * @param type $data
     * @param type $updateId
     * @return boolean
     */
    private function saveNotificationData($data, $updateId = false)
    {

        try {
            $this->db->trans_start();
            if ($updateId) {
                $this->Common_model->update_single('admin_notification', $data, ['where' => ['id' => $updateId]]);
            } else {
                $updateId = $this->Common_model->insert_single('admin_notification', $data);
            }

            if (true === $this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        } catch (Exception $exception) {
            $this->db->trans_rollback();
            showException($exception->getMessage());
            exit;
        }
    }



    private function uploadImage()
    {
        $config = getConfig(UPLOAD_IMAGE_PATH, 'jpeg|jpg|png', 6000, 2048, 2048);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('notificationImage')) { //IF 4
            $upload_data   = $this->upload->data();
            $imageName     = $upload_data['file_name'];
            $thumbFileName = $upload_data['file_name'];
            $fileSource    = UPLOAD_IMAGE_PATH . $thumbFileName;
            $targetPath    = UPLOAD_THUMB_IMAGE_PATH;
            $isSuccess     = 1; #$this->common_function->thumb_create( $thumbFileName, $fileSource, $targetPath );
            if ($isSuccess) { //IF 5
                $thumbName = $imageName;
            } //IF 5 END

            return base_url() . UPLOAD_PATH . $imageName;
        } else { //ELSE 4
            $this->session->set_flashdata('message', $this->lang->line('error_prefix') . strip_tags($this->upload->display_errors()) . $this->lang->line('error_suffix'));
            return false;
        }
    }

    /**
     * @function: checkeduserlist
     * @Deteil :push notification selected user list ajax action
     * @Creted 04-10-2017
     */
    public function checkeduserlist()
    {
        try {
            /* --Ajax request check--- */
            if ($request = $this->input->is_ajax_request()) {
                // $data["csrfName"] = $this->security->get_csrf_token_name();
                //$data["csrfToken"] = $this->security->get_csrf_hash();
                $postarr = $this->input->get();
                if (isset($postarr['userid']) && !empty($postarr['userid'])) {
                    if ($this->session->userdata('notification_data') == null) {
                        $data_array[$postarr['userid']] = $postarr['userid'];
                        $data_array['notificattitl'] = $postarr['notificationtitle'];
                        $data_array['notificadesc'] = $postarr['notidesc'];
                        $data_array['sendto'] = trim(preg_replace('/\D/', '', $postarr['sendto']));
                        $this->session->set_userdata('notification_data', $data_array);
                    } else {
                        $data_array = $this->session->userdata('notification_data');
                        if ($postarr['checkval'] == 1) {
                            $data_array[$postarr['userid']] = $postarr['userid'];
                        } else {
                            unset($data_array[$postarr['userid']]);
                        }
                        $data_array['notificattitl'] = $postarr['notificationtitle'];
                        $data_array['notificadesc'] = $postarr['notidesc'];
                        $data_array['sendto'] = trim(preg_replace('/\D/', '', $postarr['sendto']));

                        $this->session->set_userdata('notification_data', $data_array);

                        $responsear = array('ercode' => '200');
                    }
                    $responsear = array('ercode' => '200');
                }
            } else {
                exit('No direct script access allowed');
            }
        } catch (Exception $e) {
            $e->getMessage();
        }
        echo json_encode($responsear);
    }


    /**
     * Function used to send notification to device
     *
     * @param array $dataArr all required data to send notification
     * @param bollean $isResend to check is it resend or first time send
     * @return '' NA
     */
    private function sendNotificationCsv($userList, $dataArr)
    {
        try {
            //$sesuserlist is session selected user list

            $default      = array(
                "platform" => "",
                "gender"   => "",
                "regDate"  => "",
                "title"    => "",
                "message"  => "",
                "link"     => "",
                "image"    => ""
            );
            $defaultValue = defaultValue($dataArr, $default);
            if (!empty($userList)) {
                $this->load->model("Notification_model");
                //users erns
                $notificationsList = $this->Notification_model->sendNotification($userList);
                if (!empty($notificationsList)) {
                    $totalCounts = count($notificationsList);
                    //sns object
                    $sns = new Snspush();

                    //async pubblich push notification
                    $result = $sns->asyncPublish($notificationsList, $dataArr);
                    //if success
                    if ($result['success']) {
                        //insert push notification data
                        $pushInfoArr = [
                            "platform"     => 0,
                            "title"        => $defaultValue['title'],
                            "message"      => $defaultValue['message'],
                            "image"        => $defaultValue['image'],
                            "total_sents"  => count($notificationsList),
                            "created_date" => date('Y-m-d H:i:s')
                        ];
                        //save notification data
                        $isSuccess = $this->saveNotificationData($pushInfoArr, false);
                        $alertMsg = [];
                    }
                } else {
                    throw new Exception($this->lang->line('try_again'));
                }
            } else {
                throw new Exception($this->lang->line('try_again'));
            }
        } catch (Exception $exception) {
            $errorMessage = $exception->getMessage();
            if (!empty($errorMessage)) {
                $alertMsg['text'] = $errorMessage;
                $alertMsg['type'] = $this->lang->line('error');
                $this->session->set_flashdata('alertMsg', $alertMsg);
                redirect('/admin/notification');
            }
            showException($exception->getMessage());
            exit;
        }
    }
}
