<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Referral_user_list extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('User_model');
        $this->load->model('News_model');
        $this->lang->load('common', 'english');
        $this->load->helper('common');
        $this->load->config("form_validation_admin");
          # import csv file library
        $this->load->library('csvimport');
    }



    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try {
            
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "id" =>'',
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => "",
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END


            $userInfo = $this->User_model->referralUserList($defaultValue);
            
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->exportUser($userInfo['result']);
            }//IF 3 END

            $totalrows = $userInfo['total'];
            $data['userlist'] = $userInfo['result'];

            // Manage Pagination
            $pageurl = 'admin/referrallist';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['id'] = $defaultValue['id'];

            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userInfo['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = ( string )($defaultValue['page'] - 1);
                redirect(base_url() . "admin/referrallist?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
             //user detail
             $where['where'] = ['user_id' => $defaultValue['id']];

             $data['userDetail'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id','full_name'), $where,true);
            load_views("users/referal_user_list", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function detail()
    {
        try {
            $get = $this->input->get();

            $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/users/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $userId;

            //get user profile data
            $data['profile'] = $this->User_model->userDetail($userId);
            //User ID Proof
            $data['userIdProof'] = $this->Common_model->fetch_data('user_id_proof_details', array(), ['where' => ['user_id' => $userId]]);

            $pageurl = 'users/detail';
            $this->load->library('Common_function');

            $data['searchlike'] = "";
            $params['limit'] = $data['limit'] = 10;
            $data['page'] = 1;
            $data['order'] = "";

            $params['offset'] = ($data['page'] - 1) * $data['limit'];
            $params["user_id"] = $userId;


            $data['status_array'] = [1 => 'Active', 2 => 'Blocked', 3 => 'Deleted'];

            //User Subscription END

            if (empty($data['profile'])) {//IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            }//IF END

            load_views("users/user-detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



    /**
     * @function exportUser
     * @description To export user search list
     *
     * @param type $userData
     */
    private function exportUser($userData)
    {
        try {
            $fileName = 'userlist' . date('d-m-Y-g-i-h') . '.xlsx';

                // Defines the name of the export file
            header('Content-Disposition: attachment; filename=' . $fileName);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $format = '<table border="1">'
                . '<tr>'
                . '<th width="25%">S.no</th>'
                . '<th>UID</th>'
                . '<th>App Version</th>'
                . '<th>Name</th>'
                . '<th>Email</th>'
                . '<th>Mobile Number</th>'
                . '<th>Whatsup Number</th>'
                // . '<th>Paytm Number</th>'
                // . '<th>State</th>'
                . '<th>District</th>'
                // . '<th>College</th>'
                . '<th>Gender</th>'
                . '<th>Registration Date</th>'
                . '<th>Number verified</th>'
                . '</tr>';

            $coun = 1;
            $gender_array = array('1'=>'Male','2'=>'Female','3'=>'Other');
            foreach ($userData as $res) {
                $date = date_create($res['registered_on']);
                $Date = date_format($date, 'd/m/Y');
                $Time = date_format($date, 'g:i A');

                $fld_1 = $coun;
                $fld_2 = (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '';
                $fld_3 = (isset($res['app_version']) && ($res['app_version'] != '')) ? $res['app_version'] : '';
                $fld_4 = (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '';
                $fld_5 = (isset($res['email_id']) && ($res['email_id'] != '')) ? $res['email_id'] : '';
                $fld_6 = (isset($res['phone_number']) && ($res['phone_number'] != '')) ? $res['phone_number'] : '';
                $fld_7 = (isset($res['whatsup_number']) && ($res['whatsup_number'] != '')) ? $res['whatsup_number'] : '';
                // $fld_8 = (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : '';
                $fld_9 = (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : '';
                // $fld_10 = (isset($res['college_name']) && ($res['college_name'] != '')) ? $res['college_name'] : '';;
                $fld_11 =($gender_array[$res['gender']]);
                $fld_12 = $Date . ' ' . $Time;
                $fld_13 = $res['is_number_verified'] =='1' ? "Verified" : "Not Verified";

                $format .= '<tr>
                        <td>' . $fld_1 . '</td>
                        <td>' . $fld_2 . '</td>
                        <td>' . $fld_3 . '</td>
                        <td>' . $fld_4 . '</td>
                        <td>' . $fld_5 . '</td>
                        <td>' . $fld_6 . '</td>
                        <td>' . $fld_7 . '</td>
                        <td>' . $fld_9 . '</td>
                        <td>' . $fld_11 . '</td>
                        <td>' . $fld_12 . '</td>
                        <td>' . $fld_13 . '</td>

                      </tr>';
                $coun++;
            } //end foreach

            echo $format;
            die;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }


    /**
     * Add user in database
     *
     * @Function addUser
     * @Description add user
     * @Return view page
     */
    public function addUser()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));

            $data['admininfo'] = $this->admininfo;
            $postData = $this->input->post();
            $this->load->library('Common_function');


            if (isset($postData) && !empty($postData)) {
                 // setting Form  validation Rules load from config file
                $this->form_validation->set_rules($this->config->item("add_user"));

                //if validation is true
                if ($this->form_validation->run() == true) {
                    //check admin data
                    /* Check image or video is set or not */
                    if ($_FILES && !empty($_FILES['user_image']['name']) && isset($_FILES['user_image']['name'])) {
                        $files = $_FILES;

                        $imgarr = array();

                        $user_image_url = '';
                        
                        //image check is it is set or not
                        if (!empty($_FILES['user_image']['name'])) {
                            //image insert array
                            $imgarr['user_image']['name'] = $files['user_image']['name'];
                            $imgarr['user_image']['type'] = $files['user_image']['type'];
                            $imgarr['user_image']['tmp_name'] = $files['user_image']['tmp_name'];
                            $imgarr['user_image']['error'] = $files['user_image']['error'];
                            $imgarr['user_image']['size'] = $files['user_image']['size'];
                            
                            //upload in s3 server
                            $userImageUrl = upload_image_s3($imgarr['user_image'], '');

                            /**Image thumb block end*/
                        }
                    }
                    $registrationNo = $this->generateRegistrationNo($this->input->post('distict'));

                    
                   //insert user array
                    $insertUserArray = array(
                        'registeration_no' => $registrationNo,
                        'full_name' => $this->input->post('first_name'),
                        'email_id' => $this->input->post('email'),
                        'password' => createPassword($postData['password']),
                        'facebook_id' => $this->input->post('fbId'),
                        'twitter_id' => $this->input->post('twitterId'),
                        'phone_number' => $this->input->post('phoneno'),
                        'whatsup_number' => $this->input->post('whatsupno'),
                        'paytm_number' => $this->input->post('paytmno'),
                        'state' => $this->input->post('state'),
                        'gender' => $this->input->post('gender'),
                        'district' => $this->input->post('distict'),
                        'user_image' => isset($userImageUrl) && !empty($userImageUrl) ? $userImageUrl : '',
                        'registered_on' => date('Y-m-d H:i:s'),
                        'is_number_verified' => 0,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'college' => $this->input->post('college_name'),
                        'dob' => date('Y-m-d', strtotime(str_replace('-', '/', $postData['dob']))),
                        'referal_code' => $this->referalCodeGenerator($this->input->post('first_name'))
                    );

                    $this->db->trans_begin(); #DB transaction Start

                   //insert user details
                    $userId = $this->Common_model->insert_single('users', $insertUserArray);

                    //insert in user device detail
                    $insertDevice = array(
                        'user_id' => $userId
                    );
                    $this->common_function->writeDeviceDetailsLog('insert','Admin :: Referral_user_list >> addUser ',$insertDevice);
                    $this->Common_model->insert_single('user_device_details', $insertDevice);

                    if (true === $this->db->trans_status()) {
                        #Comminting changes
                        $this->db->trans_commit();
                        #setting Response Array
                        $this->session->set_flashdata('message_success', $this->lang->line('user_add_success'));
                        redirect(base_url() . 'admin/users');
                    } else {
                        #IF transaction failed
                        #rolling back
                        $this->db->trans_rollback();
                        $$this->session->set_flashdata('message_error', $this->lang->line('something_went_Worng'));
                        redirect(base_url() . 'admin/users');
                    }
                }
            }

            $data["csrfName"] = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;
            $data['state'] = $this->Common_model->fetch_data('state_list', array('state_id', 'state_name'), []);
            load_views("users/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

/* Generating user registration number
     * IPAC XXX PTA YYYY (IPAC, PTA are static and XXX, YYYY are variables)
        First 4 digits represents the company (IPAC) name
        XXX represents the district code (number)
        PTA represents as Part Time Associate
        YYYY represents the volunteer serial number (in respective districts)
     */
    private function generateRegistrationNo($districtId)
    {
        
        #generate 3 digit district code of user district
        $districtnum = 3;
        $district_num_padded = sprintf("%03d", $districtId);
        
        # count the number of users in a particular district
        $countUser = $this->News_model->userList($districtId);
        $usercnt = 0;
        if (!empty($countUser)) {
            $usercnt = $countUser->UserCnt;
        }
        $usercnt = $usercnt + 1;
        # generate 4 digit user count for a particular district
        $districtusernum = 4;
        $district_user_num_padded = sprintf("%04d", $usercnt);

        $prefix = 'STLN';
        $sufix = 'PTA';
        $registrationNo = $prefix . $district_num_padded . $sufix . $district_user_num_padded;
        return $registrationNo;
    }

    private function userCsvImport($file_path)
    {

        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->load->helper(array('s3', 'video_thumb'));

        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        //fir each loop
        foreach ($file_data as $row) {
            $date_time = date('Y-m-d H:i:s');

            $row = array_values($row);

            # check for required params
            $name = trim($row[0]);
            $email = trim($row[1]);
            $password = trim($row[2]);
            $phoneNumber = trim($row[3]);
            $whatsupNumber = trim($row[4]);
            $paytmNumber = trim($row[5]);
            $facebookId = trim($row[6]);
            $twitterId = trim($row[7]);
            $imgUrl = trim($row[8]);
            $state = trim($row[9]);
            $district = trim($row[10]);
            $gender = trim($row[11]);
            $college = trim($row[11]);
            $dob = trim($row[11]);

            //required validation
            if (!$name || !$email || !$password || !$phoneNumber || !$whatsupNumber || !$paytmNumber || !$state || !$district || !$gender ||!$dob) {
                $not_saved_record = $not_saved_record + 1;
                continue;
            } else {
                #valid email check
                if (!valid_email($email)) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                }
                 #valid phone check
                if (!phone_number($phoneNumber)) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                }
                 # check for unique email
                if ($email) {
                    $user_data = $this->Common_model->fetch_data('users', 'user_id', array('where' => array('email_id' => $email, 'is_active!=' => DELETED)), true);
                    //if email exist
                    if ($user_data) {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                }
                 # check for unique phone number
                if ($phoneNumber) {
                    $user_data = $this->Common_model->fetch_data('users', 'user_id', array('where' => array('phone_number' => $phoneNumber, 'is_active!=' => DELETED)), true);
                    //if email exist
                    if ($user_data) {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                }

                //valid dob
                if (!(isValidDate($dob, 'd-m-Y'))) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                }
                # check for state id
                $state_id = 0;
                if ($state) {
                    $state_data = $this->Common_model->fetch_data('state_list', 'state_id', array('where' => array('state_name' => $state)), true);

                    if ($state_data) {
                        $state_id = $state_data['state_id'];
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                }

                # check for district of the state
                $district_code = 0;
                if ($state_id != 0 && $district) {
                    $district_data = $this->Common_model->fetch_data('district', 'district_code', array('where' => array('state_id' => $state_id, 'district_name' => $district)), true);

                    if ($district_data) {
                        $district_code = $district_data['district_code'];
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                }
             # check for college of the district
                $college_code = 0;
                if ($state_id != 0 && $district_code!=0 && !empty($college)) {
                    $college_data = $this->Common_model->fetch_data('college_list', 'college_id', array('where' => array('district_id' => $district_code, 'college_name' => $college)), true);

                    if ($college_data) {
                        $college_code = $college_data['college_id'];
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                }
                # check for the gender
                $gender = 0; # default is 0. 0 means other
                if (strtolower($gender) == 'male') {
                    $gender = 1;
                } elseif (strtolower($gender) == 'female') {
                    $gender = 2;
                }
                #if image url is set
                if (!empty($imgUrl)) {
                    $file_url = explode('/', $imgUrl);
                    $file_name = array_pop($file_url);

                    $file_extenstion = explode('.', $file_name);
                    $file_extenstion = array_pop($file_extenstion);
                    $file_extenstion = strtolower($file_extenstion);
            
                    if (!in_array($file_extenstion, ['jpg', 'jpeg', 'png'])) {
                        continue;
                    }
            
                    $file_type = 1;

                  // Getting the base name of the file.
                    $filename = basename($file_name);
            
                    $temp_folder_path = TMEP_FOLDER_PATH;
                    $file_path = $temp_folder_path . $filename;
            
                    $write_status = file_put_contents($file_path, file_get_contents($imgUrl));

                    if ($write_status) {
                        $s3_main_file_url = upload_image_s3(
                            ['tmp_name'=>$file_path, 'name'=>$filename],
                            mime_content_type($file_path)
                        );
                    }
                }
                   
                # save the record to the database
                $ins_data = [
                    'registeration_no' => $this->generateRegistrationNo($district_code),
                    'full_name' => $name,
                    'registered_on' => $date_time,
                    'updated_at' => $date_time,
                    'email_id' => $email,
                    'state' => $state_id,
                    'district' => $district_code,
                    'password' => createPassword($password),
                    'gender' => $gender,
                    'facebook_id' => $facebookId,
                    'twitter_id' => $twitterId,
                    'whatsup_number' => $whatsupNumber,
                    'paytm_number' => $paytmNumber,
                    'phone_number' => $phoneNumber,
                    'is_active' => ACTIVE,
                    'user_image' => (isset($s3_main_file_url) &&!empty($s3_main_file_url)?$s3_main_file_url:''),
                    'dob' => date('Y-m-d', strtotime($dob)),
                    'college' => $college_code,
                    'is_number_verified' => 0,

                ];
                $userId = $this->Common_model->insert_single('users', $ins_data);
                if ($userId) {
                    # save the news media files
                  //  $this->news_media_files($news_image_url, $news_video_url, $newsId, $date_time);
                    $saved_record = $saved_record + 1;
                } else {
                    $not_saved_record = $not_saved_record + 1;
                }
            }
        } // end foreach loop here


        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path

    /**
     * 
     * @Function checkReferralCode
     * @Description check referal code
     */
    public function checkReferralCode() {
        $getData = $this->input->get();
        if (!empty($getData) && !empty($getData['referalcode'])) {
            $whereArry['where'] = [ 'referal_code' => $getData['referalcode'],'is_active'=> ACTIVE ];
            $valid_referal_check= $this->Common_model->fetch_data('users', [ 'user_id'], $whereArry, true);
            if(empty($valid_referal_check)) {
                echo 'false';
                die;
            } else {
                echo 'true';
                die;
            }
        }
    }

    /*
     * @Function referalCodeGenerator
     * @description referal code generator
     * @param User name and name
     * @Created date 21-08-2018
     */
    private function referalCodeGenerator($name)
    {
        if (isset($name) && !empty($name)) {
            $Username=substr($name, 0, 2);
            $uniqecode = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6));
            $referalCode = strtoupper($Username.$uniqecode);
        }
        return $referalCode;
    }
}
