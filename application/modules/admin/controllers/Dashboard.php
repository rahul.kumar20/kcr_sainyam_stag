<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    private $months = [];

    function __construct()
    {
        parent::__construct();
        $this->load->model(array ( 'Common_model', 'Dashboard_model' ));
        $this->data              = [];
        $this->data['admininfo'] = $this->session->userdata('admininfo');

        $this->months = array (
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        );
    }



    /**
     * @function index
     * @description to load and render dashboard
     */
    public function index()
    {
     
       redirect(base_url().'admin/news');
    }



    /**
     *
     */
    function fetch_data()
    {
        $res = [];
        if ($this->input->is_ajax_request()) {
            $post = $this->input->post();
            switch ($post['type']) {
                case "yearly":
                    $response = $this->Dashboard_model->get_yearly_data();
                    if (null !== $response) {
                        $years = [];
                        $users = [];
                        foreach ($response as $index => $user) {
                            $years[] = ( int ) $user['y'];
                            $users[] = ( int ) $user['total'];
                        }
                        $res['year']   = $years;
                        $res['users']  = $users;
                        $res['status'] = true;
                    } else {
                        $res['status'] = false;
                    }
                    echo json_encode($res);
                    break;
                case "monthly":
                    $response = $this->Dashboard_model->get_monthly_data($post['year']);
                    if (null !== $response) {
                        $years  = [];
                        $users  = [];
                        $months = $this->months;
                        foreach ($response as $index => $user) {
                            $month[] = $months[(( int ) $user['y']) - 1];
                            $users[] = ( int ) $user['total'];
                        }
                        $res['year']   = $month;
                        $res['users']  = $users;
                        $res['status'] = true;
                    } else {
                        $res['status'] = false;
                    }
                    echo json_encode($res);
                    break;
                case "weekly":
                    $month    = array_flip($this->months);
                    $response = $this->Dashboard_model->get_weekly_data($post['year'], $month[$post['month']] + 1);

                    if (null !== $response) {
                        $years  = [];
                        $users  = [];
                        $months = $this->months;
                        foreach ($response as $index => $user) {
                            $month[] = $user['y'];
                            $users[] = ( int ) $user['total'];
                        }
                        $res['year']   = $month;
                        $res['users']  = $users;
                        $res['status'] = true;
                    } else {
                        $res['status'] = false;
                    }
                    echo json_encode($res);
                    break;
            }
        }
    }
}
