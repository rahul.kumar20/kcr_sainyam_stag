<?php

defined('BASEPATH') or exit('No direct script access allowed');

class NewsDetail extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('News_model');
        $this->lang->load('common', 'english');
    }

    /**
     * @name newsData
     * @description This method is used to fetch news detail
     */
    public function newsData()
    {
        try {
            $get = $this->input->get();
            $newsId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/news/");

            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['news_id'] = $newsId;

            //get user profile data
            $data['news'] = $this->Common_model->fetch_data('ipac_news', array(), ['where' => ['news_id' => $newsId]], true);
            $data['news_media'] = $this->Common_model->fetch_data('ipac_news_media', array(), ['where' => ['news_id' => $newsId]]);

            /*For fetching news comments for the give news id*/
            $default = array(
                "limit" => 10,
                "page" => 1,
                "id" => $newsId,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } else {
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }

            $newsCommentsList = $this->News_model->getNewsAllComments($defaultValue, $newsId);

            $totalrows = $newsCommentsList['total'];
            $data['newsCommentsList'] = $newsCommentsList['result'];

            $this->load->library('Common_function');
            // Manage Pagination
            $pageurl = 'admin/newsDetail/newsData';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data['news_id'] = $newsId;

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_enddate"] = $data["order_by_startdate"] = "sorting";

            if (!empty($defaultValue['sortby'])) {
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            }

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['limit'] = $defaultValue['limit'];

            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['categoryArr'] = array('1' => 'Gallery', '2' => 'Article', '3' => 'News', '4' => 'Notification', '5' => 'Banner', '6' => 'Form', '7' => 'Party News', '8' => 'Murasoli', '9' => 'Text', '10' => 'Image', '11' => 'Video');

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$newsCommentsList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/newsDetail/newsData?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $data['permission'] = $GLOBALS['permission'];

            //uid list
            $data['uid'] = $defaultValue['uid'];
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);

            if (empty($data['news'])) { //IF START
                show404($this->lang->line('no_news'), "/admin/news/");
                return 0;
            } //IF END

            load_views("news/news_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /*
     *@function name : commentThreadList
     *@description This is used to fetch the thread of comments
     */
    public function commentThreadList()
    {
        try {
            $get = $this->input->get();

            $newsCommentId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/news/");

            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['comment_id'] = $newsCommentId;

            /*For fetching news comments thread for the give news comment*/
            $default = array(
                "limit" => 10,
                "page" => 1,
                "id" => $newsCommentId,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
            $defaultValue['offset'] = $offset;

            $newsCommentThreadList = $this->News_model->getChildComments($defaultValue);

            $totalrows = $newsCommentThreadList['total'];
            $data['newsCommentThreadList'] = $newsCommentThreadList['result'];

            $data['mainComment'] = '';
            if ($newsCommentId > 0) {
                $commentParams = array(
                    'comment_id' => $newsCommentId,
                    'limit' => 1,
                    'offset' => '',
                );
                $data['mainComment'] = $this->News_model->getNewsAllComments($commentParams, 'yes');
            }

            $this->load->library('Common_function');
            // Manage Pagination
            $pageurl = 'admin/newsDetail/commentThreadList';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_enddate"] = $data["order_by_startdate"] = "sorting";

            if (!empty($defaultValue['sortby'])) {
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            }

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['limit'] = $defaultValue['limit'];

            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['categoryArr'] = array('1' => 'Gallery', '2' => 'Article', '3' => 'News', '4' => 'Notification', '5' => 'Banner', '6' => 'Form', '7' => 'Party News', '8' => 'Murasoli', '9' => 'Text', '10' => 'Image', '11' => 'Video');

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$newsCommentThreadList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/newsDetail/commentThreadList?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            if (empty($data['mainComment'])) {
                show404($this->lang->line('no_news_comments'), "/admin/news/");
                return 0;
            }

            load_views("news/news_comments", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    //reply option for the comments
    public function comment_detail()
    {
        try {

            $get = $this->input->get();
            $newsCommentId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/news/");
            $data['newsCommentsList'] = $this->News_model->getAllCommentsdetails($newsCommentId);
            $news_id = $data['newsCommentsList']['news_id'];
            $parent_id = $data['newsCommentsList']['comment_id'];
            $news_title = $data['newsCommentsList']['news_title'];
            $postdata = $this->input->post();
            $user_id = (ENV == 'production') ? '97983 ' : '1';
            $whereArr['where'] = ['user_id' => $user_id];
            if (empty($data['newsCommentsList'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            } //IF END

            if (isset($postdata) && !empty($postdata)) {
                $this->load->library('common_function');
                $user_comment = $this->input->post('reply');
                $insertcommentsArray = array(
                    'news_id'       => $news_id,
                    'user_id'       => $user_id,
                    'sub_parent_id'     => $parent_id,
                    'parent_id'     => $parent_id,
                    'user_comment'  => $this->input->post('reply'),
                    'submitted_timestamp' => date('Y-m-d H:i:s'),
                );

                $commentID = $this->Common_model->insert_single('ipac_news_comments', $insertcommentsArray);
                //PUSH NOTIFICATION
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    ['full_name', 'gender', 'state', 'district', 'registeration_no', 'user_image', 'is_profile_verified', 'is_active'],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    if (ACTIVE == $userInfo['is_active']) {

                        $response['user_id'] = $user_id;
                        $response['full_name'] = $userInfo['full_name'];
                        $response['news_title'] = $news_title;
                        $response['registeration_no'] = $userInfo['registeration_no'];
                        $response['user_image'] = $userInfo['user_image'];
                        $response['is_profile_verified'] = $userInfo['is_profile_verified'];
                        $response['user_comment'] = $insertcommentsArray['user_comment'];
                        $response['submitted_timestamp'] = $insertcommentsArray['submitted_timestamp'];
                        $response['comment_id'] =  $commentID;
                        $response['parent_id'] = isset($insertcommentsArray['parent_id']) ? $insertcommentsArray['parent_id'] : 0;
                        if ($response['parent_id'] != 0) {
                            $userErn = $this->News_model->commentUserErns($user_id, $response['parent_id']);
                        
                            if (count($userErn)) {
                                $token                      = array();
                                $androidTokens              = array();
                                $iosTokens                  = array();
                                foreach ($userErn as $user) {
                                    $token[]                = $user['device_token'];
                                    if(strtolower($user['platform']) == 'android'){
                                        $androidTokens[]    = $user['device_token'];
                                    }else if(strtolower($user['platform']) == 'ios'){
                                        $iosTokens[]        = $user['device_token'];
                                    }
                                }
                                $payload = [
                                    "title"     => $userInfo['full_name'] . ' உங்கள் கருத்திற்கு பதிலளித்துள்ளார்',
                                    "message"   => $insertcommentsArray['user_comment'],
                                    "body"      => $response,
                                    "type"      => 12,
                                    "image"     => '',
                                    'news_id'   => $news_id,
                                    'task_id'   => '',
                                    'notification_id' => ''
                                ];
                                if(count($androidTokens) > 0){
                                    $this->common_function->androidPush($androidTokens, $payload);
                                }
                                if(count($iosTokens) > 0){
                                    $this->common_function->iosPush($iosTokens, $payload);
                                }
                                
                            }
                        }
                    //END PUSH NOTIFICATION
                    }
                }
                redirect(base_url() . 'admin/news');
                
            }
            load_views("news/comments_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    public function subcomment_detail()
    {
        try {

            $get = $this->input->get();
            $newsCommentId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/news/");
            $data['newsCommentsList'] = $this->News_model->getAllCommentsdetails($newsCommentId);
            $news_id = $data['newsCommentsList']['news_id'];
            $parent_id = $data['newsCommentsList']['parent_id'];
            $sub_parent_id = $data['newsCommentsList']['comment_id'];
            $defaultValue = $data['newsCommentsList']['comment_id'];
            $news_title = $data['newsCommentsList']['news_title'];
            $postdata = $this->input->post();
            $user_id = (ENV == 'production') ? '97983 ' : '1';
            $whereArr['where'] = ['user_id' => $user_id];
            if (empty($data['newsCommentsList'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            } //IF END

            if (isset($postdata) && !empty($postdata)) {
                $this->load->library('common_function');
                $user_comment = $this->input->post('reply');
                $insertsubcommentsArray = array(
                    'news_id'       => $news_id,
                    'user_id'       => $user_id,
                    'sub_parent_id' => $sub_parent_id,
                    'parent_id'     => $parent_id,
                    'user_comment'  => $this->input->post('reply'),
                    'submitted_timestamp' => date('Y-m-d H:i:s'),
                );
                 
                $commentID = $this->Common_model->insert_single('ipac_news_comments', $insertsubcommentsArray);
                //PUSH NOTIFICATION
                $userInfo = $this->Common_model->fetch_data(
                    'users',
                    ['full_name', 'gender', 'state', 'district', 'registeration_no', 'user_image', 'is_profile_verified', 'is_active'],
                    $whereArr,
                    true
                );
                if (!empty($userInfo)) {
                    if (ACTIVE == $userInfo['is_active']) {

                        $response['user_id'] = $user_id;
                        $response['full_name'] = $userInfo['full_name'];
                        $response['news_title'] = $news_title;
                        $response['registeration_no'] = $userInfo['registeration_no'];
                        $response['user_image'] = $userInfo['user_image'];
                        $response['is_profile_verified'] = $userInfo['is_profile_verified'];
                        $response['user_comment'] = $insertsubcommentsArray['user_comment'];
                        $response['submitted_timestamp'] = $insertsubcommentsArray['submitted_timestamp'];
                        $response['comment_id'] =  $commentID;
                        $response['parent_id'] = isset($insertsubcommentsArray['parent_id']) ? $insertsubcommentsArray['parent_id'] : 0;
                        $response['sub_parent_id'] = isset($insertsubcommentsArray['sub_parent_id']) ? $insertsubcommentsArray['sub_parent_id'] : 0;

                        /*If comment gives to sub comment, then that sub comment parent need to get the notification not the main parent user*/
                        if ($response['sub_parent_id'] != 0) {
                            $userErn    = $this->News_model->commentUserErns($user_id, $response['sub_parent_id']);
                            
                            if (count($userErn)) {
                                $token              = array();
                                $androidTokens      = array();
                                $iosTokens          = array();
                                foreach ($userErn as $user) {
                                    $token[]                =  $user['device_token'];
                                    if(strtolower($user['platform']) == 'android'){
                                        $androidTokens[]    = $user['device_token'];
                                    }else if(strtolower($user['platform']) == 'ios'){
                                        $iosTokens[]        = $user['device_token'];
                                    }
                                }
                                $payload            = [
                                    "title"         => $userInfo['full_name'] . ' உங்கள் கருத்திற்கு பதிலளித்துள்ளார்',
                                    "message"       => $insertsubcommentsArray['user_comment'],
                                    "body"          => $response,
                                    "type"          => 12,
                                    "image"         => '',
                                    'news_id'       => $news_id,
                                    'task_id'       => '',
                                    'notification_id' => ''
                                ];
                                if(count($androidTokens) > 0){
                                    $this->common_function->androidPush($androidTokens, $payload);
                                }
                                if(count($iosTokens) > 0){
                                    $this->common_function->iosPush($iosTokens, $payload);
                                }
                            }
                        }
                    //END PUSH NOTIFICATION
                    }
                }
                redirect(base_url() . 'admin/news');
            }
            load_views("news/subcomments_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

}
