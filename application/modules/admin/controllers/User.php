<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "/libraries/Encryption_file.php";

class User extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('User_model');
        $this->load->model('News_model');
        $this->lang->load('common', 'english');
        $this->load->helper('common');
        $this->load->config("form_validation_admin");
        $this->load->library('common_function');
        # import csv file library
        $this->load->library('csvimport');
    }



    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {
        //     echo decryptString('eAxTiI2h6ZuEfDboflaDoRNPNYqqg96ZZaceqMWIwtc');
        //  echo  encryptString('vishal');die;
        try {
            # upload csv content
            // echo "<pre>";
            // print_r($_POST);
            // print_r($_FILES);exit;
            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv'))
                && (isset($_FILES['news_csv_file']['name']) && !empty($_FILES['news_csv_file']['name']))
            ) {
                $csvFileTypes   = array('application/vnd.ms-excel','text/csv');
                if (in_array($_FILES['news_csv_file']['type'],$csvFileTypes)) {
                    $msg = $this->userCsvImport($_FILES['news_csv_file']['tmp_name']);
                } else {
                    $msg = $this->lang->line('invalid_csv');
                }

                // $file_path = getcwd () . "/public/data.csv";
                // $msg = $this->news_csv_import($file_path);
                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . 'admin/users');
            }
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => "",
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END


            $userInfo = $this->User_model->userlist($defaultValue);

            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($userInfo['result']);
            } //IF 3 END

            $totalrows = $userInfo['total'];
            $data['userlist'] = $userInfo['result'];

            // Manage Pagination
            $pageurl = 'admin/users';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = $data["order_by_earning"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "total_earning":
                            $data["order_by_earning"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            $data['GENDER_TYPE'] = unserialize(GENDER_TYPE);

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userInfo['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/users?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            /*//load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //District list for goa
            $data["districtlist"] = get_district_list(DEFAULT_STATE_ID);
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);*/
            $data['statelist']      = array();
            $data['districtlist']   = array();
            $data['uidlist']        = array();
            load_views("users/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function detail()
    {
        try {
            $get = $this->input->get();

            $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/users/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $userId;
            $roleId = $data['admininfo']['role_id'];
            //get user profile data
            $data['profile'] = $this->User_model->userDetail($userId);
            //User ID Proof
            $data['userIdProof'] = $this->Common_model->fetch_data('user_id_proof_details', array(), ['where' => ['user_id' => $userId, 'status=' => 0]]);

            $pageurl = 'users/detail';
            $this->load->library('Common_function');

            $data['searchlike'] = "";
            $params['limit'] = $data['limit'] = 10;
            $data['page'] = 1;
            $data['order'] = "";

            $params['offset'] = ($data['page'] - 1) * $data['limit'];
            $params["user_id"] = $userId;
            $data['GENDER_TYPE'] = unserialize(GENDER_TYPE);


            $data['status_array'] = [1 => 'Active', 2 => 'Blocked', 3 => 'Deleted'];

            //User Subscription END

            if (empty($data['profile'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            } //IF END

            load_views("users/user-detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function download($userlist)
    {

        //  $subscribers = $this->phpexcel_model->get_users();

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:T1')->applyFromArray($styleArray);

        foreach (range('A', 'T') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'UID')
            ->setCellValue("C1", 'App Version')
            ->setCellValue("D1", 'Name')
            ->setCellValue("E1", 'Email')
            ->setCellValue("F1", 'Mobile Number')
            ->setCellValue("G1", 'Whatsapp Number')
            ->setCellValue("H1", 'District')
            ->setCellValue("I1", 'Gender')
            ->setCellValue("J1", 'Facebook ID')
            ->setCellValue("K1", 'Facebook User Name')
            ->setCellValue("L1", 'Twitter ID')
            ->setCellValue("M1", 'Twitter User Name')
            ->setCellValue("N1", 'Registration Date')
            ->setCellValue("O1", 'Task Completed')
            ->setCellValue("P1", 'Reward Points')
            ->setCellValue("Q1", 'Total Earning')
            ->setCellValue("R1", 'Referral Code')
            ->setCellValue("S1", 'Total Referral Used')
            ->setCellValue("T1", 'Number verified');

        // Add some data
        $x = 2;
        $status = array('1' => 'Active', '2' => 'Blocked');
        $gender_type = unserialize(GENDER_TYPE);

        $count = 1;
        if(!empty($userlist) && count($userlist) > 0){
            foreach ($userlist as $res) {
                $date = date_create($res['registered_on']);
                $Date = date_format($date, 'd/m/Y');
                $Time = date_format($date, 'g:i A');
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $count)
                    ->setCellValue("B$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '')
                    ->setCellValue("C$x", (isset($res['app_version']) && ($res['app_version'] != '')) ? $res['app_version'] : '')
                    ->setCellValue("D$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '')
                    ->setCellValue("E$x", (isset($res['email_id']) && ($res['email_id'] != '')) ? $res['email_id'] : '')
                    ->setCellValue("F$x", (isset($res['phone_number']) && ($res['phone_number'] != '')) ? $res['phone_number'] : '')
                    ->setCellValue("G$x", (isset($res['whatsup_number']) && ($res['whatsup_number'] != '')) ? $res['whatsup_number'] : '')
                    ->setCellValue("H$x", (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : '')
                    ->setCellValue("I$x", ($gender_type[$res['gender']]))
                    ->setCellValue("J$x", (isset($res['facebook_id']) && ($res['facebook_id'] != '')) ? $res['facebook_id'] : '')
                    ->setCellValue("K$x", (isset($res['fb_username']) && ($res['fb_username'] != '')) ? $res['fb_username'] : '')
                    ->setCellValue("L$x", (isset($res['twitter_id']) && ($res['twitter_id'] != '')) ? $res['twitter_id'] : '')
                    ->setCellValue("M$x", (isset($res['twitter_username']) && ($res['twitter_username'] != '')) ? $res['twitter_username'] : '')
                    ->setCellValue("N$x", $Date . ' ' . $Time)
                    ->setCellValue("O$x", (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : '')
                    ->setCellValue("P$x", (isset($res['points_earned']) && ($res['points_earned'] != '')) ? $res['points_earned'] : '')
                    ->setCellValue("Q$x", (isset($res['total_earning']) && ($res['total_earning'] != '')) ? $res['total_earning'] : '')
                    ->setCellValue("R$x", (isset($res['referal_code']) && ($res['referal_code'] != '')) ? $res['referal_code'] : '')
                    ->setCellValue("S$x", (isset($res['total_referral']) && ($res['total_referral'] != '')) ? $res['total_referral'] : '0')
                    ->setCellValue("T$x", ($res['is_number_verified'] == '1') ? 'Verified' : 'Not Verified');
                $x++;
                $count++;
            }
        }



        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="user_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }



    /**
     * Add user in database
     *
     * @Function addUser
     * @Description add user
     * @Return view page
     */
    public function addUser()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;
            $postData = $this->input->post();

            if (isset($postData) && !empty($postData)) {
                // setting Form  validation Rules load from config file
                $this->form_validation->set_rules($this->config->item("add_user"));

                //if validation is true
                if ($this->form_validation->run() == true) {
                    //check admin data
                    /* Check image or video is set or not */
                    if ($_FILES && !empty($_FILES['user_image']['name']) && isset($_FILES['user_image']['name'])) {
                        $files = $_FILES;

                        $imgarr = array();

                        $user_image_url = '';

                        //image check is it is set or not
                        if (!empty($_FILES['user_image']['name'])) {
                            //image insert array
                            $imgarr['user_image']['name'] = $files['user_image']['name'];
                            $imgarr['user_image']['type'] = $files['user_image']['type'];
                            $imgarr['user_image']['tmp_name'] = $files['user_image']['tmp_name'];
                            $imgarr['user_image']['error'] = $files['user_image']['error'];
                            $imgarr['user_image']['size'] = $files['user_image']['size'];

                            //upload in s3 server
                            $userImageUrl = upload_image_s3($imgarr['user_image'], '');

                            /**Image thumb block end*/
                        }
                    }
                    //$registrationNo = $this->generateRegistrationNo($this->input->post('distict'));


                    //insert user array
                    $insertUserArray = array(
                        //'registeration_no' => $registrationNo,
                        'full_name' => $this->input->post('first_name'),
                        'email_id' => $this->input->post('email'),
                        'password' => createPassword($postData['password']),
                        'facebook_id' => $this->input->post('fbId'),
                        'twitter_id' => $this->input->post('twitterId'),
                        'phone_number' => $this->input->post('phoneno'),
                        'whatsup_number' => $this->input->post('whatsupno'),
                        'paytm_number' => '', // $this->input->post('paytmno'),
                        'state' => DEFAULT_STATE_ID, // $this->input->post('state'),
                        'gender' => $this->input->post('gender'),
                        'district' => $this->input->post('distict'),
                        'user_image' => isset($userImageUrl) && !empty($userImageUrl) ? $userImageUrl : '',
                        'registered_on' => date('Y-m-d H:i:s'),
                        'is_number_verified' => 0,
                        'is_profile_edited' => 1,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'college' => '', //$this->input->post('college_name'),
                        'dob' => date('Y-m-d', strtotime(str_replace('-', '/', $postData['dob']))),
                        'referal_code' => $this->referalCodeGenerator($this->input->post('first_name'))
                    );
                    //COLLEGE OPTION CHECK
                    if (isset($postData['college_option']) && $postData['college_option'] == 'yes') {
                        if (isset($postData['college_name']) && $postData['college_name'] == 'others' && !empty($postData['collegeManName'])) {
                            $college_code = 0;
                            $whereArrCollg['where'] = ['college_name' => $postData['collegeManName'], 'state_id' => $this->input->post('state'), 'district_id' => $this->input->post('distict')];
                            $college_info = $this->Common_model->fetch_data('college_list', ['college_id'], $whereArrCollg, true);
                            $college_code = 0;
                            if (!empty($college_info)) {
                                $college_code = $college_info['college_id'];
                            } else {
                                $insertCollege = array(
                                    'state_id' => $this->input->post('state'),
                                    'district_id' => $this->input->post('distict'),
                                    'college_name' => $postData['collegeManName'],
                                    'status' => 2,
                                    'college_code' => ''
                                );
                                $college_code = $this->Common_model->insert_single('college_list', $insertCollege); #save users details values in DB
                            }
                            $insertUserArray['college'] = $college_code;
                        }
                    }
                    $this->db->trans_begin(); #DB transaction Start

                    //insert user details
                    $userId = $this->Common_model->insert_single('users', $insertUserArray);

                    $registrationId         = $this->common_function->getUserRegistrationNo($this->input->post('distict'),$userId);
                    if($registrationId != ''){
                        $updateArr          = array();
                        $updateArr['registeration_no']  = $registrationId;
                        $updateCond         = array('user_id'=> $userId);
                        $updateRes          = $this->common_model->updateTableData($updateArr,'users',$updateCond);
                        
                        $insertUserArray['registeration_no'] = $registrationId;
                    }

                    //referal code insert
                    $whereArry['where'] = ['referal_code' => $this->input->post('referalcode'), 'is_active' => ACTIVE];
                    $valid_referal_check = $this->Common_model->fetch_data('users', ['user_id'], $whereArry, true);

                    if (!empty($valid_referal_check)) {
                        //referal data insert array
                        $referalInsert = array(
                            "user_id" => $userId,
                            "referal_user_id" => $valid_referal_check['user_id'],
                            "created_date" => date('Y-m-d H:i:s')
                        );
                        $this->Common_model->insert_single('ipac_referal_user', $referalInsert); #save users details values in DB
                    }
                    //insert in user device detail
                    $insertDevice = array(
                        'user_id' => $userId
                    );
                    $this->common_function->writeDeviceDetailsLog('insert','admin :: User >> addUser ',$insertDevice);
                    $this->Common_model->insert_single('user_device_details', $insertDevice);

                    if (true === $this->db->trans_status()) {
                        #Comminting changes
                        $this->db->trans_commit();
                        #send welcome message to user


                        // $mailData = [];
                        // $mailData['name'] = $insertUserArray['full_name'];
                        // $mailData['email'] = $insertUserArray['email_id'];
                        // $mailData['registration_no'] = $insertUserArray['registeration_no'];
                        // $mailData['password'] = $postData['password'];
                        // $mailData['mailerName'] = 'welcome';
                        // $subject = 'Welcome to ' . PROJECT_NAME;
                        // $mailerName = 'welcome';
                        //  #sending welcome mail
                        //  // $this->sendWelcomeMail( $mailData );
                        // sendmail($subject, $mailData, $mailerName);


                        #setting Response Array
                        $this->session->set_flashdata('message_success', $this->lang->line('user_add_success'));
                        redirect(base_url() . 'admin/users');
                    } else {
                        #IF transaction failed
                        #rolling back
                        $this->db->trans_rollback();
                        $$this->session->set_flashdata('message_error', $this->lang->line('something_went_Worng'));
                        redirect(base_url() . 'admin/users');
                    }
                }
            }

            $data["csrfName"] = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;
            
            $data['state'] = $this->Common_model->fetch_data('state_list', array('state_id', 'state_name'), []);
            //load state helper
            $this->load->helper('state');
            //District list for goa
            $data["districtlist"] = get_district_list(DEFAULT_STATE_ID);
            load_views("users/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /* Generating user registration number
     * IPAC XXX PTA YYYY (IPAC, PTA are static and XXX, YYYY are variables)
        First 4 digits represents the company (IPAC) name
        XXX represents the district code (number)
        PTA represents as Part Time Associate
        YYYY represents the volunteer serial number (in respective districts)
     */
    // private function generateRegistrationNo($districtId)
    // {

    //     #generate 3 digit district code of user district
    //     $districtnum = 3;
    //     $district_num_padded = sprintf("%03d", $districtId);

    //     # count the number of users in a particular district
    //     $countUser = $this->News_model->userList($districtId);
    //     $usercnt = 0;
    //     if (!empty($countUser)) {
    //         $usercnt = $countUser->UserCnt;
    //     }
    //     $usercnt = $usercnt + 1;
    //     # generate 4 digit user count for a particular district
    //     $districtusernum = 4;
    //     $district_user_num_padded = sprintf("%04d", $usercnt);

    //     $prefix = 'STLN';
    //     $sufix = 'PTA';
    //     $registrationNo = $prefix . $district_num_padded . $sufix . $district_user_num_padded;
    //     return $registrationNo;
    // }


    /**
     *
     * @function userCsvImport
     * @Description upload user from csv formate
     * @return success
     */
    private function userCsvImport($file_path)
    {

        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->load->helper(array('s3', 'video_thumb'));

        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        //fir each loop
        foreach ($file_data as $row) {
            $date_time = date('Y-m-d H:i:s');

            $row = array_values($row);

            # check for required params
            $name = (isset($row[0]) && !empty($row[0])) ? trim($row[0]) : '';
            $email = (isset($row[1]) && !empty($row[1])) ? trim($row[1]) : '';
            $password = (isset($row[2]) && !empty($row[2])) ? trim($row[2]) : '';
            $phoneNumber = (isset($row[3]) && !empty($row[3])) ? trim($row[3]) : '';
            $whatsupNumber = (isset($row[4]) && !empty($row[4])) ? trim($row[4]) : '';
            $paytmNumber = (isset($row[5]) && !empty($row[5])) ? trim($row[5]) : '';
            $imgUrl = (isset($row[6]) && !empty($row[6])) ? trim($row[6]) : '';
            $state = (isset($row[7]) && !empty($row[7])) ? trim($row[7]) : '';
            $district = (isset($row[8]) && !empty($row[8])) ? trim($row[8]) : '';
            $user_gender = (isset($row[9]) && !empty($row[9])) ? trim($row[9]) : '';
            //$college = (isset($row[10]) && !empty($row[10])) ? trim($row[10]) : '';
            $dob = (isset($row[11]) && !empty($row[11])) ? trim($row[11]) : '';
            //required validation
            if (!$name || !$email || !$password || !$phoneNumber || !$whatsupNumber || !$state || !$district || !$user_gender || !$dob) {
                $message .= $this->lang->line('all_required');
                $not_saved_record = $not_saved_record + 1;
                break;
            } else {
                #valid email check
                if (!valid_email($email)) {
                    $message .= $this->lang->line('invalid_email');;
                    $not_saved_record = $not_saved_record + 1;
                    break;
                }
                #valid phone check
                if (!phone_number($phoneNumber)) {
                    $message .= $this->lang->line('phone_number_invalid');
                    $not_saved_record = $not_saved_record + 1;
                    break;
                }
                # check for unique email
                if ($email) {
                    $user_data = $this->Common_model->fetch_data('users', 'user_id', array('where' => array('email_id' => $email, 'is_active!=' => DELETED)), true);
                    //if email exist
                    if ($user_data) {
                        $message .= $this->lang->line('email_already_exist');
                        $not_saved_record = $not_saved_record + 1;
                        break;
                    }
                }
                # check for unique phone number
                if ($phoneNumber) {
                    $user_data = $this->Common_model->fetch_data('users', 'user_id', array('where' => array('phone_number' => $phoneNumber, 'is_active!=' => DELETED)), true);
                    //if email exist
                    if ($user_data) {
                        $message .=  $this->lang->line('phone_already_exist');
                        $not_saved_record = $not_saved_record + 1;
                        break;
                    }
                }

                //valid dob
                if (!(isValidDate($dob, 'd-m-Y'))) {
                    $message .= $this->lang->line('dob_formate');
                    $not_saved_record = $not_saved_record + 1;
                    break;
                }

                //future dob check
                $date_now = date("Y-m-d");
                if (date('Y-m-d', strtotime($dob)) > $date_now) {
                    $message .= $this->lang->line('future_dob_error');
                    $not_saved_record = $not_saved_record + 1;
                    break;
                }
                //whats up number validation
                if (!empty($whatsupNumber)) {
                    if (!(phone_number($whatsupNumber))) {
                        $message .= $this->lang->line('whats_number_invalid');
                        $not_saved_record = $not_saved_record + 1;
                        break;
                    }
                }
                if (!empty($paytmNumber)) {
                    if (!(phone_number($paytmNumber))) {
                        $message .= $this->lang->line('paytm_number_invalid');
                        $not_saved_record = $not_saved_record + 1;
                        break;
                    }
                }
                # check for state id
                $state_id = 0;
                if ($state) {
                    $state_data = $this->Common_model->fetch_data('state_list', 'state_id', array('where' => array('state_name' => $state)), true);

                    if ($state_data) {
                        $state_id = $state_data['state_id'];
                    } else {
                        $message .= $this->lang->line('invalid_state');
                        $not_saved_record = $not_saved_record + 1;
                        break;
                    }
                }

                # check for district of the state
                $district_code = 0;
                if ($state_id != 0 && $district) {
                    $district_data = $this->Common_model->fetch_data('district', 'district_code', array('where' => array('state_id' => $state_id, 'district_name' => $district)), true);

                    if ($district_data) {
                        $district_code = $district_data['district_code'];
                    } else {
                        $message .= $this->lang->line('invalid_district');
                        $not_saved_record = $not_saved_record + 1;
                        break;
                    }
                }
                # check for college of the district
                // $college_code = 0;
                // if ($state_id != 0 && $district_code != 0 && !empty($college)) {
                //     $college_data = $this->Common_model->fetch_data('college_list', 'college_id', array('where' => array('district_id' => $district_code, 'college_name' => $college)), true);

                //     if ($college_data) {
                //         $college_code = $college_data['college_id'];
                //     } else {
                //         $message .= $this->lang->line('invalid_college');
                //         $not_saved_record = $not_saved_record + 1;
                //         break;
                //     }
                // }
                # check for the gender
                $gender = 0; # default is 0. 0 means other
                if (strtolower($user_gender) == 'male') {
                    $gender = 1;
                } elseif (strtolower($user_gender) == 'female') {
                    $gender = 2;
                } elseif (strtolower($user_gender) == 'other') {
                    $gender = 3;
                }
                #if image url is set
                if (!empty($imgUrl)) {
                    $file_url = explode('/', $imgUrl);
                    $file_name = array_pop($file_url);

                    $file_extenstion = explode('.', $file_name);
                    $file_extenstion = array_pop($file_extenstion);
                    $file_extenstion = strtolower($file_extenstion);

                    if (!in_array($file_extenstion, ['jpg', 'jpeg', 'png'])) {
                        continue;
                    }

                    $file_type = 1;

                    // Getting the base name of the file.
                    $filename = basename($file_name);

                    $temp_folder_path = TMEP_FOLDER_PATH;
                    $file_path = $temp_folder_path . $filename;

                    $write_status = file_put_contents($file_path, file_get_contents($imgUrl));

                    if ($write_status) {
                        $s3_main_file_url = upload_image_s3(
                            ['tmp_name' => $file_path, 'name' => $filename],
                            mime_content_type($file_path)
                        );
                    }
                }
                $userId = '';
                if ($not_saved_record == 0) {
                    # save the record to the database
                    $ins_data = [
                        //'registeration_no' => $this->generateRegistrationNo($district_code),
                        'full_name' => $name,
                        'registered_on' => $date_time,
                        'updated_at' => $date_time,
                        'email_id' => $email,
                        'state' => $state_id,
                        'district' => $district_code,
                        'password' => createPassword($password),
                        'gender' => $gender,
                        'whatsup_number' => $whatsupNumber,
                        'paytm_number' => $paytmNumber,
                        'phone_number' => $phoneNumber,
                        'is_active' => ACTIVE,
                        'user_image' => (isset($s3_main_file_url) && !empty($s3_main_file_url) ? $s3_main_file_url : ''),
                        'dob' => date('Y-m-d', strtotime($dob)),
                        //'college' => $college_code,
                        'is_number_verified' => 0,
                        'is_profile_edited' => 1,
                        'referal_code' => $this->referalCodeGenerator($name)


                    ];
                    $userId = $this->Common_model->insert_single('users', $ins_data);
                }
                if ($userId) {

                    // $mailData = [];
                    // $mailData['name'] = $name;
                    // $mailData['email'] = $email;
                    // $mailData['registration_no'] = $ins_data['registeration_no'];
                    // $mailData['password'] = '';
                    // $mailData['mailerName'] = 'welcome';
                    // $subject = 'Welcome to ' . PROJECT_NAME;
                    // $mailerName = 'welcome';
                    //  #sending welcome mail
                    //  // $this->sendWelcomeMail( $mailData );
                    // sendmail($subject, $mailData, $mailerName);
                    # save the news media files
                    //  $this->news_media_files($news_image_url, $news_video_url, $newsId, $date_time);
                    $saved_record = $saved_record + 1;

                    $registrationId         = $this->common_function->getUserRegistrationNo($district_code,$userId);
                    if($registrationId != ''){
                        $updateArr          = array();
                        $updateArr['registeration_no']  = $registrationId;
                        $updateCond         = array('user_id'=> $userId);
                        $updateRes          = $this->common_model->updateTableData($updateArr,'users',$updateCond);
                        
                        $insertUserArray['registeration_no'] = $registrationId;
                    }

                } else {
                    $not_saved_record = $not_saved_record + 1;
                }
            }
        } // end foreach loop here


        # generate the message
        if ($saved_record > 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }
        return $message;
    } // end import function path

    /**
     *
     * @Function checkReferralCode
     * @Description check referal code
     */
    public function checkReferralCode()
    {
        $getData = $this->input->get();
        if (!empty($getData) && !empty($getData['referalcode'])) {
            $whereArry['where'] = ['referal_code' => $getData['referalcode'], 'is_active' => ACTIVE];
            $valid_referal_check = $this->Common_model->fetch_data('users', ['user_id'], $whereArry, true);
            if (empty($valid_referal_check)) {
                echo 'false';
                die;
            } else {
                echo 'true';
                die;
            }
        }
    }

    /*
     * @Function referalCodeGenerator
     * @description referal code generator
     * @param User name and name
     * @Created date 21-08-2018
     */
    private function referalCodeGenerator($name)
    {
        if (isset($name) && !empty($name)) {
            $Username = substr($name, 0, 2);
            $uniqecode = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6));
            $referalCode = strtoupper($Username . $uniqecode);
        }
        return $referalCode;
    }

    /**
     * @function sendMessage
     * @description send message to user phone no
     *
     * @param mobile number and message
     * @return boolean
     */
    private function sendMessage($user_mobile_no, $message)
    {
        $no = (string)$user_mobile_no;
        $no = str_replace("+", "", $no);
        $msg = $message;
        // $msg = urlencode($msg);

        $url = 'https://mobilnxt.in/api/push';
        $postData = array(
            'accesskey' => 'ClkakoPSzQHhq1AqwwjNdQfxtEcMjI',
            'from' => 'PTAAPP',
            'to' => $no,
            'text' => $msg,
            'url' => $url
        );
        $postString = json_encode($postData);
        sendPostRequest($postData);
    }

    /**
     *
     * @Function addReward
     * @Description for adding rewards to an user
     */
    public function addReward()
    {

        $data['admininfo']  = $this->admininfo;
        $postData           = $this->input->post();
        $userId             = $postData['user_id'];

        $responseArr                = array();
        $responseArr['success']     = 0;
        $responseArr['message']     = $this->lang->line('something_went_Worng');
        /*echo "<pre>";
        print_r($postData);exit;*/
        if (isset($postData) && !empty($postData)) {

            $this->db->trans_begin(); #DB transaction Start

            $params                 = array();
            $params['points']       = $postData['rewards'];
            $params['user_id']      = $userId;
            $updateRes              = $this->User_model->updateUserEarnPoints($params);

            $userData               = $this->User_model->userDetail($userId);
            $current_points         = $userData['points_earned'];
            $whereArr = ['fk_user_id' => $userId];
            $user_level             = $this->User_model->getUserLevels($userId);
            $current_level          = $user_level['fk_level_id'] ? $user_level['fk_level_id'] : '0';
                                
            if ($current_level != 'null') {
                $level_array = $this->nextlevel($current_points, $current_level);
                //print_r($level_array); exit;
                $difference = $level_array['max_level'] - $current_level;
                if ($difference < 0) {
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $current_level;
                    return $level_array;
                } else {
                    for ($i = 1; $i <= $difference; $i++) {
                        $fk_level_id = $current_level + $i;
                        $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                        $reward_points = $reward_points->row_array();
                        $free_rewards =  $reward_points['free_reward_points'];
                        if ($free_rewards != 0) {
                            $params                 = array();
                            $params['points']       = $free_rewards;
                            $params['user_id']      = $userId;
                            $res                    = $this->User_model->updateUserEarnPoints($params);

                            $walletInsertArr[]      = array(
                                'user_id'           => $userId,
                                'point'             => $free_rewards,
                                'title'             => 'New Level Rewards',
                                'description'       => 'Rewards earned for completion of levels',
                                'task_id'           => '0',
                                'type'              => 'bonus',
                                'status'            => '1',
                                'created_date'      => date('Y-m-d H:i:s'),
                                'updated_date'      => date('Y-m-d H:i:s')
                            );
                        }

                        $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $userId, 'unlock_date' => date('Y-m-d H:i:s')]);
                    }
                }
                if ($updateRes) {
                    $walletInsertArr[]          = array(
                        'user_id'               => $userId,
                        'point'                 => $postData['rewards'],
                        'title'                 => $postData['rewards_title'],
                        'description'           => $postData['rewards_desc'],
                        'task_id'               => '0',
                        'type'                  => 'bonus',
                        'status'                => '1',
                        'created_date'          => date('Y-m-d H:i:s'),
                        'updated_date'          => date('Y-m-d H:i:s')
                    );
                }

                if (!empty($walletInsertArr) && count($walletInsertArr) > 0) {
                    $this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
                }


                if (true === $this->db->trans_status()) {
                    #Comminting changes
                    $this->db->trans_commit();

                    $responseArr['success']     = 1;
                    $responseArr['message']     = "Rewards added successfully.";
                }
            }
        }
        echo json_encode($responseArr);
        exit;
    }

    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }
}
