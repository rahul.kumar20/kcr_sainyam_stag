<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Form_task extends MY_Controller
{

    private $admininfo = "";
    private $data = array();
    var $fields;/** columns names retrieved after parsing */
    var $separator = ';';/** separator used to explode each line */
    var $enclosure = '"';/** enclosure used to decorate each field */
    var $max_row_size = 4096;

    public function __construct()
    {



        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->library("Csvimport");
        $this->load->model("Form_model");
    }


    public function index()
    {
        $data['admininfo'] = $this->admininfo;

        if ($this->input->post()) {
            pr($this->input->post());
        } else {
            $pcArr = $this->Form_model->fetchPc();
            $data['pc_arr'] = !empty($pcArr) ? $pcArr : array();
            load_views("form/index", $data);
        }
    }

    public function demo()
    {
        $data['admininfo'] = $this->admininfo;

        if ($this->input->post()) {
            pr($this->input->post());
        } else {
            load_views("form/demo", $data);
        }
    }

    public function csvUpload()
    {
        $data['admininfo'] = $this->admininfo;
        $allowed_ext = array("csv");

        if ($this->input->is_ajax_request()) {
            if (!empty($_FILES['file'])) {
                $extension = explode(".", $_FILES["file"]["name"]);
                $file_extension = end($extension);

                if (!in_array($file_extension, $allowed_ext)) {
                    echo json_encode(['code' => 400, 'msg' => 'Extension is not valid.']);
                    exit;
                }


                //pr($_FILES['file']);
                $filename = $_FILES['file']['name'];
                $filetype = $_FILES['file']['type'];
                $filetmpname = $_FILES['file']['tmp_name'];
                $fileerror = $_FILES['file']['error'];
                $filesize = $_FILES['file']['size'];

                $timestamp = strtotime("now");


                $csvObj = new Csvimport();
                $contentFormArr = $csvObj->get_array($filetmpname, false, 1);

                if (!empty($contentFormArr)) {
                    foreach ($contentFormArr as $key => $value) {
                        $insertOption['option_value'] = $value[key($value)];
                        $insertOption['code'] = $timestamp;
                        $insertOption['option_code'] = randomKey(4);

                        $insert[] = $insertOption;
                    }

                    //inserting data to batch
                    $reponse = $this->db->insert_batch('tmp_options', $insert);
                    if ($reponse) {
                        echo json_encode(['code' => 200, 'msg' => 'upload successfully', 'timestamp' => $timestamp]);
                    } else {
                        echo json_encode(['code' => 400, 'msg' => 'File upload failed.', 'timestamp' => $timestamp]);
                    }
                    exit();
                } else {
                    echo json_encode(['code' => 400, 'msg' => 'Can not upload blank file.']);
                    exit;
                }
            }
        } else {
            //$this->load->view('form/cropper');
            load_views("form/csv_upload", $data);
        }
    }
    
    
    public function csvDependentUpload()
    {
        $data['admininfo'] = $this->admininfo;
        $allowed_ext = array("csv");

        if ($this->input->is_ajax_request()) {
            if (!empty($_FILES['file'])) {
                $extension = explode(".", $_FILES["file"]["name"]);
                $file_extension = end($extension);

                if (!in_array($file_extension, $allowed_ext)) {
                    echo json_encode(['code' => 400, 'msg' => 'Extension is not valid.']);
                    exit;
                }


                $filename = $_FILES['file']['name'];
                $filetype = $_FILES['file']['type'];
                $filetmpname = $_FILES['file']['tmp_name'];
                $fileerror = $_FILES['file']['error'];
                $filesize = $_FILES['file']['size'];

                $timestamp = strtotime("now");


                $csvObj = new Csvimport();
                $contentFormArr = $csvObj->get_array($filetmpname, false, 1);
                if (!empty($contentFormArr)) {
                    //saving question2 values.
                    foreach ($contentFormArr as $value) {
                        if (isset($value['column2']) && !empty($value['column2'])) {
                            $question2 = explode(",", $value['column2']);
                            $parent_code = explode("-", $value['column1']);
                            foreach ($question2 as $k => $v) {
                                $insertOption['option_value'] = $v;
                                $insertOption['code'] = $timestamp;
                                $insertOption['parent_id'] = $parent_code[0];
                                $insertOption['option_code'] = randomKey(4);
                                $insertOption['next_question_status'] =1;

                                $insert[] = $insertOption;
                            }
                            $next_question = (isset($value['column3(1=>"Show Next Question",2=>"Hide Next Question")']) && !empty($value['column3(1=>"Show Next Question",2=>"Hide Next Question")'])?$value['column3(1=>"Show Next Question",2=>"Hide Next Question")']:1);
                            $isSuccess = $this->Common_model->update_single("tmp_options", ['next_question_status'=>$next_question], ['where'=>['option_code'=>$parent_code[0]]]);
                        }
                    }

                    //inserting data to batch
                    $reponse = $this->db->insert_batch('tmp_options', $insert);
                    if ($reponse) {
                        echo json_encode(['code' => 200, 'msg' => 'upload successfully', 'timestamp' => $timestamp]);
                    } else {
                        echo json_encode(['code' => 400, 'msg' => 'File upload failed.', 'timestamp' => $timestamp]);
                    }
                    exit();
                } else {
                    echo json_encode(['code' => 400, 'msg' => 'Can not upload blank file.']);
                    exit;
                }
            }
        } else {
            //$this->load->view('form/cropper');
            load_views("form/csv_upload", $data);
        }
    }

    
    /**
     * Creating preview for the form.
     *
     **/
    public function formPreview()
    {
        
        
        
        if ($this->input->is_ajax_request()) {
            $postedData =  $this->input->post();
            $data['data'] = $postedData;
            $loadView =  $this->load->view("form/preview", $data, true);

            echo $loadView;
            die;
        }
    }


    // get dynamic form html.
    public function createSurveyForm()
    {
        $tmp_options = [];
        $data['admininfo'] = $this->admininfo;
        $get = $this->input->get();
        $data['states'] = $this->Common_model->fetch_data("state_list", "*");

        if (isset($get['export']) && !empty($get['export'])) {
            $this->exportCsv($get);
        }


        if ($this->input->post()) {
            //$this->form_validation->set_rules('question[]', "Questions can ", 'trim|required');
            //$this->form_validation->set_rules('dependency', $this->lang->line('old_pass'), 'trim|required');
            //$this->form_validation->set_rules('optionCode', $this->lang->line('old_pass'), 'trim|required');
            
            
            $postedData = $this->input->post();
            $this->db->trans_begin();
            
            // save form
            
            $form['f_title'] = $postedData['form_title'];
            $form['f_desc'] = $postedData['description'];
            $form['steps'] = isset($postedData['steps'])?$postedData['steps']:0;
            $form['mobile_verification'] = (isset($postedData['mobileVerification']) && !empty($postedData['mobileVerification'])?$postedData['mobileVerification']:2);

            $form['created_date'] = date("Y-m-d H:i:s");
            //insert form
            $formId =  $this->Common_model->insert_single("forms", $form);
            
            $formUrl = base_url().'survey-form?formid='.$formId.'&type=1';
            
            //update form
            
            $formUpdateId = $this->Common_model->update_single("forms", ['form_url'=>$formUrl], ['where'=>['fid'=>$formId]]);
            $d=0;
            if (isset($postedData['question']) && !empty($postedData['question'])) {
                $questionId=[];
                foreach ($postedData['question'] as $key => $value) {
                    if (isset($postedData['relation'][$key]) && $postedData['relation'][$key]==1) {
                        $question['text'] =  $value;
                        $question['type'] =  isset($postedData['questionType'][$key])?$postedData['questionType'][$key]:"checkbox";
                        $question['status']=1;
                        $question['fid']=$formId;
                        $question['required']=1;
                        $question['created_at']=date("Y-m-d H:i:s");
                        $question['parent_qid']=isset($questionId[$postedData['dependency'][$key]])?$questionId[$postedData['dependency'][$key]]:0;
                        
                        if ($questionId[$postedData['dependency'][$key]]) {
                            $qusPaId  =$questionId[$postedData['dependency'][$key]];
                            $levelOrder = $this->Common_model->fetch_data("questions", "depth,qid", ['where'=>['parent_qid'=>$qusPaId],'order_by'=>["created_at"=>"DESC"]], true);
                            
                            if ($levelOrder) {
                                $question['depth'] = $levelOrder['depth'] +1;
                            } else {
                                $levelOrder = $this->Common_model->fetch_data("questions", "depth,qid", ['where'=>["qid"=>$qusPaId]], true);
                                
                            //  $this->db->where('qid !=',$qusPaId );
                                //$this->db->set("depth","depth+1",false);
                                //$this->db->update("questions");
                                $question['depth'] = $levelOrder['depth'] +1;
                            }
                        }
                        
                        $questionId[] =$qusId =  $this->Common_model->insert_single("questions", $question);
                        if ($qusId) {
                            $questionIdsList = $this->Common_model->fetch_data("questions", "qid", ['where'=>["depth>"=>$levelOrder['depth'],'qid!='=>$qusId,'fid'=>$formId]]);
                            foreach ($questionIdsList as $qlist) {
                                $this->db->where('qid', $qlist['qid']);
                                $this->db->set("depth", "depth+1", false);
                                $this->db->update("questions");
                            }
                        }
                    } else {
                        $levelOrder2 = $this->Common_model->fetch_data("questions", "depth,qid", ['where'=>['fid'=>$formId],'order_by'=>["created_at"=>"DESC"]], true);
                         
                        
                            
                           $question['depth']=$d;
                        
                        $question['text'] =  $value;
                        $question['type'] =  isset($postedData['questionType'][$key])?$postedData['questionType'][$key]:"checkbox";
                        $question['status']=1;
                        $question['fid']=$formId;
                        $question['parent_qid'] =0;

                        $question['required']=1;
                        $question['created_at']=date("Y-m-d H:i:s");
                        
                        $questionId[] = $qusId = $this->Common_model->insert_single("questions", $question);
                    }
                    
                    if (isset($postedData['optionCode'][$key])) {
                        //getting option values
                        
                        $tmp_options = $this->Common_model->fetch_data("tmp_options", "*", ['where'=>['code'=>$postedData['optionCode'][$key]]]);
                    }
                    // saving questions with options
                    if (!empty($tmp_options)) {
                        $allOptions= array();
                        foreach ($tmp_options as $k => $v) {
                            $options['description'] = $v['option_value'];
                            $options['qid'] = $qusId;
                            $options['option_code'] = $v['option_code'];
                            $options['parent_code'] = $v['parent_id'];
                            $options['next_question_status'] = $v['next_question_status'];

                            $allOptions[] = $options;
                        }
                       
                        //insert qus options batch
                        $batchId =  $this->db->insert_batch('options', $allOptions);
                    }
                    $d++;
                }
                
                if ($this->db->trans_status() == true) {
                    $this->db->trans_commit();
                    
                    //truncate the tmp_options table
                    $this->db->truncate('tmp_options');
                    
                    
                    redirect(base_url()."admin/form_list");
                } else {
                    $this->db->trans_rollback();
                    load_views("form/welcome");
                }
            } else {
                load_views("form/index_new", $data);
            }
        } else {
            load_views("form/index_new", $data);
        }
    }

    public function exportCsv($get)
    {
        $rows = [];

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=question'.$get['counter'].'.csv');
        $output = fopen('php://output', 'w');
        // output the column headings
        fputcsv($output, array('column1'));
        //$rows[0] = "test";
        //$rows[1] = "test";
        @fputcsv($output, $rows);
        fclose($output);
        exit();
    }
    
    public function generateDependentCsv()
    {
        $rows = [];
        $get  = $this->input->get();
        
        $timeStamp = isset($get['timestamp'])?$get['timestamp']:"";

        //get option values as per the time stamp.
        
        $options = $this->Common_model->fetch_data("tmp_options", "option_value,option_code", ['where'=>['code'=>trim($timeStamp)]]);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=question'.$get['timestamp'].'.csv');
        $output = fopen('php://output', 'w');
        // output the column headings
        fputcsv($output, array('column1','column2' ,'column3(1=>"Show Next Question",2=>"Hide Next Question")'));
        
        if (!empty($options)) {
            // put the data into the csv
            foreach ($options as $row) {
                    $rowData[] = $row['option_code'].'-'.$row['option_value'];
                fputcsv($output, $rowData);
                unset($rowData);
            }
            
            //@fputcsv($output, $rows);
        }
        //$rows[0] = "test";
        //$rows[1] = "test";
        
        fclose($output);
        exit();
    }


    public function getListOption()
    {

        $get=$this->input->get();
        if (!empty($get)) {
            $option=$get['option']?$get['option']:'';

            if ($option=='pc') {
                $responseArr=$this->Form_model->getPcList();
            }

            if ($option=='ac') {
                $type = isset($get['type'])?$get['type']:"";
                $responseArr=$this->Form_model->getAcList($get['value'], $type);
            }
            
            if ($option=='ac2') {
                $responseArr=$this->Form_model->getAcList2($get['value']);
            }

            if ($option=='st') {
                $responseArr=$this->Form_model->getStateList();
            }

            if ($option=='dis') {
                $responseArr=$this->Form_model->getDistrictList($get['value']);
            }
            
            if ($option=='cur-dis') {
                $responseArr=$this->Form_model->getDistrictList2($get['value']);
            }

            if ($option=='col') {
                $responseArr=$this->Form_model->getCollegeList();
            }

            echo $responseArr;
            die;
        }
    }
    
    
    public function thankyou()
    {
        
        load_views("form/welcome");
    }
}
