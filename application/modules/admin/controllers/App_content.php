<?php

defined('BASEPATH') or exit('No direct script access allowed');

class App_content extends MY_Controller
{

    private $admininfo = null;

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('common', "english");
    }

    
    /**
     * @function aboutUs
     * @Description load about us page
     * @respnse view page
     * @result void
     */
    public function aboutUs()
    {
        $get = $this->input->get();
        $where['where'] = ['name' => 'About Us', 'status' => ACTIVE];
        if(empty($get) || $get['lang']=='en')
            $contents = $this->Common_model->fetch_data('page_master', 'content', $where, true);
        else {
            $contents = $this->Common_model->fetch_data('page_master', 'content_ta', $where, true);
            if(empty($contents['content_ta'])){
                $contents = $this->Common_model->fetch_data('page_master', 'content', $where, true);
            } else {
                $contents['content'] = $contents['content_ta'];
            }
        }
        $data['content'] = $contents;
        $this->load->view('app_content/about-us',$data);
    }

     /**
     * @function privacy
     * @Description load privacy us page
     * @respnse view page
     * @result void
     */
    public function privacy()
    {
        $get = $this->input->get();
        $where['where'] = ['name' => 'Privacy  Policy', 'status' => ACTIVE];
        if(empty($get) || $get['lang']=='en')
            $contents = $this->Common_model->fetch_data('page_master', 'content', $where, true);
        else {
            $contents = $this->Common_model->fetch_data('page_master', 'content_ta', $where, true);
            if(empty($contents['content_ta'])){
                $contents = $this->Common_model->fetch_data('page_master', 'content', $where, true);
            } else {
                $contents['content'] = $contents['content_ta'];
            }
        }
        $data['content'] = $contents;
        $this->load->view('app_content/privacy-policy',$data);
    }

     /**
     * @function term
     * @Description load term us page
     * @respnse view page
     * @result void
     */
    public function term()
    {
        $get = $this->input->get();
        //get term and condition detail
        $where = array('where' => array('status' =>ACTIVE));
        $where['order_by'] = ['created_date' => 'desc'];
        // $contents = $this->Common_model->fetch_data('ipac_term_condition', ['*' ], $where, true);
        if(empty($get) || $get['lang']=='en')
            $contents = $this->Common_model->fetch_data('ipac_term_condition', 'description', $where, true);
        else {
            $contents = $this->Common_model->fetch_data('ipac_term_condition', 'description_ta', $where, true);
            if(empty($contents['description_ta'])){
                $contents = $this->Common_model->fetch_data('ipac_term_condition', 'description', $where, true);
            } else {
                $contents['description'] = $contents['description_ta'];
            }
        }
        $data['content'] = $contents;
        $this->load->view('app_content/term-n-condition', $data);
    }

     /**
     * @function faq
     * @Description load faq us page
     * @respnse view page
     * @result void
     */
    public function faq()
    {
        $get = $this->input->get();
        $where['where'] = ['status' => ACTIVE];
        $contents = $this->Common_model->fetch_data('faq_management', '*', $where);
        $data['content'] = $contents;
        $data['lang'] = empty($get)?'':$get['lang'];
        $this->load->view('app_content/faq',$data);
    }
}
