<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Form_test extends MY_Controller
{
    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Common_model');
    }





    /**
     * Add news in database
     *
     * @Function addNews
     * @Description add news
     * @Return view page
     */
    public function index()
    {
        try {
          $fectParentQuestion = $this->Common_model->fetch_data("questions","*",['where'=>['fid'=>2,'parent_qid'=>0]]);
		  if(!empty($fectParentQuestion)) {
			  $questionArray = array();
			  foreach($fectParentQuestion as $value) {
				  $questionChild = $this->Common_model->fetch_data("questions","*",['where'=>['fid'=>2,'parent_qid'=>$value['qid']]]);
				  if(!empty($questionChild)){
                    foreach($questionChild as $childvalue) {
							$questionArray[$value['qid']][] = $childvalue['qid'];
					} 
				  }else {
						$questionArray[$value['qid']] = 0;
					}
			  
			  }
		  }

		  pr($questionArray);

        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
