<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
require_once APPPATH . "/libraries/phpqrcode/qrlib.php";

class Task extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('s3', 'video_thumb'));

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Task_model');
        $this->load->model('News_model');
        $this->load->model('User_model');
        $this->lang->load('common', 'english');
        #import form validation confog file
        $this->load->config("form_validation_admin");
        # import csv file library
        $this->load->library('csvimport');
    }

    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {
        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            # upload csv content

            # upload csv content

            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv'))
                && (isset($_FILES['task_csv_file']['name']) && !empty($_FILES['task_csv_file']['name']))
            ) {
                if ($_FILES['task_csv_file']['type'] == 'text/csv') {
                    $msg = $this->task_csv_import($_FILES['task_csv_file']['tmp_name']);
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }

                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . 'admin/task');
            }

            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "LivestartDate" => "",
                "LiveendDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                //"state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                'taskType' => "",
                'taskAct' => ""

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END



            $taskList = $this->Task_model->taskList($defaultValue);
            $taskListDownload = $this->Task_model->taskListDownload($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($taskListDownload['result']);
            } //IF 3 END

            $totalrows = $taskList['total'];
            $data['tasklist'] = $taskList['result'];
            $defaultValue['taskType'] = !empty($defaultValue['taskType']) ? $defaultValue['taskType'] : '';
            $response = $this->Task_model->getTotalCount($defaultValue);
            $data['taskTotalData'] = $response['result'];
            // Manage Pagination
            $pageurl = 'admin/task';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_completed"] = $data["order_by_enddate"] = $data["order_by_startdate"] = $data["order_by_points"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "completed":
                            $data["order_by_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "point":
                            $data["order_by_points"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['LivestartDate'] = $defaultValue['LivestartDate'];
            $data['LiveendDate'] = $defaultValue['LiveendDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['taskType'] = $defaultValue['taskType'];

            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            $data['GENDER_TYPE'] = unserialize(GENDER_TYPE);

            //print_r($data['LivestartDate']); exit;
            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$taskList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/task?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }
            $data['uid'] = $defaultValue['uid'];
            //$data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['permission'] = $GLOBALS['permission'];
            /*//load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //District list for goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);*/
            //$data['statelist']      = array();
            $data['districtlist']   = array();
            $data['uidlist']        = array();
            load_views("task/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     * @name addTask
     * @description This method is used to add new task.
     */
    public function addTask()
    {
        try {
            $this->load->library('Common_function');
            $this->load->helper(array('s3', 'video_thumb'));
            $data['admininfo'] = $this->admininfo;

            $postdata = $this->input->post();
            if (isset($postdata) && !empty($postdata)) {

                /** VALIDATE POST DATA */
                $this->form_validation->set_rules($this->config->item("add_task"));
                if ($this->form_validation->run() == true) {
                    $action = 0;
                    if (isset($postdata['faction']) && !empty($postdata['faction']) && $postdata['taskType'] == FACEBOOK_TASK) {
                        $action = $postdata['faction'];
                    }
                    if (isset($postdata['taction']) && !empty($postdata['taction']) && $postdata['taskType'] == TWITTER_TASK) {
                        $action = $postdata['taction'];
                    }
                    if (isset($postdata['youtbaction']) && !empty($postdata['youtbaction']) && $postdata['taskType'] == YOUTUBE_TASK) {
                        $action = $postdata['youtbaction'];
                    }
                    if (isset($postdata['whatsappaction']) && !empty($postdata['whatsappaction']) && $postdata['taskType'] == WHATSAPP) {
                        $action = $postdata['whatsappaction'];
                    }
                    if (isset($postdata['instagramaction']) && !empty($postdata['instagramaction']) && $postdata['taskType'] == 8) {
                        $action = $postdata['instagramaction'];
                    }
                    if (isset($postdata['offlineaction']) && !empty($postdata['offlineaction']) && $postdata['taskType'] == OFFLINE) {
                        $action = $postdata['offlineaction'];
                    }
                    if (isset($postdata['onlineaction']) && !empty($postdata['onlineaction']) && $postdata['taskType'] == ONLINE) {
                        $action = $postdata['onlineaction'];
                    }
                    if (isset($postdata['actiondefault']) && !empty($postdata['actiondefault']) && $postdata['taskType'] == 7) {
                        $action = $postdata['actiondefault'];
                    }

                    /** INSTRUCTION VIDEO UPLOAD START HERE */
                    if (isset($_FILES['instruction_video_url']['name'])) {
                        $i = 0;
                        $instruction_video_url = '';
                        $vidarr = array();
                        if (!empty($_FILES['instruction_video_url']['name'][$i])) {
                            $allowed = array('m4v', 'avi', 'mpg', 'mp4', 'webm');
                            $filename = $_FILES['instruction_video_url']['name'][$i];
                            $ext = pathinfo($filename, PATHINFO_EXTENSION);
                            if (in_array($ext, $allowed)) {
                                $vidarr['instruction_video_url']['name'] = $_FILES['instruction_video_url']['name'][$i];
                                $vidarr['instruction_video_url']['type'] = $_FILES['instruction_video_url']['type'][$i];
                                $vidarr['instruction_video_url']['tmp_name'] = $_FILES['instruction_video_url']['tmp_name'][$i];
                                $vidarr['instruction_video_url']['error'] = $_FILES['instruction_video_url']['error'][$i];
                                $vidarr['instruction_video_url']['size'] = $_FILES['instruction_video_url']['size'][$i];
                                $mediatype = VIDEO;
                                $instruction_video_url = upload_image_s3($vidarr['instruction_video_url'], '');
                            }
                        }
                    }

                    $whatsappurl1 = '';
                    $whatsappurl2 = '';
                    /** WHATSAPP PROFILE PICTURE UPLOAD */
                    if (isset($_FILES['mul_whatsapp']['name'])) {
                        $files = $_FILES;
                        $imgarr = array();
                        $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG');
                        for ($i = 0; $i < count($_FILES['mul_whatsapp']['name']); $i++) {
                            if (!empty($_FILES['mul_whatsapp']['name'][$i])) {
                                $filename = $_FILES['mul_whatsapp']['name'][$i];
                                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                if (in_array($ext, $allowed)) {
                                    $imgarr['mul_whatsapp']['name'] = $files['mul_whatsapp']['name'][$i];
                                    $imgarr['mul_whatsapp']['type'] = $files['mul_whatsapp']['type'][$i];
                                    $imgarr['mul_whatsapp']['tmp_name'] = $files['mul_whatsapp']['tmp_name'][$i];
                                    $imgarr['mul_whatsapp']['error'] = $files['mul_whatsapp']['error'][$i];
                                    $imgarr['mul_whatsapp']['size'] = $files['mul_whatsapp']['size'][$i];

                                    $task_whatsapp_url = upload_image_s3($imgarr['mul_whatsapp'], '');
                                    if ($i == 0) {
                                        $whatsappurl1 = $task_whatsapp_url;
                                    } else {
                                        $whatsappurl2 = $task_whatsapp_url;
                                    }
                                }
                            }
                        }
                    }

                    $is_send_notif = $this->input->post('is_send_notif');
                    $is_push_notif_sent = isset($is_send_notif) ? 0 : 1;
                    $notification_update = isset($is_send_notif) ? 0 : 1;
                    /** INSERT DATA INTO TASK MASTER TABLE */
                    $insertNewsArray = array(
                        'task_title' => $this->input->post('taskName'),
                        'task_description' => $this->input->post('taskDescription'),
                        'notif_title' => !empty($this->input->post('notif_title')) ? $this->input->post('notif_title') : '',
                        'notif_description' => !empty($this->input->post('notif_desc')) ? $this->input->post('notif_desc') : '',
                        'greeting_message' => !empty($this->input->post('greeting')) ? $this->input->post('greeting') : '',
                        'is_send_notif' => isset($is_send_notif) ? 1 : 0,
                        'post_url' => $this->input->post('url'),
                        'post_content' => $this->input->post('post_content'),
                        'points' => !empty($this->input->post('taskPoints')) ? $this->input->post('taskPoints') : '0',
                        'start_date' => !empty($this->input->post('startDate')) ? date('Y-m-d H:i:s', strtotime($this->input->post('startDate'))) : '',
                        'end_date' => !empty($this->input->post('endDate')) ? date('Y-m-d H:i:s', strtotime($this->input->post('endDate'))) : '',
                        'state' => !empty($this->input->post('state')) ? $this->input->post('state') : '0',
                        'district' => !empty($this->input->post('distict')) ? $this->input->post('distict') : '0',
                        'ac' => !empty($this->input->post('ac')) ? $this->input->post('ac') : '0',
                        'gender' => !empty($this->input->post('gender')) ? $this->input->post('gender') : '0',
                        'registeration_no' => !empty($this->input->post('uid')) ? $this->input->post('uid') : '0',
                        'task_type' => !empty($this->input->post('taskType')) ? $this->input->post('taskType') : '',
                        'task_status' => !empty($this->input->post('status')) ? $this->input->post('status') : '0',
                        'college' => !empty($this->input->post('college_name')) ? $this->input->post('college_name') : '0',
                        'action' => $action,
                        'created_date' => date('Y-m-d H:i:s'),
                        'latitude' => !empty($this->input->post('latitude')) ? $this->input->post('latitude') : '0',
                        'longitude' => !empty($this->input->post('longitude')) ? $this->input->post('longitude') : '0',
                        'radius' => !empty($this->input->post('radius')) ? $this->input->post('radius') : '0',
                        'video_id' => !empty($this->input->post('videoId')) ? $this->input->post('videoId') : '0',
                        'channel_id' => !empty($this->input->post('channelId')) ? $this->input->post('channelId') : '0',
                        'twitter_follow_id' => !empty($this->input->post('twiiterAccountID')) ? $this->input->post('twiiterAccountID') : '0',
                        'twitter_follow_name' => !empty($this->input->post('twitterAccountName')) ? $this->input->post('twitterAccountName') : '0',
                        'facebook_follow_id' => !empty($this->input->post('facebookAccountID')) ? $this->input->post('facebookAccountID') : '0',
                        'facebook_follow_name' => !empty($this->input->post('facebookAccountName')) ? $this->input->post('facebookAccountName') : '0',
                        'instruction_video_url' => !empty($instruction_video_url) ? $instruction_video_url : '',
                        'instruction_description' => !empty($this->input->post('instruction_description')) ? $this->input->post('instruction_description') : '',
                        'form_id' => !empty($this->input->post('onlineform')) ? $this->input->post('onlineform') : '0',
                        'form_steps' => !empty($this->input->post('no_of_time')) ? $this->input->post('no_of_time') : '0',
                        'whatsapp_image_url1' => $whatsappurl1,
                        'whatsapp_image_url2' => $whatsappurl2,
                        'show_popup' => $this->input->post('show_popup'),
                        'is_push_notif_sent' => $is_push_notif_sent,
                        'notification_update' => $notification_update
                    );

                    $taskAllorSu = 0;
                    if (!empty($insertNewsArray['state']) || !empty($insertNewsArray['district']) || !empty($insertNewsArray['district']) || !empty($insertNewsArray['gender']) || !empty($insertNewsArray['registeration_no'])) {
                        $taskAllorSu = 1;
                    }

                    $this->db->trans_begin(); #DB transaction Start

                    /** INSERT DATA INTO TASK MASTER AND RETURN TASK ID */
                    $taskId = $this->Common_model->insert_single('ipac_task_master', $insertNewsArray);

                    $task_media_set = array();
                    $insertNewsImage = array();
                    $insertGalleryMedia = array();
                    $allowedAction = array('6', '2', '8', '20', '15', '16', '18', '22');
                    if (in_array($action, $allowedAction)) {
                        /** UPLOAD IMAGES */
                        if (isset($_FILES['news_image']['name'])) {
                            $files = $_FILES;
                            $_FILES['news_image']['name'] = array_unique($_FILES['news_image']['name']);
                            $imgarr = array();
                            $allowed = array('png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG', 'gif', 'GIF');
                            for ($i = 0; $i < count($_FILES['news_image']['name']); $i++) {
                                if (!empty($_FILES['news_image']['name'][$i])) {
                                    $filename = $_FILES['news_image']['name'][$i];
                                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                    if (in_array($ext, $allowed)) {
                                        $imgarr['news_image']['name'] = $files['news_image']['name'][$i];
                                        $imgarr['news_image']['type'] = $files['news_image']['type'][$i];
                                        $imgarr['news_image']['tmp_name'] = $files['news_image']['tmp_name'][$i];
                                        $imgarr['news_image']['error'] = $files['news_image']['error'][$i];
                                        $imgarr['news_image']['size'] = $files['news_image']['size'][$i];

                                        $task_media_files = upload_image_s3($imgarr['news_image'], '');
                                        $mediatype = IMAGE;

                                        $tempName = $_FILES['news_image']['tmp_name'][0];
                                        $imageName = $files['news_image']['name'][0];
                                        $uploads = UPLOAD_IMAGE_PATH;
                                        $pathComplete = $uploads;
                                        $isSuccess = $this->common_function->uploadImg($tempName, $pathComplete, $imageName);
                                        $thumb_url = '';

                                        if ($isSuccess) {
                                            $imgPath = UPLOAD_IMAGE_PATH . $isSuccess;
                                            $flag = $this->common_function->thumb_create($isSuccess, $imgPath, UPLOAD_THUMB_IMAGE_PATH);
                                            $thumbarr = array();
                                            $thumbarr['name'] = $isSuccess;
                                            $thumbarr['tmp_name'] = UPLOAD_THUMB_IMAGE_PATH . $isSuccess;
                                            $thumb_url = upload_image_s3($thumbarr, '');
                                        }

                                        $task_media_set[$i]['media_url'] = $task_media_files;
                                        $task_media_set[$i]['media_type'] = $mediatype;
                                        $task_media_set[$i]['media_thumb'] = $thumb_url;

                                        $insertNewsImage[$i] = array(
                                            'task_url' => $task_media_files,
                                            'task_thumb' => $thumb_url,
                                            'task_media_type' => $mediatype,
                                            'task_id' => $taskId,
                                            'created_date' => date('Y-m-d H:i:s')
                                        );

                                        $insertGalleryMedia[$i] = array(
                                            'gallary_media_url' => $task_media_files,
                                            'media_thumb' => $thumb_url,
                                            'media_type' => $mediatype,
                                            'type' => TASK_SECTION,
                                            'inserted_on' => date('Y-m-d H:i:s')
                                        );
                                    }
                                }
                            }
                        }

                        /** UPLOAD VIDEOS */
                        if (isset($_FILES['file']['name'])) {
                            $i = 0;
                            $vidarr = array();
                            if (!empty($_FILES['file']['name'][$i])) {
                                $allowed = array('m4v', 'avi', 'mpg', 'mp4', 'webm');
                                $filename = $_FILES['file']['name'][$i];
                                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                if (in_array($ext, $allowed)) {
                                    $vidarr['file']['name'] = $_FILES['file']['name'][$i];
                                    $vidarr['file']['type'] = $_FILES['file']['type'][$i];
                                    $vidarr['file']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                                    $vidarr['file']['error'] = $_FILES['file']['error'][$i];
                                    $vidarr['file']['size'] = $_FILES['file']['size'][$i];

                                    $mediatype = VIDEO;
                                    $index = count($insertNewsImage);
                                    $imageThumb = '';
                                    // $imageThumb = create_video_thumb($_FILES['file']['tmp_name']);
                                    $videoFile = upload_image_s3($vidarr['file'], '');
                                    $insertNewsImage[$index] = array(
                                        'task_url' => $videoFile,
                                        'task_thumb' => '',
                                        // 'task_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                                        'task_media_type' => $mediatype,
                                        'task_id' => $taskId,
                                        'created_date' => date('Y-m-d H:i:s')
                                    );

                                    $task_media_set[$index]['media_url'] = $videoFile;
                                    $task_media_set[$index]['media_type'] = $mediatype;
                                    $task_media_set[$index]['media_thumb'] = '';

                                    $insertGalleryMedia[$index] = array(
                                        'gallary_media_url' => $videoFile,
                                        'media_thumb' => '',
                                        // 'media_thumb' => s3_video_thumb_uploader($imageThumb, ''),
                                        'media_type' => $mediatype,
                                        'type' => TASK_SECTION,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                    // unlink($imageThumb);
                                }
                            }
                        }

                        /** UPLOAD AUDIOS */
                        if (isset($_FILES['mul_audio']['name'])) {
                            $i = 0;
                            $audioarr = array();
                            if (!empty($_FILES['mul_audio']['name'][$i])) {
                                $allowed = array('mp3');
                                $filename = $_FILES['mul_audio']['name'][$i];
                                $ext = pathinfo($filename, PATHINFO_EXTENSION);
                                if (in_array($ext, $allowed)) {
                                    $audioarr['file']['name'] = $_FILES['mul_audio']['name'][$i];
                                    $audioarr['file']['type'] = $_FILES['mul_audio']['type'][$i];
                                    $audioarr['file']['tmp_name'] = $_FILES['mul_audio']['tmp_name'][$i];
                                    $audioarr['file']['error'] = $_FILES['mul_audio']['error'][$i];
                                    $audioarr['file']['size'] = $_FILES['mul_audio']['size'][$i];

                                    $mediatype = AUDIO;
                                    $index = count($insertNewsImage);
                                    $audioFile = upload_image_s3($audioarr['file'], '');
                                    $insertNewsImage[$index] = array(
                                        'task_url' => $audioFile,
                                        'task_thumb' => '',
                                        'task_media_type' => $mediatype,
                                        'task_id' => $taskId,
                                        'created_date' => date('Y-m-d H:i:s')
                                    );

                                    $task_media_set[$index]['media_url'] = $audioFile;
                                    $task_media_set[$index]['media_type'] = $mediatype;
                                    $task_media_set[$index]['media_thumb'] = '';

                                    $insertGalleryMedia[$index] = array(
                                        'gallary_media_url' => $audioFile,
                                        'media_thumb' => '',
                                        'media_type' => $mediatype,
                                        'type' => TASK_SECTION,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                }
                            }
                        }
                    }

                    if (isset($insertGalleryMedia) && !empty($insertNewsImage)) {
                        $this->Common_model->insert_batch('ipac_task_media', array(), $insertNewsImage, '');
                    }

                    if (isset($insertGalleryMedia) && !empty($insertGalleryMedia)) {
                        $this->Common_model->insert_batch('ipac_gallary_media', array(), $insertGalleryMedia, '');
                    }

                    if (true === $this->db->trans_status()) {
                        $taskCode = $this->generateTaskCode($taskId, $insertNewsArray['task_type'], $taskAllorSu);

                        $taskArr['task_code'] = $taskCode;
                        if (isset($postdata['offlineaction']) && !empty($postdata['offlineaction']) && $postdata['offlineaction'] == 11) {
                            $taskArr['qr_code_url'] = $this->createQRCode($taskCode);
                        }
                        $whereArr['where'] = ['task_id' => $taskId];
                        $this->Common_model->update_single('ipac_task_master', $taskArr, $whereArr);

                        $taskType = array('1' => 'Facebook', '2' => 'Twitter', '3' => 'Offline', '4' => 'Whatsapp', '5' => 'Youtube', '6' => 'Online', '7' => 'Default', '8' => 'Instagram');
                        $socialTaskType = $this->input->post('taskType');
                        switch ($socialTaskType) {
                            case '1': //Facebook Message
                                $taskTitle = 'Participate in the progress of future generations! - (Facebook Task)';
                                break;
                            case '2': //Twitter Message
                                $taskTitle = 'Participate in the great political change! (Twitter Task)';
                                break;
                            case '4': // Whatsapp Message
                                $taskTitle = 'Take part in the journey towards change…! Watch Task)';
                                break;
                            case '5': // Youtube Message
                                $taskTitle = 'One click ... change ... for the first of the people! (YouTube Task)';
                                break;
                            default:
                                $taskTitle = 'New ' . $taskType[$this->input->post('taskType')] . ' task added ' . $this->input->post('taskName');
                        }

                        $taskTypeArray = [
                            '1' =>  'facebook',
                            '2' => 'twitter',
                            '3' => 'offline',
                            '4' => 'whatsapp',
                            '5' => 'youtube',
                            '6' => 'online',
                            '7' => 'default',
                            '8' => 'instagram'
                        ];

                        $actionArray = [
                            '1' => 'like',
                            '2' => 'share',
                            '3' => 'retweet',
                            '4' => 'follow',
                            '5' => 'tweet',
                            '6' => 'set dp',
                            '7' => 'event',
                            '8' => 'download media',
                            '9' => 'comment',
                            '10' => 'subscribe',
                            '11' => 'qrcode',
                            '12' => 'form',
                            '13' => 'create group',
                            '14' => 'about us',
                            '15' => 'video',
                            '16' => 'audio',
                            '17' => 'text',
                            '18' => 'gif',
                            '19' => 'link',
                            '20' => 'image',
                            '21' => 'change status',
                            '22' => 'post',
                            '23' => 'install_twitter',
                            '24' => 'install_fb',
                            '25' => 'install_whatsapp',
                            '26' => 'install_youtube',
                            '27' => 'follow_twitter',
                            '28' => 'follow_facebook',
                            '29' => 'subscribe_youtube',
                            '30' => 'upload_whatsapp'
                        ];
                        $insertNewsArray['action'] = $actionArray[$action];
                        $insertNewsArray['qr_code_url'] = isset($taskArr['qr_code_url']) ? $taskArr['qr_code_url'] : '';
                        $insertNewsArray['task_type'] = $taskTypeArray[$this->input->post('taskType')];
                        $insertNewsArray['task_media_set'] = $task_media_set;
                        $insertNewsArray['form_url'] = '';


                        //send notification
                        // if (isset($is_send_notif)) {
                        //     $this->load->library('common_function');
                        //     $taskTitle = !empty($this->input->post('notif_title')) ? $this->input->post('notif_title') : $taskTitle;

                        //     $payload = [
                        //         "title" => $taskTitle,
                        //         "message" => $this->input->post('taskName'),
                        //         "type" => 3,
                        //         "image" => '',
                        //         'task_id' => $taskId,
                        //         'news_id' => '',
                        //         'notification_id' => '',
                        //         "show_popup" => $this->input->post('show_popup'),
                        //         "task_details" => $insertNewsArray,
                        //         "body" => $this->input->post('taskName')
                        //     ];

                        //     $topic = ENV == 'production' ? '/topics/prod_task' : '/topics/stag_task';
                        //     $this->common_function->androidTopicPush($topic, $payload);

                        //     $iosTopic = ENV == 'production' ? '/topics/ios_prod_task' : '/topics/ios_stag_task';
                        //     $this->common_function->iOSTopicPush($iosTopic, $payload);
                        // }

                        #Comminting changes
                        $this->db->trans_commit();
                        $this->session->set_flashdata('message_success', $this->lang->line('task_success'));
                        redirect(base_url() . 'admin/task');
                    } else { #IF transaction failed
                        #rolling back
                        $this->db->trans_rollback();
                    }
                }
            } 
	    //IF 1 END
            $data["csrfName"] = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //District list for goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
            $data['formlist'] = get_form_list();
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("task/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function getACsByDistrictId(){
        $district_id    = $_REQUEST['district_id'];
        $ACsData        = $this->Task_model->getDistrictACs($district_id);
        
        $html           = '<option value="">Select AC</option>';
        if(!empty($ACsData)){
            foreach ($ACsData as $ACkey => $ACvalue) {
                $html   .= '<option value="'.$ACvalue->id.'" >'.$ACvalue->ac.'</option>';
            }
        }
        echo $html;exit;
    }


    /**
     *
     * @Function Downloard
     * @Description Downlord excel list of task list
     */
    public function download($taskList)
    {
        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('Task List')
            ->setSubject('Task List')
            ->setDescription('Task List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'I') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'Task')
            ->setCellValue("C1", 'Points')
            ->setCellValue("D1", 'Created On')
            ->setCellValue("E1", 'Task Live Date')
            ->setCellValue("F1", 'Deadline Date')
            ->setCellValue("G1", 'Completed By')
            ->setCellValue("H1", 'Task Type')
            ->setCellValue("I1", 'Status');

        // Add some data
        $x = 2;
        $count = 1;
        $status = array('1' => 'Active', '2' => 'Inacticve');
        foreach ($taskList as $res) {
            $date = date_create($res['created_date']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['task_title']) && ($res['task_title'] != '')) ? $res['task_title'] : '')
                ->setCellValue("C$x", (isset($res['points']) && ($res['points'] != '')) ? $res['points'] : '0')
                ->setCellValue("D$x", $Date . ' ' . $Time)
                ->setCellValue("E$x", (date_format(date_create($res['start_date']), 'd/m/Y g:i A')))
                ->setCellValue("F$x", (date_format(date_create($res['end_date']), 'd/m/Y g:i A')))
                ->setCellValue("G$x", (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : '0')
                ->setCellValue("H$x", (isset($res['task_type']) && ($res['task_type'] != '')) ? $res['task_type'] . '(' . $res['action'] . ')' : '')
                ->setCellValue("I$x", (isset($res['task_status']) && !empty($res['task_status']) ? $status[$res['task_status']] : ''));
            $x++;
            $count++;
        }



        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Task List');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="task_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }



    /**
     *  get news detail
     *
     * @Function taskList
     * @Description get news detail
     * @Return view page
     */
    public function taskDetail()
    {
        try {
            $get = $this->input->get();
            # upload csv content

            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv') && !empty($this->input->post('id')))
                && (isset($_FILES['reward_csv_file']['name']) && !empty($_FILES['reward_csv_file']['name']))
            ) {
                if ($_FILES['reward_csv_file']['type'] == 'text/csv') {
                    $msg = $this->task_reward_import($_FILES['reward_csv_file']['tmp_name'], $this->input->post('id'));
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }

                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . "admin/task/taskDetail?data=" . queryStringBuilder("id=" . encryptDecrypt($this->input->post('id'), 'decrypt')));
            }
            if (isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv')) {
                $taskId = encryptDecrypt($this->input->post('id'), 'decrypt');
            } else {
                $taskId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/TASK/");
            }
            #upload user id
            if ((isset($_POST['user-upload']) && ($_POST['user-upload'] == 'upload-user') && !empty($this->input->post('id')))) {
                if (isset($_POST['userRegistrationNumber']) && !empty(($_POST['userRegistrationNumber']))) {
                    $msg = $this->insert_user_reward($_POST);
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }

                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . "admin/task/taskDetail?data=" . queryStringBuilder("id=" . encryptDecrypt($this->input->post('id'), 'decrypt')));
            }
            if (isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv')) {
                $taskId = encryptDecrypt($this->input->post('id'), 'decrypt');
            } else {
                $taskId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/TASK/");
            }
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['task_id'] = $taskId;
            //get user profile data
            $data['task'] = $this->Task_model->taskDetail($taskId);
            $data['task_media'] = $this->Common_model->fetch_data('ipac_task_media', array(), ['where' => ['task_id' => $taskId]]);
            $data['task_status'] = array('1' => 'Active', '2' => 'Inactive');
            $data['task_gender'] = array('1' => 'Male', '2' => 'Female', 3 => 'Others');
            $data['limit'] = 10;

            if (empty($data['task'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/task/");
                return 0;
            } //IF END
            $this->load->library('common_function');
            $default = array(
                "limit" => 3,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "LivestartDate" => "",
                "LiveendDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id" => "",
                'completeStatus' => COMPLETE
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit']; 
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END


            $userList = $this->Task_model->userTaskCompletedList($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->downloadCompleteList($userList['result']);

            } //IF 3 END

            $totalrows = $userList['total'];
            $data['userlist'] = $userList['result'];

            // Manage Pagination
            $pageurl = 'admin/task/taskDetail';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['LivestartDate'] = $defaultValue['LivestartDate'];
            $data['LiveendDate'] = $defaultValue['LiveendDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/task/taskDetail?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);

            load_views("task/detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     *  import csv data into database
     *
     * @Function task_csv_import
     * @Description  import csv data into database
     * @Return void
     */
    private function task_csv_import($file_path)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->load->helper(array('s3', 'video_thumb'));

        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        $tasksArray = array();
        //creating array for task type
        foreach ($file_data as $data) {
            $data = array_values($data);

            $data = array_map('trim', $data);
            if (!array_key_exists($data[0], $tasksArray)) {
                $tasksArray[$data[0]] = $data;
            } else {
                for ($i = 11; $i < 12; $i++) {
                    if ($data[$i] != '') {
                        if (strpos($tasksArray[$data[0]][$i], $data[$i]) === false) {
                            $tasksArray[$data[0]][$i] = $tasksArray[$data[0]][$i] . "," . $data[$i];
                        }
                    }
                }
            }
        }
        //if not empty task
        if (!empty($tasksArray)) {
            foreach ($tasksArray as $row) {
                $date_time = date('Y-m-d H:i:s');

                $row = array_values($row);
                # check for required params
                $task_title = trim($row[1]);
                $task_description = trim($row[2]);
                $url = trim($row[3]);
                $task_start_date = trim($row[4]);
                $task_end_date = trim($row[5]);
                $task_points = trim($row[6]);
                $task_type = trim($row[7]);
                $task_action = trim($row[8]);
                $task_image_url = trim($row[9]);
                $task_video_url = trim($row[10]);
                $news_uid = trim($row[11]);
                $actionArray = array('like' => 1, 'share' => 2, 'retweet' => 3, 'tweet' => 5, 'set dp' => 6, 'event' => 7, 'download media' => 8, 'comment' => '9', 'subscribe' => 10, 'qrcode' => 11);
                $radius = trim($row[12]);
                $latitude = trim($row[13]);
                $longitude = trim($row[14]);
                $viseo_id = trim($row[15]);
                $channel_id = trim($row[16]);
                $twitter_id = trim($row[17]);
                $twitter_name = trim($row[18]);
                $instruction_video_url = trim($row[19]);
                $instruction_description = trim($row[20]);
                //required field
                if (!$task_title || !$task_description || !$task_start_date || !$task_end_date || !$task_points || !$task_type || !$task_action) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                } else {
                    //start date and end date validation
                    if (!(isValidDate($task_start_date, 'Y/m/d')) && !(isValidDate($task_end_date, 'Y/m/d'))) {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }
                    //end date is less than start date
                    if ($task_end_date < $task_start_date) {
                        $not_saved_record = $not_saved_record + 1;
                        continue;
                    }

                    //task action check
                    if (!empty($task_action)) {
                        if (!in_array(strtolower($task_action), $actionArray)) {
                            $taskActionType = $actionArray[$task_action];
                        }
                    }



                    # check for the user's registration id

                    if ($news_uid) {
                        $registeration_data = $this->Common_model->fetch_data('users', 'registeration_no', array('where_in' => array('registeration_no' => explode(',', $news_uid))));
                        if (!$registeration_data) {
                            $not_saved_record = $not_saved_record + 1;
                            continue;
                        }
                    }

                    # save the record to the database
                    $ins_data = [
                        'task_title' => $task_title,
                        'task_description' => $task_description,
                        'post_url' => $url,
                        'points' => $task_points,
                        'start_date' => date('Y-m-d', strtotime($task_start_date)),
                        'end_date' => date('Y-m-d', strtotime($task_end_date)),
                        'task_type' => $task_type,
                        'action' => $taskActionType,
                        'created_date' => $date_time,
                        'task_status' => ACTIVE,
                        'state' => 0,
                        'district' => 0,
                        'college' => 0,
                        'registeration_no' => !empty($news_uid) ? $news_uid : 0,
                        'gender' => 0,
                        'latitude' => !empty($latitude) ? $latitude : '0',
                        'longitude' => !empty($longitude) ? $longitude : '0',
                        'radius' => !empty($radius) ? $radius : '0',
                        'video_id' => !empty($viseo_id) ? $viseo_id : '0',
                        'channel_id' => !empty($channel_id) ? $channel_id : '0',
                        'twitter_follow_id' => !empty($twitter_id) ? $twitter_id : '0',
                        'twitter_follow_name' => !empty($twitter_name) ? $twitter_name : '0',
                        'instruction_description' => !empty($instruction_description) ? $instruction_description : '0',
                        'instruction_video_url' => !empty($instruction_video_url) ? $instruction_video_url : '0'
                    ];
                    $taskId = $this->Common_model->insert_single('ipac_task_master', $ins_data);
                    if ($taskId) {
                        $taskAllorSu = 0;
                        if (!empty($state_id) || !empty($district_code) || !empty($college_code) || !empty($news_uid) || !empty($task_gender)) {
                            $taskAllorSu = 1;
                        }
                        $taskCode = $this->generateTaskCode($taskId, $task_type, $taskAllorSu);

                        $taskArr['task_code'] = $taskCode;

                        $whereArr['where'] = ['task_id' => $taskId];
                        $codeId = $this->Common_model->update_single('ipac_task_master', $taskArr, $whereArr);
                        # save the news media files
                        $this->task_media_files($task_image_url, $task_video_url, $taskId, $date_time);
                        $saved_record = $saved_record + 1;
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                    }
                }
            } // end foreach loop here
        }



        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path

    /**
     *  import csv media data into database
     *
     * @Function news_media_files
     * @Description  import csv media data into database
     * @Return void
     */
    private function task_media_files($task_media_files, $task_video_url, $taskId, $date_time)
    {

        $images_url = explode(',', $task_media_files);
        # for images

        foreach ($images_url as $old_file_url) {
            $file_url = explode('/', $old_file_url);
            $file_name = array_pop($file_url);

            $file_extenstion = explode('.', $file_name);
            $file_extenstion = array_pop($file_extenstion);
            $file_extenstion = strtolower($file_extenstion);

            if (!in_array($file_extenstion, ['jpg', 'jpeg', 'png'])) {
                continue;
            }

            $file_type = 1;

            // Getting the base name of the file.
            $filename = basename($file_name);

            $temp_folder_path = TMEP_FOLDER_PATH;
            $file_path = $temp_folder_path . $filename;

            $write_status = file_put_contents($file_path, file_get_contents($old_file_url));

            if ($write_status) {
                $s3_main_file_url = upload_image_s3(
                    ['tmp_name' => $file_path, 'name' => $filename],
                    mime_content_type($file_path)
                );

                $news_media_data = [
                    'task_url' => $s3_main_file_url,
                    'task_thumb' => '',
                    'task_id' => $taskId,
                    'task_media_type' => 1,
                    'created_date' => $date_time,
                ];

                $this->Common_model->insert_single('ipac_task_media', $news_media_data);

                if ($file_path && file_exists($file_path)) {
                    unlink($file_path);
                }
            }
        }


        # for videos
        $video_url = explode(',', $task_video_url);

        # as only one video is allowed
        $new_video_url = $video_url[0];

        $file_extenstion = explode('.', $new_video_url);
        $file_extenstion = array_pop($file_extenstion);
        $file_extenstion = strtolower($file_extenstion);

        if (in_array($file_extenstion, ['mp4', 'mov'])) {
            $file_type = 2;

            $news_media_data = [
                'task_url' => $new_video_url,
                'task_thumb' => '',
                'task_id' => $taskId,
                'task_media_type' => 2,
                'created_date' => $date_time,
            ];

            $this->Common_model->insert_single('ipac_task_media', $news_media_data);
        }
    }

    /**
     * @function exportCompleteUserList
     * @description export complete user list
     *
     * @param type $userData
     */
    private function exportCompleteUserList($userData)
    {
        try {
            $fileName = 'completelist' . date('d-m-Y-g-i-h') . '.xlsx';
            $status = array('1' => 'Active', '2' => 'Inactive');

            // The function header by sending raw excel
            header("Content-type: application/vnd-ms-excel");

            // Defines the name of the export file
            header("Content-Disposition: attachment; filename=" . $fileName);

            $format = '<table border="1">'
                . '<tr>'
                . '<th width="25%">S.no</th>'
                . '<th>User ID</th>'
                . '<th>Name</th>'
                . '<th>Reward Points</th>'
                . '<th>State</th>'
                . '<th>City</th>'
                . '<th>College</th>'
                . '<th>Status</th>'
                . '</tr>';

            $coun = 1;
            foreach ($userData as $res) {
                $fld_1 = $coun;
                $fld_2 = (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : 'N/A';
                $fld_3 = (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : 'N/A';
                $fld_4 = 0;
                $fld_5 = (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : 'N/A';
                $fld_6 = (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : 'N/A';
                $fld_7 = (isset($res['college_name']) && ($res['college_name'] != '')) ? $res['college_name'] : 'N/A';

                // if (isset($res['state_name']) && !empty($res['state_name'])) {
                //     $fld_4 = $res['state_name'];
                // } elseif (isset($res['district_name']) && !empty($res['district_name'])) {
                //     $fld_4 = $res['district_name'];
                // } elseif (isset($res['registeration_no']) && !empty($res['registeration_no'])) {
                //     $fld_4 = $value['registeration_no'];
                // } elseif (isset($res['gender']) && !empty($res['gender'])) {
                //     $fld_4 = ($res['gender'] == MALE_GENDER ? 'Male' : 'Female');
                // } else {
                //     $fld_4 = 'All(India)';
                // }
                $fld_9 = (isset($res['is_active']) && !empty($res['is_active']) ? $status[$res['is_active']] : '');
                $format .= '<tr>
                        <td>' . $fld_1 . '</td>
                        <td>' . $fld_2 . '</td>
                        <td>' . $fld_3 . '</td>
                        <td>' . $fld_4 . '</td>
                        <td>' . $fld_5 . '</td>
                        <td>' . $fld_6 . '</td>
                        <td>' . $fld_7 . '</td>
                        <td>' . $fld_9 . '</td>

                      </tr>';
                $coun++;
            } //end foreach

            echo $format;
            die;
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    public function downloadCompleteList($userlist)
    {
        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        // auto fit column to content

        foreach (range('A', 'M') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'UID')
            ->setCellValue("C1", 'App Version')
            ->setCellValue("D1", 'Mobile Number')
            ->setCellValue("E1", 'Whatsapp Number')
            ->setCellValue("F1", 'Paytm Number')
            ->setCellValue("G1", 'State')
            ->setCellValue("H1", 'District')
            ->setCellValue("I1", 'College')
            ->setCellValue("J1", 'Gender')
            ->setCellValue("K1", 'Registration Date')
            ->setCellValue("L1", 'Task Completed')
            ->setCellValue("M1", 'Reward Points');

        // Add some data
        $x = 2;
        $status = array('1' => 'Active', '2' => 'Blocked');
        $count = 1;
        foreach ($userlist as $res) {
            $date = date_create($res['registered_on']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : 'N/A')
                ->setCellValue("C$x", 1)
                ->setCellValue("D$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : 'N/A')
                ->setCellValue("E$x", (isset($res['email_id']) && ($res['email_id'] != '')) ? $res['email_id'] : 'N/A')
                ->setCellValue("F$x", (isset($res['phone_number']) && ($res['phone_number'] != '')) ? $res['phone_number'] : 'N/A')
                ->setCellValue("G$x", (isset($res['whatsup_number']) && ($res['whatsup_number'] != '')) ? $res['whatsup_number'] : 'N/A')
                ->setCellValue("H$x", (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : 'N/A')
                ->setCellValue("I$x", (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : 'N/A')
                ->setCellValue("J$x", (isset($res['college_name']) && ($res['college_name'] != '')) ? $res['college_name'] : 'N/A')
                ->setCellValue("K$x", $Date . ' ' . $Time)
                ->setCellValue("L$x", (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : 'N/A')
                ->setCellValue("M$x", (isset($res['points_earned']) && ($res['points_earned'] != '')) ? $res['points_earned'] : 'N/A');

            $x++;
            $count++;
        }



        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="task_completed_user_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }


    /*
     * @Function generateTaskCode
     * @description to create UID for tasks
        Each task for every single day is assigned a UID based on the platform (FB, Twitter, WhatsApp, Instagram, Youtube, Offline), date, task number and target group
        Facebook – FB
        Twitter- TW
        WhatsApp – WA
        Youtube-YT
        Instagram-IG
        Offline-OL
        UID Format -Platform Type_YYYMMDD_task number_target group;

        Sample UID for task assigned to all (default) – FB_20180309_1_ALL
        Sample UID for task assigned to specific group – FB_20180309_1_SG
     * @param task id and task type
     */

    private function generateTaskCode($taskid, $tasktype, $taskAllorSu)
    {
        $date = date('Ymd');

        if ($tasktype == TWITTER_TASK || $tasktype == 'twitter') {
            $prefix = 'TW';
        } elseif ($tasktype == YOUTUBE_TASK || $tasktype == 'youtube') {
            $prefix = 'YT';
        } elseif ($tasktype == WHATSAPP || $tasktype == 'whatsapp') {
            $prefix = 'WA';
        } elseif ($tasktype == OFFLINE || $tasktype == 'offline') {
            $prefix = 'OL';
        } elseif ($tasktype == ONLINE || $tasktype == 'online') {
            $prefix = 'ON';
        } elseif ($tasktype == 7 || $tasktype == 'default') {
            $prefix = 'DF';
        } elseif ($tasktype == 8 || $tasktype == 'instagram') {
            $prefix = 'IG';
        } else {
            $prefix = 'FB';
        }

        if ($taskAllorSu == 1) {
            $suffix = 'SU';
        } else {
            $suffix = 'ALL';
        }
        $taskCode = $prefix . '_' . $date . '_' . $taskid . '_' . $suffix;
        return $taskCode;
    }


    /**
     *  import csv data into database
     *
     * @Function task_reward_import
     * @Description  import csv data into database of task reward points
     * @Return void
     */
    private function task_reward_import($file_path, $task_id)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $task_id = encryptDecrypt($task_id, 'decrypt');

        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        $task_data = $this->Common_model->fetch_data('ipac_task_master', 'task_id,task_type,points,action', array('where' => array('task_id' => $task_id)), true);

        if (!empty($task_data)) {
            foreach ($file_data as $row) {
                $date_time = date('Y-m-d H:i:s');

                $row = array_values($row);

                # check for required params
                $user_id = trim($row[0]);

                //required field
                if (!$user_id) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                } else {
                    # check for state id
                    if ($user_id) {
                        $user_data = $this->Common_model->fetch_data('users', 'user_id', array('where' => array('registeration_no' => $user_id)), true);

                        if (!$user_data) {
                            $not_saved_record = $not_saved_record + 1;
                            continue;
                        }
                    }
                    $user_task_complete = $this->Common_model->fetch_data('user_task_master', 'detail_id', array('where' => array('task_id' => $task_data['task_id'], 'user_id' => $user_data['user_id'])), true);

                    if (empty($user_task_complete)) {
                        # save the record to the database
                        $ins_data = [
                            'user_id' => $user_data['user_id'],
                            'task_id' => $task_data['task_id'],
                            'type' => $task_data['task_type'],
                            'status' => COMPLETE,
                            'complete_using_webhook' => COMPLETE,
                            'complete_using_app' => 0,
                            'added_on' => $date_time,
                        ];
                        $isSuccess = $this->Common_model->insert_single('user_task_master', $ins_data);
                        if ($isSuccess) {
                            # save the news media files
                            $saved_record = $saved_record + 1;
                        } else {
                            $not_saved_record = $not_saved_record + 1;
                        }
                        //earning inset block
                        $earning_where = array('where' => array('task_id ' => $task_data['task_id'], 'user_id' => $user_data['user_id']));
                        $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                        //earning block
                        if (empty($earningInfo)) {
                            # insert user earnings data

                            $start_date = date('Y-m-01');
                            $end_date = date('Y-m-t');
                            //earning table
                            $earningArr = array(
                                'user_id' => $user_data['user_id'],
                                'task_id' => $task_data['task_id'],
                                'type' => $task_data['task_type'],
                                'month_start_date' => $date_time,
                                'month_end_date' => $date_time,
                                'total_earned_points' => $task_data['points'],
                                'reward_payment_amt' => $task_data['points'],
                                'reward_clear_status' => 0,
                                'inserted_on' => date('Y-m-d H:i:s')
                            );
                            //earning table insert
                            $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                        }
                        //Insert in wallet log history
                        $insertWalletLog = array(
                            'user_id' => $user_data['user_id'],
                            'point' => $task_data['points'],
                            'title' => $this->lang->line('reward_earned'),
                            'description' => $task_data['action'],
                            'task_id' => $task_data['task_id'],
                            'type' => $task_data['task_type'],
                            'status' => COMPLETE,
                            'created_date' => date('Y-m-d H:i:s'),

                        );
                        //check if task history alredy exist fo
                        $where['where'] = ['user_id' => $user_data['user_id'], 'task_id' => $task_data['points'], 'type' => $task_data['task_type']];
                        $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                        if (empty($walletLogInfo)) {
                            #save users details values in DB
                            $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                            #if not success
                            if (!$isSuccess) {
                                $not_saved_record = $not_saved_record + 1;
                            }
                        }
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                    }
                }
            } // end foreach loop here
        }

        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path


    /**
     *
     * @function insert_user_reward
     * @Description insert user reward
     * in database of some  task
     */
    private function insert_user_reward($postData)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        $task_id = encryptDecrypt($postData['id'], 'decrypt');

        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        $task_data = $this->Common_model->fetch_data('ipac_task_master', 'task_id,task_type,points,action', array('where' => array('task_id' => $task_id)), true);

        if (!empty($task_data)) {
            $date_time = date('Y-m-d H:i:s');


            # check for required params
            $user_id = $postData['userRegistrationNumber'];

            //required field
            if (!$user_id) {
                $not_saved_record = $not_saved_record + 1;
            } else {
                # check for state id
                if ($user_id) {
                    $user_data = $this->Common_model->fetch_data('users', 'user_id', array('where' => array('registeration_no' => $user_id)), true);

                    if (!$user_data) {
                        $not_saved_record = $not_saved_record + 1;
                    } else {
                        $user_task_complete = $this->Common_model->fetch_data('user_task_master', 'detail_id', array('where' => array('task_id' => $task_data['task_id'], 'user_id' => $user_data['user_id'])), true);

                        if (empty($user_task_complete)) {
                            # save the record to the database
                            $ins_data = [
                                'user_id' => $user_data['user_id'],
                                'task_id' => $task_data['task_id'],
                                'type' => $task_data['task_type'],
                                'status' => COMPLETE,
                                'complete_using_webhook' => COMPLETE,
                                'complete_using_app' => 0,
                                'added_on' => $date_time,
                            ];
                            $isSuccess = $this->Common_model->insert_single('user_task_master', $ins_data);
                            if ($isSuccess) {
                                # save the news media files
                                $saved_record = $saved_record + 1;
                            } else {
                                $not_saved_record = $not_saved_record + 1;
                            }
                            //earning inset block
                            $earning_where = array('where' => array('task_id ' => $task_data['task_id'], 'user_id' => $user_data['user_id']));
                            $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                            //earning block
                            if (empty($earningInfo)) {
                                # insert user earnings data
                                $whereArray = array('where' => array('user_id' => $user_data['user_id']));
                                $user_info = $this->Common_model->fetch_data(
                                    'users',
                                    ['user_id', 'total_earning', 'points_earned', 'task_completed'],
                                    $whereArray,
                                    true
                                );
                                if (!empty($user_info)) {
                                    //update user total earning
                                    $updateUserArr = array(
                                        'total_earning' => ($user_info['total_earning'] + $task_data['points']),
                                        'points_earned' => ($user_info['total_earning'] + $task_data['points']),
                                        'updated_at' => datetime(),
                                        'last_earning_update' => datetime(),
                                        'task_completed' => ($user_info['task_completed'] + 1),
                                    );
                                    //update user table
                                    $updateId = $this->Common_model->update_single('users', $updateUserArr, ['where' => ['user_id' => $user_data['user_id']]]);
                                    /* Added By Nigam : Start */
                                    //unlock user levels
                                    $this->unlockUserlevels($user_id);
                                    /* Added By Nigam : End */
                                }
                                $start_date = date('Y-m-01');
                                $end_date = date('Y-m-t');
                                //earning table
                                $earningArr = array(
                                    'user_id' => $user_data['user_id'],
                                    'task_id' => $task_data['task_id'],
                                    'type' => $task_data['task_type'],
                                    'month_start_date' => $date_time,
                                    'month_end_date' => $date_time,
                                    'total_earned_points' => $task_data['points'],
                                    'reward_payment_amt' => $task_data['points'],
                                    'reward_clear_status' => 0,
                                    'inserted_on' => date('Y-m-d H:i:s')
                                );
                                //earning table insert
                                $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                            }
                            //Insert in wallet log history
                            $insertWalletLog = array(
                                'user_id' => $user_data['user_id'],
                                'point' => $task_data['points'],
                                'title' => $this->lang->line('reward_earned'),
                                'description' => $task_data['action'],
                                'task_id' => $task_data['task_id'],
                                'type' => $task_data['task_type'],
                                'status' => COMPLETE,
                                'created_date' => date('Y-m-d H:i:s'),

                            );
                            //check if task history alredy exist fo
                            $where['where'] = ['user_id' => $user_data['user_id'], 'task_id' => $task_data['points'], 'type' => $task_data['task_type']];
                            $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                            if (empty($walletLogInfo)) {
                                #save users details values in DB
                                $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                                #if not success
                                if (!$isSuccess) {
                                    $not_saved_record = $not_saved_record + 1;
                                }
                            }
                        } else {
                            $not_saved_record = $not_saved_record + 1;
                        }
                    }
                }
            }
        }

        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path

    public function unlockUserlevels($user_id)
    {
        $return_level = array();

        $sql = "SELECT points_earned FROM users WHERE user_id = ?";
        $query = $this->db->query($sql, array($user_id));
        $resultArray = $query->row_array();
        $current_points = $resultArray ['points_earned'];
       
        $new_levels = $this->db->query("SELECT MAX(fk_level_id) as fk_level_id from tbl_user_level where fk_user_id = $user_id");
        $new_levels = $new_levels->row_array();
    	$current_level = $new_levels['fk_level_id'] ? $new_levels['fk_level_id'] : '0';
                                
            if ($current_level != 'null'){
            $level_array = $this->nextlevel($current_points, $current_level);
       		 $difference = $level_array['max_level'] - $current_level;
            if ($difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $difference; $i++) {
                    $fk_level_id = $current_level + $i;
                    $reward_points = $this->db->query("SELECT free_reward_points from tbl_levels where eStatus='active' and pk_level_id = $fk_level_id");
                    $reward_points = $reward_points->row_array();
                    $free_rewards =  $reward_points['free_reward_points'];
                    if($free_rewards != 0){
                        $params                 = array();
                        $params['points']       = $free_rewards;
                        $params['user_id']      = $user_id;
                        $res                    = $this->User_model->updateUserEarnPoints($params);

                        $walletInsertArr[]      = array(
                            'user_id'           => $user_id,
                            'point'             => $free_rewards,
                            'title'             => 'New Level Rewards',
                            'description'       => 'Rewards earned for completion of levels',
                            'task_id'           => '0',
                            'type'              => 'bonus',
                            'status'            => '1',
                            'created_date'      => date('Y-m-d H:i:s'),
                            'updated_date'      => date('Y-m-d H:i:s')
                        );
                        
                    }
                    
                    $this->Common_model->insert_single('tbl_user_level', ['fk_level_id' => $fk_level_id, 'fk_user_id' => $user_id, 'unlock_date' => date('Y-m-d H:i:s')]);
                }
               
            }
        }
                    
            if(!empty($walletInsertArr) && count($walletInsertArr) > 0){
               	$this->Common_model->insert_multiple('ipac_user_wallet_history', $walletInsertArr);
           	}
                  
        return $return_level;
    }
    
    function nextlevel($current_points, $current_level)
    {
        $level_array = [];
        $new_levels = $this->User_model->nextlevel($current_points);
        if (!empty($new_levels)) {
            $next_levels = $new_levels['pk_level_id'];

            if ($next_levels == $current_level) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            }
            $level_difference =  $next_levels - $current_level;

            if ($level_difference < 0) {
                $level_array['current_points'] = $current_points;
                $level_array['max_level'] = $current_level;
                return $level_array;
            } else {
                for ($i = 1; $i <= $level_difference; $i++) {
                    $max_level = $current_level + $i;

                    $free_rewards = $this->db->query("SELECT free_reward_points  from tbl_levels where pk_level_id = $max_level");
                    $free_rewards = $free_rewards->row_array();
                    $current_points += $free_rewards['free_reward_points'];
                    $level_array['current_points'] = $current_points;
                    $level_array['max_level'] = $max_level;
                }
            }
            return self::nextlevel($level_array['current_points'], $level_array['max_level']);
        } else {
            $level_array['current_points'] = $current_points;
            $level_array['max_level'] = $current_level;
            return $level_array;
        }
    }

    /**
     * @Function Create QR code
     * @Description create qr code of task accoring
     * to task id and for event type
     * @param $taskid
     */
    private function createQRCode($taskId)
    {
        $qr_code_dir = HUB_QR_PATH;
        if (!file_exists($qr_code_dir)) {
            mkdir($qr_code_dir, 755, true);
        }

        $taskId = $this->sanatizeItem($taskId, 'string');

        $qr_type = 'png';

        $md5_name = md5($taskId);
        $file_name = $md5_name . '.' . $qr_type;
        $file_path = $qr_code_dir . DIRECTORY_SEPARATOR . $file_name;


        $errorCorrectionLevel = 'L';
        $matrixPointSize = 8;

        $qr_code_data = $md5_name;
        QRcode::png($qr_code_data, $file_path, $errorCorrectionLevel, $matrixPointSize, 2);
        $mime_type = mime_content_type($file_path);
        $actual_image_name = 'glubbr_hub_qr_pics/' . $file_name;
        $s3_main_file_url = upload_image_s3(
            ['tmp_name' => $file_path, 'name' => $file_name],
            mime_content_type($file_path)
        );
        return $s3_main_file_url;
    }

    /**
     * @Function @sanatizeItem
     * @Description sinitize filter input data
     */
    public function sanatizeItem($var, $type)
    {
        $flags = null;
        switch ($type) {
            case 'url':
                $filter = FILTER_SANITIZE_URL;
                break;
            case 'int':
                $filter = FILTER_SANITIZE_NUMBER_INT;
                break;
            case 'float':
                $filter = FILTER_SANITIZE_NUMBER_FLOAT;
                $flags = FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND;
                break;
            case 'email':
                $var = substr($var, 0, 254);
                $filter = FILTER_SANITIZE_EMAIL;
                break;
            case 'string':
            default:
                $filter = FILTER_SANITIZE_STRING;
                $flags = FILTER_FLAG_NO_ENCODE_QUOTES;
                break;
        }
        $output = filter_var($var, $filter, $flags);
        return ($output);
    }

    /**
     *  get news detail
     *
     * @Function taskList
     * @Description get news detail
     * @Return view page
     */
    public function taskPendingDetail()
    {
        try {
            $get = $this->input->get();
            # upload csv content

            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv') && !empty($this->input->post('id')))
                && (isset($_FILES['reward_csv_file']['name']) && !empty($_FILES['reward_csv_file']['name']))
            ) {
                if ($_FILES['reward_csv_file']['type'] == 'text/csv') {
                    $msg = $this->task_reward_import($_FILES['reward_csv_file']['tmp_name'], $this->input->post('id'));
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }

                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . "admin/task/taskDetail?data=" . queryStringBuilder("id=" . encryptDecrypt($this->input->post('id'), 'decrypt')));
            }
            if (isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv')) {
                $taskId = encryptDecrypt($this->input->post('id'), 'decrypt');
            } else {
                $taskId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/TASK/");
            }
            #upload user id
            if ((isset($_POST['user-upload']) && ($_POST['user-upload'] == 'upload-user') && !empty($this->input->post('id')))) {
                if (isset($_POST['userRegistrationNumber']) && !empty(($_POST['userRegistrationNumber']))) {
                    $msg = $this->insert_user_reward($_POST);
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }

                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url() . "admin/task/taskDetail?data=" . queryStringBuilder("id=" . encryptDecrypt($this->input->post('id'), 'decrypt')));
            }
            if (isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv')) {
                $taskId = encryptDecrypt($this->input->post('id'), 'decrypt');
            } else {
                $taskId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/TASK/");
            }
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['task_id'] = $taskId;
            //get user profile data
            $data['task'] = $this->Task_model->taskDetail($taskId);
            $data['task_media'] = $this->Common_model->fetch_data('ipac_task_media', array(), ['where' => ['task_id' => $taskId]]);
            $data['task_status'] = array('1' => 'Active', '2' => 'Inactive');
            $data['task_gender'] = array('1' => 'Male', '2' => 'Female', 3 => 'Others');
            $data['limit'] = 10;

            if (empty($data['task'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/task/");
                return 0;
            } //IF END
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id" => "",
                'completeStatus' => PENDING
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END


            $userList = $this->Task_model->userTaskCompletedList($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->downloadCompleteList($userList['result']);
            } //IF 3 END

            $totalrows = $userList['total'];
            $data['userlist'] = $userList['result'];

            // Manage Pagination
            $pageurl = 'admin/taskpendingDetail';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) { //IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { //if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } //switch end
                } //if end
            } //IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/taskpendingDetail?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);

            load_views("task/pending_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function editTask()
    {
        try {
            $post = $this->input->post();
            if (isset($post) && !empty($post)) {

                $taskId = (isset($post['token']) && !empty($post['token'])) ? encryptDecrypt($post['token'], 'decrypt') : '';
                $taskUpdateArr = [];
                // preparing array for update
                $taskUpdateArr = array(
                    'task_title' => $this->input->post('title'),
                    'task_description' => !empty($this->input->post('title_desc')) ? $this->input->post('title_desc') : '',
                    'points' => $this->input->post('points'),
                    'task_status' => $this->input->post('task_status'),
                    'end_date' => !empty($this->input->post('DeadlineDate')) ? date('Y-m-d H:i:s', strtotime($this->input->post('DeadlineDate'))) : '',
                );
                $whereArr = [];
                $whereArr['where'] = array('task_id' => $taskId);
                //update news data
                $this->Common_model->update_single('ipac_task_master', $taskUpdateArr, $whereArr);
                $this->session->set_flashdata('message_success', 'task updated successfully');
                redirect(base_url() . 'admin/task');
            }else {
                $get = $this->input->get();
                $taskId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_task'), "/admin/task/");
                $data = array();

                $data['task_id'] = $taskId;

                //get task detail data
                $data['taskDetail'] = $this->Task_model->taskDetail($taskId);
                // echo "<pre>";
                // print_r($data['taskDetail']); exit;
                if (empty($data['taskDetail'])) { //IF START
                     return 0;
                } //IF END
            }
        } catch (Exception $exception) {
        }
        load_views("task/edit", $data);
    }
}
