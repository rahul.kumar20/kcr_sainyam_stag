<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
class Poll extends MY_Controller
{
    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Poll_model');
        $this->load->model('Common_model');
        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
        $this->load->library('csvimport');
    }

    public function index()
    {
        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",                
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",                
                "distict" => "",
                "gender" => "",
                "homeContentType" => ""
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END

            $pollsList = $this->Poll_model->pollsList($defaultValue);

            $districtdata = array();
            if (!empty($defaultValue['state'])) {
                $whereArr['where'] = ['state_id' => $defaultValue['state']];
                $whereArr['order_by'] = ['district_name' => 'Asc'];
                $districtdata = $this->Common_model->fetch_data('district', 'district_code as district_id,district_name', $whereArr);
            }
          
            $totalrows = $pollsList['total'];
            $data['pollsList'] = $pollsList['result'];
            $data['districtlist'] = $districtdata;

            // Manage Pagination
            $pageurl = 'admin/poll';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_priority"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];          
            $data['endDate'] = $defaultValue['endDate'];          
            $data['limit'] = $defaultValue['limit'];         
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];          
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
           
            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            $data['gender_type'] = unserialize(GENDER_TYPE);

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$pollsList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/news?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //District list for goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("poll/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function createPoll()
    {
        $data["csrfName"] = $this->security->get_csrf_token_name();
        $data["csrfToken"] = $this->security->get_csrf_hash();
        $data['admininfo'] = $this->admininfo;
        //load state helper
        $this->load->helper('state');
        //state list
        $data['statelist'] = get_state_list();
        $data['formlist'] = get_form_list();
        //District list for goa
        $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);

        //uid list
        $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
        load_views("poll/add", $data);
    }

    public function createPollprocess()
    {
        $result = $this->Poll_model->createPollprocess();
    }
}
