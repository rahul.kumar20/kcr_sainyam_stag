<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Mks_chat extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->load->model('Mks_chat_model');
        $this->lang->load('common', "english");
    }

    /**
     * @name index
     * @description This method is used to list all the customers.
     */
    public function index()
    {
        try { //TRY START
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;

            $get = $this->input->get();

            $default      = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "export" => "",
                "order"      => "",
                "startDate" => "",
                "endDate" => "",
            );

            $defaultValue = defaultValue($get, $default);
            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) { //IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else { //ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            } //ELSE 2 END

            $data['mks_chat'] = $this->Mks_chat_model->Mkschatlist($defaultValue);
            //print_r($data['mks_chat']);
            $totalrows         = $data['mks_chat']['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$data['mks_chat']['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/mks_chat?data=" . queryStringBuilder($defaultValue));
            }

            /* Sorting Query */
            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_title"] = $data["order_by_name"]  = $data["order_by_date"]  = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "name":
                            $data["order_by_name"]  = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "title":
                            $data["order_by_title"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) { //IF 3 START
                $this->download($data['mks_chat']['result']);
            } //IF 3 END

            /* paggination */
            $pageurl            = 'admin/mks_chat';
            $links              = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);
            $data["link"]       = $links;
            $data['searchlike'] = $defaultValue['searchlike'];

            /* CSRF token */
            $data["csrfName"]   = $this->security->get_csrf_token_name();
            $data["csrfToken"]  = $this->security->get_csrf_hash();
            $data["totalrows"]  = $totalrows;
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['controller'] = $this->router->fetch_class();
            $data['method']     = $this->router->fetch_method();
            $data['module']     = $this->router->fetch_module();
            $data['startDate'] = $defaultValue['startDate'];
            $data['endDate'] = $defaultValue['endDate'];
            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            load_views("mks_chat/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }


    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function detail()
    {
        try {
            $post = $this->input->post();
            $this->form_validation->set_rules($this->config->item("reply_btn"));
            $this->load->helper('sendsms');
            if (isset($post) && !empty($post)) {
                $phone   = $_POST['phone_no'];
                $message = $_POST['reply'];
                sendMessage($phone, $message);
                $this->session->set_flashdata('message_success', "Your Reply Sent Successfully");
                redirect(base_url() . 'admin/mks_chat');
            } else {
                $get = $this->input->get();

                $Id = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/contactus/");
                $userId = (isset($get['user_id']) && !empty($get['user_id'])) ? $get['user_id'] : show404($this->lang->line('no_user'), "/admin/contactus/");
                $data = array();
                $data['admininfo'] =    $this->admininfo;
                $data['user_id'] = $userId;

                //get user profile data
                $data['contactDetail'] = $this->Mks_chat_model->mks_chatDetail($userId,$Id);
                if (empty($data['contactDetail'])) { //IF START
                    show404($this->lang->line('no_user'), "/admin/users/");
                    return 0;
                } //IF END

                load_views("mks_chat/detail", $data);
            }
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function download($subscribers)
    {
        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('MKS Chat List')
            ->setSubject('MKS Chat List')
            ->setDescription('MKS Chat List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:H1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'G') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.No')
            ->setCellValue("B1", 'User ID')
            ->setCellValue("C1", 'Phone Number')
            ->setCellValue("D1", 'User Name')
            ->setCellValue("E1", 'District')
            ->setCellValue("F1", 'AC')
            ->setCellValue("G1", 'Answer');

        // Add some data
        $x = 2;
        $y = 1;

        foreach ($subscribers as $res) {
            $user_id = !empty($res['user_id']) ? $res['user_id'] : 'Not availavle';
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $y)
                ->setCellValue("B$x", $user_id)
                ->setCellValue("C$x", $res['phone_number'])
                ->setCellValue("D$x", $res['full_name'])
                ->setCellValue("E$x", $res['district_name'])
                ->setCellValue("F$x", $res['ac'])
                ->setCellValue("G$x", $res['answer']);
            $x++;
            $y++;
        }


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('mks chat List');

        // set right to left direction
        //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="mks_chat_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
