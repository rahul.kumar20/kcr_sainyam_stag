<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Reward_earned_history extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Reward_earned_model');
        $this->load->model('User_model');

        $this->lang->load('common', 'english');
        $this->load->helper('common');
           # import csv file library
           $this->load->library('csvimport');
    }



    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try {
            $get = $this->input->get();

            # upload csv content

            if ((isset($_POST['csv-upload']) && ($_POST['csv-upload'] == 'upload-csv'))
                && (isset($_FILES['payment_csv_file']['name']) && !empty($_FILES['payment_csv_file']['name']))) {
                if ($_FILES['payment_csv_file']['type'] == 'text/csv') {
                    $msg = $this->payment_csv_import($_FILES['payment_csv_file']['tmp_name']);
                } else {
                    $msg = $this->lang->line('invalid_csv_file');
                }
                $this->session->set_flashdata('message_success', $msg);
                redirect(base_url().'admin/redards_history');
            }
            $this->load->library('common_function');
            $default = array(
                "id" =>'',
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => "",
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "paymentMethod" => "",
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END

            $userInfo = $this->Reward_earned_model->userlist($defaultValue);
            
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadUserListCsv($userInfo['result']);
            }//IF 3 END

            $totalrows = $userInfo['total'];
            $data['userlist'] = $userInfo['result'];

            // Manage Pagination
            $pageurl = 'admin/redards_history';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['id'] = $defaultValue['id'];
            $data['paymentMethod'] = $defaultValue['paymentMethod'];

            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userInfo['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = ( string )($defaultValue['page'] - 1);
                redirect(base_url() . "admin/redards_history?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //district list of goa
            $data['districtlist'] = get_district_list(DEFAULT_STATE_ID);
            load_views("users/reward_history", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



     /**
     *  import csv data into database
     *
     * @Function task_csv_import
     * @Description  import csv data into database
     * @Return void
     */
    private function payment_csv_import($file_path)
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        $this->load->helper(array('s3', 'video_thumb'));

        $file_data = $this->csvimport->get_array($file_path);
        $not_saved_record = 0;
        $saved_record = 0;
        $message = '';
        //random payment id of user set of 6 digit and time stamp
        $payment_id = randomKey(6).'_'.time();
        //if not empty task
        if (!empty($file_data)) {

            foreach ($file_data as $row) {
                $date_time = date('Y-m-d H:i:s');
    
                $row = array_values($row);
                # check for required params
                $user_uid = trim($row[0]);
                $amount_paid = trim($row[1]);
                $paid_date = trim($row[2]);
                $payment_method = trim($row[3]);
                $payment_method_detail = trim($row[4]);
                $reason = trim($row[5]);
                $trasaction_id = trim($row[6]);

                //required field
                if (!$user_uid || !$amount_paid ||  !$paid_date || !$payment_method || !$payment_method_detail) {
                    $not_saved_record = $not_saved_record + 1;
                    continue;
                } else {
                    //start date and end date validation
                    // if (!(isValidDate($paid_date, 'y/m/d'))) {
                    //     $not_saved_record = $not_saved_record + 1;
                    //     continue;
                    // }
  
                    # check for the user's registration id
                    $user_id=0;
                    if ($user_uid) {
                        $registeration_data = $this->Common_model->fetch_data('users', 'user_id,paytm_number,upi_address,account_number,default_payment_option,total_earning', array('where_in' => array('registeration_no' => explode(',', $user_uid))), true);
                     
                        if (!$registeration_data) {
                            $not_saved_record = $not_saved_record + 1;
                            continue;
                        } else {
                            $user_id = $registeration_data['user_id'];
                        }
                    }
                    # save the record to the database
                    $ins_data = [
                        'user_id' => $user_id,
                        'amount_paid' => $amount_paid,
                        'user_available_amount' => $registeration_data['total_earning'],
                        'user_left_amount' => ($registeration_data['total_earning']-$amount_paid),
                        'paid_date' => date('Y-m-d', strtotime($paid_date)),
                        'payment_option' =>$payment_method,
                        'payment_detail' => $payment_method_detail,
                        'paytm_number' =>  $registeration_data['paytm_number'],
                        'upi_address' =>  $registeration_data['upi_address'],
                        'bank_account_number' => $registeration_data['account_number'],
                        'status'    => 1,
                        'reason'    => $reason,
                        'created_date' => $date_time,
                        'transaction_id' => $trasaction_id,
                        'payment_id' =>$payment_id
                    ];
                    //save in ipac_payment_history
                    $success = $this->Common_model->insert_single('ipac_payment_history', $ins_data);
                    if ($success) {
                        $updateUserArray = array(
                            'total_earning' =>($registeration_data['total_earning']-$amount_paid),
                            'updated_at' => $date_time,
                            'last_payment_date' => $date_time,
                        );
                        $updateId = $this->Common_model->update_single(
                            'users',
                            $updateUserArray,
                            ['where' => ['user_id' => $user_id]]
                        );

                        $walletHistoryLog = array(
                           'user_id' =>$user_id,
                           'point' =>$amount_paid,
                           'title' => $this->lang->line('amount_redeem'),
                           'description' => $this->lang->line('amount_paid'),
                           'task_id'=>0,
                           'type' => 3,
                           'status' =>1,
                           'created_date'=>$date_time,
                           'updated_date' =>$date_time,
                        );
                        //save in ipac_payment_history
                        $success = $this->Common_model->insert_single('ipac_user_wallet_history', $walletHistoryLog);
                         $saved_record = $saved_record + 1;
                    } else {
                        $not_saved_record = $not_saved_record + 1;
                    }
                }
            } // end foreach loop here
        }
       


        # generate the message
        if ($saved_record >= 0) {
            $message = $saved_record . ' records saved.';
        }
        if ($not_saved_record > 0) {
            $message .= ', ' . $not_saved_record . ' records not saved';
        }

        return $message;
    } // end import function path

    /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function downloadUserListCsv($userlist)
    {

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        // $spreadsheet->getActiveSheet()->getStyle('A1:S1')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);


        // auto fit column to content
        // foreach (range('A', 'S') as $columnID) {
            // $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                // ->setAutoSize(true);
        // }
        foreach (range('A', 'J') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'UID')
            ->setCellValue("C1", 'Name')
            // ->setCellValue("D1", 'Total Earning')
            ->setCellValue("D1", 'Task Complete')
            ->setCellValue("E1", 'Reward Points')
            ->setCellValue("F1", 'Last Earning Date')
            // ->setCellValue("H1", 'State')
            ->setCellValue("G1", 'District')
            // ->setCellValue("J1", 'College')
            ->setCellValue("H1", 'Gender')
            ->setCellValue("I1", 'Registration Date')
            ->setCellValue("J1", 'Status');
            // ->setCellValue("N1", 'Payment Method Selected')
            // ->setCellValue("O1", 'Paytm Number')
            // ->setCellValue("P1", 'UPI Address')
            // ->setCellValue("Q1", 'Bank Account Number')
            // ->setCellValue("R1", 'Bank IFSC Code')
            // ->setCellValue("S1", 'Bank Account Holder Name');

        // Add some data
        $x = 2;
        $status_array = array('1' => 'Active', '2' => 'Blocked');
        $gender = array('1' => 'Male', '2' => 'Female' ,'3' =>'Others');
        $paymentOption = array('0' => 'Nothing Selected', '1' => 'Paytm' ,'2' =>'UPI Address','3' =>'Bank Account');
        $count = 1;
        foreach ($userlist as $res) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '')
                ->setCellValue("C$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '')
                // ->setCellValue("D$x", (isset($res['total_earning']) && ($res['total_earning'] != '')) ? $res['total_earning'] : '')
                ->setCellValue("D$x", (isset($res['task_completed']) && ($res['task_completed'] != '')) ? $res['task_completed'] : '')
                ->setCellValue("E$x", (isset($res['points_earned']) && ($res['points_earned'] != '')) ? $res['points_earned'] : '')
                ->setCellValue("F$x", mdate(DATE_FORMAT, strtotime($res['last_earning_update'])))
                // ->setCellValue("H$x", (isset($res['state_name']) && ($res['state_name'] != '')) ? $res['state_name'] : '')
                ->setCellValue("G$x", (isset($res['district_name']) && ($res['district_name'] != '')) ? $res['district_name'] : '')
                // ->setCellValue("J$x", (isset($res['college_name']) && ($res['college_name'] != '')) ? $res['college_name'] : '')
                ->setCellValue("H$x", (isset($res['gender']) && ($res['gender'] != '')) ? $gender[$res['gender']] : '')
                ->setCellValue("I$x", mdate(DATE_FORMAT, strtotime($res['registered_on'])))
                ->setCellValue("J$x", ( !empty($res['is_active']) ? $status_array[$res['is_active']] : 'Not available'));
                // ->setCellValue("N$x", (isset($res['default_payment_option']) && ($res['default_payment_option'] != '')) ? $paymentOption[$res['default_payment_option']] : 'No Payment Option')
                // ->setCellValue("O$x", (isset($res['paytm_number']) && ($res['paytm_number'] != '')) ? $res['paytm_number'] : '')
                // ->setCellValue("P$x", (isset($res['upi_address']) && ($res['upi_address'] != '')) ? decryptString($res['upi_address']) : '')
                // ->setCellValue("Q$x", (isset($res['account_number']) && ($res['account_number'] != '')) ? decryptString($res['account_number']) : '')
                // ->setCellValue("R$x", (isset($res['ifsc_code']) && ($res['ifsc_code'] != '')) ?decryptString($res['ifsc_code']) : '')
                // ->setCellValue("S$x", (isset($res['account_name']) && ($res['account_name'] != '')) ? decryptString($res['account_name']) : '');
            $x++;
            $count++;
        }



         // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

        // set right to left direction

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="reward_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then   the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }


    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function detail()
    {
        try {
            $get = $this->input->get();

            $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/users/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $userId;

            //get user profile data
            $data['profile'] = $this->User_model->userDetail($userId);

            $pageurl = 'users/detail';
            $this->load->library('Common_function');

            $data['searchlike'] = "";
            $params['limit'] = $data['limit'] = 10;
            $data['page'] = 1;
            $data['order'] = "";

            $params['offset'] = ($data['page'] - 1) * $data['limit'];
            $params["user_id"] = $userId;


            $data['status_array'] = [1 => 'Earned', 2 => 'Approval Pending', 3 => 'Redeem',4=>'Reject'];

            //User Subscription END

            if (empty($data['profile'])) {//IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            }//IF END
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id"=>"",
                'taskType' =>''
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END


            $walletHistoryList= $this->Reward_earned_model->getWalletHistory($defaultValue);
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadRewadCsv($walletHistoryList['result']);
            }//IF 3 END

            $totalrows = $walletHistoryList['total'];
            $data['walletHistoryList'] = $walletHistoryList['result'];

            // Manage Pagination
            $pageurl = 'admin/leaderboard';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['taskType'] = $defaultValue['taskType'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$walletHistoryList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/leaderboard?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
           //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
           //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("users/reward_history_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

/**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function paymentDetail()
    {
        try {
            $get = $this->input->get();

            $userId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/users/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $userId;

            //get user profile data
            $data['profile'] = $this->User_model->userDetail($userId);

            $pageurl = 'users/detail';
            $this->load->library('Common_function');

            $data['searchlike'] = "";
            $params['limit'] = $data['limit'] = 10;
            $data['page'] = 1;
            $data['order'] = "";

            $params['offset'] = ($data['page'] - 1) * $data['limit'];
            $params["user_id"] = $userId;


            $data['status_array'] = [1 => 'Earned', 2 => 'Approval Pending', 3 => 'Redeem',4=>'Reject'];

            //User Subscription END

            if (empty($data['profile'])) {//IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            }//IF END
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id"=>"",
                'taskType' =>''
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END


            $walletHistoryList= $this->Reward_earned_model->getPaymentHistory($defaultValue);
         
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadPaymentCsv($walletHistoryList['result']);
            }//IF 3 END

            $totalrows = $walletHistoryList['total'];
            $data['walletHistoryList'] = $walletHistoryList['result'];

            // Manage Pagination
            $pageurl = 'admin/leaderboard';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];
            $data['taskType'] = $defaultValue['taskType'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
             $statusPayment = array(
             '1' =>'Success'
             );
                         $data['statusPayment'] = $statusPayment;
            $data['paymentOption'] =array('1'=>'Paytm','2'=>'UPI','3'=>'Bank Account');
            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$walletHistoryList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin/leaderboard?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);
            load_views("users/payment_history_detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
    /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function downloadRewadCsv($userlist)
    {


        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'L') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'Title')
            ->setCellValue("C1", 'Description')
            ->setCellValue("D1", 'Earn Amount')
            ->setCellValue("E1", 'Earn Date')
            ->setCellValue("F1", 'Type')
            ->setCellValue("G1", 'Status')
            ->setCellValue("H1", 'Task Title')
            ->setCellValue("I1", 'Task Id')
            ->setCellValue("J1", 'Task Live Date')
            ->setCellValue("K1", 'Task End Date')
            ->setCellValue("L1", 'Task Type');
        // Add some data
        $x = 2;
        $status_array = array('1' => 'Active', '2' => 'Blocked');
        $count = 1;
        foreach ($userlist as $res) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['title']) && ($res['title'] != '')) ? $res['title'] : '')
                ->setCellValue("C$x", (isset($res['description']) && ($res['description'] != '')) ? $res['description'] : '')
                ->setCellValue("D$x", (isset($res['point']) && ($res['point'] != '')) ? $res['point'] : '')
                ->setCellValue("E$x", mdate(DATE_FORMAT, strtotime($res['created_date'])))
                ->setCellValue("F$x", (isset($res['type']) && ($res['type'] != '')) ? $res['type'] : '')
                ->setCellValue("G$x", ( !empty($res['status']) ? ( $res['type'] =='expense' && $res['status']==1?'Approved': $status_array[$res['status']]) : 'Not Available'))
                ->setCellValue("H$x", (isset($res['task_title']) && ($res['task_title'] != '')) ? $res['task_title'] : '')
                ->setCellValue("I$x", (isset($res['task_code']) && ($res['task_code'] != '')) ? $res['task_code'] : '')
                ->setCellValue("J$x", (isset($res['start_date']) && ($res['start_date'] != '')) ? mdate(DATE_FORMAT, strtotime($res['start_date'])): '')
                ->setCellValue("K$x", (isset($res['end_date']) && ($res['end_date'] != '')) ? mdate(DATE_FORMAT, strtotime($res['end_date'])): '')
                ->setCellValue("L$x", (isset($res['task_type']) && ($res['task_type'] != '')) ? $res['task_type'] : '');
            $x++;
            $count++;
        }



        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');



        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="wallet_history_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
    
     /**
     * @function download
     * @description To export user search list
     *
     * @param type $userData
     */
    public function downloadPaymentCsv($userlist)
    {


        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:G1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'N') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'UID')
            ->setCellValue("C1", 'Payment Amount')
            ->setCellValue("D1", 'Previous Amount')
            ->setCellValue("E1", 'Left Amount')
            ->setCellValue("E1", 'Payment Date')
            ->setCellValue("F1", 'Payment Option')
            ->setCellValue("G1", 'Payment Detail')
            ->setCellValue("H1", 'Status');
        // Add some data
        $x = 2;
        $status_array = array('1' => 'Paid');
        $payment_option_array = array('1' => 'Paytm','2'=>'UPI','3'=>'Bank Account');
        $count = 1;
        foreach ($userlist as $res) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['amount_paid']) && ($res['amount_paid'] != '')) ? $res['amount_paid'] : '')
                ->setCellValue("C$x", (isset($res['user_available_amount']) && ($res['user_available_amount'] != '')) ? $res['user_available_amount'] : '')
                ->setCellValue("D$x", (isset($res['user_left_amount']) && ($res['user_left_amount'] != '')) ? $res['user_left_amount'] : '')
                ->setCellValue("E$x", mdate(DATE_FORMAT, strtotime($res['paid_date'])))
                ->setCellValue("F$x", (isset($res['payment_option']) && ($res['payment_option'] != '')) ? $payment_option_array[$res['payment_option']] : '')
                ->setCellValue("G$x", (isset($res['payment_detail']) && ($res['payment_detail'] != '')) ? $res['payment_detail'] : '')
                ->setCellValue("H$x", ( !empty($res['status']) ? ( $status_array[$res['status']]) : 'Not Available'));

            $x++;
            $count++;
        }
        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="payment_history_list.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
