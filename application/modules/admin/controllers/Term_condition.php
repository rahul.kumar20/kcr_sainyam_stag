<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Term_condition extends MY_Controller
{

    private $admininfo = null;

    function __construct()
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->load->model('Cms_model');
        $this->lang->load('common', "english");
    }



    /**
     * @name index
     * @description This method is used to list all the CMS Content.
     */
    public function index()
    {
       
        try {//TRY START
            $this->load->library('Common_function');

            $get = $this->input->get();

            $default = array (
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "order"      => ""
            );

            $defaultValue               = defaultValue($get, $default);
            $defaultValue['searchlike'] = trim($defaultValue['searchlike']);

            $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];

            // Content List fetching
            $respData = $this->Cms_model->termConditionList($defaultValue['limit'], $offset, $defaultValue);

            //Manage pagination
            $pageurl            = 'admin/term_condition';
            $data["link"]       = $this->common_function->pagination($pageurl, $respData['total'], $defaultValue['limit']);
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['cmsData']    = $respData['result'];
            $data['totalrows']  = $respData['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (! $respData['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = ( string ) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/term_condition?data=" . queryStringBuilder($defaultValue));
            }


            $getQuery          = http_build_query(array_filter([ "limit" => $defaultValue['limit'], "page" => $defaultValue['page'] ]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_date"] = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (! empty($defaultValue['order'])) {//IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (! empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            }//IF 1 END


            /* CSRF token */
            $data["csrfName"]  = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;

            if (! $GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            $controller = $this->router->fetch_class();
            $method     = $this->router->fetch_method();
            $module     = $this->router->fetch_module();

            $data['pageUrl'] = base_url() . $module . '/' . strtolower($controller) . '/' . $method;


            load_views("term_condition/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }//CATCH END
    }



    /**
     * @name add
     * @description This method is used to add a new page to the cms.
     */
    public function add()
    {

        try {//TRY START
            $postedData        = $this->input->post();
            $data['admininfo'] = $this->admininfo;

            if (count($postedData)) {//IF 1
                $this->form_validation->set_rules('title', $this->lang->line('title'), 'required|trim');
                $this->form_validation->set_rules('page_desc', $this->lang->line('page_desc'), 'required');
                $this->form_validation->set_rules('status', $this->lang->line('status'), 'required|trim');

                if ($this->form_validation->run()) {//if 2
                    $savedata['title']         = $postedData['title'];
                    $savedata['version']         = $postedData['version'];

                    $savedata['description']      = $postedData['page_desc'];
                    $savedata['status']       = $postedData['status'];
                    $savedata['created_date'] = date('Y-m-d H:i:s');

                    // calling to insert data method.
                    $res = $this->saveCmsData($savedata);

                    $alertMsg = [];
                    if ($res) {//IF 3
                        $alertMsg['text'] = $this->lang->line('term_condition_added_success');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } //IF 3 END
                    else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }//ELSE END

                    redirect('/admin/term_condition');
                }//if END
            } //IF 1 END
            else {
                // CSRF token
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            }//ELSE END

            load_views("term_condition/add", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }



    /**
     * @name saveCmsData
     * @descrition To insert/update CMS page data description.
     *
     * @param type $this->data
     * @return boolean
     */
    private function saveCmsData($data, $updateId = false)
    {

        try {//TRY START
            $this->db->trans_start(); //transaction Start

            if ($updateId) {//IF 1
                $this->Common_model->update_single('ipac_term_condition', $data, [ 'where' => [ 'id' => $updateId ] ]);
            } //IF 1 END
            else {
                $this->Common_model->insert_single('ipac_term_condition', $data);
            }//ELSE END


            if (true === $this->db->trans_status()) { //IF 2
                //Commiting Trasaction
                $this->db->trans_commit();
                return true;
            } //IF 2 END
            else {
                //Trasaction Rollback
                $this->db->trans_rollback();
                return false;
            }//ELSE END
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }//CATCH END
    }



    /**
     * @name edit
     * @description This method is used to edit the cms page.
     *
     * @access public
     */
    public function edit()
    {

        try {//TRY START
            $get               = $this->input->get();
            $data['admininfo'] = $this->admininfo;

            $pageId          = (isset($get['id']) && ! empty($get['id'])) ? $get['id'] : show404();
            $data['page_id'] = $pageId;
            $data['pages']   = $this->Common_model->fetch_data('ipac_term_condition', '*', [ 'where' => [ 'id' => $pageId ] ], true);

            //IF no data return then Display 404 page
            if (empty($data['pages']) && array () == $data['pages']) {//IF 1
                show404();
            }//IF 1 END

            $postedData = $this->input->post();

            if (count($postedData)) {//IF 2
                $this->form_validation->set_rules('title', $this->lang->line('title'), 'required|trim');
                $this->form_validation->set_rules('page_desc', $this->lang->line('page_desc'), 'required');
                $this->form_validation->set_rules('status', $this->lang->line('status'), 'required|trim');

                if ($this->form_validation->run()) {//IF START
                    $savedata['title']         = $postedData['title'];
                    $savedata['version']         = $postedData['version'];

                    $savedata['description']      = $postedData['page_desc'];
                    $savedata['status']       = $postedData['status'];
                    $savedata['created_date'] = date('Y-m-d H:i:s');
                    // calling to update data method.
                    $res = $this->saveCmsData($savedata, $pageId);

                    if ($res) {//IF 4
                        $alertMsg['text'] = $this->lang->line('term_condition_updated_success');
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } //IF 4 END
                    else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }//ELSE END
                    redirect('/admin/term_condition');
                }//IF END
            } // IF 2 END
            else {
                // CSRF token
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            }

            load_views("term_condition/edit", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }//CATCH END
    }

     /**
     *  get news detail
     *
     * @Function newsDetail
     * @Description get news detail
     * @Return view page
     */
    public function userAcceptDetail()
    {
        try {
            $get = $this->input->get();
             $termConditionId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/term_condition");

            $data = array();
            $data['admininfo'] =   $this->admininfo;
            $data['term_id'] = $termConditionId;
            //get user profile data
            $data['termConditionDetail'] = $this->Cms_model->termConditionDetail($termConditionId);
            $data['limit'] = 10;
            if (empty($data['termConditionDetail'])) {//IF START
                show404($this->lang->line('no_user'), "/admin/term_condition/");
                return 0;
            }//IF END
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => '',
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",
                "id" => "",
                'completeStatus' => COMPLETE
            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END

            $userList = $this->Cms_model->termConditionAcceptList($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadAcceptList($userList['result']);
            }//IF 3 END

            $totalrows = $userList['total'];
            $data['userlist'] = $userList['result'];

            // Manage Pagination
            $pageurl = 'admin//term-detail';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string)($defaultValue['page'] - 1);
                redirect(base_url() . "admin//term-detail?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
    //load state helper
            $this->load->helper('state');
    //state list
            $data['statelist'] = get_state_list();
    //uid list
            $data['uidlist'] = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);

            load_views("term_condition/detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function downloadAcceptList($userlist)
    {


        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

// Set document properties
        $spreadsheet->getProperties()->setCreator('')
            ->setLastModifiedBy('')
            ->setTitle('User List')
            ->setSubject('User List')
            ->setDescription('User List');

        // add style to the header
        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'top' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'fill' => array(
                'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startcolor' => array(
                    'argb' => 'FFA0A0A0',
                ),
                'endcolor' => array(
                    'argb' => 'FFFFFFFF',
                ),
            ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        // auto fit column to content

        foreach (range('A', 'M') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A1", 'S.no')
            ->setCellValue("B1", 'UID')
            ->setCellValue("C1", 'Name')
            ->setCellValue("D1", 'Status')
            ->setCellValue("E1", 'Date');
 
        // Add some data
        $x = 2;
        $status = array('1' => 'Accepted', '2' => 'Pending');
        $count = 1;
        foreach ($userlist as $res) {
            $date = date_create($res['created_date']);
            $Date = date_format($date, 'd/m/Y');
            $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A$x", $count)
                ->setCellValue("B$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : 'N/A')
                ->setCellValue("C$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : 'N/A')
                ->setCellValue("D$x", (isset($res['status']) && ($res['status'] != '')) ? $status[$res['status']] : 'N/A')
                ->setCellValue("E$x", $Date . ' ' . $Time);

            $x++;
            $count++;
        }



// Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('User List');

// set right to left direction
//      $spreadsheet->getActiveSheet()->setRightToLeft(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="term_accept_user_list.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }
}
