<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Custom_popup extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->admininfo = $this->session->userdata('admininfo');
        $this->load->model('Custom_popup_model');
        $this->lang->load('common', "english");
    }

    /**
     * @name index
     * @description This method is used to list all the customers.
     */
    public function index()
    {
        try { //TRY START
            $this->load->library('Common_function');

            $data['admininfo'] = $this->admininfo;
            load_views("custom_popup/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
  
}
