<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
require_once APPPATH . "/libraries/phpqrcode/qrlib.php";

class User_uploads extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('s3', 'video_thumb'));

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('User_uploads_model');

        $this->lang->load('common', 'english');
        #import form validation confog file
        $this->load->config("form_validation_admin");
        # import csv file library
        $this->load->library('csvimport');
    }

    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {
        try {
            $get = $this->input->get();
            $this->load->library('common_function');
            # upload csv content

            $default            = array(
                "limit"         => 10,
                "page"          => 1,
                "startDate"     => "",
                "endDate"       => "",
                "searchlike"    => "",
                "status"        => "",
                "uid"           => "",
                "export"        => "",
                "field"         => "",
                "order"         => "",
                "distict"       => ""

            );

            $defaultValue                   = defaultValue($get, $default);
            $defaultValue["sortfield"]      = trim($defaultValue["field"]);
            $defaultValue["sortby"]         = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {
                $defaultValue['limit']      = EXPORT_LIMIT;
                $defaultValue['offset']     = 0;
            } 
            else { 
                $offset                     = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset']     = $offset;
            } 


            $userUploadsList                = $this->User_uploads_model->getUserUploads($defaultValue);


            /*
            if ($defaultValue['export']) { 
                $this->download($userUploadsList['result']);
            } */

            $totalrows                      = $userUploadsList['total'];
            $data['userUploadsList']        = $userUploadsList['result'];
            
            // Manage Pagination
            $pageurl                        = 'admin/user_uploads';
            $data["link"]                   = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"]               = "asc";
            $data["order_by_date"]          = $data["order_by_name"] = $data["order_by_enddate"] = $data["order_by_startdate"] = "sorting";

            if (!empty($defaultValue['sortby'])) { 
                $data["order_by"]           = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) { 
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"]      = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"]      = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "startdate":
                            $data["order_by_startdate"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "enddate":
                            $data["order_by_enddate"]   = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    } 
                } 
            } 

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal']          = $defaultValue; #??
            $data['get_query']          = "&" . $getQuery;
            $data['searchlike']         = $defaultValue['searchlike'];
            $data['page']               = $defaultValue['page'];
            $data['startDate']          = $defaultValue['startDate'];
            $data['endDate']            = $defaultValue['endDate'];
            $data['limit']              = $defaultValue['limit'];

            $data['totalrows']          = $totalrows;
            $data['admininfo']          = $this->admininfo;

            $data['controller']         = $this->router->fetch_class();
            $data['method']             = $this->router->fetch_method();
            $data['module']             = $this->router->fetch_module();

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userUploadsList['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page']   = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/user_uploads?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['uid']                = $defaultValue['uid'];
            $data['distict']            = $defaultValue['distict'];
            $data['permission']         = $GLOBALS['permission'];

            //load state helper
            $this->load->helper('state');
            $data['districtlist']       = get_district_list(DEFAULT_STATE_ID); //District list for goa

            //uid list
            $data['uidlist']            = $this->Common_model->fetch_data('users', array('registeration_no', 'user_id'), []);

            /*echo "<pre>";
            print_r($data);
            exit;*/
            load_views("user_uploads/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    

    /**
     *  get user uploads detail page
     *
     * @Function newsDetail
     * @Description get news detail
     * @Return view page
     */
    public function userUploadsDetail()
    {
        try {
            $get                        = $this->input->get();          
            $data                       = array();
            $data['admininfo']          = $this->data['admininfo'];
            $userUploadsId              = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/user_uploads/");
            $data['user_uploads_id']    = $userUploadsId;
            
            $data['user_uploads']       = $this->User_uploads_model->userUploadsDetailInfo($userUploadsId);
            $data['uploads_media']       = $this->Common_model->fetch_data('user_uploads_media', array('media_url','media_type','user_uploads_media_id'), ['where' => ['user_uploads_id' => $userUploadsId]]);
            /*$data['task_status']        = array('1' => 'Active', '2' => 'Inactive');
            $data['task_gender']        = array('1' => 'Male', '2' => 'Female', 3 => 'Others');*/
            //$data['limit']              = 10;

            if (empty($data['user_uploads'])) { //IF START
                show404($this->lang->line('no_user'), "/admin/user_uploads/");
                return 0;
            } //IF END

            

            /*$data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];*/
            

            load_views("user_uploads/detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

}
