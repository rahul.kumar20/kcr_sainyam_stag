<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';

class Level extends MY_Controller
{
    private $admininfo = "";
    private $data = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Level_model');
        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
    }

    public function index()
    {
        try {
            $this->load->library('Common_function');
            $get = $this->input->get();
            $default = array(
                "limit"      => 10,
                "page"       => 1,
                "searchlike" => "",
                "field"      => "",
                "order"      => ""
            );

            $defaultValue               = defaultValue($get, $default);
            $defaultValue['searchlike'] = trim($defaultValue['searchlike']);
            $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];

            // Content List fetching
            $respData = $this->Level_model->levelListing($defaultValue['limit'], $offset, $defaultValue);

            //Manage pagination
            $pageurl            = 'admin/level';
            $data["link"]       = $this->common_function->pagination($pageurl, $respData['total'], $defaultValue['limit']);
            $data['page']       = $defaultValue['page'];
            $data['limit']      = $defaultValue['limit'];
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['cmsData']    = $respData['result'];
            $data['totalrows']  = $respData['total'];

            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$respData['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = (string) ($defaultValue['page'] - 1);
                redirect(base_url() . "admin/level?data=" . queryStringBuilder($defaultValue));
            }

            $getQuery          = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']]));
            $data['get_query'] = "&" . $getQuery;

            $data["order_by_date"] = "sorting";

            //Default Order by
            $data["order_by"] = "asc";

            if (!empty($defaultValue['order'])) { //IF 1 START
                $data["order_by"] = $defaultValue["order"] == "desc" ? "asc" : "desc";
                if (!empty($defaultValue["field"])) {
                    switch (trim($defaultValue["field"])) {
                        case "added":
                            $data["order_by_date"] = $defaultValue["order"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }
                }
            } //IF 1 END


            /* CSRF token */
            $data["csrfName"]  = $this->security->get_csrf_token_name();
            $data["csrfToken"] = $this->security->get_csrf_hash();
            $data['admininfo'] = $this->admininfo;

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];

            $controller = $this->router->fetch_class();
            $method     = $this->router->fetch_method();
            $module     = $this->router->fetch_module();

            $data['pageUrl'] = base_url() . $module . '/' . strtolower($controller) . '/' . $method;


            load_views("level/index", $data);
        } //TRY END
        catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        } //CATCH END
    }

    public function add()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
            $postedData        = $this->input->post();
            $data['admininfo'] = $this->admininfo;

            if (count($postedData)) {
                $this->form_validation->set_rules('name', 'Level Name', 'required|trim');
                $this->form_validation->set_rules('name_tn', 'Level Name (Tamil)', 'required|trim');
                $this->form_validation->set_rules('free_reward_points', 'Free Reward Credit', 'required');
                $this->form_validation->set_rules('start_point', 'Start Reward Point Range', 'required|trim');
                $this->form_validation->set_rules('end_point', 'End Reward Point Range', 'required|trim');
                $this->form_validation->set_rules('incentive', 'Incentive', 'required|trim');
                $this->form_validation->set_rules('incentive_tn', 'Incentive (Tamil)', 'required|trim');
                $this->form_validation->set_rules('description', 'Description', 'required|trim');
                $this->form_validation->set_rules('description_tn', 'Description (Tamil)', 'required|trim');

                if ($this->form_validation->run()) {
                    $savedata['name']                    = $postedData['name'];
                    $savedata['name_tn']                 = $postedData['name_tn'];
                    $savedata['free_reward_points']      = $postedData['free_reward_points'];
                    $savedata['start_point']             = $postedData['start_point'];
                    $savedata['end_point']               = $postedData['end_point'];
                    $savedata['incentive'] = $postedData['incentive'];
                    $savedata['incentive_tn'] = $postedData['incentive_tn'];
                    $savedata['description'] = $postedData['description'];
                    $savedata['description_tn'] = $postedData['description_tn'];
                    $savedata['created_on']              = date('Y-m-d H:i:s');
                    $userImageUrl = '';
                    if (isset($_FILES['user_image']['name'])) {
                        $files = $_FILES;
                        $imgarr = array();
                        if (!empty($_FILES['user_image']['name'])) {
                            $imgarr['user_image']['name'] = $files['user_image']['name'];
                            $imgarr['user_image']['type'] = $files['user_image']['type'];
                            $imgarr['user_image']['tmp_name'] = $files['user_image']['tmp_name'];
                            $imgarr['user_image']['error'] = $files['user_image']['error'];
                            $imgarr['user_image']['size'] = $files['user_image']['size'];

                            $userImageUrl = upload_image_s3($imgarr['user_image'], '');
                        }
                    }
                    $savedata['image']              = $userImageUrl;
                    $res = $this->saveLevelData($savedata);

                    $alertMsg = [];
                    if ($res) {
                        $alertMsg['text'] = 'Level has been created successfully.';
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }

                    redirect('/admin/level');
                }
            } else {
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            }
            load_views("level/add", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    private function saveLevelData($data, $updateId = false)
    {
        try {
            $this->db->trans_start();
            if ($updateId) {
                $this->Common_model->update_single('tbl_levels', $data, ['where' => ['pk_level_id' => $updateId]]);
            } else {
                $this->Common_model->insert_single('tbl_levels', $data);
            }

            if (true === $this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }

    public function edit()
    {
        try {
            $this->load->helper(array('s3', 'video_thumb'));
            $get               = $this->input->get();
            $data['admininfo'] = $this->admininfo;
            $pageId          = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404();
            $data['page_id'] = $pageId;
            $data['pages']   = $this->Common_model->fetch_data('tbl_levels', '*', ['where' => ['pk_level_id' => $pageId]], true);
            if (empty($data['pages']) && array() == $data['pages']) {
                show404();
            }

            $postedData = $this->input->post();

            if (count($postedData)) {
                $this->form_validation->set_rules('name', 'Level Name', 'required|trim');
                $this->form_validation->set_rules('name_tn', 'Level Name(Tamil)', 'required|trim');
                $this->form_validation->set_rules('free_reward_points', 'Free Reward Credit', 'required');
                $this->form_validation->set_rules('start_point', 'Start Reward Point Range', 'required|trim');
                $this->form_validation->set_rules('end_point', 'End Reward Point Range', 'required|trim');
                $this->form_validation->set_rules('incentive', 'Incentive', 'required|trim');
                $this->form_validation->set_rules('incentive_tn', 'Incentive (Tamil)', 'required|trim');
                $this->form_validation->set_rules('description', 'Description', 'required|trim');
                $this->form_validation->set_rules('description_tn', 'Description (Tamil)', 'required|trim');

                if ($this->form_validation->run()) {
                    $savedata['name']                    = $postedData['name'];
                    $savedata['name_tn']                 = $postedData['name_tn'];
                    $savedata['free_reward_points']      = $postedData['free_reward_points'];
                    $savedata['start_point']             = $postedData['start_point'];
                    $savedata['end_point']               = $postedData['end_point'];
                    $savedata['incentive'] = $postedData['incentive'];
                    $savedata['incentive_tn'] = $postedData['incentive_tn'];
                    $savedata['description'] = $postedData['description'];
                    $savedata['description_tn'] = $postedData['description_tn'];


                    if (isset($_FILES['user_image']['name'])) {
                        $files = $_FILES;
                        $imgarr = array();
                        if (!empty($_FILES['user_image']['name'])) {
                            $imgarr['user_image']['name'] = $files['user_image']['name'];
                            $imgarr['user_image']['type'] = $files['user_image']['type'];
                            $imgarr['user_image']['tmp_name'] = $files['user_image']['tmp_name'];
                            $imgarr['user_image']['error'] = $files['user_image']['error'];
                            $imgarr['user_image']['size'] = $files['user_image']['size'];

                            $userImageUrl        = upload_image_s3($imgarr['user_image'], '');
                            $savedata['image']   = $userImageUrl;
                        }
                    }

                    $res = $this->saveLevelData($savedata, $pageId);
                    if ($res) {
                        $alertMsg['text'] = 'Level has been updated successfully.';
                        $alertMsg['type'] = $this->lang->line('success');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    } else {
                        $alertMsg['text'] = $this->lang->line('try_again');
                        $alertMsg['type'] = $this->lang->line('error');
                        $this->session->set_flashdata('alertMsg', $alertMsg);
                    }
                    redirect('/admin/level');
                }
            } else {
                $data["csrfName"]  = $this->security->get_csrf_token_name();
                $data["csrfToken"] = $this->security->get_csrf_hash();
            }
            load_views("level/edit", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
