<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . "/libraries/Encryption_file.php";

class Campaign_expense_request extends MY_Controller
{

    private $admininfo = "";
    private $data = array();

    public function __construct()
    {

        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('Expense_request_model');

        $this->lang->load('common', 'english');
        $this->load->config("form_validation_admin");
          # import csv file library
        $this->load->library('csvimport');
    }



    /**
     * @name index
     * @description This method is used to list all the Users.
     */
    public function index()
    {

        try {
            # upload csv content

           
            $get = $this->input->get();
            $this->load->library('common_function');
            $default = array(
                "limit" => 10,
                "page" => 1,
                "startDate" => "",
                "endDate" => "",
                "searchlike" => "",
                "status" => "",
                "uid" => "",
                "export" => "",
                "field" => "",
                "order" => "",
                'taskCompleted' => "",
                "state" => "",
                "distict" => "",
                "gender" => "",
                "college" => "",

            );

            $defaultValue = defaultValue($get, $default);
            $defaultValue["sortfield"] = trim($defaultValue["field"]);
            $defaultValue["sortby"] = trim($defaultValue["order"]);

            //If Request if Excel Export then restrict to 65000 limit
            if ($defaultValue['export']) {//IF 2 START
                $defaultValue['limit'] = EXPORT_LIMIT;
                $defaultValue['offset'] = 0;
            } //IF 2 END
            else {//ELSE 2 START
                $offset = ($defaultValue['page'] - 1) * $defaultValue['limit'];
                $defaultValue['offset'] = $offset;
            }//ELSE 2 END


            $userInfo = $this->Expense_request_model->requestedUserList($defaultValue);
            # print_r ( $this->common_function );
            /*
             * Export to Csv
             */
            if ($defaultValue['export']) {//IF 3 START
                $this->downloadRequestedList($userInfo['result']);
            }//IF 3 END

            $totalrows = $userInfo['total'];
            $data['userlist'] = $userInfo['result'];

            // Manage Pagination
            $pageurl = 'admin/campaign_expense_request';
            $data["link"] = $this->common_function->pagination($pageurl, $totalrows, $defaultValue['limit']);

            $data["order_by"] = "asc";
            $data["order_by_date"] = $data["order_by_name"] = $data["order_by_task_completed"] = $data["order_by_reward_point"] = "sorting";

            if (!empty($defaultValue['sortby'])) {//IF 4 START
                $data["order_by"] = $defaultValue["sortby"] == "desc" ? "asc" : "desc";

                if (!empty($defaultValue["sortfield"])) {//if
                    switch ($defaultValue["sortfield"]) {
                        case "name":
                            $data["order_by_name"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "registered":
                            $data["order_by_date"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "task_completed":
                            $data["order_by_task_completed"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                        case "reward_point":
                            $data["order_by_reward_point"] = $defaultValue["sortby"] == "desc" ? "sort-descending" : "sort-ascending";
                            break;
                    }//switch end
                }//if end
            }//IF 4 END

            unset($defaultValue["sortby"]); //unset sortfields

            $getQuery = http_build_query(array_filter(["limit" => $defaultValue['limit'], "page" => $defaultValue['page']])); // build query to append it to sort url

            $data['filterVal'] = $defaultValue; #??
            $data['get_query'] = "&" . $getQuery;
            $data['searchlike'] = $defaultValue['searchlike'];
            $data['page'] = $defaultValue['page'];
            $data['startDate'] = $defaultValue['startDate'];
            $data['taskCompleted'] = $defaultValue['taskCompleted'];
            $data['endDate'] = $defaultValue['endDate'];
            $data['status'] = $defaultValue['status'];
            $data['uid'] = $defaultValue['uid'];
            $data['state'] = $defaultValue['state'];
            $data['distict'] = $defaultValue['distict'];
            $data['gender'] = $defaultValue['gender'];
            $data['college'] = $defaultValue['college'];

            $data['limit'] = $defaultValue['limit'];
            $data['totalrows'] = $totalrows;
            $data['admininfo'] = $this->admininfo;

            $data['controller'] = $this->router->fetch_class();
            $data['method'] = $this->router->fetch_method();
            $data['module'] = $this->router->fetch_module();
            $data['expenseStatus'] = array(1=>'Requested',2=>'Approved',3=>'Rejected');
            #IF user is on other than First page, having only one element
            #IF last row is deleted by user
            #than page will redirected to previous page
            if (!$userInfo['result'] && $defaultValue['page'] > 1) {
                $defaultValue['page'] = ( string )($defaultValue['page'] - 1);
                redirect(base_url() . "admin/campaign_expense_request?data=" . queryStringBuilder($defaultValue));
            }

            if (!$GLOBALS['permission']) {
                setDefaultPermission();
            }

            $data['permission'] = $GLOBALS['permission'];
            //load state helper
            $this->load->helper('state');
            //state list
            $data['statelist'] = get_state_list();
            load_views("expense_request/index", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }


    public function downloadRequestedList($userlist)
    {

      //  $subscribers = $this->phpexcel_model->get_users();

        require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

        // Create new Spreadsheet object
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

  // Set document properties
        $spreadsheet->getProperties()->setCreator('Webeasystep.com ')
                ->setLastModifiedBy('')
                ->setTitle('User List')
                ->setSubject('User List')
                ->setDescription('User List');

        // add style to the header
        $styleArray = array(
                'font' => array(
                        'bold' => true,
                ),
                'alignment' => array(
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ),
                'borders' => array(
                        'top' => array(
                                'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ),
                ),
                'fill' => array(
                        'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 90,
                        'startcolor' => array(
                                'argb' => 'FFA0A0A0',
                        ),
                        'endcolor' => array(
                                'argb' => 'FFFFFFFF',
                        ),
                ),
        );
        $spreadsheet->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);


        // auto fit column to content

        foreach (range('A', 'J') as $columnID) {
            $spreadsheet->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }
        // set the names of header cells
        $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue("A1", 'S.no')
                ->setCellValue("B1", 'Campaign Title')
                ->setCellValue("C1", 'Expense Id')
                ->setCellValue("D1", 'UID')
                ->setCellValue("E1", 'Name')
                ->setCellValue("F1", 'Amount')
                ->setCellValue("G1", 'Campaign Date')
                ->setCellValue("H1", 'Expense Image')
                ->setCellValue("I1", 'Expense Particular')
                ->setCellValue("J1", 'Status');
 
        // Add some data
        $x= 2;
                    $status = array('1' => 'Requested', '2' => 'Approved' , '3'=>'Rejected');
            $count=1;
        foreach ($userlist as $res) {
            $date = date_create($res['campaign_date']);
                $Date = date_format($date, 'd/m/Y');
                $Time = date_format($date, 'g:i A');
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue("A$x", $count)
                    ->setCellValue("B$x", (isset($res['campaign_title']) && ($res['campaign_title'] != '')) ? $res['campaign_title'] : '')
                    ->setCellValue("C$x", (isset($res['expense_code']) && ($res['expense_code'] != '')) ? $res['expense_code'] : '')
                    ->setCellValue("D$x", (isset($res['registeration_no']) && ($res['registeration_no'] != '')) ? $res['registeration_no'] : '')
                    ->setCellValue("E$x", (isset($res['full_name']) && ($res['full_name'] != '')) ? $res['full_name'] : '')
                    ->setCellValue("F$x", (isset($res['total_amount']) && ($res['total_amount'] != '')) ? $res['total_amount'] : '')
                    ->setCellValue("G$x", $Date . ' ' . $Time)
                    ->setCellValue("H$x", (isset($res['expense_media_url']) && ($res['expense_media_url'] != '')) ? $res['expense_media_url'] : '')
                    ->setCellValue("I$x", (isset($res['expense_media_particular']) && ($res['expense_media_particular'] != '')) ? $res['expense_media_particular'] : '')
                    ->setCellValue("J$x", (isset($res['status']) && ($res['status'] != '')) ? $status[$res['status']] : '');

            $x++;
            $count++;
        }



  // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Expense Request User List');

  // set right to left direction
  //      $spreadsheet->getActiveSheet()->setRightToLeft(true);

  // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

  // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="expense_requested_list.xlsx"');
        header('Cache-Control: max-age=0');
  // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

  // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
        $writer->save('php://output');
        exit;

        //  create new file and remove Compatibility mode from word title
    }

    /**
     *
     * @function detail
     * @description To fetch user details and display it on web
     *
     * @return int 0
     */
    public function expenseDetail()
    {
        try {
            $get = $this->input->get();

            $expenseId = (isset($get['id']) && !empty($get['id'])) ? $get['id'] : show404($this->lang->line('no_user'), "/admin/users/");
            $data = array();
            $data['admininfo'] = $this->data['admininfo'];
            $data['user_id'] = $expenseId;

            //get user profile data
            $data['expenseDetail'] = $this->Expense_request_model->expenseDetail($expenseId);
           //User ID Proof
            $data['expenseMedia'] = $this->Common_model->fetch_data('user_campaign_expences_media', array(), ['where' => ['expence_id' => $expenseId]]);
            $pageurl = 'users/detail';
            $this->load->library('Common_function');

            $data['searchlike'] = "";
            $params['limit'] = $data['limit'] = 10;
            $data['page'] = 1;
            $data['order'] = "";

            $params['offset'] = ($data['page'] - 1) * $data['limit'];
            $params["expense_id"] = $expenseId;


            $data['status_array'] = [1 => 'Active', 2 => 'Blocked', 3 => 'Deleted'];

            //User Subscription END

            if (empty($data['expenseDetail'])) {//IF START
                show404($this->lang->line('no_user'), "/admin/users/");
                return 0;
            }//IF END

            load_views("expense_request/user-detail", $data);
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
