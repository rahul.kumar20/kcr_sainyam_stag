<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/SNSPush.php';
class SendNotification extends MY_Controller
{
    private $admininfo = "";
    private $data = array();

    public function __construct()
    {
        parent::__construct();

        $this->admininfo = $this->session->userdata('admininfo');
        $this->data['admininfo'] = $this->admininfo;
        $this->load->model('News_model');
        $this->load->model('Common_model');
    }





    /**
     * Add news in database
     *
     * @Function addNews
     * @Description add news
     * @Return view page
     */
    public function index()
    {
        try {
                    //sns object
            $sns = new Snspush();
            $usersErns = array();
                 
                          #Comminting changes
            $userErn = $this->News_model->userErns(array());
            $message = [
                "default" => 'Task',
                "APNS_SANDBOX" => json_encode(
                    [
                    "aps" => [
                        "alert" => array(
                            "title" => 'Task Added',
                            "body" => 'Task Added'
                        ),
                        "sound" => "default",
                        "mutable-content" => 1,
                        "badge" => 1,
                        "data" => array(
                            'notification_id' => '',
                            'news_id' => 1,
                            "task_id" => '',
                            "content_type" => "image",
                            "type" => 1
                        )
                    ]
                    ]
                ),
                "GCM" => json_encode([
                    "data" => [
                        "title" =>  'Task Added',
                        "message" => 'Task Added',
                        "body" =>  'Task Added',
                        "type" =>  1,
                        "image" => '',
                        'news_id' => 0,
                        'task_id' => '',
                        'notification_id' => '',

                    ]
                ])
            ];
                         //send notification
            if (!empty($userErn)) {
               
                  //send message to filtered users or selected users
                $result = $sns->asyncPublish3($userErn, $message);
                pr($result);
            }
        } catch (Exception $exception) {
            showException($exception->getMessage());
            exit;
        }
    }
}
