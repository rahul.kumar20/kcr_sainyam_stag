<?php

class Common_model extends CI_Model
{

    public $finalrole = array();

    public function __construct()
    {
        $this->load->database();
        $this->load->library('session');
    }

    /**
     * Fetch data from any table based on different conditions
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	array
     * @return	bool
     */
    public function fetch_data($table, $fields = '*', $conditions = array(), $returnRow = false)
    {

        //Preparing query
        $this->db->select($fields);
        $this->db->from($table);

        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query());exit;

        //Return
        if (FALSE != $query && $query->num_rows() > 0) {
            // $this->totalrows = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $returnRow ? $query->row_array() : $query->result_array();
        } else {
            return NULL;
        }
    }

    /**
     * Insert data in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function insert_single($table, $data = array())
    {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }

        $this->db->insert($table, $data);
        //echo $this->db->last_query();

        return $this->db->insert_id();
    }



    public function insert_multiple($table, $data = array())
    {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }
        $this->db->insert_batch($table, $data);
        return true;
    }

    // public function update_multiple($table, $data = array(), $key)
    // {
    //     //Check if any data to insert
    //     if (count($data) < 1) {
    //         return false;
    //     }
    //     return $this->db->update_batch($table, $data, $key);
    // }

    /**
     * Insert batch data
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @param	bool
     * @return	bool
     */
    public function insert_batch($table, $defaultArray, $dynamicArray = array(), $updatedTime = false)
    {
        //Check if default array has values
        if (count($dynamicArray) < 1) {
            return false;
        }

        //If updatedTime is true
        if ($updatedTime) {
            $defaultArray['UpdatedTime'] = time();
        }

        //Iterate it
        foreach ($dynamicArray as $val) {
            $updates[] = array_merge($defaultArray, $val);
        }
        return $this->db->insert_batch($table, $updates);
    }



    /**
     * Delete data from DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function delete_data($table, $conditions = array())
    {
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        #for deleting friend request
        if (isset($conditions["customWhere"])) {
            $this->db->where($conditions["customWhere"]);
        }
        return $this->db->delete($table);
    }



    /**
     * Handle different conditions of query
     *
     * @access	public
     * @param	array
     * @return	bool
     */
    private function condition_handler($conditions)
    {
        //Inner Join
        if (array_key_exists('inner_join', $conditions)) {

            //Iterate all where's
            foreach ($conditions['inner_join'] as $key => $val) {
                $this->db->join($key, $val, 'inner');
            }
        }

        //Left Join
        if (array_key_exists('left_join', $conditions)) {

            //Iterate all where's
            foreach ($conditions['left_join'] as $key => $val) {
                $this->db->join($key, $val, 'left');
            }
        }

        //Where
        if (array_key_exists('where', $conditions)) {

            //Iterate all where's
            foreach ($conditions['where'] as $key => $val) {
                $this->db->where($key, $val);
            }
        }

        //Where OR
        if (array_key_exists('or_where', $conditions)) {

            //Iterate all where or's
            foreach ($conditions['or_where'] as $key => $val) {
                $this->db->or_where($key, $val);
            }
        }

        //Where In
        if (array_key_exists('where_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_in'] as $key => $val) {
                $this->db->where_in($key, $val);
            }
        }

        //Where Not In
        if (array_key_exists('where_not_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_not_in'] as $key => $val) {
                $this->db->where_not_in($key, $val);
            }
        }

        //Having
        if (array_key_exists('having', $conditions)) {
            $this->db->having($conditions['having']);
        }

        //Group By
        if (array_key_exists('group_by', $conditions)) {
            $this->db->group_by($conditions['group_by']);
        }

        //Order By
        if (array_key_exists('order_by', $conditions)) {

            //Iterate all order by's
            foreach ($conditions['order_by'] as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        //Order By
        if (array_key_exists('like', $conditions)) {

            //Iterate all likes
            foreach ($conditions['like'] as $key => $val) {
                $this->db->like($key, $val);
            }
        }

        //Limit
        if (array_key_exists('limit', $conditions)) {

            //If offset is there too?
            if (count($conditions['limit']) == 1) {
                $this->db->limit($conditions['limit'][0]);
            } else {
                $this->db->limit($conditions['limit'][0], $conditions['limit'][1]);
            }
        }
    }



    /**
     * Update Batch
     *
     * @access	public
     * @param	string
     * @param	array
     * @return	boolean
     */
    // public function update_batch_data($table, $defaultArray, $dynamicArray = array(), $key)
    // {
    //     //Check if any data
    //     if (count($dynamicArray) < 1) {
    //         return false;
    //     }

    //     //Prepare data for insertion
    //     foreach ($dynamicArray as $val) {
    //         $data[] = array_merge($defaultArray, $val);
    //     }
    //     return $this->db->update_batch($table, $data, $key);
    // }



    /**
     * Update details in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @return	string
     */
    public function update_single($table, $updates, $conditions = array())
    {
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->update($table, $updates);
    }



    public function updateTableData($data, $tableName, $where)
    {
        $this->db->set($data);
        foreach ($where as $key => $value) {
            $this->db->where($key, $value);
        }
        if (!$this->db->update($tableName)) {
            throw new Exception("Update error");
        } else {
            return true;
        }
    }



    /**
     * Count all records
     *
     * @access	public
     * @param	string
     * @return	array
     */
    public function fetch_count($table, $conditions = array())
    {
        $this->db->from($table);
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->count_all_results();
    }



    function sendIphonePushMessage($deviceToken, $payload)
    {

        $date        = @strtotime(date('Y-m-d'));
        $data['aps'] = $payload;
        $apnsHost    = 'gateway.sandbox.push.apple.com';
        //$apnsHost = 'gateway.push.apple.com';
        $apnsPort    = '2195';

        //$apnsCert = getcwd().'/public/ckpem/ROVO_dev.pem'; // this is for development mode (development mode)
        $apnsCert = getcwd() . '/public/ckpem/pushcertdevelopment.pem'; // this is for production mode (distribution mode)
        //$passphrase = '1234';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $apnsCert);
        //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        //$fp = stream_socket_client( $apnsHost, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
        $fp  = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        //var_dump($fp); die;
        if (!$fp) return false;


        $sec_payload = json_encode($data);
        $msg         = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($sec_payload)) . $sec_payload;
        // Send it to the server
        $result      = @fwrite($fp, $msg, strlen($msg));
        if ($result) {
            //echo "true";
            return true;
        } else {
            //print $deviceToken.'=========';
            //echo "false";
            return false;
        }
        fclose($fp);
    }



    /**
     * @name uploadfile
     * @param type $filename
     * @param type $filearr
     * @param type $restype
     * @param type $foldername
     * @return boolean
     */
    // public function uploadfile($filename = '', $filearr, $restype = 'name', $foldername = '', $allowedType = NULL)
    // {

    //     if (!is_dir(COMMON_UPLOAD_PATH . '/' . $foldername)) {
    //         mkdir(COMMON_UPLOAD_PATH . '/' . $foldername);
    //         chmod(COMMON_UPLOAD_PATH . '/' . $foldername, 0755);
    //     }

    //     if ($filearr[$filename]['name'] != '') {
    //         $config['upload_path'] = COMMON_UPLOAD_PATH . $foldername;
    //         if (!empty($allowedType)) {
    //             $config['allowed_types'] = $allowedType;
    //         } else {
    //             $config['allowed_types'] = '*';
    //         }
    //         $new_name            = date('Y/m/d') . '_' . time() . '_' . $filearr[$filename]['name'];
    //         $config['file_name'] = $new_name;
    //         $this->load->library('upload', $config);
    //         if ($this->upload->do_upload($filename)) {
    //             $res = $this->upload->data();
    //             if ($restype == 'name') {
    //                 unset($foldername);
    //                 return $res['file_name'];
    //             } elseif ($restype == 'url') {
    //                 return COMMON_FILE_URL . $foldername . '/' . $res['file_name'];
    //             }
    //         } else {
    //             return false;
    //         }
    //     }
    // }



    /**
     * @name createvideothumb
     * @param type $vidurl
     * @param type $restype
     * @param type $foldername
     * @return string
     */
    // public function createvideothumb($vidurl, $restype = 'name', $foldername)
    // {

    //     $newthumbnail = time() . '_video_thumbnail.jpg';
    //     $thumbnail    = COMMON_UPLOAD_PATH . $foldername . '/' . $newthumbnail;

    //     // shell command [highly simplified, please don't run it plain on your script!]
    //     shell_exec("ffmpeg -i $vidurl -deinterlace -an -ss 11 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $thumbnail 2>&1");

    //     if ($restype == 'name') {
    //         return $newthumbnail;
    //     } else if ($restype == 'url') {
    //         return COMMON_FILE_URL . $foldername . '/' . $newthumbnail;
    //     }
    // }



    public function sendFCMNotification($devices, $message)
    {
        $url     = 'https://fcm.googleapis.com/fcm/send';
        $fields  = array(
            'registration_ids' => $devices,
            'data'             => $message,
        );
        $data    = json_encode($fields);
        $headers = array(
            'Authorization: key=' . "AAAAF7Ip-2I:APA91bEWcPV7JebecqFGRvUVitLJbIgc96qVkjoregT45P116DvYuLi0Q6ELiekaP9trQHe5wLmmB7rTnl_bRS9VnmAEreDkOARFG2-cNHvxMmLzlTfseN6g1InO0ck_SDYu_PRAu5oY",
            'Content-Type: application/json'
        );

        $ch     = curl_init();
        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);
        //adding headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //adding the fields in json format
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //finally executing the curl request
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        //Now close the connection
        curl_close($ch);
        //and return the result


        return $result;
    }



    /*
     * Method to upload images
     *
     * @param array $files $_FILES array
     * @param string $fieldName Field Name i.e <input type="file" name="fieldName">
     * @param string $uploadPath Upload Path
     * @param array $validImage Array of valid image mime types eg. image/jpg, image/png
     * @param bool $rename Renames files to a sha256 value with extension
     *
     * @return array ["error"=> true|false, "message"=> error_message|"data" => image_name]
     */

    public function uploadImage($files, $fieldName, $uploadPath, $fileSize = 2097152, $validImage = ["image/jpg", "image/jpeg", "image/png"], $rename = true)
    {

        $image = getimagesize($files[$fieldName]["tmp_name"]);

        if (!$image) {
            return [
                "error"   => true,
                "message" => "not an image."
            ];
        }

        if (!in_array($image["mime"], $validImage)) {
            return [
                "error"   => true,
                "message" => "image type not supported."
            ];
        }

        if ($files[$fieldName]["size"] > $fileSize) {
            return [
                "error"   => true,
                "message" => "file size not supported."
            ];
        }

        preg_match("/^image\/(.*)$/", $image["mime"], $extension);
        $name = "";
        if ($rename) {
            $name = hash("sha256", uniqid("", true)) . "." . $extension[1];
        } else {
            $name = $files[$fieldName]["name"];
        }


        if (move_uploaded_file($files[$fieldName]["tmp_name"], $uploadPath . $name)) {
            return [
                "error" => false,
                "data"  => $name
            ];
        }
    }



    public function removeSpace($str)
    {

        return str_replace(' ', '', $str);
    }



    public function s3_uplode($filename, $temp_name)
    {
        $name = explode('.', $filename);
        $ext  = array_pop($name);
        $name = 'RCC-' . hash('sha1', shell_exec("date +%s%N")) . '.' . $ext;

        $imgdata = $temp_name;
        $s3      = new S3(AWS_ACCESSKEY, AWS_SECRET_KEY);
        $uri     = AWS_URI . $name;
        $bucket  = AMAZONS3_BUCKET;
        $result  = $s3->putObjectFile($imgdata, $bucket, $uri, S3::ACL_PUBLIC_READ);
        $url     = 'https://s3.amazonaws.com/' . AMAZONS3_BUCKET . '/' . $name;
        return $url;
    }


    /**
     * @name  fetch_using_join
     * @description fetch data from join
     * @param string $select
     * @param string $from
     * @param string $joinCondition
     * @param string $joinType
     * @param string $where
     * @return arrray
     */
    public function fetch_using_join($select, $from, $join, $where, $asArray = NULL, $offset = NULL, $orderBy = NULL, $limit = NULL)
    {

        $this->db->select($select, FALSE);
        $this->db->from($from);
        for ($i = 0; $i < count($join); $i++) {
            $this->db->join($join[$i]["table"], $join[$i]["condition"], $join[$i]["type"]);
        }
        $this->db->where($where);
        if (isset($orderBy['order']) && $orderBy !== NULL) {
            $this->db->order_by($orderBy["order"], $orderBy["sort"]);
        }

        if ($limit !== NULL) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return ($asArray !== NULL) ? $query->row() : $query->result_array();
    }
}
