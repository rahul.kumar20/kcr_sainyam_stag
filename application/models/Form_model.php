<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Form_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function getPcList()
    {
           
        $this->db->select('*');
        $this->db->from('tbl_pc');
        
        
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                echo '<select name="pc_list" data-live-search="true" class="selectpicker">';
                echo '<option value=" ">Select PC</option>';
                foreach ($data->result_array() as $key => $values) {
                    ?>

                    <option  value="<?php echo $values['id']; ?>"><?php echo ucfirst($values['ac']); ?></option>


                    <?php
                }
                echo '</select>';
            } else {
                ?>
                <select required class="selectpicker">
                    <option>No Records</option>
                </select>
                <?php
            }
        }
    }

    public function getAcList($disId, $type,$districtId=false)
    {

        $dist="";
        $this->db->select('id,state,LOWER(ac) as ac,district');
        $this->db->from('tbl_ac');
        
        $this->db->order_by("ac","ASC");
        
        if (isset($disId) && !empty($disId) && empty($type)) {
            $dist = "vote_ac";
            $qus  =2;
            $this->db->where("district", $disId);
        } elseif (isset($disId) && !empty($disId) && !empty($type)) {
            $dist = "home_ac";
            $qus=1;
            $this->db->where("state", $disId);
        }
        
        if($districtId){
            
            $this->db->where("district",$districtId);
        }
        
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                $response =  '<select name="'.$dist.'" data-live-search="true" onchange="displayNextQues(this.value,this.name)" class="form-control select-form">';
                $response.='<option value="">Select AC</option>';
                foreach ($data->result_array() as $key => $values) {
                    $response.=  "<option  value=".$values['id'].">".ucfirst($values['ac'])."</option>";
                }
                $response.='</select>';
            } else {
                $response='<select class="form-control select-form">
                    <option >No Records</option>
                </select>';
            }
            
            return $response;
        }
    }
     
    public function getAcList2($disId)
    {

           
        $this->db->select('id,state,LOWER(ac) as ac,district');
        $this->db->from('tbl_ac');
        $this->db->order_by("ac","ASC");
        if (isset($disId) && !empty($disId)) {
            $this->db->where("district", $disId);
        }
        
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                $response =  '<select name="cur_ac" data-live-search="true" class="form-control select-form">';
                $response.='<option value="">Select AC</option>';
                foreach ($data->result_array() as $key => $values) {
                    $response.=  "<option  value=".$values['id'].">".ucfirst($values['ac'])."</option>";
                }
                $response.='</select>';
            } else {
                $response='<select  class="form-control select-form">
                    <option >No Records</option>
                </select>';
            }
            
            return $response;
        }
    }

    public function getStateList()
    {
           
        $this->db->select('*');
        $this->db->from('state_list');

        $this->db->order_by("state_name","ASC");
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                echo '<select name="state_list" required data-live-search="true" class="selectpicker">';
                echo '<option value="">Select State</option>';
                foreach ($data->result_array() as $key => $values) {
                    ?>
                  <option  value="<?php echo $values['state_id']; ?>"><?php echo ucfirst($values['state_name']); ?></option>
                    <?php
                }
                echo '</select>';
            } else {
                ?>
                <select required class="selectpicker">
                    <option value="">No Records</option>
                </select>
                <?php
            }
        }
    }


    public function getDistrictList($stateId)
    {
           
        $this->db->select('district_id,state_id,district_code,LOWER(district_name) as district_name');
        $this->db->from('district');
        
        if (isset($stateId) && !empty($stateId)) {
            $this->db->where("state_id", $stateId);
        }
        $this->db->order_by("district_name","ASC");
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                $response = '<select name="vote_dis" required data-live-search="true" class="form-control select-form" onchange=getAcList(this.value,"ac-list","")>';
                $response.='<option value="">Select District</option>';
                foreach ($data->result_array() as $key => $values) {
                    $response.="<option  value=".$values['district_id'].">".ucfirst($values['district_name'])."</option>";
                }
                $response.= "</select>";
            } else {
                $response="
                <select required class='form-control'>
                    <option>No Records</option>
                </select>";
            }
        
            
            return $response;
        }
    }
     
    public function getDistrictList2($stateId)
    {
           
        $this->db->select('district_id,state_id,district_code,LOWER(district_name) as district_name');
        $this->db->from('district');
        $this->db->order_by("district_name","ASC");
        if (isset($stateId) && !empty($stateId)) {
            $this->db->where("state_id", $stateId);
        }
        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                $response = '<select name="cur_dis" required data-live-search="true" class="form-control select-form" onchange=getAcList2(this.value,"ac-list3")>';
                $response.='<option value="">Select District</option>';
                foreach ($data->result_array() as $key => $values) {
                    $response.="<option  value=".$values['district_id'].">".ucfirst($values['district_name'])."</option>";
                }
                $response.= "</select>";
            } else {
                $response="
                <select required class='form-control'>
                    <option>No Records</option>
                </select>";
            }
        
            
            return $response;
        }
    }

    public function getCollegeList()
    {
           
        $this->db->select('*');
        $this->db->from('college_list');
        $this->db->limit('1000');

        $data = $this->db->get();
        $error = $this->db->error();
        if (isset($error) && !empty($error['code'])) {
            throw new Exception($error['code']);
        } else {
            if ($data->num_rows() > 0) {
                $response =  '<select name="col_list" required data-live-search="true" class="form-control">';
                $response.= '<option value=" ">Select College</option>';
                foreach ($data->result_array() as $key => $values) {
                    $response.="<option  value=".$values['college_id'].">".$values['college_name']."</option>";
                }
                $response."</select>";
            } else {
                $response="<select required class='form-control'>
                    <option>No Records</option>
                </select>";
            }
            
            return $response;
        }
    }

    public function fetchPc()
    {
        $this->db->select('*');
        $this->db->from('tbl_pc');
        $query = $this->db->get();
        $returnArr=array();
        if (false != $query && $query->num_rows() > 0) {
            $returnArr=$query->result_array();
            return $returnArr;
        } else {
            return $returnArr;
        }
    }
}
