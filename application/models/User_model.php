<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model
{

    public $finalrole = array();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }



    /**
     * @name userlist
     * @description Used to filter the users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function userlist($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "u.user_id",
            "task_completed" => "u.task_completed",
            "reward_point" => "u.points_earned",
            "total_earning" => "u.total_earning"

        ];

        $this->db->select("SQL_CALC_FOUND_ROWS u.*, d.district_name, totalRefrralUsed(u.user_id) as total_referral", false);
        $this->db->from('users as u');
        // $this->db->join('state_list as sl', 'u.state=sl.state_id', 'inner');
        $this->db->join('district as d', 'u.district=d.district_code', 'inner');
        // $this->db->join('college_list as cl', 'u.college=cl.college_id', 'inner');

        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->or_like('phone_number', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) &&
            (isset($params["sortby"]) && !empty($params["sortby"]))
        ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("u.registered_on", "DESC");
        }
        //status filter
        if (!empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
        //state filter
        if (!empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
        //district filter
        if (!empty($params['distict'])) {
            $this->db->where('district', $params['distict']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
        //college filter
        // if (!empty($params['college'])) {
        //     $this->db->where('college', $params['college']);
        // }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(registered_on) >= '" . $startDate . "' AND DATE(registered_on) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }
        $this->db->limit($params['limit'], $params['offset']);

        $query = $this->db->get();
        
        if ($query !== false && $query->num_rows() > 0) {
            $res['result'] = $query->result_array();
            $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;
        } else {
            $res['result'] = array();
            $res['total'] = 0;
        }


        return $res;
    }



    /**
     * @function sendNotification
     * @dscription function to get user device token from database| According to device type
     *
     * @param array $params
     * @return array
     */
    public function sendNotification($params)
    {
        $this->db->select('first_name,u.user_id,s.device_token,s.platform');
        $this->db->from('users as u');
        $this->db->join('session as s', 'u.user_id=s.user_id', 'left');
        if (!empty($params['platform']) && $params['platform'] != 1) {
            $platform = $params['platform'] - 1;
            $this->db->where('s.platform', $platform);
        }
        if (!empty($params['gender']) && "3" != $params['gender']) {
            $this->db->where('u.gender', $params['gender']);
        }

        $this->db->where('s.login_status = 1');
        $this->db->group_by('s.device_id');
        $query = $this->db->get();

        return $query->result_array();
    }



    /**
     * @function getUserList
     * @description function to get user list from API call to get user list according to type
     *
     * @param type $params
     * @return type
     */
    public function getUserList($params)
    {
        $userlist_type = isset($params['userlist_type']) ? $params['userlist_type'] : 1;
        if ($userlist_type == 1) {
            $sql = 'SQL_CALC_FOUND_ROWS CONCAT(first_name," ",last_name) as name,u.user_id,IF(f.status IS NULL,"",f.status) as status';
        } elseif ($userlist_type == 2) {
            $sql = 'SQL_CALC_FOUND_ROWS CONCAT(first_name," ",last_name) as name,u.user_id,IF(f.id IS NULL,"",1) as status';
        } elseif ($userlist_type == 3) {
            $sql = 'SQL_CALC_FOUND_ROWS CONCAT(first_name," ",last_name) as name,u.user_id,IF(f.id IS NULL,"",1) as status';
        }
        $this->db->select($sql, false);
        $this->db->from('users as u');

        if ($userlist_type == 1) {
            $this->db->join(
                'friend_request as f',
                '((f.sender_id=u.user_id AND f.receiver_id=' . $params['user_id'] . ') OR (f.sender_id=' . $params['user_id'] . ' AND f.receiver_id=u.user_id))',
                'left',
                false
            );
        } elseif ($userlist_type == 2) {
            $this->db->join('follows as f', '(sender_id=' . $params['user_id'] . ' AND receiver_id = u.user_id)', 'left', false);
        } elseif ($userlist_type == 3) {
            $this->db->join('favorite as f', '(f.user_id=' . $params['user_id'] . ' AND favorited_userid = u.user_id)', 'left', false);
        }

        if (!empty($params['searchlike'])) {
            $this->db->like('u.first_name', $params['searchlike']);
        }
        $this->db->limit($params['limit'], $params['offset']);
        $query = $this->db->get();

        $respArr           = [];
        $respArr['result'] = $query->result_array();
        $respArr['count']  = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        return $respArr;
    }



    /**
     *
     * @param type $limit
     * @param type $offset
     * @param type $params
     * @return Array Getting user details
     */
    public function get_user_data($limit, $offset, $params)
    {
        $this->db->select('SQL_CALC_FOUND_ROWS user_id, middle_name,first_name,last_name, image', false)
            ->from('users as u');
        if (!empty($params['searchlike'])) {
            $this->db->like('u.middle_name', $params['searchlike']);
        }
        $this->db->limit($limit, $offset);
        $query          = $this->db->get();
        $err            = $this->db->error();
        $this->totalmsg = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        if (isset($err['code']) && $err['code'] != 0) {
            throw new Exception($this->lang->line('somthing_went_wrong'));
        } else {
            return $query->result_array();
        }
    }



    /**
     * @function get_users
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function get_users($user_ids)
    {

        $this->db->select("CONCAT(first_name,' ',last_name) as username , user_id , image_thumb as pic", false);
        $this->db->from('users');
        $this->db->where('status', ACTIVE);
        $this->db->where_in('user_id', $user_ids);
        $this->db->group_by('user_id');
        $result = $this->db->get();
        $status = $result->result_array();
        return $status;
    }
    /**
     * @function userDetail
     * @description get all users list for API call
     *
     * @param type $user_ids
     * @return type
     */
    public function userDetail($userId)
    {
        //if user id is set
        if (isset($userId) && !empty($userId)) {
            $this->db->select("u.user_id,u.registeration_no,u.full_name,u.email_id,u.facebook_id,u.twitter_id,"
                . "u.user_image,u.language,u.whatsup_number,u.paytm_number,u.phone_number,about,gender,"
                . "address,dob,state,district,college,is_user_college_student,points_earned,task_completed,fb_username,twitter_username,"
                . "ranking,campaign_participated,registered_on,is_active,is_deleted,sl.state_name,d.district_name,cl.college_name,is_email_approved,referal_code,is_profile_edited,
                    requested_for_edit_profile,task_completed,total_earning,
                    last_earning_update,upi_address,account_name,
                    account_number,ifsc_code,is_bonus_received,
                    getuserTotalTaskComplted($userId,'') as totalTaskComplete,get_campaign_form_status($userId) as campaignStatus", false);
            $this->db->from('users as u');
            $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
            $this->db->join('district as d', 'u.district=d.district_code', 'left');
            $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

            $this->db->where_in('user_id', $userId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }
    /**
     * @name referralUserList
     * @description Used to filter the referral user users
     * @used_at ADMIN
     *
     * @param int $offset To set offset in MySql Query. E.g : select * from xxxx limit offset, limit
     * @param int $limit To set number of Rows at a time
     * @param array $params An array of parameters to filter out CMS content list
     * @return array $res An array of fetched result
     */
    public function referralUserList($params)
    {
        $sortMap = [
            "name"       => "full_name",
            "registered" => "u.user_id",
            "task_completed" => "u.task_completed",
            "reward_point" => "u.points_earned"
        ];

        $this->db->select("SQL_CALC_FOUND_ROWS u.*,sl.state_name,d.district_name,cl.college_name", false);
        $this->db->from('users as u');
        $this->db->join('ipac_referal_user as ru', 'u.user_id = ru.user_id', 'inner');
        $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
        $this->db->join('district as d', 'u.district=d.district_code', 'left');
        $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');

        //search block
        if (!empty($params['searchlike'])) {
            $this->db->group_start();
            $this->db->like('registeration_no', $params['searchlike']);
            $this->db->group_end();
        }
        //sort by block
        if ((isset($params["sortfield"]) && !empty($params["sortfield"]) && in_array($params["sortfield"], array_keys($sortMap))) &&
            (isset($params["sortby"]) && !empty($params["sortby"]))
        ) {
            if ($params["sortfield"] == "name") {
                $this->db->order_by("u.full_name", $params["sortby"]);
            } else {
                $this->db->order_by($sortMap[$params["sortfield"]], $params["sortby"]);
            }
        } else {
            $this->db->order_by("u.registered_on", "DESC");
        }
        //status filter
        if (!empty($params['status'])) {
            $this->db->where('is_active', $params['status']);
        } else {
            $this->db->where('is_active != 3');
        }
        //UID filter
        if (!empty($params['uid'])) {
            $this->db->where('registeration_no', $params['uid']);
        }
        //state filter
        if (!empty($params['state'])) {
            $this->db->where('state', $params['state']);
        }
        //district filter
        if (!empty($params['district'])) {
            $this->db->where('district', $params['distict']);
        }
        //Gender filter
        if (!empty($params['gender'])) {
            $this->db->where('gender', $params['gender']);
        }
        //college filter
        if (!empty($params['college'])) {
            $this->db->where('college', $params['college']);
        }
        //date filter
        if (!empty($params['startDate']) && !empty($params['endDate'])) {
            $startDate = date('Y-m-d', strtotime($params['startDate']));
            $endDate   = date('Y-m-d', strtotime($params['endDate']));
            $this->db->where("DATE(registered_on) >= '" . $startDate . "' AND DATE(registered_on) <= '" . $endDate . "' ");
        }
        //task completed filter
        if (!empty($params['taskCompleted'])) {
            $taskCompleted = explode('-', $params['taskCompleted']);
            $this->db->where("u.task_completed>= '" . $taskCompleted['0'] . "' AND u.task_completed
            <= '" . $taskCompleted['1'] . "' ");
        }

        $this->db->where('ru.referal_user_id', $params['id']);

        $this->db->limit($params['limit'], $params['offset']);

        $query         = $this->db->get();
        $res['result'] = $query->result_array();
        $res['total']  = $this->db->query('SELECT FOUND_ROWS() count')->row()->count;

        return $res;
    }

    public function userProfileDetail($userId)
    {
        //if user id is set
        if (isset($userId) && !empty($userId)) {
            $this->db->select("
            u.user_id,
            u.registeration_no,
            u.full_name,
            u.email_id,
            u.facebook_id,
            u.twitter_id,
            u.user_image,
            u.language,
            u.whatsup_number,
            u.paytm_number,
            u.phone_number,
            about,
            gender,
            address,
            dob,
            u.state,
            u.district,
            u.ac,
            u.block,
            u.ward_panchayat,
            u.ward_panchayat_other,
            u.block_other,      
            inclined_party,
            party_name,
            college,
            is_user_college_student,
            points_earned,
            task_completed,
            fb_username,
            u.twitter_username,
            ranking,
            campaign_participated,
            registered_on,
            is_active,
            is_deleted,
            sl.state_name,
            d.district_name,
            tbl_ac.ac as ac_name,
            tbl_block.block as block_name,
            tbl_ward_panchayat.ward_panchayat as ward_panchayat_name,
            cl.college_name,
            is_email_approved,
            referal_code,
            is_profile_edited,
            requested_for_edit_profile,
            total_earning,
            last_earning_update,
            task_completed,
            u.upi_address,
            u.account_name,
            u.ifsc_code,
            u.account_number,
            is_bonus_received,
            dmk_member,
            dmk_id,
            u.is_notification_received,
            u.is_profile_verified,
            ", false);
            $this->db->from('users as u');
            $this->db->join('user_profile_change_request as pc', 'u.user_id=pc.user_id', 'left');
            $this->db->join('state_list as sl', 'u.state=sl.state_id', 'left');
            $this->db->join('district as d', 'u.district=d.district_code', 'left');
            $this->db->join('tbl_ac', 'u.ac=tbl_ac.id', 'left');
            $this->db->join('tbl_block', 'u.block=tbl_block.id', 'left');
            $this->db->join('tbl_ward_panchayat', 'u.ward_panchayat=tbl_ward_panchayat.id', 'left');
            $this->db->join('college_list as cl', 'u.college=cl.college_id', 'left');
            $this->db->where_in('u.user_id', $userId);
            $result = $this->db->get();
            $resultArr = array();
            //if num or rows greater than 0
            if ($result->num_rows() > 0) {
                $resultArr = $result->row_array();
            } else {
                $resultArr = array();
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    public function updateUserEarnPoints($params)
    {
        $sql        = "UPDATE users SET points_earned = points_earned + ".$params['points'].",total_earning= total_earning + ".$params['points']." ,last_earning_update = NOW()  WHERE user_id = '".$params['user_id']."'";
        $response   = $this->db->query($sql);
        //$response   = $query->result_array();
        return $response;
    }

    public function getUserLevels($userId)
    {
        $this->db->select_max('fk_level_id');
        $this->db->from('tbl_user_level');
        $this->db->where('fk_user_id', $userId);
        $query = $this->db->get();
        $resultArr = $query->row_array();
        //print_r($resultArr); exit;
        return $resultArr;
    
    }
    public function nextlevel($current_points)
    {
        $resultSet =  $this->db->query("SELECT pk_level_id from tbl_levels where eStatus='active' and $current_points >= start_point AND $current_points <= end_point");
        $res = $resultSet->row_array();
        return $res;
    }

    public function getSystemLevels($userPoints)
    {
        $this->db->select('pk_level_id,free_reward_points');
        $this->db->from('tbl_levels');
        $this->db->where('eStatus', 'active');
        $this->db->where("(start_point <= '".$userPoints."' AND end_point >= '".$userPoints."')");
        $query = $this->db->get();
        $resultArr = $query->row_array();
        //print_r($resultArr);
        return $resultArr;
    }
}
