<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Survey_model extends CI_Model
{

    public $finalrole = array ();
    public $totalmsg;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    //get survey question
    public function getSurveyQuestion($formId = false, $step = 0)
    {
        $newStep   = $step+1;
        //$this->db->select();
        $this->db->from("questions a");
        
        if (isset($formId) && !empty($formId)) {
            $this->db->where("fid", $formId);
        }
         $this->db->where("depth= '" . $step . "'", null, false);
        $this->db->limit(1);
        $this->db->order_by('depth', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    //get survey question
    public function getRelatedQuestion($formId, $qusId = false, $step = 0)
    {
       
        //$this->db->select();
        $this->db->from("questions a");
        
        if (isset($formId) && !empty($formId)) {
            $this->db->where("fid", $formId);
        }
        if (isset($qusId) && !empty($qusId)) {
            $this->db->where("parent_qid", $qusId);
        }
        $this->db->limit(1, $step);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    
    public function getWhatsappTaskCompletedList()
    {
        $this->db->select('utm.*,
		itm.points,
		itm.start_date,
		itm.end_date,
		itm.task_type,
		itm.action,
		itm.task_id,
		GROUP_CONCAT(itt.media_url SEPARATOR ",") as whatsapp_images,
        itm.whatsapp_image_url1,
        itm.whatsapp_image_url2,		
		');
        $this->db->from('user_task_master as utm');
        $this->db->join('ipac_task_master as itm', 'utm.task_id=itm.task_id', 'inner');
        $this->db->join('offline_task_tracking itt', 'itm.task_id=itt.task_id AND utm.user_id=itt.user_id', 'inner');
        $this->db->where('itm.task_type', 'whatsapp');
        $this->db->where('itm.action', 'set dp');
        $this->db->where('utm.status', PENDING);
        $this->db->group_by('utm.user_id,utm.task_id');
         $query = $this->db->get();
        return $query->result_array();
    }
}
