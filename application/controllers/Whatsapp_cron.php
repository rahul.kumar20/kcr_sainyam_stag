<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Whatsapp_cron extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->library('session');
        $this->load->model('Survey_model');
    }

    public function index()
    {
    
        //listing of user list who completed whatsapp set dp task
        $userList = $this->Survey_model->getWhatsappTaskCompletedList();
         //if userlist is not blank
        if (!empty($userList)) {
            $i=1;
            //prepare array for check images match
            foreach ($userList as $list) {
                //if admin add whatsapp image
                if (!empty($list['whatsapp_image_url1']) && !empty($list['whatsapp_image_url2'])) {
                    $image = explode(',', $list['whatsapp_images']);
                    //images array
                    $imagesArray = array(
                     'user_img_url1' =>trim($image[0]),
                     'user_img_url2'=>trim($image[1]),
                     'admin_img_url1'=>trim($list['whatsapp_image_url1']),
                     'admin_img_url2'=>trim($list['whatsapp_image_url2'])
                    );
                    //curl function call
                    $result = $this->curlImagesCheck($imagesArray);
                    //curl response decode
                    $response =json_decode($result);
                    //if images matches success fully
                    if ($response->code == 200 && $response->result == 'True') {
                        //update task complete status equal to complete
                              $updateId = $this->Common_model->update_single(
                                  'user_task_master',
                                  ['status'=>COMPLETE],
                                  ['where'=>['detail_id'=>$list['detail_id']]]
                              );
                                                          
                               //earning inset block
                                        $earning_where = array('where' => array('task_id ' => $list['task_id'], 'user_id' => $list['user_id']));
                                        $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                                       //earning block
                        if (empty($earningInfo)) {
                            # insert user earnings data

                            $start_date = date('Y-m-01');
                            $end_date = date('Y-m-t');
                            //earning table
                            $earningArr = array(
                                'user_id' =>  $list['user_id'],
                                'task_id' => $list['task_id'],
                                'type' => $list['task_type'],
                                'month_start_date' => $start_date,
                                'month_end_date' => $end_date,
                                'total_earned_points' => $list['points'],
                                'reward_payment_amt' => $list['points'],
                                'reward_clear_status' => 0,
                                'inserted_on' => date('Y-m-d H:i:s')
                            );
                            //earning table insert
                            $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                            if ($earningSuccess) {
                                  $whereArr['where'] = ['user_id' => $list['user_id']];

                                    #fetching user details from db
                                    $userInfo = $this->Common_model->fetch_data(
                                        'users',
                                        [
                                            'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                                            'points_earned', 'total_earning', 'task_completed','is_bonus_received'
                                        ],
                                        $whereArr,
                                        true
                                    );
                                //updating user value
                                $updateUserArray = array(
                                    'task_completed' => ($userInfo['task_completed'] + 1),
                                    'total_earning' => ($userInfo['total_earning'] + $list['points']),
                                    'points_earned' => ($userInfo['points_earned'] + $list['points']),
                                    'last_earning_update' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s')
                                );
                                $whereUserArr['where'] = ['user_id' => $list['user_id']];
                                $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
                            }
                        }
                                          //Insert in wallet log history
                                        $insertWalletLog = array(
                                            'user_id' => $list['user_id'],
                                            'point' => $list['points'],
                                            'title' => $this->lang->line('reward_earned'),
                                            'description' => $list['action'],
                                            'task_id' => $list['task_id'],
                                            'type' => $list['task_type'],
                                            'status' => COMPLETE,
                                            'created_date' => date('Y-m-d H:i:s'),

                                        );
                                         //check if task history alredy exist fo
                                        $where['where'] = ['user_id' => $list['user_id'], 'task_id' => $list['task_id'], 'type' => $list['task_type']];
                                        $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                        if (empty($walletLogInfo)) {
                            #save users details values in DB
                            $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                         #if not success
                            if (!$isSuccess) {
                                throw new Exception($this->lang->line('try_again'));
                            }
                        }
                    }
                    $i++;
                }
            }
        }
    }
    
    /*
     *@function curlImagesCheck
     *@Description check images matching
     *@param array $imagesArray array of images
     *@return response array
    */
    private function curlImagesCheck($imagesArray)
    {
        $ch = curl_init(); //http post to another server

        curl_setopt($ch, CURLOPT_URL, "http://appinventive.com:8193/detectimage");

        curl_setopt($ch, CURLOPT_POST, 1);

          $username = 'user1';
          $password = 'strongpassword';


        $params = http_build_query($imagesArray);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        // receive server response

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);
        return $server_output;
    }
}
