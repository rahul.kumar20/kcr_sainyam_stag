<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Campaign extends MY_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('session');
        $this->load->library("Csvimport");
        $this->load->model ('Common_model');
        $this->load->helper("common_helper");
    }

    public function index() {
       
       
       $get = $this->input->get(); 
        
        
        
       $data['user_id'] = isset($get['user_id'])?($get['user_id']):"";
           
           
        $data['detail'] =$stateDetail =  $this->getUserStateList($data['user_id']);
        
        $data['states'] = $this->Common_model->fetch_data("state_list", "state_id,LOWER(state_name) as state_name",['order_by'=>['state_name'=>'ASC']]);
        $data['acs'] = $this->Common_model->fetch_data("tbl_ac", "id,LOWER(ac) as ac",['where'=>['district'=>$stateDetail['district_id']],'order_by'=>['ac'=>'ASC']]);

        
       if($this->input->post()){
           
           $this->form_validation->set_rules('answer3', "Answer 3", 'required|trim');
           
           
           
           if ($this->form_validation->run() == FALSE) {
                $this->load->view("campaign/index",$data);
           }else{
               
               $postedData = $this->input->post();
               
               $insert['user_id']  = isset($postedData['user_id'])?$postedData['user_id']:"";
               $insert['home_ac']  = isset($postedData['home_ac'])?$postedData['home_ac']:1;
               $insert['answer_3']  = isset($postedData['answer3'])?$postedData['answer3']:"";
               $insert['campaign_state']  = isset($postedData['current-state'])?$postedData['current-state']:0;
               $insert['campaign_district']  = isset($postedData['cur_dis'])?$postedData['cur_dis']:0;
               $insert['campaign_ac']  = isset($postedData['cur_ac'])?$postedData['cur_ac']:1;
               
               $insert['language1']  = isset($postedData['language1'])?$postedData['language1']:"";
               $insert['language2']  = isset($postedData['language2'])?$postedData['language2']:"";
               $insert['address']  = isset($postedData['address'])?$postedData['address']:"";
               $insert['pincode']  = isset($postedData['pincode'])?$postedData['pincode']:"";
               $insert['created_date']  = date("Y-m-d H:i:s");
               
               $insertId = $this->Common_model->insert_single("user_campaign",$insert);
               //echo $this->db->last_query(); die;
               if($insertId){
                $this->session->set_flashdata('message', "Form submitted successfully.");
                redirect(base_url() . "campaign-thankyou");
               }else{
                   $this->session->set_flashdata('message', "Error in submitting form.");
                   $this->load->view("campaign/index",$data);
                   
               }
               
           }
           
           
       }    
       else{
           
           $this->load->view("campaign/index",$data);
       }
       
    }
    
    // gettng user states.
    public function getUserStateList($userId){
        
        
        $join =[
                ['table'=>'state_list b','condition'=>'a.state = b.state_id','type'=>'inner'],
                ['table'=>'district c','condition'=>'a.district = c.district_id','type'=>'inner']
              ];
        
        return (array)$this->Common_model->fetch_using_join("LOWER(b.state_name) as state_name,b.state_id,LOWER(c.district_name) as district_name,c.district_id","users a",$join,['user_id'=>$userId],true);
        
    }
    
    
    public function thankyou(){
        
        $this->load->view("campaign/thankyou");
        
    }

}
