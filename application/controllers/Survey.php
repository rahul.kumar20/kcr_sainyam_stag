<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Survey extends MY_Controller
{

    public function __construct()
    {

        parent::__construct();

        //$this->admininfo = $this->session->userdata('admininfo');
        //$this->data['admininfo'] = $this->admininfo;
        $this->load->library("Csvimport");
        $this->load->model('Common_model');
    }

    public function index()
    {


        $get = $this->input->get();
        $data['formid'] = isset($get['formid']) ? $get['formid'] : "";
        $data['type'] = isset($get['type']) ? $get['type'] : "";
        if (isset($get['user_id']) && !empty($get['user_id'])) {
            $user_id = $get['user_id'];
        }
        if (isset($get['uid']) && !empty($get['uid'])) {
            $user_id = $get['uid'];
        }

        $data['user_id'] = (isset($user_id) ? $user_id : show404());

        $task_id = isset($get['task_id']) ? $get['task_id'] : 0;
        $news_id = isset($get['news_id']) ? $get['news_id'] : 0;
        // if ((empty($task_id) || empty($news_id))) {
        //     show404();
        // }
        $data['task_id'] = $task_id;
        $data['news_id'] = $news_id;

        //if get request and mobile number verification
        if (isset($get) && !empty($get)) {
            $formDetail = $this->Common_model->fetch_data(
                'forms',
                'mobile_verification',
                ['where' => ['fid' => $get['formid']]],
                true
            );

            if (!empty($formDetail)) {
                //if mobile verification is off for the form
                if ($formDetail['mobile_verification'] == 2) {
                    //insert array
                    $insert['otp'] = 0;
                    $insert['code'] = mt_rand(100000, 999999) . time();
                    $insert['mobile'] = '';
                    $insert['form_id'] = $get['formid'];
                    $insert['type'] = $get['type'];
                    $insert['user_id'] = $get['user_id'];
                    $insert['steps'] = 0;
                    $insert['status'] = 1;
                    $insert['created_at'] = date("Y-m-d H:i:s");
                    $insert['task_id'] = $task_id;
                    $insert['news_id'] = $news_id;

                    //insert in user forms table
                    $userFormId = $this->Common_model->insert_single("user-forms", $insert);

                    redirect(base_url() . "start-survey?code=" . $insert['code'] . "&step=0");
                }
            }
        }
        //if post request is there
        if ($this->input->post()) {
            $postedData = $this->input->post();
            //$this->form_validation->set_rules('mobile', "Mobile", 'trim|required');
            $insert['otp'] = generate_otp();
            $insert['code'] = mt_rand(100000, 999999) . time();
            $insert['mobile'] = $postedData['mobile'];
            $insert['form_id'] = $postedData['formid'];
            $insert['type'] = $postedData['type'];
            $insert['user_id'] = $postedData['user_id'];
            $insert['steps'] = 0;
            $insert['status'] = 1;
            $insert['created_at'] = date("Y-m-d H:i:s");
            $insert['task_id'] = $postedData['task_id'];
            $insert['news_id'] = $postedData['news_id'];

            //send OTP
            $this->load->helper("sendsms_helper");
            $msg = "Welcome to Goa Deserves Better Survey. Please enter the below OTP (" . $insert['otp'] . ") to submit your survey.";
            sendMessage($postedData['mobile'], $msg);

            $userFormId = $this->Common_model->insert_single("user-forms", $insert);

            redirect(base_url() . "start-survey?code=" . $insert['code'] . "&step=0");
        } else {
            $this->load->view("survey/mobile-otp", $data);
        }
    }

    /**
     * Start Survey
     */
    public function startSurvey()
    {
        $data = [];
        $get = $this->input->get();
        $code = isset($get['code']) ? $get['code'] : show404();
        $step = isset($get['step']) ? $get['step'] : show404();
        $data['code'] = $code;
        $data['step'] = $step;
        $formDetail = $this->Common_model->fetch_data('user-forms', '*', ['where' => ['code' => $code]], true);
        if (empty($formDetail)) {
            show404();
        }

        $question = $this->getFormDetail($formDetail, $step, 0);

        if ($this->input->post()) {
            $postedData = $this->input->post();
            $i = 0;
            foreach ($postedData['qid'] as $questionvalue) {
                $insert[$i]['qid'] = $questionvalue;
                $insert[$i]['fid'] = $formDetail['form_id'];
                if (!is_numeric($postedData['answer'][$questionvalue][0])) {
                    $insert[$i]['answer'] = $postedData['answer'][$questionvalue][0];
                    $insert[$i]['optionId'] = $postedData['answer'][$questionvalue][0];
                } else {
                    $insert[$i]['optionId'] = $postedData['answer'][$questionvalue][0];
                    $insert[$i]['answer'] = $postedData['answer'][$questionvalue][0];
                }
                $insert[$i]['user_answer_id'] = $formDetail['id'];

                $i++;
            }
            $is_success = $this->db->insert_batch("answers", $insert);


            $formMobileCheck = $this->Common_model->fetch_data(
                'forms',
                'mobile_verification',
                ['where' => ['fid' => $formDetail['form_id']]],
                true
            );
            if (!empty($formMobileCheck)) {
                if ($formMobileCheck['mobile_verification'] == 1) {
                    redirect(base_url() . "verify-survey?code=" . $code);
                } else {
                    $this->notVerifySurvey($code);
                }
            } else {
                redirect(base_url() . "verify-survey?code=" . $code);
            }
        } else {
            $data['questions'] = $question;
            $data['count'] = count($question);
            $data['options'] = $this->Common_model->fetch_data("options", "*", ['where' => ['qid' => $question[0]['qid']]]);
            $this->load->view("survey/index", $data);
        }
        
    
        //getting form question
    }

    public function getNextQuestion()
    {
        $post = $this->input->post();
        $post = array_map("trim", $post);
        $removeNext = 1;
        if (isset($post) && !empty($post)) {
            if (isset($post['code']) && !empty($post['code'])) {
                $formDetail = $this->Common_model->fetch_data('user-forms', '*', ['where' => ['code' => $post['code']]], true);
                if (!empty($formDetail)) {
                    $step = $post['depth'] + 1;
                    if(isset($post['optionCode']) && !empty($post['optionCode'])) {
                        $nextQuestion = $this->Common_model->fetch_data('options', 'next_question_status', ['where' => ['option_code' => $post['optionCode']]], true);  
                        if($nextQuestion['next_question_status'] == 2){
                            $step =  $step+1;
                            $removeNext = 2;
                        }
                    }
                    $optionCode = (isset($post['optionCode']) && !empty($post['optionCode']) ? $post['optionCode'] : 0);
                    $question = $this->getFormDetail($formDetail, $step, $optionCode);
                    $respArr = array("code" => 200, 'question' => $question, 'formCode' => $post['code'], "removeNext"=> $removeNext);
                    echo json_encode($respArr);
                    die;
                }
            }
        }
    }

    /**
     *@Function getFormDetail
     *@Description get form detail
     *@param array $formDetail array of form detail
     *@param int $step form steps
     *@return array survey questions
     */
    public function getFormDetail($formDetail, $step, $optioncode = 0)
    {

        try {
            $this->load->model("Survey_model");
            $result = $this->Survey_model->getSurveyQuestion($formDetail['form_id'], $step);
            if (!empty($result)) {
                $result = array_map(function ($element) use ($optioncode) {
                    $element["options"] = $this->Common_model->fetch_data("options", "*", ['where' => ['qid' => $element['qid'], 'parent_code' => $optioncode]]);
                     if(empty( $element["options"])) {
                        $element["options"] = $this->Common_model->fetch_data("options", "*", ['where' => ['qid' => $element['qid'], 'parent_code' => 0]]);
   
                     }
                    return $element;
                }, $result);

            }
            return $result;
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
        }
    }
    
 
    
    //verify the survey
    public function verifySurvey()
    {


        $get = $this->input->get();
        $data['code'] = isset($get['code']) ? $get['code'] : show404();



        if ($this->input->post()) {
            $postedData = $this->input->post();

            $formDetail = $this->Common_model->fetch_data("user-forms", "*", ['where' => ['code' => $data['code'], 'otp' => $postedData['otp']]], true);
            //update is verify
            if (empty($formDetail)) {
                $this->session->set_flashdata('message', "Invlid OTP.");
                redirect(base_url() . "verify-survey?code=" . $data['code']);
            }
            $updateId = $this->Common_model->update_single('user-forms', ['is_verified' => 1, 'status' => 2], ['where' => ['form_id' => $formDetail['form_id'], 'user_id' => $formDetail['user_id'], 'code' => $data['code']]]);

            if (!empty($formDetail['task_id'])) {
               //total form step count
                $totalFormCount = $this->Common_model->fetch_data("forms", "steps", ['where' => ['fid' => $formDetail['form_id']]], true);


                $this->db->where(['form_id' => $formDetail['form_id'], 'user_id' => $formDetail['user_id'], 'code' => $data['code']]);
                $this->db->set('steps', 'steps+ 1', false);
                $this->db->update('user-forms');

                $totalCount = $formDetail['steps'] + 1;

                $totalFormCompleted = $this->Common_model->fetch_count("user-forms", ['where' => ['form_id' => $formDetail['form_id'], 'user_id' => $formDetail['user_id'], 'status' => 2, 'task_id' => $formDetail['task_id']]]);
                if ($updateId) {
                    //check if form completed
                    $taskInfo = $this->Common_model->fetch_data(
                        'ipac_task_master',
                        ['task_id', 'task_status', 'start_date', 'end_date', 'points', 'task_type', 'action', 'form_steps'],
                        ['where' => ['task_id' => $formDetail['task_id']]],
                        true
                    );
                    $status = '';
                    if ($taskInfo['form_steps'] > $totalFormCompleted) {
                        $status = 'pending';
                    } else {
                        $status = 'complete';
                    }
 
                    //if te=ask info is  not blank
                    if (!empty($taskInfo)) {
                          //task complete info
                        $taskCompleteInfo = $this->Common_model->fetch_data(
                            'user_task_master',
                            ['detail_id', 'status'],
                            ['where' => ['task_id' => $formDetail['task_id'], 'user_id' => $formDetail['user_id'], ]],
                            true
                        );
                          //if not data exist in user task complete table
                        if (empty($taskCompleteInfo)) {
                            $taskCompleteStatus = PENDING;
                            if ($taskInfo['form_steps'] == $totalFormCompleted) {
                                $taskCompleteStatus = COMPLETE;
                            }
                            //insert in user task complete table
                            $taskCompleteInsert = array(
                                'user_id' => $formDetail['user_id'],
                                'task_id' => $formDetail['task_id'],
                                'type' => $taskInfo['task_type'],
                                'status' => $taskCompleteStatus,
                                'complete_using_app' => 1,
                                'complete_using_webhook' => 0,
                                'added_on' => date('Y-m-d H:i:s')
                            );
                                  //insert in task complete
                            $isSuccess = $this->Common_model->insert_single('user_task_master', $taskCompleteInsert); #save users details values in DB
                                       //if task complete status is pending and form step complete
                            if ($isSuccess && $taskInfo['form_steps'] == $totalFormCompleted) {
                             //update task complete status equal to complete
                                $updateId = $this->Common_model->update_single(
                                    'user_task_master',
                                    ['status' => COMPLETE],
                                    ['where' => ['detail_id' => $taskCompleteInfo['detail_id']]]
                                );
                                              
                                 //earning inset block
                                $earning_where = array('where' => array('task_id ' => $formDetail['task_id'], 'user_id' => $formDetail['user_id']));
                                $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                                         //earning block
                                if (empty($earningInfo)) {
                                    # insert user earnings data

                                    $start_date = date('Y-m-01');
                                    $end_date = date('Y-m-t');
                                    //earning table
                                    $earningArr = array(
                                        'user_id' => $formDetail['user_id'],
                                        'task_id' => $formDetail['task_id'],
                                        'type' => $taskInfo['task_type'],
                                        'month_start_date' => $start_date,
                                        'month_end_date' => $end_date,
                                        'total_earned_points' => $taskInfo['points'],
                                        'reward_payment_amt' => $taskInfo['points'],
                                        'reward_clear_status' => 0,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                    //earning table insert
                                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                                    if ($earningSuccess) {
                                        $whereArr['where'] = ['user_id' => $formDetail['user_id']];

                                            #fetching user details from db
                                        $userInfo = $this->Common_model->fetch_data(
                                            'users',
                                            [
                                                'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                                                'points_earned', 'total_earning', 'task_completed', 'is_bonus_received'
                                            ],
                                            $whereArr,
                                            true
                                        );
                                        //updating user value
                                        $updateUserArray = array(
                                            'task_completed' => ($userInfo['task_completed'] + 1),
                                            'total_earning' => ($userInfo['total_earning'] + $taskInfo['points']),
                                            'points_earned' => ($userInfo['points_earned'] + $taskInfo['points']),
                                            'last_earning_update' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $whereUserArr['where'] = ['user_id' => $formDetail['user_id']];
                                        $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
                                    }
                                }
                                        //Insert in wallet log history
                                $insertWalletLog = array(
                                    'user_id' => $formDetail['user_id'],
                                    'point' => $taskInfo['points'],
                                    'title' => $this->lang->line('reward_earned'),
                                    'description' => $taskInfo['action'],
                                    'task_id' => $taskInfo['task_id'],
                                    'type' => $taskInfo['task_type'],
                                    'status' => COMPLETE,
                                    'created_date' => date('Y-m-d H:i:s'),

                                );
                                           //check if task history alredy exist fo
                                $where['where'] = ['user_id' => $formDetail['user_id'], 'task_id' => $taskInfo['task_id'], 'type' => $taskInfo['task_type']];
                                $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                                if (empty($walletLogInfo)) {
                                    #save users details values in DB
                                    $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                               #if not success
                                    if (!$isSuccess) {
                                        throw new Exception($this->lang->line('try_again'));
                                    }
                                }
                            }
                        }
                           //if user completed all step and task complete is in pending status
                        if (!empty($taskCompleteInfo)) {
                            //if task complete status is pending and form step complete
                            if ($taskCompleteInfo['status'] == PENDING && $taskInfo['form_steps'] == $totalFormCompleted) {
                             //update task complete status equal to complete
                                $updateId = $this->Common_model->update_single(
                                    'user_task_master',
                                    ['status' => COMPLETE],
                                    ['where' => ['detail_id' => $taskCompleteInfo['detail_id']]]
                                );
                                              
                                 //earning inset block
                                $earning_where = array('where' => array('task_id ' => $formDetail['task_id'], 'user_id' => $formDetail['user_id']));
                                $earningInfo = $this->Common_model->fetch_data('user_earnings', ['earning_id'], $earning_where, true);
                                     //earning block
                                if (empty($earningInfo)) {
                                    # insert user earnings data

                                    $start_date = date('Y-m-01');
                                    $end_date = date('Y-m-t');
                                    //earning table
                                    $earningArr = array(
                                        'user_id' => $formDetail['user_id'],
                                        'task_id' => $formDetail['task_id'],
                                        'type' => $taskInfo['task_type'],
                                        'month_start_date' => $start_date,
                                        'month_end_date' => $end_date,
                                        'total_earned_points' => $taskInfo['points'],
                                        'reward_payment_amt' => $taskInfo['points'],
                                        'reward_clear_status' => 0,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                    //earning table insert
                                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                                    if ($earningSuccess) {
                                        $whereArr['where'] = ['user_id' => $formDetail['user_id']];

                                            #fetching user details from db
                                        $userInfo = $this->Common_model->fetch_data(
                                            'users',
                                            [
                                                'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                                                'points_earned', 'total_earning', 'task_completed', 'is_bonus_received'
                                            ],
                                            $whereArr,
                                            true
                                        );
                                        //updating user value
                                        $updateUserArray = array(
                                            'task_completed' => ($userInfo['task_completed'] + 1),
                                            'total_earning' => ($userInfo['total_earning'] + $taskInfo['points']),
                                            'points_earned' => ($userInfo['points_earned'] + $taskInfo['points']),
                                            'last_earning_update' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $whereUserArr['where'] = ['user_id' => $formDetail['user_id']];
                                        $isSuccess = $this->Common_model->update_single('users', $updateUserArray, $whereUserArr);
                                    }
                                }
                                        //Insert in wallet log history
                                $insertWalletLog = array(
                                    'user_id' => $formDetail['user_id'],
                                    'point' => $taskInfo['points'],
                                    'title' => $this->lang->line('reward_earned'),
                                    'description' => $taskInfo['action'],
                                    'task_id' => $taskInfo['task_id'],
                                    'type' => $taskInfo['task_type'],
                                    'status' => COMPLETE,
                                    'created_date' => date('Y-m-d H:i:s'),

                                );
                                       //check if task history alredy exist fo
                                $where['where'] = ['user_id' => $formDetail['user_id'], 'task_id' => $taskInfo['task_id'], 'type' => $taskInfo['task_type']];
                                $walletLogInfo = $this->Common_model->fetch_data('ipac_user_wallet_history', ['wallet_id'], $where, true);
                                if (empty($walletLogInfo)) {
                                    #save users details values in DB
                                    $isSuccess = $this->Common_model->insert_single('ipac_user_wallet_history', $insertWalletLog);
                               #if not success
                                    if (!$isSuccess) {
                                        throw new Exception($this->lang->line('try_again'));
                                    }
                                }
                            }
                        }
                    }
                    redirect(base_url() . "campaign-thankyou?status=" . $status . "&step=" . $totalFormCompleted);
                } else {
                    $this->session->set_flashdata('message', "Something went wrong.");
                    $this->load->view("survey/verify-otp", $data);
                }
            } else {
                if ($updateId) {
                    redirect(base_url() . "campaign-thankyou?status=" . 'complete');
                } else {
                    $this->session->set_flashdata('message', "Something went wrong.");
                    $this->load->view("survey/verify-otp", $data);
                }
            }
        } else {
            $this->load->view("survey/verify-otp", $data);
        }
    }


    /**
     * @Function thankyouSurvey
     * @Description load thank you page
     * @return void
     */
    public function thankyouSurvey()
    {
        //get request
        $get = $this->input->get();

        if ($get) {
            $this->load->view("survey/thankyou");
        }
    }

    /**
     *
     * @function checkMobileNumber
     * @description check  survey number duplicacy
     * @return boolean
     */
    public function checkMobileNumber()
    {
        $postData = $this->input->post();
        if (isset($postData) && !empty($postData)) {
            $userId = trim($postData['user_id']);
            $taskId = trim($postData['task_id']);
            $formId = trim($postData['formid']);
            $mobile = trim($postData['mobile']);
            if (!empty($taskId)) {
            //check if mobile number entered by user is alrady used
                $totalFormMobileCheck = $this->Common_model->fetch_count(
                    "user-forms",
                    ['where' => ['form_id' => $formId, 'user_id' => $userId, 'task_id' => $taskId, 'mobile' => $mobile, 'status' => 2]]
                );
            //if data exist return false and true
                if (!empty($totalFormMobileCheck)) {
                    echo 'false';
                    exit;
                } else {
                    echo 'true';
                    exit;
                }
            } else {
                echo 'true';
                exit;
            }
        }
    }

    /**
     *
     * @function checkRegisteredMobileNumber
     * @description check  registered mobile number of user
     * @return boolean
     */
    public function checkRegisteredMobileNumber()
    {
        $postData = $this->input->post();
        if (isset($postData) && !empty($postData)) {
            $userId = trim($postData['user_id']);
            $taskId = trim($postData['task_id']);
            $formId = trim($postData['formid']);
            $mobile = trim($postData['mobile']);
            if (!empty($taskId)) {
                //check if enter mobile number is same of login user
                $userInfo = $this->Common_model->fetch_count(
                    "users",
                    ['where' => ['user_id' => $userId, 'phone_number' => $mobile]]
                );
                //if userinfo exist
                if (!empty($userInfo)) {
                    echo 'false';
                    exit;
                } else {
                    echo 'true';
                    exit;
                }
            } else {
                echo 'true';
                exit;
            }
        }
    }

     //verify the survey
    private function notVerifySurvey($code)
    {
        if ($code) {
            $data['code'] = $code;
            $formDetail = $this->Common_model->fetch_data("user-forms", "*", ['where' => ['code' => $code]], true);
            //update is verify
            if (empty($formDetail)) {
                show404();
                exit();
            }
            $updateId = $this->Common_model->update_single(
                'user-forms',
                ['is_verified' => 1, 'status' => 2],
                ['where' => [
                    'form_id' => $formDetail['form_id'],
                    'user_id' => $formDetail['user_id'],
                    'code' => $data['code']
                ]]
            );
            if (!empty($formDetail['task_id'])) {
              //total form step count
                $totalFormCount = $this->Common_model->fetch_data(
                    "forms",
                    "steps",
                    ['where' => ['fid' => $formDetail['form_id']]],
                    true
                );
                $this->db->where([
                    'form_id' => $formDetail['form_id'],
                    'user_id' => $formDetail['user_id'], 'code' => $data['code']
                ]);
                $this->db->set('steps', 'steps+ 1', false);
                $this->db->update('user-forms');

                $totalCount = $formDetail['steps'] + 1;

                $totalFormCompleted = $this->Common_model->fetch_count(
                    "user-forms",
                    ['where' => [
                        'form_id' => $formDetail['form_id'], 'user_id' => $formDetail['user_id'],
                        'status' => 2,
                        'task_id' => $formDetail['task_id']
                    ]]
                );
                if ($updateId) {
                    //check if form completed
                    $taskInfo = $this->Common_model->fetch_data(
                        'ipac_task_master',
                        ['task_id', 'task_status', 'start_date', 'end_date', 'points', 'task_type', 'action', 'form_steps'],
                        ['where' => ['task_id' => $formDetail['task_id']]],
                        true
                    );
                    $status = '';
                    if ($taskInfo['form_steps'] > $totalFormCompleted) {
                        $status = 'pending';
                    } else {
                        $status = 'complete';
                    }
  
                    //if te=ask info is  not blank
                    if (!empty($taskInfo)) {
                          //task complete info
                        $taskCompleteInfo = $this->Common_model->fetch_data(
                            'user_task_master',
                            ['detail_id', 'status'],
                            ['where' => ['task_id' => $formDetail['task_id'], 'user_id' => $formDetail['user_id'], ]],
                            true
                        );
                          //if not data exist in user task complete table
                        if (empty($taskCompleteInfo)) {
                            $taskCompleteStatus = PENDING;
                            if ($taskInfo['form_steps'] == $totalFormCompleted) {
                                $taskCompleteStatus = COMPLETE;
                            }
                            //insert in user task complete table
                            $taskCompleteInsert = array(
                                'user_id' => $formDetail['user_id'],
                                'task_id' => $formDetail['task_id'],
                                'type' => $taskInfo['task_type'],
                                'status' => $taskCompleteStatus,
                                'complete_using_app' => 1,
                                'complete_using_webhook' => 0,
                                'added_on' => date('Y-m-d H:i:s')
                            );
                                  //insert in task complete
                            $isSuccess = $this->Common_model->insert_single(
                                'user_task_master',
                                $taskCompleteInsert
                            ); #save users details values in DB
                                       //if task complete status is pending and form step complete
                            if ($isSuccess && $taskInfo['form_steps'] == $totalFormCompleted) {
                              //update task complete status equal to complete
                                $updateId = $this->Common_model->update_single(
                                    'user_task_master',
                                    ['status' => COMPLETE],
                                    ['where' => ['detail_id' => $taskCompleteInfo['detail_id']]]
                                );
                                               
                                 //earning inset block
                                $earning_where = array('where' => array('task_id ' =>
                                    $formDetail['task_id'], 'user_id' => $formDetail['user_id']));
                                $earningInfo = $this->Common_model->fetch_data(
                                    'user_earnings',
                                    ['earning_id'],
                                    $earning_where,
                                    true
                                );
                                         //earning block
                                if (empty($earningInfo)) {
                                    # insert user earnings data

                                    $start_date = date('Y-m-01');
                                    $end_date = date('Y-m-t');
                                    //earning table
                                    $earningArr = array(
                                        'user_id' => $formDetail['user_id'],
                                        'task_id' => $formDetail['task_id'],
                                        'type' => $taskInfo['task_type'],
                                        'month_start_date' => $start_date,
                                        'month_end_date' => $end_date,
                                        'total_earned_points' => $taskInfo['points'],
                                        'reward_payment_amt' => $taskInfo['points'],
                                        'reward_clear_status' => 0,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                    //earning table insert
                                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                                    if ($earningSuccess) {
                                        $whereArr['where'] = ['user_id' => $formDetail['user_id']];

                                            #fetching user details from db
                                        $userInfo = $this->Common_model->fetch_data(
                                            'users',
                                            [
                                                'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                                                'points_earned', 'total_earning', 'task_completed', 'is_bonus_received'
                                            ],
                                            $whereArr,
                                            true
                                        );
                                        //updating user value
                                        $updateUserArray = array(
                                            'task_completed' => ($userInfo['task_completed'] + 1),
                                            'total_earning' => ($userInfo['total_earning'] + $taskInfo['points']),
                                            'points_earned' => ($userInfo['points_earned'] + $taskInfo['points']),
                                            'last_earning_update' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $whereUserArr['where'] = ['user_id' => $formDetail['user_id']];
                                        $isSuccess = $this->Common_model->update_single(
                                            'users',
                                            $updateUserArray,
                                            $whereUserArr
                                        );
                                    }
                                }
                                            //Insert in wallet log history
                                $insertWalletLog = array(
                                    'user_id' => $formDetail['user_id'],
                                    'point' => $taskInfo['points'],
                                    'title' => $this->lang->line('reward_earned'),
                                    'description' => $taskInfo['action'],
                                    'task_id' => $taskInfo['task_id'],
                                    'type' => $taskInfo['task_type'],
                                    'status' => COMPLETE,
                                    'created_date' => date('Y-m-d H:i:s'),

                                );
                                           //check if task history alredy exist fo
                                $where['where'] = [
                                    'user_id' => $formDetail['user_id'],
                                    'task_id' => $taskInfo['task_id'], 'type' => $taskInfo['task_type']
                                ];
                                $walletLogInfo = $this->Common_model->fetch_data(
                                    'ipac_user_wallet_history',
                                    ['wallet_id'],
                                    $where,
                                    true
                                );
                                if (empty($walletLogInfo)) {
                                    #save users details values in DB
                                    $isSuccess = $this->Common_model->insert_single(
                                        'ipac_user_wallet_history',
                                        $insertWalletLog
                                    );
                                #if not success
                                    if (!$isSuccess) {
                                        throw new Exception($this->lang->line('try_again'));
                                    }
                                }
                            }
                        }
                           //if user completed all step and task complete is in pending status
                        if (!empty($taskCompleteInfo)) {
                            //if task complete status is pending and form step complete
                            if ($taskCompleteInfo['status'] == PENDING && $taskInfo['form_steps'] == $totalFormCompleted) {
                              //update task complete status equal to complete
                                $updateId = $this->Common_model->update_single(
                                    'user_task_master',
                                    ['status' => COMPLETE],
                                    ['where' => ['detail_id' => $taskCompleteInfo['detail_id']]]
                                );
                                               
                                 //earning inset block
                                $earning_where = array('where' => array(
                                    'task_id ' => $formDetail['task_id'],
                                    'user_id' => $formDetail['user_id']
                                ));
                                $earningInfo = $this->Common_model->fetch_data(
                                    'user_earnings',
                                    ['earning_id'],
                                    $earning_where,
                                    true
                                );
                                         //earning block
                                if (empty($earningInfo)) {
                                    # insert user earnings data

                                    $start_date = date('Y-m-01');
                                    $end_date = date('Y-m-t');
                                    //earning table
                                    $earningArr = array(
                                        'user_id' => $formDetail['user_id'],
                                        'task_id' => $formDetail['task_id'],
                                        'type' => $taskInfo['task_type'],
                                        'month_start_date' => $start_date,
                                        'month_end_date' => $end_date,
                                        'total_earned_points' => $taskInfo['points'],
                                        'reward_payment_amt' => $taskInfo['points'],
                                        'reward_clear_status' => 0,
                                        'inserted_on' => date('Y-m-d H:i:s')
                                    );
                                    //earning table insert
                                    $earningSuccess = $this->Common_model->insert_single('user_earnings', $earningArr);
                                    if ($earningSuccess) {
                                        $whereArr['where'] = ['user_id' => $formDetail['user_id']];

                                            #fetching user details from db
                                        $userInfo = $this->Common_model->fetch_data(
                                            'users',
                                            [
                                                'gender', 'state', 'district', 'registeration_no', 'college', 'is_active',
                                                'points_earned', 'total_earning', 'task_completed', 'is_bonus_received'
                                            ],
                                            $whereArr,
                                            true
                                        );
                                        //updating user value
                                        $updateUserArray = array(
                                            'task_completed' => ($userInfo['task_completed'] + 1),
                                            'total_earning' => ($userInfo['total_earning'] + $taskInfo['points']),
                                            'points_earned' => ($userInfo['points_earned'] + $taskInfo['points']),
                                            'last_earning_update' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        );
                                        $whereUserArr['where'] = ['user_id' => $formDetail['user_id']];
                                        $isSuccess = $this->Common_model->update_single(
                                            'users',
                                            $updateUserArray,
                                            $whereUserArr
                                        );
                                    }
                                }
                                            //Insert in wallet log history
                                $insertWalletLog = array(
                                    'user_id' => $formDetail['user_id'],
                                    'point' => $taskInfo['points'],
                                    'title' => $this->lang->line('reward_earned'),
                                    'description' => $taskInfo['action'],
                                    'task_id' => $taskInfo['task_id'],
                                    'type' => $taskInfo['task_type'],
                                    'status' => COMPLETE,
                                    'created_date' => date('Y-m-d H:i:s'),

                                );
                                           //check if task history alredy exist fo
                                $where['where'] = [
                                    'user_id' => $formDetail['user_id'],
                                    'task_id' => $taskInfo['task_id'], 'type' => $taskInfo['task_type']
                                ];
                                $walletLogInfo = $this->Common_model->fetch_data(
                                    'ipac_user_wallet_history',
                                    ['wallet_id'],
                                    $where,
                                    true
                                );
                                if (empty($walletLogInfo)) {
                                    #save users details values in DB
                                    $isSuccess = $this->Common_model->insert_single(
                                        'ipac_user_wallet_history',
                                        $insertWalletLog
                                    );
                                #if not success
                                    if (!$isSuccess) {
                                        throw new Exception($this->lang->line('try_again'));
                                    }
                                }
                            }
                        }
                    }
                    redirect(base_url() . "campaign-thankyou?status=" . $status . "&step=" . $totalFormCompleted);
                } else {
                    $this->session->set_flashdata('message', "Something went wrong.");
                    $this->load->view("survey/verify-otp", $data);
                }
            } else {
                if ($updateId) {
                    redirect(base_url() . "campaign-thankyou?status=" . 'complete');
                } else {
                    show404();
                    exit();
                }
            }
        } else {
            show404();
            exit();
        }
    }
}
