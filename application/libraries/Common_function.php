<?php

class Common_function
{

    private $androidPushKey = 'AAAABe80dUs:APA91bEPBxpZU2VRH2NbH7d7fg_aMzx_Xehq1F2Vhw_x9nNZIEgSNp68HsQmpoKlCoYIvv4-vNZQEZEb9ihmNvDNTZvUewD9OqJCSmJvr1PGSyo67I9BwhOmWhf-APYPMzwLFYF2as2k';

    public function __construct()
    {
        ini_set('display_errors', 1);

        $this->CI = &get_instance();
        $this->CI->load->library('pagination');
        $this->CI->load->library('email');
        $this->CI->config->load('email');
    }



    public function iosPushOld($deviceToken, $payload)
    {
        $data['aps'] = $payload;
        try {
            /*
             * for developement mode
             */
            //            $apnsHost = 'gateway.sandbox.push.apple.com';
            /*
             * production or distribution mode)
             */
            $apnsHost   = 'gateway.push.apple.com';
            $apnsPort   = '2195';
            //            $apnsCert = base_url() . 'public/ckpm/development.pem';
            $apnsCert   = getcwd() . '/public/ckpm/Onboarding_Dist_1.pem';
            $passPhrase = '1234';

            $streamContext = stream_context_create();
            $a             = stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
            $a             = stream_context_set_option($streamContext, 'ssl', 'passphrase', $passPhrase);

            try {
                $apnsConnection = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
            } catch (Exception $e) {
                echo '<pre>';
                print_r($e);
                die;
            }
            if ($apnsConnection == false) {
                die("Push Sending Failed");
            }
            $payload = json_encode($data);

            if (!empty($payload)) {
                try {
                    /*
                     * Device token is of array then send push using loop
                     */
                    if (is_array($deviceToken)) {
                        foreach ($deviceToken as $token) {
                            if ($token == 'DummyDeviceToken') {
                                continue;
                            }
                            $apnsMessage = chr(0) . pack("n", 32) . pack('H*', $token) . pack("n", strlen($payload)) . $payload;
                            fwrite($apnsConnection, $apnsMessage);
                        }
                    } else {
                        /*
                         * Device token is single then send push
                         */
                        $token       = $deviceToken;
                        $apnsMessage = chr(0) . pack("n", 32) . pack('H*', $token) . pack("n", strlen($payload)) . $payload;
                        if (fwrite($apnsConnection, $apnsMessage)) {
                            return "true";
                        } else {
                            return "false";
                        }
                    }
                } catch (Exception $e) {
                    return true;
                }
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            die;
        }
    }



    public function androidPush($deviceToken, $payload)
    {

        $deviceToken = is_array($deviceToken) ? $deviceToken : array($deviceToken);
        $fields = array(
            'registration_ids' => $deviceToken,
            'data'             => $payload,
            'priority'         => 'high'
        );

        $pushKey = FCM_SERVER_KEY;

        $headers = array(
            'Authorization: key=' . $pushKey,
            'Content-Type: application/json'
        );

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    public function androidTopicPush($topic, $payload)
    {

        $fields = array(
            'to'               => $topic,
            'data'             => $payload,
            'priority'         => 'high'
        );

        $pushKey = FCM_SERVER_KEY;
        $headers = array(
            'Authorization: key=' . $pushKey,
            'Content-Type: application/json'
        );

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function iOSTopicPush($topic, $payload)
    {

        $iOSSilentNotiTypes         = array('14');
        if(in_array($payload['type'], $iOSSilentNotiTypes)){
            $fields = array(
                'to'                => $topic,
                "content_available" => true,
                'data'              => array(
                    'type'          => $payload['type']
                ),
                'notification'      => array(
                    'title'         => '',
                    'body'          => '',
                    'mutable_content' => true
                )
            );
        }else{
            $fields = array(
                'to'                => $topic,
                'data'              => $payload,
                'notification'      => array(
                    'title'         => $payload['title'],
                    'body'          => $payload['message'],
                    'mutable_content' => true,
                    'sound'         => "Tri-tone"
                )
            );
        }


        $pushKey = FCM_SERVER_KEY;
        $headers = array(
            'Authorization: key=' . $pushKey,
            'Content-Type: application/json'
        );

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function iosPush($deviceToken, $payload)
    {

        $deviceToken = is_array($deviceToken) ? $deviceToken : array($deviceToken);
        $fields = array(
            'registration_ids' => $deviceToken,
            'data'             => $payload,
            'notification'      => array(
                'title'         => $payload['title'],
                'body'          => $payload['message'],
                'mutable_content' => true,
                'sound'         => "Tri-tone"
            )/*,
            'priority'          => 'high'*/
        );

        $pushKey = FCM_SERVER_KEY;

        $headers = array(
            'Authorization: key=' . $pushKey,
            'Content-Type: application/json'
        );

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    public function webPush($deviceToken, $payload, $usertype)
    {

        $registrationIDs = array($deviceToken);

        $url = 'https://fcm.googleapis.com/fcm/send';

        $push_data['payload'] = $payload;
        $fields               = array(
            'registration_ids' => $registrationIDs,
            'data'             => $push_data,
        );
        if ($usertype == 1) {
            $androidkey = $this->web_push_key_patientApp;
        } else {
            $androidkey = $this->web_push_key_patientApp;
        }
        $headers = array(
            'Authorization: key=' . $androidkey,
            'Content-Type: application/json'
        );
        //        print_r($fields);die;
        $ch      = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        json_encode($fields);

        $result = curl_exec($ch);
        curl_close($ch);
        //echo $result; die;
        return $result;
    }



    public function sendEmailToUser($mailinfoarr)
    {
        $subject   = $mailinfoarr['subject'];
        $issuccess = $this->sendmail($subject, $mailinfoarr, $mailinfoarr['mailerName']);
        return $issuccess;
    }



    private function sendmail($subject, $data, $mailtemplate)
    {
        $this->CI->email->from($this->CI->config->item('from'), $this->CI->config->item('from_name'));
        $this->CI->email->reply_to($this->CI->config->item('reply_to'), $this->CI->config->item('reply_to_name'));
        $this->CI->email->to($data['email']);
        $this->CI->email->subject($subject);
        $body = $this->CI->load->view('mail/' . $mailtemplate, $data, true);
        $this->CI->email->message($body);
        return $this->CI->email->send() ? true : false;
    }



    public function thumb_create($filename, $filepath, $targetpath, $width = 512, $height = 288)
    {

        try {
            /*             * * a new imagick object ** */
            $im1 = new \Imagick($filepath);

            /*             * * ping the image ** */
            $im1->pingImage($filepath);

            /*             * * read the image into the object ** */
            $im1->readImage($filepath);

            $im1->setImageCompressionQuality(70);
            /*             * * thumbnail the image ** */
            $im1->thumbnailImage($width, $height);

            /*             * * Write the thumbnail to disk ** */

            $im1->writeImage($targetpath . $filename);
            //echo $image;die;
            /*             * * Free resources associated with the Imagick object ** */
            $im1->destroy();

            return true;
        } catch (Exception $e) {
            print($e);
            die;
            return $file;
        }
    }



    public function getthumb($videopath, $uplodedvideoname, $thumbpath)
    {
        echo $videopath . ' video path';
        echo $uplodedvideoname . 'uploaded video path';
        echo $thumbpath . ' thumb path';

        $name          = explode('.', $uplodedvideoname);
        $ext           = array_pop($name);
        $fullvideopath = $videopath . $uplodedvideoname;
        $name          = $name[0] . '.png';
        $thumbpath     = $thumbpath . $name;
        $cmd           = "ffmpeg -i " . $fullvideopath . " -ss 00:00:01.435 -vframes 1 " . $thumbpath . "";
        exec($cmd);
        return $name;
    }



    public function uploadImg($tmppath, $uploadpath, $filename)
    {

        $name     = explode('.', $filename);
        $ext      = array_pop($name);
        $name     = $this->clean($name[0]);
        $filename = $name . '_' . uniqid() . strtotime("now") . '.' . $ext;
        $st       = move_uploaded_file($tmppath, $uploadpath . $filename);
        if ($st) {
            return $filename;
        } else {
            return false;
        }
    }



    function clean($string)
    {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }



    public function pagination($pageurl, $totalrows, $limit)
    {
        $config                         = array();
        $config["per_page"]             = $limit;
        $config['base_url']             = base_url() . $pageurl;
        $config['use_page_numbers']     = true;
        $config['page_query_string']    = true;
        $config['reuse_query_string']   = true;
        $config['next_link']            = 'Next';
        $config['prev_link']            = 'Prev';
        $config['query_string_segment'] = 'page';
        $config['total_rows']           = $totalrows;
        $config['attributes']           = array('class' => "pagination prolist-pagination");
        $this->CI->pagination->initialize($config);
        return $this->CI->pagination->create_links();
    }



    public function sendPush($pushData)
    {

        $whereArr             = [];
        $whereArr['where']    = ['user_id' => $pushData['receiver_id']];
        $whereArr['group_by'] = 'device_token';
        $whereArr['order_by'] = ['login_time' => 'desc'];

        $userInfo = $this->CI->Common_model->fetch_data('session', ['user_id', 'platform', 'device_token', 'login_status'], $whereArr);

        foreach ($userInfo as $user) {
            if (!empty($user['device_token']) && strlen($user['device_token']) > 60 && $user['login_status'] == 1) {
                if ($user['platform'] == 1) {
                    $isSuccess = $this->androidPush($user['device_token'], $pushData['androidPayload']);
                } elseif ($user['platform'] == 2) {
                    $this->iosPush($user['device_token'], $pushData['iosPayload']);
                }
            }
        }
    }



    public function send_otp($params)
    {
        $CI = &get_instance();

        $CI->load->library('SNSPush');

        require_once APPPATH . "libraries/SNSPush.php";

        $lib = new \SNSPush();

        return $isSuccess = $lib->send_msg($params);
    }

    public function prepareAssociativeArr($inputArr = array(), $inputKey = '')
    {
        $retArr                             = array();
        if (!empty($inputArr) && $inputKey != '') {
            foreach ($inputArr as $value) {
                $retArr[$value[$inputKey]]  = $value;
            }
        } else {
            $retArr                         = $inputArr;
        }
        return $retArr;
    }

    public function prepareMultipleAssociativeArr($inputArr = array(), $inputKey = '', $inputKey2 = '', $inputKey3 = '', $level = '')
    {
        $retArr         = array();
        if (!empty($inputArr) && $inputKey != '') {
            if ($inputKey2 == '') {
                foreach ($inputArr as $value) {
                    $retArr[$value[$inputKey]][]                            = $value;
                }
            } else {
                if ($level == '3') {
                    if ($inputKey3 == '') {
                        foreach ($inputArr as $value) {
                            $retArr[$value[$inputKey]][$value[$inputKey2]][]                    = $value;
                        }
                    } else {
                        foreach ($inputArr as $value) {
                            $retArr[$value[$inputKey]][$value[$inputKey2]][$value[$inputKey3]]  = $value;
                        }
                    }
                } else {
                    foreach ($inputArr as $value) {
                        $retArr[$value[$inputKey]][$value[$inputKey2]]      = $value;
                    }
                }
            }
        } else {
            $retArr     = $inputArr;
        }
        return $retArr;
    }

    /*
    *operation :: insert/update/delete
    *action :: From while file it has been calling
    *inputdata :: Data of user device details before and after 
    */
    public function writeDeviceDetailsLog($operation = '',$action = '',$inputData = array()){

        if(strtolower(STORE_USER_DEVICE_LOGS) == 'yes'){
            $file_name = 'user_device_log_details.txt';
            $file_path = $this->CI->config->item('site_path') .'public/logs/'. $file_name;

            if (is_writable($file_path)) {

                if (!$handle = fopen($file_path, 'a')) {
                    echo "Cannot open file ($file_path)";
                    exit;
                }

                $data       = '';
                $data       .= "====================================================================================================== \n";
                $data       .= " Date :: ".date('Y-m-d H:i:s')." \n";
                $data       .= " Operation :: ".$operation." \n";
                $data       .= " File Name :: ".$action." \n";
                $data       .= " Inputs :: ".json_encode($inputData)."  \n";

                if (fwrite($handle, $data . "\n") === FALSE) {
                    echo "Cannot write to file ($file_path)";
                    exit;
                }

                fclose($handle);
            } else {
                echo "The file $filename is not writable";
            }
        }           

        return true;
    }

    /* To generate Registration id of an user */
    function getUserRegistrationNo($districtId,$userId){
        $usersDataCond              = array('where' => array('district' => $districtId,'user_id !=' => $userId));
        $usersDataCond['order_by']  = ['user_id' => 'desc'];
        $usersDataCond['limit'][0]  = 1;
        $usersData                  = $this->CI->Common_model->fetch_data('users','registeration_no',  $usersDataCond);        
        $newRegId                   = "";
        if(!empty($usersData) && $usersData[0]['registeration_no'] != ''){
            $lastRegistrationId     = $usersData[0]['registeration_no'];
            $suffixCode             = str_pad((substr($lastRegistrationId,10,5) + 1), 5, '0', STR_PAD_LEFT); 
            $newRegId               = substr($lastRegistrationId,0,7).substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3). $suffixCode;
        }else{
            $districtDataCond       = array('where' => array('district_id' => $districtId));
            $districtInfo           = $this->CI->Common_model->fetch_data('district','district_display_code,district_name,user_count',  $districtDataCond);
            $districtCodePadded     = str_pad($districtInfo[0]['district_display_code'], 4, '0', STR_PAD_LEFT); 
            $newRegId               = "KCR".$districtCodePadded.substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3).'00001';
        }
        return $newRegId;
    }
}
