<?php
//echo APPPATH . "composer/vendor/autoload.php";die;
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'composer/vendor/autoload.php';

use Aws\Sns\SnsClient;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class SNSPush
{

    const GCM_ARN = SNS_GCM_ARN;
    const APNS_ARN = SNS_APNS_ARN;
    const REGION = SNS_REGION;

    private $topicARN;
    private $sns;
    private $protocol;

    const PROTOCOL = 'application';

    public function __construct($topicARN = "", $protocol = "")
    {
        $this->topicARN = '';
        $this->protocol = $protocol;
        $this->sns = SnsClient::factory(array(
            'credentials' => array(
                'key'    => SNS_KEY,
                'secret' => SNS_SECRET,
            ),
            'region' => self::REGION,
            'version' => 'latest'
        ));
    }
    /**
     * Add device end point to paltform
     *
     * @param string $deviceToken
     * @param string $userData
     * @param string $deviceType
     * @param integer $count
     * @return array
     */
    public function addDeviceEndPoint($deviceToken, $deviceType, $userData = "", $count = 0)
    {
        if ($count > 2) {
            return [
                "success" => false,
                "message" => "Error"
            ];
        }
        if ($deviceType == "android") {
            $platformApplicationArn = SNS_GCM_ARN;
        } elseif ($deviceType == "ios") {
            $platformApplicationArn = SNS_APNS_ARN;
        } else {
            return false;
        }
        try {
            $result = $this->sns->createPlatformEndpoint(array(
                'PlatformApplicationArn' => $platformApplicationArn,
                'CustomUserData' => $userData,
                'Token' => $deviceToken,
            ));
            return [
                "success" => true,
                "message" => "OK",
                "result" => $result
            ];
        } catch (\Exception $error) {
            //file_put_contents(getcwd(). "/error_logs/error.txt", $error);
            $substr = $this->getStringBetween($error, "Endpoint ", " already exists with the same Token");
            if (!empty($substr) && strpos($substr, 'endpoint') !== false) {
                $this->deleteDeviceEndPoint($substr);
                $count++;
                return $this->addDeviceEndPoint($deviceToken, $deviceType, $userData, $count);
            } else {
                return [
                    "success" => false,
                    "message" => "Error",
                    "error" => $error
                ];
            }
        }
    }

    public function deleteDeviceEndPoint($deviceArn)
    {
        try {
            $this->sns->deleteEndpoint([
                "EndpointArn" => $deviceArn
            ]);
            return [
                "success" => true,
                "message" => "OK"
            ];
        } catch (\Exception $error) {
            return [
                "success" => false,
                "message" => "error"
            ];
        }
    }

    public function subscribeDevicetoTopic($endPointArn)
    {
        try {
            $result = $this->sns->subscribe([
                'Endpoint' => $endPointArn,
                'Protocol' => $this->protocol,
                'TopicArn' => $this->topicARN,
            ]);
            return [
                "success" => true,
                "message" => "OK",
                "result" => $result
            ];
        } catch (\Exception $error) {
            return [
                "success" => false,
                "message" => "error",
            ];
        }
    }



    public function sendMessageByPlatform($message, $platform)
    {
        $appArn = "";
        if ($platform == "android") {
            $appArn = self::GCM_ARN;
        } elseif ($platform == "ios") {
            $appArn = self::APNS_ARN;
        } else {
            return [
                "success" => false,
                "message" => "error"
            ];
        }

        $endPointList = [];

        $endPointList = $this->sns->listEndpointsByPlatformApplication([
            'PlatformApplicationArn' => $appArn
        ]);

        foreach ($endPointList["Endpoints"] as $endPoint) {
            $endPointArn = $endPoint['EndpointArn'];
            try {
                $this->sns->publish([
                    'Message' => json_encode($message),
                    'MessageStructure' => 'json',
                    'TargetArn' => $endpointArn
                ]);
                return [
                    "success" => true,
                    "message" => "OK"
                ];
            } catch (\Exception $error) {
                return [
                    "success" => false,
                    "message" => "error"
                ];
            }
        }
    }

    public function deleteSubscription($subscriptionArn)
    {
        try {
            $this->sns->unsubscribe([
                'SubscriptionArn' => $subscriptionArn
            ]);
            return [
                "success" => true,
                "message" => "OK"
            ];
        } catch (\Exception $error) {
            return [
                "success" => false,
                "message" => "error"
            ];
        }
    }


    /*randon sent push by admin*/
    public function asyncCouponPublish($message, $arns)
    {
        try {
            $promises = array();
            $arr = [];


            foreach ($arns as $row) {
                $promises[] = $this->sns->publishAsync([
                    'Message' => json_encode($message),
                    'MessageStructure' => 'json',
                    'TargetArn' => $row['arn_endpoint']
                ]);
                $allPromise = \GuzzleHttp\Promise\all($promises);
                $data_promise = $allPromise->wait();
                file_put_contents(getcwd() . "/error_logs/error.txt", $data_promise);
            }
            return [
                "success" => true,
                "message" => "OK",
                "result" => []
            ];
        } catch (\Exception $error) {
            file_put_contents(getcwd() . "/error_logs/error.txt", $error);

            //file_put_contents(getcwd(). "/error_logs/error.txt", $error);
            return [
                "success" => false,
                "message" => "error"
            ];
        }
    }

    /*randon sent push by admin*/
    public function asyncPublish($arns, $post_data)
    {
        $CI = &get_instance();
        $CI->load->model('Common_model');
        try {
            $notification_id = uniqid();
            $message = [
                "default" => $post_data["title"],
                "APNS_SANDBOX" => json_encode([
                    "aps" => [
                        "alert" => array(
                            "title" => $post_data["title"],
                            "body" => $post_data["message"]
                        ),
                        "sound" => "default",
                        "mutable-content" => 1,
                        "badge" => 1,
                        "data" => array(
                            "attachment_url" => isset($post_data["image"]) ? $post_data["image"] : '',
                            "ext_link" => '',
                            "url_title" => '',
                            'notification_id' => $notification_id,
                            "content_type" => "image",
                            "type" => ADMIN_NOTIFICATION
                        )
                    ]
                ]),
                "GCM" => json_encode([
                    "data" => [
                        "title" => $post_data["title"],
                        "body" => $post_data["message"],
                        "message" => $post_data["message"],
                        "type" => ADMIN_NOTIFICATION,
                        "image" => isset($post_data["image"]) ? $post_data["image"] : '',
                        "ext_link" => '',
                        "url_title" => '',
                        "news_id" => '',
                        "task_id" => '',
                        'notification_id' => $notification_id

                    ]
                ])
            ];

            $result = $this->sns->createTopic(array(
                'Name' => 'ipac_topic_' . time(),
            ));
            $topicERN = $result['TopicArn'];
            foreach ($arns as $row => $val) {
                $result = $this->sns->subscribe(array(
                    'TopicArn' => $topicERN,
                    'Protocol' => self::PROTOCOL,
                    'Endpoint' => $val['arn_endpoint'],
                ));
            }
            $this->sendMessageByTopic($message, $topicERN);
            return [
                "success" => true,
                "message" => "OK",
                "result" => []
            ];
        } catch (\Exception $error) {
            return [
                "success" => false,
                "message" =>  $error->getMessage()
            ];
        }
    }
    /*randon sent push by admin*/
    public function asyncPublish2($arns, $message)
    {
        $CI = &get_instance();
        // $CI->load->model('Common_model');
        try {
            $promises = array();
            $i = 0;
            $insert_data = array();
            $result = $this->sns->createTopic(array(
                // Name is required
                'Name' => 'ipac_topic_' . time(),
            ));

            $topicERN = $result['TopicArn'];
            //$userIds=implode(",", array_column($arns, "arn_endpoint"));

            foreach ($arns as $row => $val) {
                //$promises[] = $this->sns->publishAsync([
                //'Message' => json_encode($message),
                //'MessageStructure' => 'json',
                //'TargetArn' => $val['arn_endpoint']
                //]);
                //              pr($val['arn_endpoint']);die;
                $result = $this->sns->subscribe(array(
                    // TopicArn is required
                    'TopicArn' => $topicERN,
                    // Protocol is required
                    'Protocol' => self::PROTOCOL,
                    'Endpoint' => $val['arn_endpoint'],
                ));

                // file_put_contents(getcwd(). "/error_logs/error.txt", $val['user_id']);
                $i++;
            }
            $this->sendMessageByTopic($message, $topicERN);
            // $allPromise = \GuzzleHttp\Promise\all($promises);
            //$data_promise = $allPromise->wait();
            //  file_put_contents(getcwd(). "/error_logs/error.txt", $data_promise);
            return [
                "success" => true,
                "message" => "OK",
                "result" => []
            ];
        } catch (\Exception $error) {
            file_put_contents(getcwd() . "/error_logs/error.txt", $error);
            return [
                "success" => false,
                "message" => $error->getMessage()
            ];
        }
    }
    public function send_msg($params)
    {
        $result = array();
        try {
            $result = $this->sns->publish([
                'Message' => $params['message'],
                'PhoneNumber' => $params['phone'],
                'Subject' => $params['subject']
            ]);
            return [
                "success" => true,
                "message" => "OK"
            ];
        } catch (\Exception $error) {
            return [
                "success" => false,
                "message" => "error"
            ];
        }
    }

    private function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    /**
     *
     * @param type $message
     * @return type
     */
    public function sendMessageByTopic($message, $topicErn)
    {
        try {
            $result = $this->sns->publish(
                [
                    'Message' => json_encode($message),
                    'MessageStructure' => 'json',
                    'TopicArn' => $topicErn
                ]
            );
            return [
                "success" => true,
                "message" => "OK",
                "result" => $result
            ];
        } catch (\Exception $error) {
            return [
                "success" => false,
                "message" => "error",
                "error" => $error
            ];
        }
    }
}
