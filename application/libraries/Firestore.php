

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Storage\StorageClient;

class Firestore {

    protected $config	= array();
    protected $storage;

    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();

        $this->storage = new StorageClient([
            'keyFilePath' => $this->CI->config->item('firebase_app_key')
        ]);
        $this->storage = new StorageClient([
            'projectId' => 'stalin-ani'
        ]);

    }

    
    public function init()
    {
       return $firebase = new FirestoreClient([
            'projectId' => 'stalin-ani',
            'keyFile' => json_decode(file_get_contents($this->CI->config->item('firebase_app_key')), true)
        ]);
    }
}