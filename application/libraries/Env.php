<?php 
defined("BASEPATH") OR exit("No direct script access allowed");

require_once APPPATH . "libraries/phpdotenv/Dotenv.php";
require_once APPPATH . "libraries/phpdotenv/Loader.php";
require_once APPPATH . "libraries/phpdotenv/Validator.php";

class Env 
{
	public function __construct()
	{
		
	}

	public function loadEnv() 
	{	
		$dotenv = new Dotenv\Dotenv(getcwd());
		$dotenv->load();
		
	}
}
