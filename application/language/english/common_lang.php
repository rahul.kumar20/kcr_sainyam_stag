<?php
defined('BASEPATH') or exit('No direct script access allowed');
/* WEBSITE LANG FILES */
$lang["not_an_image"] = "Not an Image";
$lang["image_too_big"] = "Image too large";
$lang["use_app_to_signin"] = "Can't Sign in using Website use Android or IOS App, thank you";
/*
 * English language
 */
/* API LANGFILES */
$lang['success_prefix'] = '<label class="alert alert-success alert-dismissable" style="margin-bottom:0px;"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><!--<strong>Success! </strong>-->';
$lang['success_suffix'] = '</label>';

$lang['error_prefix'] = '<div class="alert alert-danger alert-dismissable" style="margin-bottom:0px"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><strong>Error!</strong> ';
$lang['error_suffix'] = '</div>';

$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_ip_address_time_limit'] = 'This IP Address has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';
$lang['invalid_promo_code'] = 'Entered promo code is invalid, please check entered promo code';
$lang['promo_code_applied'] = 'Promocode successfully applied';

/* SEERVE LANG */
/* API MESSAGE */
$lang["invalid_request"] = "Invalid request, Check your request";
$lang["missing_parameter"] = "Parameter missing";
$lang["missing_json_parameter"] = "JSON parameter missing";
/* HEADER MESSAGES */
$lang["not_authenticated"] = "Not authenticated, permission denied";
$lang["unauthorized_access"] = "Not authorized, permission denied";
$lang["access_token_not_set"] = "Access token not set, permission denied";
/* MISMATCH */
$lang["old_password_mismatch"] = "Old password does not match";
$lang["social_type_not_supported"] = "Social Network Not supported";
/* ACTIONS */
$lang["site_information_fetched"] = "Site Infomation successfully fetched";
$lang["success_recommended_store_fetched"] = "Recommended Store fetched";
$lang["mail_not_sent"] = "Mail alert not sent";
$lang["password_updated"] = "Password changed successfully";
$lang["otp_set"] = "OTP set successfully";
$lang["otp_incorrect"] = "Entered OTP is not valid";
$lang["otp_verify"] = "OTP has been successfully verified";
$lang["otp_expire"] = "OTP has been expired";
$lang["otp_reset_request"] = "Request for new OTP by phone/mail";
$lang["password_reset_successfully"] = "Password reset successfully";
$lang["mobile_not_verified"] = "Mobile not verified";

/* FIELD EXISTS */
$lang["email_exists"] = "Email Exists";
$lang["vendor_social_exists"] = "Vendor social id exists";
$lang["mobile_exists"] = "Mobile Number Exists";
$lang["social_id_exists"] = "Social Id Exists";
$lang["user_exists"] = "User Exists";
$lang['select_category_exist'] = 'Category has been selected by user';

/* FIELD DOES NOT EXISTS */
$lang["email_does_not_exist"] = "Email does not exist";
$lang["mobile_does_not_exist"] = "Mobile Number does not exist";
$lang["social_id_does_not_exist"] = "Social Id does not exist";
$lang["user_does_not_exists"] = "User does not exist";
/* INVALID PARAMS */
$lang["invalid_email_format"] = "Invalid email format";
$lang["invalid_parameter"] = "Entered parameter is invalid";
$lang["invalid_date"] = "Invalid Date of Birth";
$lang["invalid_mobile_format"] = "Invalid mobile format";
$lang["invalid_name_format"] = "Invalid name format";
$lang["invalid_customer_value"] = "Invalid customer value";
$lang["invalid_gender_value"] = "Invalid gender value";
$lang["invalid_credentials"] = "Invalid Credentials";
$lang["invalid_usertype"] = "Invalid User Type, Requires user_type Parameter";
$lang["invalid_user_id"] = "Invalid User Id format";
$lang["invalid_access_token"] = "Invalid Access Token";
$lang["sorry_ivalid_email"] = "Sorry, we don't recognise this email address";
$lang['somthing_went_wrong'] = 'Something Went Worng Please Try After SomeTime';

/* IMAGES */
$lang['invalid_image_format'] = 'Please select valid image format';

/* STATUS */
$lang["mobile_number_empty"] = "Mobile Number Empty";
$lang["user_inactive"] = "User Inactive";
$lang["mobile_updated"] = "Mobile Number for the user has been updated and no longer exists";
$lang["user_registered_as_merchant"] = "User is registered as Merchant";
$lang["user_registered_as_customer"] = "User is registered as Customer";
$lang["user_registered_as_unknown"] = "Unknown User Type";

$lang['user_blocked'] = 'You have been blocked from using Seerve, please contact Seerve for more info';

/* no data */
$lang['no_records_found'] = 'No records found';
$lang['incorrect_filter_type'] = 'filter type not exists';
$lang['reset_link_sent_successfully'] = 'Reset Link Sent Successfully';
$lang['something_went_Worng'] = 'Something Went Worng Please Try After SomeTime';

//
$lang['title'] = 'Title';
$lang['title'] = 'Title (Tamil)';
$lang['page_desc'] = 'Page Description';
$lang['page_desc'] = 'Page Description (Tamil)';
$lang['status'] = 'Status';
$lang['page_added'] = 'Your page has been added successfully.';
$lang['page_updated'] = 'Your page has been updated successfully.';
$lang['version_added'] = 'App version has been added successfully.';
$lang['version_updated'] = 'App version has been updated successfully.';

$lang['version_name'] = 'Version Name';
$lang['version_title'] = 'Version Title';
$lang['description'] = 'Description';
$lang['platform'] = 'Platform';
$lang['update_type'] = 'Update Type';
$lang['current_version'] = 'Current Version';
$lang['old_pass'] = 'Ols password';
$lang['new_password'] = 'New password';
$lang['confirm_password'] = 'Confirm password';

$lang['profile_update'] = 'Profile updated successfully.';
$lang['email'] = 'email';
$lang['password'] = 'password';
$lang['invalid_email_password'] = 'Either the email address or password is incorrect.';
$lang['invalid_email'] = 'This email is not registered with us.';
$lang['reset_email'] = 'A reset password link has been sent to your registered email address';
$lang['login_welcome'] = 'You have successfully logged in';
$lang['welcome'] = 'Welcome!';
$lang['sorry'] = 'Sorry!';
$lang['reset_password'] = "Reset your password";
$lang['something_went_wrong'] = "Some thing went wrong!! Please try again";

$lang['chat_not_deleted'] = 'Chat not deleted somthing went wrong';
$lang['chat_deleted'] = 'Chat deleted successfully.';
$lang['chat_read'] = 'Chat read successfully.';
$lang['chat_not_read'] = 'Chat not read somthing went wrong.';

$lang['message_deleted'] = 'Message deleted successfully.';
$lang['message_not_read'] = 'Message not read somthing went wrong.';

$lang['NO_RECORD_FOUND'] = 'No record found.';

$lang['notification_added'] = 'Notification sent';
$lang['password_changed'] = 'Password has been reset successfully';
$lang['name_missing'] = 'Please enter the name';

$lang['subadmin_created'] = 'Subadmin created successfully';
$lang['subadmin_updated'] = 'Subadmin info has been updated';

$lang['delete_success'] = 'Deleted successfully';
$lang['block_success'] = 'Blocked successfully';
$lang['unblocked_success'] = 'Unblocked successfully';

$lang['success'] = 'Success!';
$lang['error'] = 'Error!';
$lang['try_again'] = 'Please try again';
$lang['forbidden'] = 'Forbidden!';
$lang['permission_denied'] = 'Permission Denied';
/*
 *  Reset Password Language
 */
$lang['link_expired'] = 'This link is expired';
$lang['invalid_token'] = 'Token is not valid';
$lang['invalid_request'] = 'Token is missing';
$lang['account_blocked'] = 'Your account is blocked';
$lang['reset_success'] = 'Password successfully reseted';
$lang['password_already_reset'] = 'You have already reset your password';

/**
 * Sub admin permission
 */
$lang['user'] = 'Manage User';
$lang['cms'] = 'Manage Content';
$lang['subadmin'] = 'Manage Sub-Admin';
$lang['notification'] = 'Manage Notification';
$lang['version'] = 'Manage Version';

$lang['email_not_found'] = 'Email Does not Exist';
$lang['no_user'] = "Sorry!! no user found";
$lang['list_fetched'] = "list fetched";

$lang['reset_password'] = "Reset Password";

$lang['subscription_created'] = 'Subscription is created successfully';
$lang['subscription_updated'] = 'Subscription is updated successfully';
$lang['logout_successful'] = 'You have successfully logged Out';
$lang['no_password_mastch'] = "Password didn't match.";
$lang['inactive_post'] = "Inactive post.";

$lang['category_added'] = "Category is added successfully.";
$lang['no_category'] = "Sorry!! no category found";
$lang['category_updated'] = "Category is updated successfully.";

$lang["news_success"] = "Home Content added successfully";
$lang["event_success"] = "Event added successfully";
$lang["news_upd_success"] = "Home Content updated successfully";
$lang["event_upd_success"] = "Event updated successfully";
$lang["user_add_success"] = "User added successfully";

//task success
$lang["task_success"] = "Task added successfully";
//notification text

$lang['news_add_notification'] = 'news added';
$lang['task_add_notification'] = 'task added';
$lang['request_accept_not'] = 'Your request to update your profile has been successfully accepted by the GDB Admin Team.';

$lang['request_accept'] = 'Request successfully accepted';
$lang['request_reject'] = 'Request successfully rejected';
//user csv nmessage
$lang['all_required'] = 'Required fields are missing';
$lang['invalid_email'] = 'Email id is not valid';
$lang['phone_number_invalid'] = 'Phone number is not valid';
$lang['whats_number_invalid'] = 'Whatsapp number is not valid';
$lang['paytm_number_invalid'] = 'Paytm number is not valid';

$lang['email_already_exist'] = 'Email Id already exist';
$lang['phone_already_exist'] = 'Phone no already exist';
$lang['dob_formate'] = 'Date of birth is not in proper format i.e (d-m-Y)';
$lang['invalid_state'] = 'Invalid state';
$lang['invalid_district'] = 'Invalid district';
$lang['invalid_college'] = 'Invalid College';
$lang['invalid_csv'] = 'Invalid file type. Please upload csv file only';
$lang['future_dob_error'] = 'Future DOB is not allowed';
//wallet
$lang['add_expense'] = 'Expense Added';
$lang['reward_earned'] = 'Reward Earned';
$lang['amount_redeem'] = 'Reward Redeem';
$lang['amount_paid'] = 'Paid';

//task tye
$lang['facebook'] = 'Facebook';
$lang['twitter'] = 'Twitter';
$lang['whatsapp'] = 'Whatsapp';
$lang['offline'] = 'Offline';
$lang['youtube'] = 'Youtube';
$lang['online'] = 'Online';
$lang['default'] = 'Default';
$lang['istagram'] = 'Instagram';
//whats app
$lang['change_dp'] = 'Change DP';
$lang['create_group'] = 'Create Group';
$lang['about_us'] = 'About us';
$lang['whatsapp_video'] = 'Share Video';
$lang['whatsapp_audio'] = 'Share Audio';
$lang['whatsapp_text'] = 'Share Text';
$lang['whatsapp_gif'] = 'Share GIF';
$lang['whatsapp_link'] = 'Share Link';
$lang['whatsapp_image'] = 'Share Image';
$lang['whatsapp_status'] = 'Change Status';

$lang['share'] = 'Share';
$lang['tweet'] = 'Tweet';
$lang['retweet'] = 'Retweet';
$lang['poll_retweet'] = 'Poll&Retweet';
$lang['follow'] = 'Follow';
$lang['like'] = 'Like';
$lang['post'] = 'Post';
$lang['comment'] = 'comment';
$lang['subscribe'] = 'Subscribe';
$lang['qrcode'] = 'Qrcode';
$lang['form'] = 'Form';
$lang['event'] = 'Event';
$lang['multimedia'] = 'Multimedia';
$lang['image'] = 'Image';
$lang['video'] = 'Video';
$lang['audio'] = 'Audio';
$lang['install_twitter'] = 'Install twitter app';
$lang['install_facebook'] = 'Install Facebook app';
$lang['install_whatsapp'] = 'Install WhatsApp app';
$lang['install_youtube'] = 'Install Youtube app';
$lang['follow_twitter'] = 'Follow MK Stalin on twitter';
$lang['follow_facebook'] = 'Follow MK Stalin Facebook page';
$lang['subscribe_youtube'] = 'Subscribe to MK Stalin Youtube channel';
$lang['upload_whatsapp'] = 'Upload WhatsApp number screenshot';

$lang['term_condition_added_success'] = 'Term & Condition added successfully';
$lang['term_condition_updated_success'] = 'Term & Condition updated successfully';
$lang["category_add_success"] = "Category added successfully";

$lang["help_line_number_req"] = "Helpline number required";
$lang["setting_update_Success"] = "Setting updated successfully";
