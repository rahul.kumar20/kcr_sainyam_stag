<?php

/*
 * English language
 */

$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';
$lang['somthing_went_wrong'] = 'Something Went Worng Please Try After SomeTime';

/*
 * Custom Messages
 */
$lang['registration_success'] = 'You have successfully registered';
$lang['header_missing'] = 'Header is missing';
$lang['login_successful'] = 'You have successfully logged In';
$lang['account_blocked'] = 'Your account is blocked please contact admin';
$lang['account_inactive'] = 'Your account is not activated';
$lang['invalid_credentials'] = 'You  have entered invalid credentials';
$lang['not_registered'] = 'Your phone number is not registered with us';

$lang['email_link_sent'] = 'We have sent you the link to reset your password';
$lang['password_already_reset'] = 'You have already reset your password';
$lang['email_not_exists'] = 'This email is not registered with us';
$lang['password_reset_success'] = 'You have successfully reset your password';
$lang['reset_password'] = "Reset Password";
/*
 * Friend Section language
 */
$lang['req_already_sent'] = 'You have already send request to this user';
$lang['req_already_recieved'] = 'You have already received request from this user';
$lang['request_sent'] = 'Request sent successfully';
$lang['req_accepted'] = 'Request accepted successfully';
$lang['req_rejected'] = 'Request rejected successfully';
$lang['pending_request_fetched'] = 'Pending request fetched successfully';
$lang['sent_pending_request_fetched'] = 'Sent Pending request fetched successfully';
$lang['friends_list_fetched'] = 'Friends list fetched successfully';
$lang['user_list_fetched'] = 'All users list fetched successfully';
$lang['no_request_found'] = 'No requests found';
$lang['no_friends_found'] = 'No friends found';
$lang['no_users_found'] = 'No users found';
/*
 * Review Language
 */
$lang['review_success'] = 'Review successfull';
$lang['review_exist'] = 'You have already reviewed this post';
$lang['reviews_list_fetched'] = 'Reviews list fetched successfully';
$lang['no_reviews_found'] = 'No reviews found';
$lang['review_update_success'] = 'Review updated';
$lang['delete_success'] = 'Review Deleted';
/*
 * Comment Language
 */
$lang['comment_posted'] = 'Comment posted successfully';
$lang['comment_update_success'] = 'Comment updated successfully';
$lang['comment_delete_success'] = 'Comment Deleted';
$lang['comments_list_fetched'] = 'Comments list fetched successfully';
$lang['no_comments_found'] = 'No comments found';
/*
 * Follow-Following Language
 */
$lang['already_following'] = 'You have already followed that user';
$lang['follow_success'] = 'Follow request success';
$lang['followers_list_fetched'] = 'Followers list fetched';
$lang['followings_list_fetched'] = 'Following users list fetched';
$lang['no_followers'] = 'No followers found';
$lang['no_followings'] = 'No followings found';
$lang['unfollow_success'] = 'Unfollow success';
/*
 * Favorite-Unfavorite Langugage
 */
$lang['favorite_success'] = 'Favorite success';
$lang['unfavorite_success'] = 'Unfavorite success';
$lang['already_favorite'] = 'Already favorite';
$lang['favorite_list_fetched'] = 'Favorite users list fetched';
/*
 * Password language
 */
$lang['old_password_wrong'] = 'Old password not matched';
$lang['password_exist'] = 'New password is same as old password';
$lang['password_change_success'] = 'Password changed successfully';
/*
 * Feeds language
 */
$lang['empty_feed'] = 'You can post a empty feed';
/*
 * Common language
 */
$lang['invalid_access_token'] = 'Access Token is not valid';
$lang['access_token_expired'] = 'Session expired';
$lang['try_again'] = 'Something is not right please try again';
$lang['missing_parameter'] = 'Required parameter are missing.';
$lang['invalid_phone'] = 'Please enter a valid 10 digit phone number';
$lang['invalid_dob'] = 'Please enter a valid dob in m-d-Y format';
$lang['message_sent'] = 'Message Sent';
$lang['no_data_found'] = 'No Data Found.';
$lang['success'] = 'Success.';
$lang['account_exist'] = 'This email is already exist';
$lang['feed_posted'] = 'Feed posted successfully';
$lang['feed_updated'] = 'Feed updated successfully';
$lang['feed_deleted'] = 'Feed is deleted successfully';
$lang['feed_reported'] = 'Feed is reported successfully';
$lang['feed_already_reported'] = 'Feed is already reported.';
$lang['location_saved'] = 'Location is saved successfully.';
$lang['event_data_saved'] = 'Event data is saved successfully.';
$lang['subscription_created'] = 'Subscription is created successfully.';

//Subscription API
$lang['subscription_success'] = 'The user is subscribed successfully.';
$lang['msg_sfx'] = 'Please enter the ';
$lang['message_not_deleted'] = 'Message not deleted.Try again';

//post
$lang['inactive_post'] = 'Inactive post.';

$lang['phone_exist'] = 'This phone number already exist';

$lang['email_and_phone_missing'] = 'Please enter email id or phone number.';

/* * *
 * OTP Language Variables
 */
$lang['otp_sent'] = 'OTP sent';
$lang["mobile_does_not_exist"] = "Mobile Number does not exist";
$lang["otp_mismatch_expired"] = "Provided OTP is mismatched or expired";
$lang["otp_verified"] = "OTP Verified Successfully";

$lang['list_fetched'] = "list fetched";
$lang['product_list_fetched'] = "Product List Fetched";
$lang['added_in_cart'] = "Product Added In Cart Successfuly";
$lang['already_in_cart'] = "Product Alredy In Cart";
$lang['cart_updated'] = "cart Updated Succcessfully";
$lang['low_stock'] = "Quantity is More Than Present Stock";
$lang['no_user'] = "Sorry!! no user found";

$lang['address_fetched'] = "Address Fetched Successfully";
$lang['cart_detail'] = "Cart Detail Fetched Successfully";
$lang['profile_fetched'] = "Profile Fetched Successfully";
$lang['category_fetched'] = "Category Fetched Successfully";
$lang['version_fetched'] = "App Version Fetched Successfully";
$lang['profile_updated'] = "Your profile request has been submitted";
$lang['proof_deleted'] = "Proof Deleted Successfully";

$lang["news_success"] = "News added successfully";
$lang["contact_success"] = "Your request to contact us has been successfully registered. We will get in touch with you in the next 48 hours.";
$lang["number_not_verified"] = "Your mobile number is not yet verified. Please click here Link to verify the same.";
$lang["invalid_referal"] = "The referral code you've entered is invalid.  Please check the code and try again.";
$lang["request_profile_edit"] = "Your request to update profile successfully made.Please wait for admin approval.";

$lang['fbid_exist'] = 'The Facebook ID you have entered is already registered with another user. Please choose a different account and try again.';
$lang['twitterid_exist'] = 'The Twitter ID you have entered is already registered with another user. Please choose a different account and try again.';
$lang['logs_success'] = 'Logs updated successfully';
$lang['expenses_success'] = 'Expences added successfully';
$lang['payment_method_success'] = 'Payment method changed successfully';

//wallet
$lang['add_expense'] = 'Expense Added';
$lang['reward_earned'] = 'Reward Earned';
$lang['bonus_earned'] = 'Bonus Earned';
$lang['profile_complete'] = 'Profile Complete';
$lang['term_accept_success'] = 'Thank you for successfully accepting the Terms & Conditions of the Goa Deserves Better app.';

$lang['term_alresdy_accept_success'] = 'You have already accepted the Terms & Conditions. Please proceed to the next step.';
$lang['welcome_to_campaign'] = 'Welcome to Goa Deserves Better. To use the app, login with your registered mobile number (RMN).Your registration ID is ';
$lang['welcome_2'] = ' ,please keep it for future references. - DMK';
$lang['to_use_the_app'] = 'to use the app.';
$lang['otp_message'] = ' is your OTP to signup for the Goa Deserves Better app.Valid till 15 minutes.- DMK';
$lang['login_otp_message'] = ' is your OTP to signup for the Goa Deserves Better app.Valid till 15 minutes.- DMK';

//bonus point
$lang['bonus_point_notification'] = "You've just earned 50 bonus points for completing your profile! Keep up the good work.";
$lang['bonus_point_notification_title'] = 'Congratulations';
//reset message
$lang['your_reset_password_link'] = 'Your reset password link is: ';

$lang['whats_app_already_exist'] = 'The whatsapp number you have entered is already registered with another user';
//contact us
$lang['contact_us'] = 'Your request query with id ';
$lang['contact_us_2'] = ' has been sent to Goa Deserves Better team, and a fellow associate will contact you in next 48hrs or you shall be communicated through email post resolution of your  posted issue.';

//referal wallet log
$lang['referal_bonus_earned']= 'Referal bonus earned';

$lang['district_cant_change'] = "You can't change your district";