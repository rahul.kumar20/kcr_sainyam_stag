
from flask import Flask,request,Response,render_template,url_for,jsonify,Session


app = Flask(__name__)
app = Flask(__name__,template_folder='views',static_url_path='/static')
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'xoxb-142435181460'
#app.config['ALLOWED_EXTENSIONS'] = set(['csv'])

sess = Session()

# routing error json
@app.route('/')
def hello_world():
   data = {'error_code':401, 'error_string': 'Authorization fail'}
   return jsonify(data)

import mimetypes

mimetypes.add_type('image/svg+xml', '.svg')

@app.route('/static/<svgFile>.svg')
def serve_content(svgFile):
    return file('static/'+svgFile+'.svg').read()

@app.route('/findcircle', methods=['GET','POST'])
def findcircle():
    #im_haystack = Image.open(r"/var/www/lily/lilyenv/source_code/1_dp.jpeg")
    # import the necessary packages
    # import cv2,sys
    #
    # method = cv2.TM_SQDIFF_NORMED
    #
    # # Read the images from the file
    # # small_image = cv2.imread('/var/www/lily/lilyenv/source_code/1_dp.jpeg')
    # # large_image = cv2.imread('/var/www/lily/lilyenv/source_code/1_or.jpeg')
    #
    # small_image = cv2.imread('/var/www/lily/lilyenv/source_code/parroteye.png')
    # large_image = cv2.imread('/var/www/lily/lilyenv/source_code/parrot.jpeg')
    #
    # result = cv2.matchTemplate(small_image, large_image, method)
    #
    #
    # print (result)
    #
    # # We want the minimum squared difference
    # mn, _, mnLoc, _ = cv2.minMaxLoc(result)
    #
    # # Draw the rectangle:
    # # Extract the coordinates of our best match
    # MPx, MPy = mnLoc
    #
    # # Step 2: Get the size of the template. This is the same size as the match.
    # trows, tcols = small_image.shape[:2]
    #
    # print ('mpx',MPx)
    # print ('mpy' , MPy)
    # print ('trows' , trows)
    # print ('tcols' ,tcols)
    #
    # # Step 3: Draw the rectangle on large_image
    # cv2.rectangle(large_image, (MPx, MPy), (MPx + tcols, MPy + trows), (0, 0, 255), 2)
    #
    # # Display the original image with the rectangle around the match.
    # cv2.imshow('output', large_image)
    #
    # # The image is only displayed if we call this
    # cv2.waitKey(0)
    # sys.exit()
    # import the necessary packages


    import numpy as np
    import argparse
    import cv2

    # construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True, help="Path to the image")
    args = vars(ap.parse_args())

    # load the image, clone it for output, and then convert it to grayscale
    image = cv2.imread(args["image"])
    output = image.copy()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect circles in the image
    circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.2, 100)

    # ensure at least some circles were found
    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")

        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            cv2.circle(output, (x, y), r, (0, 255, 0), 4)
            cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

        # show the output image
        cv2.imshow("output", np.hstack([image, output]))
        cv2.waitKey(0)



    #
    # # !/usr/bin/env python3
    # # -*- coding: utf-8 -*-
    # """
    # Created on Sat Sep 22 18:05:54 2018
    # @author: chaitanya
    # """
    # from skimage import io
    # import cv2
    #
    # # original image
    # img1url = "/var/www/lily/lilyenv/source_code/parrot.jpeg"  # "2_or.jpeg"#"1_or.jpeg"#"https://images.pexels.com/photos/33045/lion-wild-africa-african.jpg?cs=srgb&dl=nature-animal-eyes-33045.jpg&fm=jpg""3_or.jpeg"
    # # screenshot
    # img2url = "/var/www/lily/lilyenv/source_code/screenshot.png"  # "2_dp.jpeg"#img1url#"https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.KoyOjSBKNNsb4rBvCv0kswHaE8%26pid%3D15.1&f=1""3_dp (copy).jpeg"
    #
    # img3url = "/var/www/lily/lilyenv/source_code/2_dp.jpeg"
    #
    # img1 = cv2.cvtColor(io.imread(img1url), cv2.COLOR_RGBA2BGR)
    # img2 = cv2.cvtColor(io.imread(img2url), cv2.COLOR_RGBA2BGR)
    # img3 = cv2.cvtColor(io.imread(img3url), cv2.COLOR_RGBA2BGR)
    #
    # # if img2.shape[0] < img2.shape[1]*1.5:
    # #    print("Image is not a Screenshot")
    # #    exit()
    # # Initiate SIFT detector
    # sift = cv2.xfeatures2d.SIFT_create()
    #
    # # find the keypoints and descriptors with SIFT
    # kp1, des1 = sift.detectAndCompute(img1, None)
    # kp2, des2 = sift.detectAndCompute(img2, None)
    # kp2, des3 = sift.detectAndCompute(img3, None)
    # # BFMatcher with default params
    # bf = cv2.BFMatcher()
    # matches1 = bf.knnMatch(des1, des2, k=2)
    # matches2 = bf.knnMatch(des1, des3, k=2)
    #
    # # ratio test
    # good1 = []
    # for m, n in matches1:
    #     if m.distance < 0.75 * n.distance:
    #         good1.append([m])
    #
    # good2 = []
    # for m, n in matches2:
    #     if m.distance < 0.75 * n.distance:
    #         good2.append([m])
    #
    # # percent of good to matched > 15
    # success1 = (len(good1) * 100) / float(len(des2))  # screenshot matched/total FD
    # print(success1)
    #
    # success2 = (len(good2) * 100) / float(len(des3))  # screenshot matched/total FD
    # print(success2)
    # # if success1 >= 15 and success1 <= 75:
    # #     print("True")
    # # else:
    # #     print("False")
    # return '1'



def find_matches(haystack, needle):
    import numpy as np
    arr_h = np.asarray(haystack)
    arr_n = np.asarray(needle)

    y_h, x_h = arr_h.shape[:2]
    y_n, x_n = arr_n.shape[:2]

    xstop = x_h - x_n + 1
    ystop = y_h - y_n + 1

    matches = []
    for xmin in range(0, xstop):
        for ymin in range(0, ystop):
            xmax = xmin + x_n
            ymax = ymin + y_n

            arr_s = arr_h[ymin:ymax, xmin:xmax]  # Extract subimage
            arr_t = (arr_s == arr_n)  # Create test matrix

            if arr_t.all():  # Only consider exact matches
                matches.append((xmin, ymin))

    return matches


# api for social and normal signup
@app.route('/detectimage', methods=['GET','POST'])
def signup():
    # !/usr/bin/env python3
    # -*- coding: utf-8 -*-
    """
    Created on Sat Sep 22 18:05:54 2018
    @author: chaitanya
    """
    from skimage import io
    import cv2

    # # original image
    # img1url = "https://appinventiv-development.s3.amazonaws.com/ipac/5b6407ce60a37.user_180feca2-ef98-4369-afdc-4d89533ed30f.jpg"
    # # screenshot
    # img2url = "https://appinventiv-development.s3.amazonaws.com/ipac/5b6851bce70d7.69815726highqualitywallpapers_5b6851bccaf741533563324.jpg"

    user_img_url1 = request.form.get('user_img_url1')
    user_img_url2 = request.form.get('user_img_url2')
    admin_img_url1 = request.form.get('admin_img_url1')
    admin_img_url2 = request.form.get('admin_img_url2')

    if user_img_url1 and user_img_url2 and admin_img_url1 and admin_img_url2:

        img1 = cv2.cvtColor(io.imread(user_img_url1), cv2.COLOR_RGBA2BGR)
        img2 = cv2.cvtColor(io.imread(admin_img_url1), cv2.COLOR_RGBA2BGR)

        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # BFMatcher with default params
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1, des2, k=2)

        # ratio test
        good1 = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good1.append([m])

        # percent of good to matched > 15
        success1 = (len(good1) * 100) / float(len(des2))  # screenshot matched/total FD



        ###########################################################
        img1 = cv2.cvtColor(io.imread(user_img_url1), cv2.COLOR_RGBA2BGR)
        img2 = cv2.cvtColor(io.imread(admin_img_url2), cv2.COLOR_RGBA2BGR)

        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # BFMatcher with default params
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1, des2, k=2)

        # ratio test
        good2 = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good2.append([m])

        # percent of good to matched > 15
        success2 = (len(good2) * 100) / float(len(des2))  # screenshot matched/total FD



        ####################################################################



        img1 = cv2.cvtColor(io.imread(user_img_url2), cv2.COLOR_RGBA2BGR)
        img2 = cv2.cvtColor(io.imread(admin_img_url1), cv2.COLOR_RGBA2BGR)

        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # BFMatcher with default params
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1, des2, k=2)

        # ratio test
        good3 = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good3.append([m])

        # percent of good to matched > 15
        success3 = (len(good3) * 100) / float(len(des2))  # screenshot matched/total FD


        #################################################################
        img1 = cv2.cvtColor(io.imread(user_img_url2), cv2.COLOR_RGBA2BGR)
        img2 = cv2.cvtColor(io.imread(admin_img_url2), cv2.COLOR_RGBA2BGR)

        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # BFMatcher with default params
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1, des2, k=2)

        # ratio test
        good4 = []
        for m, n in matches:
            if m.distance < 0.75 * n.distance:
                good4.append([m])

        # percent of good to matched > 15
        success4 = (len(good4) * 100) / float(len(des2))  # screenshot matched/total FD
        count = 0
        if success1 >= 90:
            count += 1
        if success2 >= 90:
            count += 1
        if success3 >= 90:
            count += 1
        if success4 >= 90:
            count += 1

        if count >= 2:
            result = "True"
        else:
            result = "False"

        return jsonify(dict(code=200, string='Response matched succesfully',result = result))
    else:
        return jsonify(dict(code=400, string='Parameter missing', result=[]))
#########################Admin Routing End####################

if __name__ == '__main__':
   app.run(host='0.0.0.0',
           port='8193',
           debug=True)
