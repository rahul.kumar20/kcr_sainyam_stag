navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
var ctxstock = false;
var video;
var mediaStreamTrack;
var cropper;
var canvas;
var ctx;

init();
myInitCode();

/**
 *
 * @returns {undefined}
 */
function startWebcam() {
    if ( navigator.getUserMedia ) {
        navigator.getUserMedia(
                // constraints
                        {
                            video: true,
                            audio: false
                        },
                        // successCallback
                                function ( localMediaStream ) {
                                    video = document.querySelector( 'video' );
                                    video.srcObject = localMediaStream;
                                    mediaStreamTrack = localMediaStream;
                                    $( '#webCemModal' ).show();
                                    $( '#profilePicError' ).text( '' );
                                },
                                // errorCallback
                                        function ( err ) {
                                            alert( 'Web cam not found' );
                                            console.log( "The following error occured: " + err );
                                        }
                                );
                            }
                    else {
                        console.log( "getUserMedia not supported" );
                    }
                }

        /**
         *
         * @returns {undefined}
         */
        function stopWebcam() {
            mediaStreamTrack.getTracks()[0].stop();

        }
        //---------------------
        // TAKE A SNAPSHOT CODE
        //---------------------


        /**
         *
         * @returns {undefined}
         */
        function init() {
            // Get the canvas and obtain a context for
            // drawing in it
            canvas = document.getElementById( "myCanvas" );
            ctx = canvas.getContext( '2d' );
            ctx.clearRect( 0, 0, canvas.width, canvas.height );
            myInitCode()
        }


        /**
         *
         * @returns {undefined}
         */
        function snapshot() {
            canvas = document.getElementById( "myCanvas" );
            console.log( canvas );
            ctx.clearRect( 0, 0, canvas.width, canvas.height );
            ctxstock = true;
            ctx.drawImage( video, 0, 0, canvas.width, canvas.height );

            cropper.replace( canvas.toDataURL() );
            $( '#cropImageModal' ).show();
            $( '#webCemModal' ).hide();
            stopWebcam();
        }

        function hideWebCemModal()
        {
            $( '#webCemModal' ).hide();
            stopWebcam();
        }



        function getRoundedCanvas( sourceCanvas ) {
            var canvas = document.createElement( 'canvas' );
            var context = canvas.getContext( '2d' );
            var width = sourceCanvas.width;
            var height = sourceCanvas.height;

            canvas.width = width;
            canvas.height = height;

            context.imageSmoothingEnabled = true;
            context.drawImage( sourceCanvas, 0, 0, width, height );
            context.globalCompositeOperation = 'destination-in';
            context.beginPath();
            context.arc( width / 2, height / 2, Math.min( width, height ) / 2, 0, 2 * Math.PI, true );
            context.fill();

            return canvas;
        }



        /**
         *
         * @returns {undefined}
         */
        function myInitCode() {
            var image = document.getElementById( 'image' );
            //var button = document.getElementById('button');
            // var result = document.getElementById('result');
            var croppable = false;
            cropper = new Cropper( image, {
                aspectRatio: 1,
                viewMode: 1,
                zoomable: true,
                dragMode: 'move',
                cropBoxMovable: true,
                cropBoxResizable: false,
                minCropBoxWidth: 100,
                minCropBoxHeight: 100,
                ready: function () {
                    croppable = true;
                }
            } );



            /**
             *
             * @returns {undefined}
             */
            document.getElementById( 'avatarInput' ).onchange = function () {
                var input = this;
                var ext = $( '#avatarInput' ).val().split( '.' ).pop().toLowerCase();
                if ( $.inArray( ext, [ 'gif', 'png', 'jpg', 'jpeg' ] ) == -1 ) {
                    $( '#profilePicError' ).text( 'only png,jpg,jpeg ext are allowed' );
                }
                else if ( input.files && input.files[0] ) {
                    var reader = new FileReader();
                    canvas = null;
                    reader.onload = function ( e ) {

                        $( '#cropImageModal' ).show();
                        cropper.replace( e.target.result );
                        $( '#avatarInput' ).val( '' );
                        $( '#profilePicError' ).text( '' );
                    }
                    reader.readAsDataURL( input.files[0] );

                }


            };


            /**
             *
             * @returns {undefined}
             */
            document.getElementById( 'saveCropImage' ).onclick = function () {

                var croppedCanvas;
                var roundedCanvas;
                var roundedImage;
                croppable = true;
                if ( !croppable ) {
                    return;
                }

                // Crop
                croppedCanvas = cropper.getCroppedCanvas();

                // Round
                roundedCanvas = getRoundedCanvas( croppedCanvas );

                var blobimg = roundedCanvas.toDataURL();
                $( '#profile-pic' ).css( 'background-image', 'url(' + blobimg + ')' );
                console.log( roundedCanvas );
                $( '#coverPicInput' ).val( blobimg );
                $( '#cropImageModal' ).hide();

            };


            document.getElementById( 'btnZoomPlus' ).onclick = function () {
                cropper.zoom( 0.1 );
            }

            document.getElementById( 'btnZoomMinus' ).onclick = function () {

                cropper.zoom( -0.1 );
            }

            //});
        }

        /**
         *
         * @returns {undefined}
         */
        function hideCropImgModal()
        {
            $( '#cropImageModal' ).hide();

        }