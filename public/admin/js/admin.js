$(document).ready(function() {
  // validate the comment form when it is submitted
  $("#category_form").validate({
submitHandler: function(form) {
  form.submit();
},
ignore: [],
rules: {
  'category_name': {
                   required: true,
                   maxlength:30
  },
  'sub_category_name': {
                   required: true,
                   maxlength:30
  },
  'description': {
                   required: true,
                   maxlength:500
  },
  'attr_name[]': {
                   required: true,
                   maxlength:30,
                   notEqualToGroup: ['.distinctattr']
  }
}
,
 messages: {
          'category_name': {
                               required: "Please enter category name",
                               maxlength: "Category name must be below 30 character"
                 },
          'sub_category_name': {
                               required: "Please enter sub category name",
                               maxlength: "Sub category name must be below 30 character"
                 },
          'description': {
                               required: "Please enter description",
                               maxlength: "Sub category name must be below 500 character"
                 },
          'attr_name[]': {
                               required: "Attributes is required",
                               maxlength: "Attributes must be below 30 character"
                 }
      }
});

});

$.validator.addMethod("notEqualToGroup", function(value, element, options) {
// get all the elements passed here with the same class
  var elems = $(element).parents('form').find(options[0]);
// the value of the current element
var valueToCompare = value;
// count
var matchesFound = 0;
// loop each element and compare its value with the current value
// and increase the count every time we find one
$.each(elems, function(){
thisVal = $(this).val();
if(thisVal == valueToCompare){
matchesFound++;
}
});
// count should be either 0 or 1 max
if(this.optional(element) || matchesFound <= 1) {
      //elems.removeClass('error');
      return true;
  } else {
      //elems.addClass('error');
  }
}, jQuery.validator.format("Please enter a different attributes."));


// Add shopper validation
$(document).ready(function() {
  // validate the comment form when it is submitted
  $("#add_buddy_form").validate({ 
submitHandler: function(form) {
  form.submit();
},
ignore: [],
rules: {
  'first_name': {
                   required: true,
                   minlength:2,
                   maxlength:30
  },
  'last_name': {
                required: true,
                minlength:2,
                maxlength:30
  },  
  'mobile_no': {
                   required: true,
                   minlength:7,
                   maxlength:15,
                   remote: baseUrl+"/admin/Buddy/check_mobile_no"
  },
  'location': {
                   required: true
                   
  },
  'email': {
                   required: true,
                   email:true,
                   maxlength:40,
                   remote: baseUrl+"/admin/Buddy/check_email"
                   
  },
  'dobDate': {
                   required: true,
                   maxlength:30
                   
  }
}
,
 messages: {
          'first_name': {
                               required: "Please enter first name",
                               minlength: "First name must be above 2 character",
                               maxlength: "Last name must be below 30 character"
                 },
          'last_name': {
                               required: "Please enter last name",
                               minlength: "First name must be above 2 character",
                               maxlength: "Last name must be below 30 character"
           },
          'mobile_no': {
                               required: "Please enter mobile number",
                               minlength: "Mobile no must be min 7 digits",
                               maxlength: "Mobile no must be max 15 digits",
                               remote:"Mobile no is already exists"
                 },
          'location': {
                               required: "Please enter location",
                                maxlength: "Email must be below 30 character"
                 },
          'email': {
                               required: "Email is required",
                               maxlength: "Email must be below 40 character",
                               email: "Please enter valid email",
                               remote:"Email is already exists"
                 },
          'dobDate': {
                               required:  "Please enter  date of birth",
                               maxlength: "Date of birth must be below 30 character"
                 }
      }
});

});


// Add buddy validation

$(document).ready(function() {
  // validate the comment form when it is submitted

  $("#edit_buddy_form").validate({ 

submitHandler: function(form) {
  form.submit();
},
ignore: [],
rules: {
  'first_name': {
                  required: true,
                  minlength:2,
                  maxlength:30
               },
  'last_name': {
                  required: true,
                  minlength:2,
                  maxlength:30
            },  
  'email': {
                   required: true,
                   email:true,
                   maxlength:40,
                   remote: baseUrl+"/admin/Buddy/check_email?userid="+$("#user_id").val()
                   
  },
  'mobile_no': {
                   required: true,
                   minlength:7,
                   maxlength:15,
                   remote: baseUrl+"/admin/Buddy/check_mobile_no?userid="+$("#user_id").val()
                   
  },     
  'dobDate': {
                   required: true,
                   maxlength:30
                   
  }
}
,
 messages: {
        'first_name': {
                      required: "Please enter first name",
                      minlength: "first name must be above 2 character",
                      maxlength: "last name must be below 30 character"
                    },
        'last_name': {
                      required: "Please enter last name",
                      minlength: "first name must be above 2 character",
                      maxlength: "last name must be below 30 character"
                    },
          'mobile_no': {
                               required: "Please enter mobile number",
                               minlength: "Mobile no must be min 7 digits",
                               maxlength: "Mobile no must be max 15 digits",
                               remote:"Mobile no is already exists"
                 },
          'location': {
                               required: "Please enter location"
                             //  maxlength: "location name must be below 50 character"
                 },
          'email': {           required: "Email is required",
                               maxlength: "Email must be below 40 character",
                               email: "Please enter valid email",
                               remote:"Email is already exists"
                               
                 },
          'dobDate': {
                               required: "Please enter  date of birth",
                               maxlength: "Date of birth must be below 30 character"
                 }
      }
});

});

// Add merchant validation

$(document).ready(function() {
  // validate the comment form when it is submitted

  $("#add_merchant_form").validate({ 

submitHandler: function(form) {
  form.submit();
},
ignore: [],
rules: {
  'name': {
                   required: true,
                   minlength:3,
                   maxlength:30
  },
   'email': {
                   required: true,
                   maxlength:40,
                   email:true,
                   remote: baseUrl+"/admin/Merchant/check_email"
                   
  },
  'mobile_no': {
                   required: true,
                   minlength:7,
                   maxlength:15,
                   remote: baseUrl+"/admin/Merchant/check_mobile_no"
                   
  },     
  'dobDate': {
                   required: true,
                   maxlength:30
                   
  }
}
,
 messages: {
          'name': {
                               required: "Please enter merchant name",
                               minlength: "merchant name must be above 3 character",
                               maxlength: "merchant name must be below 30 character"
                 },
          'mobile_no':{
                               required: "Please enter mobile number",
                               minlength: "Mobile no must be min 7 digits",
                               maxlength: "Mobile no must be max 15 digits",
                               remote:"Mobile no is already exists"
                 },
          'email': {           required: "Email is required",
                               maxlength: "Email must be below 40 character",
                               email: "Please enter valid email",
                               remote:"Email is already exists"                               
                 },
          'dobDate':{
                               required: "Please enter  date of birth",
                               maxlength: "Date of birth must be below 30 character"
                  }
      }
});

});

$(document).ready(function() {
  // validate the comment form when it is submitted

  $("#edit_merchant_form").validate({ 

submitHandler: function(form) {
  form.submit();
},
ignore: [],
rules: {
  'name': {
                   required: true,
                   minlength:3,
                   maxlength:30
  },
   'email': {
                   required: true,
                   email:true,
                   maxlength:40,
                   remote: baseUrl+"/admin/Merchant/check_email?userid="+$("#user_id").val()
                   
  },
  'mobile_no': {
                   required: true,
                   minlength:7,
                   maxlength:15,
                   remote: baseUrl+"/admin/Merchant/check_mobile_no?userid="+$("#user_id").val()
                   
  },     
  'dobDate': {
                   required: true,
                   maxlength:30
                   
  }
}
,
 messages: {
          'name': {
                              required: "Please enter merchant name",
                              minlength: "merchant name must be above 3 character",
                              maxlength: "merchant name must be below 30 character"
                  },          
          'mobile_no': {
                              required: "Please enter mobile number",
                              minlength: "Mobile no must be min 7 digits",
                              maxlength: "Mobile no must be max 15 digits",
                              remote:"Mobile no is already exists"
                 },
          'email': {            required: "Email is required",
                               maxlength: "Email must be below 40 character",
                               email: "Please enter valid email",
                               remote:"Email is already exists"                               
                 },          
          'dobDate': {
                               required: "Please enter  date of birth",
                               maxlength: "Date of birth must be below 30 character"
                 }
      }
});

});


$(document).ready(function() {
  // validate the comment form when it is submitted

  $("#add_store_form").validate({
    submitHandler: function(form) {
    form.submit();
  },
ignore: [],
rules: {
  'store_name': {
                  required: true,
                  minlength:3,
                  maxlength:30
  },
  'store_address1': {
                   required: true,                   
                   minlength:3,                  
                   maxlength:100 
  },
  'store_address2': {                   
                    minlength:3,                  
                    maxlength:100                  
  }
}
,
 messages: {
          'store_name': {
                                required: "Please enter store name",
                                minlength: "Store name must be above 3 character",
                                maxlength: "Store name must be below 30 character"
                 },
          'store_address1': {   required: "store address1 is required",
                                minlength: "Store address1 must be above 3 character",
                                maxlength: "store address1 must be below 100 character"
                                                           
                 },
          'store_address2': {
                                minlength: "Store address2 must be above 3 character",  
                                maxlength: "store address2 must be below 100 character"
                               
          }
      }
});

});

$(document).ready(function() {
  // validate the comment form when it is submitted

  $("#edit_store_form").validate({
    submitHandler: function(form) {
    form.submit();
  },
ignore: [],
rules: {
  'store_name': {
                  required: true,
                  minlength:3,
                  maxlength:30
  },
  'store_address1': {
                   required: true, 
                   minlength:3,                  
                   maxlength:100 
  },
  'store_address2': { 
                   minlength:3,                  
                   maxlength:100                  
  }
}
,
 messages: {
          'store_name': {
                                required: "Please enter store name",
                                minlength: "Store name must be above 3 character",
                                maxlength: "Store name must be below 30 character"
                 },
          'store_address1': {   required: "store address1 is required",
                                minlength: "Store address1 must be above 3 character",
                                maxlength: "store address1 must be below 100 character"
                                                           
                 },
          'store_address2': {
                              minlength: "Store address2 must be above 3 character",  
                              maxlength: "store address2 must be below 100 character"
                               
          }
      }
});



$("#add_deal_form").validate({
  submitHandler: function(form) {
  form.submit();
},
ignore: [],
          rules: {
            deal_title: {
                required: true,
                minlength: 3,
                maxlength: 50,
            },
           deal_description: {
                required: true,
                minlength: 15,
                maxlength: 200,
            },
             deal_category: {
                required: true,
            },
          //  deal_subcategory: {
            //    required: true,
           // },
            deal_original_price: {
                required: true,
                minStrict: 0,
            },
            deal_selling_price: {
                required: true,
            },
           deal_startDate: {
                required: true,
            },
            deal_endDate: {
                required: true,
            },
           deal_currency: {
                required: true,
            },
          deal_quantity: {
                required: true,
                number: true,
                minStrict: 0,
            }
          },
          messages: {
            deal_title: {
                required: string.deal_title,
                minlength: string.deal_title_minlength,
                maxlength: string.deal_title_maxlength
            },
            deal_description: {
                required: string.deal_description,
                minlength: string.deal_description_minlength,
                maxlength: string.deal_description_maxlength
            },
           deal_category: {
                required: string.deal_category
            },
            deal_subcategory: {
                required: string.deal_subcategory
            },
            deal_original_price: {
                required: string.deal_original_price,
                minStrict: string.deal_original_price_min
            },
            deal_selling_price: {
                required: string.deal_selling_price
            },
            deal_startDate: {
                required: string.deal_startDate
            },
            deal_endDate: {
                required: string.deal_endDate
            },
            deal_currency: {
                required: string.deal_currency
            },
            deal_quantity: {
                required: string.deal_quantity,
                minStrict: string.deal_quantity_min
            }
        }
});

jQuery.validator.addMethod( 'minStrict', function ( value, el, param ) {
  return value > param;
} );


});
