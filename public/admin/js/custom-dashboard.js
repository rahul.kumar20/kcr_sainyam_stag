$(document).ready(function(){

    // Date Picker
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    // =============== Linked Datepicker =============== //
    var add_start = $('#dpd1').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight:'TRUE',
        autoclose: true,
        onRender: function(date) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < add_end.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            add_end.setValue(newDate);
            add_start.hide();
        }
        add_start.hide();
        $('#dpd2')[0].focus();
    }).data('datepicker');

    var add_end = $('#dpd2').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight:'TRUE',
        autoclose: true,
        onRender: function(date) {
            return date.valueOf() < add_start.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        add_end.hide();
    }).data('datepicker');

});

$(function() {

    // =========== Chart ===========
    Highcharts.chart('chart1', {

        chart: {
            type: 'line',
             backgroundColor: 'transparent'
        },

        title: {
            text: false
        },

        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July']
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: false
            }
        },

        credits: {
            enabled: false
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
            }
        },

        exporting: {
               enabled: false
        },

        series: [{
            // showInLegend: false,
            name: 'Number of Users',
            data: [2000, 8000, 8000, 800, 1000, 3000, 2500],     
            color: '#53c4ce',   
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
    
    // =========== Chart ===========
    Highcharts.chart('chart2', {

        chart: {
            type: 'column',
             backgroundColor: 'transparent'
        },

        title: {
            text: false
        },

        xAxis: {
            categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: false
            }
        },

        credits: {
            enabled: false
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
            }
        },

        exporting: {
               enabled: false
        },

        series: [{
            // showInLegend: false,
            name: 'Location',
            data: [2000, 8000, 8000, 800, 1000, 3000, 2500],     
            color: '#53c4ce',   
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });

    Highcharts.chart('chart3', {

        chart: {
            type: 'line',
             backgroundColor: 'transparent'
        },

        title: {
            text: false
        },

        xAxis: {
            categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: false
            }
        },

        credits: {
            enabled: false
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
            }
        },

        exporting: {
               enabled: false
        },

        series: [{
            // showInLegend: false,
            name: 'City',
            data: [2000, 8000, 8000, 800, 1000, 3000, 2500],     
            color: '#53c4ce',   
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });


Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Monthly Average Rainfall'
    },
    subtitle: {
        text: 'Source: WorldClimate.com'
    },
    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Tokyo',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }, {
        name: 'New York',
        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    }, {
        name: 'London',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

    }, {
        name: 'Berlin',
        data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

    }]
});

});