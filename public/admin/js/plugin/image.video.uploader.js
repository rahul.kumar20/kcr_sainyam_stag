var count = 0;
    var getFirstImg = null;
    var names = [];
    $('body').on('change','input[name="thumbnail[]"]',function(event){
        var ele = $(this);
        var files = event.target.files;
        var uploader = '<li><div class="album-thumnail"><i class="fa fa-plus"></i><input type="file" name="thumbnail[]" data-container="#album" class="album-uploader" multiple="" id="album-add"></div></li>';
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
                if (file.type.match('image')) {
                    //check file size 1 mb maximum
                    if(file.size>=1*1024*1024) {
                        $('#media-err').show().text(string.img_size_err);
                        $('#media-err').fadeOut(6000)
                    return false;;
                    }
                    //5 max image or video allow
                    if(count ==5){
                        $('#media-err').show().text(string.five_imge_max_err);
                        $('#media-err').fadeOut(6000)
                        return false;;
                    }
                    count++;
                    var picReader = new FileReader();
                    picReader.fileName = file.name
                    picReader.addEventListener("load", function (event) {
                    var picFile = event.target;
                    $(ele).after('<div class="albub-item"><img src="'+picFile.result+'"></div><span class="remove-item"><i class="fa fa-times"></i></span>');
                    $('#album').append(uploader);
                });
             } else if(file.type.match('video')){
                 //file size check
                 if(file.size>=2*1024*1024) {
                     $('#media-err').show().text(string.video_size_err);
                     $('#media-err').fadeOut(6000)
                    return false;;
                    }
                    
                    //5 max image or video allow
                    if(count ==5){
                        $('#media-err').show().text(string.five_imge_max_err);
                        $('#media-err').fadeOut(6000)
                        return false;;
                    }
                    count++;
                var picReader = new FileReader();
                    picReader.fileName = file.name
                    picReader.addEventListener("load", function (event) {
                    var picFile = event.target;
                    $(ele).after('<div class="albub-item"><video src="'+picFile.result+'"></video></div><span class="remove-item"><i class="fa fa-times"></i></span><span class="player"><i class="fa fa-play-circle"></i></span>');
                    $('#album').append(uploader);
                });
             }
            picReader.readAsDataURL(file);
        }
    });

    $('body').on('click','.remove-item',function(){
        count--;
        $(this).parents('li').remove();
    });