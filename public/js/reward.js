$( document ).ready( function () {

    var nowTemp = new Date();
    var now = new Date( nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0 );

    var checkin = $( '#startDate' ).datepicker( {
        onRender: function ( date ) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    } ).on( 'changeDate', function ( ev ) {
        $( '#endDate' ).val( '' );
        if ( ev.date.valueOf() < checkout.date.valueOf() ) {
            var newDate = new Date( ev.date )
            newDate.setDate( newDate.getDate() );
            checkout.setValue( newDate );
        }
        checkin.hide();
        $( '#endDate' )[0].focus();
    } ).data( 'datepicker' );
    var checkout = $( '#endDate' ).datepicker( {
        onRender: function ( date ) {
            return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    } ).on( 'changeDate', function ( ev ) {
        checkout.hide();
    } ).data( 'datepicker' );


    //on datepicker 2 focus
    $( '#datepicker_2' ).focus( function () {
        if ( $( '#datepicker_1' ).val() == '' ) {
            checkout.hide();
        }
    } );
    //prevent typing datepicker's input
    $( '#datepicker_2, #datepicker_1' ).keydown( function ( e ) {
        e.preventDefault();
        return false;
    } );

} );


 /*
     * Apply Filter User
     */
    $( '.applyFilterReward' ).click( function () {

        var filter = { };
        filter = $( '#filterVal' ).val();
        var pageUrl = $( '#pageUrl' ).val();

        filter = JSON.parse( filter );

        delete filter['status'];
        delete filter['startDate'];
        delete filter['endDate'];

        delete filter['taskType'];


        var status = $( '.status' ).find( ":selected" ).val();
      
        var taskType = $( '.taskType' ).find( ":selected" ).val();

        var startDate = $( '.startDate' ).val();
        var endDate = $( '.endDate' ).val();
        //status
        if ( status != undefined && status.length != 0 ) {
            filter['status'] = status;
        }
        
        //start date
        if ( startDate != undefined && startDate.length != 0 ) {
            filter['startDate'] = startDate;
        }

        //end date
        if ( endDate != undefined && endDate.length != 0 ) {
            filter['endDate'] = endDate;
        }
        
        //task type
        if ( taskType != undefined && taskType.length != 0 ) {
            filter['taskType'] = taskType;
        }
        if ( !status.length && !startDate.length && !endDate.length &&  !taskType.length) {
            console.log( "Not any filter selected" );
            return;
        }
        var filterLen = $.keyCount( filter );
        if ( filterLen == 0 ) {
            alert( 'Please select a filter' );
            return false;
        }

        //var queryParams = $.param(filter)
        //window.location.href = pageUrl + '?' + queryParams;

        var queryParams = JSON.stringify( filter );
        window.location.href = pageUrl + '?data=' + window.btoa( queryParams );

    } );

      /*
     * Apply Filter User
     */
    $( '.applyFilterRewardHistory' ).click( function () {

        var filter = { };
        filter = $( '#filterVal' ).val();
        var pageUrl = $( '#pageUrl' ).val();

        filter = JSON.parse( filter );

        delete filter['status'];
        delete filter['startDate'];
        delete filter['endDate'];
        delete filter['uid'];
        delete filter['state'];
        delete filter['distict'];
        delete filter['gender'];
        delete filter['taskCompleted'];
        delete filter['college'];
        delete filter['paymentMethod'];

        var status = $( '.status' ).find( ":selected" ).val();
        var uid = $( '.uid' ).find( ":selected" ).val();
        var state = $( '.state' ).find( ":selected" ).val();
        var distict = $( '.distict' ).find( ":selected" ).val();
        var gender = $( '.gender' ).find( ":selected" ).val();
        var college = $( '.college' ).find( ":selected" ).val();

        var startDate = $( '.startDate' ).val();
        var endDate = $( '.endDate' ).val();
        var taskCompleted = $( '.taskCompleted' ).find( ":selected" ).val();
        var paymentMethod = $( '.paymentMethod' ).find( ":selected" ).val();

        //status
        if ( uid != undefined && status.length != 0 ) {
            filter['status'] = status;
        }
        //UID
        if ( uid != undefined && uid.length != 0 ) {
            filter['uid'] = uid;
        }
        //state
        if ( state != undefined && state.length != 0 ) {
            filter['state'] = state;
        }
        //district
        if ( distict != undefined && distict.length != 0 ) {
            filter['distict'] = distict;
        }
        //gender
        if ( gender != undefined && gender.length != 0 ) {
            filter['gender'] = gender;
        }
        //college
        if ( college != undefined && college.length != 0 ) {
            filter['college'] = college;
        }
        //start date
        if ( startDate != undefined && startDate.length != 0 ) {
            filter['startDate'] = startDate;
        }

        //end date
        if ( endDate != undefined && endDate.length != 0 ) {
            filter['endDate'] = endDate;
        }
        //task complete
        if ( taskCompleted != undefined && taskCompleted.length != 0 ) {
            filter['taskCompleted'] = taskCompleted;
        }
        //task complete
        if ( paymentMethod != undefined && paymentMethod.length != 0 ) {
            filter['paymentMethod'] = paymentMethod;
        }
        if (!status.length && !uid.length && !distict.length && !gender.length && !startDate.length && !endDate.length && !taskCompleted.length /*&& !college.length*/ ) {
            console.log( "Not any filter selected" );
            return;
        }
        var filterLen = $.keyCount( filter );
        if ( filterLen == 0 ) {
            alert( 'Please select a filter' );
            return false;
        }

        //var queryParams = $.param(filter)
        //window.location.href = pageUrl + '?' + queryParams;

        var queryParams = JSON.stringify( filter );
        window.location.href = pageUrl + '?data=' + window.btoa( queryParams );

    } );
	
	/*
     * Apply Filter User
     */
    $( '.applyFilterPH' ).click( function () {

        var filter = { };
        filter = $( '#filterVal' ).val();
        var pageUrl = $( '#pageUrl' ).val();

        filter = JSON.parse( filter );

        delete filter['startDate'];
        delete filter['endDate'];

        delete filter['taskType'];


      
        var taskType = $( '.taskType' ).find( ":selected" ).val();

        var startDate = $( '.startDate' ).val();
        var endDate = $( '.endDate' ).val();
      
        
        //start date
        if ( startDate != undefined && startDate.length != 0 ) {
            filter['startDate'] = startDate;
        }

        //end date
        if ( endDate != undefined && endDate.length != 0 ) {
            filter['endDate'] = endDate;
        }
        
        //task type
        if ( taskType != undefined && taskType.length != 0 ) {
            filter['taskType'] = taskType;
        }
        if ( !startDate.length && !endDate.length &&  !taskType.length) {
            console.log( "Not any filter selected" );
            return;
        }
        var filterLen = $.keyCount( filter );
        if ( filterLen == 0 ) {
            alert( 'Please select a filter' );
            return false;
        }

        //var queryParams = $.param(filter)
        //window.location.href = pageUrl + '?' + queryParams;

        var queryParams = JSON.stringify( filter );
        window.location.href = pageUrl + '?data=' + window.btoa( queryParams );

    } );
