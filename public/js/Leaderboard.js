/*
     * Apply Filter User
     */
    $( '.applyFilterLeaderboard' ).click( function () {
        var filter = { };
        filter = $( '#filterVal' ).val();
        var pageUrl = $( '#pageUrl' ).val();

        filter = JSON.parse( filter );

        delete filter['status'];
        delete filter['startDate'];
        delete filter['endDate'];
        delete filter['uid'];
        delete filter['state'];
        delete filter['distict'];
        delete filter['gender'];
        delete filter['taskCompleted'];
        delete filter['college'];

        var status = $( '.status' ).find( ":selected" ).val();
        var uid = $( '.uid' ).find( ":selected" ).val();
        var state = $( '.state' ).find( ":selected" ).val();
        var distict = $( '.distict' ).find( ":selected" ).val();
        var gender = $( '.gender' ).find( ":selected" ).val();
        var college = $( '.college' ).find( ":selected" ).val();
        var taskPlatform = $( '.taskType' ).find( ":selected" ).val();

        var startDate = $( '.startDate' ).val();
        var endDate = $( '.endDate' ).val();
        var taskCompleted = $( '.taskCompleted' ).find( ":selected" ).val();
        //status
        if ( uid != undefined && status.length != 0 ) {
            filter['status'] = status;
        }
        //UID
        if ( uid != undefined && uid.length != 0 ) {
            filter['uid'] = uid;
        }
        //state
        if ( state != undefined && state.length != 0 ) {
            filter['state'] = state;
        }
        //district
        if ( distict != undefined && distict.length != 0 ) {
            filter['distict'] = distict;
        }
        //gender
        if ( gender != undefined && gender.length != 0 ) {
            filter['gender'] = gender;
        }
        //college
        if ( college != undefined && college.length != 0 ) {
            filter['college'] = college;
        }
        //start date
        if ( startDate != undefined && startDate.length != 0 ) {
            filter['startDate'] = startDate;
        }

        //end date
        if ( endDate != undefined && endDate.length != 0 ) {
            filter['endDate'] = endDate;
        }
        //task complete
        if ( taskCompleted != undefined && taskCompleted.length != 0 ) {
            filter['taskCompleted'] = taskCompleted;
        }
        //task complete
        if ( taskPlatform != undefined && taskPlatform.length != 0 ) {
            filter['taskPlatform'] = taskPlatform;
        }
        if ( !status.length && !uid.length && !state.length && !distict.length && !gender.length && !startDate.length && !endDate.length && !taskPlatform.length && !college.length ) {
            console.log( "Not any filter selected" );
            return;
        }
        var filterLen = $.keyCount( filter );
        if ( filterLen == 0 ) {
            alert( 'Please select a filter' );
            return false;
        }

        //var queryParams = $.param(filter)
        //window.location.href = pageUrl + '?' + queryParams;

        var queryParams = JSON.stringify( filter );
        window.location.href = pageUrl + '?data=' + window.btoa( queryParams );

    } );