var protocol = window.location.protocol;
var domain = window.location.hostname;
var URL = protocol + "//" + domain;
//-----------------------------------------------------------------------
/**
 *@Description Here is the methods starts for the form validations in admin using jquery validator.
 *
 */



$( document ).ready( function () {

    $( '.alert-success' ).fadeOut( 5000 );
    /**
     * @name validate add app version form
     * @description This method is used to validate add app version form.
     *
     */
    $( "#login_admin_form" ).validate( {
        errorClass: "alert-danger",
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            },
        },
        messages: {
            email: {
                required: string.loginemail,
            },
            password: {
                required: string.loginpassword,
            },
        },
        submitHandler: function ( form ) {
            form.submit();
            $('#load').attr('disabled', true);
        }
    } );
    /**
     * New Admin Login Form JS to validate
     */
    var error_msg = "";
    $( '.loading-btn' ).on( 'click', function () {
        error_msg = "";
    } );
    $( "#admin_login_form" ).validate( {
        invalidHandler: function ( form, validator ) {
            var errors = validator.numberOfInvalids();
            var id = validator.currentElements[0].id;
            var pass_val = $( '#passwd' ).val();

            $( "#" + id ).parents( '.input-form' ).addClass( 'has-error' );
            
            $( '.common-msg-server' ).removeClass( 'show' );

            if ( errors ) {
                error_msg = validator.errorList[0].message;
                $( '#error_msgs' ).text( error_msg );
                
                if ( pass_val.trim() == '' || pass_val.trim() == null ) {

                    $( '#passwd' ).parent().addClass( 'has-error' );
                }

                if ( error_msg != null && error_msg != '' && error_msg.length ) {

                    $( '.common-msg' ).addClass( 'show' );
                    error_msg = "";
                    setTimeout( function () {
                        $( '.common-msg' ).removeClass( 'show' );
                    }, 10000 );
                }
            }
        },
        rules: {

            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            }

        },
        messages: {

            email: {
                required: string.loginemail,
            },
            password: {
                required: string.loginpassword,
            }

        },
        errorPlacement: function ( error, validator ) {


        },
        //on success,remove error class from form fields
        success: function ( label, element ) {

            $( "#email_id" ).parent( '.input-form' ).removeClass( 'has-error' );
            var pass_val = $( '#passwd' ).val();
            if ( pass_val.trim() != '' && pass_val.trim() != null ) {
                $( "#passwd" ).parent( '.input-form' ).removeClass( 'has-error' );
            }

        },
        submitHandler: function ( form ) {
            form.submit();
        }
    } );
    /* Admin login form validation JS END */


    /**
     * @name validate add app version form
     * @description This method is used to validate add app version form.
     *
     */
    $( "#forget_pwd_admin_form" ).validate( {
        errorClass: "alert-danger",
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            email: {
                required: string.loginemail,
            },
        },
        submitHandler: function ( form ) {
            form.submit();
        }
    } );
    $( 'input' ).keypress( function ( e ) {
        var inp = $.trim( $( this ).val() ).length;
        if ( inp == 0 && e.which === 32 ) {
            return false;
        }
    } );
    // Restrict copy - paste on web login.
    $( '#web_login_pass' ).bind( "cut copy paste", function ( e ) {
        e.preventDefault();
    } );
} );
function validatepassword() {
    var password = $.trim( $( '#password' ).val() );
    var cnfpassword = $.trim( $( '#cnfpassword' ).val() );
    var flag = 0;
    if ( password.length == 0 || password.length < 8 ) {
        $( '.passwordErr' ).css( { opacity: 1 } );
        $( '.passwordErr' ).text( string.passwordErr );
    }
    else {
        $( '.passwordErr' ).css( { opacity: 0 } );
        $( '.passwordErr' ).text( '' );
        flag++;
    }
    if ( cnfpassword.length == 0 || (password != cnfpassword) ) {
        $( '.cnfpasswordErr' ).text( string.cnfPassowrdErr );
        $( '.cnfpasswordErr' ).css( { opacity: 1 } );
    }
    else {
        $( '.cnfpasswordErr' ).css( { opacity: 0 } );
        $( '.cnfpasswordErr' ).text( '' );
        flag++;
    }
    if ( flag == 2 ) {
        return true;
    }
    else {
        return false;
    }

}

function CheckforNum( e ) {
//console.log(String.fromCharCode(e.keyCode));
// Allow: backspace, delete, tab, escape, enter and  +
    if ( $.inArray( e.keyCode, [ 46, 8, 9, 27, 13 ] ) !== -1 || (e.which === 187) || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 86 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39) ) {
// let it happen, don't do anything
        return;
    }
// Ensure that it is a number and stop the keypress
    if ( (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ) {
        e.preventDefault();
    }
}


/* Admin login form validation JS END */

/**
 * @description  New Admin Forget passowrd form validation JS
 */

$( "#admin_forget_pwd_form" ).validate( {
    rules: {
        email: {
            required: true,
            email: true
        },
    },
    messages: {
        email: {
            required: string.loginemail,
        },
    },
    errorPlacement: function ( error, element ) {

        element.parent( 'div' ).addClass( 'has-error' );
        $( '#error_msgs' ).text( '' );
        var error_msg = error[0]['innerText'];
        if ( error_msg != null && error_msg != '' && error_msg.length > 0 ) {
            $( '#error_msgs' ).text( error_msg );
            $( '.common-msg' ).addClass( 'show' );
        }

        setTimeout( function () {
            $( '.common-msg-server' ).removeClass( 'show' );
            $( '.common-msg' ).removeClass( 'show' );
        }, 5000 );
    },
    success: function ( error, element ) {
        $( '#error_msgs' ).text( "" );
    },
    submitHandler: function ( form ) {
        check_validation();
        //form.submit();
    }
} );
/**
 * @function to check user existance and send him/her reset password link
 * @returns {undefined}
 */
var check_validation = function () {
    var user_id = $( "#email" ).val();
    var data = {
        email: user_id,
        csrf_token: csrf_token
    };
    $.ajax( {
        url: URL + "/admin/Ajax_Util/forgot_password",
        data: data,
        type: "POST",
        cache: false,
        beforesend: function ( before_send ) {
            $('#load').attr('disabled', true);
            $( "#load" ).button( 'loading' );
        },
        success: function ( response ) {
            var obj = JSON.parse( response );
            $('#load').attr('disabled', false);
            if ( 200 == obj.code ) {
                $( ".sendsuccess" ).css( "display", "block" );
                $( '.showemailsuccess-btn' ).parents( '.form-wrapper' ).addClass( 'showemailsuccess' ).removeClass( 'showforgot' );
                $( '.form-wrapper.sendsuccess' ).addClass( 'showemailsuccess' );
            }  else if ( 201 == obj.code ) {

                $( '#error_msgs' ).text( obj.msg );
                $( '.common-msg' ).addClass( 'show' );
                setTimeout( function () {
                    $( '.common-msg-server' ).removeClass( 'show' );
                    $( '.common-msg' ).removeClass( 'show' );
                }, 5000 );
            }
        }
    } );
}
