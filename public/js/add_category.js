  /**
     * @name: add cms content form
     * @description: Thie function is used to validate admin add content form in cms.
     */
    $( "#category_add" ).validate( {
        ignore: [ ],
        debug: false,
        errorClass: "alert-danger",
        rules: {
            title: {
                required: true,
            },
            title_tn: {
                required: true,
            },
            status: {
                required: true,
            }
        },
       
        submitHandler: function ( form ) {
            form.submit();
        }
    } );