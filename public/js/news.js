// add news
$.validator.addMethod(
    "ckrequired",
    function(value, element, params) {
        var idname = jQuery(element).attr("id");
        var messageLength = jQuery.trim(CKEDITOR.instances[idname].getData());
        return !params || messageLength.length !== 0;
    },
    "Image field is required"
);
$("#news_add_form").validate({
    errorClass: "alert-danger",
    ignore: [],
    debug: false,
    rules: {
        title: {
            required: true,
        },
        section: {
            required: true,
        },
        desc: {
            required: function() {
                CKEDITOR.instances.desc.updateElement();
            },

            minlength: 10,
        },
        url: {
            required: false,
            url: true,
        },
        onlineform: {
            required: {
                depends: function() {
                    if ($("#newsCategory").val() == "6") {
                        return true;
                    }
                },
            },
        },
    },
    messages: {
        title: {
            required: string.title_req,
        },
        desc: {
            required: string.desc_req,
        },
    },
    url: {
        url: string.task_url_req,
    },
    /* use below section if required to place the error*/
    errorPlacement: function(error, element) {
        if (element.attr("name") == "desc") {
            error.insertAfter("#cke_desc");
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function(form) {
        if (
            $("#is_event").find(":selected").val() == "yes"
        ) {
            //console.log('hi');
            var latitude = $("#user_lat").val();
            if (latitude != undefined && latitude.length == "") {
                $("#location").focus();
                $(".map_error")
                    .show()
                    .html("Please allow your location from browser")
                    .delay(5000)
                    .fadeOut();
                return false;
            }
        }
        form.submit();
        $("body").addClass("loader-wrap");
        $(".loader-img").show();
        $("#add_news").attr("disabled", true);

    },
});


$("#event_add_form").validate({
    errorClass: "alert-danger",
    ignore: [],
    debug: false,
    /* use below section if required to place the error*/
    // errorPlacement: function (error, element) {
    //     if (element.attr("name") == "desc") {
    //         error.insertAfter("#cke_desc");
    //     } else {
    //         error.insertAfter(element);
    //     }
    // },
    submitHandler: function(form) {
        form.submit();
        $("body").addClass("loader-wrap");
        $(".loader-img").show();
        $("#add_event").attr("disabled", true);
    },
});

function deleteUploadedImg(id) {
    var deleteImg = $("#" + id).attr("data-id");
    var deletedImageIds = $("#deleteImgId").val();
    if (deletedImageIds == "") {
        deletedImageIds = deleteImg;
    } else {
        deletedImageIds = deletedImageIds + "," + deleteImg;
    }

    $("#deleteImgId").val(deletedImageIds);
    console.log(deletedImageIds);
    $("#" + id).remove();
    var count = $("#imageCnt").val();
    if (count > 0) {
        count = count - 1;
        $("#imageCnt").val(count);
    }
}

//-----add custom validation for name------------------------------

$.validator.addMethod(
    "lettersonlys",
    function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    },
    "Letters only please"
);

//-------------------------------add user validation--------------------
$("#user_add_form").validate({
    errorClass: "alert-danger",
    rules: {
        first_name: {
            required: true,
            lettersonlys: true,
        },
        email: {
            // required: true,
            email: true,
            remote: baseUrl + "req/check-email-exists",
        },
        password: {
            required: true,
            minlength: 6,
        },
        fbId: {
            required: false,
        },
        twitterId: {
            required: false,
        },
        dob: {
            required: true,
        },
        phoneno: {
            required: true,
            minlength: 10,
            maxlength: 10,
            digits: true,
            remote: baseUrl + "req/check-mobile-exists",
        },
        referalcode: {
            minlength: 8,
            maxlength: 8,
            remote: baseUrl + "admin/user/checkReferralCode",
        },
        whatsupno: {
            required: true,
            minlength: 10,
            maxlength: 10,
            digits: true,
            remote: baseUrl + "req/check-whatsup-exists",
        },
        paytmno: {
            required: false,
            minlength: 10,
            maxlength: 10,
            digits: true,
        },
        state: {
            required: true,
        },
        distict: {
            required: true,
        },
        college_name: {
            required: {
                depends: function() {
                    if ($("#college_option").val() == "yes") {
                        return true;
                    }
                },
            },
        },
        collegeManName: {
            required: {
                depends: function() {
                    if ($("#collegeDa").val() == "others") {
                        return true;
                    }
                },
            },
        },
        gender: {
            required: true,
        },
    },
    messages: {
        required: string.requiredErr,
        first_name: {
            required: string.name_req,
        },
        email: {
            // required: string.email_req,
            email: string.emailErr,
            remote: "Email is already registered",
        },
        password: {
            required: string.pass_req,
            minlength: string.pass_req,
        },
        dob: {
            required: string.dob_req,
        },
        phoneno: {
            required: string.phone_req,
            minlength: string.phone_req_min,
            maxlength: string.phone_req_max,
            remote: "Phone Number is already registered",
        },
        referalcode: {
            remote: string.invalid_referal_code,
        },
        whatsupno: {
            required: string.wts_phone_req,
            minlength: string.wts_phone_req_min,
            maxlength: string.wts_phone_req_max,
            remote: "Whatsapp number already exist",
        },
        paytmno: {
            required: string.pay_phone_req,
            minlength: string.pay_phone_req_min,
            maxlength: string.pay_phone_req_max,
        },
        state: {
            required: string.state_req,
        },
        distict: {
            required: string.dist_req,
        },
        college_name: {
            required: string.college_req,
        },
        gender: {
            required: string.gender_req,
        },
    },
    submitHandler: function(form) {
        form.submit();
        $("body").addClass("loader-wrap");
        $(".loader-img").show();
        $("#add_user").attr("disabled", true);
    },
});

/**
 * @function numeralsOnly
 *  allow only numeber enter
 *  in input box
 *
 * @param {type} evt
 * @returns {Boolean}
 */
function numeralsOnly(evt) {
    evt = evt ? evt : event;
    var charCode = evt.charCode ?
        evt.charCode :
        evt.keyCode ?
        evt.keyCode :
        evt.which ?
        evt.which :
        0;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

/**
 *
 *@function lettersOnly
 *allow only letters
 *
 * @param {type} evt
 * @returns {Boolean}
 */
function lettersOnly(evt) {
    evt = evt ? evt : event;
    var charCode = evt.charCode ?
        evt.charCode :
        evt.keyCode ?
        evt.keyCode :
        evt.which ?
        evt.which :
        0;
    if (
        charCode > 31 &&
        (charCode < 65 || charCode > 90) &&
        (charCode < 97 || charCode > 122)
    ) {
        alert("Enter letters only.");
        return false;
    }
    return true;
}
//manage category of news
function manageNewsCategory() {
    $(".notification").show();
    $(".gallary").show();
    $(".banner").show();
    $(".camera").show();

    type = $("#newsCategory").val();
    if (type == 4) {
        $("#upload_show,#onlineHomeForm").hide();
        $(".notification").hide();
        $(".trash-ico").click();
        $("#imageCnt").val(0);
        $(".bannerHeight").hide();
        $("#url").val("");
    } else if (type == 1) {
        $("#upload_show,#onlineHomeForm").hide();
        $(".bannerHeight").hide();
        $("#url").val("");
        $(".gallary").hide();
    } else if (type == 5) {
        $(".banner,#onlineHomeForm").hide();
        $("#imageCnt").val(0);
        $(".trash-ico").click();
        $(".bannerHeight").show();
        $("#url").val("");
    } else if (type == 2) {
        $("#upload_show,#onlineHomeForm").hide();
        $("#imageCnt").val(0);
        $(".trash-ico").click();
        $(".bannerHeight").hide();
        $("#url").val("");
    } else if (type == 6) {
        $("#onlineHomeForm").show();
        $("#upload_show").hide();
        $(".notification").hide();
        $(".trash-ico").click();
        $("#imageCnt").val(0);
        $(".bannerHeight").hide();
    } else {
        $(".bannerHeight,#onlineHomeForm").hide();
        $("#url").val("");
    }
}

function manageEditNewsCategory() {
    $(".notification").show();
    $(".gallary").show();
    $(".banner").show();
    $(".camera").show();

    type = $("#newsCategory").val();
    if (type == 4) {
        $("#upload_show,#onlineHomeForm").hide();
        $(".bannerHeight").hide();

        $(".notification").hide();
        $(".trash-ico").click();
        $("#imageCnt").val(0);
    } else if (type == 1) {
        $(".gallary,#onlineHomeForm").hide();
        $("#upload_show").hide();
        $(".bannerHeight").hide();
    } else if (type == 5) {
        $(".banner,#onlineHomeForm").hide();
        $(".bannerHeight").show();
    } else if (type == 2) {
        $("#imageCnt,#onlineHomeForm").val(0);
        $(".trash-ico").click();
        $("#upload_show").hide();
        $(".bannerHeight").hide();
    } else if (type == 6) {
        $("#onlineHomeForm").show();
        $("#upload_show").hide();
        $(".notification").hide();
        $(".trash-ico").click();
        $("#imageCnt").val(0);
        $(".bannerHeight").hide();
    } else {
        $(".bannerHeight,#onlineHomeForm").hide();
    }
}

function csvUploadNewsUser() {
    var file_data = $("#upload-doc").prop("files")[0];
    var form_data = new FormData();
    var csrf = csrf_token;

    form_data.append("file", file_data);
    form_data.append("csrf_token", csrf);
    $.ajax({
        url: baseUrl + "req-uidimport", // point to server-side PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: "post",
        success: function(res) {
            res = JSON.parse(res);
            csrf_token = res.csrf_token;
            if (res.code === 200) {
                $("#mymodal-csv").modal("hide");
                $("#uid").val(res.result);
                $("body").removeClass("loader-wrap");
                $(".loader-img").hide();
            }
        },
    });
}

$(".csvupload").click(function() {
    $("#file_error").text("");
    var cnt = 0;
    var allowed_image_type = ["text/csv"];
    var landscape_image_data = $("#upload-doc")[0].files;
    if (
        landscape_image_data == 0 ||
        typeof landscape_image_data == "undefined" ||
        landscape_image_data.length == 0
    ) {
        $("#file_error").text("Please select csv file");
        $("#file_error").css("color", "red");
        cnt++;
    }
    if (
        landscape_image_data.length > 0 &&
        typeof landscape_image_data != "undefined" &&
        allowed_image_type.indexOf(landscape_image_data[0].type) == -1
    ) {
        $("#file_error").text("Please select csv file only");
        $("#file_error").show();
        cnt++;
    }
    if (cnt > 0) {
        return false;
    } else {
        modelHide();
        csvUploadNewsUser();
        return true;
    }
});

function showPriorityDate(priorityValue) {
    var priorityHomeValue = priorityValue;
    if (priorityHomeValue != undefined && priorityHomeValue == 1) {
        $(".priorityDate").show();
    } else {
        $(".priorityDate").hide();
        $("#priotityEndDate").val("");
    }
}

$(".priotityEndDate").keypress(function(event) {
    return (event.keyCode || event.which) === 9 ? true : false;
});

function getFormURL2(formId, url) {
    // var formId = this.value;
    if (formId != undefined) {
        var form_id = formId;
        var csrf = csrf_token;
        var data = {
            id: form_id,
            csrf_token: csrf,
        };
        $.ajax({
            method: "POST",
            url: baseUrl + url,
            data: data,
            beforeSend: function() {},
            success: function(res) {
                res = JSON.parse(res);
                csrf_token = res.csrf_token;
                if (res.code == 200) {
                    $("#url").val(res.url);
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
            },
        });
    }
}