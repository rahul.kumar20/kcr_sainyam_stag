$( document ).ready( function () {

    var nowTemp = new Date();
    var now = new Date( nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0 );

    var checkin = $( '#from_date' ).datepicker( {
        onRender: function ( date ) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    } ).on( 'changeDate', function ( ev ) {
        if ( ev.date.valueOf() < checkout.date.valueOf() ) {
            var newDate = new Date( ev.date )
            newDate.setDate( newDate.getDate() );
            checkout.setValue( newDate );
        }
        checkin.hide();
        $( '#to_date' )[0].focus();
    } ).data( 'datepicker' );
    var checkout = $( '#to_date' ).datepicker( {
        onRender: function ( date ) {
            return date.valueOf() < checkin.date.valueOf() || date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    } ).on( 'changeDate', function ( ev ) {
        checkout.hide();
    } ).data( 'datepicker' );


    //on datepicker 2 focus
    $( '#to_date' ).focus( function () {
        if ( $( '#from_date' ).val() == '' ) {
            checkout.hide();
        }
    } );
    //prevent typing datepicker's input
    $( '#to_date, #from_date' ).keydown( function ( e ) {
        e.preventDefault();
        return false;
    } );

} );