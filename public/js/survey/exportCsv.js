function importCsv(counter,obj){
    
    var qusType =  $("#questionType"+counter).val();
    var isRelation   = $("#relation"+counter).val();
    
    if(qusType == undefined  || qusType ==''){
        alert("Please select the qustion type form list.");
        return false;
    }
    
    $("body").addClass("loader-wrap");
    $('.loader-img').show();
    $('#add_task').attr('disabled', true);
    
    var fd = new FormData();
    
    fd.append('file', obj.files[0]);
    
    if(counter==0 || isRelation == undefined || isRelation == "" || isRelation==2){
        
        uploadNormalCsv(fd,counter,qusType);
    }
    else if(counter > 0 && isRelation != undefined  && isRelation != "" && isRelation !=2){
        
        uploadDependentCsv(fd,counter,qusType);
        
    }
   this.value = '';
    
}


//upload for first qus csv file.

function uploadDependentCsv(fd,counter,qusType){
    
    
    $.ajax({  
                     url:baseUrl + 'admin/form_task/csvDependentUpload',  
                     method:"POST",  
                     data:fd,  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){ 
                         $("body").removeClass("loader-wrap");
                         $('.loader-img').hide();
                         data = JSON.parse(data);
                         
                          if(data.code==400)  
                          {  
                              $( '.alertText' ).text(data.msg );
                              $( '.alertType' ).text( string.error );
                              $( '.alert-danger' ).fadeIn().fadeOut( 5000 );
                              
                          }else if(data.code==200){
                              
                              getHtmlByList("innerQusType"+counter,qusType,counter)
                              
                              $( '.alertText' ).text( data.msg );
                              $( '.alertType' ).text( string.success );
                              $( '.alert-success' ).fadeIn().fadeOut( 5000 );   
                             

                              $("#optionCode"+counter).val(data.timestamp)  
                               
                                  
                                
                          }  
                          
                          
                     }  
        })
}
//upload for first qus csv file.

function uploadNormalCsv(fd,counter,qusType){
    
    
    $.ajax({  
                     url:baseUrl + 'admin/form_task/csvUpload',  
                     method:"POST",  
                     data:fd,  
                     contentType:false,          // The content type used when sending data to the server.  
                     cache:false,                // To unable request pages to be cached  
                     processData:false,          // To send DOMDocument or non processed data file it is set to false  
                     success: function(data){ 
                         
                         $("body").removeClass("loader-wrap");
                         $('.loader-img').hide();
                         data = JSON.parse(data);
                         
                          if(data.code==400)  
                          {  
                              $( '.alertText' ).text(data.msg );
                              $( '.alertType' ).text( string.error );
                              $( '.alert-danger' ).fadeIn().fadeOut( 5000 );
                              
                          }else if(data.code==200){
                              
                              getHtmlByList("innerQusType"+counter,qusType,counter)
                              
                              $( '.alertText' ).text( data.msg );
                              $( '.alertType' ).text( string.success );
                              $( '.alert-success' ).fadeIn().fadeOut( 5000 );   
                             

                              $("#optionCode"+counter).val(data.timestamp)  
                               
                                  
                                
                          }  
                          
                          
                     }  
        })
}



function exportCsv(counter){  
    var previousCounter = counter-1;
    var isRelation   = $("#relation"+counter).val();
    var depentVal = $("#dependency"+counter).val();
    
        if(counter==0 || isRelation ==undefined || isRelation ==""){
            //to generate first blank csv
            generateBlankCSV(counter);
            
        }else if( counter > 0  && isRelation !=undefined && isRelation!="" ){
            // generate dependent csv
            generateRelatedCSV(counter,isRelation,previousCounter,depentVal);
        }
}
//
//generate csv for first question
//
function generateBlankCSV(counter){
    
     var timestamp  = $("#optionCode"+counter).val();
     window.location.href =baseUrl+"admin/create-form?export=1&counter="+counter;
}

// create realted csv file
function generateRelatedCSV(counter,isRelation,previousCounter,depentVal){
    
     var timestamp  = $("#optionCode"+depentVal).val();
     
     
     if(timestamp){
         
         window.location.href =baseUrl+"admin/Form_task/generateDependentCsv?timestamp="+timestamp;
         
     }else{
         
         alert("Please select csv file for previous question "+counter+".")
     }
     
}

