

function addQuestionSection() {
    var countSection = $('.inner-wrapper').find('section').length;

    var getSection = getSectionHtml(countSection);
    var qtype = 'checkbox';
    $(".inner-question-html").append(getSection)
    $('#questionType' + countSection).val(qtype);
}


function getSectionHtml(countSection) {


    return  '   <section class="question-block active" id="question-block'+countSection+'">  '  + 
 '                     <div class="inner-wrapper">  '  + 
 '                        <span class="toggle-btn-wrapper filter-side-wrapper">  '  + 
 '                        <span class="dots"></span>  '  + 
 '                        <span class="dots"></span>  '  + 
 '                        <span class="dots"></span>  '  + 
 '                        </span>                  '  + 
 '                        <div class="form-option">  '  + 
 '                           <div class="form-element-col">  '  + 
 '                              <ul>  '  + 
 '     '  + 
 '                                             <li onclick=getHtmlByList("innerQusType'+countSection+'","checkbox",'+countSection+')><a href="javascript:void(0)"><span class="icon checkbox"></span></a></li>  '  + 
 '                                             <li onclick=getHtmlByList("innerQusType'+countSection+'","dropdown",'+countSection+')><a href="javascript:void(0)" title="Dropdown"><span class="icon dropdown"></span></a></li>  '  + 
 '                                             <li onclick=getHtmlByList("innerQusType'+countSection+'","text",'+countSection+')><a href="javascript:void(0)" title="Text"><span class="icon text-box"></span></a></li>  '  + 
 '                                             <li onclick=getHtmlByList("innerQusType'+countSection+'","textarea",'+countSection+')><a href="javascript:void(0)" title="Text Area"><span class="icon text-area"></span></a></li> '  + 
 '                               '  + 
 '                              </ul>  '  + 
 '                           </div>  '  + 
 '                        </div>  '  + 
 '                        <div class="form-question">  '  + 
 '                           <input type="text" placeholder="Question'+(countSection+1)+'" name=question['+countSection+'] id="question'+countSection+'"> <input type="hidden" name="relation['+countSection+']" id="relation'+countSection+'"> <input type="hidden" name="questionType['+countSection+']" id="questionType'+countSection+'"> <input type="hidden" name="dependency['+countSection+']" id="dependency'+countSection+'"><input type="hidden" name="optionCode[]" id="optionCode'+countSection+'">'+ 
                                
            
            '             </div> <div id="innerQusType'+countSection+'"><div class="flex-row form-option-wrap" id="attribute0"> </div>  </div> '  + 
 '                        <div class="flex-row">  '  + 
 '                           <div class="flex-col-sm-10">  '  + 
 '                              <div class="action-btn-wrap">  '  + 
 '     '  + 
 '                                 <div class="action-wrap">      '  + 
 '                                   <span class="action-btn plus-circle"  onclick="makeQusChoice('+countSection+')" title="Add"></span>  '  + 
 '                                   <label class="action-btn export" title="Import" for="importFile'+countSection+'" ></label>  '  + 
 '                                   <input type="file" id="importFile'+countSection+'" onclick="this.value=null;" onchange=importCsv('+countSection+',this) style="display:none">  '  + 
 '                                   <label class="action-btn import" title="Export" onclick=exportCsv('+countSection+') for="export"></label>  '  + 
 '                                   <input type="file" id="import" style="display:none">  '  + 
 '                                   <span class="action-btn delete" onclick=removeQusBlock("question-block'+countSection+'") title="Delete"></span>  '  + 
 '                                   '  + 
 '                                 </div>  '  + 
 '     '  + 
 '                                 <div class="option-wrapper" id="choice'+countSection+'">  '  + 
 '                                     <ul>  '  + 
 '                                         <li onclick=getQusChoice('+countSection+',1)><a href="javascript:void(0)">Dependent</a></li>  '  + 
 '                                         <li onclick=getQusChoice('+countSection+',2)><a href="javascript:void(0)">Non dependent</a></li>  '  + 
 '                                     </ul>       '  + 
 '                                 </div>   '  + 
 '     '  + 
 '                             </div>  '  + 
 '                           </div>  '  + 
 '                        </div>  '  + 
 '                     </div>  '  + 
 '                 </section>  ' ; 

}


function removeQusBlock(id) {
    
    $("#" + id).remove();
}




function reset_menu() {
    $(".form-option").removeClass("active");
}


$("body").on('click', '.filter-side-wrapper', function (e) {


    reset_menu();
    e.stopPropagation();
    $(this).next().addClass("active");
});


$("body").click(function (e) {
    if (!$(e.target).is('.form-option *')) {
        $(".form-option").removeClass("active");

    }
});


// change question html by clicking on the list

function getHtmlByList(innerId, listType, questionIndex) {

    var innerCounter = $('#' + innerId + ' > div').length;
    
    $('#questionType' + questionIndex).val(listType);

    var innerHtml = generateHtml(innerId, listType, innerCounter);
    $("#" + innerId).html(innerHtml);

}


// This method will be used when html will be fetched using ooption list

function generateHtml(innerId, type, innerCounter) {

    if (type == "checkbox") {

        return      '<div class="flex-row form-option-wrap" id="attribute' + innerCounter + '">  ' +
                '                                       <div class="flex-col">  ' +
                '                                           <div class="th-checkbox">  ' +
                '                                               <input style="display:none;" class="filter-type filled-in"  type="checkbox" name="filter" id="Yes" value="">  ' +
                '                                               <label for="Yes" class="lbl-check"><span></span></label>  ' +
                '                                           </div>      ' +
                '                                       </div>   ' +
                '                                                 ' +
                '   		<div class="flex-col answer-col"> ' +
                '   		<input type="text"  class="answerbox" placeholder="Option 1">   ' +
                '   	</div>  ' +
                '</div>';

    } else if (type == "dropdown") {

        return   '  <div class="flex-row form-option-wrap" id="attribute' + innerCounter + '"> ' +
                '   <select class="form-control" readonly="true"><option>Select Box</option</select> ' +
                '   	</div>  ' +
                '   </div>';
        
    } else if (type == "mobile") {

        
        return   '  <div class="flex-row form-option-wrap">' +
                '<div class="flex-col answer-col">' +
                '<input type="text" readonly class="answerbox" placeholder="Mobile">' +
                '</div>' +
                '</div>';
    } else if (type == "text") {
        

        return   '<div class="flex-row form-option-wrap">' +
                '<div class="flex-col answer-col">' +
                '<input type="text" readonly class="answerbox" placeholder="Text" >' +
                '</div></div>';
    }
    
    else if (type == "textarea") {
        

        return   '<div class="flex-row form-option-wrap">' +
                '<div class="flex-col answer-col">' +
                '<textarea class="answerbox" placeholder="Text Area"></textarea>' +
                '</div></div>';
    }

}

function getOptionlist(opval) {

    $.ajax({
        type: 'GET',
        url: baseUrl + 'admin/form_task/getListOption',
        data: {option: opval},
        success: function (data) {
            $('#option_list').html(data);
            $('.selectpicker').selectpicker('refresh');
            //var res=JSON.parse(data);
            //console.log(data);
            /*if(res.code=='200'){
             $('#option_list').html(res.res_arr);
             $('.selectpicker').selectpicker('refresh');
             }*/

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}

function setRequireField(id) {
    var chkReq = $('#Required' + id).is(':checked');
    if (chkReq) {
        $('#RequiredHid' + id).val('1');
    } else {
        $('#RequiredHid' + id).val('0');
    }

}

// MAKE THAT THE QUES IS DEPENDENT OR NOT.
function makeQusChoice(counter){
    var oldCount = counter-1;
    $('#choice'+counter).removeClass("active");
    $('#choice'+oldCount).removeClass("active");
    
    $('#choice'+counter).addClass("active");
}

// get the dependent or non dependent choice of question.
function getQusChoice(counter,value){
    
    var html=""
    var j=1;
    // only for dependent qus
    if((value !=2) && counter > 0){
        
        for(var i=0;i<counter;i++){

            html+= '<div class="modal-body"><div class="th-checkbox">';
            html+='<input style="display:none;" onclick=selectDependency('+i+','+counter+')  type="radio" name="filter'+i+'" id="Yes'+i+'">';
            html+='<label for="Yes'+i+'" class="lbl-check"><span></span>Question'+ j +' </label>';
            html+='</div></div>';
          j++;  
        } 

        $("#dependancy").html(html);

        $("#myModal").modal("show");
   }
    $('#choice'+counter).removeClass("active");
    $('#relation'+counter).val(value);
    
}


// seleect qus dependency.

function selectDependency(selectedVal,qusVal){
    
    $("#dependency"+qusVal).val(selectedVal);
    $("#myModal").modal("hide");
}

// show form section

$(document).on("click", "#back_form", function () {

    $(".inner-right-panel").css("display", "block");

    $(".show_preview").css("display", "none");

})



function getDistrictList(value, divId, id) {

    $("body").addClass("loader-wrap");
    $('.loader-img').show();
    $('#add_task').attr('disabled', true);

    var opval = "dis";

    $.ajax({
        type: 'GET',
        url: baseUrl + 'admin/form_task/getListOption',
        data: {option: opval, value: value},
        success: function (data) {
            data = JSON.parse(data);
            $('#' + divId).css("display", "block");
            $('#' + id).html(data.dis);

            // $('#'+pcDivId).css("display","block");
            // $('#'+pcId).html(data.pc);

            //var res=JSON.parse(data);
            //console.log(data);
            /*if(res.code=='200'){
             $('#option_list').html(res.res_arr);
             $('.selectpicker').selectpicker('refresh');
             }*/
            $("body").removeClass("loader-wrap");
            $('.loader-img').hide();

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}


function getCollegeList(value, coldivId, colid, acDivID, acId) {

    $("body").addClass("loader-wrap");
    $('.loader-img').show();
    $('#add_task').attr('disabled', true);

    var opval = "ac";

    $.ajax({
        type: 'GET',
        url: baseUrl + 'admin/form_task/getListOption',
        data: {option: opval, value: value},
        success: function (data) {
            data = JSON.parse(data);
            $('#' + acDivID).css("display", "block");
            $('#' + acId).html(data.col);

            $('#' + coldivId).css("display", "block");
            $('#' + colid).html(data.ac);

            $("body").removeClass("loader-wrap");
            $('.loader-img').hide();

        },
        error: function (jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'URL page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }

            alert(msg);
        }


    });
}

function onTextBoxKeyUp() {
    alert('hello')
}