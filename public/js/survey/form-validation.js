
/*$("#dynamic_form").validate({
    errorClass: "alert-danger",
    rules: {
       'question[]':{required:true
                    },
       'dependency[]':{required:true},
       'optionCode[]':{required:true}
    },
    messages: {
        "question[]": {required:  "Question field can not be empty."},
        "dependency[]":{required: "Question field can not be empty."},
        "optionCode[]": {required:"Please upload answers for the question."}
    },
    submitHandler: function (form) {
                    form.submit();
       // $("body").addClass("loader-wrap");
       // $('.loader-img').show();
       // $('#add_task').attr('disabled', true);
        
        }
    
});*/


$(document).on("click","#survey-btn",function(e){
    e.preventDefault();
    var flag = false;
    var formName =  $("input[name=form_title]").val();
    
    var formDesc =  $(".form-desc").val();
    var steps = $("input[name=steps]").val();
    
    //form title
    if(formName =="" || formName =="undefined"){
        
        displayError("form-title","");
        flag = false;
    }else{
        
        hideError("form-title","");
        flag = true;
    }
    
    //form desc
    
    if(formDesc =="" || formDesc =="undefined"){
        
        displayError("form-desc","");
        flag = false;
        
    }else{
        
        hideError("form-desc","");
        flag = true;
    }
    
    // steps
    if(steps =="" || steps =="undefined"){
        
        displayError("steps","");
        flag = false;
        
    }else{
        
        hideError("steps","");
        flag = true;
    }
    
    
    //get rest part html
    
    var countSection = $('.inner-wrapper').find('section').length;
    
    for(var i=0;i<countSection;i++){
        
        
        if($("#question"+i).val() =="" || $("#question"+i).val()=="undefined"){
            
            displayError("","question"+i);
            flag = false;
        }else{
            
            hideError("","question"+i);
            flag = true;
        }
        
        if(($("#optionCode"+i).val() =="" || $("#optionCode"+i).val()=="undefined") && $("#questionType"+i).val()!="text"){
            
            displayAlert(i);
            flag = false;
            
        }else{
            flag = true;
        }
        
    }
    if(flag==true){
        
        $("#dynamic_form").submit();
    }
    
});


/*
 *Check for price only numeric and 1 dot allow
 * @param {decimal or int} elementRef
 * @returns {Boolean}
 */
function numericOnly( elementRef,event ) {
    var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;
    if ( (keyCodeEntered >= 48) && (keyCodeEntered <= 57) ) {

        return true;
    }

// '.' decimal point...

    else if ( keyCodeEntered == 46 ) {

// Allow only 1 decimal point ('.')...

        if ( (elementRef.value) && (elementRef.value.indexOf( '.' ) >= 0) )
            return false;
        else
            return true;
    }

    return false;
}


function displayError(errorClass,id){
    
    if(errorClass){
        
        $("."+errorClass).css({'border-color': 'red','color':'red'});
    }
    else if(id){
        
        $("#"+id).css({'border-color': 'red','color':'red'});
    }
    
}

function hideError(errorClass,id){
    if(errorClass){
        
        $("."+errorClass).css({'border-color': '','color':''});
    }
    else if(id){
        
        $("#"+id).css({'border-color': '','color':''});
    }  
}


 function displayAlert(counter){
      
      var count = ++counter;
      alert("Please import answers for question"+count);
      
 }

