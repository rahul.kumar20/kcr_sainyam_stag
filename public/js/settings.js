// add news

$("#settings_change_form").validate({
    errorClass: "alert-danger",
    ignore: [],
    debug: false,
    rules: {
        helplineNumber: {
          required: true,
            minlength: 10,
            maxlength: 10,
            digits: true,
        },
        
        email: {
             required: true,
            email: true,
        },
    },
    messages: {
       
    },
    url: {

        url: string.task_url_req
    },
    /* use below section if required to place the error*/
    errorPlacement: function (error, element) {
        if (element.attr("name") == "desc") {
            error.insertAfter("#cke_desc");
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {

        form.submit();
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
        $('#add_news').attr('disabled', true);
    }
});



/**
 * @function numeralsOnly
 *  allow only numeber enter
 *  in input box
 *
 * @param {type} evt
 * @returns {Boolean}
 */
function numeralsOnly(evt) {
    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
        ((evt.which) ? evt.which : 0));
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
