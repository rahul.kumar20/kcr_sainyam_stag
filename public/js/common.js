$(document).ready(function() {
    $("input").keypress(function(e) {
        var inp = $.trim($(this).val()).length;
        if (inp == 0 && e.which === 32) {
            return false;
        }
    });

    $(".dispLimit").change(function() {
        var filter = {};
        var pageUrl = $("#pageUrl").val();
        filter = $("#filterVal").val();
        filter = JSON.parse(filter);
        delete filter["perPage"];
        var limit = $(".dispLimit").find(":selected").val();

        if (limit != undefined) {
            filter["limit"] = limit;
        }
        var queryParams = JSON.stringify(filter);
        window.location.href = pageUrl + "?data=" + window.btoa(queryParams);
    });

    $(".searchlike").keyup(function(e) {
        if (e.which === 13) {
            var filter = {};
            filter = $("#filterVal").val();
            var pageUrl = $("#pageUrl").val();
            var searchlike = $.trim($(this).val());
            filter = JSON.parse(filter);
            delete filter["searchlike"];
            if (searchlike == 0) {
                alert("Please type something to search");
                return false;
            }
            if (searchlike != undefined) {
                filter["searchlike"] = searchlike;
            }

            //var queryParams = $.param(filter)
            //window.location.href = pageUrl + '?' + queryParams;

            var queryParams = JSON.stringify(filter);
            window.location.href = pageUrl + "?data=" + window.btoa(queryParams);
        }
    });

    /*$('.searchCloseBtn').click(function (e) {
       var filter = {};
       filter = $('#filterVal').val();
       var pageUrl = $('#pageUrl').val();
       filter = JSON.parse(filter);
       delete filter['searchlike'];
       var filterLen = $.keyCount(filter);
       if (filterLen == 0) {
       window.location.href = pageUrl
       } else {
       var queryParams = $.param(filter)
       window.location.href = pageUrl + '?' + queryParams;
       }

       }); */

    /*
     * Apply Filter  Notification
     */

    $(".applyfilter").click(function() {
        var filter = {};
        filter = $("#filterVal").val();
        var pageUrl = $("#pageUrl").val();

        filter = JSON.parse(filter);

        delete filter["platform"];
        delete filter["startDate"];
        delete filter["endDate"];

        var platform = $(".platform").find(":selected").val();
        var startDate = $(".startDate").val();
        var endDate = $(".endDate").val();

        if (platform != undefined && platform.length != 0) {
            filter["platform"] = platform;
        }
        if (startDate != undefined && startDate.length != 0) {
            filter["startDate"] = startDate;
        }
        if (endDate != undefined && endDate.length != 0) {
            filter["endDate"] = endDate;
        }

        var filterLen = $.keyCount(filter);
        if (filterLen == 0) {
            alert("Please select a filter");
            return false;
        }

        var queryParams = $.param(filter);

        window.location.href = pageUrl + "?" + queryParams;
    });

    /*
     * Apply Filter User
     */
    $(".applyFilterUser").click(function() {
        var filter = {};
        filter = $("#filterVal").val();
        var pageUrl = $("#pageUrl").val();

        filter = JSON.parse(filter);

        delete filter["startDate"];
        delete filter["endDate"];
        delete filter["uid"];
        delete filter["taskCompleted"];

        var status = $(".status").find(":selected").val();
        var uid = $(".uid").find(":selected").val();
        var homeContentType = $(".categoryType").find(":selected").val();

        var startDate = $(".startDate").val();
        var endDate = $(".endDate").val();
        var EffectivestartDate = $(".EffectivestartDate").val();
        var EffectiveendDate = $(".EffectiveendDate").val();
        var taskCompleted = $(".taskCompleted").find(":selected").val();
        var section = $(".section").find(":selected").val();
        //status
        if (status != undefined && status.length != 0) {
            filter["status"] = status;
        }
        //UID
        if (uid != undefined && uid.length != 0) {
            filter["uid"] = uid;
        }
        //start date
        if (startDate != undefined && startDate.length != 0) {
            filter["startDate"] = startDate;
        }

        //end date
        if (endDate != undefined && endDate.length != 0) {
            filter["endDate"] = endDate;
        }
        //effective start date
        if (EffectivestartDate != undefined && EffectivestartDate.length != 0) {
            filter["EffectivestartDate"] = EffectivestartDate;
        }

        //effective end date
        if (EffectiveendDate != undefined && EffectiveendDate.length != 0) {
            filter["EffectiveendDate"] = EffectiveendDate;
        }
        //task complete
        if (taskCompleted != undefined && taskCompleted.length != 0) {
            filter["taskCompleted"] = taskCompleted;
        }
        //task complete
        if (homeContentType != undefined && homeContentType.length != 0) {
            filter["homeContentType"] = homeContentType;
        }
        //section complete
        if (section != undefined && section.length != 0) {
            filter["section"] = section;
        }
        if (!status.length &&
            !uid.length &&
            !startDate.length &&
            !endDate.length &&
            !taskCompleted.length /* && !college.length*/
        ) {
            console.log("Not any filter selected");
            return;
        }
        var filterLen = $.keyCount(filter);
        if (filterLen == 0) {
            alert("Please select a filter");
            return false;
        }

        //var queryParams = $.param(filter)
        //window.location.href = pageUrl + '?' + queryParams;

        var queryParams = JSON.stringify(filter);
        window.location.href = pageUrl + "?data=" + window.btoa(queryParams);
    });

    $(".resetfilter").click(function() {
        var pageUrl = $("#pageUrl").val();
        window.location.href = pageUrl;
    });

    $(".resendPush").click(function() {
        var notiToken = $("#notiToken").val();
        $.ajax({
            type: "get",
            url: baseUrl + "admin/notification/resendNotification",
            dataType: "json",
            data: { notiToken: notiToken },
            success: function(respdata) {
                if (respdata.code == 200) {
                    window.location.reload();
                }
            },
        });
    });

    $(".editPush").click(function() {
        var notiToken = $("#notiToken").val();
        window.location.href =
            baseUrl + "admin/notification/edit?data=" + notiToken;
    });

    $(".exportCsv").click(function() {
        var filter = {};
        filter = $("#filterVal").val();
        var pageUrl = $("#pageUrl").val();
        filter = JSON.parse(filter);
        filter["export"] = 1;

        //var queryParams = $.param(filter);
        //window.location.href = pageUrl + '?' + queryParams;

        var queryParams = JSON.stringify(filter);
        window.location.href = pageUrl + "?data=" + window.btoa(queryParams);
    });
});

$.extend({
    keyCount: function(o) {
        if (typeof o == "object") {
            var i,
                count = 0;
            for (i in o) {
                if (o.hasOwnProperty(i)) {
                    count++;
                }
            }
            return count;
        } else {
            return false;
        }
    },
});

function changepassword(oldpassword, newpassword) {
    $.ajax({
        type: "post",
        url: "/ajax/changepassword",
        dataType: "json",
        data: { password: newpassword, oldpassword: oldpassword },
        success: function(respdata) {
            if (respdata.code == 200) {
                customalert(respdata.msg);
                $("#oldpassword").val("");
                $("#newpassword").val("");
                $("#cnfpassword").val("");
            } else {
                customalert(respdata.msg);
            }
        },
    });
}

function resetpassword(token, password) {
    $.ajax({
        type: "post",
        url: baseUrl + "ajax/reset",
        dataType: "json",
        data: { token: token, password: password },
        success: function(respdata) {
            if (respdata.code == 200) {
                $(".password").val("");
            }
            customalert(respdata.msg);
        },
    });
}

function manageSideBar(action) {
    $.ajax({
        type: "post",
        url: baseUrl + "req/manage-sidebar",
        dataType: "json",
        data: { action: action, csrf_token: csrf_token },
        success: function(respData) {
            if (respData.code == 200) {
                csrf_token = respData.csrf;
            }
        },
    });
}

function sendforgotemail(email) {
    $.ajax({
        type: "post",
        url: "/ajax/forgot",
        dataType: "json",
        data: { email: email },
        success: function(respdata) {
            if (respdata.code == 200) {
                $("#email").val("");
            }
            customalert(respdata.msg);
        },
    });
}

var controller = window.location.pathname.split("/")[3];
var action = window.location.pathname.split("/")[4];

var _validFileExtensionsImage = [".jpg", ".png", ".jpeg", ".gif", ".bmp"];
var _validFileExtensionsDoc = [".doc", ".docx", ".pdf"];

function ValidateSingleInput(oInput, _validFileExtensions, id) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (
                    sFileName
                    .substr(
                        sFileName.length - sCurExtension.length,
                        sCurExtension.length
                    )
                    .toLowerCase() == sCurExtension.toLowerCase()
                ) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                //alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                $("#image-error")
                    .empty()
                    .text(
                        "Sorry, " +
                        sFileName +
                        " is invalid, allowed extensions are: " +
                        _validFileExtensions.join(", ")
                    );
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

/**
 *
 * @ code to validate uploaded image.
 */
var loadFile_signup = function(event, id, oInput) {
    var return_data = ValidateSingleInput(oInput, _validFileExtensionsImage, id);
    if (return_data) {
        var output = document.getElementById(id);
        $("#" + id).css(
            "background-image",
            "url(" + URL.createObjectURL(event.target.files[0]) + ")"
        );
    }
};
/**
 *
 *@description This is used to fadeout the ajax loader from page.
 *
 */

$(document).ready(function() {
    $("#pre-page-loader").hide();
});

/**
 * @description This code of jquery is used to show ckeditor on the add & edit cms pages.
 *
 */

$(document).ready(function() {
    if (controller === "cms" && (action === "add" || action === "edit")) {
        CKEDITOR.replace("page_desc");
    }
});

/**
 * forgot password validation
 */

$("#forgot").click(function(event) {
    var arr = [];
    var f = 0;
    var email = $("#email").val();
    var emailptrn = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var csrf_token = $("#csrf_token").val();
    if (email.length == 0) {
        document.getElementById("emails").innerHTML = "Please fill this field";
        $("#email_error").addClass("commn-animate-error");
        f = 1;
        arr.push("email");
        setTimeout(function() {
            $("#emails").text("");
        }, 5000);
    } else if (email.match(emailptrn)) {
        $.ajax({
            type: "post",
            url: domain + "/admin/Admin/check_email_avalibility",
            data: { email: email, csrf_token: csrf_token },
            async: false,
            success: function(result) {
                console.log(result);

                var Obj = JSON.parse(result);
                if (Obj.code == 201) {
                    $("#incorrectemail").html("Email Exist");
                    $("#email_error").addClass("commn-animate-error");
                    setTimeout(function() {
                        $("#incorrectemail").text("");
                    }, 3000);
                } else if (Obj.code == 200) {
                    $("#errmsg2").html("Email Doesnot Exist");
                    $("#email_error").addClass("commn-animate-error");

                    setTimeout(function() {
                        $("#errmsg2").text("");
                    }, 3000);
                    f = 1;
                }
                $("#csrf_token").val(Obj.csrf_token);
            },
        });
    } else {
        document.getElementById("emails").innerHTML = "Please enter valid email";
        $("#email_error").addClass("commn-animate-error");
        f = 1;
        arr.push("email");
        setTimeout(function() {
            $("#emails").text("");
        }, 5000);
    }

    if (f == 1) {
        $("#" + arr[0]).focus();
        return false;
    } else {
        $("#forgetpass").submit();
    }
});

//$('#resetbtn').click(function (event) {
//
//    $('#resetform').submit();
//});

$(document).ready(function() {
    $("#resetbtn").click(function(event) {
        var arr = [];
        var f = 0;
        var new_pass = $("#new_pass").val();
        var con_pass = $("#con_pass").val();
        var token = $("#token").val();
        //  var passptr = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
        var passptr = /^.{6,}$/;
        if (new_pass.length == 0) {
            document.getElementById("new_pass1").innerHTML = "Please fill this field";
            $("#passerror").addClass("commn-animate-error");
            return false;
        } else if (!new_pass.match(passptr)) {
            document.getElementById("new_pass1").innerHTML =
                "Password must be atleast 6 character";
            $("#passerror").addClass("commn-animate-error");
            return false;
        }
        if (con_pass.length == 0) {
            document.getElementById("con_pass1").innerHTML = "Please fill this field";
            $("#conpassreq").addClass("commn-animate-error");
            return false;
        } else if (!new_pass.match(passptr)) {
            document.getElementById("con_pass1").innerHTML =
                "Password must be atleast 6 characters ";
            $("#conpassreq").addClass("commn-animate-error");
            return false;
        }

        if (new_pass != con_pass) {
            document.getElementById("con_pass1").innerHTML =
                "Confirm password does not match";
            $("#conpassreq").addClass("commn-animate-error");
            return false;
        }

        if (f == 1) {
            $("#" + arr[0]).focus();
            return false;
        } else {
            //alert('hdfh');
            $("#resetform").submit();
        }
    });
});

/*
 * Login validation
 */
$("#login").click(function(event) {
    var arr = [];
    var f = 0;
    var user_email = $("#useremail").val();
    var user_password = $("#userpassword").val();

    //var passptr = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    var emailptrn = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var passptr = /^.{6,}$/;

    if (user_email.length == 0) {
        document.getElementById("email1").innerHTML = "Please fill this field";
        $("#email_error").addClass("commn-animate-error");
        return false;
    } else if (!user_email.match(emailptrn)) {
        document.getElementById("email1").innerHTML = "Please enter valid email";
        $("#email_error").addClass("commn-animate-error");
        return false;
    }
    if (user_password.length == 0) {
        document.getElementById("password1").innerHTML = "Please enter password";
        $("#passworderr").addClass("commn-animate-error");
        return false;
    } else if (!user_password.match(passptr)) {
        document.getElementById("password1").innerHTML =
            "Password must be atleast 6 characters";
        $("#passworderr").addClass("commn-animate-error");
        return false;
    }

    if (f == 1) {
        $("#" + arr[0]).focus();
        return false;
    } else {
        $("#adminlogin").submit();
    }
});

$(".removemessage").keyup(function() {
    $(".form-field-wrap").removeClass("commn-animate-error");
});

$("#email").keyup(function() {
    $("#emails").removeClass("commn-animate-error");
});

//-----------------------------------------------------------------------
$(function() {
    var flag = false;
    $(".filter").each(function() {
        if ($(this).val()) {
            flag = true;
        }
    });

    if (flag == false) {
        $("#filterbtn").prop("disabled", true);
        $("#resetbtn").prop("disabled", true);
    } else {
        $("#filterbtn").prop("disabled", false);
        $("#resetbtn").prop("disabled", false);
    }
});

$(document.body).on("change", ".filter", function() {
    var flag = false;
    $(this).each(function() {
        if ($(this).val()) {
            flag = true;
        }
    });
    if (flag == true) {
        $("#filterbtn").prop("disabled", false);
        $("#resetbtn").prop("disabled", false);
    } else {
        $("#filterbtn").prop("disabled", true);
        $("#resetbtn").prop("disabled", true);
    }
});

$(document.body).on("blur", ".filtertxt", function() {
    var flag = false;
    $(this).each(function() {
        if ($(this).val()) {
            flag = true;
        }
    });
    if (flag == true) {
        $("#filterbtn").prop("disabled", false);
        $("#resetbtn").prop("disabled", false);
    } else {
        $("#filterbtn").prop("disabled", true);
        $("#resetbtn").prop("disabled", true);
    }
});

//-----------------------------------------------------------------------
/**
 *
 * @name starting to implement jquery code when document loaded.
 *
 **/
$(function() {
    if ($(".search-box").val()) {
        if ($(".search-box").val().length) {
            $(".srch-close-icon").show();
            $(".search-icon").hide();
        } else {
            $(".srch-close-icon").hide();
            $(".search-icon").show();
        }
    }
});

$(document.body).on("keyup", ".search-box", function() {
    if ($(this).val().length > 0) {
        //$('.srch span').removeClass('search-icon');
        // $('.srch span').addClass('search-close-icon');
        $(".srch-close-icon").show();
        $(".search-icon").hide();
    } else {
        // $('.srch span').removeClass('search-close-icon');
        //$('.srch span').addClass('search-icon');
        $(".srch-close-icon").hide();
        $(".search-icon").show();
    }
});

/**
 *
 * @returns {undefined}
 *
 */
function pageCountForm() {
    $("#page_count_form").submit();
}
//-----------------------------------------------------------------------
/**
 * @name getStates
 * @description Function to get States list as per the country.
 */
function getStates(value, id) {
    var csrf = $("#csrfToken").val();
    if (value) {
        $.ajax({
            method: "GET",
            url: baseUrl + "admin/AjaxUtil/getStatesByCountry",
            data: { id: value, csrf_token: csrf },
        }).done(function(msg) {
            msg = JSON.parse(msg);
            $("#" + id).empty();
            $("#" + id).append("<option value=>Select State</option>");
            $.each(msg, function(i, item) {
                $("#" + id).append(
                    $("<option>", {
                        value: item.id,
                        text: item.name,
                    })
                );
            });
            $("#" + id).selectpicker("refresh");
        });
    }
}

//----------------------------------------------------------------------
/**
 * @name getCities
 * @description Function to get cities list as per the states.
 */
function getCities(value, id) {
    var csrf = $("#csrfToken").val();
    if (value) {
        $.ajax({
            method: "GET",
            url: baseUrl + "admin/AjaxUtil/getCityByState",
            data: { id: value, csrf_token: csrf },
        }).done(function(msg) {
            msg = JSON.parse(msg);
            $("#" + id).empty();
            $("#" + id).append("<option value=>Select City</option>");
            $.each(msg, function(i, item) {
                $("#" + id).append(
                    $("<option>", {
                        value: item.id,
                        text: item.name,
                    })
                );
            });
            $("#" + id).selectpicker("refresh");
        });
    }
}
//----------------------------------------------------------------------
/**
 * @name blockUser
 * @description This method is used to show block user modal.
 *
 */

function blockUser(type, status, id, url, msg, action) {
    $("#new_status").val(status);
    $("#new_id").val(id);
    $("#new_url").val(url);
    $(".modal-para").text(msg);
    $("#action").text(action);
    $(".modal-title").text(action);
    $("#for").val(type);
    $("#myModal-block").modal("show");
}

//----------------------------------------------------------------------
/**
 * @name blockUser
 * @description This method is used to show block user modal.
 *
 */

function logoutUser() {
    $(".modal-para").text("Are you sure you want to logout ?");
    $("#myModal-logout").modal("show");
}

//-----------------------------------------------------------------------
/**
 * @name changeStatusToBlock
 * @description This method is used to block the user.
 *
 */

function changeStatusToBlock(type, status, id, url) {
    $.ajax({
        method: "POST",
        url: baseUrl + url,
        data: { type: type, new_status: status, id: id, csrf_token: csrf_token },
        beforeSend: function() {
            $("#pre-page-loader").fadeIn();
            $("#myModal-block").modal("hide");
        },
        success: function(res) {
            $("#pre-page-loader").fadeOut();
            res = JSON.parse(res);
            csrf_token = res.csrf_token;
            if (type == "profileupdateaccept") {
                $(".alertText").text(string.accept_success);
                $(".alertType").text(string.success);
                $("#unblock_" + res.id).show();
                $("#block_" + res.id).hide();
                $("#status_" + res.id)
                    .empty()
                    .text("Blocked");
                window.location.reload();
            } else {
                if (res.code === 200) {
                    if (status == 2) {
                        $(".alertText").text(string.block_success);
                        $(".alertType").text(string.success);
                        $("#unblock_" + res.id).show();
                        $("#block_" + res.id).hide();
                        $("#status_" + res.id)
                            .empty()
                            .text("Blocked");
                    } else {
                        $(".alertText").text(string.unblock_success);
                        $(".alertType").text(string.success);
                        $("#block_" + res.id).show();
                        $("#unblock_" + res.id).hide();
                        $("#status_" + res.id)
                            .empty()
                            .text("Active");
                    }
                    window.location.reload();
                } else if (res.code === 202) {
                    $(".alertText").text(res.msg.text);
                    $(".alertType").text(res.msg.type);
                }
                $(".alert-success").fadeIn().fadeOut(5000);
            }
        },
        error: function(xhr) {
            alert("Error occured.please try again");
            $("#pre-page-loader").fadeOut();
        },
    });
}
//----------------------------------------------------------------------
/**
 * @name deleteUser
 * @description This method is used to show delete user modal.
 *
 */

function deleteUser(type, status, id, url, msg) {
    $("#new_status").val(status);
    $("#new_id").val(id);
    $("#new_url").val(url);
    $(".modal-para").text(msg);
    $("#for").val(type);
    $(".modal-title").text("DELETE");
    $("#myModal-trash").modal("show");
}
//-----------------------------------------------------------------------
/**
 * @name changeStatusToDelete
 * @description This method is used to delte the user.
 *
 */

function changeStatusToDelete(type, status, id, url) {
    $.ajax({
        method: "POST",
        url: baseUrl + url,
        data: { type: type, new_status: status, id: id, csrf_token: csrf_token },
        beforeSend: function() {
            $("#pre-page-loader").fadeIn();
            $("#myModal-trash").modal("hide");
        },
        success: function(res) {
            $("#pre-page-loader").fadeOut();
            res = JSON.parse(res);
            if (res.code === 200) {
                window.location.reload();
            }
        },
        error: function(xhr) {
            alert("Error occured.please try again");
            $("#pre-page-loader").fadeOut();
        },
    });
}

//-----------------------------------------------------------------------
/**
 *@Description Here is the methods starts for the form validations in admin using jquery validator.
 *
 */
$(document).ready(function() {
    $.each($.validator.methods, function(key, value) {
        $.validator.methods[key] = function() {
            if (arguments.length > 0) {
                arguments[0] = $.trim(arguments[0]);
            }

            return value.apply(this, arguments);
        };
    });

    // error message
    $.validator.setDefaults({
        ignore: ":not(select:hidden, input:visible, textarea:visible):hidden:not(:checkbox)",

        errorPlacement: function(error, element) {
            if (element.hasClass("selectpicker")) {
                error.insertAfter(element);
            } else if (element.is(":checkbox")) {
                // element.siblings('span').hasClass('.check_error_msg').append(error);

                error.insertAfter($(".check_error_msg"));
            } else {
                error.insertAfter(element);
            }
            /*Add other (if...else...) conditions depending on your
             * validation styling requirements*/
        },
    });
    //custom methods

    $.validator.addMethod(
        "noSpace",
        function(value, element) {
            return value == "" || value.trim().length != 0;
        },
        ""
    );

    $.validator.addMethod(
        "searchText",
        function(value, element) {
            return value.replace(/\s+/g, "");
        },
        ""
    );

    /**
     * @name validate admin password change form
     * @description This method is used to validate admin change password form.
     *
     */
    $("#password_change_form").validate({
        errorClass: "alert-danger",
        rules: {
            oldpassword: {
                required: true,
                minlength: 6,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 50,
            },
            confirm_password: {
                required: true,
                minlength: 6,
                maxlength: 50,
                equalTo: "#password",
            },
        },
        messages: {
            oldpassword: {
                required: string.oldpasswordEmpty,
                minlength: string.passwordMinLength,
            },
            password: {
                required: string.newpasswordEmpty,
                minlength: string.passwordMinLength,
            },
            confirm_password: {
                required: string.confirmpasswordEmpty,
                minlength: string.passwordMinLength,
                equalTo: string.passwordnotmatch,
            },
        },
        submitHandler: function(form) {
            form.submit();
        },
    });

    /**
     * @name: add cms content form
     * @description: Thie function is used to validate admin add content form in cms.
     */
    $("#cms_add_form").validate({
        ignore: [],
        debug: false,
        errorClass: "alert-danger",
        rules: {
            title: {
                required: true,
            },
            page_desc: {
                required: function() {
                    CKEDITOR.instances.page_desc.updateElement();
                },
            },
            status: {
                required: true,
            },
        },
        /* use below section if required to place the error*/
        errorPlacement: function(error, element) {
            if (element.attr("name") == "page_desc") {
                element.next().css("border", "1px solid #a94442");
                error.insertBefore("textarea#page_desc");
            } else {
                error.insertBefore(element);
            }
        },
        submitHandler: function(form) {
            form.submit();
        },
    });

    /**
     * @name validate add app version form
     * @description This method is used to validate add app version form.
     *
     */
    $("#version_add_form").validate({
        errorClass: "alert-danger",
        rules: {
            name: {
                required: true,
            },
            title: {
                required: true,
            },
            desc: {
                required: true,
            },
            platform: {
                required: true,
            },
            update_type: {
                required: true,
            },
            current_version: {
                required: true,
            },
        },
        submitHandler: function(form) {
            form.submit();
        },
    });
    /**
     * @name common search for admin
     *
     *
     */
    $("#admin_search_form").validate({
        errorPlacement: function(error, element) {},
        rules: {
            search: {
                searchText: true,
                required: {
                    depends: function() {
                        if ($.trim($(this).val().length) == 0) {
                            //$('form#admin_search_from :input[type=text]').empty().css('border-color','#ff0000');
                            $("#searchuser").empty().css("border-color", "#a94442");
                            return false;
                        } else {
                            $.trim($(this).val());
                            return true;
                        }
                    },
                },
            },
        },
        submitHandler: function(form) {
            form.submit();
        },
    });

    /**
     * @name validate admin password change form
     * @description This method is used to validate admin change password form.
     *
     */
    $("#editadminprofile1").validate({
        errorClass: "alert-danger",
        rules: {
            Admin_Name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            mobile_number: {
                required: true,
            },
        },
        submitHandler: function(form) {
            form.submit();
        },
    });

    /**
     *@notification validation
     *
     */

    $("#notification_form").validate({
        errorClass: "alert-danger",
        rules: {
            title: {
                required: true,
            },
            platform: {
                required: true,
            },
            messagetext: {
                required: function() {
                    CKEDITOR.instances.messagetext.updateElement();
                },
            },
        },
        messages: {
            title: "Please select some title",
            platform: "Please select platform",
        },
        submitHandler: function(form) {
            return false;
            form.submit();
        },
    });

    /*
     * Add Subadmin
     */
    $("#subadmin_add").validate({
        errorClass: "alert-danger",
        rules: {
            name: {
                required: true,
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 16,
            },
            email: {
                required: true,
                email: true,
            },
            status: {
                required: true,
            },
        },
        messages: {
            required: string.requiredErr,
            email: {
                required: string.requiredErr,
                email: string.emailErr,
            },
            minlength: string.passwordErr,
            maxlength: string.passwordErr,
            status: string.statusErr,
        },
        submitHandler: function(form) {
            form.submit();
        },
    });
});

function checkNotiValidation() {
    var title = $("#title").val();
    var message = $("#messagetext").val();
    var checkedNum = $('input[name="userchecked[]"]:checked').length;
    if (title.length == 0) {
        $(".titleErr").text(string.title_req);
        $("#title").focus();
        return false;
    } else {
        $(".titleErr").text("");
    }
    if (message.length == 0) {
        $(".messgErr").text(string.message_req);
        $("#messagetext").focus();
        return false;
    } else {
        $(".messgErr").text("");
    }

    // if (!checkedNum) {
    //     $( '.userselecterror' ).text( string.user_req );
    //     return false;
    // }
    $("#add_not").submit();
    return true;
}

function CheckforNum(e) {
    //console.log(String.fromCharCode(e.keyCode));
    // Allow: backspace, delete, tab, escape, enter and  +
    if (
        $.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        e.which === 187 ||
        (e.keyCode == 65 && e.ctrlKey === true) ||
        (e.keyCode == 86 && e.ctrlKey === true) ||
        (e.keyCode == 67 && e.ctrlKey === true) ||
        (e.keyCode == 88 && e.ctrlKey === true) ||
        (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if (
        (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) &&
        (e.keyCode < 96 || e.keyCode > 105)
    ) {
        e.preventDefault();
    }
}

//check Add Subscription Form vaidation
function check_subscription_form() {
    var status = true;

    $(".error_name").text("");
    $(".error_price").text("");
    $(".error_type").text("");

    var subs_name = document.getElementById("title");
    var subs_price = document.getElementById("sub_price").value;

    if (subs_name.value.trim() == "" || subs_name.value.trim() == null) {
        $(".error_name").text("Please add subscription name.");
        status = false;
        subs_name.value = subs_name.value.replace(/[ ]{1,}/, "");
    } else if (subs_name.value.trim().length < 3) {
        $(".error_name").text("Subscription name should be atleast 3 characters.");
        status = false;
        subs_name.value = subs_name.value.replace(/[ ]{1,}/, "");
    }

    if (subs_price.trim() == "" || subs_price.trim() == null) {
        $(".error_price").text("Please add subscription price.");
        status = false;
    }
    if (subs_price < 1 && subs_price.trim() != "" && subs_price.trim() != null) {
        $(".error_price").text("Subscription price should be atleast $1.");
        status = false;
    }

    if (status) {
        $("#add_subscription").submit();
    }
}

//check Edit Subscription Form vaidation
function check_edit_subscription_form() {
    var status = true;

    $(".error_edit_name").text("");
    $(".error_edit_price").text("");

    var subs_name = document.getElementById("edit_title");
    var subs_price = document.getElementById("edit_sub_price").value;

    if (subs_name.value.trim() == "" || subs_name.value.trim() == null) {
        $(".error_edit_name").text("Please add subscription name.");
        status = false;
        subs_name.value = subs_name.value.replace(/[ ]{1,}/, "");
    } else if (subs_name.value.trim().length < 3) {
        $(".error_edit_name").text(
            "Subscription name should be atleast 3 characters."
        );
        status = false;
        subs_name.value = subs_name.value.replace(/[ ]{1,}/, "");
    }
    if (subs_price.trim() == "" || subs_price.trim() == null) {
        $(".error_edit_price").text("Please add subscription price.");
        status = false;
    }
    if (subs_price < 1 && subs_price.trim() != "" && subs_price.trim() != null) {
        $(".error_edit_price").text("Subscription price should be atleast $1.");
        status = false;
    }

    if (status) {
        $("#edit_subscription").submit();
    }
}

$(".mono_view").click(function() {
    $(".list_view_subscription").hide();
    $(".card_view_subscription").show();
});
$(".list_view").click(function() {
    $(".list_view_subscription").show();
    $(".card_view_subscription").hide();
});

//////// RCC Admin Login page JS
$(function() {
    //------------ floating label ----------
    $(".has-floating-label input, .has-floating-label textarea")
        .on("focus blur", function(e) {
            $(this)
                .parents(".has-floating-label")
                .toggleClass("focused", e.type === "focus" || this.value.length > 0);
        })
        .trigger("blur");

    //-------- login form toggle menu-------

    $("body").on("click", "#showforgot", function() {
        $(this).parents(".form-wrapper").addClass("showforgot");
        $(".form-wrapper.forgot").addClass("showforgot");
    });

    $("body").on("click", "#showlogin", function() {
        $(this).parents(".form-wrapper").removeClass("showforgot");
        $(".form-wrapper.login").removeClass("showforgot");
    });

    $("body").on("click", ".showemailsuccess-btn", function() {
        // $(this).parents('.form-wrapper').addClass('showemailsuccess').removeClass('showforgot');
        // $('.form-wrapper.sendsuccess').addClass('showemailsuccess');
    });

    $("body").on("click", "#showsuccess", function() {
        //  $('.form-wrapper').addClass('showsuccess');
    });

    //------- dropdown  menu  ---
    $("body").on("click", ".setting-icon", function(event) {
        event.stopPropagation();
        if ($(this).hasClass("clicked")) {
            $(".profile-dropdown").slideUp();
            $(this).removeClass("clicked");
        } else {
            $(".setting-icon.clicked").removeClass("clicked");
            $(".profile-dropdown").slideUp();
            $(this).addClass("clicked");
            $(".profile-dropdown").slideDown();
        }
    });
    $("html").click(function(event) {
        $(".profile-dropdown").slideUp();
        $(".setting-icon").removeClass("clicked");
    });

    //-------filter  show hide  ---
    $("body").on("click", ".filter-button", function(event) {
        event.stopPropagation();
        $(".filter-wrapper").addClass("open");
    });
    $("body").on("click", ".filter-wrapper", function(event) {
        event.stopPropagation();
    });
    $("body").on("click", ".filter-wrapper .close-filter", function(event) {
        $(".filter-wrapper").removeClass("open");
    });
    $("html").click(function(event) {
        $(".filter-wrapper").removeClass("open");
    });

    //-----------toggle-btn ----------
    $(".toggle-btn").click(function() {
        $("body").toggleClass("toggle");
    });
    $(".mobile-ovarlay").click(function() {
        $("body").removeClass("toggle");
    });

    //------------show-error ---------------
    $("body").on("click", ".show-error", function() {
        $(".common-msg").addClass("show");
        setTimeout(function() {
            $(".common-msg").removeClass("show");
        }, 5000);
    });
    $("body").on("click", ".common-msg .close", function() {
        $(".common-msg").removeClass("show");
    });

    //--------search add cross btn on key press --
    $(".search-input").keyup(function() {
        if ($.trim($(".search-input").val()).length) {
            $(this).parents(".search-wrapper").find(".clear-search").addClass("show");
            console.log($(this).parents(".search-wrapper").find(".clear-search"));
        } else {
            $(this)
                .parents(".search-wrapper")
                .find(".clear-search")
                .removeClass("show");
        }
    });
    $(".clear-search").click(function() {
        $('input[type="search"]').val("");
        $(this).removeClass("show");
    });

    //-------Show Hide pssword --------

    $("body").on("click", ".ShowHidepssword", function() {
        if (
            $(this).parents(".input-form").find("input").attr("type") === "password"
        ) {
            $(this).parents(".input-form").find("input").attr("type", "text");
            $(this).children().removeClass("fa fa-eye").addClass("fa fa-eye-slash");
        } else {
            $(this).parents(".input-form").find("input").attr("type", "password");
            $(this).children().removeClass("fa fa-eye-slash").addClass("fa fa-eye");
        }
    });

    //---------- animation js ---
    new WOW().init();

    //----------- choose image---------------
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(".edit-image img").css({
                    opacity: "1",
                });
                $("#uploadedImage").attr("src", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#chooseFile").change(function() {
        readURL(this);
    });

    //---------------- datepicker ---
    //$('.datepicker').datepicker();
});

//////// RCC Admin Login page JS

// Append sub-category START
var sub_categorycount = 2;

$("body").on("click", ".add_sub_category", function() {
    $(".appended-wrap").append(
        '<div class="form-group" id="' +
        sub_categorycount +
        '"><div class="input-holder"><input type="text" minlength="3" maxlength="30" class="sub_category_add" name="sub_category[]"  id = "sub_category_' +
        sub_categorycount +
        '" placeholder="Sub category name"><span id = "' +
        sub_categorycount +
        '" class="srch-close-icon remove_category" style="display: inline;">X</span></div></div>'
    );
    sub_categorycount++;
});

// Append sub-category END

// Remove sub-category START
$("body").on("click", ".remove_category", function() {
    var span_id = $(this).attr("id");
    $("#" + span_id).remove();
});
// Remove sub-category END

//check category ajax START
var status = false;

function check_category_db() {
    var csrf_token = $("#csrfToken").val();
    var category_name = $("#category_name").val();
    var category = $("#token_key").val();

    var request_data = { category_name: category_name, csrf_token: csrf_token };
    if (category != null && category != "") {
        request_data = {
            category_name: category_name,
            category: category,
            csrf_token: csrf_token,
        };
    }

    $.ajax({
        type: "get",
        url: baseUrl + "admin/check_category",
        dataType: "json",
        async: false,
        data: request_data,
        success: function(respdata) {
            if (respdata.code == 200) {
                status = true;
            } else {
                status = false;
            }
        },
    });
    return status;
}
//check category ajax END

/// Add category JS CODE START

$(".save_category").click(function() {
    $(".category_name_error").text("");
    $(".sub_category_error").text("");
    var category_name = $("#category_name").val();
    var sub_category = $("#sub_category").val();
    var error = true;
    if (category_name == null || category_name == "") {
        $(".category_name_error").text("Please enter category name.");
        error = false;
    }

    if (sub_category == null || sub_category == "") {
        $(".sub_category_error").text("Please enter sub-category name.");
        error = false;
    }

    var check_category = check_category_db();

    if (check_category != "false") {
        $(".category_name_error").text("Category already exists.");
        error = false;
    }

    if (error) {
        $("#add_category_form").submit();
    }
});

/// Add category JS CODE END

/// EDIT category JS CODE START

$(".update_category").click(function() {
    $(".category_name_error").text("");
    $(".sub_category_error").text("");
    var category_name = $("#category_name").val();
    var sub_category = $("#sub_category").val();

    var error = true;
    if (category_name == null || category_name == "") {
        $(".category_name_error").text("Please enter category name.");
        error = false;
    }

    var check_category = check_category_db();

    if (check_category != "false") {
        $(".category_name_error").text("Category already exists.");
        error = false;
    }

    if (sub_category == null || sub_category == "") {
        $(".sub_category_error").text("Please enter sub-category name.");
        error = false;
    }

    if (error) {
        $("#edit_category_form").submit();
    }
});

/// EDIT category JS CODE END

//Check category in DB START

$("body").on("blur", "#category_name", function() {
    status = false;
    var category_name = $(this).val();
    $(".category_name_error").text("");
    var csrf_token = $("#csrfToken").val();

    var category = $("#token_key").val();

    var request_data = { category_name: category_name, csrf_token: csrf_token };
    if (category != null && category != "") {
        request_data = {
            category_name: category_name,
            category: category,
            csrf_token: csrf_token,
        };
    }

    $.ajax({
        type: "get",
        url: baseUrl + "admin/check_category",
        dataType: "json",
        data: request_data,
        async: false,
        success: function(respdata) {
            if (respdata.code == 200) {
                $(".category_name_error").text("Category already exists.");
                return false;
            }
        },
    });
});

//Check category in DB END

//Check sub-category in DB START

$("body").on("blur", "#sub_category", function() {
    status = false;

    var category_name = $(this).val();
    $(".category_name_error").text("");

    $.ajax({
        type: "get",
        url: baseUrl + "admin/check_category",
        dataType: "json",
        data: { category_name: category_name },
        success: function(respdata) {
            if (respdata.code == 200) {
                $(".category_name_error").text("Category already exists.");
                return false;
            }
        },
    });
});

//Check category in DB END

//check for same category in all sub-category fileds START

$("body").on("blur", ".sub_category_add", function() {
    $(".sub_category_exists").text("");
    var sub_cat_id = $(this).attr("id");
    var sub_category_val = $(this).val().toLowerCase();

    $(".sub_category_add").css("background-color", "white");

    $(".sub_category_add").each(function(i) {
        var current_id = $(this).attr("id");

        if (
            sub_category_val != "" &&
            sub_category_val == $(this).val().toLowerCase() &&
            $(this).attr("id") != sub_cat_id
        ) {
            $("#" + sub_cat_id).css("background-color", "#DC143C");
            $("#" + sub_cat_id).val("");
            $(".sub_category_exists").text("This sub-category is already there.");
            return false;
        }
    });
});

//check for same category in all sub-category fileds END
// Wait for window load
//for loader
// $(window).on('load', function() {
//     $("body").removeClass("loader-wrap");
//     $('.loader-img').hide();
// })
$(function() {
    /* code here */
    $("body").removeClass("loader-wrap");
    $(".loader-img").hide();
});
// document.onreadystatechange = function () {
//     var state = document.readyState
//     if (state == 'complete') {
//         setTimeout(function () {
//             $("body").removeClass("loader-wrap");
//             $('.loader-img').hide();
//         }, 10);
//     }
// }

///////////////////////distict list
function getDistrictForState(url, stateId, districtId) {
    $("#distict").empty();
    $("#collegeDa").empty();
    $("#distict").selectpicker("refresh");
    $("#collegeDa").selectpicker("refresh");

    if (stateId != undefined) {
        var state_id = stateId;
        var csrf = csrf_token;
        var data = {
            id: state_id,
            csrf_token: csrf,
        };
        $.ajax({
            method: "POST",
            url: baseUrl + url,
            data: data,
            beforeSend: function() {},
            success: function(res) {
                res = JSON.parse(res);

                csrf_token = res.csrf_token;
                if (res.code == 200) {
                    $("#distict").append("<option value=>Select Distict</option>");
                    $.each(res.value, function(i, item) {
                        $("#distict").append(
                            $("<option>", {
                                value: item.district_id,
                                text: item.district_name,
                            })
                        );
                    });
                    $("#distict").val(districtId);

                    $("#distict").selectpicker("refresh");
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
            },
        });
    }
}
//college list
function getcollegeForDistrict(url, districtId, collegeId = "") {
    // $('#distict').empty();
    //  $( '#distict').selectpicker( 'refresh' );
    if (districtId != undefined) {
        var csrf = csrf_token;
        var data = {
            id: districtId,
            csrf_token: csrf,
        };
        $.ajax({
            method: "POST",
            url: baseUrl + url,
            data: data,
            beforeSend: function() {},
            success: function(res) {
                res = JSON.parse(res);

                csrf_token = res.csrf_token;
                if (res.code == 200) {
                    $("#collegeDa").append('<option value="">Select college</option>');
                    $("#collegeDa").append('<option value="others">Others</option>');
                    $.each(res.value, function(i, item) {
                        $("#collegeDa").append(
                            $("<option>", {
                                value: item.college_id,
                                text: item.college_name,
                            })
                        );
                    });
                    $("#collegeDa").val(collegeId);

                    $("#collegeDa").selectpicker("refresh");
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
            },
        });
    }
}
$(window).on("load", function() {
    //     var stateId = $('#stateId').val();
    //     var districtId = $('#districtId').val();
    //     var collegeId = $('#collegeId').val();
    //     if(stateId != undefined || stateId != '') {
    //         getDistrictForState('req/getdistrictbystate',stateId,districtId)
    //     }
    //     if(districtId != undefined && districtId != '') {
    //         getcollegeForDistrict('req/getcollegebydistrict', districtId, collegeId);
    //     }
});

$("#mymodal-csv").on("hidden.bs.modal", function(e) {
    $("#file_error").text("");
    // Invoke your server side code here.
});

function hideSmallLoader() {
    $("img").load(function() {
        $(".sm-loader").hide();
    });
}

function numericOnly(elementRef) {
    var keyCodeEntered = event.which ?
        event.which :
        window.event.keyCode ?
        window.event.keyCode :
        -1;

    if (keyCodeEntered >= 48 && keyCodeEntered <= 57) {
        return true;
    }

    // '.' decimal point...
    else if (keyCodeEntered == 46) {
        // Allow only 1 decimal point ('.')...

        if (elementRef.value && elementRef.value.indexOf(".") >= 0) return false;
        else return true;
    }

    return false;
}