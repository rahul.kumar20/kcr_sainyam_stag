//-----------------------------------------------------------------------

$('img').load(function(){
    $('.form-profile-pic-wrapper').removeClass('loader');
 });
                                                /**
 * @name deleteUser
 * @description This method is used to show delete user modal.
 *
 */

function acceptuser( type, status, id, url, msg ) {

    $( '#new_status' ).val( status );
    $( '#new_id' ).val( id );
    $( '#new_url' ).val( url );
    $( '.modal-para' ).text( msg );
    $( '#for' ).val( type );
    if (status == 3) {
        $( '.modal-title' ).text( 'Reject Request' );
        $('#action').text('Reject')
    } else {
        $( '.modal-title' ).text( 'Accept Request' );
        $('#action').text('Accept')
    }
    $( '#myModal-accept' ).modal( 'show' );
    }

    /**
 * @name changeStatusToBlock
 * @description This method is used to block the user.
 *
 */

function changeStatusToAccept( type, status, id, url ) {

    $.ajax( {
        method: "POST",
        url: baseUrl + url,
        data: { type: type, new_status: status, id: id, csrf_token: csrf_token },
        beforeSend: function () {
            $( '#pre-page-loader' ).fadeIn();
            $( '#myModal-block' ).modal( 'hide' );
        },
        success: function ( res ) {
            $( '#pre-page-loader' ).fadeOut();
            res = JSON.parse( res );
            csrf_token = res.csrf_token;
                if(type == 'profileupdateaccept') {
                    $( '.alertText' ).text( string.accept_success );
                    $( '.alertType' ).text( string.success );
                    $( '#unblock_' + res.id ).show();
                    $( '#block_' + res.id ).hide();
                    $( '#status_' + res.id ).empty().text( 'Blocked' );
                    window.location.href='admin/profileUpdateRequest';
            } else {
                if ( res.code === 200 ) {
                    if ( status == 2 ) {
                        $( '.alertText' ).text( string.block_success );
                        $( '.alertType' ).text( string.success );
                        $( '#unblock_' + res.id ).show();
                        $( '#block_' + res.id ).hide();
                        $( '#status_' + res.id ).empty().text( 'Blocked' );
                    }
                    else {
                        $( '.alertText' ).text( string.unblock_success );
                        $( '.alertType' ).text( string.success );
                        $( '#block_' + res.id ).show();
                        $( '#unblock_' + res.id ).hide();
                        $( '#status_' + res.id ).empty().text( 'Active' );
                    }
                    window.location.href='admin/profileUpdateRequest';
                }
                else if ( res.code === 202 ) {
                    $( '.alertText' ).text( res.msg.text );
                    $( '.alertType' ).text( res.msg.type );
                }
                $( '.alert-success' ).fadeIn().fadeOut( 5000 );
            }
     
           
        },
        error: function ( xhr ) {
            alert( "Error occured.please try again" );
            $( '#pre-page-loader' ).fadeOut();
        }
    } );
}