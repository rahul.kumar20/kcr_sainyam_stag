//custom validation
$.validator.addMethod(
    "validDate",
    function(value, element) {
        // put your own logic here, this is just a (crappy) example
        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    },
    string.invalid_date
);

$("#task_add_form").validate({
    errorClass: "alert-danger",
    rules: {
        taskName: {
            required: true,
            maxlength: 255,
        },
        url: {
            required: {
                depends: function() {
                    if (
                        $("#taskType").val() == "1" ||
                        ($("#taskType").val() == "2" &&
                            ($("#actiontwit").val() != 4 || $("#actiontwit").val() != 5))
                    ) {
                        return true;
                    }
                },
            },
            url: true,
        },
        startDate: {
            required: true,
        },
        endDate: {
            required: true,
        },
        taskPoints: {
            required: true,
            digits: true,
        },
        taskType: {
            required: true,
        },
        faction: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "1") {
                        return true;
                    }
                },
            },
        },
        taction: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "2") {
                        return true;
                    }
                },
            },
        },
        whatsappaction: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "4") {
                        return true;
                    }
                },
            },
        },
        instagramaction: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "8") {
                        return true;
                    }
                },
            },
        },
        offlineaction: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "3") {
                        return true;
                    }
                },
            },
        },
        youtbaction: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "5") {
                        return true;
                    }
                },
            },
        },
        onlineform: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "6") {
                        return true;
                    }
                },
            },
        },
        no_of_time: {
            required: {
                depends: function() {
                    if ($("#taskType").val() == "6") {
                        return true;
                    }
                },
            },
            digits: true,
        },
        status: {
            required: true,
        },
        taskDescription: {
            required: true,
            maxlength: 125,
        },
        videoId: {
            required: {
                depends: function() {
                    if (
                        $("#actionyoutube").val() == "1" ||
                        $("#actionyoutube").val() == "9"
                    ) {
                        return true;
                    }
                },
            },
        },
        channelId: {
            required: {
                depends: function() {
                    if ($("#actionyoutube").val() == "10") {
                        return true;
                    }
                },
            },
        },
        twitterAccountName: {
            required: {
                depends: function() {
                    if ($("#actiontwit").val() == "4") {
                        return true;
                    }
                },
            },
        },
        twiiterAccountID: {
            required: {
                depends: function() {
                    if ($("#actiontwit").val() == "4") {
                        return true;
                    }
                },
            },
            digits: true,
        },
        radius: {
            required: {
                depends: function() {
                    if ($("#actionoffline").val() == "event") {
                        return true;
                    }
                },
            },
        },
        mediatype: {
            required: {
                depends: function() {
                    if ($("#actionoffline").val() == "8") {
                        return true;
                    }
                },
            },
        },
        instruction_description: {
            required: false,
            maxlength: 150,
        },
        facebookAccountName: {
            required: {
                depends: function() {
                    if ($("#actionface").val() == "4") {
                        return true;
                    }
                },
            },
        },
        facebookAccountID: {
            required: {
                depends: function() {
                    if ($("#actionface").val() == "4") {
                        return true;
                    }
                },
            },
            digits: true,
        },
    },
    messages: {
        taskName: {
            required: string.task_req,
            maxlength: string.task_title_maxlength,
        },
        url: {
            required: string.task_url_req,
            url: string.invalid_url,
        },
        startDate: {
            required: string.task_start_date_req,
        },
        endDate: {
            required: string.task_end_date_req,
        },
        taskPoints: {
            required: string.task_points_req,
        },
        taskType: {
            required: string.task_type_req,
        },
        faction: {
            required: string.action_req,
        },
        status: {
            required: string.task_status_req,
        },
        taskDescription: {
            required: string.task_desc_req,
            maxlength: string.task_desc_limit,
        },
        videoId: {
            required: string.video_id_req,
        },
        channelId: {
            required: string.channel_id_req,
        },
        redius: {
            required: string.radius_id_req,
        },
        mediatype: {
            required: string.media_id_req,
        },
    },
    submitHandler: function(form) {
        // if ($('.whts').find(":selected").val() == 6 && $('#taskType').val() == 4) {
        //     if (document.getElementById("upload-img").files.length == 0) {
        //         $('#uid').focus();
        //         $('.img_error').show().html('Please select image').delay(5000).fadeOut();
        //         return false
        //     }
        // }
        if (
            $("#actionoffline").find(":selected").val() == "event" &&
            $("#taskType").val() == 3
        ) {
            var latitude = $("#user_lat").val();
            if (latitude != undefined && latitude.length == "") {
                $("#location").focus();
                $(".map_error")
                    .show()
                    .html("Please allow your location from browser")
                    .delay(5000)
                    .fadeOut();
                return false;
            }
        }
        if (
            $("#actionoffline").find(":selected").val() == "8" &&
            $("#taskType").val() == 3
        ) {
            if ($("#mediaType").find(":selected").val() == "1") {
                if (document.getElementById("upload-img").files.length == 0) {
                    $("#task_name").focus();
                    $(".img_error")
                        .show()
                        .html("Please select image")
                        .delay(5000)
                        .fadeOut();
                    return false;
                }
            }
        }
        form.submit();
        $("body").addClass("loader-wrap");
        $(".loader-img").show();
        $("#add_task").attr("disabled", true);
    },
});

/**
 * Delete Image
 * @param {*} id
 */
function deleteUploadedImg(id) {
    var deleteImg = $("#" + id).attr("data-id");
    var deletedImageIds = $("#deleteImgId").val();
    if (deletedImageIds == "") {
        deletedImageIds = deleteImg;
    } else {
        deletedImageIds = deletedImageIds + "," + deleteImg;
    }

    $("#deleteImgId").val(deletedImageIds);
    console.log(deletedImageIds);
    $("#" + id).remove();
    var count = $("#imageCnt").val();
    if (count > 0) {
        count = count - 1;
        $("#imageCnt").val(count);
    }
}

//-----add custom validation for name------------------------------

$.validator.addMethod(
    "lettersonlys",
    function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    },
    "Letters only please"
);

/**
 * @function numeralsOnly
 *  allow only numeber enter
 *  in input box
 *
 * @param {type} evt
 * @returns {Boolean}
 */
function numeralsOnly(evt) {
    evt = evt ? evt : event;
    var charCode = evt.charCode ?
        evt.charCode :
        evt.keyCode ?
        evt.keyCode :
        evt.which ?
        evt.which :
        0;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

/**
 *
 *@function lettersOnly
 *allow only letters
 *
 * @param {type} evt
 * @returns {Boolean}
 */
function lettersOnly(evt) {
    evt = evt ? evt : event;
    var charCode = evt.charCode ?
        evt.charCode :
        evt.keyCode ?
        evt.keyCode :
        evt.which ?
        evt.which :
        0;
    if (
        charCode > 31 &&
        (charCode < 65 || charCode > 90) &&
        (charCode < 97 || charCode > 122)
    ) {
        alert("Enter letters only.");
        return false;
    }
    return true;
}
// $('#startDate' ).datepicker({});
// $('#endDate' ).datepicker({});
function formatDate(date) {
    function pad(s) {
        return s < 10 ? "0" + s : s;
    }
    var d = new Date(date);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("/");
}
var method = $("#method").val();
$(function() {
    var dated = new Date();
    dated.setDate(dated.getDate() - 1);
    // dated = formatDate(dated)
    $(".start_date").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        // defaultDate: date,
        minDate: dated,
    });
    $(".end_date").datetimepicker({
        format: "DD-MM-YYYY HH:mm:ss",
        useCurrent: false, //Important! S
    });

    $(".start_date").on("dp.change", function(e) {
        $(".end_date").data("DateTimePicker").minDate(e.date);
    });
    $(".end_date").on("dp.change", function(e) {
        $(".start_date").data("DateTimePicker").maxDate(e.date);
    });

    $('#distict').change(function() {
        console.log(baseUrl, ' baseUrl here');
        console.log($(this).val(), ' selected val here');
        $.ajax({
            method: "POST",
            url: baseUrl + 'get-district-acs',
            data: {
                district_id: $(this).val()
            },
            success: function(res) {
                $("#ac").html(res);
                $("#ac").selectpicker("refresh");
            }
        });
    })

});
//task type change
$("#taskType").change(function() {
    var taskType = this.value;
    $("#defaultAction").hide();
    $("#actiondefault").val("");
    $("#actiondefault").selectpicker("refresh");
    //facebook type
    if (taskType == 1) {
        $("#url").attr("readonly", false);
        $("#url").val("");
        $("#actionface").prop("disabled", false);
        $("#actionface").val("");
        $("#actionface").selectpicker("refresh");
        $("#twitterAction,#onlineAction,#onlineActionForm,.whatsappUpload").hide();
        $(
            "#youtubeAction,#channelId,#videoId,#offlineAction,#whatsappAction,#instagramAction,.video_upload,.image_upload,.numberform"
        ).hide();
        $("#facebookAction,.post_url").show();
        $(".mapdiv,.audio,.youtube_div").addClass("hideinput");
        $("#offlineMediaType").hide();
        $("#mediaType").val("");
        $(".twitter_follow_div").addClass("hideinput");
        $("#urltext").text("Facebook Post Link");
        $("#no_of_time").val("");
    } else if (taskType == 2) {
        $("#url").attr("readonly", false);
        $("#url").val("");
        //twitter type
        $("#actiontwit").val("");
        $("#actiontwit").selectpicker("refresh");
        $("#facebookAction,#onlineAction,#onlineActionForm,.whatsappUpload").hide();
        $(
            "#youtubeAction,#channelId,#videoId,#offlineAction,#whatsappAction,#instagramAction,.video_upload,.image_upload,.numberform"
        ).hide();
        $("#twitterAction,.post_url").show();
        $(".mapdiv,.audio,.youtube_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#offlineMediaType").hide();
        $("#mediaType").val("");
        $("#urltext").text("Twitter Post Intent URL");
        $("#no_of_time").val("");
    } else if (taskType == 4) {
        //whatsapp type
        $("#actionwhatsapp").val("");
        $("#actionwhatsapp").selectpicker("refresh");
        $("#facebookAction,#onlineAction,#instagramAction").hide();
        $(
            "#twitterAction,#offlineAction,#youtubeAction,.post_url,#onlineActionForm,.numberform,.whatsappUpload"
        ).hide();
        $("#whatsappAction").show();
        $(".mapdiv,.audio,.youtube_div").addClass("hideinput");
        $("#offlineMediaType").hide();
        $("#mediaType").val("");
        $(".twitter_follow_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#no_of_time").val("");
    } else if (taskType == 8) {
        //instagram type
        $("#actioninstagram").val("");
        $("#actioninstagram").selectpicker("refresh");
        $("#facebookAction,#onlineAction,#whatsappAction").hide();
        $(
            "#twitterAction,#offlineAction,#youtubeAction,.post_url,#onlineActionForm,.numberform,.whatsappUpload"
        ).hide();
        $("#instagramAction").show();
        $(".mapdiv,.audio,.youtube_div").addClass("hideinput");
        $("#offlineMediaType").hide();
        $("#mediaType").val("");
        $(".twitter_follow_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#no_of_time").val("");
        $("#urltext").text("Instagram Post Link");
    } else if (taskType == 5) {
        //youtube action
        $("#actionwhatsapp").val("");
        $("#actionwhatsapp").selectpicker("refresh");
        $("#actioninstagram").val("");
        $("#actioninstagram").selectpicker("refresh");
        $("#facebookAction").hide();
        $("#twitterAction,#onlineAction,#onlineActionForm,.whatsappUpload").hide();
        $(
            "#whatsappAction,#instagramAction,.video_upload,.image_upload,#offlineAction,.post_url,.numberform"
        ).hide();
        $(".mapdiv,.audio").addClass("hideinput");
        $("#offlineMediaType").hide();
        $("#mediaType").val("");
        $("#youtubeAction").show();
        $("#offlineMediaType").hide();
        $("#upload-video,#audioinput").val("");
        $("#video_here").attr("src", "");
        $(".twitter_follow_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#no_of_time").val("");
    } else if (taskType == 3) {
        //offline
        $(
            "#facebookAction,.video_upload,.image_upload,.post_url,#onlineAction,#onlineActionForm,.numberform,.whatsappUpload"
        ).hide();
        $("#actionoffline").val("");
        $("#actionoffline").selectpicker("refresh");
        $("#twitterAction,#youtubeAction").hide();
        $("#whatsappAction").hide();
        $("#instagramAction").hide();
        $("#offlineAction").show();
        $(".youtube_div").addClass("hideinput");
        $(".twitter_follow_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#no_of_time").val("");
        //$('.mapdiv,.audio').removeClass('hideinput');
    } else if (taskType == 6) {
        $("#onlineAction,#onlineActionForm").show();
        $("#no_of_time").val("");
        $(".post_url,.numberform").show();
        $("#url").attr("readonly", true);

        //online
        $("#facebookAction,.video_upload,.image_upload,.whatsappUpload").hide();
        $("#actionoffline").val("");
        $("#actionoffline").selectpicker("refresh");
        $("#twitterAction,#youtubeAction").hide();
        $("#whatsappAction").hide();
        $("#instagramAction").hide();
        $("#offlineAction").hide();
        $(".youtube_div").addClass("hideinput");
        $(".twitter_follow_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#offlineMediaType").hide();
        $("#offlineAction").hide();
        $("#urltext").text("Form URL");
    } else if (taskType == 7) {
        $(
            "#facebookAction,.video_upload,.image_upload,.post_url,#onlineAction,#onlineActionForm,.numberform,.whatsappUpload"
        ).hide();
        $("#twitterAction,#youtubeAction").hide();
        $("#whatsappAction").hide();
        $("#instagramAction").hide();
        $("#offlineAction").hide();
        $(".youtube_div").addClass("hideinput");
        $(".twitter_follow_div").addClass("hideinput");
        $(".facebook_follow_div").addClass("hideinput");
        $("#no_of_time").val("");
        $("#defaultAction").show();
    }
});

//offline task
$("#actionyoutube").change(function() {
    var youtubeaction = this.value;
    if (
        (youtubeaction != undefined && youtubeaction == 1) ||
        youtubeaction == 9 ||
        youtubeaction == 2
    ) {
        $(".youtube_div,.youtube_video_id").removeClass("hideinput");
        $(".youtube_div,.youtube_video_id").removeClass("hideinput");
        $(".youtube_channel_id").addClass("hideinput");
    } else if (youtubeaction != undefined && youtubeaction == 10) {
        $(".youtube_video_id").addClass("hideinput");
        $(".youtube_div,.youtube_channel_id").removeClass("hideinput");
    } else {
        $(".youtube_div,.youtube_channel_id,.youtube_video_id").removeClass(
            "hideinput"
        );
    }
});
//offline task
$("#actionoffline").change(function() {
    $("#mediaType").val("");
    $("#mediaType").selectpicker("refresh");
    var offlinection = this.value;
    if (offlinection != undefined && offlinection == "event") {
        $(".mapdiv").removeClass("hideinput");
        $("#offlineMediaType").hide();
    } else if (offlinection != undefined && offlinection == 8) {
        $(".mapdiv").addClass("hideinput");
        $("#offlineMediaType").show();
    } else {
        $("#offlineMediaType").hide();
        $(".mapdiv").addClass("hideinput");
        $(".image_upload").hide();
        $(".video_upload,.audio").hide();
    }
});
//offline task
$("#mediaType").change(function() {
    var mediaType = this.value;
    if (mediaType != undefined && mediaType == "1") {
        $(".image_upload").show();
        $(".video_upload,.audio").hide();
    } else if (mediaType != undefined && mediaType == 2) {
        $(".video_upload").show();
        $(".image_upload,.audio").hide();
    } else if (mediaType != undefined && mediaType == 3) {
        $(".audio").show();
        $(".image_upload,.video_upload").hide();
    }
});

//whatsapp task
$(".whts").change(function() {
    var mediaType = this.value;
    $(
        ".image_upload, .video_upload, .whatsappUpload, .audio, .post_url, .post_content_div"
    ).hide();
    if (mediaType != undefined && (mediaType == 20 || mediaType == 18)) {
        $(".image_upload").show();
    } else if (mediaType != undefined && mediaType == 15) {
        $(".video_upload").show();
    } else if (mediaType != undefined && mediaType == 16) {
        $(".audio").show();
    } else if (mediaType != undefined && mediaType == 17) {
        $(".post_content_div").show();
    } else if (mediaType != undefined && mediaType == 19) {
        $(".post_url").show();
        $("#urltext").text("Post Link");
    } else if (mediaType != undefined && mediaType == 6) {
        $(".whatsappUpload").show();
        $(".image_upload").show();
    }
});

//whatsapp task
$(".insta").change(function() {
    var mediaType = this.value;
    $(".post_url").hide();
    $(
        ".image_upload, .video_upload, .whatsappUpload, .audio, .post_url, .post_content_div"
    ).hide();
    if (mediaType != undefined && (mediaType == 20 || mediaType == 18)) {
        $(".image_upload").show();
    } else if (mediaType != undefined && mediaType == 15) {
        $(".video_upload").show();
    } else if (mediaType != undefined && mediaType == 16) {
        $(".audio").show();
    } else if (mediaType != undefined && mediaType == 17) {
        $(".post_content_div").show();
    } else if (mediaType != undefined && mediaType == 19) {
        $(".post_url").show();
        $("#urltext").text("Post Link");
    } else if (mediaType != undefined && mediaType == 6) {
        $(".whatsappUpload").show();
        $(".image_upload").show();
    } else if (mediaType != undefined && (mediaType == 1 || mediaType == 9)) {
        $(".post_url").show();
    }
});

//twitter task
$("#actiontwit").change(function() {
    var type = this.value;
    $(".post_content_div").hide();
    if (type != undefined && type == "4") {
        $(".twitter_follow_div").removeClass("hideinput");
        $(".post_url").hide();
    } else if (type != undefined && type == "5") {
        $(".post_url").hide();
    } else if (type != undefined && type == "9") {
        $(".post_url").show();
        $(".post_content_div").show();
    } else {
        $(".twitter_follow_div").addClass("hideinput");
        $(".post_url").show();
    }
});

//Facebook Task
$("#actionface").change(function() {
    var facetype = this.value;
    $(".image_upload, .video_upload, .post_content_div").hide();
    $(".facebook_follow_div").addClass("hideinput");
    if (facetype != undefined && facetype == "9") {
        $(".post_url").show();
        $(".post_content_div").show();
    } else if (facetype != undefined && facetype == "22") {
        $(".post_url").hide();
        $(".post_content_div").show();
        $(".image_upload").show();
        $(".video_upload").show();
    } else if (facetype != undefined && (facetype == "1" || facetype == "2")) {
        $(".post_url").show();
    } else if (facetype != undefined && facetype == "4") {
        $(".facebook_follow_div").removeClass("hideinput");
        $(".post_url").hide();
    }
});

//Facebook Task
$("#actiondefault").change(function() {
    var actiondefault = this.value;
    $(".image_upload, .video_upload, .post_content_div").hide();
    $(".facebook_follow_div").addClass("hideinput");
    $(".twitter_follow_div").addClass("hideinput");
    $(".youtube_div,.youtube_video_id").addClass("hideinput");
    $(".youtube_channel_id").addClass("hideinput");
    if (actiondefault != undefined && actiondefault == "28") {
        $(".facebook_follow_div").removeClass("hideinput");
    } else if (actiondefault != undefined && actiondefault == "27") {
        $(".twitter_follow_div").removeClass("hideinput");
    } else if (actiondefault != undefined && actiondefault == "29") {
        $(".youtube_video_id").addClass("hideinput");
        $(".youtube_div,.youtube_channel_id").removeClass("hideinput");
    }
});
/*
 * Apply Filter User
 */
$(".applyFilterTak").click(function() {
    var filter = {};
    filter = $("#filterVal").val();
    var pageUrl = $("#pageUrl").val();

    filter = JSON.parse(filter);

    delete filter["status"];
    delete filter["startDate"];
    delete filter["endDate"];
    delete filter["LivestartDate"];
    delete filter["LiveendDate"];
    delete filter["uid"];
    // delete filter["state"];
    delete filter["distict"];
    delete filter["gender"];
    delete filter["taskCompleted"];
    delete filter["college"];
    delete filter["taskType"];
    delete filter["taskAct"];

    var status = $(".status").find(":selected").val();
    var uid = $(".uid").find(":selected").val();
    //var state = $(".state").find(":selected").val();
    var distict = $(".distict").find(":selected").val();
    var gender = $(".gender").find(":selected").val();
    var college = $(".college").find(":selected").val();
    var taskType = $(".taskType").find(":selected").val();
    var taskAct = $(".taskAct").find(":selected").val();

    var startDate = $(".startDate").val();
    var endDate = $(".endDate").val();
    var LivestartDate = $(".LivestartDate").val();
    var LiveendDate = $(".LiveendDate").val();
    var taskCompleted = $(".taskCompleted").find(":selected").val();
    //status
    if (uid != undefined && status.length != 0) {
        filter["status"] = status;
    }
    //UID
    if (uid != undefined && uid.length != 0) {
        filter["uid"] = uid;
    }
    //state
    // if (state != undefined && state.length != 0) {
    //     filter["state"] = state;
    // }
    //district
    if (distict != undefined && distict.length != 0) {
        filter["distict"] = distict;
    }
    //gender
    if (gender != undefined && gender.length != 0) {
        filter["gender"] = gender;
    }
    //college
    if (college != undefined && college.length != 0) {
        filter["college"] = college;
    }
    //start date
    if (startDate != undefined && startDate.length != 0) {
        filter["startDate"] = startDate;
    }

    //end date
    if (endDate != undefined && endDate.length != 0) {
        filter["endDate"] = endDate;
    }
    //livestart date
    if (LivestartDate != undefined && LivestartDate.length != 0) {
        filter["LivestartDate"] = LivestartDate;
    }

    //liveend date
    if (LiveendDate != undefined && LiveendDate.length != 0) {
        filter["LiveendDate"] = LiveendDate;
    }
    //task complete
    if (taskCompleted != undefined && taskCompleted.length != 0) {
        filter["taskCompleted"] = taskCompleted;
    }
    //task type
    if (taskType != undefined && taskType.length != 0) {
        filter["taskType"] = taskType;
    }

    //task Action
    if (taskAct != undefined && taskAct.length != 0) {
        filter["taskAct"] = taskAct;
    }
    if (!status.length &&
        !uid.length &&
        //!state.length &&
        !distict.length &&
        !gender.length &&
        !startDate.length &&
        !endDate.length &&
        !LivestartDate.length &&
        !LiveendDate.length &&
        !taskCompleted.length &&
        !college.length &&
        !taskType.length &&
        !taskAct.length
    ) {
        console.log("Not any filter selected");
        return;
    }
    var filterLen = $.keyCount(filter);
    if (filterLen == 0) {
        alert("Please select a filter");
        return false;
    }

    //var queryParams = $.param(filter)
    //window.location.href = pageUrl + '?' + queryParams;

    var queryParams = JSON.stringify(filter);
    window.location.href = pageUrl + "?data=" + window.btoa(queryParams);
});

$("#startDate,#endDate").keypress(function(event) {
    return (event.keyCode || event.which) === 9 ? true : false;
});
$("#startDate,#endDate").bind("cut copy paste", function(e) {
    e.preventDefault();
});

$("#user_rewardss").validate({
    errorClass: "alert-danger",
    rules: {
        userRegistrationNumber: {
            required: true,
            maxlength: 255,
        },
    },
    messages: {
        userRegistrationNumber: {
            required: string.task_req,
            maxlength: string.task_title_maxlength,
        },
    },
    submitHandler: function(form) {
        form.submit();
        $("body").addClass("loader-wrap");
        $(".loader-img").show();
        $("#add_task").attr("disabled", true);
    },
});

function changeStatusToAccept(type, status, id, url, tid) {
    $.ajax({
        method: "POST",
        url: baseUrl + url,
        data: {
            type: type,
            new_status: status,
            id: id,
            csrf_token: csrf_token,
            tid: tid,
        },
        beforeSend: function() {
            $("#pre-page-loader").fadeIn();
            $("#myModal-block").modal("hide");
        },
        success: function(res) {
            $("#pre-page-loader").fadeOut();
            res = JSON.parse(res);
            csrf_token = res.csrf_token;
            if (type == "profileupdateaccept") {
                $(".alertText").text(string.accept_success);
                $(".alertType").text(string.success);
                $("#unblock_" + res.id).show();
                $("#block_" + res.id).hide();
                $("#status_" + res.id)
                    .empty()
                    .text("Blocked");
                location.reload();
            } else {
                if (res.code === 200) {
                    if (status == 2) {
                        $(".alertText").text(string.block_success);
                        $(".alertType").text(string.success);
                        $("#unblock_" + res.id).show();
                        $("#block_" + res.id).hide();
                        $("#status_" + res.id)
                            .empty()
                            .text("Blocked");
                    } else {
                        $(".alertText").text(string.unblock_success);
                        $(".alertType").text(string.success);
                        $("#block_" + res.id).show();
                        $("#unblock_" + res.id).hide();
                        $("#status_" + res.id)
                            .empty()
                            .text("Active");
                    }
                    location.reload();
                } else if (res.code === 202) {
                    $(".alertText").text(res.msg.text);
                    $(".alertType").text(res.msg.type);
                }
                $(".alert-success").fadeIn().fadeOut(5000);
            }
        },
        error: function(xhr) {
            alert("Error occured.please try again");
            $("#pre-page-loader").fadeOut();
        },
    });
}
$(document).ready(function() {
    var taskActionValData = $("#taskActionValue").val();
    var taskTypeValue = $("#taskTypeValue").val();

    if (taskActionValData != undefined || taskActionValData != "") {
        taskActionChange(taskTypeValue, taskActionValData);
    }
}); //for index filter
function taskActionChange(value, taskActionValData = "") {
    var taskType = value;
    var selectAction = "";
    $("#task_action").empty();
    $("#task_action").append('<option value="" ' + "selected" + ">All</option>");
    if (taskType != undefined && taskType != "") {
        if (taskType == "facebook" || taskType == "instagram") {
            if (taskActionValData == "like" ||
                taskActionValData == "share" ||
                taskActionValData == "post" ||
                taskActionValData == "follow" ||
                taskActionValData == "comment"
            ) {
                selectAction = "selected";
            }
            $("#task_action").append(
                '<option value="like" ' + selectAction + ">Like</option>"
            );
            $("#task_action").append(
                '<option value="share" ' + selectAction + ">Share</option>"
            );
            $("#task_action").append(
                '<option value="post" ' + selectAction + ">Post</option>"
            );
            $("#task_action").append(
                '<option value="follow" ' + selectAction + ">Follow</option>"
            );
            $("#task_action").append(
                '<option value="comment" ' + selectAction + ">Comment</option>"
            );
        } else if (taskType == "twitter") {
            if (
                taskActionValData == "like" ||
                taskActionValData == "comment" ||
                taskActionValData == "tweet" ||
                taskActionValData == "retweet" ||
                taskActionValData == "poll_retweet" ||
                taskActionValData == "follow"
            ) {
                selectAction = "selected";
            }
            $("#task_action").append(
                '<option value="like" ' + selectAction + ">Like</option>"
            );
            $("#task_action").append(
                '<option value="comment" ' + selectAction + ">Comment</option>"
            );
            $("#task_action").append(
                '<option value="tweet" ' + selectAction + ">Tweet</option>"
            );
            $("#task_action").append(
                '<option value="retweet" ' + selectAction + ">Retweet</option>"
            );
            $("#task_action").append(
                '<option value="poll_retweet" ' + selectAction + ">Poll&Retweet</option>"
            );
            $("#task_action").append(
                '<option value="follow" ' + selectAction + ">Follow</option>"
            );
        } else if (taskType == "offline") {
            if (
                taskActionValData == "event" ||
                taskActionValData == "downlord" ||
                taskActionValData == "qrcode" ||
                taskActionValData == "image" ||
                taskActionValData == "video"

            ) {
                selectAction = "selected";
            }
            $("#task_action").append(
                '<option value="event" ' + selectAction + ">Event</option>"
            );
            $("#task_action").append(
                '<option value="downlord media" ' + selectAction + ">Multimedia</option>"
            );
            $("#task_action").append(
                '<option value="qrcode" ' + selectAction + ">Qrcode</option>"
            );
            $("#task_action").append(
                '<option value="image" ' + selectAction + ">Image</option>"
            );
            $("#task_action").append(
                '<option value="video" ' + selectAction + ">Video</option>"
            );
        } else if (taskType == "youtube") {
            if (
                taskActionValData == "like" ||
                taskActionValData == "comment" ||
                taskActionValData == "subscribe" ||
                taskActionValData == "share"
            ) {
                selectAction = "selected";
            }
            $("#task_action").append(
                '<option value="like" ' + selectAction + ">Like</option>"
            );
            $("#task_action").append(
                '<option value="comment" ' + selectAction + ">comment</option>"
            );
            $("#task_action").append(
                '<option value="share" ' + selectAction + ">Share</option>"
            );
            $("#task_action").append(
                '<option value="subscribe" ' + selectAction + ">Subscribe</option>"
            );
        } else if (taskType == "online") {
            if (taskActionValData == "form") {
                selectAction = "selected";
            }
            $("#task_action").append(
                '<option value="form" ' + selectAction + ">Form</option>"
            );
        } else if (taskType == "whatsapp") {
            if (
                taskActionValData == "set dp" ||
                taskActionValData == "share" ||
                taskActionValData == "create group" ||
                taskActionValData == "about us" ||
                taskActionValData == "image" ||
                taskActionValData == "video" ||
                taskActionValData == "audio" ||
                taskActionValData == "link" ||
                taskActionValData == "text" ||
                taskActionValData == "gif"
            ) {
                selectAction = "selected";
            }
            $("#task_action").append(
                '<option value="set dp" ' + selectAction + ">Change Dp</option>"
            );
            $("#task_action").append(
                '<option value="share" ' + selectAction + ">Share</option>"
            );
            $("#task_action").append(
                '<option value="create group" ' + selectAction + ">Create Group</option>"
            );
            $("#task_action").append(
                '<option value="about us" ' + selectAction + ">About Us</option>"
            );
            $("#task_action").append(
                '<option value="image" ' + selectAction + ">Image</option>"
            );
            $("#task_action").append(
                '<option value="video" ' + selectAction + ">Video</option>"
            );
            $("#task_action").append(
                '<option value="audio" ' + selectAction + ">Audio</option>"
            );
            $("#task_action").append(
                '<option value="link" ' + selectAction + ">Link</option>"
            );
            $("#task_action").append(
                '<option value="text" ' + selectAction + ">Text</option>"
            );
            $("#task_action").append(
                '<option value="gif" ' + selectAction + ">GIF</option>"
            );
        }

        $("#task_action").selectpicker("refresh");
    }
}

function uploadCsvAjax() {
    var file_data = $("#upload-doc").prop("files")[0];
    var form_data = new FormData();
    var csrf = csrf_token;

    form_data.append("file", file_data);
    form_data.append("csrf_token", csrf);
    $.ajax({
        url: baseUrl + "req-uidimport", // point to server-side PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: "post",
        success: function(res) {
            res = JSON.parse(res);
            csrf_token = res.csrf_token;
            if (res.code === 200) {
                $("#mymodal-csv").modal("hide");
                $("#uid").val(res.result);
                $("body").removeClass("loader-wrap");
                $(".loader-img").hide();
            }
        },
    });
}

function hideSmallLoader() {
    $("img").load(function() {
        $(".sm-loader").hide();
    });
}

///////////////////////distict list
function getFormURL(formId, url) {
    // var formId = this.value;
    if (formId != undefined) {
        var form_id = formId;
        var csrf = csrf_token;
        var data = {
            id: form_id,
            csrf_token: csrf,
        };
        $.ajax({
            method: "POST",
            url: baseUrl + url,
            data: data,
            beforeSend: function() {},
            success: function(res) {
                res = JSON.parse(res);

                csrf_token = res.csrf_token;
                if (res.code == 200) {
                    $("#url").val(res.url);
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
            },
        });
    }
}