$(window).load(function () {
    
    
    // if (navigator.geolocation) {
    //     navigator.geolocation.getCurrentPosition(showPosition);
    // } else { 
    //     x.innerHTML = "Geolocation is not supported by this browser.";
    // }
    //     var mylat;
    //     var mylng
	// function showPosition(position) {
	//     mylat = position.coords.latitude;
	// 	mylng = position.coords.longitude;
	//     initMap(mylat,mylng);
	// }
	// initMap2();
	initMap(13.042046, 80.247905);
	$("#address").focus(function () {
		$("#locationModal").modal("show");
                if(mylat == undefined && mylng == undefined){
                    initMap(13.042046, 80.247905);
                }else{
                  initMap(mylat, mylng);    
                }
		
	});
//        $('#step1').click(function(){
//             if(mylat == undefined && mylng == undefined){
//                    initMap1(19.432608, -99.133209);
//                }else{
//                  initMap1(mylat, mylng);    
//                }
//        })
	
	$('#locationModal').on('hidden.bs.modal', function (e) {
		$("#address").val($("#location").val());
	});
	
	$('#locationModal').on('shown.bs.modal', function () {
		google.maps.event.trigger(map, "resize");
		var currentLocation = marker.getPosition();
		map.panTo(currentLocation);
	});
});
	var map;
	var marker = false;
	var geocoder;
	

	function initMap(lat, long) {
	   var centerOfMap = new google.maps.LatLng(lat, long);
	   var mapProp = {
		   center: centerOfMap,
		   zoom: 8,
		   draggable: true
	   };
	   var input = document.getElementById('location');
	   var autocomplete = new google.maps.places.Autocomplete(input);

	   // Restrict to India
	   autocomplete.setComponentRestrictions({'country': ['in']});

	   map = new google.maps.Map(document.getElementById("map_location"), mapProp);
	   geocoder = new google.maps.Geocoder();
	   marker = new google.maps.Marker({
		   position: centerOfMap,
		   map: map,
		   draggable: true
	   });

	   google.maps.event.addListener(autocomplete, 'place_changed', function () {

		   var place = autocomplete.getPlace();

		   latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

		   map.setCenter(latlng);

		   map.setZoom(8);

		   marker.setPosition(latlng);
		   // Update current position info.
		   //updateMarkerPosition(latlng);
		   //updateMarkerAddress($('#eventLocation').val());
		   markerLocation();
	   });

	   markerLocation();
	   google.maps.event.addListener(marker, 'dragend', function (event) {
		   markerLocation();
	   });
	   
		  var pacContainerInitialized = false;
				  $('#location').keypress(function() {
				 if (!pacContainerInitialized) {
				 $('.pac-container').css('z-index', '9999');
				 pacContainerInitialized = true;
				 }
		  });

	}
      
  
   $('#marker').click(function () {
       $('.map-box').slideUp();
   })

   function resizeMap() {
       if (typeof map == "undefined")
           return;
       setTimeout(function () {
           resizingMap();
       }, 400);
   }

   function resizingMap() {
       if (typeof map == "undefined")
           return;
       var center = map.getCenter();
       google.maps.event.trigger(map, "resize");
       map.setCenter(center);
   }
//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
   function markerLocation() {
       var currentLocation = marker.getPosition();
       document.getElementById('user_lat').value = currentLocation.lat(); //latitude
       document.getElementById('user_long').value = currentLocation.lng(); //latitude
       //~ google.maps.event.addListener(marker, 'dragend', function(event){
       geocodePosition(currentLocation);
       //~ });

   }
//Load the map when the page has finished loading.
//~ google.maps.event.addDomListener(window, 'load', initMap);

   function geocodePosition(pos) {
       geocoder.geocode({
           latLng: pos
       }, function (responses) {
           if (responses && responses.length > 0) {
				count = 0;
				for (var i=0; i<responses[0].address_components.length; i++)
				{
					if (responses[0].address_components[i].types[0] == "route")
					{
						route = responses[0].address_components[i];
					}
					if (responses[0].address_components[i].types[0] == "administrative_area_level_2")
					{
						city = responses[0].address_components[i].long_name;
					}
					if (responses[0].address_components[i].types[0] == "sublocality_level_1"||responses[0].address_components[i].types[0] == "political"||responses[0].address_components[i].types[0]=="sublocality")
					{
						sublocality_level_1 = responses[0].address_components[i];
					}
					if (responses[0].address_components[i].types[0] == "locality")
					{
						locality = responses[0].address_components[i].long_name;
					}
					
					if (responses[0].address_components[i].types[0] == "postal_code")
					{
						postalcode= responses[0].address_components[i].long_name;
					}else{
						postalcode= "";
					}
					if (responses[0].address_components[i].types[0] == "country")
					{
						country= responses[0].address_components[i].long_name;
					   
					}
					if (responses[0].address_components[i].types[0] == "administrative_area_level_1")
					{
						count=i;
						state= responses[0].address_components[i].long_name;
						
					}
				}
				var locationValue = document.getElementById('location').value;
				document.getElementById('location').value = responses[0].formatted_address;
                                // document.getElementById('country').value = country;
                                // document.getElementById('postal_code').value = postalcode;
                                // document.getElementById('state').value = state;
           } else {
               document.getElementById('location').value = 'N/A';
           }
       });
   }
