

function addQuestionSection(){
	var countSection = $('.inner-wrapper').find('section').length;

	var getSection = getSectionHtml(countSection);
	var qtype='checkbox';
    $(".inner-question-html").append(getSection)
    $('#questionType'+countSection).val(qtype);
}


function getSectionHtml(countSection){
	
	
			return '      <section class="question-block active" id="question-block'+countSection+'">  '  + 
					 '                           <div class="inner-wrapper">  '  + 
					 '     '  + 
					 '                           <span class="toggle-btn-wrapper filter-side-wrapper">  '  + 
					 '                               <span class="dots"></span>  '  + 
					 '                               <span class="dots"></span>  '  + 
					 '                               <span class="dots"></span>  '  + 
					 '                           </span>                  '  + 
					 '     '  + 
					 '     '  + 
					 '                           <div class="form-option">  '  + 
					 '                              <div class="form-element-col">  '  + 
					 '                                   <ul>  '  + 
					 '                                       <li onclick=getHtmlByList("innerQusType'+countSection+'","checkbox",'+countSection+')><a href="javascript:void(0)"><span class="icon checkbox"></span></a></li>  '  + 
					 '                                       <li onclick=getHtmlByList("innerQusType'+countSection+'","dropdown",'+countSection+')><a href="javascript:void(0)" title="Dropdown"><span class="icon dropdown"></span></a></li>  '  + 
					 '                                       <li onclick=getHtmlByList("innerQusType'+countSection+'","map",'+countSection+')><a href="javascript:void(0)" title="Map"><span class="icon map"></span></a></li>  '  + 
					 '                                       <li onclick=getHtmlByList("innerQusType'+countSection+'","file",'+countSection+')><a href="javascript:void(0)" title="File Upload"><span class="icon fileupload"></span></a></li>  '  + 
					 '                                       <li onclick=getHtmlByList("innerQusType'+countSection+'","date",'+countSection+')><a href="javascript:void(0)" title="Date"><span class="icon datepicker-icon"></span></a></li>  '  + 
					 '                                       <li onclick=getHtmlByList("innerQusType'+countSection+'","time",'+countSection+')><a href="javascript:void(0)" title="Time"><span class="icon timepicker"></span></a></li>  '  + 
					 '                                   </ul>  '  + 
					 '                               </div>  '  + 
					 '                           </div>  '  + 
					 '     '  + 
					 '                              <div class="form-question">  '  + 
					 '                                   <input type="text" placeholder="Question" name="question['+countSection+']">  '  + 
					 '                                   <input type="hidden" id="questionType'+countSection+'" value="" name="questionType['+countSection+']">  '  + 
					 '                               </div> <div id="innerQusType'+countSection+'">  '  + 
					 '     '  + 
						 '                               <div class="flex-row form-option-wrap">  '  + 
						 '                                       <div class="flex-col">  '  + 
						 '                                           <div class="th-checkbox">  '  + 
						 '                                               <input style="display:none;" class="filter-type filled-in"  type="checkbox" name="filter" id="Yes" value="">  '  + 
						 '                                               <label for="Yes" class="lbl-check"><span></span></label>  '  + 
						 '                                           </div>      '  + 
						 '                                       </div>  '  + 
						 '                                       <div class="flex-col answer-col">  '  + 
						 '                                           <input type="text" name="option['+countSection+'][]" class="answerbox" placeholder="Option 1">   '  + 
						 '                                       </div>  '  + 
						 '                               </div>  '  + 
						 '                               </div> '  + 
					 '                               <div class="flex-row">  '  + 
					 '                                       <div class="flex-col">  '  + 
					 '                                       <span class="add-other" id="addother'+countSection+'" onclick=getInnerQusHtml("innerQusType'+countSection+'","'+countSection+'")>Add Option </span>  '  + 
					 '                                   </div>      '  + 
					 '                               </div>  '  + 
					 '     '  + 
					 '                               <div class="flex-row">  '  + 
					 '                                       <div class="flex-col-sm-10">  '  + 
					 '                                               <div class="action-btn-wrap">  '  + 
					 '                                                   <span class="action-btn delete" onclick=removeQusBlock("question-block'+countSection+'") title="Delete"></span>  '  +
					 '<span class="required-filed">'+
					 '<div class="th-checkbox">'+
						 '<input style="display:none;" class="filter-type filled-in"  type="checkbox" name="filter" onclick="setRequireField('+countSection+')" id="Required'+countSection+'" value="">'+
						 '<label for="Required'+countSection+'" class="lbl-check"><span></span>Required</label>'+
						 '<input  class="filter-type filled-in"  type="hidden" name="req['+countSection+']" id="RequiredHid'+countSection+'" value="">'+
					 '</div>'+    
					 '</span>'+ 
					 '                                                   <!--<span class="action-btn duplicate" title="Duplicate"></span>!-->  '  + 
					 '                                               </div>      '  + 
					 '                                       </div>  '  + 
					 '                                 </div>        '  + 
					 '     '  + 
					 '                           </div>      '  + 
					 '                  </section>  ' ; 
	
	}
	
	
	function removeQusBlock(id){
			
			$("#"+id).remove();
	}
	
	
	//get inner html inside question
	
	function getInnerHtml(innerId,type,innerCounter,questionIndex){
		  if(type =="checkbox") {
			  
			 return   '  <div class="flex-row form-option-wrap option'+innerCounter+'" id="attribute'+innerCounter+'">  '  + 
						 '                                       <div class="flex-col">  '  + 
						 '                                           <div class="th-checkbox">  '  + 
						 '                                               <input style="display:none;" class="filter-type filled-in"  type="checkbox" name="filter" id="Yes" value="">  '  + 
						 '                                               <label for="Yes" class="lbl-check"><span></span></label>  '  + 
						 '                                           </div>      '  + 
						 '                                       </div>   '  + 
						 '                                                 '  + 
						 '   		<div class="flex-col answer-col">  '  + 
						 '   			<span class="delete-icon" onclick=removeInnerHtml("attribute'+innerCounter+'","'+innerId+'","'+questionIndex+'") title="Remove"></span>  '  + 
						 '   		<input type="text" name="option['+questionIndex+'][]" class="answerbox" placeholder="Option 1">   '  + 
						 '   	</div>  '  + 
						 '   </div>' ;  
			  
		 }else if(type =="text") {
			 
			 return   '  <div class="flex-row form-option-wrap option'+innerCounter+'" id="attribute'+innerCounter+'">  '  +
						 '                                                 '  + 
						 '   		<div class="flex-col answer-col"> <span class="delete-icon"  onclick=removeInnerHtml("attribute'+innerCounter+'","'+innerId+'","'+questionIndex+'") title="Remove"></span>  '  + 
						 '   		<input type="text" name="option['+questionIndex+'][]" class="answerbox" placeholder="Option 1">   '  + 
						 '   	</div>  '  + 
						 '   </div>' ;
		 }		
			
	}

	function getRemovedInnerHtml(innerId,type,innerCounter,countercheckboxval,countertextval,questionIndex){
		//alert(countercheckboxval);
		if(type =="checkbox") {
			
		   return   '  <div class="flex-row form-option-wrap option'+innerCounter+'" id="attribute'+innerCounter+'">  '  + 
					   '                                       <div class="flex-col">  '  + 
					   '                                           <div class="th-checkbox">  '  + 
					   '                                               <input style="display:none;" class="filter-type filled-in"  type="checkbox" name="filter" id="Yes" value="'+ countercheckboxval +'">  '  + 
					   '                                               <label for="Yes" class="lbl-check"><span></span></label>  '  + 
					   '                                           </div>      '  + 
					   '                                       </div>   '  + 
					   '                                                 '  + 
					   '   		<div class="flex-col answer-col">  '  + 
					   '   			<span class="delete-icon" onclick=removeInnerHtml("attribute'+innerCounter+'","'+innerId+'","'+questionIndex+'") title="Remove"></span>  '  + 
					   '   		<input type="text" name="option['+questionIndex+'][]" class="answerbox" placeholder="Option 1" value="'+ countertextval +'">   '  + 
					   '   	</div>  '  + 
					   '   </div>' ;  
			
	   }else if(type =="text") {
		   
		   return   '  <div class="flex-row form-option-wrap option'+innerCounter+'" id="attribute'+innerCounter+'">  '  +
					   '                                                 '  + 
					   '   		<div class="flex-col answer-col"> <span class="delete-icon"  onclick=removeInnerHtml("attribute'+innerCounter+'","'+innerId+'","'+questionIndex+'") title="Remove"></span>  '  + 
					   '   		<input type="text" name="option['+questionIndex+'][]" class="answerbox" placeholder="Option 1" value="'+ countertextval +'">   '  + 
					   '   	</div>  '  + 
					   '   </div>' ;
	   }	
		  
  }


	function reset_menu(){
		$( ".form-option" ).removeClass( "active" );
	}
	
	
	$("body").on('click', '.filter-side-wrapper', function (e) {
	

		reset_menu();
		e.stopPropagation();
		$( this ).next().addClass("active");
		});
		
		
		$( "body" ).click(function ( e ) {
			if ( !$( e.target ).is( '.form-option *' ) ) {
				$(".form-option").removeClass( "active" );
		   
			 }
	});
	
	// generating the inner html.
	function getInnerQusHtml(innerId,questionIndex){
		
		innerCounter = 0;
		innerCounter = $('#'+innerId+' > div').length;

		if(innerCounter<9){
             $('#addother'+questionIndex).css('display','block');
		}else{
			$('#addother'+questionIndex).css('display','none');
		}
		
		var checkInput = $('#'+innerId).find('input').attr('type');
		
		if(checkInput==undefined){
		
			checkInput = "checkbox";
		}
		var innerHtml  = getInnerHtml(innerId,checkInput,innerCounter,questionIndex);
		$("#"+innerId).append(innerHtml);
		
	}
	
	// remove internal div
	function removeInnerHtml(attributeId,innerId,questionIndex){
		
			if(attributeId !='' && innerId !=""){
				
				$('#'+innerId+' #'+attributeId).remove();
				
				
				var innerCounter = $('#'+innerId+' > div').length;
				var checkInput = $('#'+innerId).find('input').attr('type');
				var checkboxarr=[];
				var textarr=[];

				if(innerCounter<=9) {
					   $('#addother'+questionIndex).css('display','block');
			    }else{
				       $('#addother'+questionIndex).css('display','none');
			    }
				
				$('#'+innerId).find('input').each(function(index,value) {
					if(value.type=="checkbox"){
						checkboxarr.push(value.value);
					}
					if(value.type=="text"){
						textarr.push(value.value);
					}
                });
				//alert(value.value);     
				var newHtml="";
				
				for (var i=0; i<innerCounter;i++) {
				
					 newHtml += getRemovedInnerHtml(innerId,checkInput,i,checkboxarr[i],textarr[i],questionIndex)
				}
				
				$("#"+innerId).html(newHtml);
				
				
			}
			
			
	}
	
	
	// change question html by clicking on the list
	
	function getHtmlByList(innerId,listType,questionIndex){
		
		var innerCounter = $('#'+innerId+' > div').length;
		$('#questionType'+questionIndex).val(listType);
		
		var innerHtml  = generateHtml(innerId,listType,innerCounter,questionIndex);
		$("#"+innerId).html(innerHtml);
		
	}
	
	
	// This method will be used when html will be fetched using ooption list
	
	function generateHtml(innerId,type,innerCounter,questionIndex){
		
		  if(type =="checkbox"){
			  
			 return      '<div class="flex-row form-option-wrap" id="attribute'+innerCounter+'">  '  + 
						 '                                       <div class="flex-col">  '  + 
						 '                                           <div class="th-checkbox">  '  + 
						 '                                               <input style="display:none;" class="filter-type filled-in"  type="checkbox" name="filter" id="Yes" value="">  '  + 
						 '                                               <label for="Yes" class="lbl-check"><span></span></label>  '  + 
						 '                                           </div>      '  + 
						 '                                       </div>   '  + 
						 '                                                 '  + 
						 '   		<div class="flex-col answer-col"> <span class="delete-icon" onclick=removeInnerHtml("attribute'+innerCounter+'","'+innerId+'","'+questionIndex+'") title="Remove"></span> '  + 
						 '   		<input type="text" name="option['+questionIndex+'][]" class="answerbox" placeholder="Option 1">   '  + 
						 '   	</div>  '  + 
						 '</div>' ;  
			  
		 }else if(type =="dropdown"){
			 
			 return   '  <div class="flex-row form-option-wrap" id="attribute'+innerCounter+'">  '  +
						 '                                                 '  + 
						 '   		<div class="flex-col answer-col"> <span class="delete-icon" onclick=removeInnerHtml("attribute'+innerCounter+'","'+innerId+'","'+questionIndex+'") title="Remove"></span>'  + 
						 '   		<input type="text" name="option['+questionIndex+'][]" class="answerbox" placeholder="Option 1">   '  + 
						 '   	</div>  '  + 
						 '   </div>' ;
		 }else if(type=="date"){
			 
			 $('#addother'+questionIndex).css('display','none');
			 
			return   '  <div class="flex-row form-option-wrap">'+
			'<div class="flex-col answer-col">'+
			   '<input type="text" readonly class="answerbox" placeholder="Date" value="date" name="option['+questionIndex+']">'+ 
			'</div>'+
		    '</div>';
		 }else if(type=="time"){
			 $('#addother'+questionIndex).css('display','none');
			 
			return   '<div class="flex-row form-option-wrap">'+
			'<div class="flex-col answer-col">'+
			  '<input type="text" readonly class="answerbox" placeholder="Time"  value="time" name="option['+questionIndex+']">'+ 
			'</div></div>';
		 }else if(type=="map"){
			 
			 $('#addother'+questionIndex).css('display','none');
			 
			return   '<div class="flex-row form-option-wrap">'+
			'<div class="flex-row">'+
			'<div class="flex-col-sm-10">'+
			'<input type="text" readonly name="location" class="answerbox" placeholder="Location">'+ 
			'<figure class="cover-pic-wrapper"><img src= '+baseUrl+'public/admin/images/goglemap.jpg'+'>'+
			'</figure>'+
			'</div></div>';
		}else if(type=="file"){
			
			$('#addother0').css('display','none');
			
			return   '<div class="flex-row form-option-wrap">'+
            '<div class="flex-col">'+
               '<label class="add-file" for="upload-doc">Upload CSV </label>'+    
               '<input type="file" id="upload-doc" style="display:none">'+     
               '<div class="upload-file-show">'+
                  '<span class="img-icon"></span>'+
                  '<span class="upload-path">c:/desktop/main.png</span>'+
                  '<span class="cross-icon"></span>'+
               '</div>'+
            '</div></div>';
		}	
			
	}

	function getOptionlist(opval){

		$.ajax({
			type: 'GET',
			url: baseUrl + 'admin/form_task/getListOption',
			data: {option: opval},
			success: function (data) {
				$('#option_list').html(data);
				$('.selectpicker').selectpicker('refresh');
				//var res=JSON.parse(data);
				//console.log(data);
				/*if(res.code=='200'){
				$('#option_list').html(res.res_arr);
				$('.selectpicker').selectpicker('refresh');
				}*/
	
			},
			error: function (jqXHR, exception) {
	
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'URL page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
	
				alert(msg);
			}
	
	
		});
	}

	function setRequireField(id){
		var chkReq=$('#Required'+id).is(':checked');
		if(chkReq) {
			$('#RequiredHid'+id).val('1');
		}else{
			$('#RequiredHid'+id).val('0');
		}
     
	}
	
	
	// show form section
	
	$(document).on("click","#back_form", function(){
		
		$(".inner-right-panel").css("display","block");
		
		$(".show_preview").css("display","none");
	
	})
