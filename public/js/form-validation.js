
$("#dynamic_form").validate({
    errorClass: "alert-danger",
    rules: {
       
    },
    submitHandler: function (form) {
		
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
        $('#add_task').attr('disabled', true);
        
        $.ajax({
                type: form.method,
                url: baseUrl + "admin/dynamic-form-preview",
                data: $(form).serialize(),
                beforeSend: function() {
                    $("body").addClass("loader-wrap");
                    $('.loader-img').show();
                },
                success: function(response) {
					
					$(".inner-right-panel").css("display","none");
					$(".show_preview").html(response);
					$(".show_preview").css("display","block");
                    $("body").removeClass("loader-wrap");
                    $('.loader-img').hide();
                }
            })
        }
    
});
