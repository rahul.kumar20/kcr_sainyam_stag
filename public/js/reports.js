/*var graph_data = JSON.parse($("#graph-data").attr("data-value")),
    chartLabelData = JSON.parse($("#label-data").attr("data-chart-label"));
// console.log(graph_data);
var length = Object.keys(graph_data).length;
data_date = [];
graph_data_count = [];
var i;
for (i = length - 1; i >= 0; i--) {
    data_date.push(graph_data[i].date);
}
//for (var i in graph_data) {
//    data_date.push(graph_data[i].date);
//}

for (i = length - 1; i >= 0; i--) {
    graph_data_count.push(parseFloat(graph_data[i].count));

}
//for (var i in graph_data) {
//    graph_data_count.push(parseFloat(graph_data[i].count));
//}

Highcharts.chart('container', {
    //           exporting: { enabled: false },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    credits: {
        enabled: false
    },
    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: chartLabelData.number_label
        }
    },
    xAxis: {

        title: {
            text: 'Time Period'
        },
        categories: data_date,
        Reversed: true
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },
    plotOptions: {
        series: {
            marker: {
                radius: 8
            }
        }
    },
    series: [{
        name: chartLabelData.number_label,
        data: graph_data_count
    }]

});
*/

/*
 * Apply Filter User
 */
$('.applyFilterreport').click(function () {

    var filter = {};
    filter = $('#filterVal').val();
    var pageUrl = $('#pageUrl').val();

    filter = JSON.parse(filter);

    delete filter['filterBy'];
    delete filter['status'];

    delete filter['startDate'];
    delete filter['endDate'];
    // delete filter['uid'];
    delete filter['state'];
    delete filter['distict'];
    delete filter['gender'];
    // delete filter['taskCompleted'];
    delete filter['college'];

    var days = $('.days').find(":selected").val();
    var status = $('.status').find(":selected").val();
    var state = $('.state').find(":selected").val();
    var distict = $('.distict').find(":selected").val();
    var gender = $('.gender').find(":selected").val();
    var college = $('.college').find(":selected").val();
    var taskType = $('.task').find(":selected").val();
    var filterType = 1;
    var startDate = $('.startDate').val();
    var endDate = $('.endDate').val();
    filter['filterType'] = filterType;

    // var taskCompleted = $( '.taskCompleted' ).find( ":selected" ).val();
    //status
    if (days != undefined && days.length != 0) {
        filter['filterBy'] = days;
    }
    //UID
    if (status != undefined && status.length != 0) {
        filter['status'] = status;
    }
   //task type
   if (taskType != undefined && taskType.length != 0) {
    filter['taskType'] = taskType;
}
    //state
    if (state != undefined && state.length != 0) {
        filter['state'] = state;
    }
    //district
    if (distict != undefined && distict.length != 0) {
        filter['distict'] = distict;
    }
    //gender
    if (gender != undefined && gender.length != 0) {
        filter['gender'] = gender;
    }
    //college
    if (college != undefined && college.length != 0) {
        filter['college'] = college;
    }
    //start date
    if (startDate != undefined && startDate.length != 0) {
        filter['startDate'] = startDate;
    }

    //end date
    if (endDate != undefined && endDate.length != 0) {
        filter['endDate'] = endDate;
    }
    // //task complete
    // if ( taskCompleted != undefined && taskCompleted.length != 0 ) {
    //     filter['taskCompleted'] = taskCompleted;
    // }
    if (!days.length && !status.length && !state.length && !distict.length && !college.length && !gender.length && !endDate.length && !endDate.length && !taskType.length) {
        console.log("Not any filter selected");
        return;
    }
    var filterLen = $.keyCount(filter);
    if (filterLen == 0) {
        alert('Please select a filter');
        return false;
    }

    //var queryParams = $.param(filter)
    //window.location.href = pageUrl + '?' + queryParams;

    var queryParams = JSON.stringify(filter);
    window.location.href = pageUrl + '?data=' + window.btoa(queryParams);

});

/*
 * Apply Filter User
 */
$('.topLeaders').click(function () {
    var filter = {};
    filter = $('#filterVal').val();
    var pageUrl = $('#pageUrl').val();

    filter = JSON.parse(filter);


    delete filter['startDate2'];
    delete filter['endDate2'];
    // delete filter['uid'];
    delete filter['state2'];
    delete filter['distict2'];
    delete filter['gender2'];
    // delete filter['taskCompleted'];
    delete filter['college2'];

    var state = $('.state2').find(":selected").val();
    var distict = $('.district2').find(":selected").val();
    var gender = $('.gender2').find(":selected").val();
    var college = $('.college2').find(":selected").val();
    var filterType = 2;
    var startDate = $('#startDate2').val();
    var endDate = $('#endDate2').val();
    // var taskCompleted = $( '.taskCompleted' ).find( ":selected" ).val();
    filter['filterType'] = filterType;
   

    //state
    if (state != undefined && state.length != 0) {
        filter['state2'] = state;
    }
    //district
    if (distict != undefined && distict.length != 0) {
        filter['distict2'] = distict;
    }
    //gender
    if (gender != undefined && gender.length != 0) {
        filter['gender2'] = gender;
    }
    //college
    if (college != undefined && college.length != 0) {
        filter['college2'] = college;
    }
    //start date
    if (startDate != undefined && startDate.length != 0) {
        filter['startDate2'] = startDate;
    }

    //end date
    if (endDate != undefined && endDate.length != 0) {
        filter['endDate2'] = endDate;
    }
    // //task complete
    // if ( taskCompleted != undefined && taskCompleted.length != 0 ) {
    //     filter['taskCompleted'] = taskCompleted;
    // }
    if (  !distict.length &&  !gender.length && !startDate.length && !endDate.length ) {
        console.log("Not any filter selected");
        return;
    }
    var filterLen = $.keyCount(filter);
    if (filterLen == 0) {
        alert('Please select a filter');
        return false;
    }

    //var queryParams = $.param(filter)
    //window.location.href = pageUrl + '?' + queryParams;
    // console.log(filter);return false;
    var queryParams = JSON.stringify(filter);
    window.location.href = pageUrl + '?data=' + window.btoa(queryParams);

});


///////////////////////distict list
function getDistrictForState2( url, stateId ,districtId) {
    $('#distict2').empty();
    $('#collegeDa2').empty();
    $( '#distict2').selectpicker( 'refresh' );
    $( '#collegeDa2').selectpicker( 'refresh' );

    if(stateId != undefined) {
        var state_id =  stateId;
    var csrf = csrf_token;
    var data = {
        id: state_id, 
        csrf_token: csrf
    };
    $.ajax({
        method: "POST",
        url: baseUrl + url,
        data: data,
        beforeSend: function () {
        },
        success: function (res) {
            res = JSON.parse(res);
            
            csrf_token= res.csrf_token;
            if(res.code == 200) {
                $( "#distict2" ).append( '<option value=>Select Distict</option>' );
                $.each( res.value, function ( i, item ) {
                    $( '#distict2' ).append( $( '<option>', {
                        value: item.district_id,
                        text: item.district_name,
                    } ) );
                } );
                $('#distict2').val(districtId);
    
                $( '#distict2').selectpicker( 'refresh' );
            }
   
            
        },
        error: function (xhr) {
            alert("Error occured.please try again");
        }
    });
    }
}
   //college list
function getcollegeForDistrict2( url, districtId ,collegeId = '') {
    // $('#distict').empty();
   //  $( '#distict').selectpicker( 'refresh' );
     if(districtId != undefined) {
     var csrf = csrf_token;
     var data = {
         id: districtId, 
         csrf_token: csrf
     };
     $.ajax({
         method: "POST",
         url: baseUrl + url,
         data: data,
         beforeSend: function () {
         },
         success: function (res) {
             res = JSON.parse(res);
             
             csrf_token= res.csrf_token;
             if(res.code == 200) {
                 $( "#collegeDa2" ).append( '<option value="">Select college</option>' );
                 $( "#collegeDa2" ).append( '<option value="others">Others</option>' );
                 $.each( res.value, function ( i, item ) {
                     $( '#collegeDa2' ).append( $( '<option>', {
                         value: item.college_id,
                         text: item.college_name,
                     } ) );
                 } );
                  $('#collegeDa2').val(collegeId);
     
                 $( '#collegeDa2').selectpicker( 'refresh' );
             }
    
             
         },
         error: function (xhr) {
             alert("Error occured.please try again");
         }
     });
     }
    
 }
