function gallaryFormValidate() {
    var imageValue = $('#imageCnt').val();
    var videoValue = $('#upload-video').val();
    if((imageValue != undefined && imageValue == '') && (videoValue != undefined && videoValue== ''))
    {
        $('.upload_error').show().text('Please upload image or video').fadeOut(10000);
        return false;
    } else{
        $("body").addClass("loader-wrap");
        $('.loader-img').show();
        $('#add_gallary').attr('disabled',true);
    }
}


/*
    * Apply Filter User
    */
    $('.applyFilterGallary').click(function () {

        var filter = {};
        filter = $('#filterVal').val();
        var pageUrl = $('#pageUrl').val();
    
        filter = JSON.parse(filter);
    
        delete filter['status'];
        delete filter['type'];

    
    
        var status = $('.status').find(":selected").val();
       
        var type = $('.type').find(":selected").val();
        
        //status
        if (status != undefined && status.length != 0) {
            filter['status'] = status;
        }
          //type
          if (type != undefined && type.length != 0) {
            filter['type'] = type;
        }
        if (!status.length && !type.length) {
            console.log("Not any filter selected");
            return;
        }
        var filterLen = $.keyCount(filter);
        if (filterLen == 0) {
            alert('Please select a filter');
            return false;
        }
    
        //var queryParams = $.param(filter)
        //window.location.href = pageUrl + '?' + queryParams;
    
        var queryParams = JSON.stringify(filter);
        window.location.href = pageUrl + '?data=' + window.btoa(queryParams);
    
    });

    $( document ).ready(function() {
        $('img').load(function(){
            $(this).removeClass('loader');
         });
    });
    
   