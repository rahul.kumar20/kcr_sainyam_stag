<?php
header('Access-Control-Allow-Origin: *');
if (!function_exists('base_url')) {
    function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
    {
        
        if (isset($_SERVER['HTTP_HOST'])) {
            $http       = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname   = $_SERVER['HTTP_HOST'];
            $dir        =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core       = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core       = $core[0];

            $tmplt      = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end        = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url   = sprintf($tmplt, $http, $hostname, $end);
        } else{
            $base_url   = 'http://localhost/';
        } 
        if ($parse) {
            $base_url   = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }
        return str_replace('levelone_certificate/', '', $base_url);
    }

    //Note ::  when ever you are changing this function make sure to change the same in common helper.php file
    function encrypt_decrypt($action, $string)
    {
        $output                 = false;
        $encrypt_method         = "AES-256-CBC";
        $secret_iv              = 'ipac@12361'; // change this to one more secure
        $secret_key             = 'achievement_mail';
        $key                    = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv                     = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}

$servername     = "172.16.243.169";
$username       = "stalin_db@stalin";
$password       = '{G6@vk]y|3d}';
$dbname         = 'stalinainambukirom';

try {
    $conn       = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "<center><h2> Something went wrong </h2></center>";
    exit;
}
$key                = $_GET['key'];
$key                = encrypt_decrypt('decrypt',$key);
$key                = explode('@', $key);
$userRegCode        = $key[0];
$userId             = $key[1];
$levelId            = $key[2];

if (empty($key) || $userRegCode == '') {
    //header('location:' . base_url());
    echo "<center><h2> Invalid request </h2></center>";
    exit;
}

$selectDetails      = $conn->prepare("SELECT full_name FROM users WHERE registeration_no = ? and user_id = ?");
$selectDetails->execute(array($userRegCode,$userId));
$getUserDetails     = $selectDetails->fetch(PDO::FETCH_ASSOC);

$levelDetails       = $conn->prepare("SELECT notified_date FROM tbl_user_level WHERE fk_level_id = ? and fk_user_id = ?");
$levelDetails->execute(array($levelId,$userId));
$getUserLvlDetails     = $levelDetails->fetch(PDO::FETCH_ASSOC);


if (empty($getUserDetails)) {
    //header('location:' . base_url());
    echo "<center><h2> Invalid request </h2></center>";
    exit;
}

if (empty($getUserLvlDetails)) {
    //header('location:' . base_url());
    echo "<center><h2> Invalid requests </h2></center>";
    exit;
}

$user_Name           = $getUserDetails['full_name'];
$currentDate        = date('Y-m-d');
$notifiedDate       = date('Y-m-d',strtotime($getUserLvlDetails['notified_date']));
$path               = base_url();
$levelImg           = 'Certificatelevel_1.jpg';

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Level 1 certification</title>
    </head>

<body>
        <section>
            <div id="printableArea" style="width: 980px; padding: 0px; padding-left: 20px">
                <img alt="Stalin Ani" src="Certificatelevel_1.jpg" style="width: 100%; position: relative;">
                <div style="width: 981px; text-align: center; position: absolute; top: 5px;" >
                    <div id="nameAlign" style="margin-top: 43%;font-style: inherit; text-align: center; font-size: 25px;font-family: serif;"> <?php echo $user_Name ?> </div>
                </div>
            </div>
        </section>

    <!--   Core JS Files   -->
    <script type="text/javascript">
        var printContents = document.getElementById('printableArea').innerHTML;
        var originalContents = document.body.innerHTML;

        var css = '@page { size: landscape; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

        style.type = 'text/css';
        style.media = 'print';

        if (style.styleSheet){
          style.styleSheet.cssText = css;
        } else {
          style.appendChild(document.createTextNode(css));
        }

        head.appendChild(style);

        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    </script>
</body>

</html>